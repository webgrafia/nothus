/**
 *
 * Gulpfile setup
 *
 * @since 1.0.0
 * @authors @pixeldima,@TahriAdel,@Ahmed_phi
 * @package noor
 */


// Project configuration
const project = 'noor', // Project name, used for build zip.
    buildPath = './_buildtheme/', // Files that you want to package into a zip go here
    plugin_path = '../../plugins/', // Plugins Path
    plugin_build_path = './framework/plugins/', // Plugins Path
    plugin_to_zip = ['noor_assistant'], // Plugins Path

    buildInclude = [
        // include common file types
        '**/*.php',
        '**/*.html',
        '**/*.txt',

        '**/*.scss',
        '**/*.css',
        '**/*.js',
        //Images
        '**/*.svg',
        '**/*.jpg',
        '**/*.jpeg',
        '**/*.png',
        '**/*.ico',
        '**/*.gif',
        //Font
        '**/*.ttf',
        '**/*.otf',
        '**/*.eot',
        '**/*.woff',
        '**/*.woff2',
        //Translation
        '**/*.mo',
        '**/*.pot',
        '**/*.po',

        '**/*.zip',
        '**/*.json',        
        '**/*.xml',

        'wpml-config.xml',
        'screenshot.png',
        // exclude files and folders
        '!node_modules/**/*',
        '!_buildPlugin/**/*',
        '!_buildtheme/**/*',
        '!**/*.css.map'
    ];

// Include gulp
const gulp = require('gulp');
// Include Our Plugins
const jshint = require('gulp-jshint');
const sass = require('gulp-sass');
const concat = require('gulp-concat');
const uglify = require('gulp-uglify');
const minifycss = require('gulp-minify-css');
const lineec = require('gulp-line-ending-corrector');
const autoprefixer = require('gulp-autoprefixer');
const rename = require('gulp-rename');
const notify = require('gulp-notify');
const sourcemaps = require('gulp-sourcemaps');
const filter = require('gulp-filter');
const imagemin = require('gulp-imagemin');
const newer = require('gulp-newer');
const cmq = require('gulp-combine-mq');
const plugins = require('gulp-load-plugins')({
    camelize: true
});
const ignore = require('gulp-ignore'); // Helps with ignoring files and directories in our run tasks
const rimraf = require('gulp-rimraf'); // Helps with removing files and directories in our run tasks
const zip = require('gulp-zip'); // Using to zip up our packaged theme into a tasty zip file that can be installed in WordPress!
const plumber = require('gulp-plumber'); // Helps prevent stream crashing on errors
const cache = require('gulp-cache');
const babel = require("gulp-babel");
const browserSync = require('browser-sync').create();
const projectURL = 'http://localhost/wp/noor/'; // Local project URL
const reload = browserSync.reload;
const decompress = require('gulp-decompress');
const svgstore = require('gulp-svgstore');
const svgmin = require('gulp-svgmin');
const path = require('path');
const sass_input = './framework/asset/site/css/sass/**/*.scss';
const styleDestination = './framework/asset/site/css/';
const js_es6_in = './framework/asset/site/js/es6/**/*.js';
const js_out = './framework/asset/site/js/';
const mmq = require('gulp-merge-media-queries'); // Combine matching media queries into one media query definition.

const SassDivOptions = {
    errLogToConsole: true,
    sourceComments: false,
    outputStyle: 'expanded',
    precision: 10
};

const SassOutOptions = {
    errLogToConsole: false,
    sourceComments: false,
    outputStyle: 'expanded',
    precision: 10
};

const SassMinOptions = {
    errLogToConsole: false,
    sourceComments: false,
    outputStyle: 'compact',
    precision: 10
};

// Browsers you care about for autoprefixing.
// Browserlist https ://github.com/ai/browserslist
var AUTOPREFIXER_BROWSERS = {
    browsers: ['last 2 version',
        '> 1%',
        'ie >= 9',
        'ie_mob >= 10',
        'ff >= 30',
        'chrome >= 34',
        'safari >= 7',
        'opera >= 23',
        'ios >= 7',
        'android >= 4',
        'bb >= 10'
    ]
};

// Lint Task
gulp.task('lint', function () {
    return gulp
        .src('framework/asset/site/js/module/**/*.js')
        .pipe(jshint())
        .pipe(jshint.reporter('default'));
});

/**
 * Task: `browser-sync`.
 *
 * Live Reloads, CSS injections, Localhost tunneling.
 *
 * This task does the following:
 *    1. Sets the project URL
 *    2. Sets inject CSS
 *    3. You may define a custom port
 *    4. You may want to stop the browser from openning automatically
 */
gulp.task('browser-sync', function () {
    browserSync.init({
        // For more options
        // @link http://www.browsersync.io/docs/options/

        // Project URL.
        proxy: projectURL,

        // `true` Automatically open the browser with BrowserSync live server.
        // `false` Stop the browser from automatically opening.
        open: true,

        // Inject CSS changes.
        // Commnet it to reload browser for every CSS change.
        injectChanges: true,

        // Use a specific port (instead of the one auto-detected by Browsersync).
        // port: 7000,
    });
});

/**
 * Styles
 *
 * Looking at src/sass and compiling the files into Expanded format, Autoprefixing and sending the files to the build folder
 *
 * Sass output styles: https://web-design-weekly.com/2014/06/15/different-sass-output-styles/
 */
gulp.task('sass', function () {
    return gulp.src(sass_input)
        .pipe(sourcemaps.init())
        .pipe(sass(SassDivOptions))
        .pipe(sourcemaps.write({
            includeContent: false
        }))
        .pipe(sourcemaps.init({
            loadMaps: true
        }))
        .pipe(autoprefixer(AUTOPREFIXER_BROWSERS))

        .pipe(sourcemaps.write('./map'))
        .pipe(lineec()) // Consistent Line Endings for non UNIX systems.
        .pipe(gulp.dest(styleDestination))

        .pipe(filter('**/*.css')) // Filtering stream to only css files        

        .pipe(browserSync.stream()) // Reloads style.min.css if that is enqueued.
        .pipe(notify({
            message: 'Style task for 👨‍💻 is 💯',
            onLast: true
        }))
});

gulp.task('sassOut', function () {
    return gulp.src(sass_input)
        .pipe(sourcemaps.init())
        .pipe(sass(SassOutOptions))
        .pipe(autoprefixer(AUTOPREFIXER_BROWSERS))

        .pipe(lineec())
        .pipe(gulp.dest(styleDestination))

        .pipe(filter('**/*.css')) // Filtering stream to only css files        
        .pipe(browserSync.stream()) // Reloads style.css if that is enqueued.        
        .pipe(notify({
            message: 'Style task for dev is 💯',
            onLast: true
        }))
});

gulp.task('sassMin', function () {
    return gulp.src(sass_input)
        .pipe(sass(SassMinOptions))
        .pipe(autoprefixer(AUTOPREFIXER_BROWSERS))

        .pipe(filter('**/*.css')) // Filtering stream to only css files
        .pipe(mmq({
            log: true
        })) // Merge Media Queries only for .min.css version.


        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(minifycss({
            maxLineLen: 10
        }))

        .pipe(lineec()) // Consistent Line Endings for non UNIX systems.
        .pipe(gulp.dest(styleDestination))

        .pipe(filter('**/*.css')) // Filtering stream to only css files        
        .pipe(browserSync.stream()) // Reloads style.css if that is enqueued.        
        .pipe(notify({
            message: 'Styles task for 🗜️ is 💯',
            onLast: true
        }))
});

/**
 * Images
 * Look at src/images, optimize the images and send them to the appropriate place
 */
gulp.task('images', function () {
    // Add the newer pipe to pass through newer images only
    return gulp.src(['framework/asset/images/**/*.{png,jpg,gif}'])
        .pipe(newer('framework/asset/images/'))
        .pipe(rimraf({
            force: true
        }))
        .pipe(imagemin({
            optimizationLevel: 7,
            progressive: true,
            interlaced: true
        }))
        .pipe(gulp.dest('framework/asset/images/'))
        .pipe(notify({
            message: 'Images task complete',
            onLast: true
        }));
});

/**
 * Svg Store
 * Look at framework/images/svg/,
 */
gulp.task('svgstore', function () {
    return gulp
        .src('framework/images/svg/source/*.svg')
        .pipe(svgmin(function (file) {
            var prefix = path.basename(file.relative, path.extname(file.relative));
            return {
                plugins: [{
                    cleanupIDs: {
                        prefix: prefix + '-',
                        minify: true
                    }
                }]
            }
        }))
        .pipe(svgstore())
        .pipe(gulp.dest('framework/images/svg/'));
});

/**
 *  Renem SVG files to {name}.svg.php
 *  Look at framework/images/svg/, for all svg files and change the name and add .php extantion
 */
gulp.task('svg2php', function () {
    gulp.src("framework/images/svg/source/**/*.svg")
        .pipe(rename(function (path) {
            path.suffix += ".svg";
            path.extname += '.php';
            //path.basename = path.basename.replace(/_48px/, '');
        }))
        .pipe(gulp.dest("framework/images/svg/production"));
});
var filesToMove = [
    'framework/images/svg/production/**/*.php'
];
gulp.task('move', function () {
    gulp.src(filesToMove, {
            base: 'framework/images/svg/production/'
        })
        .pipe(gulp.dest('framework/images/svg/production/out/'));
});


// to delet icon end by _48px use this command : rm -rf **/*_48px.svg
gulp.task('svgrenem', function () {
    gulp.src("framework/images/svg/source/feather/**/*.svg")
        .pipe(rename({
            prefix: "feather-",
        }))
        .pipe(gulp.dest("framework/images/svg/source/feather/"));
});


/**
 * Task: `scripts`.
 *
 * Concatenate and uglify vendor JS scripts.
 *
 * This task does the following:
 *     1. Gets the source folder for JS vendor files
 *     2. Concatenates all the files and generates vendors.js
 *     3. Renames the JS file with suffix .min.js
 *     4. Uglifes/Minifies the JS file and generates vendors.min.js
 */
gulp.task('scripts', function () {
    gulp.src('framework/asset/site/js/module/**/*.js')
        .pipe(concat('libs.js'))
        .pipe(lineec()) // Consistent Line Endings for non UNIX systems.
        .pipe(gulp.dest(js_out))
        .pipe(rename({
            basename: 'libs',
            suffix: '.min'
        }))
        .pipe(uglify())
        .pipe(lineec()) // Consistent Line Endings for non UNIX systems.
        .pipe(gulp.dest(js_out))
        .pipe(notify({
            message: 'TASK: "⚡️ scripts" 💯',
            onLast: true
        }));
});

gulp.task('es6', function () {
    return gulp.src(js_es6_in)
        .pipe(babel({
            presets: ['es2015']
        }))
        .pipe(lineec())
        .pipe(gulp.dest(js_out));
});

/**
 * Clean gulp cache
 */
gulp.task('clear', function () {
    cache.clearAll();
});

// Watch Files For Changes
gulp.task('watch', function () {
    var files = [
        './framework/asset/site/css/noor/*.css',
        './**/*.php'
    ];
    gulp.watch('framework/asset/site/js/module/**/*.js', ['scripts']);
    gulp.watch('framework/asset/site/js/es6/**/*.js', ['es6']);
    gulp.watch(sass_input, ['sass'])
        .on('change', function (evt) {
            console.log(
                '[watcher] File ' + evt.path.replace(/.*(?=sass)/, '') + ' was ' + evt.type + ', compiling...'
            );
        });
});

// Default Task
gulp.task('default', [
    'clear',
    'browser-sync',
    'sassMin',
    'sassOut',
    'svg2php',
    'svgstore',
    'imagemin',
    'sass',
    'scripts',
    'es6',
    'watch'
]);

/* ========================== Build The Theme ======================= */
const es = require('event-stream');

gulp.task('buildPlugins', function () {
    return es.merge(plugin_to_zip.map(function (obj) {
        return gulp.src(plugin_path + '/' + obj + '/**/')
            .pipe(gulp.dest('./_buildPlugin/' + obj + '/' + obj + '/'))
            .pipe(notify({
                message: 'Hooo! ' + obj + ' is complete',
                onLast: true
            }))
    }));
});

/**
 * zipPlugins task that zip plugins ( dima-shortcode,dima-portfolio) and move those
 * plugin into {theme}/framework/plugins/
 */
gulp.task('zipPlugins', ['buildPlugins'], function () {

    return es.merge(plugin_to_zip.map(function (obj) {
        return gulp.src('./_buildPlugin/' + obj + '/**/')
            .pipe(zip(obj + '.zip'))
            .pipe(notify({
                message: 'Hooo! ' + obj + '.zip is complete',
                onLast: true
            }))
            .pipe(gulp.dest(plugin_build_path));
    }));
});


gulp.task('buildFiles', function () {
    return gulp.src(buildInclude)
        .pipe(gulp.dest(buildPath+ "/" + project + "/"))
        .pipe(notify({
            message: 'BuildFiles complete',
            onLast: true
        }));
});

/**
 * Zipping build directory for distribution
 *
 * Taking the build folder, which has been cleaned, containing optimized files and zipping it up to send out as an installable theme
 */
gulp.task('zipTheme', ['buildFiles'], function () {
    return gulp.src(buildPath+"/**/*")
        .pipe(zip(project + '.zip'))
        .pipe(notify({
            message: 'Hooo! ' + project + '.zip is complete',
            onLast: true
        }))
        .pipe(gulp.dest(buildPath));
});

gulp.task('bot', ['buildFiles', 'zipTheme']);

/* ========================== !Build The Theme ======================= */