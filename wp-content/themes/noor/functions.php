<?php
/**
 * Theme functions and definitions
 *
 * @package   Dima Framework
 * @version   1.0.0
 * @since     1.0.0
 * @author    PixelDima <info@pixeldima.com>
 */


/*------------------------------------------------------------*/
# Require Files
# With require_once you can override these files with child theme it uses
# load_template() to include the files which uses require_once()
/*------------------------------------------------------------*/

/*-----------------------------*/
# System Paths
/*-----------------------------*/
$dima_admin_path = get_template_directory() . '/framework/functions/admin';
$dima_tmg_path   = get_template_directory() . '/framework/functions/admin/includes/tmg';

/*-----------------------------*/
# TMG
/*-----------------------------*/
require_once( $dima_tmg_path . '/class-tgm-plugin-activation.php' );
require_once( $dima_tmg_path . '/registration.php' );
require_once( $dima_tmg_path . '/updates.php' );

/*-----------------------------*/
# Enqueue scripts and styles.
/*-----------------------------*/
require_once( 'framework/functions/scripts.php' );
require_once( 'framework/functions/styles.php' );

/*-----------------------------*/
# Helper
/*-----------------------------*/
require_once( 'framework/functions/helper/helper.php' );
require_once( 'framework/functions/helper/filters.php' );
require_once( 'framework/functions/helper/dynamic-styles.php' );
require_once( 'framework/functions/helper/rich-sippets.php' );
require_once( 'framework/functions/helper/dima_media_support.php' );
require_once( 'framework/functions/helper/svg-icons.php' );
require_once( 'framework/functions/helper/open-graph.php' );
require_once( 'framework/functions/helper/advertisment.php' );
require_once( 'framework/functions/helper/classes.php' );
require_once( 'framework/functions/helper/pagination.php' );

require_once( 'framework/dima_framework.php' );

/*-----------------------------*/
# Output
/*-----------------------------*/
require_once( 'framework/functions/output/dima-class-breadcrumbs.php' );
require_once( 'framework/functions/output/featured.php' );
require_once( 'framework/functions/output/navbar.php' );
require_once( 'framework/functions/output/content.php' );

/*-----------------------------*/
# Global Function
/*-----------------------------*/
require_once( 'framework/functions/setup.php' );
require_once( 'framework/functions/__noor-data.php' );

/*-----------------------------*/
# specific functions.
/*-----------------------------*/
require_once( 'framework/functions/__noor-template.php' );

/*-----------------------------*/
# Widgets.
/*-----------------------------*/
require_once( 'framework/functions/widgets/widget-about.php' );
require_once( 'framework/functions/widgets/widget-twitter.php' );
require_once( 'framework/functions/widgets/widget-facebook.php' );
require_once( 'framework/functions/widgets/widget-instagram.php' );
require_once( 'framework/functions/widgets/widget-login.php' );
require_once( 'framework/functions/widgets/widget-feedburner.php' );
require_once( 'framework/functions/widgets/widget-social.php' );
require_once( 'framework/functions/widgets/widget-tabs.php' );
require_once( 'framework/functions/widgets/widget-ads.php' );
require_once( 'framework/functions/widgets/widget-adsense.php' );
require_once( 'framework/functions/widgets/widget-text-html.php' );
require_once( 'framework/functions/widgets/widget-slider.php' );

/*-----------------------------*/
# Admin.
/*-----------------------------*/
require_once( 'framework/functions/admin/meta/setup.php' );
require_once( 'framework/functions/admin/widgets.php' );
require_once( 'framework/functions/admin/customizer/setup.php' );
require_once( 'framework/functions/admin/pixeldima-setup.php' );

if ( file_exists( $dima_admin_path . '/meta/inc/cmb2/init.php' ) ) {
	require_once( 'framework/functions/admin/meta/inc/cmb2/init.php' );
	require_once( 'framework/functions/admin/meta/inc/cmb2-conditionals/cmb2-conditionals.php' );
	require_once( 'framework/functions/admin/meta/inc/cmb2-tabs/init.php' );
	require_once( 'framework/functions/admin/meta/inc/dima-meta-types/init.php' );
	require_once( 'framework/functions/admin/meta/inc/cmb2_rgba_picker/jw-cmb2-rgba-colorpicker.php' );
}

/*-----------------------------*/
# Plugins Integrations.
/*-----------------------------*/
require_once( 'framework/functions/extensions/dima-mega-menu/dima-mega-menu.php' );

if ( DIMA_WC_IS_ACTIVE ) {
	require_once( 'framework/functions/extensions/woocommerce.php' );
}
if ( DIMA_KB_IS_ACTIVE ) {
	require_once( 'framework/functions/extensions/knowledge-base.php' );
}
if ( DIMA_AMP_IS_ACTIVE ) {
	require_once( 'framework/functions/extensions/amp.php' );
}
if ( DIMA_BBPRESS_IS_ACTIVE ) {
	require_once( 'framework/functions/extensions/bbpress.php' );
}
if ( DIMA_BUDDYPRESS_IS_ACTIVE ) {
	require_once( 'framework/functions/extensions/buddypress.php' );
}
if ( DIMA_REVOLUTION_SLIDER_IS_ACTIVE ) {
	require_once( 'framework/functions/extensions/revolution-slider.php' );
}
if ( DIMA_YITH_WISHLIST_IS_ACTIVE ) {
	require_once( 'framework/functions/extensions/wc-yith-wishlist/yith-wishlist.php' );
}