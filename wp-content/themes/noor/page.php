<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 *
 * @package Dima Framework
 * @since 1.0
 * @version 1.0
 */



dima_helper::dima_get_view( dima_helper::dima_get_template(), 'wp', 'page' );
