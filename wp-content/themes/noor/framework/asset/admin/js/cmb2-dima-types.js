(function ($) {
    "use strict";
    $(document).ready(function () {

        $('.dima-switch-button').each(function () {

            var $self = $(this),
                $label = $self.find('label'),
                $button = $label.find(".button-animation");

            $label.click(function () {
                $button.toggleClass("inactive");
            });
        });

        $('.cmb2-buttonset-label').each(function () {
            var $self = $(this);
            $self.click(function () {
                var parent = $(this).parents('.cmb2-buttonset');
                $('.cmb2-buttonset-label', parent).removeClass('selected');
                $(this).addClass('selected');
            });
        });

        $('ul.cmb2-image-select-list li input[type="radio"]').each(function () {
            var $self = $(this);
            $self.click(
                function (e) {
                    e.stopPropagation(); // stop the click from bubbling
                    $(this).closest('ul').find('.cmb2-image-select-selected').removeClass('cmb2-image-select-selected');
                    $(this).parent().closest('li').addClass('cmb2-image-select-selected');
                });
        });

        $('.cmb-type-slider').each(function () {
            var $this = $(this);
            var $value = $this.find('.cmb2-slider-value');
            var $slider = $this.find('.cmb2-slider');
            var $text = $this.find('.cmb2-slider-value-text');
            var slider_data = $value.data();

            $slider.slider({
                range: 'min',
                value: slider_data.start,
                min: slider_data.min,
                step: slider_data.step,
                max: slider_data.max,
                slide: function (event, ui) {
                    $value.val(ui.value);
                    $text.text(ui.value);
                }
            });

            // Initiate the display
            $value.val($slider.slider('value'));
            $text.text($slider.slider('value'));

        });

        setTimeout(function () {
            var $elm = $(".dtheme-cmb2-tabs");
            if ($elm.length >= 1) {
                $elm.tabs();
            }
        });

        new Tippy('.tippy', {
            position: 'top',
            animation: 'scale',
            duration: 300,
            arrow: true
        });


    });

})(jQuery);




