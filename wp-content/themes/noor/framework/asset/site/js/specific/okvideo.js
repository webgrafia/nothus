/*
 * OKVideo by OKFocus v2.3.2
 * http://okfoc.us
 *
 * Copyright 2014, OKFocus
 * Licensed under the MIT license.
 *
 */

var player, OKEvents, options, youtubePlayers = new Array();

(function ($) {

    "use strict";

    var BLANK_GIF = "data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw%3D%3D";
    $.okvideo = function (options) {
        // if the option var was just a string, turn it into an object
        if (typeof options !== 'object') options = {
            'video': options
        };
        var base = this;
        // kick things off
        base.init = function () {
            base.options = $.extend({}, $.okvideo.options, options);
            // support older versions of okvideo
            if (base.options.video === null) base.options.video = base.options.source;
            base.setOptions();
            var target = base.options.target || $('body');
            var position = target[0] == $('body')[0] ? 'fixed' : 'absolute';
            var zIndex = base.options.controls === 3 ? -999 : "auto";
            if ($('#okplayer-' + base.options.id).length == 0) { //base.options.id = String(Math.round(Math.random() * 100000));
                var mask = '<div id="okplayer-mask-' + base.options.id + '" style="position:' + position + ';left:0;top:0;overflow:hidden;z-index:-998;height:100%;width:100%;"></div>';
                if (OKEvents.utils.isMobile()) {
                    target.append('<div id="okplayer-' + base.options.id + '" style="position:' + position + ';left:0;top:0;overflow:hidden;z-index:' + zIndex + ';height:100%;width:100%;"></div>');
                } else {
                    if (base.options.controls === 3) {
                        target.append(mask)
                    }
                    if (base.options.adproof === 1) {
                        target.append('<div id="okplayer-' + base.options.id + '" style="position:' + position + ';left:-10%;top:-10%;overflow:hidden;z-index:' + zIndex + ';height:120%;width:120%;"></div>');
                    } else {
                        target.append('<div id="okplayer-' + base.options.id + '" style="position:' + position + ';left:0;top:0;overflow:hidden;z-index:' + zIndex + ';height:100%;width:100%;"></div>');
                    }
                }
                $("#okplayer-mask-" + base.options.id).css("background-image", "url(" + BLANK_GIF + ")");
                if (base.options.playlist.list === null) {
                    if (base.options.video.provider === 'youtube') {
                        base.loadYouTubeAPI();
                    } else if (base.options.video.provider === 'vimeo') {
                        base.options.volume /= 100;
                        base.loadVimeoAPI();
                    }
                } else {
                    base.loadYouTubeAPI();
                }
            }
        };
        // clean the options
        base.setOptions = function () {
            // exchange 'true' for '1' and 'false' for 3
            for (var key in this.options) {
                if (this.options[key] === true) this.options[key] = 1;
                if (this.options[key] === false) this.options[key] = 3;
            }
            if (base.options.playlist.list === null) {
                base.options.video = base.determineProvider();
            }
            // pass options to the window
            $(window).data('okoptions-' + base.options.id, base.options);
        };
        // insert js into the head and exectue a callback function
        base.insertJS = function (src, callback) {
            var tag = document.createElement('script');
            if (callback) {
                if (tag.readyState) {  //IE
                    tag.onreadystatechange = function () {
                        if (tag.readyState === "loaded" ||
                            tag.readyState === "complete") {
                            tag.onreadystatechange = null;
                            callback();
                        }
                    };
                } else {
                    tag.onload = function () {
                        callback();
                    };
                }
            }
            tag.src = src;
            var firstScriptTag = document.getElementsByTagName('script')[0];
            firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
        };
        // load the youtube api
        base.loadYouTubeAPI = function (callback) {
            base.insertJS('https://www.youtube.com/player_api');
        };
        base.loadYouTubePlaylist = function () {
            player.loadPlaylist(base.options.playlist.list, base.options.playlist.index, base.options.playlist.startSeconds, base.options.playlist.suggestedQuality);
        };
        // load the vimeo api by replacing the div with an iframe and loading js
        base.loadVimeoAPI = function () {
            var source = '//player.vimeo.com/video/' + base.options.video.id + '?api=1&title=0&byline=0&portrait=0&playbar=0&loop=' + base.options.loop + '&autoplay=' + (base.options.autoplay === 1 ? 1 : 0) + '&player_id=okplayer-' + base.options.id,
                jIframe = $('<iframe data-src="' + source + '" frameborder="0" id="okplayer-' + base.options.id + '" style="visibility: hidden;" class="vimeo-background" />');
            $(window).data('okoptions-' + base.options.id).jobject = jIframe;
            $('#okplayer-' + base.options.id).replaceWith(jIframe[0]);
            base.insertJS('//origin-assets.vimeo.com/js/froogaloop2.min.js', function () {
                vimeoPlayerReady(base.options.id);
            });
        };
        // is it from youtube or vimeo?
        base.determineProvider = function () {
            var a = document.createElement('a');
            a.href = base.options.video;
            if (/youtube.com/.test(base.options.video) || /youtu.be/.test(base.options.video)) {
                var videoid = a.href.split('/')[3].toString();
                var query = videoid.substring(videoid.indexOf('?') + 1);
                if (query !== '') {
                    var vars = query.split('&');
                    for (var i = 0; i < vars.length; i++) {
                        var pair = vars[i].split('=');
                        if (pair[0] === 'v') {
                            videoid = pair[1];
                        }
                    }
                }
                return {
                    "provider": "youtube",
                    "id": videoid
                };
            } else if (/vimeo.com/.test(base.options.video)) {
                return {
                    "provider": "vimeo",
                    "id": (a.href.split('/')[3].toString()).split('#')[0],
                };
            } else if (/[-A-Za-z0-9_]+/.test(base.options.video)) {
                var id = new String(base.options.video.match(/[-A-Za-z0-9_]+/));
                if (id.length == 11) {
                    return {
                        "provider": "youtube",
                        "id": id.toString()
                    };
                } else {
                    for (var i = 0; i < base.options.video.length; i++) {
                        if (typeof parseInt(base.options.video[i]) !== "number") {
                            throw 'not vimeo but thought it was for a sec';
                        }
                    }
                    return {
                        "provider": "vimeo",
                        "id": base.options.video
                    };
                }
            } else {
                throw "OKVideo: Invalid video source";
            }
        };
        base.init();
    };
    $.okvideo.options = {
        id: null,
        source: null, // Deprecate dis l8r
        video: null,
        playlist: { // eat ur heart out @brokyo
            list: null,
            index: 0,
            startSeconds: 0,
            suggestedQuality: "default" // options: small, medium, large, hd720, hd1080, highres, default
        },
        disableKeyControl: 1,
        captions: 0,
        loop: 1,
        hd: 1,
        volume: 0,
        adproof: false,
        unstarted: null,
        onFinished: null,
        onReady: null,
        onPlay: null,
        onPause: null,
        buffering: null,
        controls: false,
        autoplay: true,
        annotations: true,
        cued: null,
        startAt: 0
    };
    $.fn.okvideo = function (options) {
        options.target = this;
        return this.each(function () {
            (new $.okvideo(options));
        });
    };

})(jQuery);

function onYouTubeIframeAPIReady() {
    YTplayers = new Array();
    jQuery('.dima-video-container.video').each(function () {
        var playerY;
        if (jQuery(this).attr('data-provider') === 'youtube') {
            var id = jQuery(this).attr('data-id');
            options = jQuery(window).data('okoptions-' + id);
            options.time = jQuery(this).attr('data-t');
            playerY = new YT.Player('okplayer-' + id, {
                videoId: options.video ? options.video.id : null,
                playerVars: {
                    'autohide': 1,
                    'autoplay': 0, //options.autoplay,
                    'disablekb': options.keyControls,
                    'cc_load_policy': options.captions,
                    'controls': options.controls,
                    'enablejsapi': 1,
                    'fs': 0,
                    'modestbranding': 1,
                    'origin': window.location.origin || (window.location.protocol + '//' + window.location.hostname),
                    'iv_load_policy': options.annotations,
                    'loop': options.loop,
                    'showinfo': 0,
                    'rel': 0,
                    'wmode': 'opaque',
                    'hd': options.hd,
                    'start': options.startAt
                },
                events: {
                    'onReady': OKEvents.yt.ready,
                    'onStateChange': OKEvents.yt.onStateChange,
                    'onError': OKEvents.yt.error
                }
            });
            YTplayers[id] = playerY;
            playerY.videoId = id;
        }
    });
}
// vimeo player ready
function vimeoPlayerReady(id) {
    options = jQuery(window).data('okoptions-' + id);
    var jIframe = options.jobject,
        iframe = jIframe[0];
    jIframe.attr('src', jIframe.data('src'));
    var playerV = $f(iframe);
    // hide player until Vimeo hides controls...
    playerV.addEvent('ready', function (e) {
        OKEvents.v.onReady(iframe);
        // "Do not try to add listeners or call functions before receiving this event."
        if (OKEvents.utils.isMobile()) {
            // mobile devices cannot listen for play event
            OKEvents.v.onPlay(playerV);
        } else {
            playerV.addEvent('play', OKEvents.v.onPlay(playerV));
            playerV.addEvent('pause', OKEvents.v.onPause);
            playerV.addEvent('finish', OKEvents.v.onFinish);
        }
        if (options.time != null) {
            playerV.api('seekTo', (options.time).replace('t=', ''));
        }

        playerV.api('play');
        jQuery(iframe).css({
            visibility: 'visible',
            opacity: 1
        });

    });
}

OKEvents = {
    yt: {
        ready: function (event) {
            var id = event.target.videoId;
            youtubePlayers[id] = event.target;
            event.target.setVolume(options.volume);
            if (options.autoplay === 1) {
                if (options.playlist.list) {
                    player.loadPlaylist(options.playlist.list, options.playlist.index, options.playlist.startSeconds, options.playlist.suggestedQuality);
                } else {
                    var inCarousel = jQuery('#okplayer-' + id).closest('.slick-item');
                    if (!inCarousel.length || (inCarousel.length && inCarousel.hasClass('active'))) {
                        if (options.time !== null) {
                            event.target.seekTo(parseInt(options.time));
                        }
                        event.target.playVideo();
                    } else {
                        event.target.pauseVideo();
                    }
                }
            }
            OKEvents.utils.isFunction(options.onReady) && options.onReady(event.target);
        },

        onStateChange: function (event) {
            var id = event.target.videoId;
            switch (event.data) {
                case -1:
                    OKEvents.utils.isFunction(options.unstarted) && options.unstarted();
                    break;
                case 0:
                    OKEvents.utils.isFunction(options.onFinished) && options.onFinished();
                    options.loop && event.target.playVideo();
                    break;
                case 1:
                    OKEvents.utils.isFunction(options.onPlay) && options.onPlay();
                    setTimeout(function () {
                        PIXELDIMA.initVideoComponent(document.body, '.dima-video-container.video, .dima-video-container.self-video');
                        jQuery('#okplayer-' + id).closest('.dima-video-container.video').css('opacity', '1');
                    }, 100);
                    break;
                case 2:
                    OKEvents.utils.isFunction(options.onPause) && options.onPause();
                    break;
                case 3:
                    OKEvents.utils.isFunction(options.buffering) && options.buffering();
                    break;
                case 5:
                    OKEvents.utils.isFunction(options.cued) && options.cued();
                    break;
                default:
                    throw "OKVideo: received invalid data from YT player.";
            }
        },
        /*error: function (event) {
         throw event;
         }*/
    },
    v: {
        onReady: function (target) {
            OKEvents.utils.isFunction(options.onReady) && options.onReady(target);
        },
        onPlay: function (player) {
            if (!OKEvents.utils.isMobile()) player.api('setVolume', options.volume);
            OKEvents.utils.isFunction(options.onPlay) && options.onPlay();
            jQuery(player.element).closest('.dima-video-container').css('opacity', '1');
        },
        onPause: function () {
            OKEvents.utils.isFunction(options.onPause) && options.onPause();
        },
        onFinish: function () {
            OKEvents.utils.isFunction(options.onFinish) && options.onFinish();
        }
    },
    utils: {
        isFunction: function (func) {
            if (typeof func === 'function') {
                return true;
            } else {
                return false;
            }
        },
        isMobile: function () {
            if (navigator.userAgent.match(/(iPhone|iPod|iPad|Android|BlackBerry)/)) {
                return true;
            } else {
                return false;
            }
        }
    }
};