(function ($) {
    let images = $("img.js-lazy-image");
    if (pxdm_js.is_lazy_image && images.length > 0) {
        images.lazyload();
    }

    $(document).ready(function () {
        this.PIXELDIMA = PIXELDIMA || {}; //Main Namespace

        /**
         * [ Main Module (PixelDima) ]
         */

        let framedLeftLine = $('.dima-framed-line.line-left').width();

        PIXELDIMA.MENU = (() => {
            const isVerticalMenu = $('.dima-navbar').hasClass('dima-navbar-vertical');

            /**
             * Magazine box filters flexmenu
             */

            /*  ========== 1. LOGO =========== */

            /**
             * Cenetr Logo
             **/
            const inline_cenet_logo = () => {
                const dima_header_style_split = $('.dima-navbar-center-active');
                if (dima_header_style_split.length) {
                    dima_header_menu_split();
                }
            };

            /**
             * Callback Function for inline_cenet_logo()
             */
            const dima_header_menu_split = () => {
                const $dima_top_menu = $('.dima-navbar-center .dima-navbar');
                if ($dima_top_menu.length) {
                    const dima_nav = $('.desk-nav.dima-navbar-center nav');
                    const logo_container = $('.desk-nav.dima-navbar-center .logo-cenetr > .logo');
                    const logo_container_two = $('.desk-nav.dima-navbar-center .logo');
                    const dima_top_navigation_li_size = dima_nav.children('ul').children('li').size();
                    const dima_top_navigation_li_break_index = Math.round(dima_top_navigation_li_size / 2) - 1;
                    if (PIXELDIMA.windowWidth > 989 && !logo_container.length && logo_container_two.length == 1) {
                        $('<li class="logo-cenetr"></li>').insertAfter($dima_top_menu.find(`nav > ul >li:nth(${dima_top_navigation_li_break_index})`));
                        logo_container_two.appendTo($dima_top_menu.find('.logo-cenetr'));
                    }
                    if (PIXELDIMA.windowWidth <= 989 && logo_container.length == 1) {
                        logo_container_two.prependTo('.dima-navbar >.container');
                        $('.logo-cenetr').remove();
                    }
                }
            };

            /* 2. MEGA MENU */
            /* =============================================== */

            /**
             * Add Image to mega-menu image
             */
            const menu_image = () => {
                $('.dima-navbar-wrap.desk-nav').find('.dima-mega-menu').each(function () {
                    const bigmenu = $(this);
                    const custom_image = bigmenu.find('.dima-custom-item-image');
                    if (custom_image.length > 0) {
                        const image_item = custom_image.find('img').attr('src');
                        const height = custom_image.find('img').attr('height');
                        const background_position = (pxdm_js.is_rtl) ? "left bottom" : "right bottom";
                        custom_image.next('.sub-menu').css({
                            'height': `${height}px`,
                            'background-image': `url(${image_item})`,
                            'background-repeat': 'no-repeat',
                            'background-position': background_position
                        });
                        custom_image.remove();
                    }
                });
            };

            let dima_mega_menu = () => {
                $('.dima-navbar-wrap.desk-nav ul.dima-nav > li.dima-mega-menu').each(
                    function (e) {
                        let $self = $(this);
                        let $item = $('> ul', $self);
                        let columns = $self.data('megamenu-columns') || 6;
                        let padding = $self.data('megamenu-padding') || '20,0,20,0';
                        if ($item.length === 0) return;
                        let padding_table = padding.split(',');

                        let default_item_css = {
                            width: 'auto',
                            height: 'auto',
                            padding: `${padding_table[0]}px ${padding_table[1]}px ${padding_table[2]}px ${padding_table[3]}px`
                        };
                        $item.css(default_item_css);

                        if (!isVerticalMenu) {
                            let child_menu = $(this).children('ul');
                            let $container = child_menu.closest('.dima-navbar-wrap.desk-nav .dima-navbar');
                            let container_width = $container.width();
                            let $window_wdith = $(window).width();
                            let megamenu_width = child_menu.outerWidth();
                            let pos = $(this).offset().left;
                            let pos_end = ($(window).width() - ($(this).offset().left + $(this).outerWidth()));

                            if (pxdm_js.is_rtl) {
                                pos = pos_end;
                            }

                            let $child_wdith_left = pos + megamenu_width;

                            if (child_menu.length === 1) {
                                /**
                                 * If the mega menu is big it will take the full width.
                                 */
                                if (megamenu_width > container_width || (pos_end + megamenu_width > container_width && $(this).offset().left + megamenu_width > container_width)) {
                                    let new_megamenu_width = container_width - parseInt(child_menu.css('padding-left')) - parseInt(child_menu.css('padding-right'));
                                    let column_width = parseFloat(new_megamenu_width - columns * parseInt($(' > li:first', child_menu).css('margin-left'))) / columns;
                                    let column_width_int = parseInt(column_width);
                                    let full_width_menu = new_megamenu_width - (column_width - column_width_int) * columns;

                                    child_menu.addClass('megamenu-fullwidth').width(full_width_menu - 60);

                                    if (framedLeftLine !== 0) {
                                        framedLeftLine = framedLeftLine + 50
                                    } else {
                                        framedLeftLine = 50
                                    }
                                    let css_1 = {'right': '0', 'left': `${-1 * pos + framedLeftLine  }px`};
                                    if (pxdm_js.is_rtl) {
                                        css_1 = {'left': '0', 'right': `${-1 * pos + framedLeftLine  }px`};
                                    }

                                    $(child_menu).css(css_1);

                                } else {

                                    let css_1 = {'right': '0', 'left': 'auto'};
                                    let css_2 = {'left': '0', 'right': 'auto'};
                                    if (pxdm_js.is_rtl) {
                                        css_1 = {'left': '0', 'right': 'auto'};
                                        css_2 = {'right': '0', 'left': 'auto'};
                                    }

                                    if ($child_wdith_left > $window_wdith) {
                                        $(child_menu).css(css_1);
                                    } else {
                                        $(child_menu).css(css_2);
                                    }
                                }
                            }
                        }

                        //Masonry
                        if ($self.hasClass('dima-megamenu-masonry')) {
                            $item.width($item.width() - 1);
                            $item.addClass('dima-megamenu-masonry-inited');

                            $item.addClass('dima-megamenu-id-' + e);
                            let $container_masonry = $('.dima-megamenu-id-' + e);

                            $container_masonry.css('width',
                                $container_masonry.outerWidth() + 1
                            );
                            $container_masonry.isotope({
                                itemSelector: '.dima-megamenu-item',
                                layoutMode: "masonry",
                                position: 'absolute',
                                isFitWidth: false,
                                resizable: false,
                                percentPosition: false
                            });
                        }
                    }
                );
            };

            /* 3. VERTICAL MENU */
            /* =============================================== */

            /**
             * vertical menu
             * calcul the "min-height" for the "vertical menu"
             * ( by decrease footer Height from window Height )
             */
            const vertical_menu_content = () => {
                $('.vertical-menu').find('.dima-main').each(function () {
                    const content = $(this);
                    const footerH = $('.dima-footer').outerHeight();
                    content.css({
                        'min-height': `${PIXELDIMA.windowHeight - footerH}px`
                    });
                });
            };

            /* 4. MENU GLOBAL */
            /* =============================================== */

            /**
             * Callback function to add a ppading top.
             */
            let add_padding_top_to_pagetitle = ($dima_header) => {
                let $dima_header_px = $('#header').outerHeight(true);
                let is_mobile = $('.dima-navbar-wrap.mobile-nav').css('display') === 'block';
                let is_vertical = $('body').hasClass('vertical-menu');
                let is_menu_transparent = $('body').hasClass('dima-transparent-navigation-active');

                if ($dima_header_px < 10) {
                    $dima_header_px = $('.dima-navbar').outerHeight(true);
                }
                if (is_vertical || (is_mobile && !is_menu_transparent)) {
                    $dima_header_px = 0;
                }
                $dima_header.css("padding-top", $dima_header_px);
            };

            /**
             * Add space after the header have the same height of the header.
             * Except Transparent Menu, Page title not on, vertical menu.
             */
            const dima_header_sizing = () => {
                const Menu_H = $('#header').find(".dima-navbar-wrap").outerHeight();
                const $menu_fixer = $('#menu-fixer');
                const $dima_main = $('.dima-main');
                const menu_fixer_boo = ($menu_fixer.length === 0);
                let is_mobile = $('.dima-navbar-wrap.mobile-nav').css('display') === 'block';

                /**
                 *  Transparent Menu, Page title not on, vertical menu
                 * @type {boolean}
                 */
                const include = !($('.dima-transparent-navigation-active').length > 0) && !($('.dima_page_title_is_on').length > 0) && !($('.vertical-menu').length > 0);

                if (menu_fixer_boo && include) {
                    let $menu_fixer_html = $('<div id="menu-fixer"></div>');
                    $dima_main.prepend($menu_fixer_html);
                    $('#menu-fixer').css({
                        'height': Menu_H,
                        'max-height': Menu_H,
                    });
                }
                setTimeout(function () {
                    /**
                     * Add padding to Page Title
                     */
                    if (!$('body').hasClass('vertical-menu') || is_mobile) {
                        add_padding_top_to_pagetitle($('.header-content'));
                    }
                });
            };

            /**
             * Drop Down Menus
             */
            const DimaDropDown = () => {
                let desk_nav_menu = $('.desk-nav ul.dima-nav, .desk-nav ul.dima-nav-tag');
                let desk_childe_li = 'li.menu-item-has-children';
                const $ul_li_li = $("ul.dima-nav li li.menu-item-has-children"); // Sub-menu
                const $ul_li = $("ul.dima-nav > li.menu-item-has-children"); // Sub-menu
                /**
                 * handle the submenu lv1+ in case it's not a Vertical menu and not mega-menu
                 */
                if (!isVerticalMenu) {
                    /**
                     * show submenu lv2+ on the left or the right depand on browser Viwe port
                     */
                    $ul_li_li.hover(function () {
                        const $ul_children = $(this).children('ul').length;
                        /**
                         * The subemenu has a subemenu (child)
                         */
                        if ($ul_children === 1) {
                            let parent = $(this);
                            let child_menu = $(this).children('ul');
                            let $parent_wdith = $(parent).width() + 2;
                            let $window_wdith = $(window).width() - 30;

                            let pos = $(parent).offset().left;
                            let css_1 = {'left': `-${$parent_wdith}px`};
                            let css_2 = {'left': `${$parent_wdith}px`};
                            if (pxdm_js.is_rtl) {
                                css_1 = {'right': `-${$parent_wdith}px`};
                                css_2 = {'right': `${$parent_wdith}px`};
                                pos = ($(window).width() - ($(parent).offset().left + $(parent).outerWidth()));
                            }

                            if (pos + $(parent).width() + $(child_menu).width() > $window_wdith) {
                                $(child_menu).css(css_1);
                            } else {
                                $(child_menu).css(css_2);
                            }
                        }
                    });

                    /**
                     * show submenu lv1 on the left or the right depand on browser Viwe port
                     */
                    $ul_li.hover(function () {
                        if ($(this).hasClass('dima-mega-menu')) {
                            return;
                        }
                        const $ul_children = $(this).children('ul').length;
                        if ($ul_children === 1) {
                            let parent = $(this);
                            let child_menu = $(this).children('ul');
                            let $window_wdith = $(window).width() - 30;

                            let pos = $(parent).offset().left;
                            let float_l = 'left';
                            let float_r = 'right';

                            if (pxdm_js.is_rtl) {
                                pos = ($(window).width() - ($(parent).offset().left + $(parent).outerWidth()));
                                float_l = 'right';
                                float_r = 'left';
                            }

                            if (pos + $(parent).width() + $(child_menu).width() > $window_wdith) {
                                $(child_menu).css({float_r: "-2px", float_l: "auto"});
                            } else {
                                $(child_menu).css({float_l: "-2px", float_r: "auto"});
                            }
                        }
                    });

                }

                /**
                 * @param el
                 */
                let menu = function (el) {
                    this.target = el;
                    this.target.hoverIntent({
                        over: this.reveal,
                        timeout: 150,// simple delay, in milliseconds, before the "out" function is called.
                        out: this.conceal,
                        selector: desk_childe_li
                    });
                };

                // Show
                menu.prototype.reveal = function () {
                    let target = $(this).children('.sub-menu');
                    if ($(this).children('.sub-menu').parent().hasClass('dima-megamenu-item')) {
                        return;
                    }

                    if (target.hasClass('is_open')) {
                        target.velocity('stop').velocity('reverse');
                    } else {
                        target.addClass('is_open');
                        target.velocity('stop').velocity(
                            'transition.slideUpIn', {
                                duration: 250,
                                delay: 0,
                                visibility: "visible",
                                display: 'undefined',
                                complete: function () {
                                    target.addClass('is_open');
                                }
                            });
                    }
                };

                // Hide
                menu.prototype.conceal = function () {
                    let target = $(this).children('.sub-menu');
                    if ($(this).children('.sub-menu').parent().hasClass('dima-megamenu-item')) {
                        return;
                    }

                    target.velocity('stop').velocity(
                        'transition.fadeOut', {
                            duration: 100,
                            delay: 0,
                            visibility: "hidden",
                            display: 'undefined',
                            complete: function () {
                                target.removeClass('is_open');
                            }
                        }
                    );

                };
                let $menu = $(desk_nav_menu);
                new menu($menu);
            };

            const mobileNav = () => {
                const $dima_nav = $(".mobile-nav .dima-nav-tag");
                const $btn = $(".mobile-nav a.dima-btn-nav");
                /**
                 * [Click Mobile button]
                 */
                $btn.click(event => {
                    event.preventDefault();
                    if ($btn.hasClass("btn-active")) {
                        $dima_nav.velocity("stop").velocity("transition.slideUpOut", 300);
                        $btn.removeClass("btn-active");
                    } else {
                        $btn.addClass("btn-active");
                        $dima_nav.velocity("stop").velocity("transition.slideDownIn", 700)
                    }
                });
                $('.mobnav-subarrow').click(function () {
                    $(this).parent().toggleClass("xpopdrop");
                });
            };

            const MobilesubMenu = () => {
                const dima_sub_icon = $('.mobile-nav .sub-icon');
                const dima_active = "dima-active";
                const submenu = dima_sub_icon.find(".sub-menu");

                dima_sub_icon.find(' > a').each(function (n) {
                    $(this).after(`<div class="dima-sub-toggle" data-toggle="collapse" data-target=".sub-menu.sm-${n}"><span class="sub-icon"><svg fill="#000000" height="24" viewBox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg"><path d="M7.41 7.84L12 12.42l4.59-4.58L18 9.25l-6 6-6-6z"/><path d="M0-.75h24v24H0z" fill="none"/></svg></span></div>`)
                });

                submenu.each(function (n) {
                    $(this).addClass(`sm-${n} collapse`);
                });

                //
                $(".dima-sub-toggle").on("click", function (n) {
                    n.preventDefault();
                    $(this).toggleClass(dima_active).closest("li").toggleClass(dima_active)
                });

                $(".mobile-nav .dima-navbar .dima-nav-tag .dima-nav li.menu-item-has-children > a").on("click", function (n) {
                    n.preventDefault();
                    $(this).parent().find(">div.dima-sub-toggle").click();
                });
            };

            const menu_animation = () => {
                if ($(".fix-headroom").length >= 1) {
                    let dimanave = document.querySelector(".dima-navbar");
                    let h = $(".dima-navbar").outerHeight();

                    let $navbar_space = $('.dima_add_space');
                    $navbar_space.addClass('dima_space').css("height", h);

                    let options = {
                        offset: h + 30,
                        tolerance: {
                            up: 15,// smoth secroll
                            down: 0
                        },
                        classes: {
                            initial: "fixed-headroom",
                            pinned: "fixed-pinned",
                            unpinned: "fixed-unpinned",
                            top: "fixed-top",
                            notTop: "fixed-not-top",
                            bottom: "fixed-bottom",
                            notBottom: "fixed-not-bottom"
                        }
                    };

                    let headroom = new Headroom(dimanave, options);
                    headroom.init();
                }

                $.fn.fix_navbar = () => {
                    const $dima_nw = $('.dima-navbar-wrap.desk-nav');
                    const $navbar = $('.dima-navbar-wrap.desk-nav .dima-navbar');
                    const $navbar_space = $('.dima_add_space');
                    const $navbarwarp = $dima_nw;
                    let navbarTop = $dima_nw.offset().top - $('#wpadminbar').outerHeight();
                    if (navbarTop < 0) {
                        navbarTop = 0;
                    }
                    if (PIXELDIMA.windowHeight < navbarTop) {
                        navbarTop = PIXELDIMA.windowHeight;
                    }

                    $(window).scroll(function () {
                        if ($(this).scrollTop() > navbarTop) {
                            $navbar.addClass('fix_nav');
                            $navbarwarp.addClass('fixed');
                            $navbar_space.addClass('dima_space');
                        } else {
                            $navbar.removeClass('fix_nav');
                            $navbarwarp.removeClass('fixed');
                            $navbar_space.removeClass('dima_space');
                        }
                    });
                };

                $.fn.show_navbar = function (el) {
                    el = $(this);
                    const $dima_nav = $('.dima-navbar');
                    const $navbarwarp = $('.dima-navbar-wrap.desk-nav');
                    const $floating = $('.dima-floating-menu');
                    const offsetBy = el.attr("data-offsetBy");
                    let oFFset = $(offsetBy).outerHeight() || 0;

                    if (typeof offsetBy === "undefined") {
                        oFFset = parseInt(el.attr("data-offsetby-px")) || 0;
                    }
                    const topBar = $('.dima-topbar').outerHeight() || 0;
                    const $navbar = $dima_nav;
                    const menuVal = $dima_nav.outerHeight() || 0;

                    const offsetTop = topBar + oFFset + menuVal;
                    $(window).scroll(function () {
                        if ($(this).scrollTop() >= offsetTop) {
                            $navbar.addClass('fix_nav animated fadeInDown');
                            $navbarwarp.addClass('fixed');
                            $floating.removeClass('container');
                        } else {
                            $navbar.removeClass('fix_nav animated fadeInDown');
                            $navbarwarp.removeClass('fixed');
                            $floating.addClass('container');
                        }
                    });
                };
            };

            const searchBox = () => {
                let search_box = $(".full-screen-menu.search-box");
                let search_container = $(".full-screen-menu.search-box .form-search .container");
                /* > search box event*/
                let SearshFullOpen = [{
                    e: search_box,
                    p: {
                        translateY: [0, "-100%"],
                    }
                    ,
                    o: {
                        delay: 0,
                        duration: 350,
                        display: "block",//undefined
                    }
                }, {
                    e: search_container,
                    p: 'transition.expandIn',
                    o: {
                        delay: 0,
                        duration: 300,
                        display: "undefined",
                    }
                },
                ];
                let SearshFullClose = [{
                    e: search_container,
                    p: 'transition.expandOut'
                    ,
                    o: {
                        delay: 0,
                        duration: 150,
                        opacity: 0,
                        display: "undefined",
                    }
                },
                    {
                        e: search_box,
                        p: {
                            translateY: ["-110%", 0],
                        }
                        ,
                        o: {
                            delay: 0,
                            duration: 350,
                            stagger: 0,
                            display: "none"
                        }
                    },];

                $(".search-btn").click(e => {
                    e.preventDefault();
                    search_box.velocity.RunSequence(SearshFullOpen);
                });

                //close search btn event
                search_box.click(function (e) {
                    e.stopPropagation();
                    if (!search_container.is(e.target) && search_container.has(e.target).length === 0) {
                        search_box.velocity.RunSequence(SearshFullClose);
                    }
                });
                /* ! search box event*/

                /* > Burger Full*/
                let menu_box = $(".menu-box");
                let info_box = $(".info-box");
                let full_div = $(".full-screen-menu .burger-full .dima-menu > li,.full-screen-menu .social-copyright > div");
                let infoFullOpen = [{
                    e: info_box,
                    p: {
                        translateY: [0, "-100%"],
                    }
                    ,
                    o: {
                        delay: 0,
                        duration: 350,
                        display: "block",
                    }
                },
                    {
                        e: full_div,
                        p: 'transition.slideDownIn'
                        ,
                        o: {
                            delay: 0,
                            duration: 300,
                            stagger: 60,
                            display: "undefined",
                        }
                    },
                ];
                let infoFullClose = [
                    {
                        e: full_div,
                        p: 'transition.slideUpOut',
                        o: {
                            delay: 0,
                            duration: 200,
                            display: "undefined",//undefined
                            opacity: 0
                        }
                    }, {
                        e: info_box,
                        p: {
                            translateY: ["-110%", 0],
                        }
                        ,
                        o: {
                            delay: 0,
                            duration: 350,
                            display: "none"
                        }
                    },];

                $(".burger-menu.info-full").click(e => {
                    e.preventDefault();
                    $(".info-box").velocity.RunSequence(infoFullOpen);
                });

                $(".full-screen-menu.info-box").click(e => {
                    e.stopPropagation();
                    let container = $(this).find('a');
                    if (!container.is(e.target) && container.has(e.target).length === 0) {
                        $(".info-box").velocity.RunSequence(infoFullClose);
                    }
                });

                let burgerFullOpen = [{
                    e: menu_box,
                    p: {
                        translateY: [0, "-100%"],
                    }
                    ,
                    o: {
                        delay: 0,
                        duration: 350,
                        display: "block",
                    }
                },
                    {
                        e: $(".full-screen-menu .burger-full .dima-menu > li,.full-screen-menu .social-copyright > div"),
                        p: 'transition.slideDownIn'
                        ,
                        o: {
                            delay: 0,
                            duration: 300,
                            stagger: 60,
                            display: "undefined",
                        }
                    },
                ];
                let burgeFullClose = [
                    {
                        e: $(".full-screen-menu .social-copyright > div,.full-screen-menu .burger-full .dima-menu > li"),
                        p: 'transition.slideUpOut',
                        o: {
                            delay: 0,
                            duration: 200,
                            stagger: 40,
                            display: "undefined",//undefined
                            opacity: 0
                        }
                    }, {
                        e: menu_box,
                        p: {
                            translateY: ["-110%", 0],
                        }
                        ,
                        o: {
                            delay: 0,
                            duration: 350,
                            stagger: 0,
                            display: "none"
                        }
                    },];

                $(".burger-menu-full").click(e => {
                    e.preventDefault();
                    $(".menu-box").velocity.RunSequence(burgerFullOpen);
                });

                /*Close*/
                $(".full-screen-menu.menu-box").click(e => {
                    e.stopPropagation();
                    let container = $(this).find('a');
                    if (!container.is(e.target) && container.has(e.target).length === 0) {
                        $(".menu-box").velocity.RunSequence(burgeFullClose);
                    }
                });
                /* ! Burger Full */

                /* > Burger menu Side */
                let menu_slider = $(".menu-slidee");
                let burger_menu_slider = $(".burger-menu-side");

                let trn = "transition.slideRightBigIn";
                if ($(".burger-menu").hasClass('burger-menu-pos-end')) {
                    trn = "transition.slideRightBigIn";
                    if (pxdm_js.is_rtl) {
                        trn = "transition.slideLeftBigIn";
                    }
                } else {
                    trn = "transition.slideLeftBigIn";
                    if (pxdm_js.is_rtl) {
                        trn = "transition.slideRightBigIn";
                    }
                }

                let burgerOpen = [{
                    e: menu_slider,
                    p: trn
                    ,
                    o: {
                        delay: 280,
                        duration: 300,
                        display: "undefined"
                    }
                },];
                let burgeClose = [{
                    e: menu_slider,
                    p: trn
                    ,
                    o: {
                        delay: 200,
                        duration: 300,
                        stagger: 0,
                        display: "undefined"
                    }
                },];

                /*Open*/
                $(".burger-menu.burger-menu-end").click(e => {
                    e.preventDefault();
                    $(".burger-menu-side").addClass("open").velocity.RunSequence(burgerOpen);
                });
                /*Close*/
                $(".dima-side-area-mask").click(e => {
                    e.preventDefault();
                    burger_menu_slider.velocity.RunSequence(burgeClose);
                    burger_menu_slider.removeClass("open");
                });

                /* ! Burger menu Side */
            };
            /**
             * OnePage
             */
            const onePage = () => {
                let dima_nav = $('.dima-navbar');
                let top_offset = parseFloat($(".dima-navbar-wrap.desk-nav .dima-navbar.fix-one").height() - 1);
                let framed_offset = parseFloat($('.dima-framed-line.line-top').height());
                let admin_offset = parseFloat($('#wpadminbar').height());
                const $desk = $(".dima-one-page-navigation-active .desk-nav .dima-nav-tag .dima-nav");
                const $mobile = $(".dima-one-page-navigation-active .mobile-nav .dima-nav");
                top_offset = top_offset + framed_offset + admin_offset;
                if (dima_nav.hasClass('dima-navbar-vertical')) {
                    top_offset = admin_offset + framed_offset;
                }

                let target_desk = $desk.find('a[href*="#"]');
                target_desk.each(function () {
                    const hrefVal = $(this).attr('href');
                    if ((hrefVal.length > 2 && hrefVal.match(/^#[^?&\/]*/g)) || hrefVal === "/") {
                        $(this).on('click', function (e) {
                            e.preventDefault();
                            e.stopPropagation();
                            let target = $(this).attr('href');
                            $(target).velocity('scroll', {
                                duration: 500,
                                offset: -(top_offset),
                                easing: 'ease-in-out'
                            });
                        });
                    }
                });

                let target_mobil = $mobile.find('a[href*="#"]');
                target_mobil.each(function () {
                    const hrefVal = $(this).attr('href');
                    if ((hrefVal.length > 2 && hrefVal.match(/^#[^?&\/]*/g)) || hrefVal === "/") {
                        $(this).on('click', function (e) {
                            e.preventDefault();
                            e.stopPropagation();
                            let target = $(this).attr('href');
                            $(target).velocity('scroll', {
                                easing: 'ease-in-out',
                                duration: 500
                            });
                        });
                    }
                });

                $("[data-scroll-link],.dima-smooth-scroll").each(function () {
                    const $self = $(this);
                    const hrefVal = $self.attr('href');
                    if ((hrefVal.length > 2 && hrefVal.match(/^#[^?&\/]*/g)) || hrefVal === "/") {
                        $(this).on('click', function (e) {
                            e.preventDefault();
                            e.stopPropagation();
                            let target = $(this).attr('href');
                            $(target).velocity('scroll', {
                                duration: 500,
                                offset: -(top_offset),
                                easing: 'ease-in-out'
                            });
                        });
                    }
                });
            };

            const myMenu = () => {
                dima_mega_menu();
                DimaDropDown();

                MobilesubMenu();
                mobileNav();

                menu_animation();

                searchBox();
                onePage();
                dima_header_sizing();
                PIXELDIMA.win.on('resize', menu_animation);
                PIXELDIMA.win.on('resize', dima_header_sizing);
            };

            /**
             * Setup Function
             */
            const init = () => {
                myMenu();
                inline_cenet_logo();
                menu_image();
                vertical_menu_content();
                $("html").imagesLoaded(); // Detect when images have been loaded.
            };

            return {
                init
            };
        })();

        PIXELDIMA.SHOP = (() => {
            const toggleBox = () => {
                $('a.show-box').click(function () {
                    const Val = $(this).attr("data-show");
                    $(Val).slideToggle();
                    return false;
                });
                $('.radio').click(function () {
                    const Val = $(this).attr("data-show");
                    $('.toHide').hide();
                    $(Val).slideToggle();
                });
                $('.checkbox').click(function () {
                    const Val = $(this).attr("data-show");
                    $(Val).slideToggle();
                });
            };

            const init = () => {
                toggleBox();
            };

            return {
                init
            };
        })();

        // Handles scrollable contents using jQuery sly and perfect scrollbar
        PIXELDIMA.SCROLL = (() => {

            const select = () => {
                $('form.wpcf7-form select, ' +
                    '.widget select, ' +
                    '.widget-arhives-empty select, ' +
                    '#bbpress-forums select, ' +
                    '.bbp-forum-form select,' +
                    '.tribe-bar-views-select,' +
                    '.dima-order-dropdown')
                    .dropkick({mobile: true});
                $('.dima-click-dropdown > a').unbind('click').bind('click', function (e) {
                    const $self = $(this).parent();
                    e.preventDefault();

                    if (!$self.hasClass('active')) {
                        $self.addClass('active').siblings('.dima-click-dropdown').removeClass('active');
                    } else {
                        $self.removeClass('active');
                    }
                });
            };

            const callingSly = () => {
                const $frame = $('#sly-frame');
                const $wrap = $frame.parent();
                const $frame_menu = $('#sly-frame-menu');
                const $wrap_menu = $frame_menu.parent();

                $frame.sly({
                    scrollBy: 200,
                    speed: 600,
                    smart: 1,
                    activatePageOn: 'click',
                    scrollBar: $wrap.find('.scrollbar'),
                    dynamicHandle: 1,
                    dragHandle: 1,
                    clickBar: 1,
                    contentEditable: 1,
                    mouseDragging: 1,
                    touchDragging: 1,
                    releaseSwing: 1,
                    swingSpeed: 0.1,
                    elasticBounds: 1,
                    cycleBy: null,
                    cycleInterval: 4000
                });

                $frame_menu.sly({
                    scrollBy: 200,
                    speed: 600,
                    smart: 1,
                    activatePageOn: 'click',
                    scrollBar: $wrap_menu.find('.scrollbar'),
                    dynamicHandle: 1,
                    dragHandle: 1,
                    clickBar: 1,
                    contentEditable: 1,
                    mouseDragging: 1,
                    touchDragging: 1,
                    releaseSwing: 1,
                    swingSpeed: 0.1,
                    elasticBounds: 1,
                    cycleBy: null,
                    cycleInterval: 4000
                });
            };
            const perfectScrollbar = () => {
                $('.quick-view-content').perfectScrollbar('update');
            };

            const parallax = () => {

                $('.background-image-hide,.background-image-holder').each(function () {
                    //noinspection JSValidateTypes
                    const imgSrc = $(this).attr('data-bg-image');
                    if (typeof imgSrc !== typeof undefined) {
                        $(this).css('background-image', `url("${imgSrc}")`);
                        //noinspection JSValidateTypes
                        //for the old version of parallex we need to hide the imag tag $(this).children('img').hide();
                    }
                    if (pxdm_js.is_lazy_image && $(this).hasClass("js-lazy-image-css")) {
                        let lazy_load = $(this).lazyload();
                        lazy_load.loadImages();
                    }
                });

                $('.parallax-background').each(function () {
                    let $start = $(this).attr('data-parallax-start');
                    let $center = $(this).attr('data-parallax-center');
                    let $bottom = $(this).attr('data-parallax-end');

                    if ($start === undefined) $start = '0%';
                    if ($center === undefined) $center = '50%';
                    if ($bottom === undefined) $bottom = '100%';

                    $(this).attr('data-bottom-top', `background-position: 50% ${$start};`);
                    $(this).attr('data-center', `background-position: 50% ${$center};`);
                    $(this).attr('data-top-bottom', `background-position: 50% ${$bottom};`);
                    $(this).attr('data-direction', 'vertical');
                });

                // Init Skrollr
                if (!(/Android|iPhone|iPad|iPod|BlackBerry|Windows Phone/i).test(navigator.userAgent || navigator.vendor || window.opera)) {
                    const skr = skrollr.init({
                        forceHeight: false,
                        smoothScrolling: true
                    });
                    // Refresh Skrollr after resizing our sections
                    skr.refresh($(".homeSlide"));
                }
            };

            const canvasBg = () => {
                $('.dima-row-bg-canvas').each(function () {
                    let $self = $(this);
                    let canvas_style = $self.data('canvas-style');
                    let canvas_id = $self.data('canvas-id');
                    let canvas_color = $self.data('canvas-color');

                    let apply_to = $self.data('canvas-size');
                    let canvas = true;
                    let wrapper = (apply_to != 'window') ? $('#' + canvas_id).parents('.vc-row-wrapper') : $(window);

                    if (canvas_color === '') {
                        canvas_color = "rgba(255, 255, 255, 0.2)";
                    }
                    if (canvas_style === 'canvas_1') {
                        $('#' + canvas_id).particleground({
                            dotColor: canvas_color,
                            lineColor: canvas_color,
                        });
                    }
                    else {
                        particlesJS.load(canvas_id, pxdm_js.DIMA_TEMPLATE_URL + '/framework/asset/site/js/specific/particlesjs-' + canvas_style + '.json', function () {
                        });
                    }
                });
            };
            const init = () => {
                parallax();
                perfectScrollbar();
                select();
                callingSly();
                canvasBg();

                PIXELDIMA.win.on('load', function () {
                    setTimeout(function () {
                        canvasBg();
                    }, 500);
                });
            };

            return {
                init
            };
        })();

        PIXELDIMA.LIGHTBOX = (() => {
            const lightBox = () => {
                const image_box = $('[data-lightbox="image"]');
                const iframe_box = $('[data-lightbox="iframe"]');
                const ajax_box = $('[data-lightbox="ajax"]');
                const gallery_box = $('[data-lightbox="gallery"]');
                const _test = $('[data-lightbox]');

                if (_test.length) {
                    image_box.magnificPopup({
                        type: 'image',
                        overflowY: 'scroll',
                        closeOnContentClick: !0,
                        closeBtnInside: !1,
                        fixedContentPos: !0,
                        mainClass: 'mfp-fade',
                        removalDelay: 500,
                        callbacks: {
                            beforeOpen() {
                                // just a hack that adds mfp-anim class to markup
                                this.st.image.markup = this.st.image.markup.replace('mfp-figure', 'mfp-figure mfp-with-anim');
                                this.st.mainClass = this.st.el.attr('data-effect');
                            },
                        },
                        midClick: true,// allow opening popup on middle mouse
                        image: {
                            verticalFit: !0
                        }
                    });

                    gallery_box.each(function () {
                        $(this).magnificPopup({
                            delegate: 'a.dima-gallery-item',
                            type: 'image',
                            overflowY: 'scroll',
                            closeOnContentClick: !0,
                            closeBtnInside: !1,
                            fixedContentPos: !0,
                            image: {
                                verticalFit: !0
                            },
                            mainClass: 'mfp-fade',
                            gallery: {
                                enabled: !0,
                                navigateByImgClick: !0,
                                preload: [0, 1]
                            }

                        });
                    });

                    //iframe ( map, youtube ...)
                    iframe_box.magnificPopup({
                        disableOn: 500,
                        type: 'iframe',
                        mainClass: 'mfp-fade',
                        callbacks: {
                            beforeOpen: function () {
                                this.st.image.markup = this.st.image.markup.replace('mfp-figure', 'mfp-figure animated ' + this.st.el.attr('data-effect'));
                            },
                        },
                        midClick: true,// allow opening popup on middle mouse
                        removalDelay: 160,
                        preloader: 50,
                        fixedContentPos: 0
                    });

                    //Stop suggested video from youtube
                    $('a[href*="youtube.com/watch"]').magnificPopup({
                        type: 'iframe',
                        iframe: {
                            patterns: {
                                youtube: {
                                    index: 'youtube.com',
                                    id: 'v=',
                                    src: '//www.youtube.com/embed/%id%?rel=0&autoplay=1'
                                }
                            }
                        }
                    });

                    //Ajax
                    ajax_box.magnificPopup({
                        type: 'ajax',
                        closeBtnInside: 0,
                        alignTop: 1,
                        cache: 1,
                        overflowY: 'scroll',
                        mainClass: "mfp-zoom-in",
                        callbacks: {
                            ajaxContentAdded() {
                                PIXELDIMA.MEDIA.mediaElement();
                                PIXELDIMA.SHOP.init();
                                PIXELDIMA.UI.init();
                                PIXELDIMA.SCROLL.init();
                            }
                        }
                    });
                }
            };

            const init = () => {
                lightBox();
            };

            const build = {
                init,
                lightBox
            };
            return build;
        })();

        PIXELDIMA.MEDIA = (() => {

            const bigVedio = () => {
                const $elm = $('.video-wrap');
                if ($elm.length) {
                    // initialize BigVideo
                    const BV = new $.BigVideo({
                        container: $elm,
                        forceAutoplay: PIXELDIMA.isTouch
                    });

                    const V = $elm.attr('data-video-wrap');
                    const img = $elm.attr('data-img-wrap');
                    if (typeof V !== typeof undefined) {
                        if (!PIXELDIMA.isTouch) {
                            BV.init();
                            BV.show(V, {
                                ambient: true,
                                doLoop: true
                            });
                        } else {
                            BV.init();
                            BV.show(img);
                        }
                    }
                }
            };

            const OkVedio = () => {
                $(".dima-video-container.video").each(function (index, el) {
                    const $this = $(this);
                    const url = $this.attr('data-video');
                    const id = $this.attr('data-id');

                    setTimeout(() => {
                        PIXELDIMA.initVideoComponent(document.body, '.dima-video-container.video, .dima-video-container.self-video');
                        jQuery(`#okplayer-${id}`).closest('.dima-video-container').css('opacity', '1');
                    }, 100);

                    $this.okvideo({
                        id,
                        source: url.split('#')[0],
                        time: (url.includes("#")) ? (url).substring((url).indexOf('#') + 1) : null,
                        autoplay: true,
                        controls: false,
                        volume: 0,
                        loop: true,
                        annotations: false,
                        hd: true,
                        caller: $this,
                    });
                });
            };

            function dima_play_video($overlay, parent) {
                let src;
                let src_splitted;
                let src_autoplay;
                const $wrapper = $overlay.parent(parent);
                const $video_iframe = $wrapper.find("iframe");
                const is_embedded = $video_iframe.length ? !0 : !1;
                if (is_embedded) {
                    if (src = $video_iframe.attr("src"), src_splitted = src.split("?"), src.includes("autoplay=")) return;
                    src_autoplay = "undefined" != typeof src_splitted[1] ? `${src_splitted[0]}?autoplay=1&amp;${src_splitted[1]}` : `${src_splitted[0]}?autoplay=1`, $video_iframe.attr({
                        src: src_autoplay
                    })
                } else $wrapper.find("video").get(0).play();
                $overlay.fadeTo(500, 0, () => {
                    $overlay.hide()
                })
            }

            $(".all_content").on("click", ".video-overlay", function (event) {
                event.preventDefault();
                dima_play_video($(this), ".post-img")
            });

            const mediaElement = () => {
                const $elm = $('.audio-video');
                const $elm_audio = $('.wp-audio-shortcode');
                const $elm_video = $('.wp-video-shortcode');

                if ($elm_audio.length) {
                    $elm_audio.each(function () {
                        const settings = {
                            startVolume: .8,
                            audioWidth: '100%',
                            pauseOtherPlayers: true,
                        };
                        if (typeof _wpmejsSettings !== 'undefined')
                            settings.pluginPath = _wpmejsSettings.pluginPath;
                        $(this).mediaelementplayer(settings);
                    });
                }

                if ($elm_video.length) {
                    $elm_video.each(function () {
                        const settings = {
                            startVolume: .8,
                            audioWidth: '100%',
                            audioHeight: 'auto',
                            pauseOtherPlayers: true,
                            alwaysShowControls: false,
                        };
                        if (typeof _wpmejsSettings !== 'undefined')
                            settings.pluginPath = _wpmejsSettings.pluginPath;
                        $(this).mediaelementplayer(settings);
                    });
                }

                if ($elm.length) {
                    $elm.each(function (element) {
                        const settings = {
                            startVolume: .8,
                            audioWidth: '100%',
                            videoVolume: 'horizontal',
                            videoWidth: '100%',
                            videoHeight: '100%',
                            pauseOtherPlayers: true,
                            alwaysShowControls: true,

                            success(mejs) {
                                let play = true;
                                const $container = $(element).find('.mejs__inner');
                                const $controls = $(element).find('.mejs__controls');

                                const controlsOn = () => {
                                    $controls.stop().animate({opacity: 1}, 150);
                                };
                                const controlsOff = () => {
                                    $controls.stop().animate({opacity: 0}, 150);
                                };
                                mejs.addEventListener('canplay', () => {
                                    if (mejs.attributes.autoplay && play) {
                                        mejs.play();
                                        play = false;
                                    }
                                    if (mejs.attributes.muted) {
                                        mejs.setMuted(true);
                                    }
                                });
                                mejs.addEventListener('ended', () => {
                                    if (mejs.attributes.loop)
                                        mejs.play();
                                });

                                if ($container.hasClass('mejs__video')) {
                                    mejs.addEventListener('playing', () => {
                                        $container.hover(controlsOn, controlsOff);
                                    });
                                    mejs.addEventListener('pause', () => {
                                        $container.off('mouseenter mouseleave');
                                        controlsOn();
                                    });
                                }
                            },

                            error() {
                                console.log('MEJS media error.');
                            }
                        };
                        if (typeof _wpmejsSettings !== 'undefined')
                            settings.pluginPath = _wpmejsSettings.pluginPath;
                        $(this).mediaelementplayer(settings);
                    });
                }
            };

            const init = () => {
                OkVedio();
                bigVedio();
                mediaElement();
            };
            return {
                init
            };
        })();

        PIXELDIMA.UI = (() => {
            const dima_spacer_shortcode = function () {
                $('.dima-spacer-module').each(function () {

                    let $self = $(this),
                        _W = PIXELDIMA.windowWidth,
                        units = $self.data('units'),
                        screen_all_spacer_size = $self.data('all_size'),

                        screen_xld_resolution = $self.data('xld_resolution'),
                        screen_xld_spacer_size = $self.data('xld_size') !== '' ? $self.data('xld_size') : screen_all_spacer_size,

                        screen_ld_resolution = $self.data('ld_resolution'),
                        screen_ld_spacer_size = $self.data('ld_size') !== '' ? $self.data('ld_size') : screen_all_spacer_size,

                        screen_md_resolution = $self.data('md_resolution'),
                        screen_md_spacer_size = $self.data('md_size') !== '' ? $self.data('md_size') : screen_all_spacer_size,

                        screen_sd_resolution = $self.data('sd_resolution'),
                        screen_sd_spacer_size = $self.data('sd_size') !== '' ? $self.data('sd_size') : screen_all_spacer_size,

                        screen_xsd_resolution = $self.data('xsd_resolution'),
                        screen_xsd_spacer_size = $self.data('xsd_size') !== '' ? $self.data('xsd_size') : screen_all_spacer_size;

                    if (units === '%' && screen_md_spacer_size !== 0 && screen_sd_spacer_size !== 0 && screen_xsd_spacer_size !== 0) {
                        screen_md_spacer_size = screen_all_spacer_size * screen_md_spacer_size / 100;
                        screen_sd_spacer_size = screen_all_spacer_size * screen_sd_spacer_size / 100;
                        screen_xsd_spacer_size = screen_all_spacer_size * screen_xsd_spacer_size / 100;
                        screen_ld_spacer_size = screen_all_spacer_size * screen_ld_spacer_size / 100;
                        screen_xld_spacer_size = screen_all_spacer_size * screen_xld_spacer_size / 100;
                    }

                    $self.css('height', screen_all_spacer_size);

                    if (_W >= screen_ld_resolution && _W <= screen_xld_resolution) {
                        $self.css('height', screen_xld_spacer_size);
                    } else if (_W >= screen_md_resolution && _W < screen_ld_resolution) {
                        $self.css('height', screen_ld_spacer_size);
                    } else if (_W >= screen_sd_resolution && _W < screen_md_resolution) {
                        $self.css('height', screen_md_spacer_size);
                    } else if (_W >= screen_xsd_resolution && _W < screen_sd_resolution) {
                        $self.css('height', screen_sd_spacer_size);
                    } else if (screen_xsd_resolution >= _W) {
                        $self.css('height', screen_xsd_spacer_size);
                    }
                });
            };

            const dima_height_responsive = function () {
                $('.dima-height-responsive').each(function () {
                    let $self = $(this),
                        _W = PIXELDIMA.windowWidth,
                        screen_all_spacer_size = 700,

                        screen_xld_resolution = $self.data('xld_resolution'),
                        screen_xld_spacer_size = $self.data('xld_size') !== '' ? $self.data('xld_size') : screen_all_spacer_size,

                        screen_ld_resolution = $self.data('ld_resolution'),
                        screen_ld_spacer_size = $self.data('ld_size') !== '' ? $self.data('ld_size') : screen_all_spacer_size,

                        screen_md_resolution = $self.data('md_resolution'),
                        screen_md_spacer_size = $self.data('md_size') !== '' ? $self.data('md_size') : screen_all_spacer_size,

                        screen_sd_resolution = $self.data('sd_resolution'),
                        screen_sd_spacer_size = $self.data('sd_size') !== '' ? $self.data('sd_size') : screen_all_spacer_size,

                        screen_xsd_resolution = $self.data('xsd_resolution'),
                        screen_xsd_spacer_size = $self.data('xsd_size') !== '' ? $self.data('xsd_size') : screen_all_spacer_size;


                    if ($self.find('.dima-block-slide')) {
                        $self = $self.find('.slick-slide');
                    }
                    $self.css('height', screen_all_spacer_size);
                    if (_W >= screen_ld_resolution && _W <= screen_xld_resolution) {
                        $self.css('height', screen_xld_spacer_size);
                    } else if (_W >= screen_md_resolution && _W < screen_ld_resolution) {
                        $self.css('height', screen_ld_spacer_size);
                    } else if (_W >= screen_sd_resolution && _W < screen_md_resolution) {
                        $self.css('height', screen_md_spacer_size);
                    } else if (_W >= screen_xsd_resolution && _W < screen_sd_resolution) {
                        $self.css('height', screen_sd_spacer_size);
                    } else if (screen_xsd_resolution >= _W) {
                        $self.css('height', screen_xsd_spacer_size);
                    }
                });
            };

            const countUp = () => {
                $(".countUp").each(function () {
                    //noinspection JSUnresolvedFunction
                    const elm = $(this);
                    new Waypoint({
                        element: elm,
                        handler: function (direction) {
                            elm.find(".number-count").countTo({
                                formatter(e) {
                                    return e = e.toFixed(), e = e.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                                }
                            });
                            this.destroy();

                        },
                        offset: "98%",
                    });
                });
            };

            const progress = () => {
                //progress bar color
                $('.progress').append(function () {
                    const elm = $(this);
                    const color = elm.attr("data-color-val");
                    elm.find('.progress-bar').css('background-color', color);
                    elm.find('.progress-bar .percent').css('background-color', color);
                    elm.find('span').css('border-top-color', color);
                });

                $(".progress .progress-bar").each(function () {
                    let $self = $(this);
                    let perc = $self.attr("aria-valuenow");
                    let current_percent = 0;

                    new Waypoint({
                        element: $self,
                        handler: function (direction) {
                            const progress = setInterval(() => {
                                if (current_percent >= perc) {
                                    clearInterval(progress);
                                } else {
                                    current_percent += 5;
                                    $self.velocity({
                                        width: `${current_percent}%`,
                                    }, {
                                        duration: 0,
                                        easing: "linear",
                                    });
                                }
                            }, 1);
                            this.destroy();
                        },
                        offset: "98%",
                    });
                });

                //noinspection JSUnresolvedFunction
                $(".dial").each(function () {
                    let elm = $(this);
                    let width = elm.attr("data-width");
                    let perc = elm.attr("value");
                    //circular progress bar color
                    new Waypoint({
                        element: elm,
                        handler: function (direction) {
                            elm.knob({
                                'value': 0,
                                'min': 0,
                                'max': 100,
                                "skin": "tron",
                                "readOnly": true,
                                "displayPrevious": true,
                                "thickness": 0.03,
                                "displayInput": false,
                                "bgColor": "rgba(255,255,255,0)",
                                "linecap": ""
                            });

                            $({value: 0}).animate({
                                value: perc
                            }, {
                                duration: 1000,
                                easing: 'swing',
                                progress() {
                                    elm.val(Math.ceil(this.value)).trigger('change');
                                }
                            });
                            elm.append(() => {
                                elm.parents('.circular-bar').find('.circular-bar-content label').text(`${perc}%`);
                            });
                            this.destroy();
                        },
                        offset: "98%",
                    });

                });

            };

            const tabs = () => {
                const $tab_id = $("#dima-tab-nav");

                $('[data-toggle="tooltip"]').tooltip({
                    animation: true,
                    html: !1,
                    delay: {
                        show: 0,
                        hide: 0
                    }
                });

                $('[data-toggle="popover"]').popover();

                $tab_id.find("a:first").tab("show"); // Select first tab
                $tab_id.find("a").click(function (e) {
                    e.preventDefault();
                    $(this).tab("show");
                });
            };

            const notification = () => {
                $(".dima-alert button.close").click(function () {
                    $(this).parent().fadeOut(200, "easeOutExpo");
                });
            };
            const initImageLayersModule = () => {

                $('.dima-image-layers-wrap').each(function () {
                    let $container = $(this),
                        layerWidth = 0,
                        initImageLayers = function () {

                            if (typeof $.fn.equalHeights !== 'undefined' && !$container.hasClass("dima-block-lazy")) {
                                $container.find('.dima-layer-container').equalHeights();
                            }
                            if (typeof $.fn.waypoint !== 'undefined') {
                                $container.waypoint(function () {
                                    $container.addClass('layer-animate');
                                }, {triggerOnce: true, offset: '70%'});
                            }
                        },
                        imageSizing = function () {
                            $container.find('.dima-layer-item').each(function () {
                                let $el = $(this),
                                    elWidth = $el.width();
                                if ($el.width() > layerWidth) {
                                    layerWidth = $el.width();
                                }
                            });

                            $container.css({'width': layerWidth});
                        };

                    PIXELDIMA.win.on('load', function () {
                        if (typeof $.fn.imagesLoaded !== 'undefined') {
                            $container.find('.dima-layer-item').imagesLoaded().done(function () {
                                imageSizing();
                                initImageLayers();
                            });
                        }
                    });

                    PIXELDIMA.win.on('resize', initImageLayers);

                    $('body').on('post-load', function () {
                        if (typeof $.fn.imagesLoaded !== 'undefined') {
                            $container.find('.dima-layer-item').imagesLoaded().done(function () {
                                imageSizing();
                            });
                        }
                        initImageLayers();
                    });
                });
            };


            const datepicker = () => {
                const elm = $("[data-datepicker]");
                elm.each(function () {
                    $(this).datepicker({
                        beforeShow(textbox, instance) {
                            $('#ui-datepicker-div').css('min-width', $(this).outerWidth());
                        },
                        showOtherMonths: true,
                        selectOtherMonths: true,
                        dateFormat: 'dd/mm/yy'
                    })
                        .datepicker('widget').wrap('<div class="ll-skin-melon"/>');
                });
            };

            const element_bg = () => {
                const elm = $("[data-element-bg]");
                elm.each(function () {
                    const b = $(this).attr("data-element-bg");
                    $(this).css({
                        "background-image": `url(${b})`,
                        "background-position": "100% 100%",
                        "background-repeat": "no-repeat"
                    });
                });

            };

            const dima_field = () => {
                $("form.matrial-form").find('input[type="search"] , input[type="text"] , input[type="email"] , textarea').after("<i class=\"bar\"></i>");
            };

            const dima_svg = () => {
                $(".svg_icon").each(function () {
                    const elm = $(this).find('svg');
                    const svg_size = $(this).data('svg-size');
                    elm.css({'width': svg_size});
                });

                $(".svg_animated").each(function () {
                    const elm = $(this).find('svg');
                    elm.find('path').css("fill", "none");
                    elm.velocity({'opacity': 0}, 0);
                    const duration = 1500;
                    setTimeout(function () {
                        const svgelm = elm.find('polyline, path, circle, rect, line');
                        new Waypoint({
                            element: elm,
                            handler: function (direction) {
                                elm.velocity({'opacity': 1}, 0);
                                svgelm
                                    .velocity({'stroke-dashoffset': 100}, 0)
                                    .velocity({'stroke-dashoffset': 0}, {duration: duration, delay: 10});
                                this.destroy();
                            },
                            offset: '98%',
                        });
                    }, 300);
                });
            };

            /**
             * Sticky Sidebars
             */
            const dima_sticky_sidebar = () => {
                let $stickySidebar = $('.sidebar-is-sticky');
                if ($stickySidebar.length) {
                    let stickySidebarBottom = 35,
                        stickySidebarTop = 0;
                    stickySidebarTop = (pxdm_js.sticky_behavior !== 'default') ? 8 : stickySidebarTop;
                    stickySidebarTop = (PIXELDIMA.body.hasClass('admin-bar')) ? stickySidebarTop + 32 : stickySidebarTop;
                    stickySidebarTop = (PIXELDIMA.body.hasClass('framed')) ? stickySidebarTop + 20 : stickySidebarTop;

                    $stickySidebar.theiaStickySidebar({
                        'additionalMarginTop': stickySidebarTop,
                        'additionalMarginBottom': stickySidebarBottom,
                        'minWidth': 990,
                    });
                }
            };

            Waves.init();
            Waves.attach('.dima-waves', ['waves-button', 'waves-light']);
            Waves.attach('.acc-with-border .dima-accordion-header', ['waves-button']);

            const init = () => {
                dima_sticky_sidebar();
                dima_svg();
                dima_spacer_shortcode();
                PIXELDIMA.win.on('load resize', dima_spacer_shortcode);
                dima_height_responsive();
                PIXELDIMA.win.on('load resize', dima_height_responsive);
                countUp();
                progress();
                tabs();
                datepicker();
                initImageLayersModule();
                notification();
                dima_field();
                element_bg();
            };

            return {
                init
            };
        })();

        //ISOTOP_PORTFOLIO
        PIXELDIMA.ISOTOP_PORTFOLIO = (() => {
            const filterIsotop = (selector, itemSelector) => {
                $(selector).each(function () {
                    const $container = $(this);
                    let $elm_filter = $('.filters-box');
                    let filter = $elm_filter.find('li.current a').data("filter");
                    if (filter === '') {
                        filter = '*';
                    }
                    let params = {
                        filter: filter,
                        itemSelector: itemSelector,
                        layoutMode: 'packery',
                        transitionDuration: 500,
                        sortBy: 'original-order',
                        resizable: true,
                        percentPosition: true,
                        masonry: {
                            columnWidth: itemSelector,
                        }
                    };

                    $container.imagesLoaded(() => {
                        $container.isotope(params);
                    });

                    $('.filters a,.filters li').click(function () {
                        const li_p = $(this).parent();
                        $('.filters .current').removeClass('current');
                        $(li_p).addClass('current');

                        const selector = $(this).attr('data-filter');
                        $container.isotope({
                            filter: selector
                        });
                        Waypoint.refreshAll();

                        /*Show items that was hiding and waiting for the class .animation-done */
                        /*And adding animation with opacity*/
                        let isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);

                        if (!isMobile) {
                            let $isoItem = $(li_p).closest('.dima-isotope-wrapper').find('.isotope-item');
                            $isoItem.each(function () {
                                $(this).addClass('animation-done').velocity({'opacity': 1}, 0);
                            });
                        }

                        return false
                    });
                });
            };

            const init = () => {
                const $container = $('.dima-isotope-container');
                filterIsotop($container, '.isotope-item');
                const $container_ = $('#members-list');
                filterIsotop($container_, 'li.even,li.odd');
            };


            return {
                init
            };
        })();

        PIXELDIMA.FOOTER = (() => {
            /* Featured Area */
            const featuredArea = () => {
                const $fa = $(".featured_area");
                const $footer_top = $(".top-footer");
                const padding_top = -1 * parseFloat($footer_top.css("padding-top"));
                const footer_height = parseFloat($footer_top.css("height"));
                $fa.removeClass('featured_area_on_sd');

                if (PIXELDIMA.windowWidth >= 989) {
                    $fa.each(function () {
                        $(this).css({
                            'height': footer_height,
                            'top': padding_top,
                            'padding-top': -1 * padding_top,
                            'position': 'absolute',
                        });
                    });
                    $footer_top.css('height', footer_height);
                } else {
                    $fa.each(function () {
                        $(this).css({
                            'height': 'auto',
                            'top': 0,
                            'padding': 20,
                            'position': 'relative',
                        });
                    });
                    $footer_top.css('height', 'auto');
                    $fa.addClass('featured_area_on_sd');
                }
            };

            if ($('.footer-container').hasClass('fixed-footer-container')) {
                const $fixed_footer = $('.fixed-footer-container');
                const fixed_footer_height = $fixed_footer.outerHeight();
                $('body').css('padding-bottom', fixed_footer_height);
            }
            /* !Featured Area */
            const init = () => {
                featuredArea();
            };
            return {
                init
            };
        })();

        PIXELDIMA.ANIMATION = (() => {

            const hideBeforeAnimation = () => {
                const elm = $("[data-animate]");
                elm.each(function () {
                    let $self = $(this),
                        $item;

                    if ($self.data('dima-animate-item')) {
                        $item = $self.find($self.data('dima-animate-item'));
                        $item.each(function () {
                            if (!$(this).hasClass('animation-done')) {
                                $(this).css('opacity', '0');
                            }
                        });
                    } else {
                        if (!$self.hasClass('animation-done')) {
                            $self.css('opacity', '0');
                        }
                    }
                });
            };

            const animations = () => {
                const elm = $("[data-animate]");

                elm.each(function () {
                        let $curr = $(this);
                        let $animation = $curr.attr("data-animate");
                        let offset = $curr.attr("data-offset") || '98%';
                        let dataDelay = parseInt($curr.attr("data-delay")) || 0;
                        let duration = parseInt($curr.attr("data-duration")) || 200;
                        let a = 0;

                        if ($curr.data('dima-animate-item')) {
                            let $item = $curr.find($curr.data('dima-animate-item'));
                            $item.each(function (i) {
                                let $self = $(this);

                                new Waypoint({
                                    element: $self,
                                    handler: function () {
                                        if (!$self.hasClass('animation-done')) {
                                            $self.addClass('animation-done')
                                                .velocity(
                                                    $animation,
                                                    {
                                                        duration: duration,
                                                        delay: (i * dataDelay),
                                                        display: 'undefined'
                                                    },
                                                );
                                        }
                                        /* Call Lazy Image*/
                                        let images = $self.find("img.js-lazy-image,.js-lazy-image-css");
                                        if (pxdm_js.is_lazy_image && images.length > 0) {
                                            let lazy_load = images.lazyload();
                                            lazy_load.loadImages();
                                        }

                                        this.destroy()
                                    },
                                    offset: offset,
                                });
                            });//each
                        } else {
                            if (a = dataDelay ? Number(dataDelay) + 10 : 300, !$curr.hasClass("animation-done")) {
                                new Waypoint({
                                    element: $curr,
                                    handler: function () {
                                        if (!$curr.hasClass('animation-done')) {
                                            $curr.addClass('animation-done')
                                                .velocity(
                                                    $animation,
                                                    {
                                                        duration: duration,
                                                        delay: a,
                                                        display: 'undefined'
                                                    }
                                                );
                                        }
                                        /* Call Lazy Image*/
                                        let images = $curr.find("img.js-lazy-image,.js-lazy-image-css");
                                        if (pxdm_js.is_lazy_image && images.length > 0) {
                                            let lazy_load = images.lazyload();
                                            lazy_load.loadImages();
                                        }

                                        this.destroy()
                                    },
                                    offset: offset,
                                });
                            }
                        }
                    }
                );

            };

            const notAnimations = () => {
                const elm = $("[data-animate]");
                elm.each(function () {
                    const elm = $(this);
                    const dataDelay = elm.attr("data-delay") || 0;
                    const offsetVal = elm.attr("data-offset") || "98%";
                    const trgger = elm.attr("data-trgger") || "false";
                    let a = 0;
                    if (a = dataDelay ? Number(dataDelay) + 300 : 300, !elm.hasClass("animated")) {
                        $(this).addClass('opacity-zero');

                        new Waypoint({
                            element: elm,
                            handler: function (direction) {
                                const $this = $(this);
                                setTimeout(() => {
                                    $this.animate({
                                        opacity: 1
                                    }, {
                                        step(now, fx) {
                                            const X = 100 * now;
                                            $(fx.elem).css("filter", `alpha(opacity=${X})`);
                                        }
                                    });
                                }, a);
                                this.destroy();
                            },
                            offset: offsetVal,

                        });
                    }
                })
            };

            const init = () => {
                if (!Modernizr.mq('only all and (max-width: 480px)')) {
                    if (PIXELDIMA.isTransitions) {
                        if (!Modernizr.touch && PIXELDIMA.win.width() > 800) {
                            hideBeforeAnimation();
                            animations();
                            $('body').on('post-load', function () {
                                hideBeforeAnimation();
                                animations();
                            });
                        }
                    } else {
                        notAnimations();
                    }
                }
            };

            return {
                init
            };
        })();

        PIXELDIMA.EVENT = (() => {
            const event = () => {
                const $dima_nav = $('.dima-navbar-wrap.desk-nav .dima-navbar');
                //Fix The Navbar
                if (PIXELDIMA.windowWidth > 960) {
                    if ($dima_nav.hasClass('fix-one')) {
                        $(".fix-one").fix_navbar();
                    }
                    //this will apply for offset-by
                    if ($dima_nav.hasClass('fix-two')) {
                        $(".fix-two").show_navbar();
                    }
                }
                //Add class active Based on URL http://css-tricks.com/snippets/jquery/add-active-navigation-class-based-on-url/
                $(`.sidebar li a[href^="${location.pathname.split("/")[2]}"]`).parent().addClass('active');

                $("[data-height]").each(function () {
                    const h = $(this).attr("data-height");
                    $(this).css("height", h);
                });
                $("[data-bg]").each(function () {
                    const bg = $(this).attr("data-bg");
                    $(this).css("background-image", `url(${bg})`);
                });
                $("[data-bg-color]").each(function () {
                    const bg = $(this).attr("data-bg-color");
                    $(this).css("background-color", bg);
                });

            };

            /**
             * Popup Module
             */
            function dimaBtnOpen(windowToOpen) {
                jQuery(windowToOpen).show();
                setTimeout(function () {
                    $('body').addClass('dima-popup-is-opend');
                    const $x = $('.dima-popup-container');
                    $x.velocity("transition.swoopIn", 700);
                }, 10);

                PIXELDIMA.html.css({'marginRight': PIXELDIMA.getScrollBarWidth(), 'overflow': 'hidden'});
            }

            /**
             * Ad Blocker
             */
            const ad_block = () => {
                if (pxdm_js.ad_blocker_detector && typeof $dimaE3 === 'undefined') {
                    PIXELDIMA.adBlock = true;
                    PIXELDIMA.html.css({'marginRight': PIXELDIMA.getScrollBarWidth(), 'overflow': 'hidden'});
                    setTimeout(function () {
                        $('body').addClass('dima-popup-is-opend');
                    }, 10);
                    dimaBtnOpen('#dima-popup-adblock');
                }
            };

            const init = () => {
                event();
                ad_block();
            };

            return {
                init
            };
        })();

        // handle the layout reinitialization on window resize
        PIXELDIMA.DOCONRESIZE = (() => {

            const nav = () => {
                const $dima_nav = $('dima-navbar-wrap.desk-nav .dima-navbar');
                PIXELDIMA.windowWidth = $(window).width();
                if ($dima_nav.hasClass('fix-one')) {
                    $(".fix-one").fix_navbar();
                }
                if ($dima_nav.hasClass('fix-two')) {
                    $(".fix-two").show_navbar();
                }
            };

            const init = () => {
                nav();
                PIXELDIMA.FOOTER.init();
            };
            return {
                init
            };
        })();

        // runs callback functions
        PIXELDIMA.PIXELDIMA_READY = (() => {

            const init = () => {
                //Please don't change the order

                PIXELDIMA.MEDIA.init();
                PIXELDIMA.SCROLL.init();
                PIXELDIMA.MENU.init();
                PIXELDIMA.LIGHTBOX.init();
                PIXELDIMA.SHOP.init();
                PIXELDIMA.ISOTOP_PORTFOLIO.init();
                PIXELDIMA.EVENT.init();
                PIXELDIMA.UI.init();
                PIXELDIMA.FOOTER.init();
                PIXELDIMA.ANIMATION.init();
                $.scrollToTop();

                $(window).resize(() => {
                    PIXELDIMA.DOCONRESIZE.init();
                });
            };

            return {
                init
            };
        })();

        /**
         * Call Our Setups Functions
         */
        PIXELDIMA.PIXELDIMA_READY.init();
    });
    /* End of document ready*/
})(jQuery);