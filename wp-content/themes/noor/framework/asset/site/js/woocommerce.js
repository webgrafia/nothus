var add_to_cart_button;

(function ($) {
    "use strict";

    /* Plus-minus buttons customization */
    var wooInitDropkick = function () {
            if ($('body').hasClass('single-product')) {
                if ($('.ul-dropdown-toggle').length > 0)
                    $('.ul-dropdown-toggle').dropkick({mobile: true});
                if ($('.variations .value select').length > 0)
                    $('.variations .value select').dropkick({mobile: true});
            }
            if ($('body.woocommerce-cart .cart-wrap .shipping select').length > 0)
                $('body.woocommerce-cart .cart-wrap .shipping select').dropkick();
        },
        shopBtn = function () {

            var $target = $('div.quantity:not(.buttons_added), td.quantity:not(.buttons_added)')
                .find('qty');
            if ($target && $target.prop('type') != 'date') {
                //buttons
                $('div.quantity:not(.buttons_added), td.quantity:not(.buttons_added)')
                    .addClass('buttons_added').append('<input type="button" value="+" class="plus dima-plus" />')
                    .prepend('<input type="button" value="-" class="minus" />');

                $('input.qty:not(.product-quantity input.qty)').each(
                    function () {
                        var min = parseFloat($(this).attr('min'));
                        if (min && min > 0 && parseFloat($(this).val()) < min) {
                            $(this).val(min);
                        }
                    }
                );
                $(document).on(
                    'click', '.plus, .minus', function () {
                        var $qty = $(this).closest('.quantity').find('.qty'),
                            Val = parseFloat($qty.val()),
                            max = parseFloat($qty.attr('max')),
                            min = parseFloat($qty.attr('min')),
                            step = $qty.attr('step');

                        if (!Val || Val === '' || Val === 'NaN') Val = 0;
                        if (max === '' || max === 'NaN') max = '';
                        if (min === '' || min === 'NaN') min = 0;
                        if (step === 'any' || step === '' || step === undefined || parseFloat(step) === 'NaN') step = 1;

                        if ($(this).is('.plus')) {
                            if (max && ( max === Val || Val > max )) {
                                $qty.val(max);
                            } else {
                                $qty.val(Val + parseFloat(step));
                            }
                        } else {
                            if (min && ( min === Val || Val < min )) {
                                $qty.val(min);
                            } else if (Val > 0) {
                                $qty.val(Val - parseFloat(step));
                            }
                        }

                        $qty.trigger('change');
                    }
                );
            }

            $(".minus").click(function () {
                var inputEl = $(this).parent().children().next();
                var qty = inputEl.val();
                if ($(this).parent().hasClass("minus"))
                    qty++;
                else
                    qty--;
            });


            $(".plus").click(function () {
                var inputEl = $(this).parent().children().next();
                var qty = inputEl.val();
                if ($(this).hasClass("plus"))
                    qty++;
                else
                    qty--;
            });

            $('.add_to_cart_button').click(function () {
                var $add_to_cart_button = $(this);
                $add_to_cart_button.find('i').removeClass('fa-shopping-bag').addClass('fa-check');
            });
        };

    var sliderRange = function () {
        var $slider_rang = $("#slider-range");
        $slider_rang.slider({
            range: true,
            min: 0,
            max: 40,
            values: [5, 30],
            slide: function (event, ui) {
                $("#amount").val("$" + ui.values[0] + " - $" + ui.values[1]);
            }
        });
        $("#amount").val("$" + $slider_rang.slider("values", 0) +
            " - $" + $slider_rang.slider("values", 1));
    };
    $(document).ready(function () {
        shopBtn();
        sliderRange();
        $('.woocommerce-page #reviews #comments ol.commentlist li').each(function () {
            var $self = $(this),
                $title = $self.find('.comment_container .comment-text .meta strong').clone(),
                $meta = $self.find('.comment_container .comment-text .meta time').clone();

            $self.find('.comment_container .comment-text .meta').text('').append($title).append($meta);
        });
        if (!$('html').hasClass('dima-ie-detected')) {
            wooInitDropkick();
            $('body').on('post-load', wooInitDropkick);

            $('body').on('post-load', function () {
                if ($('.variations .value select').length > 0)
                    $('.variations .value select').dropkick('refresh');
            });

            $('.variations_form').on('click touchend', '.reset_variations', function (e) {
                $('table.variations select').dropkick('reset', true);
            });

            $('.variations_form').on('check_variations update_variation_values hide_variation show_variation reload_product_variations', function (e) {
                $('.variations .value select').dropkick('refresh');
            });

            if ($('.woocommerce-ordering').find('select').length > 0) {
                $('.woocommerce-ordering').find('select').dropkick({mobile: true});
            }
        }

    });

    $(document.body).on('updated_wc_div cart_page_refreshed', function () {
        $(document).trigger('change input');
    });

})(jQuery);