'use strict';

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

(function ($) {
    var images = $("img.js-lazy-image");
    if (pxdm_js.is_lazy_image && images.length > 0) {
        images.lazyload();
    }

    $(document).ready(function () {
        var _this = this;

        this.PIXELDIMA = PIXELDIMA || {}; //Main Namespace

        /**
         * [ Main Module (PixelDima) ]
         */

        var framedLeftLine = $('.dima-framed-line.line-left').width();

        PIXELDIMA.MENU = function () {
            var isVerticalMenu = $('.dima-navbar').hasClass('dima-navbar-vertical');

            /**
             * Magazine box filters flexmenu
             */

            /*  ========== 1. LOGO =========== */

            /**
             * Cenetr Logo
             **/
            var inline_cenet_logo = function inline_cenet_logo() {
                var dima_header_style_split = $('.dima-navbar-center-active');
                if (dima_header_style_split.length) {
                    dima_header_menu_split();
                }
            };

            /**
             * Callback Function for inline_cenet_logo()
             */
            var dima_header_menu_split = function dima_header_menu_split() {
                var $dima_top_menu = $('.dima-navbar-center .dima-navbar');
                if ($dima_top_menu.length) {
                    var dima_nav = $('.desk-nav.dima-navbar-center nav');
                    var logo_container = $('.desk-nav.dima-navbar-center .logo-cenetr > .logo');
                    var logo_container_two = $('.desk-nav.dima-navbar-center .logo');
                    var dima_top_navigation_li_size = dima_nav.children('ul').children('li').size();
                    var dima_top_navigation_li_break_index = Math.round(dima_top_navigation_li_size / 2) - 1;
                    if (PIXELDIMA.windowWidth > 989 && !logo_container.length && logo_container_two.length == 1) {
                        $('<li class="logo-cenetr"></li>').insertAfter($dima_top_menu.find('nav > ul >li:nth(' + dima_top_navigation_li_break_index + ')'));
                        logo_container_two.appendTo($dima_top_menu.find('.logo-cenetr'));
                    }
                    if (PIXELDIMA.windowWidth <= 989 && logo_container.length == 1) {
                        logo_container_two.prependTo('.dima-navbar >.container');
                        $('.logo-cenetr').remove();
                    }
                }
            };

            /* 2. MEGA MENU */
            /* =============================================== */

            /**
             * Add Image to mega-menu image
             */
            var menu_image = function menu_image() {
                $('.dima-navbar-wrap.desk-nav').find('.dima-mega-menu').each(function () {
                    var bigmenu = $(this);
                    var custom_image = bigmenu.find('.dima-custom-item-image');
                    if (custom_image.length > 0) {
                        var image_item = custom_image.find('img').attr('src');
                        var height = custom_image.find('img').attr('height');
                        var background_position = pxdm_js.is_rtl ? "left bottom" : "right bottom";
                        custom_image.next('.sub-menu').css({
                            'height': height + 'px',
                            'background-image': 'url(' + image_item + ')',
                            'background-repeat': 'no-repeat',
                            'background-position': background_position
                        });
                        custom_image.remove();
                    }
                });
            };

            var dima_mega_menu = function dima_mega_menu() {
                $('.dima-navbar-wrap.desk-nav ul.dima-nav > li.dima-mega-menu').each(function (e) {
                    var $self = $(this);
                    var $item = $('> ul', $self);
                    var columns = $self.data('megamenu-columns') || 6;
                    var padding = $self.data('megamenu-padding') || '20,0,20,0';
                    if ($item.length === 0) return;
                    var padding_table = padding.split(',');

                    var default_item_css = {
                        width: 'auto',
                        height: 'auto',
                        padding: padding_table[0] + 'px ' + padding_table[1] + 'px ' + padding_table[2] + 'px ' + padding_table[3] + 'px'
                    };
                    $item.css(default_item_css);

                    if (!isVerticalMenu) {
                        var child_menu = $(this).children('ul');
                        var $container = child_menu.closest('.dima-navbar-wrap.desk-nav .dima-navbar');
                        var container_width = $container.width();
                        var $window_wdith = $(window).width();
                        var megamenu_width = child_menu.outerWidth();
                        var pos = $(this).offset().left;
                        var pos_end = $(window).width() - ($(this).offset().left + $(this).outerWidth());

                        if (pxdm_js.is_rtl) {
                            pos = pos_end;
                        }

                        var $child_wdith_left = pos + megamenu_width;

                        if (child_menu.length === 1) {
                            /**
                             * If the mega menu is big it will take the full width.
                             */
                            if (megamenu_width > container_width || pos_end + megamenu_width > container_width && $(this).offset().left + megamenu_width > container_width) {
                                var new_megamenu_width = container_width - parseInt(child_menu.css('padding-left')) - parseInt(child_menu.css('padding-right'));
                                var column_width = parseFloat(new_megamenu_width - columns * parseInt($(' > li:first', child_menu).css('margin-left'))) / columns;
                                var column_width_int = parseInt(column_width);
                                var full_width_menu = new_megamenu_width - (column_width - column_width_int) * columns;

                                child_menu.addClass('megamenu-fullwidth').width(full_width_menu - 60);

                                if (framedLeftLine !== 0) {
                                    framedLeftLine = framedLeftLine + 50;
                                } else {
                                    framedLeftLine = 50;
                                }
                                var css_1 = { 'right': '0', 'left': -1 * pos + framedLeftLine + 'px' };
                                if (pxdm_js.is_rtl) {
                                    css_1 = { 'left': '0', 'right': -1 * pos + framedLeftLine + 'px' };
                                }

                                $(child_menu).css(css_1);
                            } else {

                                var _css_ = { 'right': '0', 'left': 'auto' };
                                var css_2 = { 'left': '0', 'right': 'auto' };
                                if (pxdm_js.is_rtl) {
                                    _css_ = { 'left': '0', 'right': 'auto' };
                                    css_2 = { 'right': '0', 'left': 'auto' };
                                }

                                if ($child_wdith_left > $window_wdith) {
                                    $(child_menu).css(_css_);
                                } else {
                                    $(child_menu).css(css_2);
                                }
                            }
                        }
                    }

                    //Masonry
                    if ($self.hasClass('dima-megamenu-masonry')) {
                        $item.width($item.width() - 1);
                        $item.addClass('dima-megamenu-masonry-inited');

                        $item.addClass('dima-megamenu-id-' + e);
                        var $container_masonry = $('.dima-megamenu-id-' + e);

                        $container_masonry.css('width', $container_masonry.outerWidth() + 1);
                        $container_masonry.isotope({
                            itemSelector: '.dima-megamenu-item',
                            layoutMode: "masonry",
                            position: 'absolute',
                            isFitWidth: false,
                            resizable: false,
                            percentPosition: false
                        });
                    }
                });
            };

            /* 3. VERTICAL MENU */
            /* =============================================== */

            /**
             * vertical menu
             * calcul the "min-height" for the "vertical menu"
             * ( by decrease footer Height from window Height )
             */
            var vertical_menu_content = function vertical_menu_content() {
                $('.vertical-menu').find('.dima-main').each(function () {
                    var content = $(this);
                    var footerH = $('.dima-footer').outerHeight();
                    content.css({
                        'min-height': PIXELDIMA.windowHeight - footerH + 'px'
                    });
                });
            };

            /* 4. MENU GLOBAL */
            /* =============================================== */

            /**
             * Callback function to add a ppading top.
             */
            var add_padding_top_to_pagetitle = function add_padding_top_to_pagetitle($dima_header) {
                var $dima_header_px = $('#header').outerHeight(true);
                var is_mobile = $('.dima-navbar-wrap.mobile-nav').css('display') === 'block';
                var is_vertical = $('body').hasClass('vertical-menu');
                var is_menu_transparent = $('body').hasClass('dima-transparent-navigation-active');

                if ($dima_header_px < 10) {
                    $dima_header_px = $('.dima-navbar').outerHeight(true);
                }
                if (is_vertical || is_mobile && !is_menu_transparent) {
                    $dima_header_px = 0;
                }
                $dima_header.css("padding-top", $dima_header_px);
            };

            /**
             * Add space after the header have the same height of the header.
             * Except Transparent Menu, Page title not on, vertical menu.
             */
            var dima_header_sizing = function dima_header_sizing() {
                var Menu_H = $('#header').find(".dima-navbar-wrap").outerHeight();
                var $menu_fixer = $('#menu-fixer');
                var $dima_main = $('.dima-main');
                var menu_fixer_boo = $menu_fixer.length === 0;
                var is_mobile = $('.dima-navbar-wrap.mobile-nav').css('display') === 'block';

                /**
                 *  Transparent Menu, Page title not on, vertical menu
                 * @type {boolean}
                 */
                var include = !($('.dima-transparent-navigation-active').length > 0) && !($('.dima_page_title_is_on').length > 0) && !($('.vertical-menu').length > 0);

                if (menu_fixer_boo && include) {
                    var $menu_fixer_html = $('<div id="menu-fixer"></div>');
                    $dima_main.prepend($menu_fixer_html);
                    $('#menu-fixer').css({
                        'height': Menu_H,
                        'max-height': Menu_H
                    });
                }
                setTimeout(function () {
                    /**
                     * Add padding to Page Title
                     */
                    if (!$('body').hasClass('vertical-menu') || is_mobile) {
                        add_padding_top_to_pagetitle($('.header-content'));
                    }
                });
            };

            /**
             * Drop Down Menus
             */
            var DimaDropDown = function DimaDropDown() {
                var desk_nav_menu = $('.desk-nav ul.dima-nav, .desk-nav ul.dima-nav-tag');
                var desk_childe_li = 'li.menu-item-has-children';
                var $ul_li_li = $("ul.dima-nav li li.menu-item-has-children"); // Sub-menu
                var $ul_li = $("ul.dima-nav > li.menu-item-has-children"); // Sub-menu
                /**
                 * handle the submenu lv1+ in case it's not a Vertical menu and not mega-menu
                 */
                if (!isVerticalMenu) {
                    /**
                     * show submenu lv2+ on the left or the right depand on browser Viwe port
                     */
                    $ul_li_li.hover(function () {
                        var $ul_children = $(this).children('ul').length;
                        /**
                         * The subemenu has a subemenu (child)
                         */
                        if ($ul_children === 1) {
                            var parent = $(this);
                            var child_menu = $(this).children('ul');
                            var $parent_wdith = $(parent).width() + 2;
                            var $window_wdith = $(window).width() - 30;

                            var pos = $(parent).offset().left;
                            var css_1 = { 'left': '-' + $parent_wdith + 'px' };
                            var css_2 = { 'left': $parent_wdith + 'px' };
                            if (pxdm_js.is_rtl) {
                                css_1 = { 'right': '-' + $parent_wdith + 'px' };
                                css_2 = { 'right': $parent_wdith + 'px' };
                                pos = $(window).width() - ($(parent).offset().left + $(parent).outerWidth());
                            }

                            if (pos + $(parent).width() + $(child_menu).width() > $window_wdith) {
                                $(child_menu).css(css_1);
                            } else {
                                $(child_menu).css(css_2);
                            }
                        }
                    });

                    /**
                     * show submenu lv1 on the left or the right depand on browser Viwe port
                     */
                    $ul_li.hover(function () {
                        if ($(this).hasClass('dima-mega-menu')) {
                            return;
                        }
                        var $ul_children = $(this).children('ul').length;
                        if ($ul_children === 1) {
                            var parent = $(this);
                            var child_menu = $(this).children('ul');
                            var $window_wdith = $(window).width() - 30;

                            var pos = $(parent).offset().left;
                            var float_l = 'left';
                            var float_r = 'right';

                            if (pxdm_js.is_rtl) {
                                pos = $(window).width() - ($(parent).offset().left + $(parent).outerWidth());
                                float_l = 'right';
                                float_r = 'left';
                            }

                            if (pos + $(parent).width() + $(child_menu).width() > $window_wdith) {
                                $(child_menu).css({ float_r: "-2px", float_l: "auto" });
                            } else {
                                $(child_menu).css({ float_l: "-2px", float_r: "auto" });
                            }
                        }
                    });
                }

                /**
                 * @param el
                 */
                var menu = function menu(el) {
                    this.target = el;
                    this.target.hoverIntent({
                        over: this.reveal,
                        timeout: 150, // simple delay, in milliseconds, before the "out" function is called.
                        out: this.conceal,
                        selector: desk_childe_li
                    });
                };

                // Show
                menu.prototype.reveal = function () {
                    var target = $(this).children('.sub-menu');
                    if ($(this).children('.sub-menu').parent().hasClass('dima-megamenu-item')) {
                        return;
                    }

                    if (target.hasClass('is_open')) {
                        target.velocity('stop').velocity('reverse');
                    } else {
                        target.addClass('is_open');
                        target.velocity('stop').velocity('transition.slideUpIn', {
                            duration: 250,
                            delay: 0,
                            visibility: "visible",
                            display: 'undefined',
                            complete: function complete() {
                                target.addClass('is_open');
                            }
                        });
                    }
                };

                // Hide
                menu.prototype.conceal = function () {
                    var target = $(this).children('.sub-menu');
                    if ($(this).children('.sub-menu').parent().hasClass('dima-megamenu-item')) {
                        return;
                    }

                    target.velocity('stop').velocity('transition.fadeOut', {
                        duration: 100,
                        delay: 0,
                        visibility: "hidden",
                        display: 'undefined',
                        complete: function complete() {
                            target.removeClass('is_open');
                        }
                    });
                };
                var $menu = $(desk_nav_menu);
                new menu($menu);
            };

            var mobileNav = function mobileNav() {
                var $dima_nav = $(".mobile-nav .dima-nav-tag");
                var $btn = $(".mobile-nav a.dima-btn-nav");
                /**
                 * [Click Mobile button]
                 */
                $btn.click(function (event) {
                    event.preventDefault();
                    if ($btn.hasClass("btn-active")) {
                        $dima_nav.velocity("stop").velocity("transition.slideUpOut", 300);
                        $btn.removeClass("btn-active");
                    } else {
                        $btn.addClass("btn-active");
                        $dima_nav.velocity("stop").velocity("transition.slideDownIn", 700);
                    }
                });
                $('.mobnav-subarrow').click(function () {
                    $(this).parent().toggleClass("xpopdrop");
                });
            };

            var MobilesubMenu = function MobilesubMenu() {
                var dima_sub_icon = $('.mobile-nav .sub-icon');
                var dima_active = "dima-active";
                var submenu = dima_sub_icon.find(".sub-menu");

                dima_sub_icon.find(' > a').each(function (n) {
                    $(this).after('<div class="dima-sub-toggle" data-toggle="collapse" data-target=".sub-menu.sm-' + n + '"><span class="sub-icon"><svg fill="#000000" height="24" viewBox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg"><path d="M7.41 7.84L12 12.42l4.59-4.58L18 9.25l-6 6-6-6z"/><path d="M0-.75h24v24H0z" fill="none"/></svg></span></div>');
                });

                submenu.each(function (n) {
                    $(this).addClass('sm-' + n + ' collapse');
                });

                //
                $(".dima-sub-toggle").on("click", function (n) {
                    n.preventDefault();
                    $(this).toggleClass(dima_active).closest("li").toggleClass(dima_active);
                });

                $(".mobile-nav .dima-navbar .dima-nav-tag .dima-nav li.menu-item-has-children > a").on("click", function (n) {
                    n.preventDefault();
                    $(this).parent().find(">div.dima-sub-toggle").click();
                });
            };

            var menu_animation = function menu_animation() {
                if ($(".fix-headroom").length >= 1) {
                    var dimanave = document.querySelector(".dima-navbar");
                    var h = $(".dima-navbar").outerHeight();

                    var $navbar_space = $('.dima_add_space');
                    $navbar_space.addClass('dima_space').css("height", h);

                    var options = {
                        offset: h + 30,
                        tolerance: {
                            up: 15, // smoth secroll
                            down: 0
                        },
                        classes: {
                            initial: "fixed-headroom",
                            pinned: "fixed-pinned",
                            unpinned: "fixed-unpinned",
                            top: "fixed-top",
                            notTop: "fixed-not-top",
                            bottom: "fixed-bottom",
                            notBottom: "fixed-not-bottom"
                        }
                    };

                    var headroom = new Headroom(dimanave, options);
                    headroom.init();
                }

                $.fn.fix_navbar = function () {
                    var $dima_nw = $('.dima-navbar-wrap.desk-nav');
                    var $navbar = $('.dima-navbar-wrap.desk-nav .dima-navbar');
                    var $navbar_space = $('.dima_add_space');
                    var $navbarwarp = $dima_nw;
                    var navbarTop = $dima_nw.offset().top - $('#wpadminbar').outerHeight();
                    if (navbarTop < 0) {
                        navbarTop = 0;
                    }
                    if (PIXELDIMA.windowHeight < navbarTop) {
                        navbarTop = PIXELDIMA.windowHeight;
                    }

                    $(window).scroll(function () {
                        if ($(this).scrollTop() > navbarTop) {
                            $navbar.addClass('fix_nav');
                            $navbarwarp.addClass('fixed');
                            $navbar_space.addClass('dima_space');
                        } else {
                            $navbar.removeClass('fix_nav');
                            $navbarwarp.removeClass('fixed');
                            $navbar_space.removeClass('dima_space');
                        }
                    });
                };

                $.fn.show_navbar = function (el) {
                    el = $(this);
                    var $dima_nav = $('.dima-navbar');
                    var $navbarwarp = $('.dima-navbar-wrap.desk-nav');
                    var $floating = $('.dima-floating-menu');
                    var offsetBy = el.attr("data-offsetBy");
                    var oFFset = $(offsetBy).outerHeight() || 0;

                    if (typeof offsetBy === "undefined") {
                        oFFset = parseInt(el.attr("data-offsetby-px")) || 0;
                    }
                    var topBar = $('.dima-topbar').outerHeight() || 0;
                    var $navbar = $dima_nav;
                    var menuVal = $dima_nav.outerHeight() || 0;

                    var offsetTop = topBar + oFFset + menuVal;
                    $(window).scroll(function () {
                        if ($(this).scrollTop() >= offsetTop) {
                            $navbar.addClass('fix_nav animated fadeInDown');
                            $navbarwarp.addClass('fixed');
                            $floating.removeClass('container');
                        } else {
                            $navbar.removeClass('fix_nav animated fadeInDown');
                            $navbarwarp.removeClass('fixed');
                            $floating.addClass('container');
                        }
                    });
                };
            };

            var searchBox = function searchBox() {
                var search_box = $(".full-screen-menu.search-box");
                var search_container = $(".full-screen-menu.search-box .form-search .container");
                /* > search box event*/
                var SearshFullOpen = [{
                    e: search_box,
                    p: {
                        translateY: [0, "-100%"]
                    },

                    o: {
                        delay: 0,
                        duration: 350,
                        display: "block" //undefined
                    }
                }, {
                    e: search_container,
                    p: 'transition.expandIn',
                    o: {
                        delay: 0,
                        duration: 300,
                        display: "undefined"
                    }
                }];
                var SearshFullClose = [{
                    e: search_container,
                    p: 'transition.expandOut',

                    o: {
                        delay: 0,
                        duration: 150,
                        opacity: 0,
                        display: "undefined"
                    }
                }, {
                    e: search_box,
                    p: {
                        translateY: ["-110%", 0]
                    },

                    o: {
                        delay: 0,
                        duration: 350,
                        stagger: 0,
                        display: "none"
                    }
                }];

                $(".search-btn").click(function (e) {
                    e.preventDefault();
                    search_box.velocity.RunSequence(SearshFullOpen);
                });

                //close search btn event
                search_box.click(function (e) {
                    e.stopPropagation();
                    if (!search_container.is(e.target) && search_container.has(e.target).length === 0) {
                        search_box.velocity.RunSequence(SearshFullClose);
                    }
                });
                /* ! search box event*/

                /* > Burger Full*/
                var menu_box = $(".menu-box");
                var info_box = $(".info-box");
                var full_div = $(".full-screen-menu .burger-full .dima-menu > li,.full-screen-menu .social-copyright > div");
                var infoFullOpen = [{
                    e: info_box,
                    p: {
                        translateY: [0, "-100%"]
                    },

                    o: {
                        delay: 0,
                        duration: 350,
                        display: "block"
                    }
                }, {
                    e: full_div,
                    p: 'transition.slideDownIn',

                    o: {
                        delay: 0,
                        duration: 300,
                        stagger: 60,
                        display: "undefined"
                    }
                }];
                var infoFullClose = [{
                    e: full_div,
                    p: 'transition.slideUpOut',
                    o: {
                        delay: 0,
                        duration: 200,
                        display: "undefined", //undefined
                        opacity: 0
                    }
                }, {
                    e: info_box,
                    p: {
                        translateY: ["-110%", 0]
                    },

                    o: {
                        delay: 0,
                        duration: 350,
                        display: "none"
                    }
                }];

                $(".burger-menu.info-full").click(function (e) {
                    e.preventDefault();
                    $(".info-box").velocity.RunSequence(infoFullOpen);
                });

                $(".full-screen-menu.info-box").click(function (e) {
                    e.stopPropagation();
                    var container = $(_this).find('a');
                    if (!container.is(e.target) && container.has(e.target).length === 0) {
                        $(".info-box").velocity.RunSequence(infoFullClose);
                    }
                });

                var burgerFullOpen = [{
                    e: menu_box,
                    p: {
                        translateY: [0, "-100%"]
                    },

                    o: {
                        delay: 0,
                        duration: 350,
                        display: "block"
                    }
                }, {
                    e: $(".full-screen-menu .burger-full .dima-menu > li,.full-screen-menu .social-copyright > div"),
                    p: 'transition.slideDownIn',

                    o: {
                        delay: 0,
                        duration: 300,
                        stagger: 60,
                        display: "undefined"
                    }
                }];
                var burgeFullClose = [{
                    e: $(".full-screen-menu .social-copyright > div,.full-screen-menu .burger-full .dima-menu > li"),
                    p: 'transition.slideUpOut',
                    o: {
                        delay: 0,
                        duration: 200,
                        stagger: 40,
                        display: "undefined", //undefined
                        opacity: 0
                    }
                }, {
                    e: menu_box,
                    p: {
                        translateY: ["-110%", 0]
                    },

                    o: {
                        delay: 0,
                        duration: 350,
                        stagger: 0,
                        display: "none"
                    }
                }];

                $(".burger-menu-full").click(function (e) {
                    e.preventDefault();
                    $(".menu-box").velocity.RunSequence(burgerFullOpen);
                });

                /*Close*/
                $(".full-screen-menu.menu-box").click(function (e) {
                    e.stopPropagation();
                    var container = $(_this).find('a');
                    if (!container.is(e.target) && container.has(e.target).length === 0) {
                        $(".menu-box").velocity.RunSequence(burgeFullClose);
                    }
                });
                /* ! Burger Full */

                /* > Burger menu Side */
                var menu_slider = $(".menu-slidee");
                var burger_menu_slider = $(".burger-menu-side");

                var trn = "transition.slideRightBigIn";
                if ($(".burger-menu").hasClass('burger-menu-pos-end')) {
                    trn = "transition.slideRightBigIn";
                    if (pxdm_js.is_rtl) {
                        trn = "transition.slideLeftBigIn";
                    }
                } else {
                    trn = "transition.slideLeftBigIn";
                    if (pxdm_js.is_rtl) {
                        trn = "transition.slideRightBigIn";
                    }
                }

                var burgerOpen = [{
                    e: menu_slider,
                    p: trn,

                    o: {
                        delay: 280,
                        duration: 300,
                        display: "undefined"
                    }
                }];
                var burgeClose = [{
                    e: menu_slider,
                    p: trn,

                    o: {
                        delay: 200,
                        duration: 300,
                        stagger: 0,
                        display: "undefined"
                    }
                }];

                /*Open*/
                $(".burger-menu.burger-menu-end").click(function (e) {
                    e.preventDefault();
                    $(".burger-menu-side").addClass("open").velocity.RunSequence(burgerOpen);
                });
                /*Close*/
                $(".dima-side-area-mask").click(function (e) {
                    e.preventDefault();
                    burger_menu_slider.velocity.RunSequence(burgeClose);
                    burger_menu_slider.removeClass("open");
                });

                /* ! Burger menu Side */
            };
            /**
             * OnePage
             */
            var onePage = function onePage() {
                var dima_nav = $('.dima-navbar');
                var top_offset = parseFloat($(".dima-navbar-wrap.desk-nav .dima-navbar.fix-one").height() - 1);
                var framed_offset = parseFloat($('.dima-framed-line.line-top').height());
                var admin_offset = parseFloat($('#wpadminbar').height());
                var $desk = $(".dima-one-page-navigation-active .desk-nav .dima-nav-tag .dima-nav");
                var $mobile = $(".dima-one-page-navigation-active .mobile-nav .dima-nav");
                top_offset = top_offset + framed_offset + admin_offset;
                if (dima_nav.hasClass('dima-navbar-vertical')) {
                    top_offset = admin_offset + framed_offset;
                }

                var target_desk = $desk.find('a[href*="#"]');
                target_desk.each(function () {
                    var hrefVal = $(this).attr('href');
                    if (hrefVal.length > 2 && hrefVal.match(/^#[^?&\/]*/g) || hrefVal === "/") {
                        $(this).on('click', function (e) {
                            e.preventDefault();
                            e.stopPropagation();
                            var target = $(this).attr('href');
                            $(target).velocity('scroll', {
                                duration: 500,
                                offset: -top_offset,
                                easing: 'ease-in-out'
                            });
                        });
                    }
                });

                var target_mobil = $mobile.find('a[href*="#"]');
                target_mobil.each(function () {
                    var hrefVal = $(this).attr('href');
                    if (hrefVal.length > 2 && hrefVal.match(/^#[^?&\/]*/g) || hrefVal === "/") {
                        $(this).on('click', function (e) {
                            e.preventDefault();
                            e.stopPropagation();
                            var target = $(this).attr('href');
                            $(target).velocity('scroll', {
                                easing: 'ease-in-out',
                                duration: 500
                            });
                        });
                    }
                });

                $("[data-scroll-link],.dima-smooth-scroll").each(function () {
                    var $self = $(this);
                    var hrefVal = $self.attr('href');
                    if (hrefVal.length > 2 && hrefVal.match(/^#[^?&\/]*/g) || hrefVal === "/") {
                        $(this).on('click', function (e) {
                            e.preventDefault();
                            e.stopPropagation();
                            var target = $(this).attr('href');
                            $(target).velocity('scroll', {
                                duration: 500,
                                offset: -top_offset,
                                easing: 'ease-in-out'
                            });
                        });
                    }
                });
            };

            var myMenu = function myMenu() {
                dima_mega_menu();
                DimaDropDown();

                MobilesubMenu();
                mobileNav();

                menu_animation();

                searchBox();
                onePage();
                dima_header_sizing();
                PIXELDIMA.win.on('resize', menu_animation);
                PIXELDIMA.win.on('resize', dima_header_sizing);
            };

            /**
             * Setup Function
             */
            var init = function init() {
                myMenu();
                inline_cenet_logo();
                menu_image();
                vertical_menu_content();
                $("html").imagesLoaded(); // Detect when images have been loaded.
            };

            return {
                init: init
            };
        }();

        PIXELDIMA.SHOP = function () {
            var toggleBox = function toggleBox() {
                $('a.show-box').click(function () {
                    var Val = $(this).attr("data-show");
                    $(Val).slideToggle();
                    return false;
                });
                $('.radio').click(function () {
                    var Val = $(this).attr("data-show");
                    $('.toHide').hide();
                    $(Val).slideToggle();
                });
                $('.checkbox').click(function () {
                    var Val = $(this).attr("data-show");
                    $(Val).slideToggle();
                });
            };

            var init = function init() {
                toggleBox();
            };

            return {
                init: init
            };
        }();

        // Handles scrollable contents using jQuery sly and perfect scrollbar
        PIXELDIMA.SCROLL = function () {

            var select = function select() {
                $('form.wpcf7-form select, ' + '.widget select, ' + '.widget-arhives-empty select, ' + '#bbpress-forums select, ' + '.bbp-forum-form select,' + '.tribe-bar-views-select,' + '.dima-order-dropdown').dropkick({ mobile: true });
                $('.dima-click-dropdown > a').unbind('click').bind('click', function (e) {
                    var $self = $(this).parent();
                    e.preventDefault();

                    if (!$self.hasClass('active')) {
                        $self.addClass('active').siblings('.dima-click-dropdown').removeClass('active');
                    } else {
                        $self.removeClass('active');
                    }
                });
            };

            var callingSly = function callingSly() {
                var $frame = $('#sly-frame');
                var $wrap = $frame.parent();
                var $frame_menu = $('#sly-frame-menu');
                var $wrap_menu = $frame_menu.parent();

                $frame.sly({
                    scrollBy: 200,
                    speed: 600,
                    smart: 1,
                    activatePageOn: 'click',
                    scrollBar: $wrap.find('.scrollbar'),
                    dynamicHandle: 1,
                    dragHandle: 1,
                    clickBar: 1,
                    contentEditable: 1,
                    mouseDragging: 1,
                    touchDragging: 1,
                    releaseSwing: 1,
                    swingSpeed: 0.1,
                    elasticBounds: 1,
                    cycleBy: null,
                    cycleInterval: 4000
                });

                $frame_menu.sly({
                    scrollBy: 200,
                    speed: 600,
                    smart: 1,
                    activatePageOn: 'click',
                    scrollBar: $wrap_menu.find('.scrollbar'),
                    dynamicHandle: 1,
                    dragHandle: 1,
                    clickBar: 1,
                    contentEditable: 1,
                    mouseDragging: 1,
                    touchDragging: 1,
                    releaseSwing: 1,
                    swingSpeed: 0.1,
                    elasticBounds: 1,
                    cycleBy: null,
                    cycleInterval: 4000
                });
            };
            var perfectScrollbar = function perfectScrollbar() {
                $('.quick-view-content').perfectScrollbar('update');
            };

            var parallax = function parallax() {

                $('.background-image-hide,.background-image-holder').each(function () {
                    //noinspection JSValidateTypes
                    var imgSrc = $(this).attr('data-bg-image');
                    if ((typeof imgSrc === 'undefined' ? 'undefined' : _typeof(imgSrc)) !== (typeof undefined === 'undefined' ? 'undefined' : _typeof(undefined))) {
                        $(this).css('background-image', 'url("' + imgSrc + '")');
                        //noinspection JSValidateTypes
                        //for the old version of parallex we need to hide the imag tag $(this).children('img').hide();
                    }
                    if (pxdm_js.is_lazy_image && $(this).hasClass("js-lazy-image-css")) {
                        var lazy_load = $(this).lazyload();
                        lazy_load.loadImages();
                    }
                });

                $('.parallax-background').each(function () {
                    var $start = $(this).attr('data-parallax-start');
                    var $center = $(this).attr('data-parallax-center');
                    var $bottom = $(this).attr('data-parallax-end');

                    if ($start === undefined) $start = '0%';
                    if ($center === undefined) $center = '50%';
                    if ($bottom === undefined) $bottom = '100%';

                    $(this).attr('data-bottom-top', 'background-position: 50% ' + $start + ';');
                    $(this).attr('data-center', 'background-position: 50% ' + $center + ';');
                    $(this).attr('data-top-bottom', 'background-position: 50% ' + $bottom + ';');
                    $(this).attr('data-direction', 'vertical');
                });

                // Init Skrollr
                if (!/Android|iPhone|iPad|iPod|BlackBerry|Windows Phone/i.test(navigator.userAgent || navigator.vendor || window.opera)) {
                    var skr = skrollr.init({
                        forceHeight: false,
                        smoothScrolling: true
                    });
                    // Refresh Skrollr after resizing our sections
                    skr.refresh($(".homeSlide"));
                }
            };

            var canvasBg = function canvasBg() {
                $('.dima-row-bg-canvas').each(function () {
                    var $self = $(this);
                    var canvas_style = $self.data('canvas-style');
                    var canvas_id = $self.data('canvas-id');
                    var canvas_color = $self.data('canvas-color');

                    var apply_to = $self.data('canvas-size');
                    var canvas = true;
                    var wrapper = apply_to != 'window' ? $('#' + canvas_id).parents('.vc-row-wrapper') : $(window);

                    if (canvas_color === '') {
                        canvas_color = "rgba(255, 255, 255, 0.2)";
                    }
                    if (canvas_style === 'canvas_1') {
                        $('#' + canvas_id).particleground({
                            dotColor: canvas_color,
                            lineColor: canvas_color
                        });
                    } else {
                        particlesJS.load(canvas_id, pxdm_js.DIMA_TEMPLATE_URL + '/framework/asset/site/js/specific/particlesjs-' + canvas_style + '.json', function () {});
                    }
                });
            };
            var init = function init() {
                parallax();
                perfectScrollbar();
                select();
                callingSly();
                canvasBg();

                PIXELDIMA.win.on('load', function () {
                    setTimeout(function () {
                        canvasBg();
                    }, 500);
                });
            };

            return {
                init: init
            };
        }();

        PIXELDIMA.LIGHTBOX = function () {
            var lightBox = function lightBox() {
                var image_box = $('[data-lightbox="image"]');
                var iframe_box = $('[data-lightbox="iframe"]');
                var ajax_box = $('[data-lightbox="ajax"]');
                var gallery_box = $('[data-lightbox="gallery"]');
                var _test = $('[data-lightbox]');

                if (_test.length) {
                    image_box.magnificPopup({
                        type: 'image',
                        overflowY: 'scroll',
                        closeOnContentClick: !0,
                        closeBtnInside: !1,
                        fixedContentPos: !0,
                        mainClass: 'mfp-fade',
                        removalDelay: 500,
                        callbacks: {
                            beforeOpen: function beforeOpen() {
                                // just a hack that adds mfp-anim class to markup
                                this.st.image.markup = this.st.image.markup.replace('mfp-figure', 'mfp-figure mfp-with-anim');
                                this.st.mainClass = this.st.el.attr('data-effect');
                            }
                        },
                        midClick: true, // allow opening popup on middle mouse
                        image: {
                            verticalFit: !0
                        }
                    });

                    gallery_box.each(function () {
                        $(this).magnificPopup({
                            delegate: 'a.dima-gallery-item',
                            type: 'image',
                            overflowY: 'scroll',
                            closeOnContentClick: !0,
                            closeBtnInside: !1,
                            fixedContentPos: !0,
                            image: {
                                verticalFit: !0
                            },
                            mainClass: 'mfp-fade',
                            gallery: {
                                enabled: !0,
                                navigateByImgClick: !0,
                                preload: [0, 1]
                            }

                        });
                    });

                    //iframe ( map, youtube ...)
                    iframe_box.magnificPopup({
                        disableOn: 500,
                        type: 'iframe',
                        mainClass: 'mfp-fade',
                        callbacks: {
                            beforeOpen: function beforeOpen() {
                                this.st.image.markup = this.st.image.markup.replace('mfp-figure', 'mfp-figure animated ' + this.st.el.attr('data-effect'));
                            }
                        },
                        midClick: true, // allow opening popup on middle mouse
                        removalDelay: 160,
                        preloader: 50,
                        fixedContentPos: 0
                    });

                    //Stop suggested video from youtube
                    $('a[href*="youtube.com/watch"]').magnificPopup({
                        type: 'iframe',
                        iframe: {
                            patterns: {
                                youtube: {
                                    index: 'youtube.com',
                                    id: 'v=',
                                    src: '//www.youtube.com/embed/%id%?rel=0&autoplay=1'
                                }
                            }
                        }
                    });

                    //Ajax
                    ajax_box.magnificPopup({
                        type: 'ajax',
                        closeBtnInside: 0,
                        alignTop: 1,
                        cache: 1,
                        overflowY: 'scroll',
                        mainClass: "mfp-zoom-in",
                        callbacks: {
                            ajaxContentAdded: function ajaxContentAdded() {
                                PIXELDIMA.MEDIA.mediaElement();
                                PIXELDIMA.SHOP.init();
                                PIXELDIMA.UI.init();
                                PIXELDIMA.SCROLL.init();
                            }
                        }
                    });
                }
            };

            var init = function init() {
                lightBox();
            };

            var build = {
                init: init,
                lightBox: lightBox
            };
            return build;
        }();

        PIXELDIMA.MEDIA = function () {

            var bigVedio = function bigVedio() {
                var $elm = $('.video-wrap');
                if ($elm.length) {
                    // initialize BigVideo
                    var BV = new $.BigVideo({
                        container: $elm,
                        forceAutoplay: PIXELDIMA.isTouch
                    });

                    var V = $elm.attr('data-video-wrap');
                    var img = $elm.attr('data-img-wrap');
                    if ((typeof V === 'undefined' ? 'undefined' : _typeof(V)) !== (typeof undefined === 'undefined' ? 'undefined' : _typeof(undefined))) {
                        if (!PIXELDIMA.isTouch) {
                            BV.init();
                            BV.show(V, {
                                ambient: true,
                                doLoop: true
                            });
                        } else {
                            BV.init();
                            BV.show(img);
                        }
                    }
                }
            };

            var OkVedio = function OkVedio() {
                $(".dima-video-container.video").each(function (index, el) {
                    var $this = $(this);
                    var url = $this.attr('data-video');
                    var id = $this.attr('data-id');

                    setTimeout(function () {
                        PIXELDIMA.initVideoComponent(document.body, '.dima-video-container.video, .dima-video-container.self-video');
                        jQuery('#okplayer-' + id).closest('.dima-video-container').css('opacity', '1');
                    }, 100);

                    $this.okvideo({
                        id: id,
                        source: url.split('#')[0],
                        time: url.includes("#") ? url.substring(url.indexOf('#') + 1) : null,
                        autoplay: true,
                        controls: false,
                        volume: 0,
                        loop: true,
                        annotations: false,
                        hd: true,
                        caller: $this
                    });
                });
            };

            function dima_play_video($overlay, parent) {
                var src = void 0;
                var src_splitted = void 0;
                var src_autoplay = void 0;
                var $wrapper = $overlay.parent(parent);
                var $video_iframe = $wrapper.find("iframe");
                var is_embedded = $video_iframe.length ? !0 : !1;
                if (is_embedded) {
                    if (src = $video_iframe.attr("src"), src_splitted = src.split("?"), src.includes("autoplay=")) return;
                    src_autoplay = "undefined" != typeof src_splitted[1] ? src_splitted[0] + '?autoplay=1&amp;' + src_splitted[1] : src_splitted[0] + '?autoplay=1', $video_iframe.attr({
                        src: src_autoplay
                    });
                } else $wrapper.find("video").get(0).play();
                $overlay.fadeTo(500, 0, function () {
                    $overlay.hide();
                });
            }

            $(".all_content").on("click", ".video-overlay", function (event) {
                event.preventDefault();
                dima_play_video($(this), ".post-img");
            });

            var mediaElement = function mediaElement() {
                var $elm = $('.audio-video');
                var $elm_audio = $('.wp-audio-shortcode');
                var $elm_video = $('.wp-video-shortcode');

                if ($elm_audio.length) {
                    $elm_audio.each(function () {
                        var settings = {
                            startVolume: .8,
                            audioWidth: '100%',
                            pauseOtherPlayers: true
                        };
                        if (typeof _wpmejsSettings !== 'undefined') settings.pluginPath = _wpmejsSettings.pluginPath;
                        $(this).mediaelementplayer(settings);
                    });
                }

                if ($elm_video.length) {
                    $elm_video.each(function () {
                        var settings = {
                            startVolume: .8,
                            audioWidth: '100%',
                            audioHeight: 'auto',
                            pauseOtherPlayers: true,
                            alwaysShowControls: false
                        };
                        if (typeof _wpmejsSettings !== 'undefined') settings.pluginPath = _wpmejsSettings.pluginPath;
                        $(this).mediaelementplayer(settings);
                    });
                }

                if ($elm.length) {
                    $elm.each(function (element) {
                        var settings = {
                            startVolume: .8,
                            audioWidth: '100%',
                            videoVolume: 'horizontal',
                            videoWidth: '100%',
                            videoHeight: '100%',
                            pauseOtherPlayers: true,
                            alwaysShowControls: true,

                            success: function success(mejs) {
                                var play = true;
                                var $container = $(element).find('.mejs__inner');
                                var $controls = $(element).find('.mejs__controls');

                                var controlsOn = function controlsOn() {
                                    $controls.stop().animate({ opacity: 1 }, 150);
                                };
                                var controlsOff = function controlsOff() {
                                    $controls.stop().animate({ opacity: 0 }, 150);
                                };
                                mejs.addEventListener('canplay', function () {
                                    if (mejs.attributes.autoplay && play) {
                                        mejs.play();
                                        play = false;
                                    }
                                    if (mejs.attributes.muted) {
                                        mejs.setMuted(true);
                                    }
                                });
                                mejs.addEventListener('ended', function () {
                                    if (mejs.attributes.loop) mejs.play();
                                });

                                if ($container.hasClass('mejs__video')) {
                                    mejs.addEventListener('playing', function () {
                                        $container.hover(controlsOn, controlsOff);
                                    });
                                    mejs.addEventListener('pause', function () {
                                        $container.off('mouseenter mouseleave');
                                        controlsOn();
                                    });
                                }
                            },
                            error: function error() {
                                console.log('MEJS media error.');
                            }
                        };
                        if (typeof _wpmejsSettings !== 'undefined') settings.pluginPath = _wpmejsSettings.pluginPath;
                        $(this).mediaelementplayer(settings);
                    });
                }
            };

            var init = function init() {
                OkVedio();
                bigVedio();
                mediaElement();
            };
            return {
                init: init
            };
        }();

        PIXELDIMA.UI = function () {
            var dima_spacer_shortcode = function dima_spacer_shortcode() {
                $('.dima-spacer-module').each(function () {

                    var $self = $(this),
                        _W = PIXELDIMA.windowWidth,
                        units = $self.data('units'),
                        screen_all_spacer_size = $self.data('all_size'),
                        screen_xld_resolution = $self.data('xld_resolution'),
                        screen_xld_spacer_size = $self.data('xld_size') !== '' ? $self.data('xld_size') : screen_all_spacer_size,
                        screen_ld_resolution = $self.data('ld_resolution'),
                        screen_ld_spacer_size = $self.data('ld_size') !== '' ? $self.data('ld_size') : screen_all_spacer_size,
                        screen_md_resolution = $self.data('md_resolution'),
                        screen_md_spacer_size = $self.data('md_size') !== '' ? $self.data('md_size') : screen_all_spacer_size,
                        screen_sd_resolution = $self.data('sd_resolution'),
                        screen_sd_spacer_size = $self.data('sd_size') !== '' ? $self.data('sd_size') : screen_all_spacer_size,
                        screen_xsd_resolution = $self.data('xsd_resolution'),
                        screen_xsd_spacer_size = $self.data('xsd_size') !== '' ? $self.data('xsd_size') : screen_all_spacer_size;

                    if (units === '%' && screen_md_spacer_size !== 0 && screen_sd_spacer_size !== 0 && screen_xsd_spacer_size !== 0) {
                        screen_md_spacer_size = screen_all_spacer_size * screen_md_spacer_size / 100;
                        screen_sd_spacer_size = screen_all_spacer_size * screen_sd_spacer_size / 100;
                        screen_xsd_spacer_size = screen_all_spacer_size * screen_xsd_spacer_size / 100;
                        screen_ld_spacer_size = screen_all_spacer_size * screen_ld_spacer_size / 100;
                        screen_xld_spacer_size = screen_all_spacer_size * screen_xld_spacer_size / 100;
                    }

                    $self.css('height', screen_all_spacer_size);

                    if (_W >= screen_ld_resolution && _W <= screen_xld_resolution) {
                        $self.css('height', screen_xld_spacer_size);
                    } else if (_W >= screen_md_resolution && _W < screen_ld_resolution) {
                        $self.css('height', screen_ld_spacer_size);
                    } else if (_W >= screen_sd_resolution && _W < screen_md_resolution) {
                        $self.css('height', screen_md_spacer_size);
                    } else if (_W >= screen_xsd_resolution && _W < screen_sd_resolution) {
                        $self.css('height', screen_sd_spacer_size);
                    } else if (screen_xsd_resolution >= _W) {
                        $self.css('height', screen_xsd_spacer_size);
                    }
                });
            };

            var dima_height_responsive = function dima_height_responsive() {
                $('.dima-height-responsive').each(function () {
                    var $self = $(this),
                        _W = PIXELDIMA.windowWidth,
                        screen_all_spacer_size = 700,
                        screen_xld_resolution = $self.data('xld_resolution'),
                        screen_xld_spacer_size = $self.data('xld_size') !== '' ? $self.data('xld_size') : screen_all_spacer_size,
                        screen_ld_resolution = $self.data('ld_resolution'),
                        screen_ld_spacer_size = $self.data('ld_size') !== '' ? $self.data('ld_size') : screen_all_spacer_size,
                        screen_md_resolution = $self.data('md_resolution'),
                        screen_md_spacer_size = $self.data('md_size') !== '' ? $self.data('md_size') : screen_all_spacer_size,
                        screen_sd_resolution = $self.data('sd_resolution'),
                        screen_sd_spacer_size = $self.data('sd_size') !== '' ? $self.data('sd_size') : screen_all_spacer_size,
                        screen_xsd_resolution = $self.data('xsd_resolution'),
                        screen_xsd_spacer_size = $self.data('xsd_size') !== '' ? $self.data('xsd_size') : screen_all_spacer_size;

                    if ($self.find('.dima-block-slide')) {
                        $self = $self.find('.slick-slide');
                    }
                    $self.css('height', screen_all_spacer_size);
                    if (_W >= screen_ld_resolution && _W <= screen_xld_resolution) {
                        $self.css('height', screen_xld_spacer_size);
                    } else if (_W >= screen_md_resolution && _W < screen_ld_resolution) {
                        $self.css('height', screen_ld_spacer_size);
                    } else if (_W >= screen_sd_resolution && _W < screen_md_resolution) {
                        $self.css('height', screen_md_spacer_size);
                    } else if (_W >= screen_xsd_resolution && _W < screen_sd_resolution) {
                        $self.css('height', screen_sd_spacer_size);
                    } else if (screen_xsd_resolution >= _W) {
                        $self.css('height', screen_xsd_spacer_size);
                    }
                });
            };

            var countUp = function countUp() {
                $(".countUp").each(function () {
                    //noinspection JSUnresolvedFunction
                    var elm = $(this);
                    new Waypoint({
                        element: elm,
                        handler: function handler(direction) {
                            elm.find(".number-count").countTo({
                                formatter: function formatter(e) {
                                    return e = e.toFixed(), e = e.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                }
                            });
                            this.destroy();
                        },
                        offset: "98%"
                    });
                });
            };

            var progress = function progress() {
                //progress bar color
                $('.progress').append(function () {
                    var elm = $(this);
                    var color = elm.attr("data-color-val");
                    elm.find('.progress-bar').css('background-color', color);
                    elm.find('.progress-bar .percent').css('background-color', color);
                    elm.find('span').css('border-top-color', color);
                });

                $(".progress .progress-bar").each(function () {
                    var $self = $(this);
                    var perc = $self.attr("aria-valuenow");
                    var current_percent = 0;

                    new Waypoint({
                        element: $self,
                        handler: function handler(direction) {
                            var progress = setInterval(function () {
                                if (current_percent >= perc) {
                                    clearInterval(progress);
                                } else {
                                    current_percent += 5;
                                    $self.velocity({
                                        width: current_percent + '%'
                                    }, {
                                        duration: 0,
                                        easing: "linear"
                                    });
                                }
                            }, 1);
                            this.destroy();
                        },
                        offset: "98%"
                    });
                });

                //noinspection JSUnresolvedFunction
                $(".dial").each(function () {
                    var elm = $(this);
                    var width = elm.attr("data-width");
                    var perc = elm.attr("value");
                    //circular progress bar color
                    new Waypoint({
                        element: elm,
                        handler: function handler(direction) {
                            elm.knob({
                                'value': 0,
                                'min': 0,
                                'max': 100,
                                "skin": "tron",
                                "readOnly": true,
                                "displayPrevious": true,
                                "thickness": 0.03,
                                "displayInput": false,
                                "bgColor": "rgba(255,255,255,0)",
                                "linecap": ""
                            });

                            $({ value: 0 }).animate({
                                value: perc
                            }, {
                                duration: 1000,
                                easing: 'swing',
                                progress: function progress() {
                                    elm.val(Math.ceil(this.value)).trigger('change');
                                }
                            });
                            elm.append(function () {
                                elm.parents('.circular-bar').find('.circular-bar-content label').text(perc + '%');
                            });
                            this.destroy();
                        },
                        offset: "98%"
                    });
                });
            };

            var tabs = function tabs() {
                var $tab_id = $("#dima-tab-nav");

                $('[data-toggle="tooltip"]').tooltip({
                    animation: true,
                    html: !1,
                    delay: {
                        show: 0,
                        hide: 0
                    }
                });

                $('[data-toggle="popover"]').popover();

                $tab_id.find("a:first").tab("show"); // Select first tab
                $tab_id.find("a").click(function (e) {
                    e.preventDefault();
                    $(this).tab("show");
                });
            };

            var notification = function notification() {
                $(".dima-alert button.close").click(function () {
                    $(this).parent().fadeOut(200, "easeOutExpo");
                });
            };
            var initImageLayersModule = function initImageLayersModule() {

                $('.dima-image-layers-wrap').each(function () {
                    var $container = $(this),
                        layerWidth = 0,
                        initImageLayers = function initImageLayers() {

                        if (typeof $.fn.equalHeights !== 'undefined' && !$container.hasClass("dima-block-lazy")) {
                            $container.find('.dima-layer-container').equalHeights();
                        }
                        if (typeof $.fn.waypoint !== 'undefined') {
                            $container.waypoint(function () {
                                $container.addClass('layer-animate');
                            }, { triggerOnce: true, offset: '70%' });
                        }
                    },
                        imageSizing = function imageSizing() {
                        $container.find('.dima-layer-item').each(function () {
                            var $el = $(this),
                                elWidth = $el.width();
                            if ($el.width() > layerWidth) {
                                layerWidth = $el.width();
                            }
                        });

                        $container.css({ 'width': layerWidth });
                    };

                    PIXELDIMA.win.on('load', function () {
                        if (typeof $.fn.imagesLoaded !== 'undefined') {
                            $container.find('.dima-layer-item').imagesLoaded().done(function () {
                                imageSizing();
                                initImageLayers();
                            });
                        }
                    });

                    PIXELDIMA.win.on('resize', initImageLayers);

                    $('body').on('post-load', function () {
                        if (typeof $.fn.imagesLoaded !== 'undefined') {
                            $container.find('.dima-layer-item').imagesLoaded().done(function () {
                                imageSizing();
                            });
                        }
                        initImageLayers();
                    });
                });
            };

            var datepicker = function datepicker() {
                var elm = $("[data-datepicker]");
                elm.each(function () {
                    $(this).datepicker({
                        beforeShow: function beforeShow(textbox, instance) {
                            $('#ui-datepicker-div').css('min-width', $(this).outerWidth());
                        },

                        showOtherMonths: true,
                        selectOtherMonths: true,
                        dateFormat: 'dd/mm/yy'
                    }).datepicker('widget').wrap('<div class="ll-skin-melon"/>');
                });
            };

            var element_bg = function element_bg() {
                var elm = $("[data-element-bg]");
                elm.each(function () {
                    var b = $(this).attr("data-element-bg");
                    $(this).css({
                        "background-image": 'url(' + b + ')',
                        "background-position": "100% 100%",
                        "background-repeat": "no-repeat"
                    });
                });
            };

            var dima_field = function dima_field() {
                $("form.matrial-form").find('input[type="search"] , input[type="text"] , input[type="email"] , textarea').after("<i class=\"bar\"></i>");
            };

            var dima_svg = function dima_svg() {
                $(".svg_icon").each(function () {
                    var elm = $(this).find('svg');
                    var svg_size = $(this).data('svg-size');
                    elm.css({ 'width': svg_size });
                });

                $(".svg_animated").each(function () {
                    var elm = $(this).find('svg');
                    elm.find('path').css("fill", "none");
                    elm.velocity({ 'opacity': 0 }, 0);
                    var duration = 1500;
                    setTimeout(function () {
                        var svgelm = elm.find('polyline, path, circle, rect, line');
                        new Waypoint({
                            element: elm,
                            handler: function handler(direction) {
                                elm.velocity({ 'opacity': 1 }, 0);
                                svgelm.velocity({ 'stroke-dashoffset': 100 }, 0).velocity({ 'stroke-dashoffset': 0 }, { duration: duration, delay: 10 });
                                this.destroy();
                            },
                            offset: '98%'
                        });
                    }, 300);
                });
            };

            /**
             * Sticky Sidebars
             */
            var dima_sticky_sidebar = function dima_sticky_sidebar() {
                var $stickySidebar = $('.sidebar-is-sticky');
                if ($stickySidebar.length) {
                    var stickySidebarBottom = 35,
                        stickySidebarTop = 0;
                    stickySidebarTop = pxdm_js.sticky_behavior !== 'default' ? 8 : stickySidebarTop;
                    stickySidebarTop = PIXELDIMA.body.hasClass('admin-bar') ? stickySidebarTop + 32 : stickySidebarTop;
                    stickySidebarTop = PIXELDIMA.body.hasClass('framed') ? stickySidebarTop + 20 : stickySidebarTop;

                    $stickySidebar.theiaStickySidebar({
                        'additionalMarginTop': stickySidebarTop,
                        'additionalMarginBottom': stickySidebarBottom,
                        'minWidth': 990
                    });
                }
            };

            Waves.init();
            Waves.attach('.dima-waves', ['waves-button', 'waves-light']);
            Waves.attach('.acc-with-border .dima-accordion-header', ['waves-button']);

            var init = function init() {
                dima_sticky_sidebar();
                dima_svg();
                dima_spacer_shortcode();
                PIXELDIMA.win.on('load resize', dima_spacer_shortcode);
                dima_height_responsive();
                PIXELDIMA.win.on('load resize', dima_height_responsive);
                countUp();
                progress();
                tabs();
                datepicker();
                initImageLayersModule();
                notification();
                dima_field();
                element_bg();
            };

            return {
                init: init
            };
        }();

        //ISOTOP_PORTFOLIO
        PIXELDIMA.ISOTOP_PORTFOLIO = function () {
            var filterIsotop = function filterIsotop(selector, itemSelector) {
                $(selector).each(function () {
                    var $container = $(this);
                    var $elm_filter = $('.filters-box');
                    var filter = $elm_filter.find('li.current a').data("filter");
                    if (filter === '') {
                        filter = '*';
                    }
                    var params = {
                        filter: filter,
                        itemSelector: itemSelector,
                        layoutMode: 'packery',
                        transitionDuration: 500,
                        sortBy: 'original-order',
                        resizable: true,
                        percentPosition: true,
                        masonry: {
                            columnWidth: itemSelector
                        }
                    };

                    $container.imagesLoaded(function () {
                        $container.isotope(params);
                    });

                    $('.filters a,.filters li').click(function () {
                        var li_p = $(this).parent();
                        $('.filters .current').removeClass('current');
                        $(li_p).addClass('current');

                        var selector = $(this).attr('data-filter');
                        $container.isotope({
                            filter: selector
                        });
                        Waypoint.refreshAll();

                        /*Show items that was hiding and waiting for the class .animation-done */
                        /*And adding animation with opacity*/
                        var isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);

                        if (!isMobile) {
                            var $isoItem = $(li_p).closest('.dima-isotope-wrapper').find('.isotope-item');
                            $isoItem.each(function () {
                                $(this).addClass('animation-done').velocity({ 'opacity': 1 }, 0);
                            });
                        }

                        return false;
                    });
                });
            };

            var init = function init() {
                var $container = $('.dima-isotope-container');
                filterIsotop($container, '.isotope-item');
                var $container_ = $('#members-list');
                filterIsotop($container_, 'li.even,li.odd');
            };

            return {
                init: init
            };
        }();

        PIXELDIMA.FOOTER = function () {
            /* Featured Area */
            var featuredArea = function featuredArea() {
                var $fa = $(".featured_area");
                var $footer_top = $(".top-footer");
                var padding_top = -1 * parseFloat($footer_top.css("padding-top"));
                var footer_height = parseFloat($footer_top.css("height"));
                $fa.removeClass('featured_area_on_sd');

                if (PIXELDIMA.windowWidth >= 989) {
                    $fa.each(function () {
                        $(this).css({
                            'height': footer_height,
                            'top': padding_top,
                            'padding-top': -1 * padding_top,
                            'position': 'absolute'
                        });
                    });
                    $footer_top.css('height', footer_height);
                } else {
                    $fa.each(function () {
                        $(this).css({
                            'height': 'auto',
                            'top': 0,
                            'padding': 20,
                            'position': 'relative'
                        });
                    });
                    $footer_top.css('height', 'auto');
                    $fa.addClass('featured_area_on_sd');
                }
            };

            if ($('.footer-container').hasClass('fixed-footer-container')) {
                var $fixed_footer = $('.fixed-footer-container');
                var fixed_footer_height = $fixed_footer.outerHeight();
                $('body').css('padding-bottom', fixed_footer_height);
            }
            /* !Featured Area */
            var init = function init() {
                featuredArea();
            };
            return {
                init: init
            };
        }();

        PIXELDIMA.ANIMATION = function () {

            var hideBeforeAnimation = function hideBeforeAnimation() {
                var elm = $("[data-animate]");
                elm.each(function () {
                    var $self = $(this),
                        $item = void 0;

                    if ($self.data('dima-animate-item')) {
                        $item = $self.find($self.data('dima-animate-item'));
                        $item.each(function () {
                            if (!$(this).hasClass('animation-done')) {
                                $(this).css('opacity', '0');
                            }
                        });
                    } else {
                        if (!$self.hasClass('animation-done')) {
                            $self.css('opacity', '0');
                        }
                    }
                });
            };

            var animations = function animations() {
                var elm = $("[data-animate]");

                elm.each(function () {
                    var $curr = $(this);
                    var $animation = $curr.attr("data-animate");
                    var offset = $curr.attr("data-offset") || '98%';
                    var dataDelay = parseInt($curr.attr("data-delay")) || 0;
                    var duration = parseInt($curr.attr("data-duration")) || 200;
                    var a = 0;

                    if ($curr.data('dima-animate-item')) {
                        var $item = $curr.find($curr.data('dima-animate-item'));
                        $item.each(function (i) {
                            var $self = $(this);

                            new Waypoint({
                                element: $self,
                                handler: function handler() {
                                    if (!$self.hasClass('animation-done')) {
                                        $self.addClass('animation-done').velocity($animation, {
                                            duration: duration,
                                            delay: i * dataDelay,
                                            display: 'undefined'
                                        });
                                    }
                                    /* Call Lazy Image*/
                                    var images = $self.find("img.js-lazy-image,.js-lazy-image-css");
                                    if (pxdm_js.is_lazy_image && images.length > 0) {
                                        var lazy_load = images.lazyload();
                                        lazy_load.loadImages();
                                    }

                                    this.destroy();
                                },
                                offset: offset
                            });
                        }); //each
                    } else {
                        if (a = dataDelay ? Number(dataDelay) + 10 : 300, !$curr.hasClass("animation-done")) {
                            new Waypoint({
                                element: $curr,
                                handler: function handler() {
                                    if (!$curr.hasClass('animation-done')) {
                                        $curr.addClass('animation-done').velocity($animation, {
                                            duration: duration,
                                            delay: a,
                                            display: 'undefined'
                                        });
                                    }
                                    /* Call Lazy Image*/
                                    var images = $curr.find("img.js-lazy-image,.js-lazy-image-css");
                                    if (pxdm_js.is_lazy_image && images.length > 0) {
                                        var lazy_load = images.lazyload();
                                        lazy_load.loadImages();
                                    }

                                    this.destroy();
                                },
                                offset: offset
                            });
                        }
                    }
                });
            };

            var notAnimations = function notAnimations() {
                var elm = $("[data-animate]");
                elm.each(function () {
                    var elm = $(this);
                    var dataDelay = elm.attr("data-delay") || 0;
                    var offsetVal = elm.attr("data-offset") || "98%";
                    var trgger = elm.attr("data-trgger") || "false";
                    var a = 0;
                    if (a = dataDelay ? Number(dataDelay) + 300 : 300, !elm.hasClass("animated")) {
                        $(this).addClass('opacity-zero');

                        new Waypoint({
                            element: elm,
                            handler: function handler(direction) {
                                var $this = $(this);
                                setTimeout(function () {
                                    $this.animate({
                                        opacity: 1
                                    }, {
                                        step: function step(now, fx) {
                                            var X = 100 * now;
                                            $(fx.elem).css("filter", 'alpha(opacity=' + X + ')');
                                        }
                                    });
                                }, a);
                                this.destroy();
                            },
                            offset: offsetVal

                        });
                    }
                });
            };

            var init = function init() {
                if (!Modernizr.mq('only all and (max-width: 480px)')) {
                    if (PIXELDIMA.isTransitions) {
                        if (!Modernizr.touch && PIXELDIMA.win.width() > 800) {
                            hideBeforeAnimation();
                            animations();
                            $('body').on('post-load', function () {
                                hideBeforeAnimation();
                                animations();
                            });
                        }
                    } else {
                        notAnimations();
                    }
                }
            };

            return {
                init: init
            };
        }();

        PIXELDIMA.EVENT = function () {
            var event = function event() {
                var $dima_nav = $('.dima-navbar-wrap.desk-nav .dima-navbar');
                //Fix The Navbar
                if (PIXELDIMA.windowWidth > 960) {
                    if ($dima_nav.hasClass('fix-one')) {
                        $(".fix-one").fix_navbar();
                    }
                    //this will apply for offset-by
                    if ($dima_nav.hasClass('fix-two')) {
                        $(".fix-two").show_navbar();
                    }
                }
                //Add class active Based on URL http://css-tricks.com/snippets/jquery/add-active-navigation-class-based-on-url/
                $('.sidebar li a[href^="' + location.pathname.split("/")[2] + '"]').parent().addClass('active');

                $("[data-height]").each(function () {
                    var h = $(this).attr("data-height");
                    $(this).css("height", h);
                });
                $("[data-bg]").each(function () {
                    var bg = $(this).attr("data-bg");
                    $(this).css("background-image", 'url(' + bg + ')');
                });
                $("[data-bg-color]").each(function () {
                    var bg = $(this).attr("data-bg-color");
                    $(this).css("background-color", bg);
                });
            };

            /**
             * Popup Module
             */
            function dimaBtnOpen(windowToOpen) {
                jQuery(windowToOpen).show();
                setTimeout(function () {
                    $('body').addClass('dima-popup-is-opend');
                    var $x = $('.dima-popup-container');
                    $x.velocity("transition.swoopIn", 700);
                }, 10);

                PIXELDIMA.html.css({ 'marginRight': PIXELDIMA.getScrollBarWidth(), 'overflow': 'hidden' });
            }

            /**
             * Ad Blocker
             */
            var ad_block = function ad_block() {
                if (pxdm_js.ad_blocker_detector && typeof $dimaE3 === 'undefined') {
                    PIXELDIMA.adBlock = true;
                    PIXELDIMA.html.css({ 'marginRight': PIXELDIMA.getScrollBarWidth(), 'overflow': 'hidden' });
                    setTimeout(function () {
                        $('body').addClass('dima-popup-is-opend');
                    }, 10);
                    dimaBtnOpen('#dima-popup-adblock');
                }
            };

            var init = function init() {
                event();
                ad_block();
            };

            return {
                init: init
            };
        }();

        // handle the layout reinitialization on window resize
        PIXELDIMA.DOCONRESIZE = function () {

            var nav = function nav() {
                var $dima_nav = $('dima-navbar-wrap.desk-nav .dima-navbar');
                PIXELDIMA.windowWidth = $(window).width();
                if ($dima_nav.hasClass('fix-one')) {
                    $(".fix-one").fix_navbar();
                }
                if ($dima_nav.hasClass('fix-two')) {
                    $(".fix-two").show_navbar();
                }
            };

            var init = function init() {
                nav();
                PIXELDIMA.FOOTER.init();
            };
            return {
                init: init
            };
        }();

        // runs callback functions
        PIXELDIMA.PIXELDIMA_READY = function () {

            var init = function init() {
                //Please don't change the order

                PIXELDIMA.MEDIA.init();
                PIXELDIMA.SCROLL.init();
                PIXELDIMA.MENU.init();
                PIXELDIMA.LIGHTBOX.init();
                PIXELDIMA.SHOP.init();
                PIXELDIMA.ISOTOP_PORTFOLIO.init();
                PIXELDIMA.EVENT.init();
                PIXELDIMA.UI.init();
                PIXELDIMA.FOOTER.init();
                PIXELDIMA.ANIMATION.init();
                $.scrollToTop();

                $(window).resize(function () {
                    PIXELDIMA.DOCONRESIZE.init();
                });
            };

            return {
                init: init
            };
        }();

        /**
         * Call Our Setups Functions
         */
        PIXELDIMA.PIXELDIMA_READY.init();
    });
    /* End of document ready*/
})(jQuery);