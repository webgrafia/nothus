/*! DETECT IE */
function detectIE() {
    var msie = navigator.userAgent.indexOf('MSIE ');
    if (msie > 0) {
        return parseInt(navigator.userAgent.substring(msie + 5, navigator.userAgent.indexOf('.', msie)), 10);
        /*IE 10 or older*/
    }
    var trident = navigator.userAgent.indexOf('Trident/');
    if (trident > 0) {
        var rv = navigator.userAgent.indexOf('rv:');
        return parseInt(navigator.userAgent.substring(rv + 3, navigator.userAgent.indexOf('.', rv)), 10);
        /* IE 11 */
    }
    var edge = navigator.userAgent.indexOf('Edge/');
    if (edge > 0) {
        return parseInt(navigator.userAgent.substring(edge + 5, navigator.userAgent.indexOf('.', edge)), 10);
        /* Edge (IE 12+) */
    }
    return false;
}