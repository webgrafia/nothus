<?php
/**
 * @package Dima Framework
 * @subpackage root views
 * @version   1.0.0
 * @since     1.0.0
 * @author    PixelDima <info@pixeldima.com>
 */
?>

<?php if ( is_single() ) {
	$is_shear   = '1';
	$float      = "end";
	$grid_class = "ok-md-4 ok-sd-6 ok-xsd-12";
	$terms      = get_the_terms( get_the_ID(), 'portfolio-tag' );
	$tag_class  = ( sizeof( $terms ) == 1 ) ? "no-tags" : "";
	?>
    <div class="container">
        <div class="post-footer ok-row ok-no-margin <?php echo esc_attr( $tag_class ) ?>">
			<?php
			if ( sizeof( $terms ) > 1 ) { ?>
                <div class="ok-md-8 ok-sd-6 ok-xsd-12">
                    <div class="tags">
						<?php echo dima_get_svg_icon( "ic_label" ) ?>
                        <span class="tags-title">
						<?php esc_html_e( 'Tags:', 'noor' ) ?>
					</span>
						<?php
						$output = '';
						foreach ( $terms as $term ) {
							$output .= '<a href="' . get_term_link( $term ) . '" data-filter=".' . esc_attr( $term->term_id ) . '" rel="tag">' . esc_attr( $term->name ) . '</a>';
						}
						echo( $output );
						echo get_the_tag_list();
						?>
                    </div>
                </div>
				<?php
			} else {
				$float      = "start";
				$grid_class = "ok-md-12 ok-sd-6 ok-xsd-12";
			}
			if ( $is_shear ) {
				?>
                <div class="<?php echo esc_attr( $grid_class ) ?>">
					<?php
					if ( function_exists( 'ADDTOANY_SHARE_SAVE_KIT' ) ) {
						?>
                        <div class="text-end post-share">
							<?php
							ADDTOANY_SHARE_SAVE_KIT( array(
								'no_universal_button' => false,
								'no_small_icons'      => false
							) );
							?>
                        </div>
						<?php
					} else {
						if ( DIMA_NOUR_ASSISTANT_IS_ACTIVE ) {
							echo do_shortcode( '[share title="Share this Post" float="' . esc_attr( $float ) . '" circle="true" size="" facebook="true" twitter="true" google_plus="true" linkedin="true" pinterest="fales" reddit="fales" email="fales"]' );
						}
					} ?>
                </div>
			<?php }
			?>
        </div>
    </div>
<?php } ?>
