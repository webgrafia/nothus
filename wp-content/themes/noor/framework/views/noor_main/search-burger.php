<?php
$dima_copyright    = dima_helper::dima_get_option( 'dima_footer_content_text' );
$burger_menu_float = dima_helper::dima_get_inherit_option( '_dima_meta_burger_position', 'dima_header_burger_menu_float' );
$burger_menu_style = dima_helper::dima_get_inherit_option( '_dima_meta_burger_style', 'dima_header_burger_menu_style' );
$is_search_active  = dima_helper::dima_am_i_true( dima_helper::dima_get_inherit_option( '_dima_meta_search_display', 'dima_header_search_enable' ) );
$burger_menu_bg    = dima_helper::dima_get_inherit_option( '_dima_meta_burger_bg_img', 'dima_header_burger_bg_img' );
$search_menu_bg    = dima_helper::dima_get_inherit_option( '_dima_meta_search_bg_img', 'dima_header_search_bg_img' );

if ( $burger_menu_bg != '' ) {
	$burger_menu_bg = 'data-bg="' . esc_attr( $burger_menu_bg ) . '"';
}
if ( $search_menu_bg != '' ) {
	$search_menu_bg = 'data-bg="' . esc_attr( $search_menu_bg ) . '"';
}
$allowed_tags = array(
	'strong' => array(),
	'br'     => array(),
	'em'     => array(),
	'p'      => array( 'a' => true ),
	'a'      => array(
		'href'   => true,
		'target' => true,
		'title'  => true,
	)
);
?>

<?php
if ( $is_search_active ) { ?>
    <!-- > Search -->
    <div class="full-screen-menu background-cover search-box dark-bg hide" <?php echo esc_attr( $search_menu_bg ); ?>>

        <form method="get" id="searchform-3" class="form-search center-text"
              action="<?php echo esc_url( home_url( '/' ) ); ?>">
            <div class="container">
                <input type="text" name="s">
                <span class="placeholder"><?php echo esc_html__( 'Search', 'noor' ); ?><span> ...</span></span>
                <span class="search-svg-icon"><?php echo dima_get_svg_icon( "ic_search" ); ?></span>
            </div>
        </form>
    </div>
    <!-- ! Search -->
<?php } ?>

    <div class="full-screen-menu info-box background-cover dark-bg hide" <?php echo esc_attr( $burger_menu_bg ); ?>>

        <div class="social-copyright">
            <div class="menu-social-media fill-icon social-media social-small circle-social text-center">
                <ul class="inline clearfix">
					<?php
					dima_helper::dima_get_global_social();
					?>
                </ul>
            </div>
			<?php if ( dima_helper::dima_am_i_true( dima_helper::dima_get_option( 'dima_menu_copyright_display' ) ) ) : ?>
                <div class="copyright">
					<?php echo
					wp_kses( $dima_copyright, $allowed_tags );
					?>
                </div>
			<?php endif; ?>
        </div>

    </div>


<?php
if ( $burger_menu_style == 'full' ) { ?>
    <!-- > Full burger menu  -->
    <div class="full-screen-menu menu-box background-cover dark-bg hide" <?php echo esc_attr( $burger_menu_bg ); ?>>
        <div class="burger-full text-center">

			<?php
			dima_output_burger_navigation();
			?>
        </div>

        <div class="social-copyright">
            <div class="menu-social-media fill-icon social-media social-small circle-social text-center">
                <ul class="inline clearfix">
					<?php
					dima_helper::dima_get_global_social();
					?>
                </ul>
            </div>
	        <?php if ( dima_helper::dima_am_i_true( dima_helper::dima_get_option( 'dima_menu_copyright_display' ) ) ) : ?>
                <div class="copyright">
					<?php echo
					wp_kses( $dima_copyright, $allowed_tags );
					?>
                </div>
			<?php endif; ?>
        </div>

    </div>
    <!-- ! Full burger menu  -->
<?php } ?>

<?php if ( $burger_menu_style == 'end' ) { ?>
    <!-- > Side Menu -->
    <div class="burger-menu-side burger-side-pos-<?php echo( $burger_menu_float ) ?> dark-bg burger-menu-<?php echo esc_attr( $burger_menu_style ) ?>">
        <div class="dima-side-area-mask side-area-controller"></div>
        <div class="burger-menu-content" <?php echo esc_attr( $burger_menu_bg ); ?>>
            <div id="sly-frame-menu">
                <div class="menu-slidee">
                    <div class="burger-full text-start">
						<?php
						dima_output_burger_navigation();
						?>
                    </div>
					<?php dynamic_sidebar( 'burger-area' ); ?>
                </div>
            </div>

            <div class="scrollbar">
                <div class="handle"></div>
            </div>

        </div>
    </div>
    <!-- ! Side Menu -->
<?php } ?>