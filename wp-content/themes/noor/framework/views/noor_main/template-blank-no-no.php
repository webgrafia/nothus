<?php
/**
 * @package Dima Framework
 * @subpackage root views
 * @version   1.0.0
 * @since     1.0.0
 * @author    PixelDima <info@pixeldima.com>
 */
?>


<?php
dima_helper::dima_get_view( 'dima_global', 'header' );
?>

<?php dima_helper::dima_get_view( 'dima_global', 'slider-below' ); ?>

<div class="dima-container full" role="main">
	<?php while ( have_posts() ) : the_post(); ?>

		<?php dima_helper::dima_get_view( 'dima_global', 'content', 'the-content' ); ?>

	<?php endwhile; ?>

</div>

<?php
dima_helper::dima_get_view( 'dima_global', 'footer', 'scroll-top' );

wp_footer();
?>

