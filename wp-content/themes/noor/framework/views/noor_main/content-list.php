<?php
/**
 * @package Dima Framework
 * @subpackage root views-noor
 * @version   1.0.0
 * @since     1.0.0
 * @author    PixelDima <info@pixeldima.com>
 */

if ( is_singular() && ! is_page() ) {
	$args = dima_helper::get_featured_args();
} else {
	$args = dima_helper::get_featured_args( $this );
}
$post_format = ( get_post_format() != '' ) ? get_post_format() : 'standard';
?>
<li>
    <div class="tab-content-elements <?php echo esc_url( $args['post_class'] ); ?>">
        <div class="posts-list-content text-start">
            <h6>
                <a href="<?php the_permalink(); ?>"
                   title="<?php echo esc_attr( sprintf( esc_html__( 'Permalink to: "%s"', 'noor' ), the_title_attribute( 'echo=0' ) ) ); ?>">
					<?php echo get_the_title(); ?>
                </a>
            </h6>
            <span class="post-date"><?php echo get_the_date(); ?></span>
        </div>

    </div>
</li>

