<?php
/**
 * Views: 404.php
 * The template for displaying 404 pages (not found)
 *
 * @package Dima Framework
 * @subpackage root views
 * @version   1.0.0
 * @since     1.0.0
 * @author    PixelDima <info@pixeldima.com>
 */
?>


<?php get_header(); ?>

    <div class="container">
        <div class="dima-spacer-module" data-units="px" data-all_size="150" data-xld_resolution="1600"
             data-xld_size="150" data-ld_resolution="1400" data-ld_size="90" data-md_resolution="1170" data-md_size="60"
             data-sd_resolution="969" data-sd_size="40" data-xsd_resolution="480" data-xsd_size="30"
        ></div>
        <div class=" dima-container full not-found text-center" role="main">

			<?php dima_helper::dima_get_view( 'dima_global', 'content-404' ); ?>

        </div>
        <div class="dima-spacer-module" data-units="px" data-all_size="150" data-xld_resolution="1600"
             data-xld_size="150" data-ld_resolution="1400" data-ld_size="90" data-md_resolution="1170" data-md_size="60"
             data-sd_resolution="969" data-sd_size="40" data-xsd_resolution="480" data-xsd_size="30"
        ></div>
    </div>

<?php get_footer(); ?>