<?php
/**
 * @package Dima Framework
 * @subpackage root views
 * @version   1.0.0
 * @since     1.0.0
 * @author    PixelDima <info@pixeldima.com>
 */
?>
<?php
if ( is_singular() && ! is_page() ) {
	$args = dima_helper::get_featured_args();
} else {
	$args = dima_helper::get_featured_args( $this );
}
?>

<article <?php post_class( $args['post_class'] ); ?> >
	<?php
	dima_helper::dima_get_view( 'noor_main', '_content', 'post-header' ); ?>
	<?php
	if ( ( $args["blog_type"] != 'masonry' && $args["blog_type"] != 'slide' && $args["blog_type"] != 'grid' ) || ( is_singular() && ! is_page() ) ) {
		dima_get_entry_meta( $args['meta'], $args["blog_type"] );
	}

	if ( has_post_thumbnail() ) {
		if ( $args['show_image'] ) {
			dima_featured_image( array(
				'post_type' => $args['blog_type'],
				'img_hover' => $args['img_hover'],
				'elm_hover' => $args['elm_hover'],
			) );
		}
		dima_featured_audio( array(
			'post_type' => $args['blog_type'],
		) );
	} else {
		dima_featured_audio( array(
			'post_type' => $args['blog_type'],
		) );
	}

	if ( ( $args["blog_type"] == 'masonry' || $args["blog_type"] == 'slide' || $args["blog_type"] == 'grid' ) && ( ! is_singular() || is_page() ) ) {
		dima_get_entry_meta( $args['meta'], $args["blog_type"] );
	}

	?>

    <div class="<?php dima_pots_content_class(); ?>">
		<?php dima_get_post_content( $args['is_full_post_content_blog'], $args['words'] ); ?>
		<?php dima_helper::dima_get_view( 'noor_main', '_content', 'post-footer' ); ?>
    </div>
</article>