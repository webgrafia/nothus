<?php
/**
 * @package Dima Framework
 * @subpackage root views
 * @version   1.0.0
 * @since     1.0.0
 * @author    PixelDima <info@pixeldima.com>
 */
?>
<header>
	<?php if ( is_single() ) : ?>
		<h5 class="entry-title single-post-title"><?php the_title(); ?></h5>
	<?php else : ?>
		<h2 class="entry-title">
			<a href="<?php the_permalink(); ?>"
			   title="<?php echo esc_attr( sprintf( esc_html__( 'Permalink to: "%s"', 'noor' ), the_title_attribute( 'echo=0' ) ) ); ?>">
				<?php echo get_the_title(); ?>
			</a>
		</h2>
	<?php endif; ?>
</header>