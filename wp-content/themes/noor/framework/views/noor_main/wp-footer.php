<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the .dima-main div and all content after.
 *
 * @package Dima Framework
 * @subpackage root views
 * @version   1.0.0
 * @since     1.0.0
 * @author    PixelDima <info@pixeldima.com>
 */
?>


</div><!-- .dima-main -->

<?php
$footer_is_parallax = dima_helper::dima_am_i_true( dima_helper::dima_get_inherit_option( '_dima_meta_footer_parallax', 'dima_footer_parallax' ) );
$footer_is_dark     = dima_helper::dima_am_i_true( dima_helper::dima_get_inherit_option( '_dima_meta_footer_is_dark', 'dima_footer_is_dark' ) );
$footer_bg_image    = dima_helper::dima_get_inherit_option( '_dima_meta_footer_bg_image', 'dima_footer_bg_image' );
$footer_is_big      = dima_helper::dima_am_i_true( dima_helper::dima_get_inherit_option( '_dima_meta_footer_big', 'dima_footer_big' ) );
$footer_go_to_top   = dima_helper::dima_am_i_true( dima_helper::dima_get_option( 'dima_footer_go_to_top' ) );
$footer_bottom      = dima_helper::dima_am_i_true( dima_helper::dima_get_option( 'dima_footer_bottom' ) );

( ! $footer_is_parallax ) ? $footer_class = "" : $footer_class = " fixed-footer-container";
if ( $footer_is_dark ) {
	$footer_class .= " dark-bg";
}
$footer_bg_image = ( $footer_bg_image != '' ) ? 'data-bg="' . esc_attr( $footer_bg_image ) . '"' : '';

do_action( 'dima_action_before_footer' )
?>

<footer class="footer-container<?php echo esc_attr( $footer_class ); ?>" <?php echo esc_attr( $footer_bg_image ) ?>
        itemscope="itemscope" itemtype="https://schema.org/WPFooter">
	<?php
	/*------------------------------*/
	# Call the big footer where we shows the widgets areas.
	/*------------------------------*/
	if ( $footer_is_big ) {
		dima_helper::dima_get_view( 'dima_global', 'footer', 'widget-areas' );
	}

	/*------------------------------*/
	# System Paths
	/*------------------------------*/
	if ( $footer_bottom ) {
		dima_helper::dima_get_view( 'dima_global', 'footer', 'copyright' );
	}

	/*------------------------------*/
	# Call go to top button
	# !☻: For small website make it of for best performance
	/*------------------------------*/
	if ( $footer_go_to_top ) {
		dima_helper::dima_get_view( 'dima_global', 'footer', 'scroll-top' );
	}
	?>

</footer>

<?php do_action( 'dima_action_after_footer' ) ?>

</div> <!-- #dima-wrapper & .all_content -->

<?php wp_footer(); ?>

</body>
</html>