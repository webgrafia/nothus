<?php
/**
 * @package Dima Framework
 * @subpackage root views
 * @version   1.0.0
 * @since     1.0.0
 * @author    PixelDima <info@pixeldima.com>
 */
if ( is_singular() && ! is_page() ) {
	$args = dima_helper::get_featured_args();
} else {
	$args = dima_helper::get_featured_args( $this );
}
$format = get_post_format() ? get_post_format() : 'standard';
?>
<article <?php post_class( $args['post_class'] ); ?> >
    <div class="box shadow-hover dima-minimal-post">
        <div class="post-icon link_overlay">
            <ul class="icons-media">
                <li>
                    <a class="format-<?php echo esc_attr( $format ) ?>" href="<?php echo esc_url( get_permalink() ) ?>"
                       title="<?php echo esc_attr( sprintf( esc_html__( 'Permalink to: "%s"', 'noor' ), the_title_attribute( 'echo=0' ) ) ) ?>">
						<?php echo dima_get_svg_format( $format ) ?>
                    </a>
                </li>
            </ul>
        </div>
		<?php
		dima_helper::dima_get_view( 'noor_main', '_content', 'post-minimal-header' );
		?>

        <div class="<?php dima_pots_content_class(); ?>">
			<?php
			dima_get_post_content( $args['is_full_post_content_blog'], $args['words'] ); ?>
			<?php
			dima_helper::dima_get_view( 'noor_main', '_content', 'post-footer' );
			?>
        </div>
		<?php dima_get_entry_meta( $args['meta'], $args["blog_type"] ); ?>
    </div>

</article>