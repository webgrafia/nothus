<?php
/**
 * The archive template is used when visitors author, or search. 
 *
 * @package Dima Framework
 * @subpackage root views
 * @version   1.0.0
 * @since     1.0.0
 * @author    PixelDima <info@pixeldima.com>
 */
?>



<?php if ( is_author() ) : ?>

	<?php dima_helper::dima_get_view( 'dima_global', 'about-the-author' ); ?>

    <div class="divider">
        <div class="dashed"></div>
    </div>

	<?php
	global $author;
	$userdata = get_userdata( $author );
	?>

    <h5>
		<?php echo esc_html__( 'Entries by', 'noor' ) ?>
        <a href=""><?php echo esc_attr( $userdata->display_name ) ?></a>
    </h5>

    <div class="double-clear"></div>

<?php elseif ( is_search() ) : ?>
    <p><?php echo esc_html__( 'Not so happy with results? Search for a new keyword', 'noor' ); ?></p>
    <div class="dima-clear"></div>

	<?php get_search_form(); ?>
    <div class="clear-section"></div>

	<?php
	global $wp_query;
	$result = $wp_query->found_posts . ' ' . esc_html__( 'search results for', 'noor' );
	?>

    <h5 class="dima-search-title"><?php echo esc_attr( $result ); ?>: <?php echo get_search_query() ?></h5>
    <div class="double-clear"></div>
    <div class="double-clear"></div>

<?php endif;