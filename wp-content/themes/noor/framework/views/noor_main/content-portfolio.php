<?php
/**
 * @package Dima Framework
 * @subpackage root views
 * @version   1.0.0
 * @since     1.0.0
 * @author    PixelDima <info@pixeldima.com>
 */
if ( is_singular() && ! is_page() ) {
	$args = dima_helper::get_featured_args_portfolio();
} else {
	$args = dima_helper::get_featured_args_portfolio( $this );
}
$show_title = true;
$scaling    = $the_content = $title_content = $info_content = '';
if ( $args['blog_type'] == 'grid' ) {
	$scaling = "portfolio_grid";
}
?>
<?php

if ( dima_helper::dima_is_single_portfolio() ) {
	$post_id      = get_the_ID();
	$text_content = apply_filters( 'the_excerpt', get_the_excerpt() );
	$details      = dima_helper::dima_am_i_true( dima_helper::dima_get_inherit_option( '_dima_portfolio_details', 'dima_projects_details' ) );
	$Client       = $Link = $skill = $Services = $Year = '';

	$meta             = dima_helper::dima_am_i_true( dima_helper::dima_get_inherit_option( '_dima_portfolio_shear_icon', 'dima_projects_portfolio_shear_icon_and_tag' ) );
	$details_layout   = dima_helper::dima_get_inherit_option( '_dima_portfolio_details_layout', 'dima_projects_details_layout' );
	$details_style    = dima_helper::dima_get_inherit_option( '_dima_portfolio_details_style', 'dima_projects_details_style' );
	$info_details_box = '';

	if ( $details_style == 'classic' ) {
		$args['post_class'] .= "container";
	} else {
		$details_layout = "modern";
	}

	?>
    <article <?php post_class( $args['post_class'] ); ?> >
		<?php
		if ( $show_title ) {
			$title_content .= apply_filters( 'dima_before_body_title', '' );
			$title_content .= ' <div class="post-title-wrapper" ><h4 class="post-title" > ' . get_the_title() . '</h4 ></div> ';
			$title_content .= apply_filters( 'dima_after_body_title', '' );
		}

		$info_content .= ' <div class="info-content" > ' . $title_content . $text_content . $info_content . '</div> ';

		if ( $details ) {

			$Client   = dima_helper::dima_get_meta( '_dima_portfolio_client' );
			$Services = dima_helper::dima_get_meta( '_dima_portfolio_services' );
			$Year     = dima_helper::dima_get_meta( '_dima_portfolio_year' );
			$Link     = dima_helper::dima_get_meta( '_dima_portfolio_url' );
			$skill    = dima_helper::dima_get_meta( '_dima_portfolio_skill' );

			if ( $details_style == 'classic' ) {
				if ( ! empty( $Client ) ) {
					$Client = '<span class="detail-container">
									<span class="detail-label">' . esc_html__( 'Client', 'noor' ) . '</span>
									<span class="detail-value">' . esc_attr( $Client ) . '</span>
						   </span>';
				}
				if ( ! empty( $Services ) ) {
					$Services = '<span class="detail-container">
									<span class="detail-label">' . esc_html__( 'Services', 'noor' ) . '</span>
									<span class="detail-value">' . esc_attr( $Services ) . '</span>
						   </span>';
				}
				if ( ! empty( $Year ) ) {
					$Year = '<span class="detail-container">
									<span class="detail-label">' . esc_html__( 'Year', 'noor' ) . '</span>
									<span class="detail-value">' . esc_attr( $Year ) . '</span>
						   </span>';
				}
				if ( ! empty( $Link ) ) {
					$Link = '<span class="detail-container">
									<span class="detail-label">' . esc_html__( 'Link', 'noor' ) . '</span>
									<span class="detail-value"><a href="' . esc_url( $Link ) . '" rel="nofollow" target="_blank" rel="noopener">' . esc_url( $Link ) . '</a></span>
						   </span>';
				}
				if ( ! empty( $skill ) ) {
					$skill = '<span class="detail-container">
									<span class="detail-label">' . esc_attr__( 'Skill', 'noor' ) . '</span>
									<span class="detail-value">' . esc_attr( $skill ) . '</span>
						   </span>';
				}

				$info_details_box = '<div class="box-with-shadow details-box" >
								<p>								
								' . ( $Client ) . '	
								' . ( $Services ) . '	
								' . ( $skill ) . '	
								' . ( $Year ) . '	
								' . ( $Link ) . '	
								</p>
						</div>';
			} else {
				if ( ! empty( $Year ) ) {
					$Year = '<span class="detail-container">
									<span class="detail-value">' . esc_attr( $Year ) . '</span>
						   </span>';
				}
				if ( ! empty( $Client ) ) {
					$Client = '<span class="detail-container">
									<span class="detail-value">' . esc_attr( 'Delivered to ', 'noor' ) . '<span class="header-color">' . esc_attr( $Client ) . '</span></span>
						   </span>';
				}
				if ( ! empty( $Link ) ) {
					$Link = '<span class="detail-container float-end">
									<span class="detail-value">' . dima_get_svg_icon( 'ic_link' ) . '<a class="header-link-color" href="' . esc_url( $Link ) . '" rel="nofollow" target="_blank" rel="noopener">' . esc_attr( $Link ) . '</a></span>
						   </span>';
				}
				if ( empty( $Year ) AND empty( $Client ) AND empty( $Link ) ) {
					$info_details_box = '';
				} else {
					$info_details_box = '<div class="container details-modern" >
								<p>								
								' . $Year . '	
								' . $Client . '	
								' . $Link . '	
								</p>
						</div>';
				}

				$details_layout = "modern";
			}
		}
		switch ( $details_layout ) {
			case 'modern':
				$the_content = $info_details_box;
				if ( ! empty( $the_content ) ) {
					echo '<div class="page-body clearfix" >
					' . dima_helper::dima_remove_wpautop( $the_content ) . '
				</div> ';
				}
				dima_helper::dima_get_view( 'dima_global', 'content', '' );
				if ( $meta ) {
					dima_helper::dima_get_view( 'noor_main', '_content', 'portfolio-footer' );
				}
				break;

			case 'top':
				$the_content = '<div class="ok-row">
					 <div class="ok-sd-12 ok-xsd-12 ok-md-8 opacity-zero show animated fadeInUp">
						' . $info_content . '
					</div>
					<div class="ok-sd-12 ok-xsd-12 ok-md-4 opacity-zero show animated fadeInUp">
			            ' . $info_details_box . '
					</div>
					</div>';

				echo '<div class="page-body clearfix" >
				<div class="container portfolio-wrapper" >
					<div class="portfolio-body" > 
					' . dima_helper::dima_remove_wpautop( $the_content ) . '
					</div>
				</div>
				</div> ';
				dima_helper::dima_get_view( 'dima_global', 'content', '' );
				if ( $meta ) {
					dima_helper::dima_get_view( 'noor_main', '_content', 'portfolio-footer' );
				}
				break;

			case 'right':
				$the_content = '<div class="details-right">
						' . $info_content . '
			            ' . $info_details_box . '
					</div>';

				echo '<div class="ok-row">
					 <div class="ok-sd-12 ok-xsd-12 ok-md-8 opacity-zero show animated fadeInUp">';

				dima_helper::dima_get_view( 'dima_global', 'content', '' );
				if ( $meta ) {
					dima_helper::dima_get_view( 'noor_main', '_content', 'portfolio-footer' );
				}

				echo '</div>
					<div class="ok-sd-12 ok-xsd-12 ok-md-4 opacity-zero show animated fadeInUp">
			           <div class="page-body clearfix" >
								<div class="container portfolio-wrapper" >
									<div class="portfolio-body" > 
									' . dima_helper::dima_remove_wpautop( $the_content ) . '
									</div>
								</div>
							</div>
					</div>
					</div>';
				break;

			case 'left':
				$the_content = '<div class="details-left">
						' . $info_content . '
			            ' . $info_details_box . '
					</div>';

				echo '<div class="ok-sd-12 ok-xsd-12 ok-md-4 opacity-zero show animated fadeInUp">
							<div class="page-body clearfix" >
								<div class="container portfolio-wrapper" >
									<div class="portfolio-body" >
									           ' . dima_helper::dima_remove_wpautop( $the_content ) . '
									</div>
								</div>
							</div>';
				echo '</div>
					<div class="ok-sd-12 ok-xsd-12 ok-md-8 opacity-zero show animated fadeInUp">';

				dima_helper::dima_get_view( 'dima_global', 'content', '' );
				if ( $meta ) {
					dima_helper::dima_get_view( 'noor_main', '_content', 'portfolio-footer' );
				}

				echo '</div>';
				break;

			case 'bottom':
				$the_content = '<div class="ok-row classic-on-bottom">
					 <div class="ok-sd-12 ok-xsd-12 ok-md-8 opacity-zero show animated fadeInUp">
						' . $info_content . '
					</div>
					<div class="ok-sd-12 ok-xsd-12 ok-md-4 opacity-zero show animated fadeInUp">
			            ' . $info_details_box . '
					</div>
					</div>';

				dima_helper::dima_get_view( 'dima_global', 'content', '' );

				echo '<div class="page-body clearfix" >
				<div class="container portfolio-wrapper" >
					<div class="portfolio-body" > 
					' . dima_helper::dima_remove_wpautop( $the_content ) . '
					</div>					
				</div>
				</div> ';

				if ( $meta ) {
					dima_helper::dima_get_view( 'noor_main', '_content', 'portfolio-footer' );
				}

				break;

			default:
				$the_content = '<div class="ok-row">
					 <div class="ok-sd-12 ok-xsd-12 ok-md-8 opacity-zero show animated fadeInUp">
						' . $info_content . '
					</div>
					<div class="ok-sd-12 ok-xsd-12 ok-md-4 opacity-zero show animated fadeInUp">
			            ' . $info_details_box . '
					</div>
					</div>';

				dima_helper::dima_get_view( 'dima_global', 'content', '' );
				echo '<div class="page-body clearfix" >
				<div class="container portfolio-wrapper" >
					<div class="portfolio-body" > 
					' . dima_helper::dima_remove_wpautop( $the_content ) . '
					</div>					
				</div>
				</div> ';
				if ( $meta ) {
					dima_helper::dima_get_view( 'noor_main', '_content', 'portfolio-footer' );
				}
				break;
		}

		?>
    </article>
	<?php
} else {
	?>
    <article <?php post_class( $args['post_class'] ); ?> >
		<?php
		$video = get_post_format();
		if ( $video == 'video' ) {
			dima_portfolio_featured_video( array(
				'post_type' => $args['blog_type'],
				'img_hover' => $args['img_hover'],
				'elm_hover' => $args['elm_hover'],
			) );
		} else {
			dima_featured_portfolio( array(
				'post_type' => $args['blog_type'],
				'img_hover' => $args['img_hover'],
				'elm_hover' => $args['elm_hover'],
			) );
		}
		?>
    </article>
	<?php
}
?>

