<?php
/**
 * @package Dima Framework
 * @subpackage root views
 * @version   1.0.0
 * @since     1.0.0
 * @author    PixelDima <info@pixeldima.com>
 */
?>

<?php if ( is_single() ) {
	$is_shear   = dima_helper::dima_am_i_true( dima_helper::dima_get_inherit_option( '_dima_meta_shear_icons_post', 'dima_shear_icons_post' ) );
	$float      = "end";
	$grid_class = "ok-md-4 ok-sd-6 ok-xsd-12";
	$tag_class  = ( has_tag() != true ) ? "no-tags" : "";
	?>
    <div class="post-footer ok-row ok-no-margin <?php echo esc_attr( $tag_class ) ?>">
		<?php if ( has_tag() ) { ?>
            <div class="ok-md-8 ok-sd-6 ok-xsd-12">
                <div class="tags">
					<?php echo dima_get_svg_icon( "ic_label" ) ?>
                    <span class="tags-title">
						<?php esc_html_e( 'Tags:', 'noor' ) ?>
					</span>
					<?php echo get_the_tag_list(); ?>
                </div>
            </div>
			<?php
		} else {
			$float      = "start";
			$grid_class = "ok-md-12";
		}
		if ( $is_shear ) {
			?>
            <div class="<?php echo esc_attr( $grid_class ) ?>">
				<?php
				if ( function_exists( 'ADDTOANY_SHARE_SAVE_KIT' ) ) {
					?>
                    <div class="text-end post-share">
						<?php
						ADDTOANY_SHARE_SAVE_KIT( array( 'no_universal_button' => false, 'no_small_icons' => false ) );
						?>
                    </div>
					<?php
				} else {
					if ( DIMA_NOUR_ASSISTANT_IS_ACTIVE ) {
						echo do_shortcode( '[share title="Share this Post" float="' . esc_attr( $float ) . '" circle="true" size="" facebook="true"  vk="true" google_plus="true" linkedin="true" pinterest="fales" reddit="fales" email="fales"]' );
					}
				} ?>
            </div>
		<?php }
		?>
    </div>
<?php } ?>
