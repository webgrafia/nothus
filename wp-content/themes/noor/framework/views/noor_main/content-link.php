<?php
/**
 * @package Dima Framework
 * @subpackage root views
 * @version   1.0.0
 * @since     1.0.0
 * @author    PixelDima <info@pixeldima.com>
 */
if ( is_singular() && ! is_page() ) {
	$args = dima_helper::get_featured_args();
} else {
	$args = dima_helper::get_featured_args( $this );
}
$link = get_post_meta( get_the_ID(), '_dima_link_url', true );
$txt  = get_post_meta( get_the_ID(), '_dima_link_txt', true );

?>
<article <?php post_class( $args['post_class'] ); ?> >
	<?php
	$single_class = "";

	if ( is_single() ):
		$single_class = " single-blockquote";
		dima_helper::dima_get_view( 'noor_main', '_content', 'post-header' );
		dima_get_entry_meta( $args['meta'], $args["blog_type"] );
	endif;
	if ( $args['show_image'] ) {
		dima_featured_image( array(
			'post_type' => $args['blog_type'],
			'img_hover' => $args['img_hover'],
			'elm_hover' => $args['elm_hover'],
		) );

	}
	?>

    <div class="dima-blockquote<?php esc_attr( $single_class ) ?>">
        <span><?php echo dima_get_svg_icon( "ic_link" ) ?></span>
        <div class="dima-link">
            <h4 class="entry-title"><a href="<?php the_permalink(); ?>"
                                       title="<?php echo esc_attr( sprintf( esc_html__( 'Permalink to: "%s"', 'noor' ), the_title_attribute( 'echo=0' ) ) ); ?>"><?php the_title(); ?></a>
            </h4>
            <p class="no-bottom-margin"><a href="<?php echo esc_url( $link ) ?>"
                                           title="<?php echo esc_attr( sprintf( esc_html__( 'Permalink to: "%s"', 'noor' ), the_title_attribute( 'echo=0' ) ) ); ?>"><?php echo esc_url( $link ); ?></a>
            </p>
			<?php if ( $txt != '' ) { ?>
                <p class="no-bottom-margin"> <?php echo esc_attr( $txt ); ?></p>
			<?php } ?>
        </div>
    </div>
	<?php if ( is_single() ): ?>
        <div class="<?php dima_pots_content_class(); ?>">
			<?php dima_get_post_content( $args['is_full_post_content_blog'], $args['words'] ); ?>
			<?php dima_helper::dima_get_view( 'noor_main', '_content', 'post-footer' ); ?>
        </div>
	<?php endif; ?>

</article>