<?php
/**
 * @package Dima Framework
 * @subpackage root views
 * @version   1.0.0
 * @since     1.0.0
 * @author    PixelDima <info@pixeldima.com>
 */

$section_layout = dima_get_section_layout_meta();
$pagination     = dima_helper::dima_am_i_true( dima_helper::dima_get_inherit_option( '', 'dima_projects_details_pagination' ) );

if ( $section_layout != 'full-width' ) {
	?>
	<?php get_header(); ?>

    <div class="page-section-content">
        <div class="container">
            <div class="<?php dima_main_content_class(); ?>" role="main">
				<?php
				while ( have_posts() ):
					the_post();
					dima_helper::dima_get_view( 'noor_main', 'content', 'portfolio' );
				endwhile;
				?>
            </div>
			<?php get_sidebar(); ?>
        </div>
    </div>
	<?php
	dima_helper::dima_get_view( 'dima_global', 'related-portfolio' );

	if ( is_single() && $pagination ):
		do_action( 'dima_end_of_portfolios' );
		dima_post_navigation();
	endif;
	get_footer();
} else {
	?>

	<?php get_header(); ?>

    <div class="<?php dima_main_content_class(); ?>" role="main">
		<?php
		while ( have_posts() ):
			the_post();
			dima_helper::dima_get_view( 'noor_main', 'content', 'portfolio' );
		endwhile;
		?>
    </div>

	<?php
	dima_helper::dima_get_view( 'dima_global', 'related-portfolio' );

	if ( is_single() && $pagination ):
		do_action( 'dima_end_of_portfolios' );
		dima_post_navigation();
	endif;
	get_footer();
} ?>