<?php
/**
 * @package Dima Framework
 * @subpackage root views
 * @version   1.0.0
 * @since     1.0.0
 * @author    PixelDima <info@pixeldima.com>
 */
$section_layout = dima_get_section_layout_meta();

if ( $section_layout == 'full-width' ) {
	get_header();
	dima_helper::dima_get_view( 'dima_global', 'page-content' );
    get_sidebar();
	get_footer();
} else {
	get_header();
	?>
	<div class="container">
		<div class="page-section-content">
			<?php
             dima_helper::dima_get_view( 'dima_global', 'page-content' );
			 get_sidebar();
			 ?>
		</div>
	</div>
	<?php
	get_footer();
}
?>
