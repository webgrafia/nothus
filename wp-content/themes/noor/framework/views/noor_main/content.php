<?php
/**
 * @package Dima Framework
 * @subpackage root views
 * @version   1.0.0
 * @since     1.0.0
 * @author    PixelDima <info@pixeldima.com>
 */
if ( is_singular() && ! is_page() ) {
	// Singel post
	$args = dima_helper::get_featured_args();
} else {
	// post list
	$args = dima_helper::get_featured_args( $this );
}
?>
<article <?php post_class( $args['post_class'] ); ?> >

	<?php if ( is_sticky() && ( $args["blog_type"] == 'standard' || ! DIMA_NOUR_ASSISTANT_IS_ACTIVE ) && in_array( 'sticky', get_post_class() ) ) { ?>
        <div class="post-icon on_the_front">
            <ul class="icons-media">
                <li class="dima_go_sticky">
					<?php echo dima_helper::dima_get_sticky() ?>
                </li>
            </ul>
        </div>
	<?php } ?>

	<?php
	dima_helper::dima_get_view( 'noor_main', '_content', 'post-header' ); ?>
	<?php
	if ( ( $args["blog_type"] != 'masonry' && $args["blog_type"] != 'slide' && $args["blog_type"] != 'grid' ) || ( is_singular() && ! is_page() ) ) {
		dima_get_entry_meta( $args['meta'], $args["blog_type"] );
	}

	?>

	<?php if ( $args['show_image'] ) { ?>
		<?php
		dima_featured_image( array(
			'post_type' => $args['blog_type'],
			'img_hover' => $args['img_hover'],
			'elm_hover' => $args['elm_hover'],
		) ); ?>
		<?php
	}
	if ( ( $args["blog_type"] == 'masonry' || $args["blog_type"] == 'slide' || $args["blog_type"] == 'grid' ) && ( ! is_singular() || is_page() ) ) {
		dima_get_entry_meta( $args['meta'], $args["blog_type"] );
	}

	?>

    <div class="<?php dima_pots_content_class(); ?>">
		<?php dima_get_post_content( $args['is_full_post_content_blog'], $args['words'] ); ?>
		<?php dima_helper::dima_get_view( 'noor_main', '_content', 'post-footer' ); ?>
    </div>

</article>