<?php
/**
 * The template for displaying all single posts
 *
 * @package Dima Framework
 * @subpackage root views
 * @version   1.0.0
 * @since     1.0.0
 * @author    PixelDima <info@pixeldima.com>
 */
?>


<?php get_header(); ?>

    <div class="page-section-content boxed-blog blog-list blog-single">
        <div class="container">

            <div class="<?php dima_main_content_class(); ?>" role="main">
				<?php
				while ( have_posts() ): the_post();
					// Above Post Banner
					dima_above_post_ad();

					do_action( 'dima_action_before_post' );
					dima_helper::dima_get_view( 'noor_main', 'content', get_post_format() );
					do_action( 'dima_action_after_post' );

					// Below Post Banner
					dima_below_post_ad();

					do_action( 'dima_action_before_author' );
					dima_helper::dima_get_view( 'dima_global', 'about-the-author' );
					do_action( 'dima_action_after_author' );

					dima_helper::dima_get_view( 'dima_global', 'related-post' );
					dima_helper::dima_get_view( 'dima_global', 'comments-template' );
				endwhile; ?>
            </div>

			<?php get_sidebar(); ?>
        </div><!-- .container -->
    </div><!-- .page-section-content -->

<?php
/*------------------------------*/
# Add Pagination
/*------------------------------*/
if ( is_single() ):
	// Call article schemas
	// !☺: It will work if the option is on from cuzmezer.
	do_action( 'dima_end_of_post' );

	// Call Pagination.
	dima_post_navigation();
endif; ?>

<?php get_footer(); ?>