<?php
/**
 * @package Dima Framework
 * @subpackage root views
 * @version   1.0.0
 * @since     1.0.0
 * @author    PixelDima <info@pixeldima.com>
 */
?>
<header>
	<?php
	$second_title = dima_helper::dima_am_i_true( dima_helper::dima_get_inherit_option( '_dima_meta_second_title_on_post', 'dima_second_title_on_post' ) );

	if ( is_single() ) {
		$breadcrumbs_display = dima_helper::dima_get_page_title_display();
		if ( $second_title ) {
			if ( $breadcrumbs_display ) {
				?>
                <h2 class="entry-title single-post-title"><?php the_title(); ?></h2>
				<?php
			} else {
				?>
                <h1 class="entry-title single-post-title"><?php the_title(); ?></h1>
				<?php
			}
		}
		?>
	<?php } else { ?>
        <h2 class="entry-title">
            <a href="<?php the_permalink(); ?>"
               title="<?php echo esc_attr( sprintf( esc_html__( 'Permalink to: "%s"', 'noor' ), the_title_attribute( 'echo=0' ) ) ); ?>">
				<?php echo get_the_title(); ?>
            </a>
        </h2>
	<?php } ?>
</header>