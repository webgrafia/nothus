<?php
/**
 * @package Dima Framework
 * @subpackage root views
 * @version   1.0.0
 * @since     1.0.0
 * @author    PixelDima <info@pixeldima.com>
 */

if ( is_singular() && ! is_page() ) {
	$args = dima_helper::get_featured_args();
} else {
	$args = dima_helper::get_featured_args( $this );
}
$post_format = ( get_post_format() != '' ) ? get_post_format() : 'standard';
$data_bg     = '';
$thumb_bg    = '';
if ( $args['cover_color'] != '' ) {
	$thumb_bg = 'style="background-color:' . esc_attr( $args['cover_color'] ) . ';"';
}

if ( has_post_thumbnail() ) {
	$thumb   = get_the_post_thumbnail( null, 'dima-post-standard-image', null );
	$url     = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' );//dima-post-standard-image
	$data_bg = 'data-bg="' . esc_url( $url[0] ) . '"';
}
?>

<article class="post <?php echo esc_attr( $args['post_class'] ); ?>" <?php echo esc_attr( $data_bg ) ?>>

	<?php
	$post_format = get_post_format();
	dima_helper::dima_get_admin_edit();
	$date      = sprintf( '<time class="entry-date" datetime="%1$s"> %2$s</time>', esc_attr( get_the_date( 'c' ) ), esc_html( get_the_date() ) );
	$author    = sprintf( ' ' . esc_html__( 'By', 'noor' ) . ' <a href="%1$s" rel="author" class="vcard"><span  class="fn">%2$s</span></a></li>', get_author_posts_url( get_the_author_meta( 'ID' ) ), get_the_author() );
	$more_icon = apply_filters( 'dima_blog_read_more_icon_slide', '<a class="float-center read-more-icon" href="' . get_post_permalink( get_the_ID() ) . '">' . dima_get_svg_icon( "ic_more_horiz" ) . '</a>' );

	?>
    <div class="post-img">

        <a class="all-over-thumb-link" href="<?php the_permalink(); ?>"
           title="<?php echo esc_attr( sprintf( esc_html__( 'Permalink to: "%s"', 'noor' ), the_title_attribute( 'echo=0' ) ) ); ?>" <?php echo( $thumb_bg ) ?>>
        </a>

        <div class="thumb-overlay">
            <div class="container">
                <div class="thumb-content">
                    <div class="thumb-meta text-center">
                        <span class="date meta-item">
                            <span><?php echo ( $date ) . ( $author ) ?></span>
                        </span>
                    </div>
                    <h3 class="thumb-title text-center">
                        <a href="<?php the_permalink(); ?>"
                           title="<?php echo esc_attr( sprintf( esc_html__( 'Permalink to: "%s"', 'noor' ), the_title_attribute( 'echo=0' ) ) ); ?>"
                           tabindex="0"><?php echo the_title_attribute( 'echo=0' ); ?></a>
                    </h3>
					<?php echo( $more_icon ); ?>
                </div> <!-- .thumb-content /-->
            </div><!-- .container -->
        </div>
    </div>
</article>