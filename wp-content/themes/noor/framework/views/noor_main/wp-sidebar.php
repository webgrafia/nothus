<?php
/**
 * The sidebar containing the main widget area
 *
 * @package Dima Framework
 * @subpackage root views
 * @version   1.0.0
 * @since     1.0.0
 * @author    PixelDima <info@pixeldima.com>
 */
?>


<?php 
/*------------------------------------------------------------*/
# Require Files
# With require_once you can override these files with child theme it uses
# load_template() to include the files which uses require_once()
/*------------------------------------------------------------*/
if ( dima_get_content_layout() != 'no-sidebar' && dima_get_content_layout() != 'mini' ) :
	$dima_sticky_sidebar = dima_helper::dima_am_i_true( dima_helper::dima_get_inherit_option( '_dima_meta_sidebar_sticky', 'dima_sticky_sidebar' ) );

	if ( $dima_sticky_sidebar ) {
		$dima_sticky_sidebar = "sidebar-is-sticky ";
	}
	?>

    <aside class="<?php echo esc_attr( sanitize_html_class( $dima_sticky_sidebar ) ); ?> <?php dima_sidebar_class(); ?>"
           role="complementary">
		<?php dima_helper::dima_get_sidebar(); ?>
    </aside>

<?php endif; ?>

