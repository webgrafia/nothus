<?php
/**
 *  Views: header.php ( The header for our theme )
 *
 * @package Dima Framework
 * @subpackage root views
 * @version   1.0.0
 * @since     1.0.0
 * @author    PixelDima <info@pixeldima.com>
 */

 
/*------------------------------------------------------------*/
# This is the template that displays all of the <head> 
# section and everything up until <div id="all_content">
/*------------------------------------------------------------*/
dima_helper::dima_get_view( 'dima_global', 'header' );

/*------------------------------------------------------------*/
# Call Loading screen if the user active it from the customizr
/*------------------------------------------------------------*/
if (dima_helper::dima_am_i_true( dima_helper::dima_get_inherit_option( '_dima_meta_loading', 'dima_loading' ) )) {
    dima_helper::dima_get_view( 'dima_global', 'content', 'dima-loading' );
}

/*------------------------------------------------------------*/
# <header> 
#     → Add navbar for desktop
#     → Add navbar for mobile
# </header> 
/*------------------------------------------------------------*/
?>
<header id="header" class="menu-absolute clearfix">
    <div class="<?php dima_navbar_wrap_desk_class(); ?>">
        <?php
        //1. Desktop Navbar
        dima_helper::dima_get_view( 'dima_global', 'navbar' );
        ?>
    </div>

    <div class="<?php dima_navbar_wrap_mobile_class(); ?>">
        <?php
        //2. Mobile Navbar
        dima_helper::dima_get_view( 'dima_global', 'navbar-mobile' );
        ?>
    </div>
</header>
<?php


/*------------------------------*/
# Add breadcrumbs
/*------------------------------*/
dima_helper::dima_get_view( 'noor_main', '_breadcrumbs' );

?>
<div class="dima-main clearfix">
<?php
/*------------------------------*/
# Here we add the slider ( RS )
/*------------------------------*/
dima_helper::dima_get_view( 'dima_global', 'slider-below' );