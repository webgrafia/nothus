<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Displays:
 * 			1 → shortcode above sidebar.
 *  	    2 → page content & comments.
 * 			3 → Sidebar | Note: Sidebar will hide if layout is full-width.
 * 			4 → Footer.
 *
 * @package Dima Framework
 * @subpackage root views
 * @version   1.0.0
 * @since     1.0.0
 * @author    PixelDima <info@pixeldima.com>
 */



/*------------------------------*/
# Based on options selected in metabox.
/*------------------------------*/
$section_layout = dima_get_section_layout_meta();

/*------------------------------*/
# If fullWidth do not show sidebar.
/*------------------------------*/
if ($section_layout == 'full-width') {
    get_header();
	dima_helper::dima_display_shortcode_above_sidebar();
	dima_helper::dima_get_view( 'dima_global', 'page-content' ); 
    get_footer();
} else {
    get_header();
    ?>
    <div class="container">
        <div class="page-section-content">

            <?php dima_helper::dima_display_shortcode_above_sidebar(); ?>
			<?php dima_helper::dima_get_view( 'dima_global', 'page-content' ); ?> 
            <?php get_sidebar(); ?>

        </div>
    </div>
    <?php
    get_footer();
}
?>
