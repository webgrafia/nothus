<?php
/**
 * Views: index.php ( The main template file )
 * displays all of the <class="container">
 * 1 → Archive author & search: _archive-header
 * 2 → main template file: index.php
 * 3 → Sidebar
 * 
 * @package Dima Framework
 * @subpackage root views
 * @version   1.0.0
 * @since     1.0.0
 * @author    PixelDima <info@pixeldima.com>
 */
?>


<?php get_header(); ?>

<div class="container">
    <main class="page-section-content">
        <?php $demo = dima_helper::dima_get_template() ?>
        <div class="<?php dima_main_content_class(); ?>" role="main">
            <?php 

            /*------------------------------*/
            # The archive template is used when visitors 
            # → author
            # → search. 
            /*------------------------------*/
            dima_helper::dima_get_view( $demo, '_archive-header' ); 
            
            /*------------------------------*/
            #  The main template file
            /*------------------------------*/
            dima_helper::dima_get_view( 'dima_global', 'index' ); 
            ?>
        </div>
        <?php get_sidebar(); ?>
    </main>
</div>

<?php get_footer(); ?>
