<?php
/**
 * @package Dima Framework
 * @subpackage root views
 * @version   1.0.0
 * @since     1.0.0
 * @author    PixelDima <info@pixeldima.com>
 */

if ( is_singular() && ! is_page() ) {
	// Singel post
	$args = dima_helper::get_featured_args();
} else {
	// post list
	$args = dima_helper::get_featured_args( $this );
}
$quote = get_post_meta( get_the_ID(), '_dima_quote_quote', true );
$cite  = get_post_meta( get_the_ID(), '_dima_quote_cite', true );
?>

<article <?php post_class( $args['post_class'] ); ?> >
	<?php if ( is_single() ):

		dima_helper::dima_get_view( 'noor_main', '_content', 'post-header' );
		dima_get_entry_meta( $args['meta'], $args["blog_type"] );
	endif;
	?>
	<?php
	if ( $args['show_image'] ) {
		dima_featured_image( array(
			'post_type' => $args['blog_type'],
			'img_hover' => $args['img_hover'],
			'elm_hover' => $args['elm_hover'],
		) );
	}
	?>
	<?php if ( is_single() ): ?>
		<?php if ( ! empty( $quote ) ) { ?>
            <div class="dima-blockquote single-blockquote">
                <span><?php echo dima_get_svg_icon( "ic_format_quote" ) ?></span>
                <blockquote class="blog-style">
                    <p><?php echo esc_attr( $quote ); ?></p>
					<?php if ( ! empty( $cite ) ) { ?>
                        <cite><?php echo esc_attr( $cite ); ?></cite>
					<?php } ?>
                </blockquote>
            </div>
		<?php } ?>
		<?php
	else: ?>
		<?php
		if ( empty( $quote ) ) {
			$content = get_the_content();
			preg_match( '/<blockquote.*?>/', get_the_content(), $matches );
			if ( ! empty( $matches ) ) {
				$quote = wp_strip_all_tags( $content );
			}
		}
		?>
		<?php if ( ! empty( $quote ) ) { ?>
            <div class="dima-blockquote">
                <span><?php echo dima_get_svg_icon( "ic_format_quote" ) ?></span>
                <blockquote class="blog-style">
                    <p><a href="<?php the_permalink(); ?>"
                          title="<?php echo esc_attr( sprintf( esc_html__( 'Permalink to: "%s"', 'noor' ), the_title_attribute( 'echo=0' ) ) ); ?>"><?php echo esc_attr( $quote ); ?></a>
                    </p>
					<?php if ( ! empty( $cite ) ) { ?>
                        <cite><?php echo esc_attr( $cite ); ?></cite>
					<?php } ?>
                </blockquote>
            </div>
		<?php } ?>
	<?php endif; ?>

	<?php if ( is_single() ): ?>
        <div class="<?php dima_pots_content_class(); ?>">
			<?php dima_get_post_content( $args['is_full_post_content_blog'], $args['words'] ); ?>
			<?php dima_helper::dima_get_view( 'noor_main', '_content', 'post-footer' ); ?>
        </div>

	<?php endif; ?>

</article>