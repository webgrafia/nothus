<?php
/**
 * The template for displaying widgets areas.
 *
 * @package Dima Framework
 * @subpackage views global
 * @version   1.0.0
 * @since     1.0.0
 * @author    PixelDima <info@pixeldima.com>
 */
?>


<?php
$featured_data = $featured_area = $featured_area_html =
$featured_area_selecet_nb = $footer_widget_1 = $footer_widget_2 = $footer_widget_3 = $footer_widget_4 =
$second_featured_area_selecet_nb = $second_footer_widget_1 = $second_footer_widget_2 =
$second_footer_widget_3 = $second_footer_widget_4 = '';

$footer_layout         = dima_helper::dima_get_inherit_option( '_dima_meta_footer_widget_areas', 'dima_footer_widget_areas' );
$second_footer_layout  = dima_helper::dima_get_inherit_option( '_dima_meta_second_footer_widget_areas', 'dima_second_footer_widget_areas' );
$second_footer_on_off  = dima_helper::dima_am_i_true( dima_helper::dima_get_inherit_option( '_dima_meta_second_footer_on_off', 'dima_second_footer_on_off' ) );
$footer_full           = dima_helper::dima_am_i_true( dima_helper::dima_get_inherit_option( '_dima_meta_footer_full_width', 'dima_footer_full_width' ) );
$footer_full           = ( $footer_full ) ? 'full-width-footer' : 'container';
$featured_area_selecet = dima_helper::dima_am_i_true( dima_helper::dima_get_inherit_option( '_dima_meta_footer_featured_area', 'dima_footer_featured_area' ) );
$first_column          = 'footer-1';
$second_column         = 'footer-2';
$third_column          = 'footer-3';
$fourth_column         = 'footer-4';
$second_first_column   = 'footer-second-1';
$second_second_column  = 'footer-second-2';
$second_third_column   = 'footer-second-3';
$second_fourth_column  = 'footer-second-4';

/*------------------------------*/
# Generate the footer area markups classes.
# For the first widget area.
/*------------------------------*/
switch ( $footer_layout ) {
	case 'footer-1c':
		$footer_widget_1          = 'ok-md-12 ok-xsd-12 ok-sd-12';
		$featured_area_selecet_nb = 1;
		break;

	case 'footer-2c':
		$footer_widget_1          = $footer_widget_2 = 'ok-md-6 ok-xsd-12 ok-sd-12';
		$featured_area_selecet_nb = 2;
		break;

	case 'footer-3c':
		$footer_widget_1          = $footer_widget_2 = $footer_widget_3 = 'ok-md-4 ok-xsd-12 ok-sd-12';
		$featured_area_selecet_nb = 3;
		break;

	case 'footer-4c':
		$footer_widget_1          = $footer_widget_2 = $footer_widget_3 = $footer_widget_4 = 'ok-md-3 ok-xsd-12 ok-sd-12';
		$featured_area_selecet_nb = 4;
		break;

	case 'footer-2c-narrow-wide':
		$footer_widget_1          = 'ok-md-4 ok-xsd-12 ok-sd-12';
		$footer_widget_2          = 'ok-md-8 ok-xsd-12 ok-sd-12';
		$featured_area_selecet_nb = 2;
		break;

	case 'footer-2c-wide-narrow':
		$footer_widget_1          = 'ok-md-8 ok-xsd-12 ok-sd-12';
		$footer_widget_2          = 'ok-md-4 ok-xsd-12 ok-sd-12';
		$featured_area_selecet_nb = 2;
		break;


	case 'footer-3c-wide-left':
		$footer_widget_1          = 'ok-md-6 ok-xsd-12 ok-sd-12';
		$footer_widget_2          = $footer_widget_3 = 'ok-md-3 ok-xsd-12 ok-sd-12';
		$featured_area_selecet_nb = 3;
		break;

	case 'footer-3c-wide-right':
		$footer_widget_1          = $footer_widget_2 = 'ok-md-3 ok-xsd-12 ok-sd-12';
		$footer_widget_3          = 'ok-md-6 ok-xsd-12 ok-sd-12';
		$featured_area_selecet_nb = 3;
		break;

	default:
}

/*------------------------------*/
# Generate the footer area markups classes.
# For the second widget area.
/*------------------------------*/
switch ( $second_footer_layout ) {

	case 'footer-1c':
		$second_footer_widget_1          = 'ok-md-12 ok-xsd-12 ok-sd-12';
		$second_featured_area_selecet_nb = 1;
		break;

	case 'footer-2c':
		$second_footer_widget_1          = $second_footer_widget_2 = 'ok-md-6 ok-xsd-12 ok-sd-12';
		$second_featured_area_selecet_nb = 2;
		break;

	case 'footer-3c':
		$second_footer_widget_1          = $second_footer_widget_2 = $second_footer_widget_3 = 'ok-md-4 ok-xsd-12 ok-sd-12';
		$second_featured_area_selecet_nb = 3;
		break;

	case 'footer-4c':
		$second_footer_widget_1          = $second_footer_widget_2 = $second_footer_widget_3 = $second_footer_widget_4 = 'ok-md-3 ok-xsd-12 ok-sd-12';
		$second_featured_area_selecet_nb = 4;
		break;

	case 'footer-2c-narrow-wide':
		$second_footer_widget_1          = 'ok-md-4 ok-xsd-12 ok-sd-12';
		$second_footer_widget_2          = 'ok-md-8 ok-xsd-12 ok-sd-12';
		$second_featured_area_selecet_nb = 2;
		break;

	case 'footer-2c-wide-narrow':
		$second_footer_widget_1          = 'ok-md-8 ok-xsd-12 ok-sd-12';
		$second_footer_widget_2          = 'ok-md-4 ok-xsd-12 ok-sd-12';
		$second_featured_area_selecet_nb = 2;
		break;

	case 'footer-3c-wide-left':
		$second_footer_widget_1          = 'ok-md-6 ok-xsd-12 ok-sd-12';
		$second_footer_widget_2          = $second_footer_widget_3 = 'ok-md-3 ok-xsd-12 ok-sd-12';
		$second_featured_area_selecet_nb = 3;
		break;

	case 'footer-3c-wide-right':
		$second_footer_widget_1          = $second_footer_widget_2 = 'ok-md-3 ok-xsd-12 ok-sd-12';
		$second_footer_widget_3          = 'ok-md-6 ok-xsd-12 ok-sd-12';
		$second_featured_area_selecet_nb = 3;
		break;

	default:
}

/*------------------------------*/
# Generate the footer areas HTML markups.
/*------------------------------*/
if ( is_active_sidebar( $first_column ) || is_active_sidebar( $second_column ) ||
     is_active_sidebar( $third_column ) || is_active_sidebar( $fourth_column )
) { ?>
    <div class="top-footer">

        <div class=" <?php echo esc_attr( $footer_full ) ?>">

            <div class="footer-widget-area ok-row">
				<?php
				if ( ! empty( $featured_area_selecet ) ) {
					$featured_area_bg_color = dima_helper::dima_get_inherit_option( '_dima_meta_footer_featured_top_bg', 'dima_footer_featured_top_bg' );
					$featured_area_bg_image = dima_helper::dima_get_inherit_option( '_dima_meta_footer_featured_bg_image', 'dima_footer_featured_bg_image' );
					$featured_area          = ' featured_area';
					$featured_area_html     = '<div style="position: relative; height: 1px;"></div>';
					$featured_data          = ( $featured_area_bg_color != '' ) ? 'data-bg-color="' . $featured_area_bg_color . '"' : '';
					$featured_data          = ( $featured_area_bg_image != '' ) ? 'data-bg="' . $featured_area_bg_image . '"' : $featured_data;
				}

				if ( is_active_sidebar( $first_column ) && ! empty( $footer_widget_1 ) ):

					if ( 1 != $featured_area_selecet_nb ) {
						$local_featured_data = $local_featured_area = $local_featured_area_html = '';
					} else {
						$local_featured_data      = $featured_data;
						$local_featured_area      = $featured_area;
						$local_featured_area_html = $featured_area_html;
					}

					echo '<div class="' . esc_attr( $footer_widget_1 ) . ' dima-widget">';
					echo( $local_featured_area_html );
					echo '<div class="dima-area' . $local_featured_area . '"  ' . $local_featured_data . '>';
					dynamic_sidebar( 'footer-1' );
					echo '</div>';
					echo '</div>';
				endif;

				if ( is_active_sidebar( $second_column ) && ! empty( $footer_widget_2 ) ):
					if ( 2 != $featured_area_selecet_nb ) {
						$local_featured_data = $local_featured_area = $local_featured_area_html = '';
					} else {
						$local_featured_data      = $featured_data;
						$local_featured_area      = $featured_area;
						$local_featured_area_html = $featured_area_html;
					}

					echo '<div class="' . esc_attr( $footer_widget_2 ) . ' dima-widget">';
					echo( $local_featured_area_html );
					echo '<div class="dima-area' . $local_featured_area . '"  ' . $local_featured_data . '>';
					dynamic_sidebar( 'footer-2' );
					echo '</div>';
					echo '</div>';
				endif;

				if ( is_active_sidebar( $third_column ) && ! empty( $footer_widget_3 ) ):
					if ( 3 != $featured_area_selecet_nb ) {
						$local_featured_data = $local_featured_area = $local_featured_area_html = '';
					} else {
						$local_featured_data      = $featured_data;
						$local_featured_area      = $featured_area;
						$local_featured_area_html = $featured_area_html;
					}

					echo '<div class="' . esc_attr( $footer_widget_3 ) . ' dima-widget">';
					echo( $local_featured_area_html );
					echo '<div class="dima-area' . $local_featured_area . '"  ' . $local_featured_data . '>';
					dynamic_sidebar( 'footer-3' );
					echo '</div>';
					echo '</div>';
				endif;

				if ( is_active_sidebar( $fourth_column ) && ! empty( $footer_widget_4 ) ):
					if ( 4 != $featured_area_selecet_nb ) {
						$local_featured_data = $local_featured_area = $local_featured_area_html = '';
					} else {
						$local_featured_data      = $featured_data;
						$local_featured_area      = $featured_area;
						$local_featured_area_html = $featured_area_html;
					}
					echo '<div class="' . esc_attr( $footer_widget_4 ) . ' dima-widget">';
					echo( $local_featured_area_html );
					echo '<div class="dima-area' . $local_featured_area . '"  ' . $local_featured_data . '>';
					dynamic_sidebar( 'footer-4' );
					echo '</div>';
					echo '</div>';
				endif;

				?>
            </div>

			<?php if ( $second_footer_on_off ) { ?>
                <div class="second-footer-widget-area ok-row">
					<?php

					if ( is_active_sidebar( $second_first_column ) && ! empty( $second_footer_widget_1 ) ):
						echo '<div class="' . esc_attr( $second_footer_widget_1 ) . ' dima-widget">';
						echo '<div class="dima-area" >';
						dynamic_sidebar( 'footer-second-1' );
						echo '</div>';
						echo '</div>';
					endif;

					if ( is_active_sidebar( $second_second_column ) && ! empty( $second_footer_widget_2 ) ):
						echo '<div class="' . esc_attr( $second_footer_widget_2 ) . ' dima-widget">';
						echo '<div class="dima-area" >';
						dynamic_sidebar( 'footer-second-2' );
						echo '</div>';
						echo '</div>';
					endif;

					if ( is_active_sidebar( $second_third_column ) && ! empty( $second_footer_widget_3 ) ):
						echo '<div class="' . esc_attr( $second_footer_widget_3 ) . ' dima-widget">';
						echo '<div class="dima-area" >';
						dynamic_sidebar( 'footer-second-3' );
						echo '</div>';
						echo '</div>';
					endif;

					if ( is_active_sidebar( $second_fourth_column ) && ! empty( $second_footer_widget_4 ) ):
						echo '<div class="' . esc_attr( $second_footer_widget_4 ) . ' dima-widget">';
						echo '<div class="dima-area" >';
						dynamic_sidebar( 'footer-second-4' );
						echo '</div>';
						echo '</div>';
					endif;

					?>
                </div>
			<?php } ?>

        </div>
    </div>
<?php }