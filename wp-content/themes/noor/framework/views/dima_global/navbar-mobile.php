<?php
/**
 * @package Dima Framework
 * @subpackage views global
 * @version   1.0.0
 * @since     1.0.0
 * @author    PixelDima <info@pixeldima.com>
 */
// Outputs the navbar.
$header_position  = dima_get_header_positioning();
$full_width_class = dima_helper::dima_get_header_content_wrapper();
?>


<div class="<?php dima_navbar_class(); ?>">
    <div class="<?php echo esc_attr( $full_width_class ) ?>">
        <div class="mobile-nav-head">
            <!-- Nav bar button -->
            <a class="dima-btn-nav" href="#">
                <span class="menu_icon_item"><?php echo dima_get_svg_icon( "ic_menu" ) ?></span>
                <span class="menu_icon_item sort_ic"><?php echo dima_get_svg_icon( "ic_clear" ) ?></span>
            </a>

            <!-- LOGO -->
            <div class="logo">
                <p class="site-title">
					<?php dima_helper::dima_get_view( 'dima_global', 'brand-mobile' ); ?>
                </p>
            </div>
        </div>
        <!-- Manue -->
		<?php dima_helper::dima_get_view( 'dima_global', 'nav', 'primary-mobile' ); ?>

    </div>
    <!-- container -->
</div>

