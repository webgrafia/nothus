<?php
/**
 * Display page content
 * Displays:
 *          1 → page content.
 *          2 → comments template.
 *
 * @package Dima Framework
 * @subpackage global views
 * @author PixelDIma
 * @copyright (c) Copyright by PixelDIma
 * @link http://pixeldima.com
 * @since Version 1.0
 */


?>
<div class="<?php dima_main_content_class(); ?>" role="main">
    <?php while (have_posts()) :
        the_post(); ?>
        <?php dima_helper::dima_get_view( 'noor_main', 'content', 'page' ); ?>
        <?php dima_helper::dima_get_view( 'dima_global', 'comments-template' ); ?>
    <?php endwhile; ?>
</div>
