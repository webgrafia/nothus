<?php
/**
 * @package Dima Framework
 * @subpackage views global
 * @version   1.0.0
 * @since     1.0.0
 * @author    PixelDima <info@pixeldima.com>
 */
if ( dima_helper::dima_am_i_true( dima_helper::dima_get_inherit_option( '_dima_meta_author_post', 'dima_author_post' ) ) ) {
	echo( dima_get_shortcode_author_theme( '[id="' . get_the_author_meta( 'ID' ) . '" title=""]' ) );
}