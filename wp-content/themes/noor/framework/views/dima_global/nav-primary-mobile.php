<?php
/**
 * @package Dima Framework
 * @subpackage views global
 * @version   1.0.0
 * @since     1.0.0
 * @author    PixelDima <info@pixeldima.com>
 */
$two_menu                     = '';
$showme_icon_menu             = dima_helper::dima_am_i_true( dima_helper::dima_get_inherit_option( '_dima_meta_header_navbar_icon_menu', 'dima_header_navbar_icon_menu' ) );
$showme_burger_menu_on_mobile = dima_helper::dima_am_i_true( dima_helper::dima_get_inherit_option( '_dima_meta_header_navbar_burger_mobile', 'dima_header_navbar_burger_mobile' ) );
$showme_primary_menu          = dima_helper::dima_am_i_true( dima_helper::dima_get_inherit_option( '_dima_meta_header_navbar_primary_menu', 'dima_header_navbar_primary_menu' ) );

if ( $showme_icon_menu ) {
	$two_menu = " premery_and_icon";
}

?>

<nav class="dima-nav-tag dima-tag-primary-menu clearfix<?php echo esc_attr( $two_menu ) ?>">
	<?php
	if ( $showme_primary_menu ) {
		dima_output_primary_navigation( true );
	}

	if ( $showme_burger_menu_on_mobile ) {
		dima_output_burger_navigation( true );
	}

	if ( $showme_icon_menu ) {
		dima_output_icon_navigation( true );
	}

	?>
    <ul class="dima-nav nav-shop-search">
        <li class="li-shop-search">
            <form method="get" id="searchform-2" class="form-search center-text"
                  action="<?php echo esc_url( home_url( '/' ) ); ?>">
                <input type="text" name="s" placeholder="<?php esc_attr_e( 'Search...', 'noor' ); ?>">
                <span class="search_icon"><?php echo dima_get_svg_icon( "ic_search" ) ?></span>
            </form>
        </li>
		<?php
		$is_search_active = dima_helper::dima_am_i_true( dima_helper::dima_get_inherit_option( '_dima_meta_search_display', 'dima_header_search_enable' ) );
		if ( $is_search_active ) { ?>
			<?php
			if ( DIMA_WC_IS_ACTIVE && dima_helper::dima_am_i_true( dima_helper::dima_get_inherit_option( '_dima_meta_shop_menu', 'dima_shop_menu' ) ) ) {
				echo dima_wc_navbar_cart( "0" );
			}
		} ?>
    </ul>
</nav>