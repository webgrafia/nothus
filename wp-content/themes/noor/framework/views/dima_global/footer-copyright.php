<?php
/**
 * The template for copyright in footer.
 *
 * @package Dima Framework
 * @subpackage views global
 * @version   1.0.0
 * @since     1.0.0
 * @author    PixelDima <info@pixeldima.com>
 */
?>


<?php
$dima_menu_display = esc_attr( dima_helper::dima_get_option( 'dima_footer_menu_display' ) );
$dima_copyright    = dima_helper::dima_get_option( 'dima_footer_content_text' );
$dima_class        = "float-start";
$footer_is_big     = dima_helper::dima_am_i_true( dima_helper::dima_get_inherit_option( '_dima_meta_footer_big', 'dima_footer_big' ) );

$allowed_tags = array(
	'strong' => array(),
	'br'     => array(),
	'em'     => array(),
	'p'      => array( 'a' => true ),
	'a'      => array(
		'href'   => true,
		'target' => true,
		'title'  => true,
	)
);
if ( $dima_menu_display != '1' ) {
	$dima_class = "text-center";
}
?>
<?php
$small_footer = $second_footer_on_off = dima_helper::dima_am_i_true( dima_helper::dima_get_inherit_option( '_dima_meta_small_footer_on_off', 'dima_small_footer_on_off' ) );

if ( $small_footer ) {
	?>
    <div class="<?php dima_footer_class(); ?>">
		<?php

		if ( ! $footer_is_big && is_single() ) { ?>
            <div class="dima-spacer-module" data-units="px" data-all_size="30" data-xld_resolution="1600"
                 data-xld_size="30" data-ld_resolution="1400" data-ld_size="30" data-md_resolution="1170"
                 data-md_size="30" data-sd_resolution="969" data-sd_size="30" data-xsd_resolution="480"
                 data-xsd_size="30" style="height: 30px;">
            </div>
		<?php }
		?>
        <div class="container">

			<?php if ( dima_helper::dima_get_option( 'dima_footer_content_display' ) == '1' ) : ?>
                <div class="copyright <?php echo esc_attr( $dima_class ) ?>">
					<?php
					echo wp_kses( $dima_copyright, $allowed_tags );
					?>
                </div>
			<?php endif; ?>

			<?php if ( $dima_menu_display == '1' ) : ?>
				<?php dima_helper::dima_get_view( 'dima_global', 'nav', 'footer' ); ?>
			<?php endif; ?>

        </div>
    </div>
<?php } ?>