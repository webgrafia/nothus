<?php
/**
 *DESCRIPTION: Branding logo (Display logo if selected and the website name if not ).
 * @package Dima Framework
 * @subpackage views global
 * @version   1.0.0
 * @since     1.0.0
 * @author    PixelDima <info@pixeldima.com>
 */

$website_name        = get_bloginfo( 'name' );
$website_description = get_bloginfo( 'description' );
$logo_2x_info        = $logo_info = $logo_mobile_2x = $website_logo_2x = $website_logo = '';
if ( ! is_rtl() ) {
	$logo           = dima_helper::dima_get_inherit_option( "_dima_meta_header_logo", "dima_header_logo" );
	$logo_2x        = $var_header_logo_width = dima_helper::dima_get_inherit_option( "_dima_meta_header_logo_retina", "dima_header_logo_retina" );
	$logo_mobile    = $var_header_logo_width = dima_helper::dima_get_inherit_option( "_dima_meta_header_mobile_logo", "dima_header_mobile_logo" );
	$logo_mobile_2x = $var_header_logo_width = dima_helper::dima_get_inherit_option( "_dima_meta_header_mobile_logo_retina", "dima_header_mobile_logo_retina" );
} else {
	$logo           = dima_helper::dima_get_inherit_option( "_dima_meta_header_logo_rtl", "dima_header_logo_rtl" );
	$logo_2x        = $var_header_logo_width = dima_helper::dima_get_inherit_option( "_dima_meta_header_logo_retina_rtl", "dima_header_logo_retina_rtl" );
	$logo_mobile    = $var_header_logo_width = dima_helper::dima_get_inherit_option( "_dima_meta_header_mobile_logo_rtl", "dima_header_mobile_logo_rtl" );
	$logo_mobile_2x = $var_header_logo_width = dima_helper::dima_get_inherit_option( "_dima_meta_header_mobile_logo_retina_rtl", "dima_header_mobile_logo_retina_rtl" );
}

if ( $logo_mobile != '' ) {
	$logo_info    = dima_helper::dima_get_attachment_info_by_url( $logo_mobile );
	$logo_2x_info = dima_helper::dima_get_attachment_info_by_url( $logo_mobile_2x );
	$logo         = $logo_mobile;
} elseif ( $logo != '' ) {
	$logo_info    = dima_helper::dima_get_attachment_info_by_url( $logo );
	$logo_2x_info = dima_helper::dima_get_attachment_info_by_url( $logo_2x );
}

if ( $logo_2x != '' ) {
	$website_logo    = '<img width="' . esc_attr( $logo_info[1] ) . '" height="' . esc_attr( $logo_info[2] ) . '" class="dima-logo-1x" src="' . esc_url( $logo ) . '" alt="' . esc_attr( $website_description ) . '">';
	$website_logo_2x = '<img width="' . esc_attr( $logo_2x_info[1] ) . '" height="' . esc_attr( $logo_2x_info[2] ) . '" class="dima-logo-2x" src="' . esc_url( $logo_2x ) . '" alt="' . esc_attr( $website_description ) . '">';
} elseif ( $logo != '' ) {
	$website_logo = '<img  width="' . esc_attr( $logo_info[1] ) . '" height="' . esc_attr( $logo_info[2] ) . '" src="' . esc_url( $logo ) . '" alt="' . esc_attr( $website_description ) . '">';
}

?>

<a class="dima-brand" href="<?php echo esc_url( home_url( '/' ) ); ?>"
   title="<?php echo esc_attr( $website_description ); ?>">
	<?php
	if ( $logo_mobile == '' ) {
		echo ( $logo == '' ) ? esc_attr( $website_name ) : wp_kses( $website_logo, dima_helper::dima_get_allowed_html_tag() );
		echo( $website_logo_2x );
	} else {
		echo wp_kses( $website_logo, dima_helper::dima_get_allowed_html_tag() );
		echo wp_kses( $website_logo_2x, dima_helper::dima_get_allowed_html_tag() );
	}
	?>
</a>