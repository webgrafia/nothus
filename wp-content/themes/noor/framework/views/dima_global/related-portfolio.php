<?php
/**
 * @package Dima Framework
 * @subpackage views global
 * @version   1.0.0
 * @since     1.0.0
 * @author    PixelDima <info@pixeldima.com>
 */

$is_related      = dima_helper::dima_am_i_true( dima_helper::dima_get_inherit_option( '_dima_meta_projects_related_display', 'dima_projects_related_display' ) );
$columns         = dima_helper::dima_get_option( 'dima_projects_related_columns' );
$count           = dima_helper::dima_get_option( 'dima_projects_related_count' );
$elm_hover       = dima_helper::dima_get_option( 'dima_projects_related_elm_hover' );
$img_hover       = dima_helper::dima_get_option( 'dima_projects_related_img_hover' );
$portfolio_style = dima_helper::dima_get_option( 'dima_projects_related_style' );

?>
<?php if ( $is_related ) { ?>
    <div class="section related-portfolio">
        <div class="page-section-content">
            <div class="container page-section">
                <div class="portfolio-posts clearfix">

					<?php do_action( 'dima_action_before_related_projects_title' ); ?>
                    <h4 class="related-portfolio-title"><?php echo esc_html( apply_filters( 'dima_filter_related_projects_title', esc_html__( 'Related Projects', 'noor' ) ) ); ?></h4>

					<?php do_action( 'dima_action_after_related_projects_title' ); ?>

                    <span class="noor-line dima-divider noor-start"></span>
                    <span class="double-clear"></span>

					<?php do_action( 'dima_action_before_related_projects' ); ?>
					<?php echo do_shortcode( '[related_portfolio column="' . esc_attr( $columns ) . '" img_hover="' . esc_attr( $img_hover ) . '" elm_hover="' . esc_attr( $elm_hover ) . '" count="' . esc_attr( $count ) . '" portfolio_style="' . esc_attr( $portfolio_style ) . '" ]' ) ?>
					<?php do_action( 'dima_action_after_related_projects' ); ?>

                </div>
            </div>
        </div>
    </div>
<?php } ?>