<?php
/**
 * @package Dima Framework
 * @subpackage views global
 * @version   1.0.0
 * @since     1.0.0
 * @author    PixelDima <info@pixeldima.com>
 */
?>

<?php
$header_position     = dima_get_header_positioning();
$text_align          = dima_helper::dima_get_inherit_option( '_dima_meta_navbar_text_align', 'dima_header_navbar_text_align' );
$showme_btn          = dima_helper::dima_am_i_true( dima_helper::dima_get_inherit_option( '_dima_meta_navbar_button', 'dima_header_navbar_button' ) );
$showme_icon_menu    = dima_helper::dima_am_i_true( dima_helper::dima_get_inherit_option( '_dima_meta_header_navbar_icon_menu', 'dima_header_navbar_icon_menu' ) );
$showme_primary_menu = dima_helper::dima_am_i_true( dima_helper::dima_get_inherit_option( '_dima_meta_header_navbar_primary_menu', 'dima_header_navbar_primary_menu' ) );
$btn_url             = dima_helper::dima_get_inherit_option( '_dima_meta_navbar_button_url', 'dima_header_navbar_button_url' );
$btn_txt             = dima_helper::dima_get_inherit_option( '_dima_meta_navbar_button_txt', 'dima_header_navbar_button_txt' );
$items               = '';
$text_align_icon     = 'text-end';
$items               = dima_navbar_ain_icons( $items );

if ( $header_position == "big-navegation" ) {
	$text_align = "text-start";
	$showme_btn = false;
} elseif ( $header_position == "fixed-left-small" || $header_position == "fixed-right-small" ) {
	$text_align      = "text-center";
	$text_align_icon = "text-center";
	$showme_btn      = false;
} elseif ( $header_position == "fixed-left" || $header_position == "fixed-right" ) {
	$text_align_icon = $text_align;
}
?>

<?php if ( $showme_primary_menu ) { ?>
    <nav class="dima-nav-tag dima-tag-primary-menu <?php echo esc_attr( $text_align ) ?> clearfix">
		<?php dima_output_primary_navigation(); ?>
    </nav>
<?php } ?>

    <div class="dima-nav-tag dima-tag-icon-menu <?php echo esc_attr( $text_align_icon ) ?> clearfix">
		<?php
		if ( $showme_icon_menu ) {
			?>
			<?php dima_output_icon_navigation(); ?>
			<?php
		}
		?>
		<?php if ( ! empty( $items ) ) { ?>
            <ul class="dima-nav icon-menu">
				<?php
				echo( $items );
				?>
            </ul>
		<?php } ?>

    </div>

<?php
if ( $showme_btn && ( $header_position != "fixed-left-small" && $header_position != "fixed-right-small" && $header_position != "fixed-left" && $header_position != "fixed-right" ) ) {
	?>
    <div class="dima-nav-tag dima-tag-btn-menu text-end clearfix">
        <a href="<?php echo esc_url( $btn_url ); ?>"
           class="dima-dima-btn-pill dima-button fill"><span><?php echo esc_attr( $btn_txt ) ?></span></a>
    </div>
	<?php
}
?>