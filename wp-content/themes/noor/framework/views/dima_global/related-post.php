<?php
/**
 * @package Dima Framework
 * @subpackage views global
 * @version   1.0.0
 * @since     1.0.0
 * @author    PixelDima <info@pixeldima.com>
 */

$is_related     = dima_helper::dima_am_i_true( dima_helper::dima_get_inherit_option( '_dima_meta_post_related_display', 'dima_post_related_display' ) );
$is_slide       = dima_helper::dima_am_i_true( dima_helper::dima_get_option( 'dima_post_related_is_slide' ) );
$columns        = dima_helper::dima_get_inherit_option( '_dima_meta_post_related_columns', 'dima_post_related_columns' );
$count          = dima_helper::dima_get_option( 'dima_post_related_count' );

?>
<?php if ( $is_related && DIMA_NOUR_ASSISTANT_IS_ACTIVE ) { ?>
		<?php echo do_shortcode( '[related_posts column="' . esc_attr( $columns ) . '" count="' . esc_attr( $count ) . '" is_slide="' . esc_attr( $is_slide ) . '"]' ) ?>
<?php } ?>