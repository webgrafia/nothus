<?php
/**
 *DESCRIPTION: Branding logo (Display logo if selected and the website name if not ).
 * @package Dima Framework
 * @subpackage views global
 * @version   1.0.0
 * @since     1.0.0
 * @author    PixelDima <info@pixeldima.com>
 */

$website_name        = get_bloginfo( 'name' );
$website_description = get_bloginfo( 'description' );
$logo_info           = $logo_sticky_x2_info = $logo_two_info = $logo_x2_info =
$website_logo_x2 = $website_logo =
$website_logo_sticky = $website_logo_sticky_x2 = '';

/*------------------------------*/
# Get logo & 2x and sticky logo & 2x depend on direction RTL or LTR
/*------------------------------*/
if ( ! is_rtl() ) {
	$logo    = dima_helper::dima_get_inherit_option( "_dima_meta_header_logo", "dima_header_logo" );
	$logo_x2 = dima_helper::dima_get_inherit_option( "_dima_meta_header_logo_retina", "dima_header_logo_retina" );

	$logo_sticky    = dima_helper::dima_get_inherit_option( "_dima_meta_header_sticky_logo", "dima_header_sticky_logo" );
	$logo_sticky_x2 = dima_helper::dima_get_inherit_option( "_dima_meta_header_sticky_logo_retina", "dima_header_sticky_logo_retina" );
} else {
	$logo    = dima_helper::dima_get_inherit_option( "_dima_meta_header_logo_rtl", "dima_header_logo_rtl" );
	$logo_x2 = dima_helper::dima_get_inherit_option( "_dima_meta_header_logo_retina_rtl", "dima_header_logo_retina_rtl" );

	$logo_sticky    = dima_helper::dima_get_inherit_option( "_dima_meta_header_sticky_logo_rtl", "dima_header_sticky_logo_rtl" );
	$logo_sticky_x2 = dima_helper::dima_get_inherit_option( "_dima_meta_header_sticky_logo_retina_rtl", "dima_header_sticky_logo_retina_rtl" );
}

if ( $logo != '' ) {
	$logo_info    = dima_helper::dima_get_attachment_info_by_url( $logo );
	$website_logo = '<img width="' . esc_attr( $logo_info[1] ) . '" height="' . esc_attr( $logo_info[2] ) . '" class="dima-logo-1x" src="' . esc_url( $logo ) . '" alt="' . esc_attr( $website_description ) . '">';
}

/*------------------------------*/
# Get logo & 2x and sticky logo & 2x depend on direction RTL or LTR
/*------------------------------*/
if ( $logo_x2 != '' ) {
	$logo_x2_info    = dima_helper::dima_get_attachment_info_by_url( $logo_x2 );
	$website_logo_x2 = '<img width="' . esc_attr( $logo_x2_info[1] ) . '" height="' . esc_attr( $logo_x2_info[2] ) . '" class="dima-logo-2x" src="' . esc_url( $logo_x2 ) . '" alt="' . esc_attr( $website_description ) . '">';
}

if ( $logo_sticky != '' ) {
	$logo_two_info = dima_helper::dima_get_attachment_info_by_url( $logo_sticky );
}

if ( $logo_sticky_x2 != '' ) {
	$logo_sticky_x2_info    = dima_helper::dima_get_attachment_info_by_url( $logo_sticky_x2 );
	$website_logo_sticky    = '<img width="' . esc_attr( $logo_two_info[1] ) . '" height="' . esc_attr( $logo_two_info[2] ) . '" class="dima-logo-1x" src="' . esc_url( $logo_sticky ) . '" alt="' . esc_attr( $website_description ) . '">';
	$website_logo_sticky_x2 = '<img width="' . esc_attr( $logo_sticky_x2_info[1] ) . '" height="' . esc_attr( $logo_sticky_x2_info[2] ) . '" class="dima-logo-2x" src="' . esc_url( $logo_sticky_x2 ) . '" alt="' . esc_attr( $website_description ) . '">';
} elseif ( $logo_sticky != '' ) {
	$website_logo_sticky = '<img width="' . esc_attr( $logo_two_info[1] ) . '" height="' . esc_attr( $logo_two_info[2] ) . '" src="' . esc_url( $logo_sticky ) . '" alt="' . esc_attr( $website_description ) . '">';
}

?>

<a class="dima-brand" href="<?php echo esc_url( home_url( '/' ) ); ?>"
   title="<?php echo esc_attr( $website_description ); ?>">
	<?php if ( ! dima_is_transparent_navigation() || $logo_sticky == '' ) {
		?>
        <span class="no-sticky-logo">
			<?php echo ( $logo == '' ) ? $website_name : wp_kses( $website_logo, dima_helper::dima_get_allowed_html_tag() ); ?>
			<?php echo wp_kses( $website_logo_x2, dima_helper::dima_get_allowed_html_tag() ); ?>
		</span>
		<?php
	} else {
		?>
        <span class="no-fixed-logo">
			<?php echo ( $logo_sticky == '' ) ? $website_name : wp_kses( $website_logo_sticky, dima_helper::dima_get_allowed_html_tag() ); ?>
			<?php echo wp_kses( $website_logo_sticky_x2, dima_helper::dima_get_allowed_html_tag() ); ?>
		</span>

        <span class="fixed-logo">
			<?php echo ( $logo == '' ) ? $website_name : wp_kses( $website_logo, dima_helper::dima_get_allowed_html_tag() ); ?>
			<?php echo wp_kses( $website_logo_x2, dima_helper::dima_get_allowed_html_tag() ); ?>
		</span>
	<?php } ?>
</a>