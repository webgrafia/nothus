<?php
/**
 * This is the template that displays all of the <head> section and everything up until <div id="all_content">
 *
 * @package Dima Framework
 * @subpackage global view
 * @version   1.0.0
 * @since     1.0.0
 * @author    PixelDima <info@pixeldima.com>
 */
?>

<!DOCTYPE html>
<!--[if IE 9]>
<html class="no-js ie9" <?php language_attributes(); ?>>
<![endif]-->
<!--[if gt IE 9]><!-->
<html class="no-js" <?php language_attributes(); ?>>
<!--<![endif]-->

<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php
	if ( ! function_exists( 'dima_meta_site_icon' ) ) :
		/**
		 * For WordPress 4.3 and older.
		 */
		function dima_meta_site_icon() {
			/*Faveicon*/
			$dima_iphone_icon        = dima_helper::dima_get_option( 'dima_iphone_icon' );
			$dima_iphone_retina_icon = dima_helper::dima_get_option( 'dima_iphone_retina_icon' );
			$dima_ipad_icon          = dima_helper::dima_get_option( 'dima_ipad_icon' );
			$dima_ipad_retina_icon   = dima_helper::dima_get_option( 'dima_ipad_retina_icon' );

			$dima_favicon = dima_helper::dima_get_option( 'dima_favicon' );
			if ( $dima_favicon != '' ) {
				echo '<link rel="shortcut icon" href="' . esc_url( dima_helper::dima_protocol_relative( $dima_favicon ) ) . '">';
			}
			if ( $dima_iphone_icon != '' ) {
				echo '<link rel="apple-touch-icon" sizes="57x57" href="' . esc_url( dima_helper::dima_protocol_relative( $dima_iphone_icon ) ) . '">';
			}
			if ( $dima_iphone_retina_icon != '' ) {
				echo '<link rel="apple-touch-icon" sizes="120x120" href="' . esc_url( dima_helper::dima_protocol_relative( $dima_iphone_retina_icon ) ) . '">';
			}
			if ( $dima_ipad_icon != '' ) {
				echo '<link rel="apple-touch-icon" sizes="72x72" href="' . esc_url( dima_helper::dima_protocol_relative( $dima_ipad_icon ) ) . '">';
			}
			if ( $dima_ipad_retina_icon != '' ) {
				echo '<link rel="apple-touch-icon" sizes="144x144" href="' . esc_url( dima_helper::dima_protocol_relative( $dima_ipad_retina_icon ) ) . '">';
			}
			/*Appel Icon*/
		}

		if ( ! dima_helper::dima_wp_version_check( '4.3' ) ) {
			add_action( 'wp_head', 'dima_meta_site_icon', 0 );
		}
	endif;
	?>
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php do_action( 'dima_after_body_open_tag' ); ?>
<?php
dima_helper::dima_get_view( 'noor_main', 'search-burger' );
if ( dima_get_site_layout() == "framed" ) {
	dima_helper::dima_get_view( 'dima_global', 'content', 'dima-border' );
}
?>

<div id="dima-wrapper" class="all_content">