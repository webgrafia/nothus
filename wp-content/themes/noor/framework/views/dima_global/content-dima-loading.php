<?php
/**
 * Loading view
 * 
 * @package Dima Framework
 * @subpackage views global
 * @version   1.0.0
 * @since     1.0.0
 * @author    PixelDima <info@pixeldima.com>
 */
?>

<?php
$website_name        = get_bloginfo( 'name' );
$website_description = get_bloginfo( 'description' );

$website_logo = '';
$logo         = dima_helper::dima_get_inherit_option( "_dima_meta_loading_logo", "dima_loading_logo" );

if ( $logo != '' ) {
	$logo_info    = dima_helper::dima_get_attachment_info_by_url( $logo );
	$website_logo = '<img  width="' . esc_attr( $logo_info[1] ) . '" height="' . esc_attr( $logo_info[2] ) . '" src="' . esc_url( $logo ) . '" alt="' . esc_attr( $website_description ) . '">';
}
if ( $website_name != '' ) {
	$website_name = '<h3>' . $website_name . '</h3>';
}
?>


<div class="loader loader-sketched">
    <div class="loader-animation-container">
        <div class="loader-line-left"></div>
        <div class="loader-sticker"></div>
        <div class="loader-progress"></div>
        <div class="loader-line-right"></div>
    </div>
    <div class="loader-logo">
        <div class="logo">
            <div class="logo-icon">
				<?php echo ( $logo == '' ) ? wp_kses( $website_name, dima_helper::dima_get_allowed_html_tag() ) : wp_kses( $website_logo, dima_helper::dima_get_allowed_html_tag() ); ?>
            </div>
        </div>
    </div>
</div>
