<?php
/**
 * @package Dima Framework
 * @subpackage views global
 * @version   1.0.0
 * @since     1.0.0
 * @author    PixelDima <info@pixeldima.com>
 */
?>
<?php
$color   = dima_helper::dima_get_inherit_option( "_dima_meta_frame_color", "dima_frame_color" );
$fram_bg = $color == '' ? '' : 'style="background: ' . sanitize_text_field( $color ) . ';"';
?>
<span class="dima-framed-line line-top" <?php echo sanitize_text_field( $fram_bg ); ?>></span>
<span class="dima-framed-line line-right" <?php echo sanitize_text_field( $fram_bg ); ?>></span>
<span class="dima-framed-line line-bottom" <?php echo sanitize_text_field( $fram_bg ); ?>></span>
<span class="dima-framed-line line-left" <?php echo sanitize_text_field( $fram_bg ); ?>></span>