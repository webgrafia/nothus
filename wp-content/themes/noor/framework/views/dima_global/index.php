<?php
/**
 * @package Dima Framework
 * @subpackage views global
 * @version   1.0.0
 * @since     1.0.0
 * @author    PixelDima <info@pixeldima.com>
 */

$excerpt = esc_attr( dima_helper::dima_get_option( 'dima_blog_blog_excerpt' ) );

if ( is_home() ) {
	dima_get_blog_shortcode_theme();
} elseif ( is_archive() ) {
	if ( ! isset( $post->post_type ) ) {
		return;
	}
	if ( $post->post_type == 'dima-portfolio' && DIMA_NOUR_ASSISTANT_IS_ACTIVE ) {
		$column          = dima_helper::dima_get_option( 'dima_projects_related_columns' );
		$portfolio_style = "grid";
		$type            = "portfolio";
		$no_margin       = dima_helper::dima_am_i_true( dima_helper::dima_get_option( 'dima_portfolio_page_margin' ) );
		$filters         = dima_helper::dima_get_option( 'dima_portfolio_page_filters' );
		$img_hover       = dima_helper::dima_get_option( 'dima_projects_related_img_hover' );
		$elm_hover       = dima_helper::dima_get_option( 'dima_projects_related_elm_hover' );
		$count           = dima_helper::dima_get_option( 'dima_portfolio_page_count' );
		$paging          = dima_helper::dima_am_i_true( dima_helper::dima_get_option( 'dima_portfolio_page_is_pagination' ) );

		echo do_shortcode( '[portfolio
		img_hover="' . esc_attr( $img_hover ) . '"
		elm_hover="' . esc_attr( $elm_hover ) . '"
		filters="' . esc_attr( $filters ) . '"
		count="' . esc_attr( $count ) . '"
		paging="' . esc_attr( $paging ) . '"
		portfolio_style="' . esc_attr( $portfolio_style ) . '" 
		type="' . esc_attr( $type ) . '" 
		column="' . esc_attr( $column ) . '" 
		no_margin="' . esc_attr( $no_margin ) . '"]' );
	} else {
		dima_get_blog_shortcode_theme();
	}
} elseif ( is_search() ) {
	dima_get_blog_shortcode_theme( true );
}