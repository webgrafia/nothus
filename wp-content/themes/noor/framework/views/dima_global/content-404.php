<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @package Dima Framework
 * @subpackage views global
 * @version   1.0.0
 * @since     1.0.0
 * @author    PixelDima <info@pixeldima.com>
 */
?>


    <h6><?php esc_html_e( 'Oops!, This Page Could Not Be Found', 'noor' ) ?></h6>
    <p><?php esc_html_e( 'The Page are looking for might have been removed, had its name change, or is temporarily unavailable.', 'noor' ) ?></p>
    <div class="double-clear"></div>
    <h1>404</h1>
    <div class="double-clear"></div>

<?php get_search_form(); ?>