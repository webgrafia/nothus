<?php
/**
 * Go into top template for the footer.
 *
 * @package Dima Framework
 * @subpackage views global
 * @version   1.0.0
 * @since     1.0.0
 * @author    PixelDima <info@pixeldima.com>
 */
?>


<a class="scroll-to-top off" href="#" id="scrollToTop">
	<?php echo wp_kses( dima_get_svg_icon( "ic_keyboard_arrow_up" ), dima_helper::dima_get_allowed_html_tag() ); ?>
</a>