<?php
/**
 * @package Dima Framework
 * @subpackage views global
 * @version   1.0.0
 * @since     1.0.0
 * @author    PixelDima <info@pixeldima.com>
 */
// Outputs the navbar.

$allowed_tags    = array(
	'strong' => array(),
	'br'     => array(),
	'em'     => array(),
	'p'      => array( 'a' => true ),
	'a'      => array(
		'href'   => true,
		'target' => true,
		'title'  => true,
	)
);
$header_position = dima_get_header_positioning();

$full_width_class = dima_helper::dima_get_header_content_wrapper();
$btn_url          = dima_helper::dima_get_inherit_option( '_dima_meta_navbar_button_url', 'dima_header_navbar_button_url' );
$btn_txt          = dima_helper::dima_get_inherit_option( '_dima_meta_navbar_button_txt', 'dima_header_navbar_button_txt' );

$offset_id  = dima_helper::dima_get_inherit_option( '_dima_meta_navbar_offset_by_id', 'dima_header_navbar_offset_by_id' );
$offset_px  = dima_helper::dima_get_inherit_option( '_dima_meta_navbar_offset_by_px', 'dima_header_navbar_offset_by_px' );
$shear_icon = dima_helper::dima_get_option( 'dima_header_navbar_shear_icon' ) == '1';
$showme_btn = dima_helper::dima_am_i_true( dima_helper::dima_get_inherit_option( '_dima_meta_navbar_button', 'dima_header_navbar_button' ) );

$adr_string          = dima_helper::dima_get_option( 'dima_navbar_option_address_text_topbar' );
$tel_string          = dima_helper::dima_get_option( 'dima_navbar_option_tel_text_topbar' );
$email_string        = dima_helper::dima_get_option( 'dima_navbar_option_email_text_topbar' );
$today_format_string = esc_attr( dima_helper::dima_get_option( 'dima_navbar_option_today_text_topbar' ) );

$data_offset = "";
if ( $offset_id != "" ) {
	$data_offset = 'data-offsetBy="' . esc_attr( $offset_id ) . '"';
} elseif ( $offset_px != "" ) {
	$offset_px = 'data-offsetBy-px="' . esc_attr( $offset_px ) . '"';
}
?>

<?php
/**
 * 1 - Bottom Logo
 * 2 - Big Menu
 * 3 - Full width & logo on center
 * 4 - Vertical menu
 * 5 - Big Menu
 * 6 - Big Menu
 * 7 - Big Menu
 */ ?>
<?php /*1) Bottom Logo*/ ?>
<?php if ( $header_position == 'bottom-logo' ) { ?>
    <div class="<?php dima_navbar_class(); ?>" <?php echo esc_attr( $data_offset );
	echo esc_attr( $offset_px ); ?>>

        <div class="dima-navbar-global <?php echo esc_attr( $full_width_class ) ?>">
			<?php echo dima_get_burger_menu(); ?>
			<?php
			if ( $shear_icon ) {
				?>
                <div class="menu-social-media fill-icon social-media social-small circle-social">
                    <ul class="inline clearfix">
						<?php
						dima_helper::dima_get_global_social();
						?>
                    </ul>
                </div>
				<?php
			}
			?>
			<?php dima_helper::dima_get_view( 'dima_global', 'nav', 'primary' ); ?>
        </div>
    </div>
    <span class="dima_add_space"></span>
    <div class="logo-bottom">
        <div class="logo">
            <p class="site-title">
				<?php dima_helper::dima_get_view( 'dima_global', 'brand' ); ?>
            <p>
        </div>
    </div>


<?php } /*2) Big Menu*/
elseif ( $header_position == 'big-navegation' ) { ?>

    <div class="<?php dima_navbar_class(); ?>" <?php echo esc_attr( $data_offset );
	echo esc_attr( $offset_px ); ?>>
        <div class="big_nav">
            <div class="logo">
                <p class="site-title">
					<?php dima_helper::dima_get_view( 'dima_global', 'brand' ); ?>
                </p>
            </div>

            <div class="dima-nav-tag dima-tag-btn-menu text-end clearfix">
				<?php
				if ( $showme_btn ) {
					?>
                    <a href="<?php echo esc_url( $btn_url ); ?>"
                       class="dima-dima-btn-pill float-end dima-button fill">
                        <span><?php echo esc_attr( $btn_txt ) ?></span></a>
					<?php
				}
				?>
                <div class="dima-topbar">

					<?php if ( $today_format_string != '' ) { ?>
                        <div class="icon_text clearfix">
                            <span class="dima-topbar-icon"><?php echo dima_get_svg_icon( "ic_access_time" ); ?></span>
                            <span
                                    class="dima-topbar-txt"><?php echo date_i18n( $today_format_string, current_time( 'timestamp' ) ); ?></span>
                        </div>
					<?php } ?>

					<?php if ( $email_string != '' ) { ?>
                        <div class="icon_text clearfix">
                            <span class="dima-topbar-icon"><?php echo dima_get_svg_icon( "ic_email" ); ?></span>
                            <span class="dima-topbar-txt">
								<?php
								echo wp_kses( $email_string, $allowed_tags );
								?>
						</span>
                        </div>
					<?php } ?>

					<?php if ( $tel_string != '' ) { ?>
                        <div class="icon_text clearfix">
                            <span class="dima-topbar-icon"><?php echo dima_get_svg_icon( "ic_phone" ); ?></span>
                            <span class="dima-topbar-txt"><?php
								echo wp_kses( $tel_string, $allowed_tags );
								?></span>
                        </div>
					<?php } ?>

					<?php if ( $adr_string != '' ) { ?>
                        <div class="icon_text clearfix">
                            <span class="dima-topbar-icon"><?php echo dima_get_svg_icon( "ic_place" ); ?></span>
                            <span
                                    class="dima-topbar-txt"><?php
								echo wp_kses( $adr_string, $allowed_tags );
								?></span>
                        </div>
					<?php } ?>
					<?php
					dima_helper::dima_language_selector_flags();
					?>
                </div>

            </div>

        </div>

        <div class="dima-navbar-global <?php echo esc_attr( $full_width_class ) ?>">
			<?php echo dima_get_burger_menu(); ?>
			<?php dima_helper::dima_get_view( 'dima_global', 'nav', 'primary' ); ?>
        </div>

    </div>
	<?php /*3) Big menu*/ ?>
<?php } /*3) Big Menu*/
elseif ( $header_position == 'fill-width' || $header_position == 'logo-on-center' ) { ?>

    <div class="<?php dima_navbar_class(); ?>" <?php echo esc_attr( $data_offset );
	echo esc_attr( $offset_px ); ?>>
        <div class="dima-navbar-global <?php echo esc_attr( $full_width_class ) ?>">
			<?php echo dima_get_burger_menu(); ?>
            <div class="logo">
                <p class="site-title">
					<?php dima_helper::dima_get_view( 'dima_global', 'brand' ); ?>
                </p>
            </div>
			<?php dima_helper::dima_get_view( 'dima_global', 'nav', 'primary' ); ?>
        </div>
    </div>
	<?php
} /*4) Vertical menu*/
elseif ( $header_position == "fixed-left-small" || $header_position == "fixed-right-small" ) {
	$bg             = dima_helper::dima_get_option( "dima_navbar_background_image" );
	$dima_copyright = dima_helper::dima_get_option( 'dima_footer_content_text' );
	$dima_class     = "float-start";
	$text_align     = "text-center";

	$allowed_tags = array(
		'strong' => array(),
		'br'     => array(),
		'em'     => array(),
		'p'      => array( 'a' => true ),
		'a'      => array(
			'href'   => true,
			'target' => true,
			'title'  => true,
		)
	);

	?>

    <div class="<?php dima_navbar_class(); ?>" id="sly-frame">
        <div class="vertical-content">

            <div class="logo <?php echo esc_attr( $text_align ) ?>">
                <p class="site-title">
					<?php dima_helper::dima_get_view( 'dima_global', 'brand' ); ?>
                </p>
            </div>
            <div class="golder-warper">
                <a class="dima-btn-nav" href="#"><?php echo dima_get_svg_icon( "ic_menu" ) ?></a>
				<?php dima_helper::dima_get_view( 'dima_global', 'nav', 'primary' ); ?>
            </div>
        </div>

        <div class="dima-copyright clearfix">
			<?php
			if ( $shear_icon ) {
				$is_dark = dima_helper::dima_am_i_true( dima_helper::dima_get_inherit_option( '_dima_meta_navbar_menu_dark', 'dima_header_navbar_menu_dark' ) );
				if ( $is_dark ) {
					$is_dark = " dark-icon";
				}
				?>
                <div class="menu-social-media fill-icon social-media social-small circle-social <?php echo esc_attr( $text_align ) . ' ' . esc_attr( $is_dark ) ?>">
                    <ul class="inline clearfix">
						<?php
						if ( $header_position == "fixed-left-small" || $header_position == "fixed-right-small" ) {
							?>
                            <li class="burger-menu info-full">
                                <a href="#" title="social network"><?php echo dima_get_svg_icon( "ic_add" ) ?></a>
                            </li>
							<?php
						} else {
							dima_helper::dima_get_global_social();
						}
						?>
                    </ul>
                </div>
			<?php } ?>

			<?php if ( dima_helper::dima_am_i_true( dima_helper::dima_get_option( 'dima_menu_copyright_display' ) ) ) : ?>
                <div class="copyright <?php echo esc_attr( $dima_class ) . ' ' . esc_attr( $text_align ) ?>">
					<?php echo
					wp_kses( $dima_copyright, $allowed_tags );
					?>
                </div>
			<?php endif; ?>

        </div>

		<?php
		if ( ! empty( $bg ) ) {
			?>
            <div class="background-image-hide background-cover" data-bg-image="<?php echo esc_url( $bg ); ?>"
                 style="z-index: -1 !important">
            </div>
			<?php
		}
		?>
        <div class="scrollbar">
            <div class="handle"></div>
        </div>

    </div>

<?php } else {
	$bg             = dima_helper::dima_get_option( "dima_navbar_background_image" );
	$text_align     = dima_helper::dima_get_inherit_option( '_dima_meta_navbar_text_align', 'dima_header_navbar_text_align' );
	$dima_copyright = dima_helper::dima_get_option( 'dima_footer_content_text' );
	$dima_class     = "float-start";
	$allowed_tags   = array(
		'strong' => array(),
		'br'     => array(),
		'em'     => array(),
		'p'      => array( 'a' => true ),
		'a'      => array(
			'href'   => true,
			'target' => true,
			'title'  => true,
		)
	);

	?>

    <div class="<?php dima_navbar_class(); ?>">
        <div class="vertical-content">

            <div class="logo <?php echo esc_attr( $text_align ) ?>">
                <p class="site-title">
					<?php dima_helper::dima_get_view( 'dima_global', 'brand' ); ?>
                </p>
            </div>
            <div class="nav-holder" id="sly-frame">
                <div class="golder-warper">
                    <a class="dima-btn-nav" href="#"><?php echo dima_get_svg_icon( "ic_menu" ) ?></a>
					<?php dima_helper::dima_get_view( 'dima_global', 'nav', 'primary' ); ?>
                </div>
                <div class="scrollbar">
                    <div class="handle"></div>
                </div>
            </div>
        </div>

        <div class="dima-copyright clearfix">
			<?php
			if ( $shear_icon ) {
				$is_dark = dima_helper::dima_am_i_true( dima_helper::dima_get_inherit_option( '_dima_meta_navbar_menu_dark', 'dima_header_navbar_menu_dark' ) );
				if ( $is_dark ) {
					$is_dark = " dark-icon";
				}
				?>
                <div class="menu-social-media fill-icon social-media social-small circle-social <?php echo esc_attr( $text_align ) . ' ' . esc_attr( $is_dark ) ?>">
                    <ul class="inline clearfix">
						<?php
						if ( $header_position == "fixed-left-small" || $header_position == "fixed-right-small" ) {
							?>
                            <li class="burger-menu info-full">
                                <a href="#" title="social network"><?php echo dima_get_svg_icon( "ic_add" ) ?></a>
                            </li>
							<?php
						} else {
							dima_helper::dima_get_global_social();
						}
						?>
                    </ul>
                </div>
			<?php } ?>

			<?php if ( dima_helper::dima_am_i_true( dima_helper::dima_get_option( 'dima_menu_copyright_display' ) ) ) : ?>
                <div class="copyright <?php echo esc_attr( $dima_class ) . ' ' . esc_attr( $text_align ) ?>">
					<?php echo
					wp_kses( $dima_copyright, $allowed_tags );
					?>
                </div>
			<?php endif; ?>

        </div>

		<?php
		if ( ! empty( $bg ) ) {
			?>
            <div class="background-image-hide background-cover" data-bg-image="<?php echo esc_url( $bg ); ?>"
                 style="z-index: -1 !important">
            </div>
			<?php
		}
		?>
    </div>

<?php } ?>