<?php
/**
 * Function used by widgets.
 *
 *
 * @package Dima Framework
 * @subpackage Functions
 * @version   1.0.0
 * @since     1.0.0
 * @author    PixelDima <info@pixeldima.com>
 *
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! function_exists( 'dima_get_entry_meta' ) ):
	/**
	 * [Print Entry Meta]
	 */
	function dima_get_entry_meta( $show_meta, $type = '' ) {
		$date   = '<li class="post-on"><a href="' . get_permalink() . '"><time class="entry-date" datetime="' . esc_attr( get_the_date( 'c' ) ) . '"> ' . esc_html( get_the_date() ) . '</time></a>';
		$author = sprintf( ' ' . esc_html__( 'By', 'noor' ) . ' <a href="%1$s" rel="author" class="vcard"><span  class="fn">%2$s</span></a></li>', get_author_posts_url( get_the_author_meta( 'ID' ) ), get_the_author() );

		/**
		 * Comments link
		 */
		$comments = '';
		if ( dima_helper::dima_am_i_true( dima_helper::dima_get_option( 'dima_blog_enable_post_meta_comment' ) ) ) {
			if ( comments_open() ) {
				$Comment        = esc_html__( 'Comment', 'noor' );
				$Comments       = esc_html__( 'Comments', 'noor' );
				$post_title     = apply_filters( 'dima_meta_comments_title', get_the_title() );
				$comments_link  = apply_filters( 'dima_comments_link', get_comments_link() );
				$nb_of_comments = apply_filters( 'dima_comments_number', get_comments_number() );

				if ( $nb_of_comments == 0 ) {
					$text = esc_html__( '0 Comment', 'noor' );
				} else if ( $nb_of_comments == 1 ) {
					$text = $nb_of_comments . ' ' . esc_html( $Comment );
				} else {
					$text = $nb_of_comments . ' ' . esc_html( $Comments );
				}

				$comments = sprintf( '<li class="post-comments">' . wp_kses( dima_get_svg_icon( "ic_forum" ), dima_helper::dima_get_allowed_html_tag() ) . '<a href="%1$s" title="%2$s" class="meta-comments"> %3$s</a></li>', esc_url( $comments_link ), esc_attr( sprintf( esc_html__( 'Leave a comment on: &ldquo;%s&rdquo;', 'noor' ), $post_title ) ), $text );
			} else {
				$comments = '';
			}
		}

		$categories        = get_the_category();
		$separator         = ', ';
		$categories_output = '';
		$categories_list   = '';

		if ( dima_helper::dima_am_i_true( dima_helper::dima_get_option( 'dima_blog_enable_post_meta_cat' ) ) ) {
			$cat_nb    = sizeof( $categories );
			$cat_class = '';
			if ( $cat_nb > 6 ) {
				$cat_class = " full text-start float-start";
			}
			foreach ( $categories as $category ) {
				$categories_output .= '<a href="'
				                      . get_category_link( $category->term_id )
				                      . '" title="'
				                      . esc_attr( sprintf( esc_html__( "View all posts in: &ldquo;%s&rdquo;", 'noor' ), $category->name ) )
				                      . '" rel="category tag"> '
				                      . $category->name
				                      . '</a>'
				                      . $separator;
			}
			if ( $categories_output != '' ) {
				$categories_list = sprintf( '<li class="post-view' . esc_attr( $cat_class ) . '">' . wp_kses( dima_get_svg_icon( "ic_bookmark" ), dima_helper::dima_get_allowed_html_tag() ) . '%1$s</li>', trim( $categories_output, $separator ) );
			}
		}

		/**
		 * Output
		 */
		$second_title = dima_helper::dima_am_i_true( dima_helper::dima_get_inherit_option( '_dima_meta_second_title_on_post', 'dima_second_title_on_post' ) );
		$hr           = '';
		if ( $type == 'standard' || ( is_singular() && ( ! is_page() ) || is_search() ) ) {
			if ( $second_title || ! is_single() ) {
				$hr = '<hr class="entry-title-hr">';
			}
		}
		if ( dima_hide_entry_meta( $show_meta ) ) {
			printf( wp_kses( $hr, dima_helper::dima_get_allowed_html_tag() ) . '<div class="post-meta"><ul>%1$s%2$s%3$s%4$s</ul></div>', $date, $author, $categories_list, $comments );
		}
	}
endif;

/**
 * @param int $login_only
 */
function dima_login_form( $login_only = 0 ) {
	global $user_ID, $user_identity;

	if ( $user_ID ) : ?>
		<?php if ( empty( $login_only ) ): ?>
            <div id="dima-user-login">
                <span class="dima-author-avatar">
                    <?php echo get_avatar( $user_ID, $size = '90' ); ?>
                </span>
                <div class="dima-user-login-content">
                    <h5 class="welcome-text"><?php echo esc_html__( 'Welcome', 'noor' ); ?>
                        : <?php echo esc_attr( $user_identity ) ?></h5>
                    <ul>
                        <li>
                            <a href="<?php echo esc_url( home_url( '/' ) ) ?>/wp-admin/"><?php echo esc_html__( 'Dashboard', 'noor' ) ?> </a>
                        </li>
                        <li>
                            <a href="<?php echo esc_url( home_url( '/' ) ) ?>/wp-admin/profile.php"><?php echo esc_html__( 'Your Profile', 'noor' ) ?> </a>
                        </li>
                        <li>
                            <a href="<?php echo wp_logout_url(); ?>"><?php echo esc_html__( 'Logout', 'noor' ) ?> </a>
                        </li>
                    </ul>
                </div>
            </div>
		<?php endif; ?>
	<?php else: ?>
        <div id="dima-login-form">
            <form name="loginform" id="loginform" action="<?php echo esc_url( home_url( '/' ) ) ?>/wp-login.php"
                  method="post"
                  class="login">
                <p id="form-row-wide">
                    <input type="text" name="log" id="log"
                           value="<?php echo esc_html__( 'Username', 'noor' ) ?>"
                           onfocus="if (this.value == ' <?php echo esc_html__( 'Username', 'noor' ) ?>') {this.value = '';}"
                           onblur="if (this.value == '') {this.value = '<?php echo esc_html__( 'Username', 'noor' ) ?>';}"/>
                </p>
                <p id="form-row-wide">
                    <input type="password" name="pwd" id="pwd"
                           value="<?php echo esc_html__( 'Password', 'noor' ) ?>"
                           onfocus="if (this.value == '<?php echo esc_html__( 'Password', 'noor' ) ?>') {this.value = '';}"
                           onblur="if (this.value == '') {this.value = '<?php echo esc_html__( 'Password', 'noor' ) ?>';}"/>
                </p>
                <input type="submit" name="submit" value="<?php echo esc_html__( 'Login', 'noor' ) ?>"
                       class="button"/>
                <label for="rememberme"><input name="rememberme" id="login-rememberme" type="checkbox"
                                               checked="checked"
                                               value="forever"/> <?php echo esc_html__( 'Remember Me', 'noor' ) ?>
                </label>
                <input type="hidden" name="redirect_to"
                       value="<?php echo esc_url( $_SERVER['REQUEST_URI'] ); ?>"/>
            </form>
            <ul class="login-links">
				<?php if ( get_option( 'users_can_register' ) ) : ?><?php echo wp_register() ?><?php endif; ?>
                <li>
                    <a href="<?php echo esc_url( home_url( '/' ) ) ?>/wp-login.php?action=lostpassword"><?php echo esc_html__( 'Lost your password?', 'noor' ) ?></a>
                </li>
            </ul>
        </div>
	<?php endif;
}

/**
 * Widget
 *
 * @param int  $comment_posts
 * @param int  $avatar_size
 * @param bool $with_avatar
 */
function dima_most_commented( $comment_posts = 5, $avatar_size = 40, $with_avatar = false ) {
	$comments = get_comments( 'status=approve&number=' . esc_attr( $comment_posts ) );
	foreach ( $comments as $comment ) { ?>
        <li>
			<?php if ( $with_avatar ) { ?>
                <div class="dima-author-avatar" style="width:<?php echo esc_attr( $avatar_size ) ?>px">
					<?php echo get_avatar( $comment, $avatar_size ); ?>
                </div>
			<?php } ?>
            <a href="<?php echo get_permalink( $comment->comment_post_ID ); ?>#comment-<?php echo esc_attr( $comment->comment_ID ); ?>">
				<?php echo strip_tags( $comment->comment_author ); ?>
                : <?php echo wp_html_excerpt( $comment->comment_content, 80 ); ?>... </a>
        </li>
	<?php }
}

/**
 * Customizing Category Widget
 *
 * @param $output
 *
 * @return mixed
 */
function dima_cat_count_span_inline( $output ) {
	$output = str_replace( '</a> (', '</a><span> (', $output );
	$output = str_replace( ')', ')</span></a>', $output );

	return $output;
}

add_filter( 'wp_list_categories', 'dima_cat_count_span_inline' );


/**
 * @param $result
 * @param $tag
 *
 * @return mixed
 */
function dima_custom_email_confirmation_validation_filter( $result, $tag ) {
	$tag = new WPCF7_Shortcode( $tag );

	if ( 'your-email-confirm' == $tag->name ) {
		$your_email         = isset( $_POST['your-email'] ) ? trim( $_POST['your-email'] ) : '';
		$your_email_confirm = isset( $_POST['your-email-confirm'] ) ? trim( $_POST['your-email-confirm'] ) : '';
		if ( $your_email != $your_email_confirm ) {
			$result->invalidate( $tag, "Are you sure this is the correct address?" );
		}
	}

	return $result;
}

add_filter( 'wpcf7_validate_email*', 'dima_custom_email_confirmation_validation_filter', 20, 2 );

/*------------------------------------------------------------*/
#
# Remove some markup and styling from WordPress
#
/*------------------------------------------------------------*/

/*------------------------------*/
# Remove 'style=' from Tag Cloud
/*------------------------------*/
if ( ! function_exists( 'dima_remove_tag_cloud_inline_style' ) ) :
	function dima_remove_tag_cloud_inline_style( $tag_string ) {
		return preg_replace( "/style='font-size:.+pt;'/", '', $tag_string );
	}

	add_filter( 'wp_generate_tag_cloud', 'dima_remove_tag_cloud_inline_style', 10, 1 );
endif;

/*------------------------------*/
# Remove Recent Comments Style
/*------------------------------*/
if ( ! function_exists( 'dima_remove_recent_comments_style' ) ) :
	function dima_remove_recent_comments_style() {
		GLOBAL $wp_widget_factory;
		remove_action( 'wp_head', array(
			$wp_widget_factory->widgets['WP_Widget_Recent_Comments'],
			'recent_comments_style'
		) );
	}

	add_action( 'widgets_init', 'dima_remove_recent_comments_style' );
endif;

/*------------------------------*/
# Remove Gallery Style
/*------------------------------*/
if ( ! function_exists( 'dima_remove_gallery_style' ) ) :
	function dima_remove_gallery_style() {
		add_filter( 'use_default_gallery_style', '__return_false' );
	}

	add_action( 'init', 'dima_remove_gallery_style' );
endif;

/*------------------------------*/
# Remove Gallery <br> Tags
/*------------------------------*/
if ( ! function_exists( 'dima_remove_br_gallery' ) ) :
	function dima_remove_br_gallery( $output ) {
		return preg_replace( '/<br style=(.*?)>/mi', '', $output );
	}

	add_filter( 'the_content', 'dima_remove_br_gallery', 11, 2 );
endif;

/*------------------------------*/
# Remove from excerpts where buttons could be redundant or awkward
/*------------------------------*/
if ( function_exists( 'ADDTOANY_SHARE_SAVE_KIT' ) ) {
	function dima_addtoany_excerpt_remove() {
		remove_action( 'pre_get_posts', 'A2A_SHARE_SAVE_pre_get_posts' );
	}

	add_action( 'init', 'dima_addtoany_excerpt_remove' );
}