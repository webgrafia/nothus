<?php
/**
 * Class dima_Breadcrumbs
 * Breadcrumb trails on a page indicate the page's
 *
 * @package Dima Framework
 * @subpackage Functions
 * @version   1.0.0
 * @since     1.0.0
 * @author    PixelDima <info@pixeldima.com>
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class DIMA_Breadcrumbs_Class {
	/**
	 * @var mixed Current post object
	 */

	private $post;
	/**
	 * @var boolean True to shown terms in breadcrumb
	 */
	private $show_terms;
	private $home_label;
	private $tag_archive_text;
	private $search_text;
	private $error_text;
	private $options;
	private $separator;
	private $html_output;
	private $breadcrumbs_array = array();

	/**
	 * Class Constructor
	 */
	public function __construct() {

		// Initialize
		$this->post    = ( isset( $GLOBALS['post'] ) ? $GLOBALS['post'] : null );
		$this->options = $this->dima_get_bc_options();

		// Setup default array for changeable variables
		$_options = array(
			'separator'        => $this->options['bc_separator'],
			'show_terms'       => $this->options['bc_show_categories'],
			'home_label'       => esc_html__( 'Home', 'noor' ),
			'tag_archive_text' => esc_html__( 'Tag:', 'noor' ),
			'search_text'      => esc_html__( 'Search:', 'noor' ),
			'error_text'       => esc_html__( '404 - Page not Found', 'noor' ),
		);

		$args     = apply_filters( 'dima_breadcrumbs_defaults', $_options );
		$_options = wp_parse_args( $args, $_options );

		$this->separator        = $_options['separator'];
		$this->show_terms       = $_options['show_terms'];
		$this->home_label       = $_options['home_label'];
		$this->tag_archive_text = $_options['tag_archive_text'];
		$this->search_text      = $_options['search_text'];
		$this->error_text       = $_options['error_text'];
	}

	/**
	 * get options from post meta and customizer.
	 * @return array
	 */
	public function dima_get_bc_options() {
		$breadcrumbs_position     = dima_helper::dima_get_inherit_option( '_dima_meta_breadcumbs_position', 'dima_breadcrumb_position' );
		$breadcrumbs_list_display = dima_helper::dima_am_i_true( dima_helper::dima_get_inherit_option( '_dima_meta_breadcumbs_list_display', 'dima_breadcrumb_display' ) );

		if ( $breadcrumbs_list_display ) {
			$breadcrumbs_list_display = 'on';
		} else {
			$breadcrumbs_list_display = 'off';
		}
		$breadcrumbs_display = dima_helper::dima_get_page_title_display();

		if ( $breadcrumbs_position != 'start' ) {
			$position = 'center';
		} else {
			$position = 'end';
		}
		$page_type = dima_helper::dima_get_page_type();

		$separator = wp_kses( dima_get_svg_icon( "ic_keyboard_arrow_right" ), dima_helper::dima_get_allowed_html_tag() );
		if ( is_rtl() ) {
			$separator = wp_kses( dima_get_svg_icon( "ic_keyboard_arrow_left" ), dima_helper::dima_get_allowed_html_tag() );
		}

		$data_options = array(
			'bc_separator'                    => $separator,
			'dima_blog_title'                 => get_bloginfo(),
			'bc_show_categories'              => 'true',
			'disable_date_rich_snippet_pages' => 'true',
			'dima_page_type_op'               => $page_type,
			'breadcrumbs_display'             => $breadcrumbs_display,
			'breadcrumbs_position'            => $breadcrumbs_position,
			'breadcumbs_list_display'         => $breadcrumbs_list_display,
			'position_class'                  => $position
		);

		return $data_options;
	}


	/**
	 * Return Page Title text ( Post title, page title ...)
	 *
	 * @param $post_id
	 *
	 * @return array
	 */
	public function dima_get_page_title_txt( $post_id ) {
		$this->options = $this->dima_get_bc_options();
		$subtitle      = esc_attr( get_post_meta( $post_id, '_dima_meta_breadcumbs_subtitle', true ) );
		$title         = get_the_title();
		switch ( true ) {
			case ( is_home() ):
				$page_for_posts = get_option( 'page_for_posts', true );
				if ( $page_for_posts ) {
					$title = esc_html( get_the_title( $page_for_posts ) );
				} else {
					$title = esc_html__( 'Latest Posts', 'noor' );
				}
				break;
			case ( is_search() ):
				$title = sprintf( esc_html__( 'Search results for: %s', 'noor' ), get_search_query() );
				break;
			case ( is_404() ):
				$title = esc_html__( 'Error 404 Page', 'noor' );
				break;
			case ( class_exists( 'WooCommerce' ) && ( dima_helper::dima_is_product() || dima_helper::dima_is_shop() ) && ! is_search() ):
				if ( ! dima_helper::dima_is_product() ) {
					$title = woocommerce_page_title( false );
					if ( $title == '' ) {
						$title = esc_html__( 'Shop', 'noor' );
					}
				}
				break;
			case dima_helper::dima_is_portfolio_home_page():
				$title = dima_helper::dima_get_option( 'dima_portfolio_page_name' );
				if ( $title == '' ) {
					$title = post_type_archive_title( '', false );
				}
				break;
			case ( class_exists( 'Tribe__Events__Main' ) ):
				if ( is_single() && tribe_is_past_event() && is_singular() ) {
					wp_reset_postdata();
					$title = get_the_title();
				} elseif ( dima_helper::dima_is_events_archive() ) {
					$title = tribe_get_events_title();
				}
				break;
			case ( is_archive() && ! dima_helper::dima_is_bbpress() && ! is_search() ):
				if ( is_day() ) {
					$title = sprintf( esc_html__( 'Daily Archives: %s', 'noor' ), '<span>' . get_the_date() . '</span>' );
				} elseif ( is_month() ) {
					$title = sprintf( esc_html__( 'Monthly Archives: %s', 'noor' ), '<span>' . get_the_date( _x( 'F Y', 'monthly archives date format', 'noor' ) ) . '</span>' );
				} elseif ( is_year() ) {
					$title = sprintf( esc_html__( 'Yearly Archives: %s', 'noor' ), '<span> ' . get_the_date( _x( 'Y', 'yearly archives date format', 'noor' ) ) . '</span>' );
				} elseif ( is_author() ) {
					$curauth = get_user_by( 'id', get_query_var( 'author' ) );
					$title   = $curauth->nickname;
				} elseif ( is_post_type_archive() ) {
					$title = post_type_archive_title( '', false );
				} else {
					$title = single_cat_title( '', false );
				}
				break;
			default:
				$title = get_the_title( $this->post->ID );
				break;
		}

		return array( $title, $subtitle );
	}

	/**
	 * Output Page title markup depand on the breadcrumbs style.
	 *
	 * @param $breadcrumbs_style
	 *
	 * @return string
	 */
	public function dima_get_title_breadcrumb_output(
		$breadcrumbs_style
	) {
		$post_id = get_queried_object_id();
		$title   = $this->dima_get_page_title_txt( $post_id );
		$float   = '';
		if ( $breadcrumbs_style !== 'center' ) {
			$float = " float-start";
		}

		return '<h1 class="header-title undertitle text-' . esc_attr( $breadcrumbs_style ) . $float . '">' . wp_kses( $title[0], dima_helper::dima_get_allowed_html_tag() ) . '</h1>';
	}

	/**
	 * Add Background for All Page Title with options like parallax scrolling and more.
	 *
	 * @return string
	 */
	function dima_get_background_breadcrumb() {
		$background_position  = dima_helper::dima_get_meta( '_dima_meta_breadcumbs_image_style' );
		$breadcrumbs_image_id = dima_helper::dima_get_inherit_option( "_dima_meta_breadcumbs_image", "dima_breadcrumb_background_image" );
		$data_src             = $calss = '';


		if ( is_front_page() && is_home() ) {
			$breadcrumbs_image_src = esc_attr( dima_helper::dima_get_option( 'dima_breadcrumb_background_image' ) );
		} else {

			if ( is_numeric( $breadcrumbs_image_id ) ) {
				$breadcrumbs_image     = wp_get_attachment_image_src( $breadcrumbs_image_id, 'full' );
				$breadcrumbs_image_src = $breadcrumbs_image[0];
			} else {
				$breadcrumbs_image_src = $breadcrumbs_image_id;
			}
		}

		if ( DIMA_USE_LAZY ) {
			$breadcrumbs_image_id = dima_helper::dima_get_inherit_option( "_dima_meta_breadcumbs_image", "dima_breadcrumb_background_image" );
			if ( is_numeric( $breadcrumbs_image_id ) ) {
				$poster_info           = wp_get_attachment_image_src( $breadcrumbs_image_id, 'dima-lazy-image' );
				$small_img             = $poster_info[0];
				$data_src              = 'data-src="' . $breadcrumbs_image_src . '"';
				$breadcrumbs_image_src = $small_img;
				$calss                 .= "js-lazy-image-css ";
			}
		}

		if ( ! empty( $breadcrumbs_image_src ) ) {
			switch ( $background_position ) {
				case 'fixed':
					$calss         .= "background-image-hide background-cover fixed-parallax ";
					$parallax_data = 'data-bg-image="' . esc_url( $breadcrumbs_image_src ) . '"';
					break;
				case 'parallax':
					$calss         .= "background-image-hide background-cover parallax-background";
					$parallax_data = 'data-bg-image="' . esc_url( $breadcrumbs_image_src ) . '"';
					break;
				default:
					$calss         .= "background-image-hide background-cover";
					$parallax_data = 'data-bg-image="' . esc_url( $breadcrumbs_image_src ) . '"';
					break;
			}

			$out = '';

			/* Cover */
			$is_coverd = dima_helper::dima_am_i_true( dima_helper::dima_get_inherit_option( '_dima_meta_page_title_bg_is_cover', 'dima_page_title_bg_is_cover' ) );
			if ( $is_coverd ) {
				$var_page_title_bg_color = dima_helper::dima_get_inherit_option( '_dima_meta_page_title_bg_color', 'dima_page_title_bg_color' );
				$out                     .= '<div class="dima-section-cover background-image-holder" data-bg-color="' . $var_page_title_bg_color . '"></div>';
			}
			/* !Cover */

			$out .= '<div class="' . esc_attr( $calss ) . '" ' . ( $parallax_data ) . ' ' . $data_src . ' >
                    </div>';

			return $out;
		} else {
			return '';
		}
	}

	/**
	 * Get the full breadcrumb HTML markup
	 * @return void
	 */
	public function dima_get_breadcrumbs_output() {
		$this->options = $this->dima_get_bc_options();
		$display       = $this->options['breadcrumbs_display'];
		if ( $display ) {
			// Get the Wordpres SEO options
			$options = get_option( 'wpseo_internallinks' );

			// Support for Yoast Breadcrumbs
			//class_exists( 'WPSEO_Breadcrumbs' )
			if ( function_exists( 'yoast_breadcrumb' ) &&
			     $options &&
			     $options['breadcrumbs-enable'] === true
			) {
				ob_start();
				yoast_breadcrumb();
				$this->html_output = ob_get_clean();
				// PixelDIma Breadcrumbs
			} else {
				$this->dima_initialize_bc_output();
			}

			$this->dima_external_breadcrumbs();

			echo( $this->html_output );
		}
	}

	public function dima_get_sub_title_breadcrumb_output(
		$breadcrumbs_style
	) {
		$post_id           = get_queried_object_id();
		$title             = $this->dima_get_page_title_txt( $post_id );
		$breadcrumbs_float = '';
		$out               = '';
		if ( ! empty( $title[1] ) ) {
			if ( $breadcrumbs_style == 'end' ) {
				$out .= '<hr>';
			}
			if ( $breadcrumbs_style != "center" ) {
				$breadcrumbs_style = "start";
				$breadcrumbs_float = "float-start";
			}

			$out .= '<span class="dima-subtitle undertitle text-' . esc_attr( $breadcrumbs_style ) . ' ' . esc_attr( $breadcrumbs_float ) . '">' . esc_attr( $title[1] ) . '</span>';

			return $out;
		} else {
			return '';
		}
	}

	public function dima_get_title_breadcrumb_list_output(
		$position,
		$html_output
	) {
		$bc_list           = $this->options['breadcumbs_list_display'];
		$breadcrumbs_float = '';
		if ( $bc_list != 'on' ) {
			return null;
		}
		if ( $position != "center" ) {
			$breadcrumbs_float = "float-end";
		}
		$allowed = array(
			'span' => array(
				'class' => array(),
				'value' => array(),
				'style' => array(),
			),
			'a'    => array(
				'href'        => array(),
				'class'       => array(),
				'style'       => array(),
				'title'       => array(),
				'data-filter' => array(),
			),
			'div'  => array(
				'class' => array(),
				'style' => array(),
			),
			'br'   => array(),
			'p'    => array(
				'class' => array(),
				'style' => array(),
			),
			'li'   => array(
				'class' => array(),
				'style' => array(),
			),
			'ol'   => array(
				'class' => array(),
				'style' => array(),
			),
			'ul'   => array(
				'class' => array(),
				'style' => array(),
			),
			'img'  => array(
				'class'  => array(),
				'id'     => array(),
				'style'  => array(),
				'alt'    => array(),
				'src'    => array(),
				'srcset' => array(),
				'height' => array(),
				'width'  => array(),
			),
			'i'    => array(
				'class' => array(),
				'style' => array(),
			),
			'svg'  => array(
				'width'       => array(),
				'height'      => array(),
				'viewbox'     => array(),
				'version'     => array(),
				'xmlns'       => array(),
				'xmlns:xlink' => array(),
			),
			'g'    => array(
				'stroke'       => array(),
				'stroke-width' => array(),
				'fill-rule'    => array(),
			),
			'path' => array(
				'd'  => array(),
				'id' => array(),
			),
		);
		$output  = '<nav id="breadcrumb" class="dima-breadcrumbs breadcrumbs-' . esc_attr( $position ) . ' text-' . esc_attr( $position ) . ' ' . esc_attr( $breadcrumbs_float ) . '">
						<span>
						' . wp_kses( $html_output, $allowed ) . '
						</span>
					 </nav>';

		return $output;
	}


	/**
	 * Initialize the markup of breadcrumb path
	 * @return void
	 */
	private function dima_initialize_bc_output() {
		# If the breadcrumbs is disabled --------------
		if ( ! dima_helper::dima_am_i_true( dima_helper::dima_get_inherit_option( '_dima_meta_breadcumbs_list_display', 'dima_breadcrumb_display' ) ) ) {
			return;
		}


		# breadcrumbs ----------

		# Home path  ----------
		$this->html_output .= $this->dima_get_bc_home();

		# woo path ----------
		if ( dima_helper::dima_is_woo() && ( ( is_woocommerce() && is_archive() && ! dima_helper::dima_is_shop() ) || is_cart() || is_checkout() || is_account_page() ) ) {
			$this->html_output .= $this->dima_get_woo_shop_page( true, true );
		}

		# bbPress path ----------
		if ( dima_helper::dima_is_bbpress() && ( bbp_is_topic_archive() || bbp_is_single_user() || bbp_is_search() ) ) {
			$this->html_output .= $this->dima_get_bbpress_main_archive_page();
		}

		if ( ! is_home() && ! is_front_page() || is_paged() ) {
			# All Single Posts and Pages
			if ( is_singular() ) {
				// Post doesn't have parents
				if ( isset( $this->post->post_parent ) && $this->post->post_parent == 0 ) {
					$this->html_output .= $this->dima_get_post_terms();
				} else {
					$this->html_output .= $this->dima_get_post_ancestors();
				}
				$this->html_output .= $this->dima_get_crumb_html();
			} else {
				/**
				 * Archives path
				 */
				if ( is_post_type_archive() ) {
					$this->html_output .= $this->dima_get_post_archive_output( false );
					if ( is_search() ) {
						$this->html_output .= $this->dima_get_crumb_html( 'search' );
					}
				} elseif ( is_tax() || is_tag() || is_category() ) {
					if ( is_tag() ) {
						$this->html_output .= $this->tag_archive_text;
					}
					$this->html_output .= $this->dima_get_taxonomies();
					$this->html_output .= $this->dima_get_crumb_html( 'term' );
				} elseif ( is_date() ) {
					global $wp_locale;
					$year  = esc_html( get_query_var( 'year' ) );
					$month = $month_name = '';
					if ( is_month() ||
					     is_day()
					) {
						$month      = get_query_var( 'monthnum' );
						$month_name = $wp_locale->get_month( $month );
					}
					if ( is_year() ) {
						$this->html_output .= $this->dima_get_crumb_html( 'year' );
					} elseif ( is_month() ) {
						$this->html_output .= $this->dima_get_single_markup( $year, get_year_link( $year ) );
						$this->html_output .= $this->dima_get_crumb_html( 'month' );
					} elseif ( is_day() ) {
						$this->html_output .= $this->dima_get_single_markup( $year, get_year_link( $year ) );
						$this->html_output .= $this->dima_get_single_markup( $month_name, get_month_link( $year, $month ) );
						$this->html_output .= $this->dima_get_crumb_html( 'day' );
					}
				} elseif ( is_author() ) {
					$this->html_output .= $this->dima_get_crumb_html( 'author' );
				} elseif ( is_search() ) {
					$this->html_output .= $this->dima_get_crumb_html( 'search' );
				} elseif ( is_404() ) {
					if ( dima_Helper::dima_tribe_is_event() || dima_Helper::dima_is_events_archive() ) {
						$this->html_output .= $this->dima_get_crumb_html( 'events' );
					} else {
						$this->html_output .= $this->dima_get_crumb_html( '404' );
					}
					//dima-noor 2.0
				} elseif ( class_exists( 'bbPress' ) ) {
					if ( bbp_is_search() ) {
						$this->html_output .= $this->dima_get_crumb_html( 'bbpress_search' );
					} elseif ( bbp_is_single_user() ) {
						$this->html_output .= $this->dima_get_crumb_html( 'bbpress_user' );
					}
				}
			}

			if ( ! empty( $this->breadcrumbs_array ) ) {
				$cunter             = 0;
				$breadcrumbs_schema = array(
					'@context'        => 'http://schema.org',
					'@type'           => 'BreadcrumbList',
					'@id'             => '#Breadcrumb',
					'itemListElement' => array(),
				);
				foreach ( $this->breadcrumbs_array as $item ) {
					$cunter ++;

					if ( empty( $item['url'] ) ) {
						global $wp;
						$item['url'] = esc_url( home_url( add_query_arg( array(), $wp->request ) ) );
					}

					$breadcrumbs_schema['itemListElement'][] = array(
						'@type'    => 'ListItem',
						'position' => $cunter,
						'item'     => array(
							'name' => str_replace( '<span class="fa fa-home" aria-hidden="true"></span> ', '', $item['name'] ),
							'@id'  => $item['url'],
						)
					);
				}
				if ( dima_helper::dima_get_option( 'dima_structure_data' ) ) {
					echo '<script type="application/ld+json">' . wp_json_encode( $breadcrumbs_schema ) . '</script>';
				}
			}
		}
	}

	/**
	 * External breadcrumb HTML markup
	 */
	private function dima_external_breadcrumbs() {


		if ( $this->options['breadcrumbs_position'] != 'start' ) {
			$position = 'center';
		} else {
			$position = 'end';
		}
		$transparent_output = '';
		if ( dima_is_transparent_navigation() ) {
			$transparent_output = 'dima-transparent-breadcrumbs ';
		}

		$sub       = $this->dima_get_sub_title_breadcrumb_output( $position );
		$sub_class = ( $sub == '' ) ? '' : 'with_sub_title ';
		if ( $position == 'center' ) {
			$this->html_output = '
			<div class="title_container ' . esc_attr( $sub_class ) . ' ' . esc_attr( $transparent_output ) . ' ' . $this->options['breadcrumbs_position'] . '-style' . '"' . '>
				<div class="page-section-content overflow-hidden">
					' . $this->dima_get_background_breadcrumb() . '
					<div class="container header-main-container">
						<div class="header-content">
						' . $this->dima_get_title_breadcrumb_output( $this->options['breadcrumbs_position'] ) . '
						' . ( $sub ) . '
						</div>
					</div>
					' . $this->dima_get_title_breadcrumb_list_output( $position, $this->html_output ) . '
				</div>
			</div>			
			';
		} else {
			$this->html_output = '			
			<div class="title_container ' . esc_attr( $sub_class ) . ' ' . esc_attr( sanitize_html_class( $transparent_output ) ) . ' ' . esc_attr( sanitize_html_class( $this->options['breadcrumbs_position'] . '-style' ) ) . '"' . '>
				<div class="page-section-content overflow-hidden">
					' . $this->dima_get_background_breadcrumb() . '
					<div class="container header-main-container">
						<div class="header-content">
						' . $this->dima_get_title_breadcrumb_output( $this->options['breadcrumbs_position'] ) . '
						' . ( $sub ) . '
						' . $this->dima_get_title_breadcrumb_list_output( $position, $this->html_output ) . '
						</div>
					</div>
				</div>
			</div>			
			';
		}
	}

	/**
	 * Get the markup of the "Home" Link
	 * @return string
	 */
	private function dima_get_bc_home() {
		$home_link = "";
		if ( ! is_front_page() ) {
			$home_link = $this->dima_get_single_markup( $this->home_label, get_home_url(), true );
		} elseif ( is_home() ) {
			$home_link = '';
		}

		return $home_link;
	}

	/**
	 * Construct the full post term tree path and add its HTML markup
	 */
	private function dima_get_post_terms() {
		$terms_markup = '';

		// Get the post terms
		if ( $this->post->post_type == 'post' ) {
			$taxonomy = 'category';
			// Dima Portfolio
		} elseif ( $this->post->post_type == 'dima-portfolio' ) {
			$taxonomy = 'portfolio-category';
			// Woocommerce
		} elseif ( $this->post->post_type == 'product' && class_exists( 'WooCommerce' ) ) {
			$taxonomy = 'product_cat';
			// For other post types don't return a terms tree to reduce possible errors
		} elseif ( 'tribe_events' == $this->post->post_type ) {
			// The Events Calendar.
			$taxonomy = 'tribe_events_cat';
		} else {
			return $terms_markup;
		}

		$terms = wp_get_object_terms( $this->post->ID, $taxonomy );

		// If post does not have any terms assigned; possible e.g. portfolio posts
		if ( empty( $terms ) ) {
			return $terms_markup;
		}

		// Check if the terms are all part of one term tree, i.e. only related terms are selected
		$terms_by_id = array();
		foreach ( $terms as $term ) {
			$terms_by_id[ $term->term_id ] = $term;
		}

		// Unset all terms that are parents of some term
		foreach ( $terms as $term ) {
			unset( $terms_by_id[ $term->parent ] );
		}

		// If only one term is left, we have a single term tree
		if ( count( $terms_by_id ) == 1 ) {
			unset( $terms );
			$terms[0] = array_shift( $terms_by_id );
		}

		// The post is only in one term
		if ( count( $terms ) == 1 ) {
			$term_parent = $terms[0]->parent;

			// If the term has a parent we need its ancestors for a full tree
			if ( $term_parent ) {
				// Get space separated string of term tree in slugs
				$term_tree   = get_ancestors( $terms[0]->term_id, $taxonomy );
				$term_tree   = array_reverse( $term_tree );
				$term_tree[] = get_term( $terms[0]->term_id, $taxonomy );

				// Loop through the term tree
				foreach ( $term_tree as $term_id ) {
					// Get the term object by its slug
					$term_object = get_term( $term_id, $taxonomy );

					// Add it to the term breadcrumb markup string
					$terms_markup .= $this->dima_get_single_markup( $term_object->name, get_term_link( $term_object ) );
				}
				// We have a single term, so put it out
			} else {
				$terms_markup = $this->dima_get_single_markup( $terms[0]->name, get_term_link( $terms[0] ) );
			}
			// The post has multiple terms
		} else {
			// Drop the first index
			array_shift( $terms );
			// Loop through the rest of the terms, and add them to string comma separated
			$max_index = count( $terms );
			$i         = 0;

			// The lexicographically smallest term will be part of the breadcrump rich snippet path
			$terms_markup = $this->dima_get_single_markup( $terms[0]->name, get_term_link( $terms[0] ), false );
			if ( $max_index > 3 ) {
				$terms_markup .= '<span>,</span> ' . $this->dima_get_single_markup( $terms[1]->name . ' ...', get_term_link( $terms[1] ), true );
			} else {
				foreach ( $terms as $term ) {
					// For the last index also add the separator
					if ( ++ $i == $max_index ) {
						$terms_markup .= '<span>,</span> ' . $this->dima_get_single_markup( $term->name, get_term_link( $term ), true );
					} else {
						$terms_markup .= '<span>,</span> ' . $this->dima_get_single_markup( $term->name, get_term_link( $term ), false );
					}
				}
			}
		}

		return $terms_markup;
	}


	/**
	 * Get the full post of ancestors tree path.
	 * @return string : markup of ancestors tree
	 */
	private function dima_get_post_ancestors() {
		$ancestors_tree = '';

		// Array of IDs or empty if no ancestors are found.
		$post_ids = array_reverse( get_post_ancestors( $this->post ) );

		// get the full ancestors tree
		foreach ( $post_ids as $post_id ) {
			$post           = get_post( $post_id );
			$ancestors_tree .= $this->dima_get_single_markup( $post->post_title, get_permalink( $post->ID ) );
		}

		return $ancestors_tree;
	}

	/**
	 * Construct the full term ancestors tree path and add its HTML markup
	 * @return string The HTML markup of the term ancestors tree
	 */
	private function dima_get_taxonomies() {
		global $wp_query;
		//get the currently-queried object
		$term_opject  = $wp_query->get_queried_object();
		$terms_markup = '';

		if ( $term_opject->parent != 0 &&
		     is_taxonomy_hierarchical( $term_opject->taxonomy )
		) {
			$term_ancestors = get_ancestors( $term_opject->term_id, $term_opject->taxonomy );
			$term_ancestors = array_reverse( $term_ancestors );
			// Get the full tree
			foreach ( $term_ancestors as $term_ancestor ) {
				$term_object  = get_term( $term_ancestor, $term_opject->taxonomy );
				$terms_markup .= $this->dima_get_single_markup( $term_object->name, get_term_link( $term_object->term_id, $term_opject->taxonomy ) );
			}
		}

		return $terms_markup;
	}

	/**
	 * Get markup of a post type archive
	 *
	 * @param bool $linked
	 *
	 * @return string :HTML markup
	 */
	private function dima_get_post_archive_output(
		$linked = true
	) {
		global $wp_query;

		$post_type        = $wp_query->query_vars['post_type'];
		$post_type_object = get_post_type_object( $post_type );
		$link             = $archive_title = '';

		if ( is_object( $post_type_object ) ) {
			if ( $post_type == 'product' && $woocommerce_shop_page = $this->dima_get_woo_shop_page( $linked, false ) ) {
				return $woocommerce_shop_page;
			}

			// bbPress: dima-noor 2.0
			if ( class_exists( 'bbPress' ) &&
			     $post_type == 'topic'
			) {
				$archive_title = bbp_get_forum_archive_title();
				if ( $linked ) {
					$link = get_post_type_archive_link( bbp_get_forum_post_type() );
				}

				return $this->dima_get_single_markup( $archive_title, $link );
			}

			if ( isset( $post_type_object->label ) &&
			     $post_type_object->label !== ''
			) {
				$archive_title = $post_type_object->label;
			} elseif ( isset( $post_type_object->labels->menu_name ) &&
			           $post_type_object->labels->menu_name !== ''
			) {
				$archive_title = $post_type_object->labels->menu_name;
			} else {
				$archive_title = $post_type_object->name;
			}
		}

		// linked breadcrumb
		if ( $linked ) {
			$link = get_post_type_archive_link( $post_type );
		}

		return $this->dima_get_single_markup( $archive_title, $link );
	}

	/**
	 * Adds  markup for woocommerce shop page
	 *
	 * @param bool $linked
	 * @param bool $sp
	 *
	 * @return string
	 */
	private function dima_get_woo_shop_page(
		$linked = true,
		$sp = true
	) {
		$post_type        = 'product';
		$post_type_object = get_post_type_object( $post_type );
		$woo_page_markup  = $link = '';
		// if it's woocommerce page

		if ( is_object( $post_type_object ) && dima_helper::dima_is_woo() && dima_helper::dima_is_woo_pages()
		) {
			// Get shop page name
			$shop_page_name = wc_get_page_id( 'shop' ) ? get_the_title( wc_get_page_id( 'shop' ) ) : '';

			if ( ! $shop_page_name ) {
				$shop_page_name = $post_type_object->labels->name;
			}

			// linked breadcrumb
			if ( $linked ) {
				$link = get_post_type_archive_link( $post_type );
			}

			$woo_page_markup = $this->dima_get_single_markup( $shop_page_name, $link, $sp );
		}

		return $woo_page_markup;
	}


	/**
	 * Adds the markup of the breadcrumb leaf
	 *
	 * @param string $type
	 *
	 * @return string
	 */
	private function dima_get_crumb_html(
		$type = ''
	) {
		global $wp_query, $wp_locale;
		switch ( $type ) {
			case 'term':
				$term  = $wp_query->get_queried_object();
				$title = $term->name;
				break;
			case 'day':
				$title = get_query_var( 'day' );
				break;
			case 'month':
				$title = $wp_locale->get_month( get_query_var( 'monthnum' ) );
				break;
			case 'year':
				$title = esc_html( get_query_var( 'year' ) );
				break;
			case 'author':
				$user  = $wp_query->get_queried_object();
				$title = $user->display_name;
				break;
			case 'search':
				$title = sprintf( '%s %s', $this->search_text, esc_html( get_search_query() ) );
				break;
			case '404':
				$title = $this->error_text;
				break;
			/* dima-noor 2.0*/
			case 'bbpress_search':
				$title = sprintf( '%s %s', $this->search_text, esc_html( get_query_var( 'bbp_search' ) ) );
				break;
			case 'bbpress_user':
				$current_user_id = bbp_get_user_id( 0, true, false );
				$current_user    = get_userdata( $current_user_id );
				$title           = $current_user->display_name;
				break;
			case 'events':
				$title = tribe_get_events_title();
				break;
			default:
				wp_reset_postdata();
				$title = get_the_title();
				break;
		}

		return sprintf( '<span class="breadcrumb-leaf">%s</span>', $title );
	}

	/**
	 * Adds the markup of a single breadcrumb
	 *
	 * @param        $title : The title that should be displayed
	 * @param string $link : The URL of the breadcrumb
	 * @param bool   $separator :Set to TRUE to show the separator at the end of the breadcrumb
	 *
	 * @return string
	 */
	private function dima_get_single_markup(
		$title,
		$link = '',
		$separator = true
	) {

		$this->breadcrumbs_array[] = array(
			'url'  => $link,
			'name' => $title,
		);

		$separator_markup = '';
		$bc_content       = sprintf( '<span>%s</span>', $title );
		if ( $link ) {
			$bc_content = sprintf( '<a href="' . esc_url( rawurldecode( $link ) ) . '">%s</a>', $bc_content );
		}
		if ( $separator ) {
			$separator_markup = sprintf( '<span class="sep">%s</span>', $this->separator );
		}

		return sprintf( '<span>%s</span>%s', $bc_content, $separator_markup );
	}

	/**
	 * (dima-noor 2.0)
	 * Adds the markup of the bbpress main forum archive
	 * @return string The HTML markup of the bbpress main forum archive
	 */
	private function dima_get_bbpress_main_archive_page() {
		return $this->dima_get_single_markup( bbp_get_forum_archive_title(), get_post_type_archive_link( 'forum' ) );
	}
}

if ( ! function_exists( 'dima_breadcrumbs' ) ) {
	/**
	 * Render the breadcrumbs
	 * @return void
	 */

	function dima_breadcrumbs() {
		$breadcrumbs = new DIMA_Breadcrumbs_Class();
		$breadcrumbs->dima_get_breadcrumbs_output();
	}
}