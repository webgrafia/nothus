<?php
/**
 * Functions pertaining to content output.
 *
 * @package Dima Framework
 * @subpackage Functions
 * @version   1.0.0
 * @since     1.0.0
 * @author    PixelDima <info@pixeldima.com>
 *
 */

if ( ! function_exists( 'dima_hide_entry_meta' ) ) {
	/**
	 * Returns true if a statement is met where displaying the entry meta data is not desirable.
	 *
	 * @param $show_meta
	 *
	 * @return bool
	 */
	function dima_hide_entry_meta( $show_meta ) {
		return $show_meta;
	}
}

/**
 * Link Pages
 */
if ( ! function_exists( 'dima_link_pages' ) ) {
	function dima_link_pages() {
		$defaults = array(
			'before'      => '<div class="page-links clearfix"><span class="page-links-title">' . esc_html__( 'Pages:', 'noor' ) . '</span>',
			'after'       => '</div>',
			'link_before' => '<span class="page-number">',
			'link_after'  => '</span>'
		);
		// Displays page-links for paginated posts (i.e. includes the <!--nextpage--> Quicktag one or more times)
		wp_link_pages( $defaults );
	}
}

/**
 * Entry Navigation
 */
if ( ! function_exists( 'dima_post_navigation' ) ) {

	function dima_post_navigation() {
		$left_icon      = dima_get_svg_icon( 'ic_arrow_forward' );
		$right_icon     = dima_get_svg_icon( 'ic_arrow_back' );
		$is_ltr         = ! is_rtl();
		$prev_post      = get_adjacent_post( false, '', false );
		$next_post      = get_adjacent_post( false, '', true );
		$prev_icon_html = ( $is_ltr ) ? $left_icon : $right_icon;
		$next_icon_html = ( $is_ltr ) ? $right_icon : $left_icon;
		$home_url       = dima_helper::dima_get_home_page_url();

		if ( dima_helper::dima_is_product() ) {
			$next     = esc_html__( 'Next Product', 'noor' );
			$no_next  = esc_html__( 'No Next Product', 'noor' );
			$prev     = esc_html__( 'Previous Product', 'noor' );
			$no_prev  = esc_html__( 'No Previous Product', 'noor' );
			$home_txt = esc_html__( 'Shop', 'noor' );
		} else {
			$next     = esc_html__( 'Next Post', 'noor' );
			$no_next  = esc_html__( 'No Next Post', 'noor' );
			$prev     = esc_html__( 'Previous Post', 'noor' );
			$no_prev  = esc_html__( 'No Previous Post', 'noor' );
			$home_txt = esc_html__( 'Back', 'noor' );
		}

		if ( dima_helper::dima_get_inherit_option( '_dima_meta_pagination_post', 'dima_pagination_post' ) ) { ?>
            <div class="container dima_post_pagination">
                <div class="nav-reveal ok-row">
                    <div class="nav-reveal-prev ok-md-4">
						<?php if ( $prev_post ) {
							?>
                            <a href="<?php echo get_permalink( $prev_post ); ?>">
                                <span><?php echo esc_html( $prev ) ?></span>
								<?php echo wp_kses( $next_icon_html, dima_helper::dima_get_allowed_html_tag() ); ?>
                            </a>
							<?php
						} else {
							?>
                            <div class="icon-off">
                                <span><?php echo esc_html( $no_prev ) ?></span>
								<?php echo wp_kses( $next_icon_html, dima_helper::dima_get_allowed_html_tag() ); ?>
                            </div>
							<?php
						}
						?>
                    </div>

                    <div class="center_link ok-md-4">
                        <a href="<?php echo esc_url( $home_url ) ?>">
                            <span><?php echo esc_html( $home_txt ) ?></span>
							<?php echo wp_kses( dima_get_svg_icon( 'ic_apps' ), dima_helper::dima_get_allowed_html_tag() ); ?>
                        </a>
                    </div>

                    <div class="nav-reveal-next ok-md-4">
						<?php
						if ( $next_post ) {
							?>
                            <a href="<?php echo get_permalink( $next_post ); ?>">
                                <span><?php echo esc_html( $next ) ?></span>
								<?php echo wp_kses( $prev_icon_html, dima_helper::dima_get_allowed_html_tag() ); ?>
                            </a>
							<?php
						} else {
							?>
                            <div class="icon-off">
                                <span><?php echo esc_html( $no_next ) ?></span>
								<?php echo wp_kses( $prev_icon_html, dima_helper::dima_get_allowed_html_tag() ); ?>
                            </div>
							<?php
						} ?>
                    </div>
                </div>
            </div>
			<?php
		}
	}
}

/**
 * Post Content
 */
if ( ! function_exists( 'dima_get_post_content' ) ) {
	/**
	 * @param     $is_full_post_content
	 * @param int $excerpt_length
	 *
	 * @return array|mixed|string
	 */
	function dima_get_post_content( $is_full_post_content, $excerpt_length = 55 ) {
		$content_excerpted = false;

		if ( $is_full_post_content != '1' ) {
			$content_excerpted = true;
		}

		if ( is_singular() && ! is_page() ) {
			$content_excerpted = false;
		}

		if ( $content_excerpted ) {
			dima_get_post_content_excerpt( $excerpt_length );
		} else {
			dima_helper::dima_get_view( 'dima_global', 'content' );
		}
	}
}

if ( ! function_exists( 'dima_get_content_stripped_and_excerpted' ) ) {
	/**
	 * Helper function
	 *
	 * @param $excerpt_length
	 * @param $content
	 *
	 * @return array|mixed|string|void
	 */
	function dima_get_content_stripped_and_excerpted( $excerpt_length, $content ) {
		$content = apply_filters( 'dima_exclude_content', $content );
		$content = explode( ' ', $content, $excerpt_length + 1 );

		if ( $excerpt_length < count( $content ) ) {
			array_pop( $content );
		}

		$content = implode( ' ', $content );
		$content = preg_replace( '~(?:\[/?)[^/\]]+/?\]~s', '', $content ); // Strip shortcodes and keep the content.
		$content = str_replace( ']]>', ']]&gt;', $content );
		$content = strip_tags( $content );
		$content = str_replace( array( '"', "'" ), array( '&quot;', '&#39;' ), $content );
		$content = trim( $content );

		return $content;
	}
}

/*------------------------------*/
# custom excerpt length
/*------------------------------*/
if ( ! function_exists( 'dima_get_post_content_excerpt' ) ) {
	/**
	 * @param $limit
	 *
	 * @return array|mixed|string
	 */
	function dima_get_post_content_excerpt( $limit = 150 ) {
		$custom_read_more = false;
		$post             = get_post( get_the_ID() );
		$more_tag         = strpos( $post->post_content, '<!--more-->' );
		$more_text        = apply_filters( 'dima_blog_read_more_excerpt', '...' );
		$more_icon        = apply_filters( 'dima_blog_read_more_icon', '<a class="read-more-icon" href="' . get_post_permalink( get_the_ID() ) . '">' . dima_get_svg_icon( "ic_more_horiz" ) . '</a>' );
		$limit            = intval( $limit );

		if ( $limit === 0 ) {
			return '';
		}
		$read_more = $more_text;
		$content   = wp_strip_all_tags( get_the_content( $read_more ) );

		$content = dima_get_content_stripped_and_excerpted( $limit, $content );

		if ( $post->post_excerpt || $more_tag !== false ) {
			if ( ! $more_tag ) {
				$content = wp_strip_all_tags( rtrim( get_the_excerpt(), '[&hellip;]' ) );
			}
			$custom_read_more = true;
		}

		if ( ! DIMA_NOUR_ASSISTANT_IS_ACTIVE ) {
			the_excerpt();
		} else if ( $content && $custom_read_more == false ) {

			$content = explode( ' ', $content, $limit );
			if ( count( $content ) >= $limit ) {
				array_pop( $content );
				$content = implode( ' ', $content );

				if ( $limit != 0 ) {
					$content .= $read_more;
					$content .= $more_icon;
				}
			} else {
				$content = implode( ' ', $content );
			}
			echo( $content );
		}

		// If we have a custom excerpt <!--more--> tag
		if ( $custom_read_more == true ) {
			//remove ShortCode ''
			$content = str_replace( '/\[(.*?)\]/i', '', $content );
			echo apply_filters( 'the_content', $content );
		}

		return ( $content );
	}
}

/**
 * Get Post icon ( edit and play and url )
 */
if ( ! function_exists( 'dima_get_post_icon' ) ) :
	function dima_get_post_icon( $format, $url ) {
		switch ( $format ) {
			case "video":
				$_ID       = get_the_ID();
				$video_url = get_post_meta( $_ID, '_dima_video_online', true );

				if ( $video_url != '' ) {
					$url_ = '<a data-lightbox="iframe" data-effect="mfp-zoom-in" class="mfp-figure format-' . esc_attr( $format ) . '" href="' . esc_url( $video_url ) . '" title="' . esc_attr( sprintf( esc_html__( 'Permalink to: %s', 'noor' ), the_title_attribute( 'echo=0' ) ) ) . '">
                                ' . dima_get_svg_format( $format ) . '
                            </a>';
				} else {
					$url_ = '<a class="format-' . esc_attr( $format ) . '" href="' . esc_url( $url ) . '" title="' . esc_attr( sprintf( esc_html__( 'Permalink to: %s', 'noor' ), the_title_attribute( 'echo=0' ) ) ) . '">
                                ' . dima_get_svg_format( $format ) . '
                            </a>';
				}
				$output = '<ul class="icons-media">
                        <li class="dima_go_' . esc_attr( $format ) . '">
                         ' . wp_kses( $url_, dima_helper::dima_get_allowed_html_tag() ) . '   
                        </li>
                        <li class="icon_edit">
                            ' . dima_helper::dima_get_admin_edit() . '
                        </li>
                        </ul>';
				break;
			default:
				$output = '<ul class="icons-media">
                        <li class="dima_go_' . esc_attr( $format ) . '">
                            <a class="format-' . esc_attr( $format ) . '" href="' . esc_url( $url ) . '" title="' . esc_attr( sprintf( esc_html__( 'Permalink to: %s', 'noor' ), the_title_attribute( 'echo=0' ) ) ) . '">
                                ' . dima_get_svg_format( $format ) . '
                            </a>
                        </li>
                        <li class="icon_edit">
                            ' . dima_helper::dima_get_admin_edit() . '
                        </li>
                        </ul>';
				break;
		}

		return $output;
	}

endif;

/*------------------------------------------------------------*/
# Default shortcodes
/*------------------------------------------------------------*/

/**
 * Used by index.php.
 * Displays blog depends on the use options.
 * Test if the NOOR ASSISTANT plugin existe.
 *
 * @param bool $search
 */
function dima_get_blog_shortcode_theme( $search = false ) {
	$style          = esc_attr( dima_helper::dima_get_option( 'dima_blog_style' ) );
	$columns        = esc_attr( dima_helper::dima_get_option( 'dima_blog_masonry_columns' ) );
	$img_hover      = esc_attr( dima_helper::dima_get_option( 'dima_img_hover' ) );
	$elm_hover      = esc_attr( dima_helper::dima_get_option( 'dima_elm_hover' ) );
	$excerpt        = esc_attr( dima_helper::dima_get_option( 'dima_blog_blog_excerpt' ) );
	$featured_image = esc_attr( dima_helper::dima_get_option( 'dima_blog_enable_featured_image' ) ) == '1' ? 'true' : '';
	$count          = get_option( 'posts_per_page ' );
	$blog_meta      = esc_attr( dima_helper::dima_get_option( 'dima_blog_enable_post_meta' ) );
	$show_meta      = ( $blog_meta == '1' ) ? 'true' : 'false';
	$template       = esc_attr( dima_helper::dima_get_template() );

	/*------------------------------*/
	# Use ASSISTANT.
	/*------------------------------*/
	if ( DIMA_NOUR_ASSISTANT_IS_ACTIVE ) {
		if ( $search ) {
			echo do_shortcode( '[blog blog_style="search" show_meta="' . esc_attr( $show_meta ) . '" elm_hover="' . esc_attr( $elm_hover ) . '" img_hover="' . esc_attr( $img_hover ) . '" words="' . esc_attr( $excerpt ) . '" column="' . esc_attr( $columns ) . '" count="' . esc_attr( $count ) . '" paging="true"  show_image="' . esc_attr( $featured_image ) . '"]' );
		} else {
			echo do_shortcode( '[blog blog_style="' . esc_attr( $style ) . '" show_meta="' . esc_attr( $show_meta ) . '" elm_hover="' . esc_attr( $elm_hover ) . '" img_hover="' . esc_attr( $img_hover ) . '" words="' . esc_attr( $excerpt ) . '" column="' . esc_attr( $columns ) . '" count="' . esc_attr( $count ) . '" paging="true" show_image="' . esc_attr( $featured_image ) . '"]' );
		}
	} else {
		$ARG_ARRAY = array(
			'show_meta'  => $show_meta,
			'show_image' => "true",
			'no_border'  => "",
			'elm_hover'  => $elm_hover,
			'img_hover'  => $img_hover,
			'words'      => $excerpt,
			'blog_style' => $style,
			'post_class' => "",
		);
		?>
        <div class="boxed-blog blog-list dima-layout-standard"><?php
			if ( have_posts() ) {
				while ( have_posts() ):
					the_post();
					dima_helper::dima_get_view_with_args( $template, 'content', get_post_format(), $ARG_ARRAY );
				endwhile;
				wp_reset_postdata();
			} else {
				dima_helper::dima_get_view( 'global', '_content-none' );
			}
			?>
        </div>

		<?php
		global $wp_query;
		dima_pagination( $wp_query );
	}
}

/**
 * Used bu about-the-author
 *
 * @param $atts
 *
 * @return string
 */
function dima_get_shortcode_author_theme( $atts ) {
	extract( shortcode_atts( array(
		'id'        => '',
		'class'     => '',
		'style'     => '',
		'title'     => '',
		'author_id' => ''
	), $atts ) );

	$id        = ( $id != '' ) ? 'id = "' . esc_attr( $id ) . '"' : '';
	$class     = ( $class != '' ) ? 'dima-author-box clearfix ' . esc_attr( $class ) : 'dima-author-box clearfix';
	$style     = ( $style != '' ) ? 'style = "' . esc_attr( $style ) . '"' : '';
	$title     = ( $title != '' ) ? $title : esc_html__( 'Author: ', 'noor' );
	$author_id = ( $author_id != '' ) ? $author_id : get_the_author_meta( 'ID' );

	$description  = get_the_author_meta( 'description', $author_id );
	$display_name = get_the_author_meta( 'display_name', $author_id );
	$facebook     = get_the_author_meta( 'facebook', $author_id );
	$instagram    = get_the_author_meta( 'instagram', $author_id );
	$linkedin     = get_the_author_meta( 'linkedin', $author_id );
	$twitter      = get_the_author_meta( 'twitter', $author_id );
	$googleplus   = get_the_author_meta( 'googleplus', $author_id );
	$flickr       = get_the_author_meta( 'flickr', $author_id );
	$youtube      = get_the_author_meta( 'youtube', $author_id );
	$pinterest    = get_the_author_meta( 'pinterest', $author_id );
	$behance      = get_the_author_meta( 'behance', $author_id );
	$dribbble     = get_the_author_meta( 'dribbble', $author_id );

	$facebook_output   = ( $facebook ) ? "<li><a href=\"{$facebook}\"  title=\"" . esc_attr__( 'Visit the Facebook Profile for', 'noor' ) . " {$display_name}\" target=\"_blank\"><i class=\"fa fa-facebook\"></i></a></li>" : '';
	$twitter_output    = ( $twitter ) ? "<li><a href=\"{$twitter}\"  title=\"" . esc_attr__( 'Visit the Twitter Profile for', 'noor' ) . " {$display_name}\" target=\"_blank\"><i class=\"fa fa-twitter\"></i></a></li>" : '';
	$googleplus_output = ( $googleplus ) ? "<li><a href=\"{$googleplus}\"  title=\"" . esc_attr__( 'Visit the Google+ Profile for', 'noor' ) . " {$display_name}\" target=\"_blank\"><i class=\"fa fa-google-plus\"></i></a></li>" : '';
	$instagram_output  = ( $instagram ) ? "<li><a href=\"{$instagram}\"  title=\"" . esc_attr__( 'Visit the instagram Profile for', 'noor' ) . " {$display_name}\" target=\"_blank\"><i class=\"fa fa-instagram\"></i></a></li>" : '';
	$linkedin_output   = ( $linkedin ) ? "<li><a href=\"{$linkedin}\"  title=\"" . esc_attr__( 'Visit the linkedin Profile for', 'noor' ) . " {$display_name}\" target=\"_blank\"><i class=\"fa fa-linkedin\"></i></a></li>" : '';
	$flickr_output     = ( $flickr ) ? "<li><a href=\"{$flickr}\"  title=\"" . esc_attr__( 'Visit the flickr Profile for', 'noor' ) . " {$display_name}\" target=\"_blank\"><i class=\"fa fa-flickr\"></i></a></li>" : '';
	$youtube_output    = ( $youtube ) ? "<li><a href=\"{$youtube}\"  title=\"" . esc_attr__( 'Visit the youtube Profile for', 'noor' ) . " {$display_name}\" target=\"_blank\"><i class=\"fa fa-youtube\"></i></a></li>" : '';
	$pinterest_output  = ( $pinterest ) ? "<li><a href=\"{$pinterest}\"  title=\"" . esc_attr__( 'Visit the pinterest Profile for', 'noor' ) . " {$display_name}\" target=\"_blank\"><i class=\"fa fa-pinterest\"></i></a></li>" : '';
	$behance_output    = ( $behance ) ? "<li><a href=\"{$behance}\"  title=\"" . esc_attr__( 'Visit the behance Profile for', 'noor' ) . " {$display_name}\" target=\"_blank\"><i class=\"fa fa-behance\"></i></a></li>" : '';
	$dribbble_output   = ( $dribbble ) ? "<li><a href=\"{$dribbble}\"  title=\"" . esc_attr__( 'Visit the dribbble Profile for', 'noor' ) . " {$display_name}\" target=\"_blank\"><i class=\"fa fa-dribbble\"></i></a></li>" : '';

	if ( empty( $description ) ) {
		return '';
	}

	$output = "<div {$id} class=\"{$class}\" {$style}>"
	          . "<div class=\"dima-about-image circle\">"
	          . get_avatar( $author_id, 150 )
	          . "</div>"
	          . "<div class=\"dima-author-info\">"
	          . "<h5 class=\"dima-author-name\">{$title}<a href=\"" . get_author_posts_url( get_the_author_meta( 'ID' ) ) . "\">{$display_name}</a></h5>"
	          . "<p>{$description}</p>"
	          . "<div class=\"social-media text-start fill-icon dima_add_hover social-small circle-social\">"
	          . "<ul class=\"inline clearfix\">"
	          . $facebook_output
	          . $twitter_output
	          . $googleplus_output
	          . $instagram_output
	          . $linkedin_output
	          . $flickr_output
	          . $youtube_output
	          . $pinterest_output
	          . $behance_output
	          . $dribbble_output
	          . "</ul>"
	          . "</div>"
	          . "</div>"
	          . "</div>";

	return $output;
}