<?php
/**
 * Output a featured Image bsed on post type.
 *
 * @package Dima Framework
 * @subpackage Functions
 * @version   1.0.0
 * @since     1.0.0
 * @author    PixelDima <info@pixeldima.com>
 *
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/*------------------------------*/
# Helper
/*------------------------------*/

/**
 * Output Portfolio Item Project Link
 */
function dima_portfolio_item_project_link() {
	$project_link = dima_helper::dima_get_meta( "_dima_portfolio_external_link_url" );
	if ( $project_link ) {
		return $project_link;
	} else {
		return '';
	}
}

if ( ! function_exists( 'dima_featured_image' ) ) :
	function dima_featured_image( $args = array() ) {
		$default_args = array(
			'post_type' => '',
			'img_hover' => '',
			'elm_hover' => '',
			'class'     => array(),
		);

		$args = wp_parse_args( $args, $default_args );

		if ( has_post_thumbnail() ) {
			$thumb     = dima_helper::dima_get_thumb( $args );
			$show_thum = dima_helper::dima_am_i_true( dima_helper::dima_get_meta( '_dima_post_featured_Image' ) );

			if ( ( is_singular() && ! is_page() ) && $show_thum ) {
				return;
			} elseif ( is_singular() && ! is_page() ) {
				$thumb_caption = get_post( get_post_thumbnail_id() );
				$caption       = '';
				if ( ! empty( $thumb_caption->post_excerpt ) ) {
					$caption = '
							<figcaption class="single-caption-text">
							' . $thumb_caption->post_excerpt . '
							</figcaption>
						';
				}
				printf( '<div class="post-img"><div class="entry-thumb">%1s%2s</div></div>', $thumb, $caption );
			} else {
				/**
				 * Image Hover Style
				 */
				switch ( $args['img_hover'] ) {
					case 'op_vc_zoom-out':
						$args['img_hover'] = " effect-roxy";
						break;
					case 'op_vc_zoom-in':
						$args['img_hover'] = " effect-julia";
						break;
					case 'op_vc_gray':
						$args['img_hover'] = " apply-gray";
						break;
					case 'op_vc_opacity':
						$args['img_hover'] = " apply-opacity";
						break;
					case 'op_vc_none':
					default:
						$args['img_hover'] = " post-feature";
						break;
				}
				/**
				 * Element Hover
				 */
				$format = get_post_format() ? get_post_format() : 'standard';
				$icon   = wp_kses( dima_get_post_icon( $format, get_permalink() ), dima_helper::dima_get_allowed_html_tag() );

				switch ( $args['elm_hover'] ) {
					case 'op_vc_none':
					case 'none':
						$hover = '<div class="post-img"><div class="' . esc_attr( $args['img_hover'] ) . '"><a href="%1$s">%3$s</a></div></div>';
						break;
					case 'op_vc_inside':
					case 'inside':
						$hover = '<div class="post-img dima_go_inside">
 						<div class="post-icon link_overlay">%4$s</div>
                        <div class="' . esc_attr( $args['img_hover'] ) . '">
                            <a href="%1$s" class="entry-thumb" title="%2$s">%3$s</a>
                         </div>
                        </div> ';
						break;
					default:
						$hover = '<div class="post-img">
 						<div class="post-icon link_overlay">%4$s</div>
                        <div class="' . esc_attr( $args['img_hover'] ) . '">
                            <a href="%1$s" class="entry-thumb" title="%2$s">%3$s</a>
                         </div>
                        </div> ';
				}

				printf( $hover,
					esc_url( get_permalink() ),
					esc_attr( sprintf( esc_html__( 'Permalink to: %s', 'noor' ), the_title_attribute( 'echo=0' ) ) ),
					$thumb,
					$icon
				);
			}
		}
	}
endif;

if ( ! function_exists( 'dima_featured_gallery' ) ) :
	function dima_featured_gallery( $args = array() ) {
		$default_args = array(
			'post_type' => '',
			'class'     => array(),
		);

		$args = wp_parse_args( $args, $default_args );

		$_ID = get_the_ID();

		$Animation      = esc_attr( get_post_meta( $_ID, '_dima_flex_slide_animation', true ) );
		$SlideshowSpeed = esc_attr( get_post_meta( $_ID, '_dima_flex_slide_show_speed', true ) );
		$AnimationSpeed = esc_attr( get_post_meta( $_ID, '_dima_flex_slide_animation_speed', true ) );
		$ControlNav     = esc_attr( get_post_meta( $_ID, '_dima_flex_slide_control_nav', true ) );
		$Gallery_List   = get_post_meta( $_ID, '_post_image_gallery', true );
		$attachments    = $Gallery_List;

		$js_data = array(
			'dots'           => ( $ControlNav == 'on' ),
			'autoplay'       => false,
			'arrows'         => true,
			'infinite'       => true,
			'fade'           => ( $Animation == 'fade' ),
			'draggable'      => true,
			'adaptiveHeight' => true,
			'mobileFirst'    => true,
			'pauseOnHover'   => true,
			'slidesToShow'   => 1,
			'slidesToScroll' => 1,
			'speed'          => $SlideshowSpeed,
			'autoplaySpeed'  => $AnimationSpeed,
			'rtl'            => is_rtl()
		);
		if ( DIMA_NOUR_ASSISTANT_IS_ACTIVE ) {
			$data = dima_creat_data_attributes( 'slick_slider', $js_data );
		} else {
			$data = '';
		}
		$show_thum = dima_helper::dima_am_i_true( dima_helper::dima_get_meta( '_dima_post_featured_Image' ) );
		if ( ( is_singular() && ! is_page() ) && $show_thum ) {
			return;
		}


		if ( ! empty( $attachments ) ) {
			echo '<div class="post-img"><div class="slick-slider" ><div class="slides" ' . $data . '>';
			foreach ( $attachments as $attachment_id => $attachment_url ) {
				$thumb = dima_helper::dima_get_thumb_gallery( $args, $attachment_id );
				echo( $thumb );
			}
			echo '</div></div></div>';
		} else if ( has_post_thumbnail() ) {
			$thumb = dima_helper::dima_get_thumb( $args );
			printf( '<div class="post-img"><div class="entry-thumb">%s</div></div>', $thumb );
		}

	}
endif;

if ( ! function_exists( 'dima_portfolio_featured_image' ) ) :
	function dima_portfolio_featured_image( $args = array() ) {

		$default_args = array(
			'post_type' => '',
			'img_hover' => '',
			'elm_hover' => '',
			'class'     => array()
		);

		$args = wp_parse_args( $args, $default_args );

		$url = wp_get_attachment_url( get_post_thumbnail_id() );
		if ( has_post_thumbnail() ) {

			if ( dima_helper::dima_is_single_portfolio() && $args['related'] != "true" ) {
				$thumb = dima_helper::dima_get_post_thumb( array(
					'size'      => 'dima-big-image-single',
					'is_linked' => false
				) );
			} elseif ( $args['post_type'] === 'grid' ) {
				$thumb = dima_helper::dima_get_post_thumb( array(
					'size'      => 'dima-portfolio-grid-image',
					'is_linked' => false
				) );
			} else {
				$thumb = dima_helper::dima_get_post_thumb( array(
					'size'      => 'dima-massonry-image',
					'is_linked' => false
				) );
			}

			if ( is_singular() && ! is_page() && $args['related'] != "true" ) {
				printf( '<div class="post-img"><div class="entry-thumb">%s</div></div>', $thumb );
			} else {

				switch ( $args['img_hover'] ) {
					case 'op_vc_zoom-out':
						$args['img_hover'] = " effect-roxy";
						break;
					case 'op_vc_zoom-in':
						$args['img_hover'] = " effect-julia";
						break;
					case 'op_vc_gray':
						$args['img_hover'] = " apply-gray";
						break;
					case 'op_vc_opacity':
						$args['img_hover'] = " apply-opacity";
						break;
					case 'op_vc_none':
					default:
						$args['img_hover'] = " post-feature";
						break;
				}

				dima_get_portfolio_item( $args, $url, $thumb );

			}
		}

	}
endif;

function dima_portfolio_featured_video( $args = array() ) {
	$default_args = array(
		'post_type' => '',
		'img_hover' => '',
		'elm_hover' => '',
		'class'     => array()
	);

	$args = wp_parse_args( $args, $default_args );

	if ( is_singular() && ! is_page() && $args['related'] != "true" ) {
		printf( '<div class="post-img"><div class="entry-thumb">%s</div></div>', $thumb );
	} else {
		switch ( $args['img_hover'] ) {
			case 'op_vc_zoom-out':
				$args['img_hover'] = " effect-roxy";
				break;
			case 'op_vc_zoom-in':
				$args['img_hover'] = " effect-julia";
				break;
			case 'op_vc_gray':
				$args['img_hover'] = " apply-gray";
				break;
			case 'op_vc_opacity':
				$args['img_hover'] = " apply-opacity";
				break;
			case 'op_vc_none':
			default:
				$args['img_hover'] = " post-feature";
				break;
		}
		dima_get_portfolio_item_video( $args );
	}
}

if ( ! function_exists( 'dima_get_portfolio_item_video' ) ) {
	function dima_get_portfolio_item_video( $args = array() ) {
		$_ID    = get_the_ID();
		$source = get_post_meta( $_ID, '_dima_video_source', true );
		$thumb  = '';

		if ( $source == 'self_hosted' ) {
			$m4v  = dima_helper::dima_get_meta( '_dima_video_m4v' );
			$ogv  = dima_helper::dima_get_meta( '_dima_video_ogv' );
			$webm = dima_helper::dima_get_meta( '_dima_video_webm' );
			$w    = dima_helper::dima_get_meta( '_dima_video_w' );
			$h    = dima_helper::dima_get_meta( '_dima_video_h' );

			if ( $w != '' && $h != '' ) {
				$padding = ( 100 * floatval( $h ) / floatval( $w ) );
			} else {
				$padding = 75.25;
			}

			$m4v  = ( $m4v != '' ) ? '<source src="' . $m4v . '" type="video/mp4">' : '';
			$ogv  = ( $ogv != '' ) ? '<source src="' . $ogv . '" type="video/ogg">' : '';
			$webm = ( $webm != '' ) ? '<source src="' . $webm . '" type="video/webm">' : '';

			$thumb = '<div class="dima-background dima-video-background">
                        <video class="dima-block_video"  loop="on" autoplay="on">
                       ' . $m4v . '
                       ' . $ogv . '
                       ' . $webm . '
                        </video>
                        <div style="width: 100%; padding-bottom:' . $padding . '%"></div>
                    </div>';
		} elseif ( $source == 'vimeo' ) {

			$vimeo_video = get_post_meta( $_ID, '_dima_video_vimeo', true );
			if ( $vimeo_video ) {
				if ( substr_count( $vimeo_video, 'vimeo.com/' ) > 0 ) {
					$vimeo_video = substr( $vimeo_video, ( stripos( $vimeo_video, 'vimeo.com/' ) + 10 ) );
				}
				if ( substr_count( $vimeo_video, '&' ) > 0 ) {
					$vimeo_video = substr( $vimeo_video, 0, stripos( $vimeo_video, '&' ) );
				}

				$thumb = '<div class="dima-background dima-video-background">
                    <div class="dima-video embed">
                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe src=\'https://player.vimeo.com/video/' . esc_attr( $vimeo_video ) . '?portrait=0\'
                                    width=\'1200\' height=\'675\' frameborder=\'0\'></iframe>
                        </div>
                    </div>
                    </div>';

			}
		} elseif ( $source == 'youtube' ) {

			$youtube_video = get_post_meta( $_ID, '_dima_video_youtube', true );
			if ( $youtube_video ) {
				if ( substr_count( $youtube_video, '?' ) > 0 ) {
					$youtube_video = substr( $youtube_video, ( stripos( $youtube_video, '?v=' ) + 3 ) );
				}
				if ( substr_count( $youtube_video, '&' ) > 0 ) {
					$youtube_video = substr( $youtube_video, 0, stripos( $youtube_video, '&' ) );
				}

				$thumb = '<div class="dima-background dima-video-background">
                            <div class="dima-video embed">
                                <div class="embed-responsive embed-responsive-16by9">
                                    <iframe width="1200" height="675"
                                            src="https://www.youtube.com/embed/' . esc_attr( $youtube_video ) . '?wmode=opaque"
                                            class="youtube-video" allowfullscreen></iframe>
                                </div>
                            </div>
                    </div>';

			}
		} elseif ( $source == 'embedded' ) {

			$embed     = get_post_meta( $_ID, '_dima_video_embed', true );
			$embed_url = get_post_meta( $_ID, '_dima_video_embed_two', true );
			$embed     = ( $embed_url == '' ) ? $embed : wp_oembed_get( esc_url( $embed_url ) );

			$thumb = '<div class="dima-background dima-video-background">
                           ' . do_shortcode( '[dima_embed_video]' . stripslashes( htmlspecialchars_decode( $embed ) ) . '[/dima_embed_video]' ) . '
                    </div>';

		}

		$terms    = get_the_terms( $_ID, 'portfolio-category' );
		$ext_link = dima_portfolio_item_project_link();
		$target   = ( dima_helper::dima_am_i_true( dima_helper::dima_get_meta( '_dima_portfolio_external_link_target' ) ) ) ? '' : 'target="_blank" rel="noopener"';
		$ext_link = $ext_link == '' ? get_permalink() : $ext_link;

		switch ( $args['elm_hover'] ) {
			case 'op_vc_none':
				$hover = '<div class="post-img"><div class="' . esc_attr( $args['img_hover'] ) . '"><a href="%1$s">%3$s</a></div></div>';
				break;

			case 'op_vc_inside':
				$hover = '<div class="work-item dima_go_inside">
                          <div class="' . esc_attr( $args['img_hover'] ) . '">
                            <a href="%1$s" class="entry-thumb" title="%2$s">
                              %3$s
                            </a>
                            <div class="post-icon link_overlay">
                                <ul class="icons-media">                                  
                                  <li>
                                    <a data-load = "true" href = "%1$s" ' . esc_attr( $target ) . 'title = "%2$s" >
                                      ' . wp_kses( dima_get_svg_icon( 'ic_link' ), dima_helper::dima_get_allowed_html_tag() ) . '
                                    </a>
                                  </li>
                                </ul> 
                            </div>
                            </div>
                            <div class="project-info text-start">
                                <h5 class="project-name"><a href="%1$s" title="%2$s">%4$s</a></h5>
                                <ul class="porftfolio-cat">';
				if ( isset( $terms[0]->slug ) ) {
					if ( sizeof( $terms ) == 1 ) {
						$hover .= '<li><a href="' . esc_url( get_term_link( $terms[0]->slug, 'portfolio-category' ) ) . '">' . $terms[0]->name . '</a></li>';
					} elseif ( sizeof( $terms ) > 1 ) {
						foreach ( $terms as $term ) {
							$hover .= '<li><a href="' . esc_url( get_term_link( $term->slug, 'portfolio-category' ) ) . '">' . $term->name . '</a> </li>';
							$hover .= '<li><span class="sep">,</span></li> ';
						}
					}
				}
				$hover .= '</ul>
			                </div>
                         </div> ';
				break;
			default:
				$hover = '<div class="work-item dima_title_btm">
                          <div class="' . esc_attr( $args['img_hover'] ) . '">
                            <a href="%1$s" class="entry-thumb" title="%2$s">
                              %3$s
                            </a>
                            <div class="post-icon link_overlay">
                                <ul class="icons-media">                                  
                                  <li>
                                    <a data-load = "true" href = "%1$s" ' . esc_attr( $target ) . 'title = "%2$s" >
                                      ' . wp_kses( dima_get_svg_icon( 'ic_link' ), dima_helper::dima_get_allowed_html_tag() ) . '
                                    </a>
                                  </li>
                                </ul> 
                            </div>
                            <div class="project-info text-start">
                                <h5 class="project-name"><a href="%1$s" title="%2$s">%4$s</a><span class="dima-divider line-start line-hr small-line"></span></h5>
                                <ul class="porftfolio-cat">';
				if ( isset( $terms[0]->slug ) ) {
					if ( sizeof( $terms ) == 1 ) {
						$hover .= '<li><a href="' . esc_url( get_term_link( $terms[0]->slug, 'portfolio-category' ) ) . '">' . $terms[0]->name . '</a></li>';

					} elseif ( sizeof( $terms ) > 1 ) {
						foreach ( $terms as $term ) {
							$hover .= '<li><a href="' . esc_url( get_term_link( $term->slug, 'portfolio-category' ) ) . '">' . $term->name . '</a></li>';
							$hover .= '<li><span class="sep">,</span></li> ';
						}
					}
				}
				$hover .= '</ul>
			                </div>
                            </div>
                         </div> ';

		}
		printf( $hover,
			esc_url( $ext_link ),
			esc_attr( sprintf( esc_html__( 'Permalink to: %s', 'noor' ), the_title_attribute( 'echo=0' ) ) ),
			$thumb,
			the_title_attribute( 'echo=0' )
		);

	}
}

if ( ! function_exists( 'dima_get_portfolio_item' ) ) {
	function dima_get_portfolio_item( $args = array(), $url, $thumb ) {
		$_ID = get_the_ID();
		$terms    = get_the_terms( $_ID, 'portfolio-category' );
		$ext_link = dima_portfolio_item_project_link();
		$target   = ( dima_helper::dima_get_meta( '_dima_portfolio_external_link_target' ) == 'off' ) ? '' : 'target="_blank" rel="noopener"';
		$ext_link = $ext_link == '' ? get_permalink() : $ext_link;

		if ( is_wp_error( $terms ) ) {
			return;
		}
		switch ( $args['elm_hover'] ) {
			case 'op_vc_none':
				$hover = '<div class="work-item post-img">
                            <div class="' . esc_attr( $args['img_hover'] ) . '">
                            <a href="%1$s" ' . esc_attr( $target ) . '>%3$s</a>
                            <div class="project-info text-center">
                                <h5 class="project-name"><a href="%1$s" title="%2$s">%4$s</a></h5>
                            </div>
                            </div>
                         </div>';
				break;

			case 'op_vc_inside':
				$hover = '<div class="work-item dima_go_inside">
                          <div class="' . esc_attr( $args['img_hover'] ) . '">
                            <a href="%1$s" ' . esc_attr( $target ) . ' class="entry-thumb" title="%2$s">
                              %3$s
                            </a>
                            <div class="post-icon link_overlay">
                                <ul class="icons-media">
                                  <li>
                                    <a data-lightbox="image" href="' . $url . '">
				                        ' . wp_kses( dima_get_svg_icon( 'ic_remove_red_eye' ), dima_helper::dima_get_allowed_html_tag() ) . '
				                    </a>
                                  </li>
                                  <li>
                                    <a data-load = "true" href = "%1$s" ' . esc_attr( $target ) . 'title = "%2$s" >
                                      ' . wp_kses( dima_get_svg_icon( 'ic_link' ), dima_helper::dima_get_allowed_html_tag() ) . '
                                    </a>
                                  </li>
                                </ul> 
                            </div>
                            <div class="project-info text-start">
                                <h5 class="project-name"><a href="%1$s" title="%2$s">%4$s</a></h5>
                                <ul class="porftfolio-cat">';
				if ( isset( $terms[0]->slug ) ) {
					if ( sizeof( $terms ) == 1 ) {
						$url   = str_replace( '%', '%%', get_term_link( $terms[0]->slug, 'portfolio-category' ) );
						$hover .= '<li><a href="' . esc_url( $url ) . '">' . $terms[0]->name . '</a></li>';
					} elseif ( sizeof( $terms ) > 1 ) {
						foreach ( $terms as $term ) {
							$url   = str_replace( '%', '%%', get_term_link( $term->slug, 'portfolio-category' ) );
							$hover .= '<li><a href="' . esc_url( $url ) . '">' . $term->name . '</a> </li>';
							$hover .= '<li><span class="sep">,</span></li> ';
						}
					}
				}
				$hover .= '</div></ul>
			                </div>
                         </div> ';
				break;
			default:
				$hover = '<div class="work-item dima_title_btm">
                          <div class="' . esc_attr( $args['img_hover'] ) . '">
                            <a href="%1$s" ' . esc_attr( $target ) . ' class="entry-thumb" title="%2$s">
                              %3$s
                            </a>
                            <div class="post-icon link_overlay">
                                <ul class="icons-media">
                                  <li>
                                    <a data-lightbox="image" href="' . $url . '">
				                        ' . wp_kses( dima_get_svg_icon( 'ic_remove_red_eye' ), dima_helper::dima_get_allowed_html_tag() ) . '
				                    </a>
                                  </li>
                                  <li>
                                    <a data-load = "true" href = "%1$s" ' . esc_attr( $target ) . ' title = "%2$s" >
                                      ' . wp_kses( dima_get_svg_icon( 'ic_link' ), dima_helper::dima_get_allowed_html_tag() ) . '
                                    </a>
                                  </li>
                                </ul> 
                            </div>
                            <div class="project-info text-start">
                                <h5 class="project-name"><a href="%1$s" title="%2$s">%4$s<span class="dima-divider line-start line-hr small-line"></span></a></h5>
                                <ul class="porftfolio-cat">';
				if ( isset( $terms[0]->slug ) ) {
					if ( sizeof( $terms ) == 1 ) {
						$url   = str_replace( '%', '%%', get_term_link( $terms[0]->slug, 'portfolio-category' ) );
						$hover .= '<li><a href="' . esc_url( $url ) . '">' . $terms[0]->name . '</a></li>';
					} elseif ( sizeof( $terms ) > 1 ) {
						foreach ( $terms as $term ) {
							$url   = str_replace( '%', '%%', get_term_link( $term->slug, 'portfolio-category' ) );
							$hover .= '<li><a href="' . esc_url( $url ) . '">' . $term->name . '</a></li>';
							$hover .= '<li><span class="sep">,</span></li> ';
						}
					}
				}
				$hover .= '</ul>
			                </div>
                            </div>
                         </div> ';

		}

		printf( $hover,
			esc_url( $ext_link ),
			esc_attr( sprintf( esc_html__( 'Permalink to: %s', 'noor' ), the_title_attribute( 'echo=0' ) ) ),
			$thumb,
			the_title_attribute( 'echo=0' )
		);

	}
}

if ( ! function_exists( 'dima_featured_portfolio' ) ) :
	/**
	 * Featured Portfolio
	 *
	 * @param array $args
	 */
	function dima_featured_portfolio( $args = array() ) {
		dima_portfolio_featured_image( array(
			'post_type' => $args['post_type'],
			'img_hover' => $args['img_hover'],
			'elm_hover' => $args['elm_hover'],
		) );
	}
endif;

/**
 * Featured Video
 */
if ( ! function_exists( 'dima_featured_video' ) ) :
	function dima_featured_video( $args = array() ) {
		$single = is_single();
		$_ID    = get_the_ID();
		$source = get_post_meta( $_ID, '_dima_video_source', true );
		$poster = get_post_meta( $_ID, '_dima_video_poster', true );

		$thumb_id  = get_post_thumbnail_id();
		$thumb_url = wp_get_attachment_image_src( $thumb_id, 'full', null );
		if ( $thumb_url ) {
			$thumb_url = $thumb_url[0];
		}

		if ( $poster != '' ) {
			$thumb_url = $poster;
		}

		switch ( $source ) {

			case 'vimeo':
				$vimeo_video = get_post_meta( $_ID, '_dima_video_vimeo', true );
				if ( $vimeo_video ) {
					if ( substr_count( $vimeo_video, 'vimeo.com/' ) > 0 ) {
						$vimeo_video = substr( $vimeo_video, ( stripos( $vimeo_video, 'vimeo.com/' ) + 10 ) );
					}
					if ( substr_count( $vimeo_video, '&' ) > 0 ) {
						$vimeo_video = substr( $vimeo_video, 0, stripos( $vimeo_video, '&' ) );
					}
					if ( $thumb_url != '' ) {
						?>
                        <div class="post-img">
                            <div class="video-overlay" data-bg="<?php echo( $thumb_url ) ?>">
                                <div class='video-overlay-hover'>
                                    <a href='#' class='video-play-button'></a>
                                </div>
                            </div>
                            <div class="embed-responsive embed-responsive-16by9">
                                <iframe src='https://player.vimeo.com/video/<?php echo esc_attr( $vimeo_video ); ?>?portrait=0'
                                        width='1200' height='675' frameborder='0'></iframe>
                            </div>
                        </div>
						<?php
					} else {
						?>
                        <div class="post-img">
                            <div class="dima-video embed">
                                <div class="embed-responsive embed-responsive-16by9">
                                    <iframe src='https://player.vimeo.com/video/<?php echo esc_attr( $vimeo_video ); ?>?portrait=0'
                                            width='1200' height='675' frameborder='0'></iframe>
                                </div>
                            </div>
                        </div>
						<?php
					}

					if ( ! $single ) {
						return;
					}
				}
				break;

			case 'youtube':
				$youtube_video = get_post_meta( $_ID, '_dima_video_youtube', true );
				if ( $youtube_video ) {
					if ( substr_count( $youtube_video, '?' ) > 0 ) {
						$youtube_video = substr( $youtube_video, ( stripos( $youtube_video, '?v=' ) + 3 ) );
					}
					if ( substr_count( $youtube_video, '&' ) > 0 ) {
						$youtube_video = substr( $youtube_video, 0, stripos( $youtube_video, '&' ) );
					}
					if ( $thumb_url != '' ) {
						?>
                        <div class="post-img">
                            <div class="video-overlay" data-bg="<?php echo( $thumb_url ) ?>">
                                <div class='video-overlay-hover'>
                                    <a href='#' class='video-play-button'></a>
                                </div>
                            </div>
                            <div class="embed-responsive embed-responsive-16by9">
                                <iframe width="1200" height="675"
                                        src="https://www.youtube.com/embed/<?php echo esc_attr( $youtube_video ); ?>?wmode=opaque"
                                        class="youtube-video" allowfullscreen></iframe>
                            </div>
                        </div>
						<?php
					} else {
						?>
                        <div class="post-img">
                            <div class="dima-video embed">
                                <div class="embed-responsive embed-responsive-16by9">
                                    <iframe width="1200" height="675"
                                            src="https://www.youtube.com/embed/<?php echo esc_attr( $youtube_video ); ?>?wmode=opaque"
                                            class="youtube-video" allowfullscreen></iframe>
                                </div>
                            </div>
                        </div>
						<?php
					}

					if ( ! $single ) {
						return;
					}
				}
				break;

			case 'embedded':
				$embed     = get_post_meta( $_ID, '_dima_video_embed', true );
				$embed_url = get_post_meta( $_ID, '_dima_video_embed_two', true );
				$embed     = ( $embed_url == '' ) ? $embed : wp_oembed_get( esc_url( $embed_url ) );

				echo "<div class='post-img'>"
				     . do_shortcode( '[dima_embed_video  poster="' . $thumb_url . '"]' . stripslashes( htmlspecialchars_decode( $embed ) ) . '[/dima_embed_video]' )
				     . "</div>";
				break;

			case 'self_hosted':
				$m4v  = get_post_meta( $_ID, '_dima_video_m4v', true );
				$ogv  = get_post_meta( $_ID, '_dima_video_ogv', true );
				$webm = get_post_meta( $_ID, '_dima_video_webm', true );

				if ( $m4v != '' || $ogv || $webm ) {
					echo "<div class='post-img'>"
					     . do_shortcode( '[dima_video_player poster_img="' . $thumb_url . '" m4v="' . $m4v . '" ogv="' . $ogv . '"  webm="' . $webm . '" preload="none" hide_controls="false" autoplay="false" loop="false" muted="false" class="mvn"]' )
					     . "</div>";
				}
				break;

		}

	}
endif;

/**
 * Featured Audio
 */
if ( ! function_exists( 'dima_featured_audio' ) ) :
	function dima_featured_audio( $args = array() ) {
		$single = is_single();
		$_ID    = get_the_ID();
		$source = get_post_meta( $_ID, '_dima_audio_source', true );

		switch ( $source ) {
			case 'embedded':
				$embed     = get_post_meta( $_ID, '_dima_audio_embed', true );
				$embed_url = get_post_meta( $_ID, '_dima_audio_embed_url', true );
				$embed     = ( $embed_url == '' ) ? $embed : wp_oembed_get( esc_url( $embed_url ) );

				echo "<div class='post-audio'>"
				     . do_shortcode( '[dima_embed_audio class="mvn"]' . stripslashes( htmlspecialchars_decode( $embed ) ) . '[/dima_embed_audio]' ) .
				     "</div>";

				break;
			case 'self_hosted':
				$mp3 = get_post_meta( $_ID, '_dima_audio_mp3', true );
				$ogg = get_post_meta( $_ID, '_dima_audio_ogg', true );

				$comp     = '';
				$producer = dima_helper::dima_get_meta( '_dima_audio_producer' );
				$author   = dima_helper::dima_get_meta( '_dima_audio_author' );
				$out      = ( $author != '' ) ? "<span><h6>" . esc_attr( $author ) . "</h6></span>" : '';
				if ( $producer != '' && $author != '' ) {
					$out .= "<span class=\"sep\">|</span>";
				}
				$out .= ( $producer != '' ) ? "<span><h6>" . esc_attr( $producer ) . "</h6></span>" : '';

				if ( $out != '' && $single ) {
					$comp = "<div class='clearfix dima-composition'>
        			 $out
    			 </div>";
				}

				if ( $mp3 != '' || $ogg != '' ) {
					echo "<div class='post-audio'>"
					     . do_shortcode( '[dima_audio_player mp3="' . $mp3 . '" oga="' . $ogg . '" preload="none" autoplay="false" loop="false" class="mvn"]' ) .
					     $comp .
					     "</div>";
				}
				break;
		}
	}
endif;