<?php
/**
 * DIMA  Walekr nav menu
 *
 * @package    Dima Framework
 * @subpackage Functions
 * @version    1.0.0
 * @since      1.0.0
 * @author     PixelDima <info@pixeldima.com>
 *
 */


/**
 * Class DIMA_Walker_Nav_Menu
 * Example: dima-mega-menu col-3
 */
if ( ! class_exists( 'DIMA_Walker_Nav_Menu' ) ) {
	class DIMA_Walker_Nav_Menu extends Walker_Nav_Menu {

		private $is_megamenu = "";
		private $is_masonry = "";
		private $columns = 6;
		private $padding_left = 0;
		private $padding_right = 0;
		private $padding_bottom = 0;
		private $padding_top = 0;
		private $dima_megamenu_title = '';
		private $link_before = '';
		private $link_after = '';
		private $dima_megamenu_icon = '';
		private $dima_megamenu_svg = '';
		private $dima_megamenu_style = '';
		private $dima_megamenu_textclass = '';
		private $dima_megamenu_background = '';
		private $is_mobile = 6;
		private $line_columns_count = 0;

		public function __construct( $is_mobile ) {
			$this->is_mobile = $is_mobile;
		}

		/**
		 * If submenu add class sub-icon to display the arrow icon on the item
		 *
		 * @param object $element
		 * @param array  $children_elements
		 * @param int    $max_depth
		 * @param int    $depth
		 * @param array  $args
		 * @param string $output
		 */
		function display_element(
			$element, &$children_elements, $max_depth,
			$depth = 0, $args, &$output
		) {
			$id_field = $this->db_fields['id'];
			if ( ! empty( $children_elements[ $element->$id_field ] ) ) {
				$element->classes[] = 'sub-icon';
			}
			Walker_Nav_Menu::display_element(
				$element, $children_elements, $max_depth, $depth, $args, $output
			);
		}

		function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {

			$indent    = ( $depth > 0 ? str_repeat( "\t", $depth ) : '' );
			$backdrop  = ( $depth > 0 ? '<span class="link-backdrop"></span>'
				: '' );
			$sube_icon = '';
			if ( is_array( $item->classes ) ) {
				if ( in_array( "sub-icon", $item->classes ) ) {
					//vertical-menu-end
					if ( is_rtl() ) {
						if ( in_array( 'vertical-menu vertical-menu-end', get_body_class() ) ) {
							$sube_icon = ( $depth > 0 ? '<span class="svg-sub-icon">' . dima_get_svg_icon( esc_attr( "ic_keyboard_arrow_right" ) ) . '</span>' : '' );
						} else {
							$sube_icon = ( $depth > 0 ? '<span class="svg-sub-icon">' . dima_get_svg_icon( esc_attr( "ic_keyboard_arrow_left" ) ) . '</span>' : '' );
						}
					} else {
						if ( ! in_array( 'vertical-menu vertical-menu-end', get_body_class() ) ) {
							$sube_icon = ( $depth > 0 ? '<span class="svg-sub-icon">' . dima_get_svg_icon( esc_attr( "ic_keyboard_arrow_right" ) ) . '</span>' : '' );
						} else {
							$sube_icon = ( $depth > 0 ? '<span class="svg-sub-icon">' . dima_get_svg_icon( esc_attr( "ic_keyboard_arrow_left" ) ) . '</span>' : '' );
						}
					}
				}
			}

			$icon             = '';
			$icon_rtl         = '';
			$prepend          = '';
			$append           = '';
			$image            = '';
			$disable_megamenu = false;

			$this->dima_megamenu_background = get_post_meta(
				$item->ID, '_dima_megamenu_item_background', true
			);
			if ( $this->dima_megamenu_background != "" ) {
				$image_id = attachment_url_to_postid(
					$this->dima_megamenu_background
				);
				list(
					$this->dima_megamenu_background
					)
					= wp_get_attachment_image_src( $image_id, "full" );
				$image
					= "<span class='custom-item-{$item->ID} dima-custom-item-image'><img src='"
					  . esc_url( $this->dima_megamenu_background ) . "' alt='"
					  . apply_filters( 'the_title', $item->title, $item->ID )
					  . "'/></span>";
			}

			$depth_classes     = array(
				( $depth == 0 ? 'main-menu-item' : 'sub-menu-item' ),
				'menu-item-depth-' . $depth
			);
			$depth_class_names = esc_attr( implode( ' ', $depth_classes ) );

			// Passed classes.
			$classes                       = empty( $item->classes ) ? array()
				: (array) $item->classes;
			$class_names                   = esc_attr(
				implode(
					' ', apply_filters(
						'nav_menu_css_class', array_filter( $classes ), $item
					)
				)
			);
			$this->dima_megamenu_style     = esc_attr(
				get_post_meta( $item->ID, '_dima_megamenu_item_style', true )
			);
			$this->dima_megamenu_textclass = esc_attr(
				get_post_meta( $item->ID, '_dima_megamenu_item_textclass', true )
			);

			if ( ! $disable_megamenu ) {

				if ( $depth === 0 ) {
					$this->line_columns_count = 0;

					$this->is_megamenu    = get_post_meta(
						$item->ID, '_dima_megamenu_item_ismega', true
					);
					$this->is_masonry     = get_post_meta(
						$item->ID, '_dima_megamenu_item_masonry', true
					);
					$this->columns        = get_post_meta(
						$item->ID, '_dima_megamenu_item_columns', true
					);
					$this->padding_left   = floatval(
						get_post_meta(
							$item->ID, '_dima_megamenu_item_padding_left', true
						)
					);
					$this->padding_right  = floatval(
						get_post_meta(
							$item->ID, '_dima_megamenu_item_padding_right', true
						)
					);
					$this->padding_top    = floatval(
						get_post_meta(
							$item->ID, '_dima_megamenu_item_padding_top', true
						)
					);
					$this->padding_bottom = floatval(
						get_post_meta(
							$item->ID, '_dima_megamenu_item_padding_bottom',
							true
						)
					);
					$this->padding_top    = ( $this->padding_top == 0 ) ? 20
						: $this->padding_top;
					$this->padding_bottom = ( $this->padding_bottom == 0 ) ? 20
						: $this->padding_bottom;

					if ( $this->is_megamenu == "enabled" ) {
						$class_names .= " dima-mega-menu";
						if ( $this->is_masonry == "enabled" ) {
							$class_names .= " dima-megamenu-masonry";
						}
					}

					$this->dima_megamenu_title = get_post_meta(
						$item->ID, '_dima_megamenu_item_title', true
					);

				}

				$this->dima_megamenu_icon = get_post_meta(
					$item->ID, '_dima_megamenu_item_icon', true
				);
				$this->dima_megamenu_svg  = get_post_meta(
					$item->ID, '_dima_megamenu_item_svg', true
				);
			} else {
				$this->is_megamenu        = 'disabled';
				$this->dima_megamenu_icon = get_post_meta(
					$item->ID, '_dima_megamenu_item_icon', true
				);
				$this->dima_megamenu_svg  = get_post_meta(
					$item->ID, '_dima_megamenu_item_svg', true
				);
				$this->line_columns_count = 0;
			}
			//add icon
			if ( $this->dima_megamenu_svg != "" ) {
				if ( ! is_rtl() ) {
					$icon     = '<span class="menu_icon_item">'
					            . dima_get_svg_icon(
						            esc_attr( $this->dima_megamenu_svg )
					            ) . '</span>';
					$icon_rtl = "";
				} else {
					$icon_rtl = '<span class="menu_icon_item">'
					            . dima_get_svg_icon( esc_attr( $this->dima_megamenu_svg ) )
					            . '</span>';
					$icon     = '';
				}
			} else if ( $this->dima_megamenu_icon != "" ) {
				if ( ! is_rtl() ) {
					$icon     = '<span class="menu_icon_item">' . do_shortcode(
							'[icon class="' . esc_attr(
								$this->dima_megamenu_icon
							) . '"]'
						) . '</span>';
					$icon_rtl = "";
				} else {
					$icon_rtl = '<span class="menu_icon_item">' . do_shortcode(
							'[icon class="' . esc_attr(
								$this->dima_megamenu_icon
							) . '"]'
						) . '</span>';
					$icon     = '';
				}
			}

			//! add icon
			if ( $depth === 1 && $this->is_megamenu == "enabled" ) {
				$menu_id     = '';
				$new_line_li = '';

				if ( $this->line_columns_count == $this->columns ) {
					$new_line_li              = '<li class="dima-megamenu-new-row"></li>';
					$this->line_columns_count = 0;
				}
				$this->line_columns_count ++;
				if ( ! $this->is_mobile ) {
					$menu_id = 'id="nav-menu-item-' . $item->ID . '"';
				}
				$output .= $indent . $new_line_li . '<li ' . $menu_id
				           . ' class="dima-megamenu-item ' . $depth_class_names . ' '
				           . $class_names . '" style="width: 260px;">';

				$attributes = ! empty( $item->attr_title ) ? ' title="'
				                                             . esc_attr( $item->attr_title ) . '"' : '';
				$attributes .= ! empty( $item->target ) ? ' target="' . esc_attr(
						$item->target
					) . '"' : '';
				$attributes .= ! empty( $item->xfn ) ? ' rel="' . esc_attr(
						$item->xfn
					) . '"' : '';
				$attributes .= ! empty( $item->url ) ? ' href="' . esc_attr(
						$item->url
					) . '"' : '';
				$attributes .= '';

				if ( $this->dima_megamenu_title != 'disabled' ) {
					$output .= '<a ' . $attributes . ' class="dima-menu-title">'
					           . $icon;
					$output .= $args->link_before . $prepend . apply_filters(
							'the_title', $this->_parse_string( $item->title ),
							$item->ID
						) . $append;
					$output .= $args->link_after;
					$output .= $icon_rtl
					           . '<span class="dima-divider line-start line-hr small-line"></span></a>';
				} else {
					$output .= '<a' . $attributes . '>' . $icon;
					$output .= $args->link_before . $prepend . apply_filters(
							'the_title', $this->_parse_string( $item->title ),
							$item->ID
						) . $append;
					$output .= $backdrop;
					$output .= $args->link_after;
					$output .= $icon_rtl . '</a>';
				}
				$item_output = is_object( $args ) ? $args->after : '';
				// Build HTML output and pass through the proper filter.
				$output .= apply_filters(
					'DIMA_Walker_Nav_Menu', $item_output, $item, $depth, $args
				);

			} else {
				// Build HTML.
				$menu_id = '';
				if ( ! $this->is_mobile ) {
					$menu_id = 'id="nav-menu-item-' . $item->ID . '"';
				}

				$data_columns = '';
				if ( $this->is_megamenu == "enabled" ) {
					$data_columns = ' data-megamenu-columns="' . $this->columns
					                . '" ';
					$data_columns .= ' data-megamenu-padding="'
					                 . $this->padding_top . ',' . $this->padding_right . ','
					                 . $this->padding_bottom . ',' . $this->padding_left
					                 . '" ';
				}

				$output  .= $indent . '<li ' . $menu_id . ' class="'
				            . $depth_class_names . ' ' . $class_names . '" '
				            . $data_columns . '>';
				$classes = '';

				// Link attributes.
				$attributes = ! empty( $item->attr_title ) ? ' title="'
				                                             . esc_attr( $item->attr_title ) . '"' : '';
				$attributes .= ! empty( $item->target ) ? ' target="' . esc_attr(
						$item->target
					) . '"' : '';
				$attributes .= ! empty( $item->xfn ) ? ' rel="' . esc_attr(
						$item->xfn
					) . '"' : '';
				$attributes .= ! empty( $item->url ) ? ' href="' . esc_url(
						$item->url
					) . '"' : '';
				$attributes .= '';

				if ( $depth === 0 ) {
					$classes .= $this->dima_megamenu_textclass . ' ';
					if ( $this->dima_megamenu_style ) {
						$classes .= sprintf(
							'dima-menu-span dima-button fill %s', str_replace(
								'fusion-', '', $this->dima_megamenu_style
							)
						);
					} else {
						$classes .= 'dima-menu-span';
					}

					$item_output_before = sprintf(
						'<span class="%s">', $classes
					);
					$item_output_after  = '</span>';
					// Normal menu item
				} else {
					$item_output_before = sprintf(
						'<span class="%s">', $classes
					);
					$item_output_after  = '</span>';
				}

				$output      .= '<a' . $attributes . '>';
				$output      .= $this->link_before . $icon . $item_output_before
				                . apply_filters(
					                'the_title', $this->_parse_string( $item->title ),
					                $item->ID
				                ) . $item_output_after . $icon_rtl;
				$output      .= $this->link_after;
				$output      .= $backdrop;
				$output      .= $sube_icon;
				$output      .= '</a>' . $image;
				$item_output = is_object( $args ) ? $args->after : '';

				// Build HTML output and pass through the proper filter.
				$output .= apply_filters(
					'DIMA_Walker_Nav_Menu', $item_output, $item, $depth, $args
				);
			}
		}

		/**
		 * @param $string
		 *
		 * @return string
		 */
		protected function _parse_string( $string ) {
			return nl2br( $string );
		}

	}
}

/**
 * Get  Burger navigation menu
 */
if ( ! function_exists( 'dima_get_burger_navigation_menu' ) ) :
	/**
	 *
	 * @return mixed|string
	 */
	function dima_get_burger_navigation_menu() {
		$meta = get_post_meta( get_the_ID(), '_dima_meta_burger_navigation', true );
		$menu = ( $meta == '' ) ? 'off' : $meta;

		return $menu;
	}
endif;
/**
 * Get primary navigation menu
 */
if ( ! function_exists( 'dima_get_primary_navigation_menu' ) ) :
	/**
	 *
	 * @return mixed|string
	 */
	function dima_get_primary_navigation_menu() {
		$meta = get_post_meta( get_the_ID(), '_dima_meta_primary_navigation', true );
		$menu = ( $meta == '' ) ? 'off' : $meta;

		return $menu;
	}
endif;
/**
 * Get primary icon menu
 */
if ( ! function_exists( 'dima_get_icon_navigation_menu' ) ) :
	/**
	 *
	 * @return mixed|string
	 */
	function dima_get_icon_navigation_menu() {
		$meta = get_post_meta( get_the_ID(), '_dima_meta_icon_navigation', true );
		$menu = ( $meta == '' ) ? 'off' : $meta;

		return $menu;
	}
endif;

if ( ! function_exists( 'dima_is_one_page_navigation' ) ) :
	function dima_is_one_page_navigation() {
		$output = dima_helper::dima_am_i_true( dima_helper::dima_get_meta( "_dima_meta_one_page_navigation" ) );

		return $output;
	}
endif;

if ( ! function_exists( 'dima_is_transparent_navigation' ) ) :
	function dima_is_transparent_navigation() {
		$transparent_navigation = dima_helper::dima_am_i_true(
			dima_helper::dima_get_inherit_option( '_dima_meta_transparent_menu', 'dima_header_navbar_transparent' )
		);

		return $transparent_navigation;
	}
endif;


/**
 * Get Navbar Positioning
 */
if ( ! function_exists( 'dima_get_header_positioning' ) ):
	function dima_get_header_positioning() {
		$output = dima_helper::dima_get_option( 'dima_header_navbar_position' );

		if ( is_singular() ) {
			$output = dima_helper::dima_get_inherit_option( '_dima_meta_header_style', 'dima_header_navbar_position' );
		}
		if ( is_rtl() ) {
			if ( $output == 'fixed-left' ) {
				$output = 'fixed-right';
			} elseif ( $output == 'fixed-right' ) {
				$output = 'fixed-left';
			}
		}

		return $output;
	}


	add_action( 'customize_save', 'dima_get_header_positioning' );
endif;

/**
 * Get Navbar animation
 */
if ( ! function_exists( 'dima_get_header_animation' ) ):
	function dima_get_header_animation() {
		$menu_model = dima_get_header_positioning();
		//avoid side menu with animation
		if ( $menu_model == 'fixed-left' || $menu_model == 'fixed-right'
		     || $menu_model == 'fixed-left-small'
		     || $menu_model == 'fixed-right-small'
		) {
			$output = '';
		} else {
			$output = dima_helper::dima_get_inherit_option( '_dima_meta_navbar_animation', 'dima_header_navbar_animation' );
		}

		return $output;
	}

	add_action( 'customize_save', 'dima_get_header_animation' );
endif;

if ( ! function_exists( 'dima_get_burger_menu' ) ):
	function dima_get_burger_menu( $list = '' ) {
		$html_out          = '';
		$is_burger_menu    = dima_helper::dima_am_i_true( dima_helper::dima_get_inherit_option( '_dima_meta_burger_display', 'dima_header_burger_menu' ) );
		$burger_menu_float = dima_helper::dima_get_inherit_option( '_dima_meta_burger_position', 'dima_header_burger_menu_float' ) == 'start';
		$burger_menu_style = dima_helper::dima_get_inherit_option( '_dima_meta_burger_style', 'dima_header_burger_menu_style' );
		$header_style      = dima_helper::dima_get_inherit_option( '_dima_meta_header_style', 'dima_header_navbar_position' );
		if ( $header_style == "fixed-right" || $header_style == "fixed-left"
		     || $header_style == "fixed-right-small" || $header_style == "fixed-left-small"
		) {
			$burger_menu_style = 'full';
			$burger_menu_float = 'end';
		}


		if ( $is_burger_menu ) {

			if ( $burger_menu_float == 'start' ) {
				if ( $burger_menu_style == "full" ) {
					$arrow = dima_get_svg_icon( 'ic_expand_more' );
				} else {
					$arrow = dima_get_svg_icon( 'ic_chevron_right' );
					if ( is_rtl() ) {
						$arrow = dima_get_svg_icon( 'ic_chevron_left' );
					}
				}

				$html_out
					= '<div class="start-burger-menu"><ul class="dima-nav icon-menu">
				<li class="burger-menu burger-menu-pos-start burger-menu-' . $burger_menu_style . '">
				<a href="#">
				<span class="menu_icon_item">' . dima_get_svg_icon( 'ic_menu' ) . '</span>
				<span class="menu_icon_item sort_ic">' . $arrow . '</span>
				</a>
				</li>
			</ul></div>';

				if ( $list == 'list' ) {
					$html_out = '';
				}

			} else {
				if ( $burger_menu_style == "full" ) {
					$arrow = dima_get_svg_icon( 'ic_expand_more' );
				} else {
					$arrow = dima_get_svg_icon( 'ic_chevron_left' );
					if ( is_rtl() ) {
						$arrow = dima_get_svg_icon( 'ic_chevron_right' );
					}
				}

				$html_out = '<li class="burger-menu burger-menu-pos-end burger-menu-'
				            . $burger_menu_style . '">
				           <a  href="#">
				           <span class="menu_icon_item">' . dima_get_svg_icon( 'ic_menu' ) . '</span>				        
				           <span class="menu_icon_item sort_ic">' . $arrow . '</span>
				           <span class=" dima-menu-span">Menu</span>
				           </a>
				           </li>';
				if ( $list != 'list' ) {
					$html_out = '';
				}
			}
		}

		return $html_out;
	}
endif;

/**
 * Output Primary Navigation
 */
if ( ! function_exists( 'dima_output_primary_navigation' ) ):
	function dima_output_primary_navigation( $is_mobile = false ) {
		if ( dima_get_primary_navigation_menu() != 'off' ) {
			wp_nav_menu(
				array(
					'menu'           => dima_get_primary_navigation_menu(),
					'theme_location' => 'primary',
					'depth'          => 0,
					'container'      => false,
					'menu_class'     => 'dima-nav nav-primary',
					'echo'           => true,
					'before'         => '',
					'after'          => '',
					'link_before'    => '',
					'link_after'     => '',
					'walker'         => new DIMA_Walker_Nav_Menu( $is_mobile ),
				)
			);
		} elseif ( has_nav_menu( 'primary' ) ) {
			wp_nav_menu(
				array(
					'theme_location' => 'primary',
					'depth'          => 0,
					'container'      => false,
					'menu_class'     => 'dima-nav nav-primary',
					'echo'           => true,
					'before'         => '',
					'after'          => '',
					'link_before'    => '',
					'link_after'     => '',
					'walker'         => new DIMA_Walker_Nav_Menu( $is_mobile ),
				)
			);
		}

		if ( ! has_nav_menu( 'icon' ) && ! has_nav_menu( 'primary' ) ) {
			echo '<ul class="dima-nav-end"><li><a style="margin-left: 30px;margin-right: 30px;" href="'
			     . esc_url( home_url( '/' ) ) . 'wp-admin/nav-menus.php">' . esc_html__( 'Assign a Menu', 'noor' ) . '</a>
        </li>';
		}
	}
endif;

/**
 * Output Icon Navigation
 */
if ( ! function_exists( 'dima_output_icon_navigation' ) ):
	function dima_output_icon_navigation( $is_mobile = false ) {
		if ( dima_get_icon_navigation_menu() != 'off' ) {
			wp_nav_menu(
				array(
					'menu'           => dima_get_icon_navigation_menu(),
					'theme_location' => 'icon',
					'depth'          => 0,
					'container'      => false,
					'menu_class'     => 'dima-nav icon-menu',
					'echo'           => true,
					'before'         => '',
					'after'          => '',
					'link_before'    => '',
					'link_after'     => '',
					'walker'         => new DIMA_Walker_Nav_Menu( $is_mobile ),
				)
			);
		} elseif ( has_nav_menu( 'icon' ) ) {
			wp_nav_menu(
				array(
					'theme_location' => 'icon',
					'depth'          => 0,
					'container'      => false,
					'menu_class'     => 'dima-nav icon-menu',
					'echo'           => true,
					'before'         => '',
					'after'          => '',
					'link_before'    => '',
					'link_after'     => '',
					'walker'         => new DIMA_Walker_Nav_Menu( $is_mobile ),
				)
			);
		}
	}
endif;

/**
 * Output Icon Navigation
 */
if ( ! function_exists( 'dima_output_burger_navigation' ) ):
	function dima_output_burger_navigation( $is_mobile = false ) {
		$menu_class = ( $is_mobile ) ? 'dima-nav nav-primary' : 'dima-menu';
		if ( dima_get_burger_navigation_menu() != 'off' ) {
			if ( $is_mobile ) {
				wp_nav_menu(
					array(
						'menu'           => dima_get_burger_navigation_menu(),
						'theme_location' => 'burger',
						'container'      => false,
						'menu_class'     => $menu_class,
						'depth'          => 1,
						'walker'         => new DIMA_Walker_Nav_Menu( $is_mobile ),
					)
				);
			}
		} elseif ( has_nav_menu( 'burger' ) ) {
			wp_nav_menu(
				array(
					'theme_location' => 'burger',
					'container'      => false,
					'menu_class'     => $menu_class,
					'depth'          => 1,
					'walker'         => new DIMA_Walker_Nav_Menu( $is_mobile ),
				)
			);
		} else {
			echo '<ul class="dima-menu"><li><a href="' . esc_url( home_url( '/' ) ) . 'wp-admin/nav-menus.php">Assign a Menu</a></li></ul>';
		}
	}
endif;

/**
 * Navbar Search Navigation Item
 */
if ( ! function_exists( 'dima_navbar_search_li' ) ) :
	/**
	 * @param $items
	 *
	 * @return string
	 */
	function dima_navbar_ain_icons( $items ) {
		$is_search_active = dima_helper::dima_am_i_true( dima_helper::dima_get_inherit_option( '_dima_meta_search_display', 'dima_header_search_enable' ) );
		$is_burger_menu   = dima_helper::dima_am_i_true( dima_helper::dima_get_inherit_option( '_dima_meta_burger_display', 'dima_header_burger_menu' ) );
		$myaccount_show   = dima_helper::dima_am_i_true( dima_helper::dima_get_inherit_option( '_dima_meta_navbar_option_myaccount_display_topbar', 'dima_navbar_option_myaccount_display_topbar' ) );

		if ( $is_search_active ) {
			$items .= '<li class="search-btn">'
			          . '<a  href="#"><span class="menu_icon_item">'
			          . dima_get_svg_icon( 'ic_search' )
			          . '</span><span class=" dima-menu-span">Search</span></a>'
			          . '</li>';
		}

		if ( DIMA_WC_IS_ACTIVE && $myaccount_show ) {
			$items .= '<li>'
			          . '<a  href="' . get_permalink(
				          get_option( 'woocommerce_myaccount_page_id' )
			          ) . '"><span class="menu_icon_item">' . dima_get_svg_icon(
				          'ic_person'
			          ) . '</span><span class=" dima-menu-span">Account</span></a>'
			          . '</li>';
		}
		if ( DIMA_YITH_WISHLIST_IS_ACTIVE && dima_helper::dima_am_i_true( dima_helper::dima_get_inherit_option( '_dima_meta_navbar_option_wishlist', 'dima_navbar_option_wishlist' ) ) ) {
			$items .= apply_filters( 'dima_filter_wishlist_account_item', true );
		}
		if ( DIMA_WC_IS_ACTIVE
		     && dima_helper::dima_am_i_true( dima_helper::dima_get_inherit_option( '_dima_meta_shop_menu', 'dima_shop_menu' ) )
		) {
			$items .= dima_wc_navbar_cart();
		}

		if ( $is_burger_menu ) {
			$items .= dima_get_burger_menu( 'list' );
		}

		return $items;
	}

endif;