<?php
/**
 * Theme scripts , include js.
 *
 * @package Dima Framework
 * @subpackage Functions
 * @version   1.0.0
 * @since     1.0.0
 * @author    PixelDima <info@pixeldima.com>
 *
 */


// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

add_action( 'wp_enqueue_scripts', 'dima_global_scripts', 10 );
add_action( 'admin_enqueue_scripts', 'dima_admin_scripts_meta_box' );
add_action( 'customize_preview_init', 'dima_customizer_scripts' );
add_action( 'customize_controls_print_footer_scripts', 'dima_customizer_control_scripts' );

if ( ! function_exists( 'dima_global_scripts' ) ) :
	/**
	 * Undocumented function
	 *
	 * @return void
	 */
	function dima_global_scripts() {
		if ( ! is_admin() && ! in_array( $GLOBALS['pagenow'], array( 'wp-login.php', 'wp-register.php' ) ) ) {
			$dima_smoothscroll = dima_helper::dima_get_option( 'dima_smoothscroll' );
			$js_min            = dima_helper::dima_am_i_true( dima_helper::dima_get_option( 'dima_minified_js_files' ) );

			if ( dima_helper::dima_am_i_true( dima_helper::dima_get_option( 'dima_lazy_image' ) ) ) {
				wp_register_script( 'dima-js-lazyload-images', DIMA_TEMPLATE_URL . '/framework/asset/site/js/specific/lazyload.min.js', array( 'jquery' ), DIMA_VERSION, true );
				wp_enqueue_script( 'dima-js-lazyload-images' );
			}

			//Global
			if ( dima_helper::dima_am_i_true( dima_helper::dima_get_inherit_option( '_dima_meta_loading', 'dima_loading' ) ) ) {
				wp_register_script( 'dima-load-js', DIMA_TEMPLATE_URL . '/framework/asset/site/js/core/load.js', array( 'jquery' ), DIMA_VERSION, false );
				wp_enqueue_script( 'dima-load-js' );
			}

			wp_register_script( 'modernizr-js', DIMA_TEMPLATE_URL . '/framework/asset/site/js/core/modernizr-custom.js', array( 'jquery' ), DIMA_VERSION, false );
			wp_enqueue_script( 'modernizr-js' );

			wp_register_script( 'dima_init_js', DIMA_TEMPLATE_URL . '/framework/asset/site/js/init-es6.js', array( 'jquery' ), DIMA_VERSION, false );
			wp_enqueue_script( 'dima_init_js' );

			global $is_IE;
			if ( $is_IE ) {
				preg_match( '/MSIE (.*?);/', $_SERVER['HTTP_USER_AGENT'], $matches );
				if ( count( $matches ) < 2 ) {
					preg_match( '/Trident\/\d{1,2}.\d{1,2}; rv:([0-9]*)/', $_SERVER['HTTP_USER_AGENT'], $matches );
				}

				$version = $matches[1];

				// IE 10
				if ( $version <= 11 ) {
					wp_enqueue_script( 'dima-ie-scripts', DIMA_TEMPLATE_URL . '/framework/asset/site/js/ie.js', array( 'jquery' ), false, true );
				}

			}

			//* ------------ All files on module
			//* ------------ Do not change the files order
			if ( ! $js_min ) {
				wp_register_script( 'bootstrap', DIMA_TEMPLATE_URL . '/framework/asset/site/js/module/bootstrap.min.js', array( 'jquery' ), DIMA_VERSION, false );
				wp_enqueue_script( 'bootstrap' );

				wp_register_script( 'bootstrap-transition', DIMA_TEMPLATE_URL . '/framework/asset/site/js/module/bootstrap-transition.js', array( 'jquery' ), DIMA_VERSION, false );
				wp_enqueue_script( 'bootstrap-transition' );

				wp_register_script( 'countup', DIMA_TEMPLATE_URL . '/framework/asset/site/js/module/countup.js', array( 'jquery' ), DIMA_VERSION, false );
				wp_enqueue_script( 'countup' );

				wp_register_script( 'dropkick', DIMA_TEMPLATE_URL . '/framework/asset/site/js/module/dropkick.js', array( 'jquery' ), DIMA_VERSION, false );
				wp_enqueue_script( 'dropkick' );

				wp_register_script( 'headroom', DIMA_TEMPLATE_URL . '/framework/asset/site/js/module/headroom.min.js', array( 'jquery' ), DIMA_VERSION, false );
				wp_enqueue_script( 'headroom' );

				wp_register_script( 'hoverintent', DIMA_TEMPLATE_URL . '/framework/asset/site/js/module/hoverintent.js', array( 'jquery' ), DIMA_VERSION, false );
				wp_enqueue_script( 'hoverintent' );

				wp_register_script( 'dima-ie', DIMA_TEMPLATE_URL . '/framework/asset/site/js/module/ie.js', array( 'jquery' ), DIMA_VERSION, false );
				wp_enqueue_script( 'dima-ie' );

				wp_register_script( 'isotope.pkgd', DIMA_TEMPLATE_URL . '/framework/asset/site/js/module/isotope.pkgd.min.js', array( 'jquery' ), DIMA_VERSION, false );
				wp_enqueue_script( 'isotope.pkgd' );

				wp_register_script( 'fit-rows', DIMA_TEMPLATE_URL . '/framework/asset/site/js/module/layout-modes/fit-rows.js', array( 'jquery' ), DIMA_VERSION, false );
				wp_enqueue_script( 'fit-rows' );

				wp_register_script( 'masonry', DIMA_TEMPLATE_URL . '/framework/asset/site/js/module/layout-modes/masonry.js', array( 'jquery' ), DIMA_VERSION, false );
				wp_enqueue_script( 'masonry' );

				wp_register_script( 'vertical', DIMA_TEMPLATE_URL . '/framework/asset/site/js/module/layout-modes/vertical.js', array( 'jquery' ), DIMA_VERSION, false );
				wp_enqueue_script( 'vertical' );

				wp_register_script( 'jquery.knob', DIMA_TEMPLATE_URL . '/framework/asset/site/js/module/jquery.knob.js', array( 'jquery' ), DIMA_VERSION, false );
				wp_enqueue_script( 'jquery.knob' );

				wp_register_script( 'jquery.scrollto', DIMA_TEMPLATE_URL . '/framework/asset/site/js/module/jquery.scrollto-min.js', array( 'jquery' ), DIMA_VERSION, false );
				wp_enqueue_script( 'jquery.scrollto' );

				wp_register_script( 'jquery.validate', DIMA_TEMPLATE_URL . '/framework/asset/site/js/module/jquery.validate.min.js', array( 'jquery' ), DIMA_VERSION, false );
				wp_enqueue_script( 'jquery.validate' );

				wp_register_script( 'jquery-ui', DIMA_TEMPLATE_URL . '/framework/asset/site/js/module/jquery-ui.min.js', array( 'jquery' ), DIMA_VERSION, false );
				wp_enqueue_script( 'jquery-ui' );

				wp_register_script( 'packery-mode.pkgd', DIMA_TEMPLATE_URL . '/framework/asset/site/js/module/packery-mode.pkgd.min.js', array( 'jquery' ), DIMA_VERSION, false );
				wp_enqueue_script( 'packery-mode.pkgd' );

				wp_register_script( 'perfect-scrollbar', DIMA_TEMPLATE_URL . '/framework/asset/site/js/module/perfect-scrollbar.js', array( 'jquery' ), DIMA_VERSION, false );
				wp_enqueue_script( 'perfect-scrollbar' );

				wp_register_script( 'skrollr', DIMA_TEMPLATE_URL . '/framework/asset/site/js/module/skrollr.js', array( 'jquery' ), DIMA_VERSION, false );
				wp_enqueue_script( 'skrollr' );

				wp_register_script( 'sly', DIMA_TEMPLATE_URL . '/framework/asset/site/js/module/sly.min.js', array( 'jquery' ), DIMA_VERSION, false );
				wp_enqueue_script( 'sly' );

				wp_register_script( 'theia-sticky-sidebar', DIMA_TEMPLATE_URL . '/framework/asset/site/js/module/theia-sticky-sidebar.min.js', array( 'jquery' ), DIMA_VERSION, false );
				wp_enqueue_script( 'theia-sticky-sidebar' );

				wp_register_script( 'waves', DIMA_TEMPLATE_URL . '/framework/asset/site/js/module/waves.js', array( 'jquery' ), DIMA_VERSION, false );
				wp_enqueue_script( 'waves' );

				wp_register_script( 'waypoints-js', DIMA_TEMPLATE_URL . '/framework/asset/site/js/module/waypoints.min.js', array( 'jquery' ), DIMA_VERSION, false );
				wp_enqueue_script( 'waypoints-js' );
			} else {
				wp_register_script( 'dima-js-lib', DIMA_TEMPLATE_URL . '/framework/asset/site/js/libs.min.js', array( 'jquery' ), DIMA_VERSION, true );
				wp_enqueue_script( 'dima-js-lib' );
			}

			wp_register_script( 'jquery.easing', DIMA_TEMPLATE_URL . '/framework/asset/site/js/core/jquery.easing.1.3.js', array( 'jquery' ), DIMA_VERSION, true );
			wp_enqueue_script( 'jquery.easing' );

			wp_register_script( 'imagesloaded.pkgd', DIMA_TEMPLATE_URL . '/framework/asset/site/js/core/imagesloaded.pkgd.min.js', array( 'jquery' ), DIMA_VERSION, true );
			wp_enqueue_script( 'imagesloaded.pkgd' );

			wp_register_script( 'velocity', DIMA_TEMPLATE_URL . '/framework/asset/site/js/core/velocity.min.js', array( 'jquery' ), DIMA_VERSION, true );
			wp_enqueue_script( 'velocity' );

			wp_register_script( 'velocity.ui', DIMA_TEMPLATE_URL . '/framework/asset/site/js/core/velocity.ui.min.js', array( 'jquery' ), DIMA_VERSION, true );
			wp_enqueue_script( 'velocity.ui' );

			wp_register_script( 'smoothscroll', DIMA_TEMPLATE_URL . '/framework/asset/site/js/core/smoothscroll.js', array( 'jquery' ), DIMA_VERSION, true );
			if ( $dima_smoothscroll == '1' ) {
				wp_enqueue_script( 'smoothscroll' );
			}

			wp_register_script( 'respond.src', DIMA_TEMPLATE_URL . '/framework/asset/site/js/core/respond.src.js', array( 'jquery' ), DIMA_VERSION, true );
			wp_enqueue_script( 'respond.src' );

			//depending on shortcodes
			wp_register_script( 'video-js', DIMA_TEMPLATE_URL . '/framework/asset/site/js/specific/video.js', array( 'jquery' ), DIMA_VERSION, true );
			wp_register_script( 'bigvideo-js', DIMA_TEMPLATE_URL . '/framework/asset/site/js/specific/bigvideo.js', array( 'jquery' ), DIMA_VERSION, true );

			wp_register_script( 'jquery.magnific-popup', DIMA_TEMPLATE_URL . '/framework/asset/site/js/specific/jquery.magnific-popup.min.js', array( 'jquery' ), DIMA_VERSION, true );
			wp_enqueue_script( 'jquery.magnific-popup' );

			wp_register_script( 'jquery.flexslider', DIMA_TEMPLATE_URL . '/framework/asset/site/js/specific/jquery.flexslider.js', array( 'jquery' ), DIMA_VERSION, true );

			wp_register_script( 'slick.min', DIMA_TEMPLATE_URL . '/framework/asset/site/js/specific/slick.min.js', array( 'jquery' ), DIMA_VERSION, true );
			wp_enqueue_script( 'slick.min' );

			wp_register_script( 'jquery.particleground', DIMA_TEMPLATE_URL . '/framework/asset/site/js/specific/jquery.particleground.min.js', array( 'jquery' ), DIMA_VERSION, true );
			wp_register_script( 'dima-particles', DIMA_TEMPLATE_URL . '/framework/asset/site/js/specific/dima_canvas_bg_style_2.js', array( 'jquery' ), DIMA_VERSION, true );

			if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
				wp_enqueue_script( 'comment-reply' );
			}

			wp_register_script( 'okvideo-js', DIMA_TEMPLATE_URL . '/framework/asset/site/js/specific/okvideo.js', array( 'jquery' ), DIMA_VERSION, true );
			wp_enqueue_script( 'okvideo-js' );

			if ( dima_helper::dima_is_woo() ) {
				wp_register_script( 'dima-js-woocommerce', DIMA_TEMPLATE_URL . '/framework/asset/site/js/woocommerce.js', array( 'jquery' ), DIMA_VERSION, true );
				wp_enqueue_script( 'dima-js-woocommerce' );
			}

			wp_register_script( 'dima-js-main', DIMA_TEMPLATE_URL . '/framework/asset/site/js/main-es6.js', array( 'jquery' ), DIMA_VERSION, true );
			wp_enqueue_script( 'dima-js-main' );

			if ( dima_helper::dima_am_i_true( dima_helper::dima_get_option( "dima_fontawesome_five" ) ) ) {
				wp_register_script( 'fontawesome-all', 'https://use.fontawesome.com/releases/v5.0.0/js/all.js', array( 'jquery' ), DIMA_VERSION, true );
				wp_enqueue_script( 'fontawesome-all' );
				wp_register_script( 'v4-shims', 'https://use.fontawesome.com/releases/v5.0.0/js/v4-shims.js', array( 'jquery' ), DIMA_VERSION, true );
				wp_enqueue_script( 'v4-shims' );
			}

			$js_vars = array(
				'is_rtl'              => is_rtl(),
				'sticky_behavior'     => 'default',
				'DIMA_TEMPLATE_URL'   => DIMA_TEMPLATE_URL,
				'is_lazy_image'       => dima_helper::dima_am_i_true( dima_helper::dima_get_option( 'dima_lazy_image' ) ),
				'is_singular'         => is_singular(),
				'ad_blocker_detector' => dima_helper::dima_am_i_true( dima_helper::dima_get_option( 'dima_ad_blocker_detector' ) ),
			);

			wp_localize_script( 'jquery', 'pxdm_js', $js_vars );

			do_action( 'dima_website_schemas' );
			do_action( 'dima_action_organization_schemas' );

		}
	}

endif;

if ( ! function_exists( 'dima_admin_scripts_meta_box' ) ) :
	function dima_admin_scripts_meta_box( $hook ) {
		GLOBAL $wp_customize;

		if ( isset( $wp_customize ) ) {
			return;
		}

		wp_enqueue_script( DIMA_THEME_NAME . '-pixeldima-global-js', DIMA_TEMPLATE_URL . '/framework/asset/admin/js/pixeldima-global.js', array( 'jquery' ), DIMA_VERSION, true );
		wp_enqueue_script( 'pixeldima-popper', DIMA_TEMPLATE_URL . '/framework/asset/admin/js//popper.js', DIMA_VERSION, true );
		wp_enqueue_script( 'pixeldima-tippy', DIMA_TEMPLATE_URL . '/framework/asset/admin/js/tippy.standalone.js', array( 'jquery' ), DIMA_VERSION, true );

		if ( strpos( $hook, 'pixeldima-customizer-backup' ) != false ) {
			wp_enqueue_script( 'dima-customizer-backup-js', DIMA_TEMPLATE_URL . '/framework/asset/admin/js/dima-customizer-backup.js', array( 'jquery' ), DIMA_VERSION, true );
		}

		if ( strpos( $hook, 'pixeldima-demo' ) != false ) {

			wp_register_script( 'dima-demo-content-js', DIMA_TEMPLATE_URL . '/framework/asset/admin/js/dima-demo-content.js', array( 'jquery' ), DIMA_VERSION, true );
			wp_localize_script( 'dima-demo-content-js', 'DimaDemoAlert', array(
				'msg_debug'    => ( defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ),
				'msg_start'    => esc_html__( 'Let&apos;s go!', 'noor' ),
				'msg_working'  => esc_html__( 'Working...!', 'noor' ),
				'msg_timeoutA' => esc_html__( 'Please wait...!', 'noor' ),
				'msg_timeoutB' => esc_html__( 'Hang in there, trying to reconnect...', 'noor' ),
				'msg_timeoutC' => esc_html__( 'Experiencing technical difficulties...', 'noor' ),
				'msg_fail'     => esc_html__( 'We&apos;re sorry, but the demo data could not be imported.', 'noor' ),
				'msg_complete' => esc_html__( 'All done. Have fun!', 'noor' ),
			) );
			wp_enqueue_script( 'dima-demo-content-js' );
		}

		if ( $hook == 'post.php' || $hook == 'post-new.php' || $hook == 'edit-tags.php' ) {
			wp_enqueue_script( 'dima-meta-js', DIMA_TEMPLATE_URL . '/framework/asset/admin/js/dima-meta-boxes.js', array(
				'jquery',
				'media-upload',
				'thickbox'
			), DIMA_VERSION, true );
		}

		wp_enqueue_script( 'dima-cmb2-types', DIMA_TEMPLATE_URL . '/framework/asset/admin/js/cmb2-dima-types.js', array( 'jquery-ui-tabs' ), DIMA_VERSION, true );
		wp_enqueue_script( 'dima-cmb2-rgba', DIMA_TEMPLATE_URL . '/framework/asset/admin/js/jw-cmb2-rgba-picker.js', array( 'wp-color-picker' ), DIMA_VERSION, true );
		if ( $hook == 'post.php' || $hook == 'post-new.php' ) {
			wp_enqueue_script( 'jquery-ui-datepicker' );
		}

	}
endif;

if ( ! function_exists( 'dima_customizer_control_scripts' ) ) :
	function dima_customizer_control_scripts() {
		wp_enqueue_script( 'dima-customizer-control-js', DIMA_TEMPLATE_URL . '/framework/asset/admin/js/dima-custom-controls.js', array( 'jquery' ), DIMA_VERSION, false );
		wp_enqueue_script( 'wp-color-picker' );
		wp_enqueue_style( 'wp-color-picker' );
		wp_enqueue_script( 'wp-color-picker-alpha', DIMA_TEMPLATE_URL . '/framework/asset/admin/js/wp-color-picker-alpha.js', array(
			'jquery',
			'wp-color-picker'
		), DIMA_VERSION, true );
	}
endif;


if ( ! function_exists( 'dima_customizer_scripts' ) ) :
	function dima_customizer_scripts() {
		wp_enqueue_script( 'dima-customizer-js', DIMA_TEMPLATE_URL . '/framework/asset/admin/js/dima-theme-customizer.js', array(
			'jquery',
			'customize-preview'
		), DIMA_VERSION, true );
	}
endif;



