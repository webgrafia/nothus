<?php
/**
 * Get Noor Layout Options
 *
 * @package Dima Framework
 * @subpackage Functions
 * @version   1.0.0
 * @since     1.0.0
 * @author    PixelDima <info@pixeldima.com>
 *
 */


/**
 * [Checks if the global content layout is "full-width" Or "mini",
 * then it go through all possible pages to determine the correct layout for that template]
 */
function dima_get_content_layout() {

	$default_content_layout     = dima_helper::dima_get_option( 'dima_layout_content' );
	$default_bbp_content_layout = dima_helper::dima_get_option( 'dima_layout_bbpress_content' );
	$default_bp_content_layout  = dima_helper::dima_get_option( 'dima_layout_bp_content' );
	$section_layout             = dima_get_section_layout_meta();

	if ( $section_layout == 'full-width' && ( is_singular( 'page' ) || is_singular( 'dima-portfolio' ) ) ) {
		return "no-sidebar";
	}

	//with sidebar
	if ( is_page_template( 'template-right-sidebar.php' ) ) {
		$layout = 'right-sidebar';
	} elseif ( is_page_template( 'template-left-sidebar.php' ) ) {
		$layout = 'left-sidebar';
	} elseif ( is_page_template( 'template-no-sidebar.php' ) ) {
		$layout = 'no-sidebar';
	} elseif ( is_home() ) {
		$opt    = dima_helper::dima_get_option( 'dima_blog_layout' );
		$layout = ( $opt == '' ) ? $default_content_layout : $opt;
	} elseif ( dima_helper::dima_is_product() ) {
		$layout = dima_helper::dima_get_inherit_option( '_dima_meta_layout', 'dima_shop_layout' );
	} elseif ( dima_helper::dima_is_bbpress() ) {
		$layout = $default_bbp_content_layout;
	} elseif ( dima_helper::dima_is_buddypress() ) {
		$layout = $default_bp_content_layout;
	} elseif ( dima_helper::dima_is_single_portfolio() ) {
		$layout = dima_helper::dima_get_inherit_option( '_dima_meta_layout', 'dima_layout_projects_details_content' );
	} elseif ( dima_helper::dima_is_dimablock() ) {
		$layout = "full-width";
	} elseif ( is_single() && is_singular() ) {
		$opt    = dima_helper::dima_get_option( 'dima_blog_layout' );
		$layout = ( $opt == '' || $opt == 'inherit' ) ? $default_content_layout : dima_helper::dima_get_inherit_option( '_dima_meta_layout', 'dima_blog_layout' );
	} elseif ( is_singular() ) {
		$layout = dima_helper::dima_get_inherit_option( '_dima_meta_layout', 'dima_layout_content' );
	} elseif ( is_404() ) {
		$layout = 'full-width';
	} elseif ( is_archive() ) {
		if ( dima_helper::dima_is_shop() || dima_helper::dima_is_product_category() || dima_helper::dima_is_product_tag() ) {
			$opt    = dima_helper::dima_get_option( 'dima_shop_layout' );
			$layout = ( $opt == '' ) ? $default_content_layout : $opt;
		} else {
			$layout = $default_content_layout;
		}
	} else {
		$layout = $default_content_layout;
	}

	if ( $layout == '' ) {
		$layout = $default_content_layout;
	}

	return $layout;
}

/**
 * Get Section Layout
 * Based on options selected in metabox.
 */
/**
 * @return string
 */
function dima_get_section_layout_meta( $default_section_layout = "" ) {

	if ( is_singular() ) {
		$meta                   = dima_helper::dima_am_i_true( get_post_meta( get_the_ID(), '_dima_meta_section_fullwidth', true ) );
		$default_section_layout = ( $meta ) ? 'full-width' : $default_section_layout;
	}

	return $default_section_layout;
}

/**
 * Get Site Layoutclasses based on options selected in customization and metabox
 */

function dima_get_site_layout() {
	$default_site_layout = dima_helper::dima_get_option( 'dima_layout_site' );
	if ( is_home() ) {
		$layout = $default_site_layout;
	} elseif ( is_singular() ) {
		$layout = dima_helper::dima_get_inherit_option( '_dima_meta_site_layout', 'dima_layout_site' );
	} else {
		$layout = $default_site_layout;
	}

	return $layout;
}


/**
 *  views : Comments.
 *
 * @param $comment
 * @param $args
 * @param $depth
 */
function dima_noor_comment( $comment, $args, $depth ) {
	GLOBAL $post;
	?>
    <li id="li-comment-<?php comment_ID(); ?>" <?php comment_class(); ?>>
    <div class="content-comment post-content">
		<?php
		printf(
			'<div class="dima-comment-img">%1$s %2$s</div>',
			'<span class="avatar-wrap cf circle">' . get_avatar( $comment, 120 ) . '</span>', ( $comment->user_id === $post->post_author ) ? '<span class="dima-post-author">' . esc_html__( 'Author', 'noor' ) . '</span>' : ''
		);

		?>
        <div class="dima-comment-entry">
            <article id="comment-<?php comment_ID(); ?>">
                <header class="dima-comment-header clearfix">
                    <ul class="user-comment-titel float-start">
                        <li>
							<?php printf( '<cite class="dima-comment-author">%1$s</cite>', get_comment_author_link() ); ?>
                        </li>
						<?php
						printf( '<li><a href="%1$s"><time class="comment-time" datetime="%2$s">%3$s</time></a></li>',
							esc_url( get_comment_link( $comment->comment_ID ) ),
							get_comment_time( 'c' ),
							sprintf( esc_html__( '%1$s at %2$s', 'noor' ),
								get_comment_date(), get_comment_time() ) );
						?>
                    </ul>
                    <ul class="user-comment-titel float-end">
                        <li>
                            <div class="dima-reply">
								<?php

								if ( comments_open( $post->ID ) && $depth < $args['max_depth'] ) {
									echo wp_kses( dima_get_svg_icon( "ic_reply" ), dima_helper::dima_get_allowed_html_tag() ) ?>
									<?php echo "  ";
									$allowed_tags = array(
										'span' => array(),
									);

									comment_reply_link( array_merge( $args, array(
										'reply_text' => wp_kses( __( 'Reply <span class="comment-reply-link-after"></span>', 'noor' ), $allowed_tags ),
										'depth'      => $depth,
										'max_depth'  => $args['max_depth']
									) ) );
								}
								?>
                            </div>
                        </li>
                        <li><?php echo wp_kses( dima_get_svg_icon( "ic_mode_edit" ), dima_helper::dima_get_allowed_html_tag() );
							edit_comment_link( esc_html__( 'Edit', 'noor' ) ); ?></li>

                    </ul>
                </header>
				<?php if ( '0' == $comment->comment_approved ): ?>
                    <p class="dima-comment-awaiting-moderation"><?php esc_html_e( 'Your comment is awaiting moderation.', 'noor' ); ?></p>
				<?php endif; ?>
                <div class="dima-comment-content">
					<?php comment_text(); ?>
                </div>
            </article>
        </div>
    </div>
	<?php
}

/**
 * dima_portfolio_filters Display list of filters by category
 *
 * @param array  $filters_array
 * @param string $type
 */
function dima_portfolio_filters( $filters_array = array(), $type = "portfolio" ) {

	if ( ! DIMA_NOUR_ASSISTANT_IS_ACTIVE ) {
		return;
	}
	global $post;
	$show_filters   = $filters_array['filters'];
	$all            = $filters_array['hide_all'];
	$i              = 0;
	$just_those_cat = array();
	if ( is_archive() ) {
		if ( ( $post->post_type == 'dima-portfolio' && DIMA_NOUR_ASSISTANT_IS_ACTIVE ) && is_tax( 'portfolio-category' ) || is_tax( 'portfolio-tag' ) ) {
			$show_filters = false;
		}
	}
	if ( $filters_array['category'] != '' ) {
		$just_those_cat = explode( ',', preg_replace( '/\s+/', '', ( $filters_array['category'] ) ) );
	}
	if ( $type == "portfolio" ) {
		$terms = get_terms( 'portfolio-category' );
		$class = ".portfolio-category-";
	} else {
		$terms = get_terms( 'product_cat' );
		$class = ".product_cat-";
	}
	if ( ! $all ) {
		$current = 'class="current"';
	} else {
		$current = '';
	}

	$ARG_ARRAY = array(
		'animation'      => 'transition.slideUpBigIn',
		'delay'          => 30,
		'delay_offset'   => '98%',
		'delay_duration' => 750
	);

	$animation_data = '';
	$animation_data .= ( $ARG_ARRAY['animation'] != '' ) ? 'data-animate=' . esc_attr( $ARG_ARRAY['animation'] ) . '' : '';
	$animation_data .= ( $ARG_ARRAY['delay'] != '' ) ? ' data-delay=' . esc_attr( $ARG_ARRAY['delay'] ) . '' : '';
	$animation_data .= ( $ARG_ARRAY['delay_offset'] != '' ) ? ' data-offset=' . esc_attr( $ARG_ARRAY['delay_offset'] ) . '' : '';
	$animation_data .= ( $ARG_ARRAY['delay_duration'] != '' ) ? ' data-duration=' . esc_attr( $ARG_ARRAY['delay_duration'] ) . '' : '';

	if ( dima_helper::dima_am_i_true( $show_filters ) ) {
		?>
        <nav class="filters-box filters" id="filters" data-enable-isotope="1" <?php echo( $animation_data ); ?>>
            <ul>
				<?php if ( $all ) { ?>
                    <li class="current"><a data-filter="*"><?php esc_html_e( 'All', 'noor' ); ?></a></li>
				<?php }

				?>

				<?php foreach ( $terms as $term ) {
					$term_class = sanitize_html_class( $term->slug, $term->term_id );
					if ( is_numeric( $term_class ) || ! trim( $term_class, '-' ) ) {
						$term_class = $term->term_id;
					}

					if ( $i > 0 ) {
						$current = '';
					}
					if ( is_array( $just_those_cat ) && ! empty( $just_those_cat ) ) {
						if ( in_array( $term->term_id, $just_those_cat ) ) {
							?>
                            <li <?php echo esc_attr( $current ); ?>>
                                <a data-filter="<?php echo esc_attr( $class ) . esc_attr( $term_class ); ?>"><?php echo esc_attr( $term->name ); ?>
                                    <span><?php echo esc_attr( $term->count ) ?></span></a>
                            </li>
							<?php
						}
					} else {

						?>
                        <li <?php echo esc_attr( $current ); ?>>
                            <a data-filter="<?php echo esc_attr( $class ) . esc_attr( $term_class ); ?>"><?php echo esc_attr( $term->name ); ?>
                                <span><?php echo esc_attr( $term->count ) ?></span></a>
                        </li>
					<?php }
					$i ++;
				} ?>
            </ul>
        </nav>
		<?php

	}
}