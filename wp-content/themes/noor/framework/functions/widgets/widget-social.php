<?php

/**
 * Class DIMA_Social_Widget.
 *
 * @package Dima Framework
 * @subpackage Widget
 * @version   1.0.0
 * @since     1.0.0
 * @author    PixelDima <info@pixeldima.com>
 *
 */
class DIMA_Social_Widget extends WP_Widget {

	function __construct() {
		$widget_ops  = array( 'classname' => 'social-icons-widget' );
		$control_ops = array( 'width' => 250, 'height' => 400 );
		parent::__construct( false, $name = DIMA_THEME_NAME . ' - ' . esc_html__( 'Social Icons', 'noor' ), $widget_ops, $control_ops );
	}

	function widget( $args, $instance ) {
		extract( $args );
		$instance = wp_parse_args( (array) $instance, self::get_defaults() );
		$title  = empty( $instance['title'] ) ? '' : apply_filters( 'widget_title', $instance['title'] );
		$circle = empty( $instance['circle'] ) ? '' : 'circle-social';
		$size   = empty( $instance['size'] ) ? 'medium' : $instance['size'];
		$float  = empty( $instance['float'] ) ? 'start' : $instance['float'];

		$bottom_margin = '';


		echo( $before_widget );

		if ( ! empty( $title ) ) {
			echo ( $before_title ) . esc_attr( $title ) . ( $after_title );
		};
		?>
        <div class="social-media fill-icon dima_add_hover social-<?php echo esc_attr( $size );
		echo ' ';
		echo esc_attr( $circle );
		echo ' ';
		echo esc_attr( $bottom_margin );
		echo ' float-' . esc_attr( $float );
		?>">
            <ul class="inline clearfix">
				<?php
				dima_helper::dima_get_global_social();
				?>
            </ul>
        </div>
		<?php
		echo( $after_widget );

	}

	function update( $new_instance, $instance ) {
		$instance['title']  = trim( strip_tags( $new_instance['title'] ) );
		$instance['circle'] = isset( $new_instance['circle'] ) ? 1 : 0;
		$instance['size']   = strip_tags( $new_instance['size'] );
		$instance['float']  = strip_tags( $new_instance['float'] );
		$updated_instance   = wp_parse_args( (array) $instance, self::get_defaults() );

		return $updated_instance;
	}

	function form( $instance ) {
		$instance = wp_parse_args( (array) $instance, self::get_defaults() );
		?>
        <p>
            <label
                    for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_html_e( 'Title:', 'noor' ) ?></label>
            <input id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"
                   name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>"
                   value="<?php if ( ! empty( $instance['title'] ) ) {
				       echo esc_attr( $instance['title'] );
			       } ?>" class="widefat"
                   type="text"/>
        </p>
        <p>
            <label
                    for="<?php echo esc_attr( $this->get_field_id( 'circle' ) ); ?>"><?php esc_html_e( 'Circle Icon:', 'noor' ) ?></label>
            <input id="<?php echo esc_attr( $this->get_field_id( 'circle' ) ); ?>"
                   name="<?php echo esc_attr( $this->get_field_name( 'circle' ) ); ?>"
                   value="true" <?php checked( $instance['circle'] ); ?> type="checkbox"/>
        </p>
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'size' ) ); ?>"><?php esc_html_e( 'Icon size', 'noor' ); ?>
                :</label>
            <select id="<?php echo esc_attr( $this->get_field_id( 'size' ) ); ?>"
                    name="<?php echo esc_attr( $this->get_field_name( 'size' ) ); ?>" class="widefat">
                <option value="small" <?php selected( 'small', esc_attr( $instance['size'] ) ) ?>><?php esc_html_e( 'Small', 'noor' ); ?></option>
                <option value="medium" <?php selected( 'medium', esc_attr( $instance['size'] ) ) ?>><?php esc_html_e( 'Medium', 'noor' ); ?></option>
                <option value="big" <?php selected( 'big', esc_attr( $instance['size'] ) ) ?>><?php esc_html_e( 'Big', 'noor' ); ?></option>
            </select>
        </p>
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'float' ) ); ?>"><?php esc_html_e( 'Position', 'noor' ); ?>
                :</label>
            <select id="<?php echo esc_attr( $this->get_field_id( 'float' ) ); ?>"
                    name="<?php echo esc_attr( $this->get_field_name( 'float' ) ); ?>" class="widefat">
                <option value="start" <?php selected( 'start', esc_attr( $instance['float'] ) ) ?>><?php esc_html_e( 'Start', 'noor' ); ?></option>
                <option value="center" <?php selected( 'center', esc_attr( $instance['float'] ) ) ?>><?php esc_html_e( 'Center', 'noor' ); ?></option>
                <option value="end" <?php selected( 'end', esc_attr( $instance['float'] ) ) ?>><?php esc_html_e( 'End', 'noor' ); ?></option>
            </select>
        </p>
		<?php
	}

	/**
	 * Render an array of default values.
	 *
	 * @return array default values
	 */
	private static function get_defaults() {

		$defaults = array(
			'title'  => esc_html__( 'Social', 'noor' ),
			'size'   => 'medium',
			'float'  => 'start',
			'circle' => 1,
			'dark'   => 0,
		);

		return $defaults;
	}
}

add_action( 'widgets_init', 'DIMA_Social_Widget_box' );
function DIMA_Social_Widget_box() {
	register_widget( 'DIMA_Social_Widget' );
}

?>