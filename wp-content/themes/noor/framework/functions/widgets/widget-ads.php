<?php

/**
 * Class DIMA_Ads_125_Widget.
 * Class DIMA_Ads_300_Widget.
 *
 * @package Dima_Framework
 * @subpackage widgets
 * @version   1.0.0
 * @since     1.0.0
 * @author    PixelDima <info@pixeldima.com>
 * @copyright    (c) Copyright by PixelDima
 *
 */
class DIMA_Ads_125_Widget extends WP_Widget {
	function __construct() {
		$widget_ops  = array(
			'description' => esc_html__( 'Displays Advertisements', 'noor' ),
			'classname'   => 'dima-dsa'
		);
		$control_ops = array( 'width' => 250, 'height' => 400 );
		parent::__construct( false, $name = DIMA_THEME_NAME . ' - ' . esc_html__( 'Advertisements (125 x 125)', 'noor' ), $widget_ops, $control_ops );
	}

	/**
	 * Creating widget front-end / This is where the action happens
	 *
	 * @param array $args
	 * @param array $instance
	 */
	function widget( $args, $instance ) {
		extract( $args );
		$title    = empty( $instance['title'] ) ? esc_html__( 'Advertisement', 'noor' ) : apply_filters( 'widget_title', $instance['title'] );
		$nofollow = esc_attr( $instance['nofollow'] );
		$target   = ' target="_blank" rel="noopener" ';

		if ( $nofollow ) {
			$nofollow = 'rel="nofollow"';
		} else {
			$nofollow = '';
		}

		echo( $before_widget );

		if ( $title ) {
			echo ( $before_title ) . esc_attr( $title ) . ( $after_title );
		}

		?>

        <div <?php
		echo 'id="' . esc_attr( $args['widget_id'] ) . '"';
		?>
                class="clearfix dima-dsa-widget-content dima-dsa125-widget">
            <ul>
				<?php for ( $i = 0; $i < 10; $i ++ ) { ?>
					<?php if ( ! empty( $instance[ 'ads' . esc_attr( $i ) . '_code' ] ) ) { ?>
                        <li class="dima-dsa-cell">
							<?php echo do_shortcode( $instance[ 'ads' . esc_attr( $i ) . '_code' ] ); ?>
                        </li>
					<?php } elseif ( ! empty( $instance[ 'ads' . esc_attr( $i ) . '_img' ] ) ) { ?>
                        <li class="dima-dsa-cell">
							<?php if ( $instance[ 'ads' . esc_attr( $i ) . '_url' ] ){ ?><a
                                    href="<?php echo esc_url( $instance[ 'ads' . esc_attr( $i ) . '_url' ] ) ?>" <?php echo esc_attr( $target ) ?> <?php echo esc_attr( $nofollow ) ?>><?php } ?>
                                <img src="<?php echo esc_url( $instance[ 'ads' . esc_attr( $i ) . '_img' ] ) ?>"
                                     width="125"
                                     height="125" alt=""/>
								<?php if ( $instance[ 'ads' . esc_attr( $i ) . '_url' ] ){ ?></a><?php } ?>
                        </li>
						<?php
					}
				} ?>
            </ul>
        </div>
		<?php
		echo( $after_widget );
	}

	/**
	 * Updating widget replacing old instances with new
	 *
	 * @param array $new_instance
	 * @param array $old_instance
	 *
	 * @return array
	 */
	function update( $new_instance, $old_instance ) {
		$instance             = $old_instance;
		$instance['title']    = strip_tags( $new_instance['title'] );
		$instance['nofollow'] = strip_tags( $new_instance['nofollow'] );

		for ( $i = 0; $i < 10; $i ++ ) {
			$instance[ 'ads' . esc_attr( $i ) . '_img' ]  = strip_tags( $new_instance[ 'ads' . esc_attr( $i ) . '_img' ] );
			$instance[ 'ads' . esc_attr( $i ) . '_url' ]  = strip_tags( $new_instance[ 'ads' . esc_attr( $i ) . '_url' ] );
			$instance[ 'ads' . esc_attr( $i ) . '_code' ] = $new_instance[ 'ads' . esc_attr( $i ) . '_code' ];
		}

		return $instance;
	}

	/**
	 * Creates the form for the widget
	 */
	function form( $instance ) {
		$defaults = array( 'title' => esc_html__( 'Advertisement', 'noor' ) );
		$instance = wp_parse_args( (array) $instance, $defaults ); ?>

        <p>
            <label
                    for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_attr_e( 'Title:', 'noor' ) ?></label>
            <input id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"
                   name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>"
                   value="<?php if ( ! empty( $instance['title'] ) ) {
				       echo esc_attr( $instance['title'] );
			       } ?>" class="widefat"
                   type="text"/>
        </p>
        <p>
            <label
                    for="<?php echo esc_attr( $this->get_field_id( 'nofollow' ) ); ?>"><?php esc_attr_e( 'Nofollow:', 'noor' ) ?></label>
            <input id="<?php echo esc_attr( $this->get_field_id( 'nofollow' ) ); ?>"
                   name="<?php echo esc_attr( $this->get_field_name( 'nofollow' ) ); ?>"
                   value="true" <?php if ( ! empty( $instance['nofollow'] ) ) {
				echo 'checked="checked"';
			} ?> type="checkbox"/>
        </p>
		<?php
		for ( $i = 0; $i < 10; $i ++ ) { ?>
            <strong><?php esc_attr_e( 'ADS', 'noor' ) ?> <?php echo esc_attr( $i ); ?> :</strong>
            <p>
                <label
                        for="<?php echo esc_attr( $this->get_field_id( 'ads' . esc_attr( $i ) . '_img' ) ); ?>"><?php esc_attr_e( 'Ad image URL:', 'noor' ) ?></label>
                <input id="<?php echo esc_attr( $this->get_field_id( 'ads' . esc_attr( $i ) . '_img' ) ); ?>"
                       name="<?php echo esc_attr( $this->get_field_name( 'ads' . esc_attr( $i ) . '_img' ) ); ?>"
                       value="<?php if ( ! empty( $instance[ 'ads' . esc_attr( $i ) . '_img' ] ) ) {
					       echo esc_attr( $instance[ 'ads' . esc_attr( $i ) . '_img' ] );
				       } ?>"
                       class="widefat" type="text"/>
            </p>
            <p>
                <label
                        for="<?php echo esc_attr( $this->get_field_id( 'ads' . esc_attr( $i ) . '_url' ) ); ?>"><?php esc_attr_e( 'Ad Link URL:', 'noor' ) ?></label>
                <input id="<?php echo esc_attr( $this->get_field_id( 'ads' . esc_attr( $i ) . '_url' ) ); ?>"
                       name="<?php echo esc_attr( $this->get_field_name( 'ads' . esc_attr( $i ) . '_url' ) ); ?>"
                       value="<?php if ( ! empty( $instance[ 'ads' . esc_attr( $i ) . '_url' ] ) ) {
					       echo esc_attr( $instance[ 'ads' . esc_attr( $i ) . '_url' ] );
				       } ?>"
                       class="widefat" type="text"/>
            </p>
            <p>
                <label
                        for="<?php echo esc_attr( $this->get_field_id( 'ads' . esc_attr( $i ) . '_code' ) ); ?>"><?php esc_attr_e( 'Ad Adsense code:', 'noor' ) ?></label>
                <textarea id="<?php echo esc_attr( $this->get_field_id( 'ads' . esc_attr( $i ) . '_code' ) ); ?>"
                          name="<?php echo esc_attr( $this->get_field_name( 'ads' . esc_attr( $i ) . '_code' ) ); ?>"
                          class="widefat"><?php if ( ! empty( $instance[ 'ads' . esc_attr( $i ) . '_code' ] ) ) {
						echo esc_attr( $instance[ 'ads' . esc_attr( $i ) . '_code' ] );
					} ?></textarea>
            </p>
		<?php } ?>
		<?php
	}
}

/**
 * Register and load the widget
 */
function DIMA_Ads_125_Widget_Register() {
	register_widget( 'DIMA_Ads_125_Widget' );
}

add_action( 'widgets_init', 'DIMA_Ads_125_Widget_Register' );


//**********//

class DIMA_Ads_300_Widget extends WP_Widget {
	function __construct() {
		$widget_ops  = array(
			'description' => esc_html__( 'Displays Advertisements', 'noor' ),
			'classname'   => 'dima-dsa'
		);
		$control_ops = array( 'width' => 250, 'height' => 400 );
		parent::__construct( false, $name = DIMA_THEME_NAME . ' - ' . esc_html__( 'Advertisements', 'noor' ), $widget_ops, $control_ops );
	}

	/**
	 * Creating widget front-end / This is where the action happens
	 *
	 * @param array $args
	 * @param array $instance
	 */
	function widget( $args, $instance ) {
		extract( $args );
		$title    = apply_filters( 'widget_title', $instance['title'] );
		$nofollow = esc_attr( $instance['nofollow'] );
		$target   = ' target="_blank" rel="noopener" ';

		if ( $nofollow ) {
			$nofollow = 'rel="nofollow"';
		} else {
			$nofollow = '';
		}

		echo( $before_widget );
		if ( $title ) {
			echo ( $before_title ) . esc_attr( $title ) . ( $after_title );
		}
		?>
        <div <?php
		echo 'id="' . esc_attr( $args['widget_id'] ) . '"';
		?>
                class="clearfix dima-dsa-widget-content dima-dsa300-widget">
			<?php for ( $i = 0; $i < 1; $i ++ ) { ?>
				<?php if ( ! empty( $instance[ 'ads' . esc_attr( $i ) . '_code' ] ) ) { ?>
                    <div class="dima-dsa-cell">
						<?php echo do_shortcode( $instance[ 'ads' . esc_attr( $i ) . '_code' ] ); ?>

                    </div>
				<?php } elseif ( ! empty( $instance[ 'ads' . esc_attr( $i ) . '_img' ] ) ) { ?>
                    <div class="dima-dsa-cell">
						<?php if ( $instance[ 'ads' . esc_attr( $i ) . '_url' ] ){ ?><a
                                href="<?php echo esc_url( $instance[ 'ads' . esc_attr( $i ) . '_url' ] ) ?>" <?php echo esc_attr( $target ) ?> <?php echo esc_attr( $nofollow ) ?>><?php } ?>
                            <img src="<?php echo esc_url( $instance[ 'ads' . esc_attr( $i ) . '_img' ] ) ?>" width="368"
                                 height="280" alt=""/>
							<?php if ( $instance[ 'ads' . esc_attr( $i ) . '_url' ] ){ ?></a><?php } ?>
                    </div>
					<?php
				}
			} ?>
        </div>
		<?php
		echo( $after_widget );
	}

	/**
	 * Updating widget replacing old instances with new
	 *
	 * @param array $new_instance
	 * @param array $old_instance
	 *
	 * @return array
	 */
	function update( $new_instance, $old_instance ) {
		$instance             = $old_instance;
		$instance['title']    = strip_tags( $new_instance['title'] );
		$instance['nofollow'] = strip_tags( $new_instance['nofollow'] );

		for ( $i = 0; $i < 1; $i ++ ) {
			$instance[ 'ads' . esc_attr( $i ) . '_img' ]  = strip_tags( $new_instance[ 'ads' . esc_attr( $i ) . '_img' ] );
			$instance[ 'ads' . esc_attr( $i ) . '_url' ]  = strip_tags( $new_instance[ 'ads' . esc_attr( $i ) . '_url' ] );
			$instance[ 'ads' . esc_attr( $i ) . '_code' ] = $new_instance[ 'ads' . esc_attr( $i ) . '_code' ];
		}

		return $instance;
	}

	/**
	 * Creates the form for the widget
	 */
	function form( $instance ) {
		$defaults = array( 'title' => esc_html__( 'Advertisement', 'noor' ) );
		$instance = wp_parse_args( (array) $instance, $defaults ); ?>

        <p>
            <label
                    for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_attr_e( 'Title:', 'noor' ) ?></label>
            <input id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"
                   name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>"
                   value="<?php if ( ! empty( $instance['title'] ) ) {
				       echo esc_attr( $instance['title'] );
			       } ?>" class="widefat"
                   type="text"/>
        </p>
        <p>
            <label
                    for="<?php echo esc_attr( $this->get_field_id( 'nofollow' ) ); ?>"><?php esc_attr_e( 'Nofollow:', 'noor' ) ?></label>
            <input id="<?php echo esc_attr( $this->get_field_id( 'nofollow' ) ); ?>"
                   name="<?php echo esc_attr( $this->get_field_name( 'nofollow' ) ); ?>"
                   value="true" <?php if ( ! empty( $instance['nofollow'] ) ) {
				echo 'checked="checked"';
			} ?> type="checkbox"/>
        </p>
		<?php
		for ( $i = 0; $i < 1; $i ++ ) { ?>
            <strong><?php esc_attr_e( 'ADS', 'noor' ) ?> <?php echo esc_attr( $i ); ?> :</strong>
            <p>
                <label
                        for="<?php echo esc_attr( $this->get_field_id( 'ads' . esc_attr( $i ) . '_img' ) ); ?>"><?php esc_attr_e( 'Ad image URL:', 'noor' ) ?></label>
                <input id="<?php echo esc_attr( $this->get_field_id( 'ads' . esc_attr( $i ) . '_img' ) ); ?>"
                       name="<?php echo esc_attr( $this->get_field_name( 'ads' . esc_attr( $i ) . '_img' ) ); ?>"
                       value="<?php if ( ! empty( $instance[ 'ads' . esc_attr( $i ) . '_img' ] ) ) {
					       echo esc_attr( $instance[ 'ads' . esc_attr( $i ) . '_img' ] );
				       } ?>"
                       class="widefat" type="text"/>
            </p>
            <p>
                <label
                        for="<?php echo esc_attr( $this->get_field_id( 'ads' . esc_attr( $i ) . '_url' ) ); ?>"><?php esc_attr_e( 'Ad Link URL:', 'noor' ) ?></label>
                <input id="<?php echo esc_attr( $this->get_field_id( 'ads' . esc_attr( $i ) . '_url' ) ); ?>"
                       name="<?php echo esc_attr( $this->get_field_name( 'ads' . esc_attr( $i ) . '_url' ) ); ?>"
                       value="<?php if ( ! empty( $instance[ 'ads' . esc_attr( $i ) . '_url' ] ) ) {
					       echo esc_attr( $instance[ 'ads' . esc_attr( $i ) . '_url' ] );
				       } ?>"
                       class="widefat" type="text"/>
            </p>
            <p>
                <label
                        for="<?php echo esc_attr( $this->get_field_id( 'ads' . esc_attr( $i ) . '_code' ) ); ?>"><?php esc_attr_e( 'Ad Adsense code:', 'noor' ) ?></label>
                <textarea id="<?php echo esc_attr( $this->get_field_id( 'ads' . esc_attr( $i ) . '_code' ) ); ?>"
                          name="<?php echo esc_attr( $this->get_field_name( 'ads' . esc_attr( $i ) . '_code' ) ); ?>"
                          class="widefat"><?php if ( ! empty( $instance[ 'ads' . esc_attr( $i ) . '_code' ] ) ) {
						echo esc_attr( $instance[ 'ads' . esc_attr( $i ) . '_code' ] );
					} ?></textarea>
            </p>
		<?php } ?>
		<?php
	}
}

/**
 * Register and load the widget
 */
function DIMA_Ads_300_Widget_Register() {
	register_widget( 'DIMA_Ads_300_Widget' );
}

add_action( 'widgets_init', 'DIMA_Ads_300_Widget_Register' );
?>