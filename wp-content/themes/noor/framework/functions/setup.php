<?php
/**
 * Sets up theme defaults
 * 1. Languages
 * 2. Add theme support Woo,
 * 3. Menu
 * 4. Image sizes
 *
 * @package Dima Framework
 * @subpackage Admin
 * @version   1.0.0
 * @since     1.0.0
 * @author    PixelDima <info@pixeldima.com>
 */


// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! isset( $content_width ) ) :
	$content_width = dima_helper::dima_get_option( 'dima_content_max_width' );
endif;

function dima_setup_theme() {

	global $dima_theme_image_sizes;

	// Translations
	load_theme_textdomain( 'noor', DIMA_TEMPLATE_PATH . '/languages' );

	//	Title Tag
	add_theme_support( 'title-tag' );

	// Enables Automatic Feed Links for post and comment in the head
	add_theme_support( 'automatic-feed-links' );

	// Allows a theme to register support "Post Formats"
	add_theme_support( 'post-formats', array( 'link', 'gallery', 'quote', 'image', 'video', 'audio', 'chat' ) );

	// Theme support for featured images and thumbnail sizes.
	add_theme_support( 'post-thumbnails' );

	add_theme_support( 'customize-selective-refresh-widgets' );

	add_theme_support( 'html5', array( 'comment-list', 'comment-form', 'search-form', 'gallery', 'caption' ) );

	/*------------------------------*/
	# Adding WooCommerce Support to theme
	/*------------------------------*/
	if ( class_exists( 'WooCommerce' ) ) {
		add_theme_support( 'woocommerce' );
		add_theme_support( 'wc-product-gallery-lightbox' );
		remove_theme_support( 'wc-product-gallery-slider' );
	}

	// WordPress menus.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary Menu', 'noor' ),
		'icon'    => esc_html__( 'Icon Menu', 'noor' ),
		'burger'  => esc_html__( 'Burger', 'noor' ),
		'footer'  => esc_html__( 'Footer Menu', 'noor' )
	) );

	$dima_theme_image_sizes = array(
		'dima-post-standard-image' => array( '870', '575', true ),
		'dima-grid-image'          => array( '585', '385', true ),
		'dima-related-image'       => array( '370', '245', true ),
		'dima-massonry-image'      => array( '585', '9999', false ),
		'dima-big-image-single'    => array( '1170', '9999', false ),//is_singular
	);

	if ( DIMA_USE_LAZY ) {
		$dima_theme_image_sizes['dima-lazy-image'] = array( '100', '9999', false );
	}

	if ( DIMA_NOUR_ASSISTANT_IS_ACTIVE ) {
		$dima_theme_image_sizes['dima-portfolio-grid-image'] = array( '585', '585', true );
	}

	$dima_theme_image_sizes_array = apply_filters( 'dima_filter_theme_image_sizes', $dima_theme_image_sizes );

	if ( is_array( $dima_theme_image_sizes_array ) ) {
		foreach ( $dima_theme_image_sizes as $image_size_name => $image_size_info ) {
			add_image_size( $image_size_name, $image_size_info[0], $image_size_info[1], $image_size_info[2] );
		}
	}
}

add_action( 'after_setup_theme', 'dima_setup_theme' );