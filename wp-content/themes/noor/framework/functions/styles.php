<?php
/**
 * Theme styles , include style bsed on demo name.
 * include google font url.
 *
 * @package Dima Framework
 * @subpackage Functions
 * @version   1.0.0
 * @since     1.0.0
 * @author    PixelDima <info@pixeldima.com>
 *
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! function_exists( 'dima_global_styles' ) ) :

	function dima_global_styles() {
		GLOBAL $dima_customizer_data;
		$is_custom_fonts    = dima_helper::dima_get_option( 'dima_custom_font' ) == '1';
		$dima_css_style_dir = '/framework/asset/site/css/styles/';
		$demo               = DIMA_Demos::get_installed_demo( "noor_main" );
		$ext                = '';
		$min                = dima_helper::dima_get_option( 'dima_minified_files' ) ? '.min' : '';
		$rtl                = is_rtl() ? '-rtl' : '';

		switch ( $demo ) {
			case 'business_modern':
				$demo = 'noor_main';
				break;
			default:
				$demo = 'noor_main';
				break;
		}

		$body_font_family           = dima_helper::dima_get_option( 'dima_body_font_list' );
		$body_font_weight_and_style = dima_helper::dima_get_option( 'dima_body_weights_list' );
		$body_font_weight           = $body_font_weight_and_style;//dima_get_font_weight()

		$btn_font_family           = dima_helper::dima_get_option( 'dima_btn_font_list' );
		$btn_font_weight_and_style = dima_helper::dima_get_option( 'dima_btn_weights_list' );
		$btn_font_weight           = $btn_font_weight_and_style;

		# Heading H -----------------------
		$heading_font_family           = dima_helper::dima_get_option( 'dima_heading_font_list' );
		$heading_font_weight_and_style = dima_helper::dima_get_option( 'dima_heading_weights_list' );
		$heading_font_weight           = $heading_font_weight_and_style;

		$custom_font_args_all = array();
		# Heading H2 -----------------------
		$heading_font_family_2 = dima_helper::dima_get_option( 'dima_heading_font_list_2' );
		if ( $heading_font_family_2 !== $dima_customizer_data["dima_heading_font_list_2"] ) {
			$heading_font_weight_and_style_2 = dima_helper::dima_get_option( 'dima_heading_weights_list_2' );
			$heading_font_weight_2           = $heading_font_weight_and_style_2;
			$custom_font_args_2              = $heading_font_family_2 . ':' . $heading_font_weight_2;
			array_push( $custom_font_args_all, $custom_font_args_2 );
		}

		# Heading H3 -----------------------
		$heading_font_family_3 = dima_helper::dima_get_option( 'dima_heading_font_list_3' );
		if ( $heading_font_family_3 !== $dima_customizer_data["dima_heading_font_list_3"] ) {
			$heading_font_weight_and_style_3 = dima_helper::dima_get_option( 'dima_heading_weights_list_3' );
			$heading_font_weight_3           = $heading_font_weight_and_style_3;
			$custom_font_args_3              = $heading_font_family_3 . ':' . $heading_font_weight_3;
			array_push( $custom_font_args_all, $custom_font_args_3 );
		}

		# Heading H4 -----------------------
		$heading_font_family_4 = dima_helper::dima_get_option( 'dima_heading_font_list_4' );
		if ( $heading_font_family_4 !== $dima_customizer_data["dima_heading_font_list_4"] ) {
			$heading_font_weight_and_style_4 = dima_helper::dima_get_option( 'dima_heading_weights_list_4' );
			$heading_font_weight_4           = $heading_font_weight_and_style_4;
			$custom_font_args_4              = $heading_font_family_4 . ':' . $heading_font_weight_4;
			array_push( $custom_font_args_all, $custom_font_args_4 );
		}

		# Heading H5 -----------------------
		$heading_font_family_5 = dima_helper::dima_get_option( 'dima_heading_font_list_5' );
		if ( $heading_font_family_5 !== $dima_customizer_data["dima_heading_font_list_5"] ) {
			$heading_font_weight_and_style_5 = dima_helper::dima_get_option( 'dima_heading_weights_list_5' );
			$heading_font_weight_5           = $heading_font_weight_and_style_5;
			$custom_font_args_5              = $heading_font_family_5 . ':' . $heading_font_weight_5;
			array_push( $custom_font_args_all, $custom_font_args_5 );
		}

		# Heading H6 -----------------------
		$heading_font_family_6 = dima_helper::dima_get_option( 'dima_heading_font_list_6' );
		if ( $heading_font_family_6 !== $dima_customizer_data["dima_heading_font_list_6"] ) {
			$heading_font_weight_and_style_6 = dima_helper::dima_get_option( 'dima_heading_weights_list_6' );
			$heading_font_weight_6           = $heading_font_weight_and_style_6;
			$custom_font_args_6              = $heading_font_family_5 . ':' . $heading_font_weight_6;
			array_push( $custom_font_args_all, $custom_font_args_6 );
		}

		if ( is_array( $custom_font_args_all ) && ! empty( $custom_font_args_all ) ) {
			$custom_font_args_all = '|' . implode( "|", $custom_font_args_all );
		} else {
			$custom_font_args_all = '';
		}

		$logo_font_family           = dima_helper::dima_get_option( 'dima_logo_font_list' );
		$logo_font_weight_and_style = dima_helper::dima_get_option( 'dima_logo_weights_list' );
		$logo_font_weight           = $logo_font_weight_and_style;

		$navbar_font_family           = dima_helper::dima_get_option( 'dima_navbar_font_list' );
		$navbar_font_weight_and_style = dima_helper::dima_get_option( 'dima_navbar_weights_list' );
		$navbar_font_weight           = $navbar_font_weight_and_style;

		$protocol = is_ssl() ? 'https' : 'http';

		if ( $is_custom_fonts ) {
			$subsets = dima_helper::dima_get_option( 'dima_body_subsets_list' );

			$custom_font_args = array(
				'family' => $body_font_family . ':' . $body_font_weight . '|'
				            . $logo_font_family . ':' . $logo_font_weight . '|'
				            . $navbar_font_family . ':' . $navbar_font_weight . '|'
				            . $btn_font_family . ':' . $btn_font_weight . '|'
				            . $heading_font_family . ':' . $heading_font_weight
				            . $custom_font_args_all,
				'subset' => $subsets
			);

			$body_font_arabic      = dima_is_arabic_font( $body_font_family );
			$btn_font_arabic       = dima_is_arabic_font( $btn_font_family );
			$heading_font_arabic   = dima_is_arabic_font( $heading_font_family );
			$logo_font_arabic      = dima_is_arabic_font( $logo_font_family );
			$menu_font_arabic      = dima_is_arabic_font( $navbar_font_family );
			$heading_font_arabic_2 = $heading_font_arabic_3 = $heading_font_arabic_4 =
			$heading_font_arabic_5 = $heading_font_arabic_6 = '';

			if ( $heading_font_family_2 != '' ) {
				$heading_font_arabic_2 = dima_is_arabic_font( $heading_font_family_2 );
			}
			if ( $heading_font_family_3 != '' ) {
				$heading_font_arabic_3 = dima_is_arabic_font( $heading_font_family_3 );
			}
			if ( $heading_font_family_4 != '' ) {
				$heading_font_arabic_4 = dima_is_arabic_font( $heading_font_family_4 );
			}
			if ( $heading_font_family_4 != '' ) {
				$heading_font_arabic_4 = dima_is_arabic_font( $heading_font_family_4 );
			}
			if ( $heading_font_family_5 != '' ) {
				$heading_font_arabic_5 = dima_is_arabic_font( $heading_font_family_5 );
			}
			if ( $heading_font_family_6 != '' ) {
				$heading_font_arabic_6 = dima_is_arabic_font( $heading_font_family_6 );
			}

			$get_custom_font_family = add_query_arg( $custom_font_args, $protocol . '://fonts.googleapis.com/css' );

			//RTL
			if ( $body_font_arabic != '' ) {
				wp_enqueue_style( 'dima-body-font-custom', $body_font_arabic, null, DIMA_VERSION, 'all' );
			}
			if ( $btn_font_arabic != '' ) {
				wp_enqueue_style( 'dima-btn-font-custom', $btn_font_arabic, null, DIMA_VERSION, 'all' );
			}
			if ( $heading_font_arabic != '' ) {
				wp_enqueue_style( 'dima-heading-font-custom', $heading_font_arabic, null, DIMA_VERSION, 'all' );
			}
			if ( $heading_font_arabic_2 != '' ) {
				wp_enqueue_style( 'dima-heading-font-custom-2', $heading_font_arabic_2, null, DIMA_VERSION, 'all' );
			}
			if ( $heading_font_arabic_3 != '' ) {
				wp_enqueue_style( 'dima-heading-font-custom-3', $heading_font_arabic_3, null, DIMA_VERSION, 'all' );
			}
			if ( $heading_font_arabic_4 != '' ) {
				wp_enqueue_style( 'dima-heading-font-custom-4', $heading_font_arabic_4, null, DIMA_VERSION, 'all' );
			}
			if ( $heading_font_arabic_5 != '' ) {
				wp_enqueue_style( 'dima-heading-font-custom-5', $heading_font_arabic_5, null, DIMA_VERSION, 'all' );
			}
			if ( $heading_font_arabic_6 != '' ) {
				wp_enqueue_style( 'dima-heading-font-custom-6', $heading_font_arabic_6, null, DIMA_VERSION, 'all' );
			}
			if ( $menu_font_arabic != '' ) {
				wp_enqueue_style( 'dima-menu-font-custom', $menu_font_arabic, null, DIMA_VERSION, 'all' );
			}
			if ( $logo_font_arabic != '' ) {
				wp_enqueue_style( 'dima-logo-font-custom', $logo_font_arabic, null, DIMA_VERSION, 'all' );
			}
			//! RTL
			wp_enqueue_style( 'dima-font-custom', $get_custom_font_family, null, DIMA_VERSION, 'all' );
		} else {
			/**
			 * Set default font based on the direction and demo name
			 */

			if ( is_rtl() ) {
				$body_default_font    = 'Droid Arabic Naskh';
				$btn_default_font     = 'Droid Arabic Naskh';
				$heading_default_font = 'Droid Arabic Kufi';
				$subsets              = 'arabic,latin,latin-ext';
			} else {
				switch ( $demo ) {
					case 'noor':
						$body_default_font    = 'Poppins';
						$btn_default_font     = 'Maven Pro';
						$heading_default_font = 'Poppins';
						break;
					default:
						$body_default_font    = 'Poppins';
						$btn_default_font     = 'Maven Pro';
						$heading_default_font = 'Poppins';
						break;
				}
				$subsets = 'latin,latin-ext';
			}

			$menu_default_font = $heading_default_font;

			$default_font_args = array(
				'family' => $body_default_font . ':' . $body_font_weight . '|'
				            . $btn_default_font . ':' . $btn_font_weight . '|'
				            . $heading_default_font . ':' . $logo_font_weight . '|'
				            . $menu_default_font . ':' . $navbar_font_weight . '|'
				            . $heading_default_font . ':' . $heading_font_weight,
				'subset' => $subsets
			);

			$get_default_font_family = add_query_arg( $default_font_args, $protocol . '://fonts.googleapis.com/css' );

			wp_enqueue_style( 'dima-font', $get_default_font_family, null, DIMA_VERSION, 'all' );
		}

		/**include Icons font**/
		//Register styles.
		wp_enqueue_style( DIMA_THEME_NAME . '-style', DIMA_TEMPLATE_URL . $dima_css_style_dir . $demo . '/style' . $ext . $rtl . $min . '.css', array(), DIMA_VERSION );

		if ( dima_helper::dima_am_i_true( dima_helper::dima_get_option( "dima_fontawesome_five" ) ) ) {
			wp_enqueue_style( 'fontawesome', 'https://use.fontawesome.com/releases/v5.0.1/css/all.css', array(), DIMA_VERSION );
		} else {
			wp_enqueue_style( 'fontawesome', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css', array(), DIMA_VERSION );
		}

		wp_register_style( 'mediaelement-css', DIMA_TEMPLATE_URL . $dima_css_style_dir . $demo . '/mediaelementplayer.css', array(), DIMA_VERSION );
		wp_enqueue_style( 'mediaelement-css' );


		if ( DIMA_WC_IS_ACTIVE ) {
			wp_enqueue_style( 'dima-woocommerce-style', DIMA_TEMPLATE_URL . $dima_css_style_dir . $demo . '/woocommerce-style' . $ext . $rtl . $min . '.css', null, DIMA_VERSION, 'screen' );
		}
		if ( DIMA_THE_EVENTS_CALENDAR_ACTIVE ) {
			wp_enqueue_style( 'dima-theeventscalendar-style', DIMA_TEMPLATE_URL . $dima_css_style_dir . $demo . '/dima-events-calenda' . $ext . $rtl . $min . '.css', null, DIMA_VERSION, 'screen' );
		}
		if ( DIMA_BBPRESS_IS_ACTIVE ) {
			wp_enqueue_style( 'dima-bbpress', DIMA_TEMPLATE_URL . $dima_css_style_dir . $demo . '/bbpress-style' . $ext . $min . '.css', null, DIMA_VERSION, 'screen' );
		}
		if ( DIMA_GRAVITY_FORMS_IS_ACTIVE ) {
			wp_enqueue_style( 'dima-gravity-forms', DIMA_TEMPLATE_URL . $dima_css_style_dir . $demo . '/gf-style' . $ext . $min . '.css', null, DIMA_VERSION, 'screen' );
		}
		if ( DIMA_CONTACT_FORM_7_IS_ACTIVE ) {
			add_filter( 'wpcf7_load_css', '__return_false' );
		}
		if ( DIMA_KB_IS_ACTIVE ) {
			wp_enqueue_style( 'dima-gravity-forms', DIMA_TEMPLATE_URL . $dima_css_style_dir . $demo . '/kb-style' . $rtl . $min . '.css', null, DIMA_VERSION, 'screen' );
		}
		if ( DIMA_YITH_WISHLIST_IS_ACTIVE ) {
			wp_enqueue_style( 'dima-wishlist', DIMA_TEMPLATE_URL . $dima_css_style_dir . $demo . '/wishlist' . $rtl . $min . '.css', null, DIMA_VERSION, 'screen' );
		}
	}

endif;

add_action( 'wp_enqueue_scripts', 'dima_global_styles' );

if ( ! function_exists( 'dima_admin_styles' ) ) :
	function dima_admin_styles( $hook ) {
		$rtl = '';
		$rtl .= is_rtl() ? '-rtl' : '';
		//include global admin style for pixeldima
		wp_enqueue_style( 'pixeldima-global', DIMA_TEMPLATE_URL . '/framework/asset/admin/css/pixeldima' . $rtl . '.css', null, DIMA_VERSION, 'all' );
		wp_enqueue_style( 'pixeldima-global' );

		//include meta style
		if ( $hook == 'post.php' || $hook == 'post-new.php' || $hook == 'edit-tags.php' ) {
			wp_enqueue_style( 'dima-meta', DIMA_TEMPLATE_URL . '/framework/asset/admin/css/meta' . $rtl . '.css', null, DIMA_VERSION, 'all' );
		}
		wp_enqueue_style( 'dima-tippy', DIMA_TEMPLATE_URL . '/framework/asset/admin/css/tippy.css', null, DIMA_VERSION, 'all' );
	}
endif;

add_action( 'admin_enqueue_scripts', 'dima_admin_styles' );

if ( ! function_exists( 'customizer_controls' ) ) :
	/**
	 * [Add Style to customizer controls 'customizer-controls.css']
	 */
	function customizer_controls() {
		wp_enqueue_style( 'dima_global_styles', DIMA_TEMPLATE_URL . '/framework/asset/admin/css/customizer-controls.css', array(), DIMA_VERSION, 'screen' );
		//wp_enqueue_style( 'font-awesome', 'https://use.fontawesome.com/releases/v5.0.1/css/all.css', array(), DIMA_VERSION );
	}
endif;

add_action( 'customize_controls_print_styles', 'customizer_controls' );


if ( ! function_exists( 'dima_is_arabic_font' ) ) :
	function dima_is_arabic_font( $font_family ) {
		switch ( $font_family ) {
			case 'Noto Arabic Naskh' :
				return 'http://fonts.googleapis.com/earlyaccess/notonaskharabic.css';
				break;
			case 'Noto Naskh Arabic' :
				return 'http://fonts.googleapis.com/earlyaccess/notonaskharabic.css';
				break;
			case 'Noto Kufi Arabic' :
				return 'http://fonts.googleapis.com/earlyaccess/notokufiarabic.css';
				break;
			case 'Noto Nastaliq Urdu' :
				return 'http://fonts.googleapis.com/earlyaccess/notonastaliqurdu.css';
				break;
			case 'Lateef' :
				return 'https://fonts.googleapis.com/css?family=Lateef';
				break;
			default:
				return '';
		}
	}
endif;