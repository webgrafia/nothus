<?php

/**
 * Define a function to get options and other hellper function.
 *
 * @package Dima Framework
 * @subpackage Functions
 * @version   1.0.0
 * @since     1.0.0
 * @author    PixelDima <info@pixeldima.com>
 *
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


if ( ! class_exists( 'dima_helper' ) ) {
	class dima_helper {
		/**
		 * Display edit button above the post to send the current user to edit page.
		 */
		static function dima_get_admin_edit() {
			$buffy = '';
			if ( current_user_can( 'edit_posts' ) && ! is_single() ) {
				$buffy .= '<a class="dima-admin-edit" href="' . get_edit_post_link() . '">' . dima_get_svg_icon( 'ic_edit' ) . '</a>';
			}

			return $buffy;
		}

		static function dima_get_sticky() {
			$buffy = '';
			if ( current_user_can( 'edit_posts' ) && ! is_single() ) {
				$buffy .= '<a class="dima-admin-sticky" href="' . get_permalink() . '">' . dima_get_svg_icon( 'ic_star' ) . '</a>';
			}

			return $buffy;
		}

		static function dima_get_allowed_html_tag( $allowedposttags = array() ) {
			$my_allowed = wp_kses_allowed_html( 'post' );

			$my_allowed['a'] = array_merge( $my_allowed['a'], array(
				'data-filter'   => array(),
				'data-lightbox' => array(),
				'data-effect'   => array(),
			) );

			$my_allowed['svg'] = array(
				'width'       => array(),
				'height'      => array(),
				'viewbox'     => array(),
				'version'     => array(),
				'xmlns'       => array(),
				'xmlns:xlink' => array(),
			);

			$my_allowed['g'] = array(
				'stroke'       => array(),
				'stroke-width' => array(),
				'fill'         => array(),
				'fill-rule'    => array(),
			);

			$my_allowed['path'] = array(
				'd'  => array(),
				'id' => array(),
			);

			$my_allowed['iframe'] = array(
				'src'             => array(),
				'height'          => array(),
				'width'           => array(),
				'frameborder'     => array(),
				'allowfullscreen' => array(),
				'marginheight'    => array(),
				'marginwidth'     => array(),
				'scrolling'       => array(),
				'style'           => array(),
			);
			// form fields - input
			$my_allowed['input'] = array(
				'class' => array(),
				'id'    => array(),
				'name'  => array(),
				'value' => array(),
				'type'  => array(),
			);
			// select
			$my_allowed['select'] = array(
				'class' => array(),
				'id'    => array(),
				'name'  => array(),
				'value' => array(),
				'type'  => array(),
			);
			// select options
			$my_allowed['option'] = array(
				'selected' => array(),
			);
			// style
			$my_allowed['style'] = array(
				'types' => array(),
			);

			return $my_allowed;
		}

		/**
		 * @param        $option
		 * @param string $default
		 *
		 * @return mixed
		 */
		static function dima_get_option( $option, $default = '' ) {
			GLOBAL $dima_customizer_data;
			if ( $default == '' ) {
				$default = $dima_customizer_data[ $option ];
			}
			$output = get_option( $option, $default );

			return apply_filters( 'dima_option_' . $option, $output );
		}

		/**
		 * Get Demo
		 */
		static function dima_get_template() {
			return dima_helper::dima_get_option( 'dima_template' );
		}

		/**
		 * @param        $str
		 * @param string $term
		 * @param string $taxo
		 *
		 * @return array|string
		 */
		static function dima_get_slug_by_ids( $str, $term = 'term_id', $taxo = 'portfolio-category' ) {
			if ( empty( $str ) ) {
				return null;
			}
			$arr_cat_id   = explode( ',', $str );
			$arr_cat_slug = array();
			foreach ( $arr_cat_id as $key => $cat_id ) {
				$category_ = get_term_by( $term, $cat_id, $taxo );

				if ( is_numeric( $cat_id ) && ! empty( $category_ ) ) {
					$arr_cat_slug[ $key ] = $category_->slug;
				} else {
					$arr_cat_slug[ $key ] = $cat_id;
				}
			}
			$arr_cat_slug = implode( ",", $arr_cat_slug );

			return $arr_cat_slug;
		}

		static function dima_language_selector_flags() {
			if ( self::dima_am_i_true( self::dima_get_inherit_option( '_dima_meta_header_navbar_wpml_lang_show', 'dima_header_navbar_wpml_lang_show' ) ) ) {
				$switcher_html = $flag_html = $active_switcher_html = $active_item = $active_flag = '';

				if ( function_exists( 'wpml_get_active_languages_filter' ) ) {
					$languages = wpml_get_active_languages_filter( 'skip_missing=0&orderby=code' );

					if ( ! empty( $languages ) ) {
						foreach ( $languages as $l ) {
							if ( strcmp( $l['active'], '0' ) != 0 ) {
								$active_item          = $l['translated_name'];
								$active_flag          = $l['country_flag_url'];
								$active_switcher_html = '<a class="dima-topbar-txt" href="' . esc_url( $l['url'] ) . '"><span class="flag" style="background: transparent url(' . esc_url( $active_flag ) . ') center center no-repeat;"></span><span>' . esc_html( $active_item ) . '</span></a>';
							} else {
								$flag_html     = '<span class="flag" style="background: transparent url(' . $l['country_flag_url'] . ') center center no-repeat;"></span>';
								$switcher_html .= '<li>';
								$switcher_html .= '<a href="' . esc_url( $l['url'] ) . '">';
								$switcher_html .= $flag_html;
								$switcher_html .= $l['translated_name'];
								$switcher_html .= '</a>';
								$switcher_html .= '</li>';
							}
						}
					}
				} ?>
                <div class="dima-lan icon_text clearfix">
					<?php echo ( ! empty( $active_switcher_html ) ) ? $active_switcher_html : ''; ?>

					<?php echo ! empty( $switcher_html ) ? '<ul>' . $switcher_html . '</ul>' : ''; ?>
                </div>

				<?php
			} elseif ( self::dima_get_option( 'dima_navbar_lang_shortcode' ) != '' ) {
				echo do_shortcode( self::dima_get_option( 'dima_navbar_lang_shortcode' ) );
			}
		}

		/**
		 * @param $url
		 *
		 * @return array|false
		 */
		static function dima_get_attachment_info_by_url( $url ) {
			if ( $url == '' ) {
				return array();
			}
			$id   = attachment_url_to_postid( $url );
			$info = wp_get_attachment_image_src( $id );

			return $info;
		}

		/**
		 * @param $url
		 *
		 * @return mixed
		 */
		static function dima_protocol_relative( $url ) {
			$new_url = str_replace( array( 'http://', 'https://' ), '//', $url );

			return $new_url;
		}

		/**
		 * @param $option
		 *
		 * @return mixed|void
		 */
		static function dima_get_theme( $option ) {
			$theme_obj = wp_get_theme()->get( $option );

			return apply_filters( 'dima_theme_' . $option, $theme_obj );
		}

		/**
		 * Plugin Exists
		 *
		 * @param $plugin
		 *
		 * @return bool
		 */
		static function dima_is_plugin_exists( $plugin ) {

			if ( file_exists( WP_PLUGIN_DIR . '/' . $plugin ) ) {
				return true;
			} else {
				return false;
			}

		}

		/**
		 * Gets the ID of the current page, post,blog,shop
		 * @return int|mixed|null|void
		 */
		static function dima_get_the_ID() {
			GLOBAL $post;
			if ( is_home() ) {
				$id = get_option( 'page_for_posts' );
			} elseif ( dima_helper::dima_is_shop() ) {
				$id = wc_get_page_id( 'shop' );
			} elseif ( is_404() ) {
				return null;
			} else {
				if ( $post != '' ) {
					$id = $post->ID;
				} else {
					$id = null;
				}
			}

			return $id;
		}

		/**
		 * @param $post_format
		 *
		 * @return string
		 */
		static function dima_get_post_format_thumb( $post_format ) {
			if ( in_array( $post_format, array( 'video', 'quote', 'link', 'audio' ) ) ) {
				$img = 'post-format-thumb-' . $post_format . '.svg';
			} else {
				$img = 'post-format-thumb-text.svg';
			}

			return DIMA_TEMPLATE_URL . '/framework/images/' . $img;
		}

		/*Conditionals*/

		/**
		 * @return bool
		 */
		static function dima_is_single_portfolio() {
			if ( is_singular( 'dima-portfolio' ) ) {
				return true;
			} else {
				return false;
			}
		}

		/**
		 * @return bool
		 */
		static function dima_is_dimablock() {
			if ( is_singular( 'dimablock' ) ) {
				return true;
			} else {
				return false;
			}
		}

		static function dima_is_portfolio_home_page() {
			GLOBAL $post;
			if ( isset( $post ) ) {
				if ( $post->post_type == 'dima-portfolio' && ! is_singular( 'dima-portfolio' ) && DIMA_NOUR_ASSISTANT_IS_ACTIVE ) {
					return true;
				} else {
					return false;
				}
			} else {
				return false;
			}
		}

		/**
		 * dima_get_current_post_type function.
		 */
		static function dima_get_current_post_type() {
			global $post, $typenow, $current_screen;
			if ( $post && $post->post_type ) {
				return $post->post_type;
			} elseif ( $typenow ) {
				return $typenow;
			} elseif ( $current_screen && $current_screen->post_type ) {
				return $current_screen->post_type;
			} elseif ( isset( $_REQUEST['post_type'] ) ) {
				return sanitize_key( $_REQUEST['post_type'] );
			} elseif ( isset( $_GET['post'] ) && $_GET['post'] != - 1 ) {
				$thispost = get_post( $_GET['post'] );
				if ( $thispost ) {
					return $thispost->post_type;
				} else {
					return null;
				}
			} else {
				return null;
			}
		}

		/**
		 * @return string
		 */
		static function dima_get_home_page_url() {
			if ( dima_helper::dima_is_single_portfolio() ) {
				$slug_name        = dima_helper::dima_get_option( 'dima_projects_slug_name' );
				$page_id          = dima_helper::dima_get_option( 'dima_portfolio_page' );
				$page_url         = dima_helper::dima_get_option( 'dima_portfolio_page_url' );
				$page_link_source = dima_helper::dima_get_option( 'dima_portfolio_top_link_source' );
				$slug_name        = ( $slug_name != '' ) ? $slug_name : 'dima-portfolio';

				if ( $page_link_source != "page" && $page_url != '' ) {
					return $page_url;
				}
				if ( $page_id != '' && $page_id != 0 ) {
					return get_permalink( $page_id );
				} else {
					return home_url( $slug_name );
				}
			}
			if ( dima_helper::dima_is_product() ) {
				return home_url( 'shop' );
			} else {
				return get_home_url();
			}
		}

		static function dima_get_page_type() {
			if ( is_page() ) {
				$page_type = '_page_';
			} elseif ( dima_helper::dima_is_single_portfolio() ) {
				$page_type = '_portfolio_';
			} elseif ( dima_helper::dima_is_product() ) {
				$page_type = '_product_';
			} else {
				$page_type = '_post_';
			}

			return $page_type;
		}

		/**
		 * Is Shop
		 * @return bool
		 */
		static function dima_is_shop() {
			if ( function_exists( 'is_shop' ) && is_shop() ) {
				return true;
			} else {
				return false;
			}
		}

		/**
		 * Is woo
		 * @return bool
		 */
		static function dima_is_woo() {
			if ( class_exists( 'WooCommerce' ) ) {
				return true;
			} else {
				return false;
			}
		}

		/**
		 * Is Shop
		 * @return bool
		 */
		static function dima_is_woo_pages() {
			if ( is_woocommerce() || is_cart() || is_checkout() || is_account_page() ) {
				return true;
			} else {
				return false;
			}
		}

		/**
		 * Is Product
		 * @return bool
		 */
		static function dima_is_product() {
			if ( function_exists( 'is_product' ) && is_product() ) {
				return true;
			} else {
				return false;
			}
		}

		/**
		 * Is product category
		 * @return bool
		 */
		static function dima_is_product_category() {
			if ( function_exists( 'is_product_category' ) && is_product_category() ) {
				return true;
			} else {
				return false;
			}
		}

		/**
		 * @return bool
		 */
		static function dima_is_product_tag() {
			if ( function_exists( 'is_product_tag' ) && is_product_tag() ) {
				return true;
			} else {
				return false;
			}
		}

		/*NOOR Version 2.0*/
		static function dima_is_bbpress() {
			if ( function_exists( 'is_bbpress' ) && is_bbpress() ) {
				return true;
			} else {
				return false;
			}
		}

		static function dima_is_buddypress() {
			if ( function_exists( 'is_buddyPress' ) && is_buddyPress() ) {
				return true;
			} else {
				return false;
			}
		}

		/* Check WordPress Version */
		static function dima_wp_version_check( $version = '3.0' ) {
			global $wp_version;
			if ( version_compare( $wp_version, $version, ">=" ) ) {
				return true;
			}

			return false;
		}

		/**
		 * @param $url
		 * @param $param_name
		 * @param $param_value
		 *
		 * @return string
		 */
		static function dima_add_url_parameter( $url, $param_name, $param_value ) {
			$data = parse_url( $url );
			if ( ! isset( $data["query"] ) ) {
				$data["query"] = "";
			}

			$params = array();
			parse_str( $data['query'], $params );

			if ( is_array( $param_value ) ) {
				$param_value = $param_value[0];
			}

			$params[ $param_name ] = $param_value;

			if ( $param_name == 'product_count' ) {
				$params['paged'] = '1';
			}

			$data['query'] = http_build_query( $params );

			return dima_helper::dima_make_url( $data );
		}

		/**
		 * @param $data
		 *
		 * @return string
		 */
		static function dima_make_url( $data ) {
			$url = '';
			if ( isset( $data['host'] ) ) {
				$url .= $data['scheme'] . '://';
				if ( isset ( $data['user'] ) ) {
					$url .= $data['user'];
					if ( isset( $data['pass'] ) ) {
						$url .= ':' . $data['pass'];
					}
					$url .= '@';
				}
				$url .= $data['host'];
				if ( isset ( $data['port'] ) ) {
					$url .= ':' . $data['port'];
				}
			}

			if ( isset( $data['path'] ) ) {
				$url .= $data['path'];
			}

			if ( isset( $data['query'] ) ) {
				$url .= '?' . $data['query'];
			}

			if ( isset( $data['fragment'] ) ) {
				$url .= '#' . $data['fragment'];
			}

			return $url;
		}

		static function dima_get_view_with_args( $style, $base, $extension = '', $args = array() ) {
			$template = new DIMA_Get_View( $style, $base, $extension, $args );
			$template->render();
		}

		/**
		 * @param int    $clm 5 use in shortcode -dima_shortcode-
		 *
		 * @param string $post_type
		 *
		 * @return bool|WP_Query
		 */
		static function dima_get_post_related_posts( $clm = 3, $post_type = 'post' ) {

			if ( $post_type == "dima-portfolio" ) {
				$taxonomy = "portfolio-category";
			} else {
				$taxonomy = "category";
			}

			$post_id = get_the_ID();
			$terms   = get_the_terms( $post_id, $taxonomy );

			$term_ids = array();
			if ( is_array( $terms ) ) {
				foreach ( $terms as $term ) {
					$term_ids[] = $term->term_id;
				}
			}

			$related_posts = new WP_Query( array(
				'tax_query'      => array(
					array(
						'taxonomy' => $taxonomy,
						'field'    => 'id',
						'terms'    => $term_ids,
						'operator' => 'IN',
					),
				),
				'post_type'      => $post_type,
				'post__not_in'   => array( $post_id ),
				'posts_per_page' => $clm,
				'orderby'        => 'rand',
			) );

			if ( $related_posts->have_posts() ) {
				return $related_posts;
			} else {
				return false;
			}
		}

		/**
		 * @param        $toggle
		 * @param        $trigger
		 * @param        $placement
		 * @param string $title
		 * @param string $content
		 *
		 * @return string
		 */
		static function dima_tooltip_data( $toggle, $trigger, $placement, $title = '', $content = '' ) {

			if ( ! in_array( $toggle, array( 'tooltip', 'popover' ) ) ) {
				return '';
			}

			$params = array(
				'data-toggle'    => ( $toggle == 'tooltip' ) ? 'tooltip' : 'popover',
				'data-trigger'   => $trigger,
				'data-placement' => $placement,
				'data-title'     => $title,
				'data-content'   => $content
			);

			return self::implode_key( $params );
		}

		/**
		 * @param array $pieces
		 *
		 * @return string
		 */
		static function implode_key( $pieces = array() ) {

			$keys = array_keys( $pieces );
			$val  = array_values( $pieces );
			$par  = "";
			for ( $i = 0; $i < sizeof( $val ); $i ++ ) {
				if ( ! empty( $val[ $i ] ) ) {
					$par .= $keys[ $i ] . "=\"" . $val[ $i ] . "\" ";
				}
			}

			return $par;
		}

		/**
		 * @param array $args
		 *
		 * @return string
		 */
		static function dima_get_post_thumb( $args = array() ) {
			$default_args = array(
				'post_id'                    => 0,
				'size'                       => '',
				'height'                     => 100,
				'width'                      => 100,
				'title'                      => '',
				'is_linked'                  => false,
				'permalink'                  => '',
				'a_class'                    => array(),
				'img_class'                  => array(),
				'img_style'                  => '',
				'post_format_thumb_fallback' => false,
				'fallback'                   => '',
				'thumb_src'                  => '',
				'popup_type'                 => '',
			);

			/*extracting all args*/
			$args      = wp_parse_args( $args, $default_args );
			$post_id   = $args['post_id'] ? $args['post_id'] : get_the_ID();
			$permalink = ! empty( $args['permalink'] ) ? $args['permalink'] : get_the_permalink( $post_id );
			$title     = ! empty( $args['title'] ) ? $args['title'] : get_the_title( $post_id );
			$width     = (int) apply_filters( 'dima_post_thumbnail_width', $args['width'] );
			$height    = (int) apply_filters( 'dima_post_thumbnail_height', $args['height'] );

			$size      = ! empty( $args['size'] ) ? $args['size'] : array( $width, $height );
			$thumb_src = $args['thumb_src'];

			$img_style    = $args['img_style'];
			$tooltip_attr = ( $args['popup_type'] != '' ) ? self::dima_tooltip_data(
				$args['popup_type'],
				'hover',
				'top', '',
				'popup_content' ) : '';

			$thumbnail_id = get_post_thumbnail_id( $post_id );

			if ( ! $thumbnail_id && ! $args['thumb_src'] ) {

				if ( $args['post_format_thumb_fallback'] ) {
					$post_format = get_post_format();
					if ( in_array( $post_format, array( 'video', 'quote', 'link', 'audio' ) ) ) {
						$thumb_src = dima_helper::dima_get_post_format_thumb( $post_format );
					} else {
						$thumb_src = dima_helper::dima_get_post_format_thumb( 'text' );
					}
				} else if ( ! empty( $args['fallback'] ) ) {
					return $args['fallback'];
				} else {
					$thumb_src = dima_helper::dima_get_post_format_thumb( 'text' );
				}
			}

			if ( $thumbnail_id ) {
				$image_output = get_the_post_thumbnail( $post_id, $size, null );
			} else {
				$image_output = sprintf(
					'<img  width="230px" height="150px" src="%1$s" alt="%2$s"%3$s %4$s/>',
					esc_attr( $thumb_src ),
					esc_attr( $title ),
					( ! empty( $args['img_class'] ) ? sprintf( ' class="%s"', esc_attr( implode( ' ', $args['img_class'] ) ) ) : '' ),
					( ! empty( $img_style ) ? sprintf( ' style="%s"', esc_attr( $img_style ) ) : '' )
				);
			}
			if ( $args['is_linked'] ) {
				$image_output = sprintf(
					'<a href="%1$s" title="%2$s"%3$s%5$s%6$s>
               %4$s
               </a>',
					esc_attr( $permalink ),
					esc_attr( $title ),
					( ! empty( $args['a_class'] ) ? sprintf( ' class="%s"', esc_attr( implode( ' ', $args['a_class'] ) ) ) : '' ),
					$image_output,
					( ! empty( $img_style ) ? sprintf( ' style="%s"', esc_attr( $img_style ) ) : '' ),
					$tooltip_attr
				);
			}

			return $image_output;
		}

		static function dima_get_thumb_size( $post_type = '', $is_single = false ) {
			if ( $is_single ) {
				return 'dima-big-image-single';
			} else {
				switch ( $post_type ) {
					case 'timeline':
					case 'standard':
						return 'dima-post-standard-image';
						break;
					case 'grid':
						return 'dima-grid-image';
						break;
					case 'masonry':
						return 'dima-massonry-image';
						break;
					default:
						return 'full';
						break;
				}
			}
		}

		static function dima_get_thumb( $args ) {

			$post_type  = $args['post_type'];
			$size       = self::dima_get_thumb_size( $post_type, is_single() );
			$array_size = array( 'size' => $size );
			$thumb      = dima_helper::dima_get_post_thumb( $array_size );

			return $thumb;
		}

		static function dima_get_thumb_gallery( $args, $attachment ) {
			$size  = self::dima_get_thumb_size( $args['post_type'], is_single() );
			$thumb = wp_get_attachment_image( $attachment, $size, false, false );

			return $thumb;
		}

		/**
		 * @param string $_this
		 *
		 * @return array
		 */
		static function get_featured_args( $_this = '' ) {

			if ( is_singular() && ! is_page() ) {
				$default_args = array(
					'is_full_post_content_blog' => "",
					'meta'                      => esc_attr( dima_helper::dima_get_option( 'dima_blog_enable_post_meta' ) ),
					'words'                     => intval( dima_helper::dima_get_option( 'dima_blog_blog_excerpt' ) ),
					'elm_hover'                 => "",
					'img_hover'                 => "",
					'show_image'                => true,
					'blog_type'                 => esc_attr( dima_helper::dima_get_option( 'dima_blog_style' ) ),
					'post_class'                => '',
					'cover_color'               => '',
				);
			} else {
				$default_args = array(
					'is_full_post_content_blog' => dima_helper::dima_get_option( 'dima_blog_enable_full_post_index' ),
					'meta'                      => ( isset( $_this->show_meta ) ) ? $_this->show_meta : true,
					'words'                     => ( isset( $_this->words ) ) ? $_this->words : intval( esc_attr( dima_helper::dima_get_option( 'dima_blog_blog_excerpt' ) ) ),
					'elm_hover'                 => ( isset( $_this->elm_hover ) ) ? $_this->elm_hover : '',
					'img_hover'                 => ( isset( $_this->img_hover ) ) ? $_this->img_hover : '',
					'show_image'                => ( isset( $_this->show_image ) ) ? $_this->show_image : true,
					'blog_type'                 => ( isset( $_this->blog_style ) ) ? $_this->blog_style : esc_attr( dima_helper::dima_get_option( 'dima_blog_style' ) ),
					'post_class'                => ( isset( $_this->post_class ) ) ? $_this->post_class : '',
					'cover_color'               => ( isset( $_this->cover_color ) ) ? $_this->cover_color : '',
				);
			}

			return $default_args;
		}

		static function get_featured_args_portfolio( $_this = '' ) {

			if ( $_this == '' ) {
				$default_args = array(
					'is_full_post_content_blog' => "",
					'elm_hover'                 => "",
					'img_hover'                 => "",
					'blog_type'                 => 'grid',
					'post_class'                => '',
					'no_border'                 => '',
					'no_margin'                 => '',
				);
			} else {
				$default_args = array(
					'is_full_post_content_blog' => dima_helper::dima_get_option( 'dima_blog_enable_full_post_index' ),
					'elm_hover'                 => ( isset( $_this->elm_hover ) ) ? $_this->elm_hover : '',
					'img_hover'                 => ( isset( $_this->img_hover ) ) ? $_this->img_hover : '',
					'blog_type'                 => ( isset( $_this->blog_type ) ) ? $_this->blog_type : 'grid',
					'post_class'                => ( isset( $_this->post_class ) ) ? $_this->post_class : '',
					'no_border'                 => ( isset( $_this->no_border ) ) ? $_this->no_border : '',
					'no_margin'                 => ( isset( $_this->no_margin ) ) ? $_this->no_margin : '',
				);
			}

			return $default_args;
		}

		static function dima_get_featured_image_url( $size = 'dima-post-standard-image' ) {
			$post_id  = get_the_ID();
			$image_id = get_post_thumbnail_id( $post_id );
			$image    = wp_get_attachment_image_src( $image_id, $size );

			return $image[0];
		}

		static function dima_get_image_attrs( $src = '', $id = '', $w = '', $h = '', $default_alt = '' ) {
			if ( empty( $default_alt ) ) {
				$default_alt = esc_attr__( 'Image', 'noor' );
			}

			$attr     = array();
			$atts_str = '';

			$alt = trim( strip_tags( get_post_meta( $id, '_wp_attachment_image_alt', true ) ) );

			if ( empty( $alt ) ) {
				$alt = $default_alt;
			}

			$attr['alt'] = $alt;

			foreach ( $attr as $name => $val ) {
				$atts_str .= $name . '="' . $val . '" ';
			}

			return $atts_str;
		}

		/**
		 * Get Slider Shortcode (rev_slider)
		 *
		 * @param $string
		 *
		 * @return string
		 */
		static function dima_get_slider_shortcode( $string ) {
			$Slide_Alias = strpos( $string, 'dima-revolution-slider-' ) !== false;
			if ( $Slide_Alias ) {
				$string_pieces = explode( '-', $string );
				$slider_name   = end( $string_pieces );
			} else {
				$slider_name = end( $string );
			}

			return "[rev_slider alias=\"{$slider_name}\"]";
		}

		/**
		 * Get View
		 * https://codex.wordpress.org/Function_Reference/get_template_part
		 *
		 * @param        $style
		 * @param        $base
		 * @param string $extension
		 */
		static function dima_get_view( $style, $base, $extension = '' ) {
			get_template_part( 'framework/views/' . $style . '/' . $base, $extension );
		}

		/**
		 * Use in menu and content
		 * @return string
		 */
		static function dima_get_header_content_wrapper() {
			$class = 'full-wrapper';

			return $class;
		}

		/**
		 * @return string
		 */
		static function dima_display_shortcode_above_sidebar() {
			$id         = dima_helper::dima_get_the_ID();
			$big_active = get_post_meta( $id, '_dima_meta_shortcode_above_sidebar', true );
			$big_active = ( $big_active == '' ) ? 'Off' : $big_active;
			if ( $big_active != 'Off' ) :
				?>
                <div class="above_sidebar-container">
					<?php
					echo do_shortcode( html_entity_decode( $big_active ) );
					?>
                </div>
                <div class="clear-section"></div>
			<?php endif;
		}

		static function dima_get_sidebar( $name = "0" ) {
			wp_reset_postdata();
			if ( $name != "0" ) {
				dynamic_sidebar( sanitize_title( $name ) );

				return;
			}
			if ( is_home() ) {
				dynamic_sidebar( 'sidebar-main' );
			} elseif ( is_singular( 'page' ) ) {
				$sidebar_id = get_post_meta( get_the_ID(), '_dima_meta_sidebar', true );
				if ( $sidebar_id == '' ) {
					dynamic_sidebar( 'sidebar-main' );
				} else {
					dynamic_sidebar( $sidebar_id );
				}
			} elseif ( is_singular( 'post' ) ) {
				$sidebar_id = get_post_meta( get_the_ID(), '_dima_meta_sidebar', true );
				if ( $sidebar_id == '' ) {
					dynamic_sidebar( 'sidebar-main' );
				} else {
					dynamic_sidebar( $sidebar_id );
				}
			} elseif ( is_singular( 'dima-portfolio' ) ) {
				$sidebar_id = dima_helper::dima_get_inherit_option( '_dima_meta_sidebar', 'dima_projects_details_sidebar' );

				if ( $sidebar_id == '' ) {
					dynamic_sidebar( 'sidebar-main' );
				} else {
					dynamic_sidebar( $sidebar_id );
				}
			} elseif ( dima_helper::dima_is_shop() || dima_helper::dima_is_product() ) {
				$sidebar_id = get_post_meta( get_the_ID(), '_dima_meta_sidebar', true );
				if ( $sidebar_id == '' ) {
					dynamic_sidebar( 'shop-widget-area' );
				} else {
					dynamic_sidebar( $sidebar_id );
				}

			} elseif ( dima_helper::dima_is_bbpress() ) {
				$sidebar_id = get_post_meta( get_the_ID(), '_dima_meta_sidebar', true );
				if ( $sidebar_id == '' ) {
					dynamic_sidebar( 'forums-widget-area' );
				} else {
					dynamic_sidebar( $sidebar_id );
				}

			} else {
				dynamic_sidebar( 'sidebar-main' );
			}
		}

		public static function dima_get_meta( $name ) {
			global $post;

			if ( isset( $post ) && ! empty( $post->ID ) && ! is_archive() && ! is_search() ) {
				return get_post_meta( $post->ID, $name, true );
			}

			return false;
		}

		/**
		 * Inherit
		 *
		 * @param string $meta
		 * @param        $opt
		 *
		 * @return bool|mixed|string
		 */
		static function dima_get_inherit_option( $meta = '', $opt ) {
			$_opt  = self::dima_get_option( $opt );
			$_meta = '';
			if ( $meta != '' ) {
				$_meta = self::dima_get_meta( $meta );
			}
			if ( $_meta == '' || $_meta == 'inherit' || $_meta == 'default' ) {
				$_meta = $_opt;
			}

			return $_meta;
		}

		static function dima_get_menus_list_options() {
			$menus = wp_get_nav_menus();
			if ( ! empty( $menus ) ) {
				$menus = wp_list_pluck( $menus, 'name', 'term_id' );
			} else {
				$menus = array();
			}

			$menus = array( '' => esc_attr( 'Select Menu', 'noor' ) ) + $menus;

			return $menus;
		}

		static function dima_am_i_true( $boolean ) {
			if ( is_bool( $boolean ) ) {
				return $boolean;
			}
			switch ( $boolean ) {
				case '1':
					return true;
					break;
				case 'true':
					return true;
					break;
				case 'on':
					return true;
					break;
				default:
					return false;
					break;
			}
		}

		static function dima_remove_white_space( $text ) {
			$text = preg_replace( '/[\t\n\r\0\x0B]/', '', $text );
			$text = preg_replace( '/([\s])\1+/', ' ', $text );
			$text = trim( $text );

			return $text;
		}

		static function dima_https_or_http() {
			$protocol = is_ssl() ? 'https' : 'http';

			return $protocol;
		}

		/**
		 * @return boolean
		 */
		static function dima_get_page_title_display() {
			if ( is_home() AND is_front_page() ) {
				$breadcrumbs_display = esc_attr( dima_helper::dima_get_option( 'dima_page_title_display' ) );
				$breadcrumbs_display = ( $breadcrumbs_display == '1' ) ? 'on' : 'off';
			} else {
				$breadcrumbs_display = dima_helper::dima_get_inherit_option( '_dima_meta_breadcumbs_display', 'dima_page_title_display' );
			}

			return dima_helper::dima_am_i_true( $breadcrumbs_display );
		}

		static function dima_get_breadcumbs_display() {
			$protocol = is_ssl() ? 'https' : 'http';

			return $protocol;
		}

		/**
		 * Instantiates the WordPress filesystem.
		 *
		 * @static
		 * @access public
		 * @return object WP_Filesystem
		 */
		public static function dima_init_filesystem() {

			$credentials = array();

			if ( ! defined( 'FS_METHOD' ) ) {
				define( 'FS_METHOD', 'direct' );
			}

			$method = defined( 'FS_METHOD' ) ? FS_METHOD : false;

			if ( 'ftpext' === $method ) {
				// If defined, set it to that, Else, set to NULL.
				$credentials['hostname'] = defined( 'FTP_HOST' ) ? preg_replace( '|\w+://|', '', FTP_HOST ) : null;
				$credentials['username'] = defined( 'FTP_USER' ) ? FTP_USER : null;
				$credentials['password'] = defined( 'FTP_PASS' ) ? FTP_PASS : null;

				// Set FTP port.
				if ( strpos( $credentials['hostname'], ':' ) && null !== $credentials['hostname'] ) {
					list( $credentials['hostname'], $credentials['port'] ) = explode( ':', $credentials['hostname'], 2 );
					if ( ! is_numeric( $credentials['port'] ) ) {
						unset( $credentials['port'] );
					}
				} else {
					unset( $credentials['port'] );
				}

				// Set connection type.
				if ( ( defined( 'FTP_SSL' ) && FTP_SSL ) && 'ftpext' === $method ) {
					$credentials['connection_type'] = 'ftps';
				} elseif ( ! array_filter( $credentials ) ) {
					$credentials['connection_type'] = null;
				} else {
					$credentials['connection_type'] = 'ftp';
				}
			}

			// The Wordpress filesystem.
			global $wp_filesystem;

			if ( empty( $wp_filesystem ) ) {
				require_once wp_normalize_path( ABSPATH . '/wp-admin/includes/file.php' );
				WP_Filesystem( $credentials );
			}

			return $wp_filesystem;
		}

		/**
		 * Check if we're on an Event page.
		 */
		public static function dima_tribe_is_event( $id = false ) {

			if ( function_exists( 'tribe_is_event' ) ) {
				if ( false === $id ) {
					return (bool) tribe_is_event();
				} else {
					return (bool) tribe_is_event( $id );
				}
			}

			return false;
		}

		public static function dima_is_events_archive() {
			if ( is_post_type_archive( 'tribe_events' ) || ( self::dima_tribe_is_event() && is_archive() ) ) {
				return true;
			}

			return false;
		}

		/**
		 * @param bool $colored
		 */
		public static function dima_get_global_social( $colored = false ) {
			$output = '';
			if ( ! $colored ) {
				$colored = self::dima_am_i_true( self::dima_get_option( 'dima_social_icons_is_colored' ) );
			}
			$socials = $socials_classes = array();
			GLOBAL $dima_array_of_social;
			foreach ( $dima_array_of_social as $option_name ) {
				if ( dima_helper::dima_get_option( $option_name ) != '' ) {
					$class = explode( "dima_social_", $option_name );
					array_push( $socials_classes, $class[1] );
					array_push( $socials, dima_helper::dima_get_option( $option_name ) );
				}
			}

			foreach ( $socials as $index => $social ) {
				$li_class = '';
				$url      = $social;
				$title    = ucfirst( $socials_classes[ $index ] );
				$class    = $socials_classes[ $index ];
				if ( $colored ) {
					$li_class = ' class="' . $class . '_icon colord_icon"';
				}
				if ( $class == 'googleplus' ) {
					$class = 'google-plus';
				}
				$output .= '<li' . $li_class . '><a href="' . esc_url( $url ) . '" title="' . esc_url( $title ) . '" target="_blank" rel="noopener"><i class="fa fa-' . $class . '"></i></a></li>';
			}
			$html_output = $output;
			echo( $html_output );
		}

		/**
		 * @param string $clm
		 *
		 * @return string
		 */
		public static function dima_get_clm( $clm = '', $clm_sd = '2', $clm_xsd = '1' ) {
			$nb_clm = 12;
			if ( $clm != 0 ) {
				$clm     = $nb_clm / $clm;
				$clm_sd  = $nb_clm / $clm_sd;
				$clm_xsd = $nb_clm / $clm_xsd;
			} else {
				$clm_xsd = $clm_sd = $clm = 12;
			}

			return 'ok-md-' . $clm . ' ok-sd-' . $clm_sd . ' ok-xsd-' . $clm_xsd . '';
		}

		/**
		 * @param      $content
		 * @param bool $autop
		 *
		 * @return string
		 */
		public static function dima_remove_wpautop( $content, $autop = false ) {
			if ( $autop ) {
				$content = wpautop( preg_replace( '/<\/?p\>/', "\n", $content ) . "\n" );
			}

			return do_shortcode( shortcode_unautop( $content ) );
		}

		//--------------------Admin
		public static function dima_get_filesystem() {
			$access_type = get_filesystem_method();
			if ( $access_type === 'direct' ) {
				/* you can safely run request_filesystem_credentials() without any issues and don't need to worry about passing in a URL */
				$creds = request_filesystem_credentials( site_url() . '/wp-admin/', '', false, false, array() );
				/* initialize the API */
				if ( ! WP_Filesystem( $creds ) ) {
					/* any problems and we exit */
					return false;
				}

				return true;
			} else {
				return false;
			}
		}

		public static function get_local_file_contents( $file_path ) {
			ob_start();
			include $file_path;
			$contents = ob_get_clean();

			return $contents;
		}
		//--------------------!Admin
	}
}


if ( ! class_exists( 'DIMA_Get_View' ) ) {
	/**
	 * Include a file and(optionally) pass arguments to it.
	 */
	class DIMA_Get_View {
		private $args;
		private $style;
		private $base;
		private $extension;

		public function __get( $name ) {
			return $this->args[ $name ];
		}

		public function __construct( $style, $base, $extension = '', $args = array() ) {
			$this->style     = $style;
			$this->base      = $base;
			$this->extension = $extension;
			$this->args      = $args;
		}

		public function __isset( $name ) {
			return isset( $this->args[ $name ] );
		}

		public function render() {
			if ( empty( $this->extension ) ) {
				$file = 'framework/views/' . $this->style . '/' . $this->base . '.php';
			} else {
				$file = 'framework/views/' . $this->style . '/' . $this->base . '-' . $this->extension . '.php';
			}

			if ( file_exists( get_template_directory() . '/' . $file ) ) {
				include( get_template_directory() . '/' . $file );
			}

		}
	}
}

if ( ! class_exists( 'DIMA_Demos' ) ) {
	class DIMA_Demos {

		static function update_demo( $demo_nem ) {
			update_option( 'dima_demo_name', $demo_nem );
		}

		/**
		 * @return bool|array
		 */
		static function get_installed_demo( $def_demo_neme = "noor_main" ) {
			$demo_neme = esc_attr( dima_helper::dima_get_option( 'dima_demo_name' ) );
			if ( $demo_neme != '' ) {
				return $demo_neme;
			}

			return $def_demo_neme;
		}
	}
}