<?php

/*
 * Implement svg4everybody in order to better support external sprite references
 * on IE8-10. For lower versions, we need an older copy of the script.
 * https://github.com/jonathantneal/svg4everybody
 */
function dima_easy_as_svg_svg_scripts() {
	global $dima_easy_as_svg_sprite_external;

	/*
	 * Implement svg4everybody in order to better support external sprite references
	 * on IE8-10. For lower versions, we need an older copy of the script.
	 * https://github.com/jonathantneal/svg4everybody
	 */
	if ( $dima_easy_as_svg_sprite_external ) :
		wp_register_script( 'easy_as_svg-svg4everybody', DIMA_TEMPLATE_URL . '/framework/asset/site/js/core/svg4everybody.js', DIMA_VERSION, false );
		wp_enqueue_script( 'easy_as_svg-svg4everybody' );
	endif;
}

add_action( 'wp_enqueue_scripts', 'dima_easy_as_svg_svg_scripts' );

/**
 * @param $name
 *
 * @return bool|string
 */
function dima_get_svg_icon( $name ) {
	$icon      = 'framework/images/svg/' . esc_attr( $name ) . '.svg.php';
	$icon_path = get_template_directory() . "/" . $icon;
	if ( file_exists( $icon_path ) ) {
		ob_start();
		include $icon_path;
		$contents = ob_get_clean();

		return $contents;
	}
}

function dima_get_svg_format( $name ) {
	switch ( $name ) {
		case 'quote':
			$name = "ic_format_quote";
			break;
		case 'video':
			$name = "ic_play_arrow";
			break;
		case 'audio':
			$name = "ic_music_note";
			break;
		default:
			$name = "ic_link";
			break;
	}
	$icon_path = get_template_directory() . '/framework/images/svg/' . esc_attr( $name ) . '.svg.php';
	if ( file_exists( $icon_path ) ) {
		ob_start();
		include $icon_path;
		$contents = ob_get_clean();

		return $contents;

	}
}