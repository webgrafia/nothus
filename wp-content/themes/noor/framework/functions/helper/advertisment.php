<?php

/*-----------------------------------------------------------------------------------*/
# Popup module
/*-----------------------------------------------------------------------------------*/
if ( ! function_exists( 'dima_add_popup_module' ) ) {

	add_action( 'wp_footer', 'dima_add_popup_module' );
	function dima_add_popup_module() {
		if ( dima_helper::dima_am_i_true( dima_helper::dima_get_option( 'dima_ad_blocker_detector' ) ) ) {
			?>
			<div id="dima-popup-adblock" class="dima-popup is-fixed-popup">
				<div class="dima-popup-container">
					<div class="container-wrapper">
						<?php echo wp_kses( dima_get_svg_icon( "ic_visibility_off" ), dima_helper::dima_get_allowed_html_tag() ); ?>
						<h4><?php esc_html_e( 'Adblock Detected', 'noor' ) ?></h4>
						<div class="adblock-message"><?php esc_html_e( 'Please consider supporting us by disabling your ad blocker', 'noor' ) ?></div>
					</div><!-- .container-wrapper  /-->
				</div><!-- .dima-popup-container /-->
			</div><!-- .dima-popup /-->
			<script type='text/javascript'
			        src='<?php echo DIMA_TEMPLATE_URL ?>/framework/asset/site/js/advertisement.js'></script>
			<?php
		}
	}

}


/*-----------------------------------------------------------------------------------*/
# Above post Ad
/*-----------------------------------------------------------------------------------*/
if ( ! function_exists( 'dima_above_post_ad' ) ) {

	function dima_above_post_ad() {
		if ( dima_helper::dima_am_i_true( dima_helper::dima_get_option( 'dima_ad_above_article' ) ) ) {
			if ( dima_helper::dima_get_option( 'dima_ad_above_article_custom_code' ) ) {
				echo '<div class="stream-item stream-item-above-post">'
				     . do_shortcode( dima_helper::dima_get_option( 'dima_ad_above_article_custom_code' ) ) .
				     '</div>';
			}
		}
	}

}

/*-----------------------------------------------------------------------------------*/
# Above post Ad
/*-----------------------------------------------------------------------------------*/
if ( ! function_exists( 'dima_below_post_ad' ) ) {

	function dima_below_post_ad() {
		if ( dima_helper::dima_am_i_true( dima_helper::dima_get_option( 'dima_ad_below_article' ) ) ) {
			if ( dima_helper::dima_get_option( 'dima_ad_below_article_custom_code' ) ) {
				echo '<div class="stream-item stream-item-below-post">'
				     . do_shortcode( dima_helper::dima_get_option( 'dima_ad_below_article_custom_code' ) ) .
				     '</div>';
			}
		}
	}

}