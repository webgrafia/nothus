<?php


/**
 * Class dima_media_support - DIMA media (video) support V 1.0
 *
 */
class dima_media_support {

	/**
	 * Returns the video thumb url from the video URL
	 *
	 * @param $videoUrl
	 *
	 * @return string
	 */
	static function dima_get_thumb_url( $videoUrl ) {

		switch ( self::dima_detect_video_service( $videoUrl ) ) {
			case 'youtube':
				$maxresdefault = dima_helper::dima_https_or_http() . '://img.youtube.com/vi/' . self::dima_get_youtube_id( $videoUrl ) . '/maxresdefault.jpg';
				$sddefault     = dima_helper::dima_https_or_http() . '://img.youtube.com/vi/' . self::dima_get_youtube_id( $videoUrl ) . '/sddefault.jpg';
				$hqdefault     = dima_helper::dima_https_or_http() . '://img.youtube.com/vi/' . self::dima_get_youtube_id( $videoUrl ) . '/hqdefault.jpg';

				if ( ! self::dima_is_404( $maxresdefault ) ) {
					return $maxresdefault;
				} elseif ( ! self::dima_is_404( $sddefault ) ) {
					return $sddefault;
				} elseif ( ! self::dima_is_404( $hqdefault ) ) {
					return $hqdefault;
				} else {
					dima_log::dima_add_log( __FILE__, __FUNCTION__, 'No suitable thumb found for youtube.', $videoUrl );
				}
				break;

			case 'vimeo':
				//@todo e stricat nu mai merge de ceva timp cred
				$url = 'http://vimeo.com/api/oembed.json?url=https://vimeo.com/' . self::dima_get_vimeo_id( $videoUrl );

				$response = wp_remote_get( $url, array(
					'timeout'    => 10,
					'sslverify'  => false,
					'user-agent' => 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:35.0) Gecko/20100101 Firefox/35.0'
				) );

				if ( ! is_wp_error( $response ) ) {
					$dima_result = json_decode( wp_remote_retrieve_body( $response ) );

					return ( $dima_result->thumbnail_url );
				}
				break;
		}

		return '';
	}

	static function dima_get_metadata( $videoUrl ) {
		switch ( self::dima_detect_video_service( $videoUrl ) ) {
			case 'youtube':
				$url      = 'https://www.youtube.com/oembed?url=https://www.youtube.com/watch?v=' . self::dima_get_youtube_id( $videoUrl ) . '&format=json';
				$response = wp_remote_get( $url, array(
					'timeout'    => 10,
					'sslverify'  => false,
					'user-agent' => 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:35.0) Gecko/20100101 Firefox/35.0'
				) );
				if ( ! is_wp_error( $response ) ) {
					$dima_result = json_decode( wp_remote_retrieve_body( $response ) );

					return ( $dima_result );
				}

				break;
			case 'vimeo':
				// eg. https://vimeo.com/api/oembed.json?url=https%3A//vimeo.com/203689226
				$url      = 'http://vimeo.com/api/oembed.json?url=https://vimeo.com/' . self::dima_get_vimeo_id( $videoUrl );
				$response = wp_remote_get( $url, array(
					'timeout'    => 10,
					'sslverify'  => false,
					'user-agent' => 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:35.0) Gecko/20100101 Firefox/35.0'
				) );
				if ( ! is_wp_error( $response ) ) {
					$dima_result = json_decode( wp_remote_retrieve_body( $response ) );

					return ( $dima_result );
				}

				break;
		}
	}

	/*
	 * youtube
	 */
	private static function dima_get_youtube_id( $videoUrl ) {
		$query_string = array();
		parse_str( parse_url( $videoUrl, PHP_URL_QUERY ), $query_string );

		if ( empty( $query_string["v"] ) ) {
			//explode at ? mark
			$yt_short_link_parts_explode1 = explode( '?', $videoUrl );

			//short link: http://youtu.be/AgFeZr5ptV8
			$yt_short_link_parts = explode( '/', $yt_short_link_parts_explode1[0] );
			if ( ! empty( $yt_short_link_parts[3] ) ) {
				return $yt_short_link_parts[3];
			}

			return $yt_short_link_parts[0];
		} else {
			return $query_string["v"];
		}
	}

	/*
	 * Vimeo id
	 */
	private static function dima_get_vimeo_id( $videoUrl ) {
		sscanf( parse_url( $videoUrl, PHP_URL_PATH ), '/%d', $video_id );

		return $video_id;
	}

	/*
	 * Detect the video service from url
	 */
	static function dima_detect_video_service( $videoUrl ) {
		$videoUrl = strtolower( $videoUrl );
		if ( strpos( $videoUrl, 'youtube.com' ) !== false or strpos( $videoUrl, 'youtu.be' ) !== false ) {
			return 'youtube';
		}
		if ( strpos( $videoUrl, 'dailymotion.com' ) !== false ) {
			return 'dailymotion';
		}
		if ( strpos( $videoUrl, 'vimeo.com' ) !== false ) {
			return 'vimeo';
		}

		return false;
	}

	/**
	 * @param $url
	 *
	 * @return bool
	 */
	private static function dima_is_404( $url ) {
		$headers = get_headers( $url );
		if ( ! empty( $headers[0] ) and strpos( $headers[0], '404' ) !== false ) {
			return true;
		}

		return false;
	}
}