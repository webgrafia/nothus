<?php

/**
 * filters goes here.
 *
 * @package Dima Framework
 * @subpackage Functions
 * @version   1.0.0
 * @since     1.0.0
 * @author    PixelDima <info@pixeldima.com>
 *
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Remove Query Strings From Static Resources
 *
 * @param $src
 *
 * @return mixed
 */
function dima_remove_script_version( $src ) {
	$parts = explode( '?ver', $src );

	return $parts[0];
}

function dima_remove_script_version_and( $src ) {
	$parts = explode( '&ver', $src );

	return $parts[0];
}

function dima_wp_title( $title ) {

	if ( is_front_page() ) {
		return get_bloginfo( 'name' ) . '&#32;|&#32;' . get_bloginfo( 'description' );
	} elseif ( is_feed() ) {
		return ' | RSS Feed';
	} else {
		return trim( $title ) . ' | ' . get_bloginfo( 'name' );
	}
}

add_filter( 'wp_title', 'dima_wp_title' );

if ( ! is_admin() ) {
	add_filter( 'script_loader_src', 'dima_remove_script_version', 15, 1 );
	add_filter( 'style_loader_src', 'dima_remove_script_version', 15, 1 );
	add_filter( 'script_loader_src', 'dima_remove_script_version_and', 15, 1 );
	add_filter( 'style_loader_src', 'dima_remove_script_version_and', 15, 1 );
}

/**
 * Fixes empty <p> and <br> tags showing before and after shortcodes in the
 * output content.
 */
function dima_the_content_filter( $content ) {
	// array of custom shortcodes requiring the fix
	$block = join( "|", array( 'iconbox_content', 'list_item', 'text' ) );
	// opening tag
	$rep = preg_replace( "/(<p>)?\[($block)(\s[^\]]+)?\](<\/p>|<br \/>)?/", "[$2$3]", $content );

	// closing tag
	$rep = preg_replace( "/(<p>)?\[\/($block)](<\/p>|<br \/>)?/", "[/$2]", $rep );

	return $rep;
}

add_filter( 'the_content', 'dima_the_content_filter' );


/*-----------------------------------------------------------------------------------*/
# Gif images
/*-----------------------------------------------------------------------------------*/

add_filter( 'wp_get_attachment_image_src', 'dima_gif_full_image', 10, 4 );
function dima_gif_full_image( $image, $attachment_id, $size, $icon ) {

	if ( ! dima_helper::dima_get_option( 'dima_disable_featured_gif' ) ) {

		$file_type = wp_check_filetype( $image[0] );

		if ( ! empty( $file_type ) && $file_type['ext'] == 'gif' && $size != 'full' ) {
			return wp_get_attachment_image_src( $attachment_id, $size = 'full', $icon );
		}
	}

	return $image;
}

/*-----------------------------------------------------------------------------------*/
# Lazyload images
/*-----------------------------------------------------------------------------------*/
add_filter( 'wp_get_attachment_image_attributes', 'dima_lazyload_image_attributes', 8, 3 );
function dima_lazyload_image_attributes( $attr, $attachment, $size ) {

	# Check if we are in an AMP page ----------
	if ( DIMA_AMP_IS_ACTIVE && is_amp_endpoint() ) {
		return $attr;
	}

	# Check if the JetPack Plugin is active & the Photon option is enabled & Current images displayed in the post content ----------
	if ( DIMA_JETPACK_IS_ACTIVE && in_array( 'photon', Jetpack::get_active_modules() ) && in_array( 'the_content', $GLOBALS['wp_current_filter'] ) ) {
		return $attr;
	}

	# ----------

	if ( DIMA_USE_LAZY && ! is_admin() && ! is_feed() ) {
		switch ( $size ) {
			case 'shop_thumbnail':
				return $attr;
				break;

			case 'full':
			case 'dima-massonry-image':
				$small_src = wp_get_attachment_image_url( $attachment->ID, 'dima-lazy-image' );
				break;

			case 'shop_catalog':
			case 'shop_single':
			case 'dima-portfolio-grid-image':
				$small_src = DIMA_TEMPLATE_URL . '/images/dima-empty-' . $size . '.png';
				break;
			default:
				$small_src = DIMA_TEMPLATE_URL . '/images/dima-empty.png';
				break;
		}

		if ( empty( $small_src ) ) {
			$small_src = DIMA_TEMPLATE_URL . '/images/dima-empty.png';
		}

		$attr['class']    .= ' js-lazy-image';
		$attr['data-src'] = $attr['src'];
		$attr['src']      = $small_src;

		if ( isset( $attr['srcset'] ) && ! empty( $attr['srcset'] ) ) {
			$attr['data-srcset'] = $attr['srcset'];
		}
		unset( $attr['srcset'] );
		unset( $attr['sizes'] );
	}

	return $attr;
}


//--------By PixelDima
//
//
/*-----------------------------------------------------------------------------------*/
# Remove Shortcodes code and Keep the content
/*-----------------------------------------------------------------------------------*/
add_filter( 'dima_exclude_content', 'dima_strip_shortcodes' );
/**
 * Remove Shortcodes code and Keep the content
 *
 * @param string $text
 *
 * @return mixed|string
 */
function dima_strip_shortcodes( $text = '' ) {
	$text = preg_replace( '/\[(.*?)\]/i', '', $text );

	return $text;
}
