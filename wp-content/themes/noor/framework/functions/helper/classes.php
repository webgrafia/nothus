<?php
/**
 * Outputs custom classes for various elements,
 * sometimes based on options selected in customization.
 * sometimes based on options set on meta boxes.
 *
 * @package Dima Framework
 * @subpackage Functions
 * @version   1.0.0
 * @since     1.0.0
 * @author    PixelDima <info@pixeldima.com>
 *
 * TABLE OF CONTENTS
 *
 * - Body Classes
 * - Content classes
 * - Sidebar classes
 * - Navbar Class
 * - footer class
 *
 */


if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/*------------------------------*/
# Filter that return body class
/*------------------------------*/
if ( ! function_exists( 'dima_body_class' ) ):
	function dima_body_class( $output ) {

		$layout                = dima_get_site_layout();
		$framed_size           = dima_helper::dima_get_inherit_option( '_dima_meta_frame_size', 'dima_frame_size' );
		$is_blog               = is_home();
		$entry_id              = get_the_ID();
		$is_blog_style_masonry = esc_attr( dima_helper::dima_get_option( 'dima_blog_style' ) );

		$custom_class     = esc_attr( get_post_meta( $entry_id, '_dima_meta_body_css_class', true ) );
		$is_dark_menu     = dima_helper::dima_am_i_true( dima_helper::dima_get_inherit_option( '_dima_meta_navbar_menu_dark', 'dima_header_navbar_menu_dark' ) );
		$is_dark_sub_menu = dima_helper::dima_am_i_true( dima_helper::dima_get_inherit_option( '_dima_meta_navbar_sup_menu_dark', 'dima_header_navbar_sup_menu_dark' ) );
		$is_dark_bre_menu = dima_helper::dima_am_i_true( dima_helper::dima_get_inherit_option( '_dima_meta_navbar_page_title_dark', 'dima_header_navbar_page_title_dark' ) );

		$output[] .= "$layout";
		$output[] .= "$framed_size";

		if ( ! function_exists( 'dima_post_class' ) ):
			function dima_post_class( $output ) {
				switch ( has_post_thumbnail() ) {
					case true:
						$output[] .= 'has-post-thumbnail';
						break;
					case false:
						$output[] .= 'no-post-thumbnail';
						break;
				}

				return $output;
			}

			add_filter( 'post_class', 'dima_post_class' );
		endif;

		switch ( dima_get_header_positioning() ) {

			case 'static-top':
				$output[] .= 'dima-navbar-static-active';
				break;

			case 'fixed-top':
				$output[] .= 'dima-navbar-fixed-top-active';
				break;

			case 'fixed-left':
				$output[] .= 'vertical-menu vertical-menu-start';
				break;

			case 'fixed-right':
				$output[] .= 'vertical-menu vertical-menu-end';
				break;

			case 'fixed-left-small':
				$output[] .= 'vertical-menu vertical-menu-start small-menu';
				break;

			case 'fixed-right-small':
				$output[] .= 'vertical-menu vertical-menu-end small-menu';
				break;

			case 'big-navegation':
				$output[] .= 'dima-big-navegation-active';
				break;
		}

		if ( dima_is_one_page_navigation() ) {
			$output[] .= 'dima-one-page-navigation-active';
		}

		if ( dima_is_transparent_navigation() ) {
			$output[] .= 'dima-transparent-navigation-active';
		}

		/**
		 * Content layout.
		 */
		switch ( dima_get_content_layout() ) {
			case 'right-sidebar':
				$output[] .= 'right-content-sidebar-active';
				break;
			case 'left-sidebar':
				$output[] .= 'left-content-sidebar-active';
				break;
			case 'full-width':
				$output[] .= 'dima-full-width-active';
				break;
			case 'mini':
				$output[] .= 'dima-mini-width-active';
				break;
		}

		switch ( dima_get_section_layout_meta() ) {
			case 'full-width':
				$output[] .= 'dima-full-width-active';
				break;
		}

		switch ( dima_get_header_positioning() ) {
			case 'logo-on-center' :
				$output[] .= 'dima-navbar-center-active';
				break;
		}

		if ( dima_helper::dima_get_page_title_display() ) {
			$output[] .= 'dima_page_title_is_on';
		} else {
			$output[] .= 'dima_page_title_is_off';
		}

		if ( $is_dark_menu ) {
			$output[] .= 'navbar_is_dark';
		}
		if ( $is_dark_sub_menu ) {
			$output[] .= 'sub_menu_is_dark';
		}
		if ( $is_dark_bre_menu ) {
			$output[] .= 'bre_is_dark';
		}

		/**
		 * Blog and posts.
		 */
		if ( $is_blog ) {
			if ( $is_blog_style_masonry == 'masonry' ) {
				$output[] .= 'dima-masonry-active dima-blog-masonry-active';
			} elseif ( $is_blog_style_masonry == 'timeline' ) {
				$output[] .= 'dima-blog-timeline-active';
			} else {
				$output[] .= 'dima-blog-standard-active';
			}
		}

		$footer_is_parallax = dima_helper::dima_am_i_true( dima_helper::dima_get_inherit_option( '_dima_meta_footer_parallax', 'dima_footer_parallax' ) );

		if ( $footer_is_parallax ) {
			$output[] .= "footer-parallax";
		}

		if ( $custom_class != '' && ! is_home() ) {
			$output[] .= $custom_class;
		}

		$output[] = DIMA_THEME_FOLDER . '-ver-' . DIMA_VERSION;

		return $output;
	}

	add_filter( 'body_class', 'dima_body_class' );
endif;

/**
 * Content class
 */
if ( ! function_exists( 'dima_main_content_class' ) ):

	/**
	 * [Get content classes based on options selected in customization and meta box']
	 */
	function dima_main_content_class() {
		switch ( dima_get_content_layout() ) {
			case 'right-sidebar':
				$output = ' dima-container float-start ';
				break;
			case 'left-sidebar':
				$output = ' dima-container float-end';
				break;
			case 'full-width':
				$output = ' dima-container full';
				break;
			case 'mini':
				$output = 'mini-width';
				break;
			default:
				$output = 'dima-container full';
				break;
		}
		echo( $output );
	}
endif;

/**
 * Post Content class
 */
if ( ! function_exists( 'dima_pots_content_class' ) ):
	/**
	 * [Get content classes based on options selected in customization and meta box']
	 */
	function dima_pots_content_class() {
		$output = "entry-content post-content text-start";
		echo( $output );
	}
endif;

/**
 * Sidebar class
 */
if ( ! function_exists( 'dima_sidebar_class' ) ):

	/**
	 * [Get sidebar classes based on options selected in customization and metabox 'aside class dima_sidebar_class()']
	 */
	function dima_sidebar_class() {
		switch ( dima_get_content_layout() ) {
			case 'right-sidebar':
				$output = 'dima-sidebar hidden-tm hidden float-end';
				break;
			case 'left-sidebar':
				$output = 'dima-sidebar hidden-tm hidden float-start';
				break;
			default:
				$output = 'dima-sidebar hidden-tm hidden float-end';
		}
		echo( $output );
	}
endif;

/**
 * Navbar Class
 * [Get navbar positioning classes based on options selected in customization]
 */
if ( ! function_exists( 'dima_navbar_class' ) ) :
	function dima_navbar_class() {
		switch ( dima_get_header_positioning() ) {
			case 'fixed-left' :
				$output = 'dima-navbar dima-navbar-vertical ';
				break;
			case 'fixed-right' :
				$output = 'dima-navbar dima-navbar-vertical ';
				break;
			case 'fixed-left-small' :
				$output = 'dima-navbar dima-navbar-vertical left-small ';
				break;
			case 'fixed-right-small' :
				$output = 'dima-navbar dima-navbar-vertical right-small ';
				break;
			default :
				$output = 'dima-navbar ';
				break;
		}

		switch ( dima_get_header_animation() ) {
			case 'fixed-top' :
				$output .= 'fix-one ';
				break;
			case 'fixed-top-offset' :
				$output .= 'fix-two ';
				break;
			case 'headroom' :
				$output .= 'fix-headroom ';
				break;
			default :
				$output .= '';
				break;
		}

		if ( dima_is_transparent_navigation() ) {
			if ( strpos( $output, 'dima-navbar-vertical' ) === false ) {
				$output .= "dima-navbar-transparent ";
			}
		}
		if(dima_helper::dima_am_i_true(dima_helper::dima_get_option('dima_navbar_underline_on_off'))){
			$output .= "dima-navbar-line";
		}
		echo( $output );
	}
endif;

if ( ! function_exists( 'dima_navbar_wrap_desk_class' ) ):

	/**
	 * Logo potition.
	 */
	function dima_navbar_wrap_desk_class() {
		$output = 'dima-navbar-wrap desk-nav ';
		switch ( dima_get_header_positioning() ) {
			case 'logo-on-top' :
				$output .= 'dima-navbar-top ';
				break;
			case 'logo-on-center' :
				$output .= 'dima-navbar-center ';
				break;
			case 'logo-on-inline' :
				$output .= 'dima-navbar-inline ';
				break;
			default :
				$output .= '';
				break;
		}
		echo( $output );
	}
endif;

if ( ! function_exists( 'dima_navbar_wrap_mobile_class' ) ):
	/**
	 * [Get sidebar classes based on options selected in customization 'aside class dima_sidebar_class()']
	 */
	function dima_navbar_wrap_mobile_class() {
		$output = 'dima-navbar-wrap mobile-nav';
		echo( $output );
	}
endif;

/**
 * footer class
 */
if ( ! function_exists( 'dima_footer_class' ) ):
	/**
	 * [Get footer classes based on options selected in customization']
	 */
	function dima_footer_class() {
		$is_center = esc_attr( dima_helper::dima_get_option( 'dima_footer_bottom_center' ) ) == '1';
		if ( $is_center ) {
			$output = 'dima-footer text-center';
		} else {
			$output = 'dima-footer';
		}
		echo( $output );
	}
endif;

