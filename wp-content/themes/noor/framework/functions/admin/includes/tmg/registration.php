<?php
/**
 * Registers the plugins to be included via the TMG Plugin Activation class.
 *
 * @package tmg
 * @version   1.0.0
 * @since     1.0.0
 * @author    PixelDima <info@pixeldima.com>
 */


if ( ! function_exists( 'dima_register_theme_plugins' ) ) :

	add_action( 'tgmpa_register', 'dima_register_theme_plugins' );
	function dima_register_theme_plugins() {

		/*------------------------------*/
		# Theme plugins
		/*------------------------------*/
		$plugins = array(

			/*------------------------------*/
			# Noor Assistant
			/*------------------------------*/
			'noor_assistant'            => array(
				'name'                => 'Noor Assistant',
				'slug'                => 'noor_assistant',
				'source'              => get_stylesheet_directory() . '/framework/plugins/noor_assistant.zip',
				'required'            => true,
				'version'             => '1.0.16',
				'force_activation'    => false,
				'force_deactivation'  => false,
				'external_url'        => 'pixeldima.com',
				'dima_plugin'         => 'noor_assistant/noor-assistant.php',
				'dima_author'         => 'PixelDIma',
				'dima_description'    => sprintf( 'This plugin is required to run %1s. It provides a all the shortcodes used in %2s.', DIMA_THEME_NAME, DIMA_THEME_NAME ),
				'dima_logo'           => DIMA_TEMPLATE_URL . '/framework/images/dima-shortcode.png',
				'dima_plugin_upgrade' => true
			),

			/*------------------------------*/
			# WPBakery Page Builder
			/*------------------------------*/
			'js_composer'               => array(
				'name'                => 'WPBakery Page Builder',
				'slug'                => 'js_composer',
				'source'              => get_stylesheet_directory() . '/framework/plugins/visual_composer.zip',
				'required'            => true,
				'version'             => '5.4.5',
				'force_activation'    => false,
				'force_deactivation'  => false,
				'external_url'        => '',
				'dima_plugin'         => 'js_composer/js_composer.php',
				'dima_author'         => 'WPBakery',
				'dima_description'    => 'Visual Composer Page Builder Plugin for WordPress, with Frontend and Backend Editor.',
				'dima_logo'           => DIMA_TEMPLATE_URL . '/framework/images/Visual-Composer-Logo.png',
				'dima_plugin_upgrade' => true
			),

			/*------------------------------*/
			# Slider Revolution
			/*------------------------------*/
			'revslider'                 => array(
				'name'                => 'Slider Revolution',
				'slug'                => 'revslider',
				'source'              => get_stylesheet_directory() . '/framework/plugins/slider_revolution_responsive.zip',
				'required'            => false,
				'version'             => '5.4.6.4',
				'force_activation'    => false,
				'force_deactivation'  => false,
				'external_url'        => '',
				'dima_plugin'         => 'revslider/revslider.php',
				'dima_author'         => 'ThemePunch',
				'dima_description'    => 'Create responsive sliders with must-see-effects.',
				'dima_logo'           => DIMA_TEMPLATE_URL . '/framework/images/slider_revolution_responsive.png',
				'dima_plugin_upgrade' => true
			),

			/*------------------------------*/
			# AMP
			/*------------------------------*/
			'amp'                       => array(
				'name'               => 'AMP',
				'slug'               => 'amp',
				'required'           => false,
				'force_activation'   => false,
				'force_deactivation' => false,
				'dima_author'        => 'Automattic',
				'dima_plugin'        => 'amp/amp.php',
				'dima_description'   => 'This plugin adds support for the Accelerated Mobile Pages (AMP) Project',
				'dima_logo'          => DIMA_TEMPLATE_URL . '/framework/images/logo-amp.png',
			),

			/*------------------------------*/
			# WooCommerce
			/*------------------------------*/
			'woocommerce'               => array(
				'name'               => 'WooCommerce',
				'slug'               => 'woocommerce',
				'required'           => false,
				'force_activation'   => false,
				'force_deactivation' => false,
				'dima_author'        => 'WooThemes',
				'dima_plugin'        => 'woocommerce/woocommerce.php',
				'dima_description'   => 'The world\'s favorite eCommerce solution that gives you complete control to sell anything.',
				'dima_logo'          => DIMA_TEMPLATE_URL . '/framework/images/woocommerce-Logo.png',
			),

			/*------------------------------*/
			# YITH WooCommerce Wishlist
			/*------------------------------*/
			'yith-woocommerce-wishlist' => array(
				'name'               => 'YITH WooCommerce Wishlist',
				'slug'               => 'yith-woocommerce-wishlist',
				'required'           => false,
				'force_activation'   => false,
				'force_deactivation' => false,
				'dima_author'        => 'YITHEMES',
				'dima_plugin'        => 'woocommerce/woocommerce.php',
				'dima_description'   => 'What can really make the difference in conversions and amount of sales is, without a doubt, the freedom to share your own wishlist.',
				'dima_logo'          => DIMA_TEMPLATE_URL . '/framework/images/yith-woocommerce-wishlist.jpg',
			),

			/*------------------------------*/
			# Contact Form 7
			/*------------------------------*/
			'contact-form-7'            => array(
				'name'               => 'Contact Form 7',
				'slug'               => 'contact-form-7',
				'required'           => false,
				'force_activation'   => false,
				'force_deactivation' => false,
				'dima_author'        => 'Takayuki Miyoshi',
				'dima_plugin'        => 'contact-form-7/wp-contact-form-7.php',
				'dima_description'   => 'Contact Form 7 can manage multiple contact forms, plus you can customize the form.',
				'dima_logo'          => DIMA_TEMPLATE_URL . '/framework/images/contactform7-Logo.png',
			),

			/*------------------------------*/
			# bbPress
			/*------------------------------*/
			'bbPress'                   => array(
				'name'               => 'bbPress',
				'slug'               => 'bbpress',
				'required'           => false,
				'force_activation'   => false,
				'force_deactivation' => false,
				'dima_author'        => 'The bbPress Community',
				'dima_plugin'        => 'bbpress/bbpress.php',
				'dima_description'   => 'bbPress is forum software with a twist from the creators of WordPress.',
				'dima_logo'          => DIMA_TEMPLATE_URL . '/framework/images/bbpree-Logo.png',
			),

			/*------------------------------*/
			# Add To Any
			/*------------------------------*/
			'add-to-any'                => array(
				'name'               => 'Add To Any',
				'slug'               => 'add-to-any',
				'required'           => false,
				'force_activation'   => false,
				'force_deactivation' => false,
				'dima_author'        => 'AddToAny',
				'dima_plugin'        => 'add-to-any/add-to-any.php',
				'dima_description'   => 'Share buttons for WordPress including the AddToAny sharing button, Facebook, Twitter, Google+, Pinterest, WhatsApp, many more',
				'dima_logo'          => DIMA_TEMPLATE_URL . '/framework/images/addtoany-Logo.png',
			),

			/*------------------------------*/
			# MailChimp for WordPress
			/*------------------------------*/
			'mailchimp-for-wp'          => array(
				'name'               => 'MailChimp for WordPress',
				'slug'               => 'mailchimp-for-wp',
				'required'           => false,
				'force_activation'   => false,
				'force_deactivation' => false,
				'dima_author'        => 'Pat Diven',
				'dima_plugin'        => 'mailchimp-for-wp/mailchimp-for-wp.php',
				'dima_description'   => 'MailChimp for WordPress, the absolute best. Subscribe your WordPress site visitors to your MailChimp lists, with ease.',
				'dima_logo'          => DIMA_TEMPLATE_URL . '/framework/images/mailchimp-logo.png',
			),

			/*------------------------------*/
			# Taxonomy Terms Order
			/*------------------------------*/
			'taxonomy-terms-order'      => array(
				'name'               => 'Taxonomy Terms Order',
				'slug'               => 'taxonomy-terms-order',
				'required'           => false,
				'force_activation'   => false,
				'force_deactivation' => false,
				'dima_author'        => 'Nsp-Code',
				'dima_plugin'        => 'taxonomy-terms-order/taxonomy-terms-order.php',
				'dima_description'   => 'Order Categories and all custom taxonomies terms using a Drag and Drop Sortable JS capability.',
				'dima_logo'          => DIMA_TEMPLATE_URL . '/framework/images/taxonomy-logo.png',
			),

			/*------------------------------*/
			# Regenerate Thumbnails
			/*------------------------------*/
			'regenerate-thumbnails'     => array(
				'name'               => 'Regenerate Thumbnails',
				'slug'               => 'regenerate-thumbnails',
				'required'           => false,
				'force_activation'   => false,
				'force_deactivation' => false,
				'dima_author'        => 'Alex Mills (Viper007Bond)',
				'dima_plugin'        => 'regenerate-thumbnails/regenerate-thumbnails.php',
				'dima_description'   => 'Regenerate Thumbnails allows you to regenerate the thumbnails for your image attachments.',
				'dima_logo'          => DIMA_TEMPLATE_URL . '/framework/images/regenerate-thumbnails.png',
			),

			/*------------------------------*/
			# WP Page Widget
			/*------------------------------*/
			'wp-page-widget'            => array(
				'name'               => 'WP Page Widget',
				'slug'               => 'wp-page-widget',
				'required'           => false,
				'force_activation'   => false,
				'force_deactivation' => false,
				'dima_author'        => 'CodeAndMore',
				'dima_plugin'        => 'wp-page-widget/wp-page-widget.php',
				'dima_description'   => 'With this plugin activated we can select widgets to show specifically for one page.',
				'dima_logo'          => DIMA_TEMPLATE_URL . '/framework/images/wp-page-widget.png',
			),

			/*------------------------------*/
			# Dima – Google Analytics
			/*------------------------------*/
			'dima-google-analytics'     => array(
				'name'               => 'Dima – Google Analytics',
				'slug'               => 'dima-google-analytics',
				'source'             => get_stylesheet_directory() . '/framework/plugins/dima-google-analytics.zip',
				'version'            => '1.0.0',
				'required'           => false,
				'force_activation'   => false,
				'force_deactivation' => false,
				'external_url'       => '',
				'dima_author'        => 'PixelDima',
				'dima_plugin'        => 'dima-google-analytics/dima-google-analytics.php',
				'dima_description'   => 'Connect Google Analytics with WordPress by adding your Google Analytics tracking code.',
				'dima_logo'          => DIMA_TEMPLATE_URL . '/framework/images/google-an.png',
			),

			/*------------------------------*/
			# WP Page Widget
			/*------------------------------*/
			'dima-take-action'          => array(
				'name'               => 'Dima Take Action',
				'slug'               => 'dima-take-action',
				'required'           => false,
				'force_activation'   => false,
				'force_deactivation' => false,
				'dima_author'        => 'PixelDima',
				'dima_plugin'        => 'dima-take-action/dima-take-action.php',
				'dima_description'   => 'Adds a beautiful, customizable take action banner containe a promotion or a news to the top or the bottom of your WordPress site.',
				'dima_logo'          => DIMA_TEMPLATE_URL . '/framework/images/dima-take-action.jpg',
			),

			/*------------------------------*/
			# Under Construction
			/*------------------------------*/
			'under-construction-page'   => array(
				'name'               => 'Under Construction',
				'slug'               => 'under-construction-page',
				'required'           => false,
				'force_activation'   => false,
				'force_deactivation' => false,
				'dima_author'        => 'Under Construction',
				'dima_plugin'        => 'under-construction-page/under-construction.php',
				'dima_description'   => 'Create an Under Construction Page, Maintenance Mode Page or a Landing Page that takes less than a minute to install & configure',
				'dima_logo'          => DIMA_TEMPLATE_URL . '/framework/images/under-construction-page.png',
			),

			/*------------------------------*/
			# Envato Market
			/*------------------------------*/
			'envato-market'             => array(
				'name'               => 'Envato Market',
				'slug'               => 'envato-market',
				'source'             => get_stylesheet_directory() . '/framework/plugins/envato-market.zip',
				'version'            => '1.0.0-RC2',
				'required'           => false,
				'force_activation'   => false,
				'force_deactivation' => false,
				'dima_author'        => 'PixelDima',
				'dima_plugin'        => 'envato-market/envato-market.php',
				'dima_description'   => 'The Envato Market plugin can install WordPress themes and plugins purchased from ThemeForest & CodeCanyon.',
				'dima_logo'          => DIMA_TEMPLATE_URL . '/framework/images/envato.png',
			),

		);


		/*------------------------------*/
		# TMG configuration
		/*------------------------------*/
		$config = array(
			'id'           => 'noor',
			'domain'       => 'noor',
			'default_path' => '',
			'parent_slug'  => 'themes.php',
			'menu'         => 'install-required-plugins',
			'has_notices'  => true,
			'dismissable'  => true,
			'is_automatic' => false,
			'message'      => '',
			'strings'      => array(
				'page_title'                      => esc_html__( 'Install Required Plugins', 'noor' ),
				'menu_title'                      => esc_html__( 'Install Plugins', 'noor' ),
				'installing'                      => esc_html__( 'Installing Plugin: %s', 'noor' ),
				'oops'                            => esc_html__( 'Something went wrong with the plugin API.', 'noor' ),
				'notice_can_install_required'     => _n_noop( 'This theme requires the following plugin: %1$s.', 'This theme requires the following plugins: %1$s.', 'noor' ),
				'notice_can_install_recommended'  => _n_noop( 'This theme recommends the following plugin: %1$s.', 'This theme recommends the following plugins: %1$s.', 'noor' ),
				'notice_cannot_install'           => _n_noop( 'Sorry, but you do not have the correct permissions to install the %s plugin. Contact the administrator of this site for help on getting the plugin installed.', 'Sorry, but you do not have the correct permissions to install the %s plugins. Contact the administrator of this site for help on getting the plugins installed.', 'noor' ),
				'notice_can_activate_required'    => _n_noop( 'The following required plugin is currently inactive: %1$s.', 'The following required plugins are currently inactive: %1$s.', 'noor' ),
				'notice_can_activate_recommended' => _n_noop( 'The following recommended plugin is currently inactive: %1$s.', 'The following recommended plugins are currently inactive: %1$s.', 'noor' ),
				'notice_cannot_activate'          => _n_noop( 'Sorry, but you do not have the correct permissions to activate the %s plugin. Contact the administrator of this site for help on getting the plugin activated.', 'Sorry, but you do not have the correct permissions to activate the %s plugins. Contact the administrator of this site for help on getting the plugins activated.', 'noor' ),
				'notice_ask_to_update'            => _n_noop( 'The following plugin needs to be updated to its latest version to ensure maximum compatibility with this theme: %1$s.', 'The following plugins need to be updated to their latest version to ensure maximum compatibility with this theme: %1$s.', 'noor' ),
				'notice_cannot_update'            => _n_noop( 'Sorry, but you do not have the correct permissions to update the %s plugin. Contact the administrator of this site for help on getting the plugin updated.', 'Sorry, but you do not have the correct permissions to update the %s plugins. Contact the administrator of this site for help on getting the plugins updated.', 'noor' ),
				'install_link'                    => _n_noop( 'Begin installing plugin', 'Begin installing plugins', 'noor' ),
				'activate_link'                   => _n_noop( 'Activate installed plugin', 'Activate installed plugins', 'noor' ),
				'return'                          => esc_html__( 'Return to Required Plugins Installer', 'noor' ),
				'plugin_activated'                => esc_html__( 'Plugin activated successfully.', 'noor' ),
				'complete'                        => esc_html__( 'All plugins installed and activated successfully. %s', 'noor' ),
				'nag_type'                        => 'updated'
			)
		);

		tgmpa( $plugins, $config );

	}
endif;


if ( ! function_exists( 'dima_remove_tgm_install_menu_item' ) ) :
	/**
	 * Remove "Install Plugins" Submenu Item
	 */
	function dima_remove_tgm_install_menu_item() {
		remove_submenu_page( 'themes.php', 'install-required-plugins' );
	}

	add_action( 'admin_menu', 'dima_remove_tgm_install_menu_item', 9999 );
endif;