<?php

/**
 * Sets up the options for Customizer.
 *
 *
 * @package Dima Framework
 * @subpackage Admin customizer
 * @version   1.0.0
 * @since     1.0.0
 * @author    PixelDima <info@pixeldima.com>
 *
 */

/**
 * Class and Function List:
 * Function list:
 * - dima_register_theme_customizer_options()
 * - dima_customizer_controls_list()
 * - dima_customizer_options_list()
 * Classes list:
 */

/**
 * [Functions to register Options into customizer. using "customize_register" hook]
 *
 * @param  [WP_Customize_Manager] $wp_customize [WP_Customize_Manager instance]
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

function dima_register_theme_customizer_options( $wp_customize ) {
	GLOBAL $dima_customizer_data;
	$customizer_path = DIMA_TEMPLATE_PATH . '/framework/functions/admin/customizer';
	include_once $customizer_path . '/choices.php';

	/*[2]=================================================*/
	$allowed_tags = array(
		'strong' => array(),
		'br'     => array(),
		'em'     => array(),
		'p'      => array( 'a' => true ),
		'a'      => array(
			'href'   => true,
			'target' => true,
			'title'  => true,
		)
	);

	//sanitize_callback functions
	function dima_validate_color( $value ) {
		$value = str_replace( ' ', '', $value );
		if ( empty( $value ) || is_array( $value ) ) {
			return 'rgba(0,0,0,0)';
		}
		// check hex color
		if ( preg_match( '|^#([A-Fa-f0-9]{3}){1,2}$|', $value ) ) {
			return $value;
		} //check RGBA color
		elseif ( false !== strpos( $value, 'rgba' ) ) {
			sscanf( $value, 'rgba(%d,%d,%d,%f)', $red, $green, $blue, $alpha );

			return 'rgba(' . esc_attr( $red ) . ',' . esc_attr( $green ) . ',' . esc_attr( $blue ) . ',' . esc_attr( $alpha ) . ')';
		} //check RGB color
		elseif ( false !== strpos( $value, 'rgb' ) ) {
			sscanf( $value, 'rgb(%d,%d,%d)', $red, $green, $blue );

			return 'rgb(' . esc_attr( $red ) . ',' . esc_attr( $green ) . ',' . esc_attr( $blue ) . ')';
		} else {
			return false;
		}
	}

	function dima_validate_slider( $value ) {
		if ( is_numeric( $value ) ) {
			return $value;
		} else {
			return false;
		}
	}

	function dima_validate_image( $value ) {
		return esc_url_raw( $value );
	}

	function dima_validate_radio( $value ) {
		if ( filter_var( $value, FILTER_VALIDATE_URL ) === false ) {
			return sanitize_key( $value );
		} else {
			return $value;
		}
	}

	function dima_validate_attr( $value ) {
		return esc_attr( $value );
	}

	function dima_validate_url( $value ) {
		return esc_url_raw( $value );
	}

	function dima_validate_textarea( $value ) {
		return esc_textarea( $value );
	}

	function dima_validate_html( $value ) {
		return esc_html( $value );
	}

	function dima_return_false( $value ) {
		return $value;
	}

	/**
	 * Out - Sections
	 */
	$i                  = 0;
	$DIMA['sections'][] = array(
		'dima_customizer_section_templates',
		esc_html__( 'Demos', 'noor' ),
		++ $i,
		''
	);

	$DIMA['sections'][] = array(
		'dima_customizer_section_layout',
		esc_html__( 'Site Layout and design', 'noor' ),
		++ $i,
		''
	);

	$DIMA['sections'][] = array(
		'dima_customizer_section_loading',
		esc_html__( 'Loading Screen', 'noor' ),
		++ $i,
		''
	);

	$DIMA['panel'][] = array(
		'dima_customizer_panel_typography',
		esc_html__( 'Typography', 'noor' ),
		esc_html__( 'Typography', 'noor' ),
		++ $i,
	);

	//*Header*//
	$DIMA['panel'][]    = array(
		'dima_customizer_panel_header',
		esc_html__( 'Header', 'noor' ),
		esc_html__( 'Header', 'noor' ),
		++ $i,
	);
	$DIMA['sections'][] = array(
		'dima_customizer_section_navbar_style',
		esc_html__( 'Header Style', 'noor' ),
		++ $i,
		'dima_customizer_panel_header'
	);
	$DIMA['sections'][] = array(
		'dima_customizer_section_navbar_burger_style',
		esc_html__( 'Menu-burger & Search', 'noor' ),
		++ $i,
		'dima_customizer_panel_header'
	);
	$DIMA['sections'][] = array(
		'dima_customizer_section_navbar_logo',
		esc_html__( 'Logo', 'noor' ),
		++ $i,
		'dima_customizer_panel_header'
	);
	$DIMA['sections'][] = array(
		'dima_customizer_section_navbar_logo_rtl',
		esc_html__( 'Logo (RTL)', 'noor' ),
		++ $i,
		'dima_customizer_panel_header'
	);
	$DIMA['sections'][] = array(
		'dima_customizer_section_navbar_menu',
		esc_html__( 'Menu', 'noor' ),
		++ $i,
		'dima_customizer_panel_header'
	);

	$DIMA['sections'][] = array(
		'dima_customizer_section_navbar_page_title',
		esc_html__( 'Page Title', 'noor' ),
		++ $i,
		'dima_customizer_panel_header'
	);

	//**!Header**//

	$DIMA['sections'][] = array(
		'dima_customizer_section_typography_body',
		esc_html__( 'Body', 'noor' ),
		++ $i,
		'dima_customizer_panel_typography'
	);
	$DIMA['sections'][] = array(
		'dima_customizer_section_typography_menu',
		esc_html__( 'Navbar', 'noor' ),
		++ $i,
		'dima_customizer_panel_typography'
	);
	$DIMA['sections'][] = array(
		'dima_customizer_section_typography_sidebar',
		esc_html__( 'Sidebar', 'noor' ),
		++ $i,
		'dima_customizer_panel_typography'
	);

	$DIMA['sections'][] = array(
		'dima_customizer_section_typography_heading_h1',
		esc_html__( 'Heading H1', 'noor' ),
		++ $i,
		'dima_customizer_panel_typography'
	);
	$DIMA['sections'][] = array(
		'dima_customizer_section_typography_heading_h2',
		esc_html__( 'Heading H2', 'noor' ),
		++ $i,
		'dima_customizer_panel_typography'
	);
	$DIMA['sections'][] = array(
		'dima_customizer_section_typography_heading_h3',
		esc_html__( 'Heading H3', 'noor' ),
		++ $i,
		'dima_customizer_panel_typography'
	);
	$DIMA['sections'][] = array(
		'dima_customizer_section_typography_heading_h4',
		esc_html__( 'Heading H4', 'noor' ),
		++ $i,
		'dima_customizer_panel_typography'
	);
	$DIMA['sections'][] = array(
		'dima_customizer_section_typography_heading_h5',
		esc_html__( 'Heading H5', 'noor' ),
		++ $i,
		'dima_customizer_panel_typography'
	);
	$DIMA['sections'][] = array(
		'dima_customizer_section_typography_heading_h6',
		esc_html__( 'Heading H6', 'noor' ),
		++ $i,
		'dima_customizer_panel_typography'
	);
	$DIMA['sections'][] = array(
		'dima_customizer_section_typography_logo',
		esc_html__( 'Logo', 'noor' ),
		++ $i,
		'dima_customizer_panel_typography'
	);
	$DIMA['sections'][] = array(
		'dima_customizer_section_typography_button',
		esc_html__( 'button', 'noor' ),
		++ $i,
		'dima_customizer_panel_typography'
	);

	$DIMA['sections'][] = array( 'dima_customizer_section_blog', esc_html__( 'Blog', 'noor' ), ++ $i, '' );
	$DIMA['sections'][] = array( 'dima_customizer_section_wc', esc_html__( 'WooCommerce', 'noor' ), ++ $i, '' );
	$DIMA['sections'][] = array( 'dima_customizer_section_bbp', esc_html__( 'bbPress', 'noor' ), ++ $i, '' );
	$DIMA['sections'][] = array( 'dima_customizer_section_bp', esc_html__( 'BuddyPress', 'noor' ), ++ $i, '' );

	$DIMA['sections'][] = array(
		'dima_customizer_section_footer',
		esc_html__( 'General Settings', 'noor' ),
		++ $i,
		'dima_customizer_panel_footer'
	);
	$DIMA['sections'][] = array(
		'dima_customizer_section_copyright',
		esc_html__( 'Copyright Settings', 'noor' ),
		++ $i,
		'dima_customizer_panel_footer'
	);
	$DIMA['sections'][] = array(
		'dima_customizer_section_featured',
		esc_html__( 'Featured Area', 'noor' ),
		++ $i,
		'dima_customizer_panel_footer'
	);
	$DIMA['sections'][] = array(
		'dima_customizer_section_footer_widget',
		esc_html__( 'Widgets', 'noor' ),
		++ $i,
		'dima_customizer_panel_footer'
	);
	$DIMA['sections'][] = array( 'dima_customizer_section_social', esc_html__( 'Social', 'noor' ), ++ $i, '' );
	$DIMA['sections'][] = array( 'dima_customizer_global', esc_html__( 'Site icon & SEO', 'noor' ), ++ $i, '' );

	/* Portfolio */
	$DIMA['panel'][] = array(
		'dima_customizer_panel_projects',
		esc_html__( 'Portfolio', 'noor' ),
		esc_html__( 'Portfolio', 'noor' ),
		++ $i,
	);

	$DIMA['sections'][] = array(
		'dima_customizer_base_portfolio_options',
		esc_html__( 'Base portfolio options', 'noor' ),
		++ $i,
		'dima_customizer_panel_projects'
	);

	$DIMA['sections'][] = array(
		'dima_customizer_single_portfolio_item',
		esc_html__( 'Single portfolio item options', 'noor' ),
		++ $i,
		'dima_customizer_panel_projects'
	);

	$DIMA['sections'][] = array(
		'dima_customizer_portfolio_page_options',
		esc_html__( 'Portfolio page options', 'noor' ),
		++ $i,
		'dima_customizer_panel_projects'
	);

	$DIMA['sections'][] = array(
		'dima_customizer_portfolio_hover_options',
		esc_html__( 'Portfolio hover options', 'noor' ),
		++ $i,
		'dima_customizer_panel_projects'
	);

	$DIMA['sections'][] = array(
		'dima_customizer_portfolio_archive_page_options',
		esc_html__( 'Portfolio archive page options', 'noor' ),
		++ $i,
		'dima_customizer_panel_projects'
	);

	$DIMA['sections'][] = array(
		'dima_customizer_related_projects',
		esc_html__( 'Related Projects', 'noor' ),
		++ $i,
		'dima_customizer_panel_projects'
	);


	/**/
	$DIMA['sections'][] = array( 'dima_customizer_section_code', esc_html__( 'Code Fields', 'noor' ), 150, '' );
	$DIMA['sections'][] = array( 'dima_customizer_section_amp', esc_html__( 'AMP', 'noor' ), 150, '' );

	$DIMA['panel'][] = array(
		'dima_customizer_panel_advanced',
		esc_html__( 'Advanced Settings', 'noor' ),
		esc_html__( 'Advanced Settings', 'noor' ),
		200,
	);

	$DIMA['sections'][] = array(
		'dima_customizer_section_advanced',
		esc_html__( 'Advanced', 'noor' ),
		150,
		'dima_customizer_panel_advanced'
	);
	$DIMA['sections'][] = array(
		'dima_customizer_section_ad',
		esc_html__( 'Advertisement', 'noor' ),
		150,
		'dima_customizer_panel_advanced'
	);
	$DIMA['sections'][] = array(
		'dima_customizer_section_loading',
		esc_html__( 'Loading Screen', 'noor' ),
		150,
		'dima_customizer_panel_advanced'
	);


	$DIMA['sections'][] = array( 'dima_customizer_section_custom', esc_html__( 'Custom', 'noor' ), 150, '' );


	/**
	 * Options
	 */

	$DIMA['settings'][] = array( 'dima_layout_site', 'postMessage', 'dima_return_false', 'dima_validate_radio' );
	$DIMA['controls'][] = array(
		'dima_layout_site',
		'radio-image',
		esc_html__( 'Site Layout', 'noor' ),
		'dima_customizer_section_layout',
		$Choices_site_layouts,
		''
	);

	$DIMA['settings'][] = array( 'dima_layout_content', 'refresh', 'dima_validate_radio' );
	$DIMA['controls'][] = array(
		'dima_layout_content',
		'radio-image',
		esc_html__( 'Content Layout', 'noor' ),
		'dima_customizer_section_layout',
		$Choices_of_content_layouts,
		''
	);

	$DIMA['settings'][] = array( 'dima_header_navbar_position', 'refresh', 'dima_validate_radio' );
	$DIMA['controls'][] = array(
		'dima_header_navbar_position',
		'radio-image',
		esc_html__( 'Header Style', 'noor' ),
		'dima_customizer_section_navbar_style',
		$Choices_navbar_position,
		''
	);

	$DIMA['settings'][] = array( 'dima_header_navbar_text_align', 'refresh', 'dima_validate_radio' );
	$DIMA['controls'][] = array(
		'dima_header_navbar_text_align',
		'radio_button_set',
		esc_html__( 'Navbar Alignement', 'noor' ),
		'dima_customizer_section_navbar_style',
		$Choices_alignement,
		''
	);

	$DIMA['settings'][] = array( 'dima_header_navbar_animation', 'refresh', 'dima_validate_radio' );
	$DIMA['controls'][] = array(
		'dima_header_navbar_animation',
		'radio',
		esc_html__( 'Navbar Animation', 'noor' ),
		'dima_customizer_section_navbar_style',
		$Choices_navbar_animation,
		''
	);

	$DIMA['settings'][] = array( 'dima_header_navbar_offset_by_px', 'refresh', 'dima_return_false' );
	$DIMA['controls'][] = array(
		'dima_header_navbar_offset_by_px',
		'text',
		esc_html__( 'Offset By Pixels', 'noor' ),
		'dima_customizer_section_navbar_style',
		''
	);

	$DIMA['settings'][] = array( 'dima_header_navbar_offset_by_id', 'refresh', 'dima_return_false' );
	$DIMA['controls'][] = array(
		'dima_header_navbar_offset_by_id',
		'text',
		esc_html__( 'Offset By Elemnt Target', 'noor' ),
		'dima_customizer_section_navbar_style',
		''
	);

	$DIMA['settings'][] = array( 'dima_header_burger_menu', 'refresh', 'dima_validate_radio' );
	$DIMA['controls'][] = array(
		'dima_header_burger_menu',
		'radio_button_set',
		esc_html__( 'Navbar Burger', 'noor' ),
		'dima_customizer_section_navbar_burger_style',
		$Choices_on_off,
		''
	);

	$DIMA['settings'][] = array( 'dima_header_navbar_burger_mobile', 'refresh', 'dima_validate_radio' );
	$DIMA['controls'][] = array(
		'dima_header_navbar_burger_mobile',
		'radio_button_set',
		esc_html__( 'Display Desktop Burger in mobile', 'noor' ),
		'dima_customizer_section_navbar_burger_style',
		$Choices_on_off,
		esc_html( 'Check “On” to display the contents of Burger Menu desktop version in Mobile.', 'noor' )
	);

	$DIMA['settings'][] = array( 'dima_header_burger_menu_style', 'refresh', 'dima_validate_radio' );
	$DIMA['controls'][] = array(
		'dima_header_burger_menu_style',
		'radio_button_set',
		esc_html__( 'Burger Style', 'noor' ),
		'dima_customizer_section_navbar_burger_style',
		$Choices_burger,
		''
	);

	$DIMA['settings'][] = array( 'dima_header_navbar_transparent', 'refresh', 'dima_validate_radio' );
	$DIMA['controls'][] = array(
		'dima_header_navbar_transparent',
		'radio_button_set',
		esc_html__( 'Navbar Transparent', 'noor' ),
		'dima_customizer_section_navbar_style',
		$Choices_on_off,
		''
	);

	$DIMA['settings'][] = array( 'dima_header_burger_menu_float', 'refresh', 'dima_validate_radio' );
	$DIMA['controls'][] = array(
		'dima_header_burger_menu_float',
		'radio_button_set',
		esc_html__( 'Burger Position', 'noor' ),
		'dima_customizer_section_navbar_burger_style',
		$Choices_left_right_float,
		''
	);

	$DIMA['settings'][] = array( 'dima_header_burger_bg_img', 'refresh', 'dima_validate_image' );
	$DIMA['controls'][] = array(
		'dima_header_burger_bg_img',
		'image',
		esc_html__( 'Burger background Image', 'noor' ),
		'dima_customizer_section_navbar_burger_style',
		''
	);

	$DIMA['settings'][] = array( 'dima_page_title_display', 'refresh', 'dima_validate_radio' );
	$DIMA['controls'][] = array(
		'dima_page_title_display',
		'radio_button_set',
		esc_html__( 'Page Title Display', 'noor' ),
		'dima_customizer_section_navbar_page_title',
		$Choices_on_off,
		''
	);

	$DIMA['settings'][] = array( 'dima_breadcrumb_position', 'refresh', 'dima_validate_radio' );
	$DIMA['controls'][] = array(
		'dima_breadcrumb_position',
		'radio_button_set',
		esc_html__( 'Page Title Style', 'noor' ),
		'dima_customizer_section_navbar_page_title',
		$Choices_breadcrumb,
		''
	);

	$DIMA['settings'][] = array( 'dima_header_navbar_page_title_dark', 'refresh', 'dima_validate_radio' );
	$DIMA['controls'][] = array(
		'dima_header_navbar_page_title_dark',
		'radio_button_set',
		esc_html__( 'Page Title Dark', 'noor' ),
		'dima_customizer_section_navbar_page_title',
		$Choices_on_off,
		esc_html__( '---', 'noor' ),
	);

	$DIMA['settings'][] = array( 'dima_breadcrumb_display', 'refresh', 'dima_validate_radio' );
	$DIMA['controls'][] = array(
		'dima_breadcrumb_display',
		'radio_button_set',
		esc_html__( 'Breadcrumbs Display', 'noor' ),
		'dima_customizer_section_navbar_page_title',
		$Choices_on_off,
		''
	);

	$DIMA['settings'][] = array( 'dima_breadcrumb_background_image', 'refresh', 'dima_validate_image' );
	$DIMA['controls'][] = array(
		'dima_breadcrumb_background_image',
		'image',
		esc_html__( 'Page Title Background Image', 'noor' ),
		'dima_customizer_section_navbar_page_title',
		''
	);

	$DIMA['settings'][] = array( 'dima_page_title_bg_color', 'refresh', 'dima_validate_color' );
	$DIMA['controls'][] = array(
		'dima_page_title_bg_color',
		'color-alpha',
		esc_html__( 'Page Title Background Color', 'noor' ),
		'dima_customizer_section_navbar_page_title',
		''
	);

	$DIMA['settings'][] = array( 'dima_page_title_bg_is_cover', 'refresh', 'dima_validate_radio' );
	$DIMA['controls'][] = array(
		'dima_page_title_bg_is_cover',
		'radio_button_set',
		esc_html__( 'Page Title Color As Cover', 'noor' ),
		'dima_customizer_section_navbar_page_title',
		$Choices_on_off,
		''
	);

	$DIMA['settings'][] = array( 'dima_menu_border_color', 'refresh', 'dima_validate_color' );
	$DIMA['controls'][] = array(
		'dima_menu_border_color',
		'color-alpha',
		esc_html__( 'Header Border Color', 'noor' ),
		'dima_customizer_section_navbar_style',
		''
	);

	$DIMA['settings'][] = array( 'dima_navbar_background_image', 'refresh', 'dima_validate_image' );
	$DIMA['controls'][] = array(
		'dima_navbar_background_image',
		'image',
		esc_html__( 'Side Menu Background Image', 'noor' ),
		'dima_customizer_section_navbar_style',
		esc_html__( 'Work with Fixed Right & Fixed Left menu', 'noor' )
	);

	$DIMA['settings'][] = array( 'dima_menu_copyright_display', 'refresh', 'dima_validate_radio' );
	$DIMA['controls'][] = array(
		'dima_menu_copyright_display',
		'radio_button_set',
		esc_html__( 'Copyright Display', 'noor' ),
		'dima_customizer_section_navbar_style',
		$Choices_on_off,
		esc_html__( 'This option for vertical menu.', 'noor' ),
	);

	$DIMA['settings'][] = array( 'dima_header_search_enable', 'refresh', 'dima_validate_radio' );
	$DIMA['controls'][] = array(
		'dima_header_search_enable',
		'radio_button_set',
		esc_html__( 'Search', 'noor' ),
		'dima_customizer_section_navbar_burger_style',
		$Choices_on_off,
		''
	);

	$DIMA['settings'][] = array( 'dima_header_burger_bg_color', 'refresh', 'dima_validate_color' );
	$DIMA['controls'][] = array(
		'dima_header_burger_bg_color',
		'color-alpha',
		esc_html__( 'Search/burger background Color', 'noor' ),
		'dima_customizer_section_navbar_burger_style',
		''
	);

	$DIMA['settings'][] = array( 'dima_header_search_bg_img', 'refresh', 'dima_validate_image' );
	$DIMA['controls'][] = array(
		'dima_header_search_bg_img',
		'image',
		esc_html__( 'Search background Image', 'noor' ),
		'dima_customizer_section_navbar_burger_style',
		''
	);

	$DIMA['settings'][] = array( 'dima_header_logo_width', 'refresh', 'dima_return_false' );
	$DIMA['controls'][] = array(
		'dima_header_logo_width',
		'text',
		esc_html__( 'Logo Width (px)', 'noor' ),
		'dima_customizer_section_navbar_logo',
		''
	);

	$DIMA['settings'][] = array( 'dima_header_logo', 'refresh', 'dima_validate_image' );
	$DIMA['controls'][] = array(
		'dima_header_logo',
		'image',
		esc_html__( 'Upload Your Logo', 'noor' ),
		'dima_customizer_section_navbar_logo',
		''
	);

	$DIMA['settings'][] = array( 'dima_header_sticky_logo', 'refresh', 'dima_validate_image' );
	$DIMA['controls'][] = array(
		'dima_header_sticky_logo',
		'image',
		esc_html__( 'Upload your logo for sticky & transparent menu', 'noor' ),
		'dima_customizer_section_navbar_logo',
		''
	);

	$DIMA['settings'][] = array( 'dima_header_logo_retina', 'refresh', 'dima_validate_image' );
	$DIMA['controls'][] = array(
		'dima_header_logo_retina',
		'image',
		esc_html__( 'Upload Your Logo (Retina Version @2x)', 'noor' ),
		'dima_customizer_section_navbar_logo',
		''
	);

	$DIMA['settings'][] = array( 'dima_header_sticky_logo_retina', 'refresh', 'dima_validate_image' );
	$DIMA['controls'][] = array(
		'dima_header_sticky_logo_retina',
		'image',
		esc_html__( 'Upload your logo for sticky & transparent menu (Retina Version @2x)', 'noor' ),
		'dima_customizer_section_navbar_logo',
		''
	);

	$DIMA['settings'][] = array( 'dima_header_mobile_logo', 'refresh', 'dima_validate_image' );
	$DIMA['controls'][] = array(
		'dima_header_mobile_logo',
		'image',
		esc_html__( 'Upload Your Mobile Logo', 'noor' ),
		'dima_customizer_section_navbar_logo',
		''
	);

	$DIMA['settings'][] = array( 'dima_header_mobile_logo_retina', 'refresh', 'dima_validate_image' );
	$DIMA['controls'][] = array(
		'dima_header_mobile_logo_retina',
		'image',
		esc_html__( 'Upload Your Mobile Logo (Retina Version @2x)', 'noor' ),
		'dima_customizer_section_navbar_logo',
		''
	);

	//RTL
	$DIMA['settings'][] = array( 'dima_header_logo_width_rtl', 'refresh', 'dima_return_false' );
	$DIMA['controls'][] = array(
		'dima_header_logo_width_rtl',
		'text',
		esc_html__( 'Logo Width (px)', 'noor' ),
		'dima_customizer_section_navbar_logo_rtl',
		''
	);

	$DIMA['settings'][] = array( 'dima_header_logo_rtl', 'refresh', 'dima_validate_image' );
	$DIMA['controls'][] = array(
		'dima_header_logo_rtl',
		'image',
		esc_html__( 'Upload Your Logo', 'noor' ),
		'dima_customizer_section_navbar_logo_rtl',
		''
	);

	$DIMA['settings'][] = array( 'dima_header_sticky_logo_rtl', 'refresh', 'dima_validate_image' );
	$DIMA['controls'][] = array(
		'dima_header_sticky_logo_rtl',
		'image',
		esc_html__( 'Upload Your Sticky Logo', 'noor' ),
		'dima_customizer_section_navbar_logo_rtl',
		''
	);

	$DIMA['settings'][] = array( 'dima_header_mobile_logo_rtl', 'refresh', 'dima_validate_image' );
	$DIMA['controls'][] = array(
		'dima_header_mobile_logo_rtl',
		'image',
		esc_html__( 'Upload Your Mobile Logo', 'noor' ),
		'dima_customizer_section_navbar_logo_rtl',
		''
	);

	$DIMA['settings'][] = array( 'dima_header_logo_retina_rtl', 'refresh', 'dima_validate_image' );
	$DIMA['controls'][] = array(
		'dima_header_logo_retina_rtl',
		'image',
		esc_html__( 'Upload Your Logo (Retina Version @2x)', 'noor' ),
		'dima_customizer_section_navbar_logo_rtl',
		''
	);
	$DIMA['settings'][] = array( 'dima_header_sticky_logo_retina_rtl', 'refresh', 'dima_validate_image' );
	$DIMA['controls'][] = array(
		'dima_header_sticky_logo_retina_rtl',
		'image',
		esc_html__( 'Upload Your Sticky Logo (Retina Version @2x)', 'noor' ),
		'dima_customizer_section_navbar_logo_rtl',
		''
	);

	$DIMA['settings'][] = array( 'dima_header_mobile_logo_retina_rtl', 'refresh', 'dima_validate_image' );
	$DIMA['controls'][] = array(
		'dima_header_mobile_logo_retina_rtl',
		'image',
		esc_html__( 'Upload Your Mobile Logo (Retina Version @2x)', 'noor' ),
		'dima_customizer_section_navbar_logo_rtl',
		''
	);
	//!RTL
	//====================================

	$DIMA['settings'][] = array( 'dima_header_navbar_primary_menu', 'refresh', 'dima_validate_radio' );
	$DIMA['controls'][] = array(
		'dima_header_navbar_primary_menu',
		'radio_button_set',
		esc_html__( 'Display Primary Menu', 'noor' ),
		'dima_customizer_section_navbar_menu',
		$Choices_on_off,
		''
	);

	$DIMA['settings'][] = array( 'dima_header_navbar_icon_menu', 'refresh', 'dima_validate_radio' );
	$DIMA['controls'][] = array(
		'dima_header_navbar_icon_menu',
		'radio_button_set',
		esc_html__( 'Display Icon Menu', 'noor' ),
		'dima_customizer_section_navbar_menu',
		$Choices_on_off,
		''
	);


	$DIMA['settings'][] = array( 'dima_header_navbar_shear_icon', 'refresh', 'dima_validate_radio' );
	$DIMA['controls'][] = array(
		'dima_header_navbar_shear_icon',
		'radio_button_set',
		esc_html__( 'Display social icons', 'noor' ),
		'dima_customizer_section_navbar_menu',
		$Choices_on_off,
		''
	);

	$DIMA['settings'][] = array( 'dima_header_navbar_wpml_lang_show', 'refresh', 'dima_validate_radio' );
	$DIMA['controls'][] = array(
		'dima_header_navbar_wpml_lang_show',
		'radio_button_set',
		esc_html__( 'WPML language switcher', 'noor' ),
		'dima_customizer_section_navbar_menu',
		$Choices_on_off,
		esc_html__( 'Allows you to have the WPML language switcher in header. Note, WPML plugin must be installed (WPML plugin is not included to the theme pack. You can find the plugin here)', 'noor' ) . ': http://wpml.org/',
	);

	$DIMA['settings'][] = array( 'dima_header_navbar_menu_dark', 'refresh', 'dima_validate_radio' );
	$DIMA['controls'][] = array(
		'dima_header_navbar_menu_dark',
		'radio_button_set',
		esc_html__( 'Menu Dark', 'noor' ),
		'dima_customizer_section_navbar_menu',
		$Choices_on_off,
		''
	);
	$DIMA['settings'][] = array( 'dima_header_navbar_sup_menu_dark', 'refresh', 'dima_validate_radio' );
	$DIMA['controls'][] = array(
		'dima_header_navbar_sup_menu_dark',
		'radio_button_set',
		esc_html__( 'Sub-Menu Dark', 'noor' ),
		'dima_customizer_section_navbar_menu',
		$Choices_on_off,
		''
	);

	$DIMA['settings'][] = array( 'dima_header_navbar_button', 'refresh', 'dima_validate_radio' );
	$DIMA['controls'][] = array(
		'dima_header_navbar_button',
		'radio_button_set',
		esc_html__( 'Display Button', 'noor' ),
		'dima_customizer_section_navbar_menu',
		$Choices_on_off,
		''
	);

	$DIMA['settings'][] = array( 'dima_header_navbar_button_url', 'refresh', 'dima_return_false' );
	$DIMA['controls'][] = array(
		'dima_header_navbar_button_url',
		'text',
		esc_html__( 'Button URL', 'noor' ),
		'dima_customizer_section_navbar_menu',
		''
	);

	$DIMA['settings'][] = array( 'dima_header_navbar_button_txt', 'refresh', 'dima_return_false' );
	$DIMA['controls'][] = array(
		'dima_header_navbar_button_txt',
		'text',
		esc_html__( 'Button Text', 'noor' ),
		'dima_customizer_section_navbar_menu',
		''
	);

	$DIMA['settings'][] = array( 'dima_header_navbar_button_bg_color', 'refresh', 'dima_validate_color' );
	$DIMA['controls'][] = array(
		'dima_header_navbar_button_bg_color',
		'color-alpha',
		esc_html__( 'Button background color', 'noor' ),
		'dima_customizer_section_navbar_menu',
		''
	);

	$DIMA['settings'][] = array( 'dima_header_navbar_button_bg_color_hover', 'refresh', 'dima_validate_color' );
	$DIMA['controls'][] = array(
		'dima_header_navbar_button_bg_color_hover',
		'color-alpha',
		esc_html__( 'Button hover background', 'noor' ),
		'dima_customizer_section_navbar_menu',
		''
	);

	$DIMA['settings'][] = array( 'dima_header_navbar_button_txt_color', 'refresh', 'dima_validate_color' );
	$DIMA['controls'][] = array(
		'dima_header_navbar_button_txt_color',
		'color-alpha',
		esc_html__( 'Button text color', 'noor' ),
		'dima_customizer_section_navbar_menu',
		''
	);

	$DIMA['settings'][] = array( 'dima_navbar_background_color', 'postMessage', 'dima_validate_color' );
	$DIMA['controls'][] = array(
		'dima_navbar_background_color',
		'color-alpha',
		esc_html__( 'NavBar Background Color', 'noor' ),
		'dima_customizer_section_navbar_menu',
		''
	);

	$DIMA['settings'][] = array( 'dima_logo_on_top_background_color', 'postMessage', 'dima_validate_color' );
	$DIMA['controls'][] = array(
		'dima_logo_on_top_background_color',
		'color-alpha',
		esc_html__( 'Menu Background Color', 'noor' ),
		'dima_customizer_section_navbar_menu',
		''
	);

	$DIMA['settings'][] = array( 'dima_navbar_background_color_after', 'refresh', 'dima_validate_color' );
	$DIMA['controls'][] = array(
		'dima_navbar_background_color_after',
		'color-alpha',
		esc_html__( 'NavBar Background Color After Fixed', 'noor' ),
		'dima_customizer_section_navbar_menu',
		''
	);

	$DIMA['settings'][] = array( 'dima_navbar_text_color', 'refresh', 'dima_validate_color' );
	$DIMA['controls'][] = array(
		'dima_navbar_text_color',
		'color-alpha',
		esc_html__( 'NavBar Text Color', 'noor' ),
		'dima_customizer_section_navbar_menu',
		''
	);

	$DIMA['settings'][] = array( 'dima_menu_hover_text_color', 'refresh', 'dima_validate_color' );
	$DIMA['controls'][] = array(
		'dima_menu_hover_text_color',
		'color-alpha',
		esc_html__( 'NavBar Text Hover Color', 'noor' ),
		'dima_customizer_section_navbar_menu',
		''
	);

	$DIMA['settings'][] = array( 'dima_navbar_text_color_after', 'refresh', 'dima_validate_color' );
	$DIMA['controls'][] = array(
		'dima_navbar_text_color_after',
		'color-alpha',
		esc_html__( 'NavBar Text Color After Fixed', 'noor' ),
		'dima_customizer_section_navbar_menu',
		''
	);

	$DIMA['settings'][] = array( 'dima_navbar_underline_on_off', 'refresh', 'dima_validate_radio' );
	$DIMA['controls'][] = array(
		'dima_navbar_underline_on_off',
		'radio_button_set',
		esc_html__( 'Display Underline', 'noor' ),
		'dima_customizer_section_navbar_menu',
		$Choices_on_off,
		''
	);

	$DIMA['settings'][] = array( 'dima_navbar_text_hover_color', 'refresh', 'dima_validate_color' );
	$DIMA['controls'][] = array(
		'dima_navbar_text_hover_color',
		'color-alpha',
		esc_html__( 'NavBar Underline Hover Color', 'noor' ),
		'dima_customizer_section_navbar_menu',
		''
	);

	//Submenu
	$DIMA['settings'][] = array( 'dima_submenu_bg_color', 'refresh', 'dima_validate_color' );
	$DIMA['controls'][] = array(
		'dima_submenu_bg_color',
		'color-alpha',
		esc_html__( 'Sub-Menu Background Color', 'noor' ),
		'dima_customizer_section_navbar_menu',
		''
	);

	$DIMA['settings'][] = array( 'dima_submenu_text_color', 'refresh', 'dima_validate_color' );
	$DIMA['controls'][] = array(
		'dima_submenu_text_color',
		'color-alpha',
		esc_html__( 'Sub-Menu Text Color', 'noor' ),
		'dima_customizer_section_navbar_menu',
		''
	);

	$DIMA['settings'][] = array( 'dima_submenu_text_hover_color', 'refresh', 'dima_validate_color' );
	$DIMA['controls'][] = array(
		'dima_submenu_text_hover_color',
		'color-alpha',
		esc_html__( 'Sub-Menu Text Hover Color', 'noor' ),
		'dima_customizer_section_navbar_menu',
		''
	);

	$DIMA['settings'][] = array( 'dima_transparent_navbar_background_color', 'refresh', 'dima_validate_color' );
	$DIMA['controls'][] = array(
		'dima_transparent_navbar_background_color',
		'color-alpha',
		esc_html__( 'Transparent NavBar Background Color', 'noor' ),
		'dima_customizer_section_navbar_menu',
		''
	);

	$DIMA['settings'][] = array( 'dima_transparent_navbar_text_color', 'refresh', 'dima_validate_color' );
	$DIMA['controls'][] = array(
		'dima_transparent_navbar_text_color',
		'color-alpha',
		esc_html__( 'Transparent NavBar Text Color', 'noor' ),
		'dima_customizer_section_navbar_menu',
		''
	);

	//!Submenu

	/*Top Bar*/
	$DIMA['settings'][] = array( 'dima_navbar_option_address_text_topbar', 'refresh', 'dima_return_false' );
	$DIMA['controls'][] = array(
		'dima_navbar_option_address_text_topbar',
		'textarea',
		esc_html__( 'Address', 'noor' ),
		'dima_customizer_section_navbar_menu',
		''
	);

	$DIMA['settings'][] = array( 'dima_navbar_option_email_text_topbar', 'refresh', 'dima_return_false' );
	$DIMA['controls'][] = array(
		'dima_navbar_option_email_text_topbar',
		'textarea',
		esc_html__( 'Email', 'noor' ),
		'dima_customizer_section_navbar_menu',
		''
	);

	$DIMA['settings'][] = array( 'dima_navbar_option_tel_text_topbar', 'refresh', 'dima_return_false' );
	$DIMA['controls'][] = array(
		'dima_navbar_option_tel_text_topbar',
		'textarea',
		esc_html__( 'Phone', 'noor' ),
		'dima_customizer_section_navbar_menu',
		''
	);

	//http://codex.wordpress.org/Formatting_Date_and_Time
	$DIMA['settings'][] = array( 'dima_navbar_option_today_text_topbar', 'refresh', 'dima_return_false' );
	$DIMA['controls'][] = array(
		'dima_navbar_option_today_text_topbar',
		'text',
		esc_html__( 'Today\'s Date Format', 'noor' ),
		'dima_customizer_section_navbar_menu',
		wp_kses( __( 'For more details see <a href="https://codex.wordpress.org/Formatting_Date_and_Time" target="_blank" rel="noopener">Formatting Date and Time</a>', 'noor' ), $allowed_tags )
	);

	$DIMA['settings'][] = array( 'dima_navbar_lang_shortcode', 'refresh', 'dima_return_false' );
	$DIMA['controls'][] = array(
		'dima_navbar_lang_shortcode',
		'textarea',
		esc_html__( 'Language selection shortcode', 'noor' ),
		'dima_customizer_section_navbar_menu',
		esc_attr__( 'In this field you can paste the language shortcode provided by the translating plugin', 'noor' )
	);

	/*End Top Bar*/
	/*--!NavBar--*/

	/* < (8) content width > */
	$DIMA['settings'][] = array( 'dima_content_width', 'postMessage', 'dima_validate_slider' );
	$DIMA['controls'][] = array(
		'dima_content_width',
		'slider',
		esc_html__( 'Site Content Width (%)', 'noor' ),
		$Choices_sizing_content_width,
		'dima_customizer_section_layout',
		''
	);

	/* < (9) box width > */
	$DIMA['settings'][] = array( 'dima_content_max_width', 'postMessage', 'dima_validate_slider' );
	$DIMA['controls'][] = array(
		'dima_content_max_width',
		'slider',
		esc_html__( 'Site Max Content Width (px)', 'noor' ),
		$Choices_max_sizing_content_width,
		'dima_customizer_section_layout',
		''
	);

	/* < (9) box width > */
	$DIMA['settings'][] = array( 'dima_sidebar_width', 'postMessage', 'dima_validate_slider' );
	$DIMA['controls'][] = array(
		'dima_sidebar_width',
		'slider',
		esc_html__( 'Site Sidebar Width (px)', 'noor' ),
		array(
			'min'  => '240',
			'max'  => '350',
			'step' => '5'
		),
		'dima_customizer_section_layout',
		''
	);

	$DIMA['settings'][] = array( 'dima_sticky_sidebar', 'refresh', 'dima_validate_radio' );
	$DIMA['controls'][] = array(
		'dima_sticky_sidebar',
		'radio_button_set',
		esc_html__( 'Sticky Sidebar', 'noor' ),
		'dima_customizer_section_layout',
		$Choices_on_off,
		''
	);

	$DIMA['settings'][] = array( 'dima_frame_size', 'refresh', 'dima_validate_radio' );
	$DIMA['controls'][] = array(
		'dima_frame_size',
		'radio_button_set',
		esc_html__( 'Framed', 'noor' ),
		'dima_customizer_section_layout',
		$Choices_frame_size,
		''
	);

	$DIMA['settings'][] = array( 'dima_frame_color', 'refresh', 'dima_return_false' );
	$DIMA['controls'][] = array(
		'dima_frame_color',
		'color-alpha',
		esc_html__( 'Frame color', 'noor' ),
		'dima_customizer_section_layout',
		''
	);


	$DIMA['settings'][] = array( 'dima_smoothscroll', 'refresh', 'dima_validate_radio' );
	$DIMA['controls'][] = array(
		'dima_smoothscroll',
		'radio_button_set',
		esc_html__( 'Smooth Scroll', 'noor' ),
		'dima_customizer_section_layout',
		$Choices_on_off,
		''
	);

	/* < (5) Website Backgound > */
	$DIMA['settings'][] = array( 'dima_body_background_color', 'refresh', 'dima_validate_color' );
	$DIMA['controls'][] = array(
		'dima_body_background_color',
		'color-alpha',
		esc_html__( 'Website Background Color', 'noor' ),
		'dima_customizer_section_layout',
		''
	);

	$DIMA['settings'][] = array( 'dima_body_background_image', 'refresh', 'dima_validate_image' );
	$DIMA['controls'][] = array(
		'dima_body_background_image',
		'image',
		esc_html__( 'Website Background Image', 'noor' ),
		'dima_customizer_section_layout',
		''
	);

	$DIMA['settings'][] = array( 'dima_body_background_image_repeat', 'refresh', 'dima_return_false' );
	$DIMA['controls'][] = array(
		'dima_body_background_image_repeat',
		'select',
		esc_html__( 'Website Background Repeat', 'noor' ),
		'dima_customizer_section_layout',
		array(
			''          => esc_html__( 'background-repeat', 'noor' ),
			'no-repeat' => esc_html__( 'No Repeat', 'noor' ),
			'repeat'    => esc_html__( 'Repeat All', 'noor' ),
			'repeat-x'  => esc_html__( 'Repeat Horizontally', 'noor' ),
			'repeat-y'  => esc_html__( 'Repeat Vertically', 'noor' ),
			'inherit'   => esc_html__( 'Inherit', 'noor' ),
		),
		wp_kses( __( 'Define the background repeat. <a href=\'http://www.w3schools.com/cssref/pr_background-repeat.asp\' target=\'_blank\'>Check this for reference</a>', 'noor' ), array(
			'a' => array(
				'href'   => array(),
				'target' => array()
			)
		) ),
	);

	$DIMA['settings'][] = array( 'dima_body_background_image_attachment', 'refresh', 'dima_return_false' );
	$DIMA['controls'][] = array(
		'dima_body_background_image_attachment',
		'select',
		esc_html__( 'Website Background Attachment', 'noor' ),
		'dima_customizer_section_layout',
		array(
			''        => esc_html__( 'background-attachement', 'noor' ),
			'fixed'   => esc_html__( 'Fixed', 'noor' ),
			'scroll'  => esc_html__( 'Scroll', 'noor' ),
			'inherit' => esc_html__( 'Inherit', 'noor' ),
		),
		wp_kses( __( "Define the background attachment. <a href='http://www.w3schools.com/cssref/pr_background-attachment.asp' target='_blank'>Check this for reference</a>", 'noor' ),
			array(
				'a' => array(
					'href'   => array(),
					'target' => array()
				)
			) ),
	);

	$DIMA['settings'][] = array( 'dima_body_background_image_position', 'refresh', 'dima_return_false' );
	$DIMA['controls'][] = array(
		'dima_body_background_image_position',
		'select',
		esc_html__( 'Website Background Position', 'noor' ),
		'dima_customizer_section_layout',
		array(
			''              => esc_html__( 'background-position', 'noor' ),
			'left top'      => esc_html__( 'Left Top', 'noor' ),
			'left center'   => esc_html__( 'Left Center', 'noor' ),
			'left bottom'   => esc_html__( 'Left Bottom', 'noor' ),
			'center top'    => esc_html__( 'Center Top', 'noor' ),
			'center center' => esc_html__( 'Center Center', 'noor' ),
			'center bottom' => esc_html__( 'Center Bottom', 'noor' ),
			'right top'     => esc_html__( 'Right Top', 'noor' ),
			'right center'  => esc_html__( 'Right Center', 'noor' ),
			'right bottom'  => esc_html__( 'Right Bottom', 'noor' ),
		),
		wp_kses( __( "Define the background position. <a href='http://www.w3schools.com/cssref/pr_background-position.asp' target='_blank'>Check this for reference</a>", 'noor' ),
			array(
				'a' => array(
					'href'   => array(),
					'target' => array()
				)
			) ),
	);
	$DIMA['settings'][] = array( 'dima_body_background_image_size', 'refresh', 'dima_return_false' );
	$DIMA['controls'][] = array(
		'dima_body_background_image_size',
		'text',
		esc_html__( 'Website Background Size', 'noor' ),
		'dima_customizer_section_layout',
		wp_kses( __( "Define the background size (Default value is 'cover'). <a href='http://www.w3schools.com/cssref/css3_pr_background-size.asp' target='_blank'>Check this for reference</a>", 'noor' ),
			array(
				'a' => array(
					'href'   => array(),
					'target' => array()
				)
			) ),
	);

	$DIMA['settings'][] = array( 'dima_main_color', 'refresh', 'dima_return_false' );
	$DIMA['controls'][] = array(
		'dima_main_color',
		'color-alpha',
		esc_html__( 'Main color', 'noor' ),
		'dima_customizer_section_layout',
		esc_html__( 'This color will be applied to call to action elements such as buttons and hover icons. It\'s preferable to be attractive color and define the main color of your website.', 'noor' )
	);

	$DIMA['settings'][] = array( 'dima_secondary_main_color', 'refresh', 'dima_return_false' );
	$DIMA['controls'][] = array(
		'dima_secondary_main_color',
		'color-alpha',
		esc_html__( 'Secendery Main color', 'noor' ),
		'dima_customizer_section_layout',
		esc_html__( 'This color will be applied to other elements and it\'s preferable to be like your headings color or a dark color.', 'noor' )
	);


	/*==TypoGraphy==*/
	/*--Body--*/

	$DIMA['settings'][] = array( 'dima_custom_font', 'refresh', 'dima_validate_radio' );
	$DIMA['controls'][] = array(
		'dima_custom_font',
		'radio_button_set',
		esc_html__( 'Custom Fonts', 'noor' ),
		'dima_customizer_section_typography_body',
		$Choices_on_off,
		''
	);

	$DIMA['settings'][] = array( 'dima_body_font_list', 'refresh', 'dima_return_false' );
	$DIMA['controls'][] = array(
		'dima_body_font_list',
		'select',
		esc_html__( 'Body Font', 'noor' ),
		'dima_customizer_section_typography_body',
		$list_all_google_font_name,
		''
	);


	$DIMA['settings'][] = array( 'dima_body_weights_list', 'refresh', 'dima_return_false' );
	$DIMA['controls'][] = array(
		'dima_body_weights_list',
		'multi_check',
		esc_html__( 'Body Font Weights ( Included )', 'noor' ),
		'dima_customizer_section_typography_body',
		$list_all_font_weights,
		''
	);

	$DIMA['settings'][] = array( 'dima_body_weight_selected', 'refresh', 'dima_validate_radio' );
	$DIMA['controls'][] = array(
		'dima_body_weight_selected',
		'radio',
		esc_html__( 'Body Font Weights', 'noor' ),
		'dima_customizer_section_typography_body',
		$list_all_font_weight_selected,
		''
	);


	$DIMA['settings'][] = array( 'dima_body_subsets_list', 'refresh', 'dima_return_false' );
	$DIMA['controls'][] = array(
		'dima_body_subsets_list',
		'multi_check',
		esc_html__( 'Font Subsets', 'noor' ),
		'dima_customizer_section_typography_body',
		$list_font_subsets,
		''
	);

	$DIMA['settings'][] = array( 'dima_body_text_size', 'refresh', 'dima_return_false' );
	$DIMA['controls'][] = array(
		'dima_body_text_size',
		'text',
		esc_html__( 'Body Font Size (px)', 'noor' ),
		'dima_customizer_section_typography_body',
		''
	);

	$DIMA['settings'][] = array( 'dima_body_text_color', 'refresh', 'dima_validate_color' );
	$DIMA['controls'][] = array(
		'dima_body_text_color',
		'color',
		esc_html__( 'Body text color', 'noor' ),
		'dima_customizer_section_typography_body',
		''
	);

	$DIMA['settings'][] = array( 'dima_body_link_color', 'refresh', 'dima_return_false' );
	$DIMA['controls'][] = array(
		'dima_body_link_color',
		'color',
		esc_html__( 'Text Link color', 'noor' ),
		'dima_customizer_section_typography_body',
		esc_html__( 'This color will be applied to the important text links in your website and it\'s preferable to be like your website main color.', 'noor' ),
	);

	$DIMA['settings'][] = array( 'dima_body_link_color_hover', 'refresh', 'dima_validate_color' );
	$DIMA['controls'][] = array(
		'dima_body_link_color_hover',
		'color',
		esc_html__( 'Text Link Hover color', 'noor' ),
		'dima_customizer_section_typography_body',
		''
	);

	/*--!Body--*/


	/*--headings 1--*/
	$DIMA['settings'][] = array( 'dima_heading_text_style', 'refresh', 'dima_return_false' );
	$DIMA['controls'][] = array(
		'dima_heading_text_style',
		'font_style',
		esc_html__( 'Heading Font Style', 'noor' ),
		'dima_customizer_section_typography_heading_h1',
		$Choices_font_style,
		''
	);

	$DIMA['settings'][] = array( 'dima_heading_font_list', 'refresh', 'dima_return_false' );
	$DIMA['controls'][] = array(
		'dima_heading_font_list',
		'select',
		esc_html__( 'Headings Font', 'noor' ),
		'dima_customizer_section_typography_heading_h1',
		$list_all_google_font_name,
		''
	);

	$DIMA['settings'][] = array(
		'dima_heading_font_and_weight_list',
		$list_font_weights_and_name,
		'postMessage',
		'dima_return_false'
	);
	$DIMA['settings'][] = array( 'dima_heading_weights_list', 'refresh', 'dima_return_false' );
	$DIMA['controls'][] = array(
		'dima_heading_weights_list',
		'multi_check',
		esc_html__( 'Heading Font Weights ( Included )', 'noor' ),
		'dima_customizer_section_typography_heading_h1',
		$list_all_font_weights,
		''
	);

	$DIMA['settings'][] = array( 'dima_heading_weight_selected', 'refresh', 'dima_validate_radio' );
	$DIMA['controls'][] = array(
		'dima_heading_weight_selected',
		'radio',
		esc_html__( 'Heading Font Weights', 'noor' ),
		'dima_customizer_section_typography_heading_h1',
		$list_all_font_weight_selected,
		''
	);


	$DIMA['settings'][] = array( 'dima_heading_letter_spacing', 'refresh', 'dima_return_false' );
	$DIMA['controls'][] = array(
		'dima_heading_letter_spacing',
		'text',
		esc_html__( 'Headings Letter Spacing', 'noor' ),
		'dima_customizer_section_typography_heading_h1',
		''
	);

	$DIMA['settings'][] = array( 'dima_heading_text_color', 'refresh', 'dima_validate_color' );
	$DIMA['controls'][] = array(
		'dima_heading_text_color',
		'color',
		esc_html__( 'Headings color', 'noor' ),
		'dima_customizer_section_typography_heading_h1',
		''
	);
	/*--!headings 1--*/

	/*--headings 2--*/
	$DIMA['settings'][] = array( 'dima_heading_text_style_2', 'refresh', 'dima_return_false' );
	$DIMA['controls'][] = array(
		'dima_heading_text_style_2',
		'font_style',
		esc_html__( 'Heading Font Style', 'noor' ),
		'dima_customizer_section_typography_heading_h2',
		$Choices_font_style,
		''
	);

	$DIMA['settings'][] = array( 'dima_heading_font_list_2', 'refresh', 'dima_return_false' );
	$DIMA['controls'][] = array(
		'dima_heading_font_list_2',
		'select',
		esc_html__( 'Headings Font', 'noor' ),
		'dima_customizer_section_typography_heading_h2',
		$list_all_google_font_name,
		''
	);

	$DIMA['settings'][] = array(
		'dima_heading_font_and_weight_list_2',
		$list_font_weights_and_name,
		'postMessage',
		'dima_return_false'
	);
	$DIMA['settings'][] = array( 'dima_heading_weights_list_2', 'refresh', 'dima_return_false' );
	$DIMA['controls'][] = array(
		'dima_heading_weights_list_2',
		'multi_check',
		esc_html__( 'Heading Font Weights ( Included )', 'noor' ),
		'dima_customizer_section_typography_heading_h2',
		$list_all_font_weights,
		''
	);

	$DIMA['settings'][] = array( 'dima_heading_weight_selected_2', 'refresh', 'dima_validate_radio' );
	$DIMA['controls'][] = array(
		'dima_heading_weight_selected_2',
		'radio',
		esc_html__( 'Heading Font Weights', 'noor' ),
		'dima_customizer_section_typography_heading_h2',
		$list_all_font_weight_selected,
		''
	);

	$DIMA['settings'][] = array( 'dima_heading_letter_spacing_2', 'refresh', 'dima_return_false' );
	$DIMA['controls'][] = array(
		'dima_heading_letter_spacing_2',
		'text',
		esc_html__( 'Headings Letter Spacing', 'noor' ),
		'dima_customizer_section_typography_heading_h2',
		''
	);

	$DIMA['settings'][] = array( 'dima_heading_text_color_2', 'refresh', 'dima_validate_color' );
	$DIMA['controls'][] = array(
		'dima_heading_text_color_2',
		'color',
		esc_html__( 'Headings color', 'noor' ),
		'dima_customizer_section_typography_heading_h2',
		''
	);
	/*--!headings 2--*/

	/*--headings 3--*/
	$DIMA['settings'][] = array( 'dima_heading_text_style_3', 'refresh', 'dima_return_false' );
	$DIMA['controls'][] = array(
		'dima_heading_text_style_3',
		'font_style',
		esc_html__( 'Heading Font Style', 'noor' ),
		'dima_customizer_section_typography_heading_h3',
		$Choices_font_style,
		''
	);

	$DIMA['settings'][] = array( 'dima_heading_font_list_3', 'refresh', 'dima_return_false' );
	$DIMA['controls'][] = array(
		'dima_heading_font_list_3',
		'select',
		esc_html__( 'Headings Font', 'noor' ),
		'dima_customizer_section_typography_heading_h3',
		$list_all_google_font_name,
		''
	);

	$DIMA['settings'][] = array(
		'dima_heading_font_and_weight_list_3',
		$list_font_weights_and_name,
		'postMessage',
		'dima_return_false'
	);
	$DIMA['settings'][] = array( 'dima_heading_weights_list_3', 'refresh', 'dima_return_false' );
	$DIMA['controls'][] = array(
		'dima_heading_weights_list_3',
		'multi_check',
		esc_html__( 'Heading Font Weights ( Included )', 'noor' ),
		'dima_customizer_section_typography_heading_h3',
		$list_all_font_weights,
		''
	);

	$DIMA['settings'][] = array( 'dima_heading_weight_selected_3', 'refresh', 'dima_validate_radio' );
	$DIMA['controls'][] = array(
		'dima_heading_weight_selected_3',
		'radio',
		esc_html__( 'Heading Font Weights', 'noor' ),
		'dima_customizer_section_typography_heading_h3',
		$list_all_font_weight_selected,
		''
	);

	$DIMA['settings'][] = array( 'dima_heading_letter_spacing_3', 'refresh', 'dima_return_false' );
	$DIMA['controls'][] = array(
		'dima_heading_letter_spacing_3',
		'text',
		esc_html__( 'Headings Letter Spacing', 'noor' ),
		'dima_customizer_section_typography_heading_h3',
		''
	);

	$DIMA['settings'][] = array( 'dima_heading_text_color_3', 'refresh', 'dima_validate_color' );
	$DIMA['controls'][] = array(
		'dima_heading_text_color_3',
		'color',
		esc_html__( 'Headings color', 'noor' ),
		'dima_customizer_section_typography_heading_h3',
		''
	);
	/*--!headings 3--*/

	/*--headings 4--*/
	$DIMA['settings'][] = array( 'dima_heading_text_style_4', 'refresh', 'dima_return_false' );
	$DIMA['controls'][] = array(
		'dima_heading_text_style_4',
		'font_style',
		esc_html__( 'Heading Font Style', 'noor' ),
		'dima_customizer_section_typography_heading_h4',
		$Choices_font_style,
		''
	);

	$DIMA['settings'][] = array( 'dima_heading_font_list_4', 'refresh', 'dima_return_false' );
	$DIMA['controls'][] = array(
		'dima_heading_font_list_4',
		'select',
		esc_html__( 'Headings Font', 'noor' ),
		'dima_customizer_section_typography_heading_h4',
		$list_all_google_font_name,
		''
	);

	$DIMA['settings'][] = array(
		'dima_heading_font_and_weight_list_4',
		$list_font_weights_and_name,
		'postMessage',
		'dima_return_false'
	);
	$DIMA['settings'][] = array( 'dima_heading_weights_list_4', 'refresh', 'dima_return_false' );
	$DIMA['controls'][] = array(
		'dima_heading_weights_list_4',
		'multi_check',
		esc_html__( 'Heading Font Weights ( Included )', 'noor' ),
		'dima_customizer_section_typography_heading_h4',
		$list_all_font_weights,
		''
	);

	$DIMA['settings'][] = array( 'dima_heading_weight_selected_4', 'refresh', 'dima_validate_radio' );
	$DIMA['controls'][] = array(
		'dima_heading_weight_selected_4',
		'radio',
		esc_html__( 'Heading Font Weights', 'noor' ),
		'dima_customizer_section_typography_heading_h4',
		$list_all_font_weight_selected,
		''
	);

	$DIMA['settings'][] = array( 'dima_heading_letter_spacing_4', 'refresh', 'dima_return_false' );
	$DIMA['controls'][] = array(
		'dima_heading_letter_spacing_4',
		'text',
		esc_html__( 'Headings Letter Spacing', 'noor' ),
		'dima_customizer_section_typography_heading_h4',
		''
	);

	$DIMA['settings'][] = array( 'dima_heading_text_color_4', 'refresh', 'dima_validate_color' );
	$DIMA['controls'][] = array(
		'dima_heading_text_color_4',
		'color',
		esc_html__( 'Headings color', 'noor' ),
		'dima_customizer_section_typography_heading_h4',
		''
	);
	/*--!headings 4--*/

	/*--headings 5--*/
	$DIMA['settings'][] = array( 'dima_heading_text_style_5', 'refresh', 'dima_return_false' );
	$DIMA['controls'][] = array(
		'dima_heading_text_style_5',
		'font_style',
		esc_html__( 'Heading Font Style', 'noor' ),
		'dima_customizer_section_typography_heading_h5',
		$Choices_font_style,
		''
	);

	$DIMA['settings'][] = array( 'dima_heading_font_list_5', 'refresh', 'dima_return_false' );
	$DIMA['controls'][] = array(
		'dima_heading_font_list_5',
		'select',
		esc_html__( 'Headings Font', 'noor' ),
		'dima_customizer_section_typography_heading_h5',
		$list_all_google_font_name,
		''
	);

	$DIMA['settings'][] = array(
		'dima_heading_font_and_weight_list_5',
		$list_font_weights_and_name,
		'postMessage',
		'dima_return_false'
	);
	$DIMA['settings'][] = array( 'dima_heading_weights_list_5', 'refresh', 'dima_return_false' );
	$DIMA['controls'][] = array(
		'dima_heading_weights_list_5',
		'multi_check',
		esc_html__( 'Heading Font Weights ( Included )', 'noor' ),
		'dima_customizer_section_typography_heading_h5',
		$list_all_font_weights,
		''
	);

	$DIMA['settings'][] = array( 'dima_heading_weight_selected_5', 'refresh', 'dima_validate_radio' );
	$DIMA['controls'][] = array(
		'dima_heading_weight_selected_5',
		'radio',
		esc_html__( 'Heading Font Weights', 'noor' ),
		'dima_customizer_section_typography_heading_h5',
		$list_all_font_weight_selected,
		''
	);

	$DIMA['settings'][] = array( 'dima_heading_letter_spacing_5', 'refresh', 'dima_return_false' );
	$DIMA['controls'][] = array(
		'dima_heading_letter_spacing_5',
		'text',
		esc_html__( 'Headings Letter Spacing', 'noor' ),
		'dima_customizer_section_typography_heading_h5',
		''
	);

	$DIMA['settings'][] = array( 'dima_heading_text_color_5', 'refresh', 'dima_validate_color' );
	$DIMA['controls'][] = array(
		'dima_heading_text_color_5',
		'color',
		esc_html__( 'Headings color', 'noor' ),
		'dima_customizer_section_typography_heading_h5',
		''
	);
	/*--!headings 5--*/

	/*--headings 6--*/
	$DIMA['settings'][] = array( 'dima_heading_text_style_6', 'refresh', 'dima_return_false' );
	$DIMA['controls'][] = array(
		'dima_heading_text_style_6',
		'font_style',
		esc_html__( 'Heading Font Style', 'noor' ),
		'dima_customizer_section_typography_heading_h6',
		$Choices_font_style,
		''
	);

	$DIMA['settings'][] = array( 'dima_heading_font_list_6', 'refresh', 'dima_return_false' );
	$DIMA['controls'][] = array(
		'dima_heading_font_list_6',
		'select',
		esc_html__( 'Headings Font', 'noor' ),
		'dima_customizer_section_typography_heading_h6',
		$list_all_google_font_name,
		''
	);

	$DIMA['settings'][] = array(
		'dima_heading_font_and_weight_list_6',
		$list_font_weights_and_name,
		'postMessage',
		'dima_return_false'
	);
	$DIMA['settings'][] = array( 'dima_heading_weights_list_6', 'refresh', 'dima_return_false' );
	$DIMA['controls'][] = array(
		'dima_heading_weights_list_6',
		'multi_check',
		esc_html__( 'Heading Font Weights ( Included )', 'noor' ),
		'dima_customizer_section_typography_heading_h6',
		$list_all_font_weights,
		''
	);

	$DIMA['settings'][] = array( 'dima_heading_weight_selected_6', 'refresh', 'dima_validate_radio' );
	$DIMA['controls'][] = array(
		'dima_heading_weight_selected_6',
		'radio',
		esc_html__( 'Heading Font Weights', 'noor' ),
		'dima_customizer_section_typography_heading_h6',
		$list_all_font_weight_selected,
		''
	);

	$DIMA['settings'][] = array( 'dima_heading_letter_spacing_6', 'refresh', 'dima_return_false' );
	$DIMA['controls'][] = array(
		'dima_heading_letter_spacing_6',
		'text',
		esc_html__( 'Headings Letter Spacing', 'noor' ),
		'dima_customizer_section_typography_heading_h6',
		''
	);

	$DIMA['settings'][] = array( 'dima_heading_text_color_6', 'refresh', 'dima_validate_color' );
	$DIMA['controls'][] = array(
		'dima_heading_text_color_6',
		'color',
		esc_html__( 'Headings color', 'noor' ),
		'dima_customizer_section_typography_heading_h6',
		''
	);
	/*--!headings 6--*/

	/*--Menu--*/
	$DIMA['settings'][] = array( 'dima_navbar_text_style', 'refresh', 'dima_return_false' );
	$DIMA['controls'][] = array(
		'dima_navbar_text_style',
		'font_style',
		esc_html__( 'Navbar Font Style', 'noor' ),
		'dima_customizer_section_typography_menu',
		$Choices_font_style,
		''
	);

	$DIMA['settings'][] = array( 'dima_navbar_font_list', 'refresh', 'dima_return_false' );
	$DIMA['controls'][] = array(
		'dima_navbar_font_list',
		'select',
		esc_html__( 'Navbar Font', 'noor' ),
		'dima_customizer_section_typography_menu',
		$list_all_google_font_name,
		''
	);


	$DIMA['settings'][] = array(
		'dima_navbar_font_and_weight_list',
		$list_font_weights_and_name,
		'postMessage',
		'dima_return_false'
	);
	$DIMA['settings'][] = array( 'dima_navbar_weights_list', 'refresh', 'dima_return_false' );
	$DIMA['controls'][] = array(
		'dima_navbar_weights_list',
		'multi_check',
		esc_html__( 'Navbar Font Weights ( Included )', 'noor' ),
		'dima_customizer_section_typography_menu',
		$list_all_font_weights,
		''
	);

	$DIMA['settings'][] = array( 'dima_navbar_weight_selected', 'refresh', 'dima_validate_radio' );
	$DIMA['controls'][] = array(
		'dima_navbar_weight_selected',
		'radio',
		esc_html__( 'Navbar Font Weights', 'noor' ),
		'dima_customizer_section_typography_menu',
		$list_all_font_weight_selected,
		''
	);


	$DIMA['settings'][] = array( 'dima_navbar_text_size', 'refresh', 'dima_return_false' );
	$DIMA['controls'][] = array(
		'dima_navbar_text_size',
		'text',
		esc_html__( 'Navbar Font Size', 'noor' ),
		'dima_customizer_section_typography_menu',
		''
	);

	/*--logo--*/

	$DIMA['settings'][] = array( 'dima_logo_text_style', 'refresh', 'dima_return_false' );
	$DIMA['controls'][] = array(
		'dima_logo_text_style',
		'font_style',
		esc_html__( 'Logo Font Style', 'noor' ),
		'dima_customizer_section_typography_logo',
		$Choices_font_style,
		''
	);

	$DIMA['settings'][] = array( 'dima_logo_font_list', 'refresh', 'dima_return_false' );
	$DIMA['controls'][] = array(
		'dima_logo_font_list',
		'select',
		esc_html__( 'Logo Font', 'noor' ),
		'dima_customizer_section_typography_logo',
		$list_all_google_font_name,
		''
	);

	$DIMA['settings'][] = array(
		'dima_logo_font_and_weight_list',
		$list_font_weights_and_name,
		'postMessage',
		'dima_return_false'
	);
	$DIMA['settings'][] = array( 'dima_logo_weights_list', 'refresh', 'dima_return_false' );
	$DIMA['controls'][] = array(
		'dima_logo_weights_list',
		'multi_check',
		esc_html__( 'Logo Font Weights ( Included )', 'noor' ),
		'dima_customizer_section_typography_logo',
		$list_all_font_weights,
		''
	);

	$DIMA['settings'][] = array( 'dima_logo_weight_selected', 'refresh', 'dima_validate_radio' );
	$DIMA['controls'][] = array(
		'dima_logo_weight_selected',
		'radio',
		esc_html__( 'Logo Font Weights', 'noor' ),
		'dima_customizer_section_typography_logo',
		$list_all_font_weight_selected,
		''
	);


	$DIMA['settings'][] = array( 'dima_logo_text_size', 'refresh', 'dima_return_false' );
	$DIMA['controls'][] = array(
		'dima_logo_text_size',
		'text',
		esc_html__( 'Logo Font Size (px)', 'noor' ),
		'dima_customizer_section_typography_logo',
		''
	);

	$DIMA['settings'][] = array( 'dima_logo_letter_spacing', 'refresh', 'dima_return_false' );
	$DIMA['controls'][] = array(
		'dima_logo_letter_spacing',
		'text',
		esc_html__( 'Logo Letter Spacing (px)', 'noor' ),
		'dima_customizer_section_typography_logo',
		''
	);

	$DIMA['settings'][] = array( 'dima_logo_text_color', 'refresh', 'dima_validate_color' );
	$DIMA['controls'][] = array(
		'dima_logo_text_color',
		'color',
		esc_html__( 'Logo Text Color', 'noor' ),
		'dima_customizer_section_typography_logo',
		''
	);

	/*--!logo--*/

	/*Button typo*/
	$DIMA['settings'][] = array( 'dima_btn_font_list', 'refresh', 'dima_return_false' );
	$DIMA['controls'][] = array(
		'dima_btn_font_list',
		'select',
		esc_html__( 'Button Font', 'noor' ),
		'dima_customizer_section_typography_button',
		$list_all_google_font_name,
		''
	);


	$DIMA['settings'][] = array( 'dima_btn_weights_list', 'refresh', 'dima_return_false' );
	$DIMA['controls'][] = array(
		'dima_btn_weights_list',
		'multi_check',
		esc_html__( 'Button Font Weights ( Included )', 'noor' ),
		'dima_customizer_section_typography_button',
		$list_all_font_weights,
		''
	);

	$DIMA['settings'][] = array( 'dima_btn_weight_selected', 'refresh', 'dima_validate_radio' );
	$DIMA['controls'][] = array(
		'dima_btn_weight_selected',
		'radio',
		esc_html__( 'Button Font Weights', 'noor' ),
		'dima_customizer_section_typography_button',
		$list_all_font_weight_selected,
		''
	);


	$DIMA['settings'][] = array( 'dima_btn_subsets_list', 'refresh', 'dima_return_false' );
	$DIMA['controls'][] = array(
		'dima_btn_subsets_list',
		'multi_check',
		esc_html__( 'Font Subsets', 'noor' ),
		'dima_customizer_section_typography_button',
		$list_font_subsets,
		''
	);

	$DIMA['settings'][] = array( 'dima_btn_text_size', 'refresh', 'dima_return_false' );
	$DIMA['controls'][] = array(
		'dima_btn_text_size',
		'text',
		esc_html__( 'Button Font Size', 'noor' ),
		'dima_customizer_section_typography_button',
		''
	);
	/*!Button typo*/
	/*--BLOG--*/

	$DIMA['settings'][] = array( 'dima_blog_style', 'refresh', 'dima_validate_radio' );
	$DIMA['controls'][] = array(
		'dima_blog_style',
		'radio-image',
		esc_html__( 'Posts Style', 'noor' ),
		'dima_customizer_section_blog',
		$Choices_of_blog_styles,
		''
	);

	$DIMA['settings'][] = array( 'dima_blog_layout', 'refresh', 'dima_validate_radio' );
	$DIMA['controls'][] = array(
		'dima_blog_layout',
		'radio-image',
		esc_html__( 'Content Layout', 'noor' ),
		'dima_customizer_section_blog',
		$Choices_of_section_layouts,
		''
	);

	$DIMA['settings'][] = array( 'dima_blog_masonry_columns', 'refresh', 'dima_validate_radio' );
	$DIMA['controls'][] = array(
		'dima_blog_masonry_columns',
		'radio',
		esc_html__( 'Columns', 'noor' ),
		'dima_customizer_section_blog',
		$Choices_masonry_columns,
		''
	);


	$DIMA['settings'][] = array( 'dima_blog_comments_style', 'refresh', 'dima_validate_radio' );
	$DIMA['controls'][] = array(
		'dima_blog_comments_style',
		'radio_button_set',
		esc_html__( 'Comments Style', 'noor' ),
		'dima_customizer_section_blog',
		$Choices_comments_style,
		''
	);

	$DIMA['settings'][] = array( 'dima_blog_enable_full_post_index', 'refresh', 'dima_validate_radio' );
	$DIMA['controls'][] = array(
		'dima_blog_enable_full_post_index',
		'radio_button_set',
		esc_html__( 'Full Post Content on Index', 'noor' ),
		'dima_customizer_section_blog',
		$Choices_on_off,
		''
	);

	$DIMA['settings'][] = array( 'dima_blog_blog_excerpt', 'refresh', 'dima_return_false' );
	$DIMA['controls'][] = array(
		'dima_blog_blog_excerpt',
		'text',
		esc_html__( 'Excerpt Limit', 'noor' ),
		'dima_customizer_section_blog',
		esc_html__( 'Displayed Post Excerpt', 'noor' ),
		''
	);

	$DIMA['settings'][] = array( 'dima_blog_enable_post_meta', 'refresh', 'dima_validate_radio' );
	$DIMA['controls'][] = array(
		'dima_blog_enable_post_meta',
		'radio_button_set',
		esc_html__( 'Display Post Meta', 'noor' ),
		'dima_customizer_section_blog',
		$Choices_on_off,
		''
	);

	$DIMA['settings'][] = array( 'dima_blog_enable_post_meta_cat', 'refresh', 'dima_validate_radio' );
	$DIMA['controls'][] = array(
		'dima_blog_enable_post_meta_cat',
		'radio_button_set',
		esc_html__( 'Display Categories Meta', 'noor' ),
		'dima_customizer_section_blog',
		$Choices_on_off,
		''
	);

	$DIMA['settings'][] = array( 'dima_blog_enable_post_meta_comment', 'refresh', 'dima_validate_radio' );
	$DIMA['controls'][] = array(
		'dima_blog_enable_post_meta_comment',
		'radio_button_set',
		esc_html__( 'Display Comment Meta', 'noor' ),
		'dima_customizer_section_blog',
		$Choices_on_off,
		''
	);

	$DIMA['settings'][] = array( 'dima_blog_enable_featured_image', 'refresh', 'dima_validate_radio' );
	$DIMA['controls'][] = array(
		'dima_blog_enable_featured_image',
		'radio_button_set',
		esc_html__( 'Featured Image', 'noor' ),
		'dima_customizer_section_blog',
		$Choices_on_off,
		''
	);


	$DIMA['settings'][] = array( 'dima_elm_hover', 'refresh', 'dima_validate_radio' );
	$DIMA['controls'][] = array(
		'dima_elm_hover',
		'radio',
		esc_html__( 'Blog Element Hover Effects ', 'noor' ),
		'dima_customizer_section_blog',
		$Choices_element_hover,
		''
	);

	$DIMA['settings'][] = array( 'dima_img_hover', 'refresh', 'dima_validate_radio' );
	$DIMA['controls'][] = array(
		'dima_img_hover',
		'radio',
		esc_html__( 'Blog Image Hover Effects ', 'noor' ),
		'dima_customizer_section_blog',
		$Choices_image_hover,
		''
	);

	$DIMA['settings'][] = array( 'dima_second_title_on_post', 'refresh', 'dima_validate_radio' );
	$DIMA['controls'][] = array(
		'dima_second_title_on_post',
		'radio_button_set',
		esc_html__( 'Second Title', 'noor' ),
		'dima_customizer_section_blog',
		$Choices_on_off,
		''
	);

	$DIMA['settings'][] = array( 'dima_pagination_post', 'refresh', 'dima_validate_radio' );
	$DIMA['controls'][] = array(
		'dima_pagination_post',
		'radio_button_set',
		esc_html__( 'Display Previous/Next Pagination ', 'noor' ),
		'dima_customizer_section_blog',
		$Choices_on_off,
		''
	);

	$DIMA['settings'][] = array( 'dima_shear_icons_post', 'refresh', 'dima_validate_radio' );
	$DIMA['controls'][] = array(
		'dima_shear_icons_post',
		'radio_button_set',
		esc_html__( 'Display Social Share Icons', 'noor' ),
		'dima_customizer_section_blog',
		$Choices_on_off,
		''
	);

	$DIMA['settings'][] = array( 'dima_author_post', 'refresh', 'dima_validate_radio' );
	$DIMA['controls'][] = array(
		'dima_author_post',
		'radio_button_set',
		esc_html__( 'Display Author Box', 'noor' ),
		'dima_customizer_section_blog',
		$Choices_on_off,
		''
	);

	$DIMA['settings'][] = array( 'dima_post_related_display', 'refresh', 'dima_validate_radio' );
	$DIMA['controls'][] = array(
		'dima_post_related_display',
		'radio_button_set',
		esc_html__( 'Display Related Posts', 'noor' ),
		'dima_customizer_section_blog',
		$Choices_on_off,
		''
	);

	$DIMA['settings'][] = array( 'dima_post_related_is_slide', 'refresh', 'dima_validate_radio' );
	$DIMA['controls'][] = array(
		'dima_post_related_is_slide',
		'radio_button_set',
		esc_html__( 'Related Posts Is Slide', 'noor' ),
		'dima_customizer_section_blog',
		$Choices_on_off,
		''
	);

	$DIMA['settings'][] = array( 'dima_post_related_columns', 'refresh', 'dima_validate_radio' );
	$DIMA['controls'][] = array(
		'dima_post_related_columns',
		'radio',
		esc_html__( 'Related Posts Columns', 'noor' ),
		'dima_customizer_section_blog',
		$Choices_masonry_columns,
		''
	);

	$DIMA['settings'][] = array( 'dima_post_related_count', 'refresh', 'dima_return_false' );
	$DIMA['controls'][] = array(
		'dima_post_related_count',
		'text',
		esc_html__( 'Related Posts Count', 'noor' ),
		'dima_customizer_section_blog',
		''
	);

	$DIMA['settings'][] = array( 'dima_pagination_bg_color', 'refresh', 'dima_return_false' );
	$DIMA['controls'][] = array(
		'dima_pagination_bg_color',
		'color',
		esc_html__( 'Pagination background color', 'noor' ),
		'dima_customizer_section_blog',
		''
	);

	/*-----------------------*/
	$DIMA['settings'][] = array( 'dima_structure_data', 'refresh', 'dima_validate_radio' );
	$DIMA['controls'][] = array(
		'dima_structure_data',
		'radio_button_set',
		esc_html__( 'Structure Data', 'noor' ),
		'dima_customizer_section_blog',
		$Choices_on_off,
		''
	);
	$DIMA['settings'][] = array( 'dima_schema_type', 'refresh', 'dima_validate_radio' );
	$DIMA['controls'][] = array(
		'dima_schema_type',
		'radio',
		esc_html__( 'Default Schema type', 'noor' ),
		'dima_customizer_section_blog',
		array(
			'NewsArticle' => esc_html__( 'NewsArticle', 'noor' ),
			'Article'     => esc_html__( 'Article', 'noor' ),
			'BlogPosting' => esc_html__( 'BlogPosting', 'noor' ),
		),
		''
	);


	/*--!BLOG--*/

	/*--Sidebar*/

	$DIMA['settings'][] = array( 'dima_sidebar_widget_header_size', 'refresh', 'dima_return_false' );
	$DIMA['controls'][] = array(
		'dima_sidebar_widget_header_size',
		'text',
		esc_html__( 'Header text size', 'noor' ),
		'dima_customizer_section_typography_sidebar',
		''
	);

	$DIMA['settings'][] = array( 'dima_sidebar_widget_header_uppercase', 'refresh', 'dima_return_false' );
	$DIMA['controls'][] = array(
		'dima_sidebar_widget_header_uppercase',
		'font_style',
		esc_html__( 'Header Font Style', 'noor' ),
		'dima_customizer_section_typography_sidebar',
		$Choices_font_style,
		''
	);

	$DIMA['settings'][] = array( 'dima_sidebar_widget_body_size', 'refresh', 'dima_return_false' );
	$DIMA['controls'][] = array(
		'dima_sidebar_widget_body_size',
		'text',
		esc_html__( 'Body/Link Text Size', 'noor' ),
		'dima_customizer_section_typography_sidebar',
		''
	);

	$DIMA['settings'][] = array( 'dima_sidebar_widget_body_uppercase', 'refresh', 'dima_return_false' );
	$DIMA['controls'][] = array(
		'dima_sidebar_widget_body_uppercase',
		'font_style',
		esc_html__( 'Body/Link Font Style', 'noor' ),
		'dima_customizer_section_typography_sidebar',
		$Choices_font_style,
		''
	);


	/*--!Sidebar*/

	/*--!Woo*/

	$DIMA['settings'][] = array( 'dima_shop_menu', 'refresh', 'dima_validate_radio' );
	$DIMA['controls'][] = array(
		'dima_shop_menu',
		'radio_button_set',
		esc_html__( 'Cart', 'noor' ),
		'dima_customizer_section_wc',
		$Choices_on_off,
		''
	);

	$DIMA['settings'][] = array( 'dima_shop_sub_menu', 'refresh', 'dima_validate_radio' );
	$DIMA['controls'][] = array(
		'dima_shop_sub_menu',
		'radio_button_set',
		esc_html__( 'Cart Dropdown', 'noor' ),
		'dima_customizer_section_wc',
		$Choices_on_off,
		''
	);

	$DIMA['settings'][] = array( 'dima_navbar_option_myaccount_display_topbar', 'refresh', 'dima_validate_radio' );
	$DIMA['controls'][] = array(
		'dima_navbar_option_myaccount_display_topbar',
		'radio_button_set',
		esc_html__( 'My Account', 'noor' ),
		'dima_customizer_section_wc',
		$Choices_on_off,
		''
	);

	$DIMA['settings'][] = array( 'dima_navbar_option_wishlist', 'refresh', 'dima_validate_radio' );
	$DIMA['controls'][] = array(
		'dima_navbar_option_wishlist',
		'radio_button_set',
		esc_html__( 'Wishlist', 'noor' ),
		'dima_customizer_section_wc',
		$Choices_on_off,
		''
	);

	$DIMA['settings'][] = array( 'dima_shop_layout', 'refresh', 'dima_validate_radio' );
	$DIMA['controls'][] = array(
		'dima_shop_layout',
		'radio-image',
		esc_html__( 'Shop Layout', 'noor' ),
		'dima_customizer_section_wc',
		$Choices_of_content_shop_layouts,
		''
	);

	$DIMA['settings'][] = array( 'dima_shop_columns', 'refresh', 'dima_validate_radio' );
	$DIMA['controls'][] = array(
		'dima_shop_columns',
		'radio',
		esc_html__( 'Shop Columns', 'noor' ),
		'dima_customizer_section_wc',
		$Choices_masonry_columns,
		''
	);

	$DIMA['settings'][] = array( 'dima_shop_posts_per_page', 'refresh', 'dima_return_false' );
	$DIMA['controls'][] = array(
		'dima_shop_posts_per_page',
		'text',
		esc_html__( 'Product Per Page', 'noor' ),
		'dima_customizer_section_wc',
		''
	);

	$DIMA['settings'][] = array( 'dima_shop_elm_hover', 'refresh', 'dima_validate_radio' );
	$DIMA['controls'][] = array(
		'dima_shop_elm_hover',
		'radio',
		esc_html__( 'Shop Element Hover Effects', 'noor' ),
		'dima_customizer_section_wc',
		$Choices_element_hover,
		''
	);

	$DIMA['settings'][] = array( 'dima_shop_sort', 'refresh', 'dima_validate_radio' );
	$DIMA['controls'][] = array(
		'dima_shop_sort',
		'radio_button_set',
		esc_html__( 'Noor Sort', 'noor' ),
		'dima_customizer_section_wc',
		$Choices_on_off,
		''
	);

	$DIMA['settings'][] = array( 'dima_shop_product_layout', 'refresh', 'dima_validate_radio' );
	$DIMA['controls'][] = array(
		'dima_shop_product_layout',
		'radio_button_set',
		esc_html__( 'Product Layout', 'noor' ),
		'dima_customizer_section_wc',
		$Choices_shop_product_layout,
		''
	);

	//Single
	$DIMA['settings'][] = array( 'dima_shop_product_tap_display', 'refresh', 'dima_validate_radio' );
	$DIMA['controls'][] = array(
		'dima_shop_product_tap_display',
		'radio_button_set',
		esc_html__( 'Product Tabs', 'noor' ),
		'dima_customizer_section_wc',
		$Choices_on_off,
		''
	);

	$DIMA['settings'][] = array( 'dima_shop_description_tap_display', 'refresh', 'dima_validate_radio' );
	$DIMA['controls'][] = array(
		'dima_shop_description_tap_display',
		'radio_button_set',
		esc_html__( 'Description Tab', 'noor' ),
		'dima_customizer_section_wc',
		$Choices_on_off,
		''
	);

	$DIMA['settings'][] = array( 'dima_shop_info_tap_display', 'refresh', 'dima_validate_radio' );
	$DIMA['controls'][] = array(
		'dima_shop_info_tap_display',
		'radio_button_set',
		esc_html__( 'Additional Information Tab', 'noor' ),
		'dima_customizer_section_wc',
		$Choices_on_off,
		''
	);

	$DIMA['settings'][] = array( 'dima_shop_reviews_tap_display', 'refresh', 'dima_validate_radio' );
	$DIMA['controls'][] = array(
		'dima_shop_reviews_tap_display',
		'radio_button_set',
		esc_html__( 'Reviews Tab', 'noor' ),
		'dima_customizer_section_wc',
		$Choices_on_off,
		''
	);


	$DIMA['settings'][] = array( 'dima_shop_slide_animation', 'refresh', 'dima_validate_radio' );
	$DIMA['controls'][] = array(
		'dima_shop_slide_animation',
		'radio_button_set',
		esc_html__( 'Slide Animation Type', 'noor' ),
		'dima_customizer_section_wc',
		$Choices_slide_animation,
		''
	);

	$DIMA['settings'][] = array( 'dima_shop_related_products_display', 'refresh', 'dima_validate_radio' );
	$DIMA['controls'][] = array(
		'dima_shop_related_products_display',
		'radio_button_set',
		esc_html__( 'Related Products', 'noor' ),
		'dima_customizer_section_wc',
		$Choices_on_off,
		''
	);

	$DIMA['settings'][] = array( 'dima_shop_related_product_columns', 'refresh', 'dima_validate_radio' );
	$DIMA['controls'][] = array(
		'dima_shop_related_product_columns',
		'radio',
		esc_html__( 'Related Product Columns', 'noor' ),
		'dima_customizer_section_wc',
		$Choices_masonry_columns,
		''
	);

	$DIMA['settings'][] = array( 'dima_shop_related_product_count', 'refresh', 'dima_return_false' );
	$DIMA['controls'][] = array(
		'dima_shop_related_product_count',
		'text',
		esc_html__( 'Related Product Post Count', 'noor' ),
		'dima_customizer_section_wc',
		''
	);

	$DIMA['settings'][] = array( 'dima_shop_upsells_display', 'refresh', 'dima_validate_radio' );
	$DIMA['controls'][] = array(
		'dima_shop_upsells_display',
		'radio_button_set',
		esc_html__( 'Upsells', 'noor' ),
		'dima_customizer_section_wc',
		$Choices_on_off,
		''
	);

	$DIMA['settings'][] = array( 'dima_shop_upsells_columns', 'refresh', 'dima_validate_radio' );
	$DIMA['controls'][] = array(
		'dima_shop_upsells_columns',
		'radio',
		esc_html__( 'Upsell Columns', 'noor' ),
		'dima_customizer_section_wc',
		$Choices_masonry_columns,
		''
	);

	$DIMA['settings'][] = array( 'dima_shop_upsells_count', 'refresh', 'dima_return_false' );
	$DIMA['controls'][] = array(
		'dima_shop_upsells_count',
		'text',
		esc_html__( 'Upsell Post Count', 'noor' ),
		'dima_customizer_section_wc',
		''
	);
	//cart
	$DIMA['settings'][] = array( 'dima_shop_cart_display', 'refresh', 'dima_validate_radio' );
	$DIMA['controls'][] = array(
		'dima_shop_cart_display',
		'radio_button_set',
		esc_html__( 'Cross Sells', 'noor' ),
		'dima_customizer_section_wc',
		$Choices_on_off,
		''
	);

	$DIMA['settings'][] = array( 'dima_shop_cart_columns', 'refresh', 'dima_validate_radio' );
	$DIMA['controls'][] = array(
		'dima_shop_cart_columns',
		'radio',
		esc_html__( 'Cross Sells Columns', 'noor' ),
		'dima_customizer_section_wc',
		$Choices_masonry_columns,
		''
	);

	$DIMA['settings'][] = array( 'dima_shop_cart_count', 'refresh', 'dima_return_false' );
	$DIMA['controls'][] = array(
		'dima_shop_cart_count',
		'text',
		esc_html__( 'Cross Sells Post Count', 'noor' ),
		'dima_customizer_section_wc',
		''
	);


	/*--!Woo*/

	/*Widgetbar*/

	/*!Widgetbar*/

	/*Footer*/

	$DIMA['settings'][] = array( 'dima_footer_full_width', 'refresh', 'dima_validate_radio' );
	$DIMA['controls'][] = array(
		'dima_footer_full_width',
		'radio_button_set',
		esc_html__( 'Full Width', 'noor' ),
		'dima_customizer_section_footer',
		$Choices_on_off,
		''
	);

	$DIMA['settings'][] = array( 'dima_footer_parallax', 'refresh', 'dima_validate_radio' );
	$DIMA['controls'][] = array(
		'dima_footer_parallax',
		'radio_button_set',
		esc_html__( 'Footer Parallax', 'noor' ),
		'dima_customizer_section_footer',
		$Choices_on_off,
		''
	);

	$DIMA['settings'][] = array( 'dima_footer_big', 'refresh', 'dima_validate_radio' );
	$DIMA['controls'][] = array(
		'dima_footer_big',
		'radio_button_set',
		esc_html__( 'Big Footer ( Widgets Area )', 'noor' ),
		'dima_customizer_section_footer',
		$Choices_on_off,
		''
	);

	$DIMA['settings'][] = array( 'dima_second_footer_on_off', 'refresh', 'dima_validate_radio' );
	$DIMA['controls'][] = array(
		'dima_second_footer_on_off',
		'radio_button_set',
		esc_html__( 'Second Footer Widgets Area', 'noor' ),
		'dima_customizer_section_footer',
		$Choices_on_off,
		''
	);

	$DIMA['settings'][] = array( 'dima_small_footer_on_off', 'refresh', 'dima_validate_radio' );
	$DIMA['controls'][] = array(
		'dima_small_footer_on_off',
		'radio_button_set',
		esc_html__( 'Small Footer', 'noor' ),
		'dima_customizer_section_footer',
		$Choices_on_off,
		''
	);

	$DIMA['settings'][] = array( 'dima_footer_bottom_center', 'refresh', 'dima_validate_radio' );
	$DIMA['controls'][] = array(
		'dima_footer_bottom_center',
		'radio_button_set',
		esc_html__( 'Footer In Center', 'noor' ),
		'dima_customizer_section_copyright',
		$Choices_on_off,
		''
	);

	$DIMA['settings'][] = array( 'dima_footer_bottom', 'refresh', 'dima_validate_radio' );
	$DIMA['controls'][] = array(
		'dima_footer_bottom',
		'radio_button_set',
		esc_html__( 'Small Footer', 'noor' ),
		'dima_customizer_section_copyright',
		$Choices_on_off,
		''
	);

	$DIMA['settings'][] = array( 'dima_footer_menu_display', 'refresh', 'dima_validate_radio' );
	$DIMA['controls'][] = array(
		'dima_footer_menu_display',
		'radio_button_set',
		esc_html__( 'Footer Menu', 'noor' ),
		'dima_customizer_section_copyright',
		$Choices_on_off,
		''
	);

	$DIMA['settings'][] = array( 'dima_footer_content_body_color', 'refresh', 'dima_return_false' );
	$DIMA['controls'][] = array(
		'dima_footer_content_body_color',
		'color',
		esc_html__( 'Small Footer Text Color', 'noor' ),
		'dima_customizer_section_copyright',
		''
	);

	$DIMA['settings'][] = array( 'dima_footer_content_link_color', 'refresh', 'dima_return_false' );
	$DIMA['controls'][] = array(
		'dima_footer_content_link_color',
		'color',
		esc_html__( 'Small Footer Link Color', 'noor' ),
		'dima_customizer_section_copyright',
		''
	);

	$DIMA['settings'][] = array( 'dima_footer_content_display', 'refresh', 'dima_validate_radio' );
	$DIMA['controls'][] = array(
		'dima_footer_content_display',
		'radio_button_set',
		esc_html__( 'Copyright Info on footer', 'noor' ),
		'dima_customizer_section_copyright',
		$Choices_on_off,
		''
	);

	$DIMA['controls'][] = array(
		'dima_footer_content_text',
		'textarea',
		esc_html__( 'Copyright Content', 'noor' ),
		'dima_customizer_section_copyright',
		''
	);

	/*--featured areas--*/
	$DIMA['settings'][] = array( 'dima_footer_featured_area', 'refresh', 'dima_return_false' );
	$DIMA['controls'][] = array(
		'dima_footer_featured_area',
		'radio_button_set',
		esc_html__( 'Featured Areas', 'noor' ),
		'dima_customizer_section_featured',
		$Choices_on_off,
		''
	);
	$DIMA['settings'][] = array( 'dima_footer_featured_top_bg', 'refresh', 'dima_validate_color' );
	$DIMA['controls'][] = array(
		'dima_footer_featured_top_bg',
		'color-alpha',
		esc_html__( 'Background Color', 'noor' ),
		'dima_customizer_section_featured',
		''
	);
	$DIMA['settings'][] = array( 'dima_footer_featured_border_color', 'refresh', 'dima_validate_color' );
	$DIMA['controls'][] = array(
		'dima_footer_featured_border_color',
		'color-alpha',
		esc_html__( 'Border Color', 'noor' ),
		'dima_customizer_section_featured',
		''
	);

	$DIMA['settings'][] = array( 'dima_footer_featured_bg_image', 'refresh', 'dima_validate_image' );
	$DIMA['controls'][] = array(
		'dima_footer_featured_bg_image',
		'image',
		esc_html__( 'Background Image', 'noor' ),
		'dima_customizer_section_featured',
		''
	);

	/*--featured areas--*/

	$DIMA['settings'][] = array( 'dima_footer_is_dark', 'refresh', 'dima_validate_radio' );
	$DIMA['controls'][] = array(
		'dima_footer_is_dark',
		'radio_button_set',
		esc_html__( 'Footer Is dark', 'noor' ),
		'dima_customizer_section_footer',
		$Choices_on_off,
		esc_html__( 'Enabling this option will affect the color scheme of the inputs, dropdown and tags in your footer’s widgets (To change the title, text, link and border colors of your widgets you can find the settings in Footer > Widgets) .', 'noor' ),
	);

	$DIMA['settings'][] = array( 'dima_footer_content_top_bg', 'refresh', 'dima_validate_color' );
	$DIMA['controls'][] = array(
		'dima_footer_content_top_bg',
		'color-alpha',
		esc_html__( 'Footer Background Color', 'noor' ),
		'dima_customizer_section_footer',
		''
	);

	$DIMA['settings'][] = array( 'dima_footer_bg_image', 'refresh', 'dima_validate_image' );
	$DIMA['controls'][] = array(
		'dima_footer_bg_image',
		'image',
		esc_html__( 'Footer Background Image', 'noor' ),
		'dima_customizer_section_footer',
		''
	);

	$DIMA['settings'][] = array( 'dima_footer_go_to_top', 'refresh', 'dima_validate_radio' );
	$DIMA['controls'][] = array(
		'dima_footer_go_to_top',
		'radio_button_set',
		esc_html__( 'Back To The Top Arrow', 'noor' ),
		'dima_customizer_section_footer',
		$Choices_on_off,
		''
	);

	$DIMA['settings'][] = array(
		'dima_footer_content_text',
		'refresh',
		'dima_return_false'
	);

	/*widget*/
	$DIMA['settings'][] = array( 'dima_footer_widget_areas', 'refresh', 'dima_return_false' );
	$DIMA['controls'][] = array(
		'dima_footer_widget_areas',
		'radio-image',
		esc_html__( 'Footer Widget Areas', 'noor' ),
		'dima_customizer_section_footer_widget',
		array(
			'footer-1c'             => '',
			'footer-2c'             => '',
			'footer-2c-narrow-wide' => '',
			'footer-2c-wide-narrow' => '',
			'footer-3c'             => '',
			'footer-3c-wide-left'   => '',
			'footer-3c-wide-right'  => '',
			'footer-4c'             => '',
		),
		''
	);

	$DIMA['settings'][] = array( 'dima_second_footer_widget_areas', 'refresh', 'dima_return_false' );
	$DIMA['controls'][] = array(
		'dima_second_footer_widget_areas',
		'radio-image',
		esc_html__( 'Second Footer Widget Areas', 'noor' ),
		'dima_customizer_section_footer_widget',
		array(
			'footer-1c'             => '',
			'footer-2c'             => '',
			'footer-2c-narrow-wide' => '',
			'footer-2c-wide-narrow' => '',
			'footer-3c'             => '',
			'footer-3c-wide-left'   => '',
			'footer-3c-wide-right'  => '',
			'footer-4c'             => '',
		),
		''
	);

	$DIMA['settings'][] = array( 'dima_footer_widget_header_size', 'refresh', 'dima_return_false' );
	$DIMA['controls'][] = array(
		'dima_footer_widget_header_size',
		'text',
		esc_html__( 'Widget Header text size', 'noor' ),
		'dima_customizer_section_footer_widget',
		''
	);

	$DIMA['settings'][] = array( 'dima_footer_widget_header_uppercase', 'refresh', 'dima_return_false' );
	$DIMA['controls'][] = array(
		'dima_footer_widget_header_uppercase',
		'font_style',
		esc_html__( 'Widget Header Font Style', 'noor' ),
		'dima_customizer_section_footer_widget',
		$Choices_font_style,
		''
	);

	$DIMA['settings'][] = array( 'dima_footer_widget_body_size', 'refresh', 'dima_return_false' );
	$DIMA['controls'][] = array(
		'dima_footer_widget_body_size',
		'text',
		esc_html__( 'Widget Body/Link Text Size', 'noor' ),
		'dima_customizer_section_footer_widget',
		''
	);

	$DIMA['settings'][] = array( 'dima_footer_widget_body_uppercase', 'refresh', 'dima_return_false' );
	$DIMA['controls'][] = array(
		'dima_footer_widget_body_uppercase',
		'font_style',
		esc_html__( 'Widget Body/Link Font Style', 'noor' ),
		'dima_customizer_section_footer_widget',
		$Choices_font_style,
		''
	);

	$DIMA['settings'][] = array( 'dima_footer_widget_header_color', 'refresh', 'dima_return_false' );
	$DIMA['controls'][] = array(
		'dima_footer_widget_header_color',
		'color',
		esc_html__( 'Widget Header Color', 'noor' ),
		'dima_customizer_section_footer_widget',
		''
	);

	$DIMA['settings'][] = array( 'dima_footer_widget_body_color', 'refresh', 'dima_validate_color' );
	$DIMA['controls'][] = array(
		'dima_footer_widget_body_color',
		'color',
		esc_html__( 'Widget Text Color', 'noor' ),
		'dima_customizer_section_footer_widget',
		''
	);

	$DIMA['settings'][] = array( 'dima_footer_widget_link_color', 'refresh', 'dima_validate_color' );
	$DIMA['controls'][] = array(
		'dima_footer_widget_link_color',
		'color',
		esc_html__( 'Widget Link Color', 'noor' ),
		'dima_customizer_section_footer_widget',
		''
	);

	$DIMA['settings'][] = array( 'dima_footer_widget_link_hover_color', 'refresh', 'dima_validate_color' );
	$DIMA['controls'][] = array(
		'dima_footer_widget_link_hover_color',
		'color',
		esc_html__( 'Widget Hover Link Color', 'noor' ),
		'dima_customizer_section_footer_widget',
		''
	);

	$DIMA['settings'][] = array( 'dima_footer_widget_border_color', 'refresh', 'dima_validate_color' );
	$DIMA['controls'][] = array(
		'dima_footer_widget_border_color',
		'color',
		esc_html__( 'Widget Border Color', 'noor' ),
		'dima_customizer_section_footer_widget',
		''
	);

	if ( ! dima_helper::dima_wp_version_check( '4.7' ) ) {
		$DIMA['settings'][] = array( 'dima_custom_style', 'refresh', 'dima_return_false' );
		$DIMA['controls'][] = array(
			'dima_custom_style',
			'textarea',
			esc_html__( 'CSS', 'noor' ),
			'dima_customizer_section_custom',
			''
		);
	}

	$DIMA['settings'][] = array( 'dima_custom_js', 'refresh', 'dima_return_false' );
	$DIMA['controls'][] = array(
		'dima_custom_js',
		'textarea',
		esc_html__( 'JavaScript', 'noor' ),
		'dima_customizer_section_custom',
		''
	);
	//**  AMP **//
	if ( DIMA_AMP_IS_ACTIVE ) {

		$DIMA['settings'][] = array( 'dima_amp_enable', 'refresh', 'dima_validate_radio' );
		$DIMA['controls'][] = array(
			'dima_amp_enable',
			'radio_button_set',
			esc_html__( 'Enable AMP', 'noor' ),
			'dima_customizer_section_amp',
			$Choices_on_off,
			''
		);

		$DIMA['settings'][] = array( 'dima_amp_logo', 'refresh', 'dima_return_false' );
		$DIMA['controls'][] = array(
			'dima_amp_logo',
			'image',
			esc_html__( 'Upload Your AMP Logo', 'noor' ),
			'dima_customizer_section_amp',
			''
		);

		$DIMA['settings'][] = array( 'dima_amp_facebook_id', 'refresh', 'dima_validate_textarea' );
		$DIMA['controls'][] = array(
			'dima_amp_facebook_id',
			'text',
			esc_html__( 'Facebook App ID', 'noor' ),
			'dima_customizer_section_amp',
			''
		);

		$DIMA['settings'][] = array( 'dima_amp_leave_a_comment', 'refresh', 'dima_validate_textarea' );
		$DIMA['controls'][] = array(
			'dima_amp_leave_a_comment',
			'text',
			esc_html__( 'Leave a Comment ( Translate )', 'noor' ),
			'dima_customizer_section_amp',
			''
		);

		$DIMA['settings'][] = array( 'dima_amp_check_also', 'refresh', 'dima_validate_textarea' );
		$DIMA['controls'][] = array(
			'dima_amp_check_also',
			'text',
			esc_html__( 'Check Also ( Translate )', 'noor' ),
			'dima_customizer_section_amp',
			''
		);

		$DIMA['settings'][] = array( 'dima_amp_categories', 'refresh', 'dima_validate_textarea' );
		$DIMA['controls'][] = array(
			'dima_amp_categories',
			'text',
			esc_html__( 'Categories ( Translate )', 'noor' ),
			'dima_customizer_section_amp',
			''
		);

		$DIMA['settings'][] = array( 'dima_amp_ago', 'refresh', 'dima_validate_textarea' );
		$DIMA['controls'][] = array(
			'dima_amp_ago',
			'text',
			esc_html__( 'Ago ( Translate )', 'noor' ),
			'dima_customizer_section_amp',
			''
		);

		$DIMA['settings'][] = array( 'dima_amp_tags', 'refresh', 'dima_validate_textarea' );
		$DIMA['controls'][] = array(
			'dima_amp_tags',
			'text',
			esc_html__( 'Tags ( Translate )', 'noor' ),
			'dima_customizer_section_amp',
			''
		);

		/*Post Settings*/
		//Related Posts
		$DIMA['settings'][] = array( 'dima_amp_related_posts', 'refresh', 'dima_validate_radio' );
		$DIMA['controls'][] = array(
			'dima_amp_related_posts',
			'radio_button_set',
			esc_html__( 'Related Posts', 'noor' ),
			'dima_customizer_section_amp',
			$Choices_on_off,
			''
		);
		//Share Buttons
		$DIMA['settings'][] = array( 'dima_amp_share_buttons', 'refresh', 'dima_validate_radio' );
		$DIMA['controls'][] = array(
			'dima_amp_share_buttons',
			'radio_button_set',
			esc_html__( 'Share Buttons', 'noor' ),
			'dima_customizer_section_amp',
			$Choices_on_off,
			''
		);

		//Back to top button
		$DIMA['settings'][] = array( 'dima_amp_back_to_top', 'refresh', 'dima_validate_radio' );
		$DIMA['controls'][] = array(
			'dima_amp_back_to_top',
			'radio_button_set',
			esc_html__( 'Back to top button', 'noor' ),
			'dima_customizer_section_amp',
			$Choices_on_off,
			''
		);
		//Footer Logo Image
		$DIMA['settings'][] = array( 'dima_amp_footer_logo', 'refresh', 'dima_return_false' );
		$DIMA['controls'][] = array(
			'dima_amp_footer_logo',
			'image',
			esc_html__( 'Footer Logo Image', 'noor' ),
			'dima_customizer_section_amp',
			''
		);

		//Footer Menu
		$DIMA['settings'][] = array( 'dima_amp_footer_menu', 'refresh', 'dima_return_false' );
		$DIMA['controls'][] = array(
			'dima_amp_footer_menu',
			'select',
			esc_html__( 'Footer Menu', 'noor' ),
			'dima_customizer_section_amp',
			dima_helper::dima_get_menus_list_options(),
			''
		);

		//Copyright Text
		$DIMA['settings'][] = array( 'dima_amp_footer_content_text', 'refresh', 'dima_return_false' );
		$DIMA['controls'][] = array(
			'dima_amp_footer_content_text',
			'textarea',
			esc_html__( 'Copyright Text', 'noor' ),
			'dima_customizer_section_amp',
			''
		);

		//Advertisement
		$DIMA['settings'][] = array( 'dima_amp_ad_abover', 'refresh', 'dima_return_false' );
		$DIMA['controls'][] = array(
			'dima_amp_ad_abover',
			'textarea',
			esc_html__( 'Advertisement Above Content', 'noor' ),
			'dima_customizer_section_amp',
			sprintf( wp_kses_post( __( 'Enter your Ad code, AMP pages support &lt;amp-ad&gt; tag only, <a href="%s" target="_blank">Click Here</a> For More info.', 'noor' ) ), 'https://www.ampproject.org/docs/reference/extended/amp-ad.html' )
		);
		$DIMA['settings'][] = array( 'dima_amp_ad_below', 'refresh', 'dima_return_false' );
		$DIMA['controls'][] = array(
			'dima_amp_ad_below',
			'textarea',
			esc_html__( 'Advertisement Below Content', 'noor' ),
			'dima_customizer_section_amp',
			sprintf( wp_kses_post( __( 'Enter your Ad code, AMP pages support &lt;amp-ad&gt; tag only, <a href="%s" target="_blank">Click Here</a> For More info.', 'noor' ) ), 'https://www.ampproject.org/docs/reference/extended/amp-ad.html' )
		);

		/* Styling */
		$DIMA['settings'][] = array( 'dima_amp_bg_color', 'refresh', 'dima_validate_color' );
		$DIMA['controls'][] = array(
			'dima_amp_bg_color',
			'color-alpha',
			esc_html__( 'Background Color', 'noor' ),
			'dima_customizer_section_amp',
			''
		);

		$DIMA['settings'][] = array( 'dima_amp_header_bg_color', 'refresh', 'dima_validate_color' );
		$DIMA['controls'][] = array(
			'dima_amp_header_bg_color',
			'color-alpha',
			esc_html__( 'Header Background Color', 'noor' ),
			'dima_customizer_section_amp',
			''
		);

		$DIMA['settings'][] = array( 'dima_amp_title_color', 'refresh', 'dima_validate_color' );
		$DIMA['controls'][] = array(
			'dima_amp_title_color',
			'color-alpha',
			esc_html__( 'Title Color', 'noor' ),
			'dima_customizer_section_amp',
			''
		);

		$DIMA['settings'][] = array( 'dima_amp_meta_color', 'refresh', 'dima_validate_color' );
		$DIMA['controls'][] = array(
			'dima_amp_meta_color',
			'color-alpha',
			esc_html__( 'Post meta Color', 'noor' ),
			'dima_customizer_section_amp',
			''
		);

		$DIMA['settings'][] = array( 'dima_amp_link_color', 'refresh', 'dima_validate_color' );
		$DIMA['controls'][] = array(
			'dima_amp_link_color',
			'color-alpha',
			esc_html__( 'Links color', 'noor' ),
			'dima_customizer_section_amp',
			''
		);

		$DIMA['settings'][] = array( 'dima_amp_footer_bg_color', 'refresh', 'dima_validate_color' );
		$DIMA['controls'][] = array(
			'dima_amp_footer_bg_color',
			'color-alpha',
			esc_html__( 'Footer color', 'noor' ),
			'dima_customizer_section_amp',
			''
		);
		$DIMA['settings'][] = array( 'dima_amp_footer_border_color', 'refresh', 'dima_validate_color' );
		$DIMA['controls'][] = array(
			'dima_amp_footer_border_color',
			'color-alpha',
			esc_html__( 'Footer border color', 'noor' ),
			'dima_customizer_section_amp',
			''
		);

	}
	//**  AMP **//

	/*Ad*/

	$DIMA['settings'][] = array( 'dima_ad_blocker_detector', 'refresh', 'dima_validate_radio' );
	$DIMA['controls'][] = array(
		'dima_ad_blocker_detector',
		'radio_button_set',
		esc_html__( 'Ad Blocker Detector', 'noor' ),
		'dima_customizer_section_ad',
		$Choices_on_off,
		''
	);

	//----------------1
	$DIMA['settings'][] = array( 'dima_ad_above_article', 'refresh', 'dima_validate_radio' );
	$DIMA['controls'][] = array(
		'dima_ad_above_article',
		'radio_button_set',
		esc_html__( 'Above Article Ad', 'noor' ),
		'dima_customizer_section_ad',
		$Choices_on_off,
		''
	);
	$DIMA['settings'][] = array( 'dima_ad_above_article_custom_code', 'refresh', 'dima_return_false' );
	$DIMA['controls'][] = array(
		'dima_ad_above_article_custom_code',
		'textarea',
		esc_html__( 'Custom Ad Code -Above-', 'noor' ),
		'dima_customizer_section_ad',
		esc_html__( 'Supports: Text, HTML and Shortcodes', 'noor' ),
	);

	//----------------2
	$DIMA['settings'][] = array( 'dima_ad_below_article', 'refresh', 'dima_validate_radio' );
	$DIMA['controls'][] = array(
		'dima_ad_below_article',
		'radio_button_set',
		esc_html__( 'Below Article Ad', 'noor' ),
		'dima_customizer_section_ad',
		$Choices_on_off,
		''
	);
	$DIMA['settings'][] = array( 'dima_ad_below_article_custom_code', 'refresh', 'dima_return_false' );
	$DIMA['controls'][] = array(
		'dima_ad_below_article_custom_code',
		'textarea',
		esc_html__( 'Custom Ad Code -Below-', 'noor' ),
		'dima_customizer_section_ad',
		esc_html__( 'Supports: Text, HTML and Shortcodes', 'noor' ),
	);

	//----------------3
	$DIMA['settings'][] = array( 'dima_ad1_shortcode', 'refresh', 'dima_return_false' );
	$DIMA['controls'][] = array(
		'dima_ad1_shortcode',
		'textarea',
		esc_html__( '[ads1] Ad Shortcode', 'noor' ),
		'dima_customizer_section_ad',
		esc_html__( 'Supports: Text, HTML and Shortcodes', 'noor' ),
	);
	//----------------3
	$DIMA['settings'][] = array( 'dima_ad2_shortcode', 'refresh', 'dima_return_false' );
	$DIMA['controls'][] = array(
		'dima_ad2_shortcode',
		'textarea',
		esc_html__( '[ads2] Ad Shortcode', 'noor' ),
		'dima_customizer_section_ad',
		esc_html__( 'Supports: Text, HTML and Shortcodes', 'noor' ),
	);
	//----------------3
	$DIMA['settings'][] = array( 'dima_ad3_shortcode', 'refresh', 'dima_return_false' );
	$DIMA['controls'][] = array(
		'dima_ad3_shortcode',
		'textarea',
		esc_html__( '[ads3] Ad Shortcode', 'noor' ),
		'dima_customizer_section_ad',
		esc_html__( 'Supports: Text, HTML and Shortcodes', 'noor' ),
	);
	//----------------3
	$DIMA['settings'][] = array( 'dima_ad4_shortcode', 'refresh', 'dima_return_false' );
	$DIMA['controls'][] = array(
		'dima_ad4_shortcode',
		'textarea',
		esc_html__( '[ads4] Ad Shortcode', 'noor' ),
		'dima_customizer_section_ad',
		esc_html__( 'Supports: Text, HTML and Shortcodes', 'noor' ),
	);
	//----------------3
	$DIMA['settings'][] = array( 'dima_ad5_shortcode', 'refresh', 'dima_return_false' );
	$DIMA['controls'][] = array(
		'dima_ad5_shortcode',
		'textarea',
		esc_html__( '[ads51] Ad Shortcode', 'noor' ),
		'dima_customizer_section_ad',
		esc_html__( 'Supports: Text, HTML and Shortcodes', 'noor' ),
	);
	/*!Ad*/

	/*GIF*/
	$DIMA['settings'][] = array( 'dima_disable_featured_gif', 'refresh', 'dima_validate_radio' );
	$DIMA['controls'][] = array(
		'dima_disable_featured_gif',
		'radio_button_set',
		esc_html__( 'Disable GIF Featured Images', 'noor' ),
		'dima_customizer_section_advanced',
		$Choices_on_off,
		''
	);
	/*!GIF*/

	/*GIF*/
	$DIMA['settings'][] = array( 'dima_minified_files', 'refresh', 'dima_validate_radio' );
	$DIMA['controls'][] = array(
		'dima_minified_files',
		'radio_button_set',
		esc_html__( 'Minified CSS files', 'noor' ),
		'dima_customizer_section_advanced',
		$Choices_on_off,
		''
	);

	$DIMA['settings'][] = array( 'dima_lazy_image', 'refresh', 'dima_validate_radio' );
	$DIMA['controls'][] = array(
		'dima_lazy_image',
		'radio_button_set',
		esc_html__( 'Applay Lazy Image', 'noor' ),
		'dima_customizer_section_advanced',
		$Choices_on_off,
		esc_html__( 'lazy Image mean: Images outside of viewport will not be loaded before user scrolls to them.', 'noor' )
	);

	$DIMA['settings'][] = array( 'dima_fontawesome_five', 'refresh', 'dima_validate_radio' );
	$DIMA['controls'][] = array(
		'dima_fontawesome_five',
		'radio_button_set',
		esc_html__( 'Applay to use the fontawesome v5', 'noor' ),
		'dima_customizer_section_advanced',
		$Choices_on_off,
		''
	);

	$DIMA['settings'][] = array( 'dima_minified_js_files', 'refresh', 'dima_validate_radio' );
	$DIMA['controls'][] = array(
		'dima_minified_js_files',
		'radio_button_set',
		esc_html__( 'Minified JS files', 'noor' ),
		'dima_customizer_section_advanced',
		$Choices_on_off,
		esc_html__( 'Concatenate JS-Module files into one file lib.min.js', 'noor' )
	);
	/*!GIF*/

	$DIMA['settings'][] = array( 'dima_space_before_head', 'refresh', 'dima_return_false' );
	$DIMA['controls'][] = array(
		'dima_space_before_head',
		'textarea',
		esc_html__( 'Space before &lt;/head&gt;', 'noor' ),
		'dima_customizer_section_code',
		esc_html__( 'Only accepts javascript code wrapped with &lt;script&gt;  tags and HTML markup that is valid inside the &lt;head&gt;  tag.', 'noor' )
	);

	$DIMA['settings'][] = array( 'dima_space_before_body', 'refresh', 'dima_return_false' );
	$DIMA['controls'][] = array(
		'dima_space_before_body',
		'textarea',
		esc_html__( 'Space before &lt;/body&gt;', 'noor' ),
		'dima_customizer_section_code',
		esc_html__( 'Only accepts javascript code, wrapped with &lt;script&gt; tags and valid HTML markup inside the &lt;body&gt; tag.', 'noor' )
	);

	$DIMA['settings'][] = array( 'dima_space_after_body', 'refresh', 'dima_return_false' );
	$DIMA['controls'][] = array(
		'dima_space_after_body',
		'textarea',
		esc_html__( 'Space before &lt;/body&gt;', 'noor' ),
		'dima_customizer_section_code',
		esc_html__( 'Only accepts javascript code, wrapped with &lt;script&gt; tags and valid HTML markup inside the &lt;body&gt; tag.', 'noor' )
	);


	$DIMA['settings'][] = array( 'dima_google_map_api_key', 'refresh', 'dima_validate_textarea' );
	$DIMA['controls'][] = array(
		'dima_google_map_api_key',
		'text',
		esc_html__( 'Google Map API Key', 'noor' ),
		'dima_customizer_section_code',
		''
	);


	/*loading*/
	$DIMA['settings'][] = array( 'dima_loading', 'refresh', 'dima_validate_radio' );
	$DIMA['controls'][] = array(
		'dima_loading',
		'radio_button_set',
		esc_html__( 'Loading Screen', 'noor' ),
		'dima_customizer_section_loading',
		$Choices_on_off,
		''
	);

	$DIMA['settings'][] = array( 'dima_loading_logo', 'refresh', 'dima_validate_image' );
	$DIMA['controls'][] = array(
		'dima_loading_logo',
		'image',
		esc_html__( 'Loading Logo', 'noor' ),
		'dima_customizer_section_loading',
		''
	);

	$DIMA['settings'][] = array( 'dima_loading_bg_color', 'refresh', 'dima_validate_color' );
	$DIMA['controls'][] = array(
		'dima_loading_bg_color',
		'color-alpha',
		esc_html__( 'Loading Background Color', 'noor' ),
		'dima_customizer_section_loading',
		''
	);

	$DIMA['settings'][] = array( 'dima_loading_border_color', 'refresh', 'dima_validate_color' );
	$DIMA['controls'][] = array(
		'dima_loading_border_color',
		'color-alpha',
		esc_html__( 'Loading Frame Color', 'noor' ),
		'dima_customizer_section_loading',
		''
	);
	/*! loading*/

	/*social*/

	$DIMA['settings'][] = array( 'dima_social_icons_is_colored', 'refresh', 'dima_validate_radio' );
	$DIMA['controls'][] = array(
		'dima_social_icons_is_colored',
		'radio_button_set',
		esc_html__( 'Use colored icons?', 'noor' ),
		'dima_customizer_section_social',
		$Choices_on_off,
		''
	);

	$DIMA['settings'][] = array( 'dima_social_facebook', 'refresh', 'dima_validate_url' );
	$DIMA['controls'][] = array(
		'dima_social_facebook',
		'text',
		esc_html__( 'Facebook', 'noor' ),
		'dima_customizer_section_social',
		''
	);

	$DIMA['settings'][] = array( 'dima_social_twitter', 'refresh', 'dima_validate_url' );
	$DIMA['controls'][] = array(
		'dima_social_twitter',
		'text',
		esc_html__( 'Twitter', 'noor' ),
		'dima_customizer_section_social',
		''
	);

	$DIMA['settings'][] = array( 'dima_social_googleplus', 'refresh', 'dima_validate_url' );
	$DIMA['controls'][] = array(
		'dima_social_googleplus',
		'text',
		esc_html__( 'Google+', 'noor' ),
		'dima_customizer_section_social',
		''
	);

	$DIMA['settings'][] = array( 'dima_social_linkedin', 'refresh', 'dima_validate_url' );
	$DIMA['controls'][] = array(
		'dima_social_linkedin',
		'text',
		esc_html__( 'linkedin', 'noor' ),
		'dima_customizer_section_social',
		''
	);

	$DIMA['settings'][] = array( 'dima_social_youtube', 'refresh', 'dima_validate_url' );
	$DIMA['controls'][] = array(
		'dima_social_youtube',
		'text',
		esc_html__( 'Youtube', 'noor' ),
		'dima_customizer_section_social',
		''
	);

	$DIMA['settings'][] = array( 'dima_social_vimeo', 'refresh', 'dima_validate_url' );
	$DIMA['controls'][] = array(
		'dima_social_vimeo',
		'text',
		esc_html__( 'Vimeo', 'noor' ),
		'dima_customizer_section_social',
		''
	);

	$DIMA['settings'][] = array( 'dima_social_foursquare', 'refresh', 'dima_validate_url' );
	$DIMA['controls'][] = array(
		'dima_social_foursquare',
		'text',
		esc_html__( 'Foursquare', 'noor' ),
		'dima_customizer_section_social',
		''
	);

	$DIMA['settings'][] = array( 'dima_social_tumblr', 'refresh', 'dima_validate_url' );
	$DIMA['controls'][] = array(
		'dima_social_tumblr',
		'text',
		esc_html__( 'Tumblr', 'noor' ),
		'dima_customizer_section_social',
		''
	);

	$DIMA['settings'][] = array( 'dima_social_instagram', 'refresh', 'dima_validate_url' );
	$DIMA['controls'][] = array(
		'dima_social_instagram',
		'text',
		esc_html__( 'Instagram', 'noor' ),
		'dima_customizer_section_social',
		''
	);

	$DIMA['settings'][] = array( 'dima_social_dribbble', 'refresh', 'dima_return_false' );
	$DIMA['controls'][] = array(
		'dima_social_dribbble',
		'text',
		esc_html__( 'Dribbble', 'noor' ),
		'dima_customizer_section_social',
		''
	);

	$DIMA['settings'][] = array( 'dima_social_flickr', 'refresh', 'dima_return_false' );
	$DIMA['controls'][] = array(
		'dima_social_flickr',
		'text',
		esc_html__( 'Flickr', 'noor' ),
		'dima_customizer_section_social',
		''
	);

	$DIMA['settings'][] = array( 'dima_social_behance', 'refresh', 'dima_return_false' );
	$DIMA['controls'][] = array(
		'dima_social_behance',
		'text',
		esc_html__( 'Behance', 'noor' ),
		'dima_customizer_section_social',
		''
	);

	$DIMA['settings'][] = array( 'dima_social_pinterest', 'refresh', 'dima_return_false' );
	$DIMA['controls'][] = array(
		'dima_social_pinterest',
		'text',
		esc_html__( 'Pinterest', 'noor' ),
		'dima_customizer_section_social',
		''
	);

	$DIMA['settings'][] = array( 'dima_social_whatsapp', 'refresh', 'dima_return_false' );
	$DIMA['controls'][] = array(
		'dima_social_whatsapp',
		'text',
		esc_html__( 'Whatsapp', 'noor' ),
		'dima_customizer_section_social',
		''
	);

	$DIMA['settings'][] = array( 'dima_social_soundcloud', 'refresh', 'dima_return_false' );
	$DIMA['controls'][] = array(
		'dima_social_soundcloud',
		'text',
		esc_html__( 'Soundcloud', 'noor' ),
		'dima_customizer_section_social',
		''
	);

	$DIMA['settings'][] = array( 'dima_social_rss', 'refresh', 'dima_return_false' );
	$DIMA['controls'][] = array(
		'dima_social_rss',
		'text',
		esc_html__( 'RSS', 'noor' ),
		'dima_customizer_section_social',
		''
	);

	$DIMA['settings'][] = array( 'dima_social_vk', 'refresh', 'dima_return_false' );
	$DIMA['controls'][] = array(
		'dima_social_vk',
		'text',
		esc_html__( 'VK', 'noor' ),
		'dima_customizer_section_social',
		''
	);

	/*social*/
	if ( ! dima_helper::dima_wp_version_check( '4.3' ) ) {

		$DIMA['settings'][] = array( 'dima_favicon', 'refresh', 'dima_validate_image' );
		$DIMA['controls'][] = array(
			'dima_favicon',
			'image',
			esc_html__( 'Upload Favicon', 'noor' ),
			'dima_customizer_global',
			wp_kses( __( 'There are many favicon generators available online, such as  <a href="http://www.convertico.com/" target="_blank" rel="noopener" title="Convertico">Convertico</a> and <a href="http://www.favicon.co.uk/" target="_blank" rel="noopener" title="Favicon.co.uk">Favicon.co.uk</a>.', 'noor' ), $allowed_tags )
		);

		$DIMA['settings'][] = array( 'dima_iphone_icon', 'refresh', 'dima_validate_image' );
		$DIMA['controls'][] = array(
			'dima_iphone_icon',
			'image',
			esc_html__( 'Apple iPhone Icon', 'noor' ),
			'dima_customizer_global',
			esc_html__( 'Icon for Apple iPhone (57px x 57px)', 'noor' )
		);

		$DIMA['settings'][] = array( 'dima_iphone_retina_icon', 'refresh', 'dima_validate_image' );
		$DIMA['controls'][] = array(
			'dima_iphone_retina_icon',
			'image',
			esc_html__( 'Apple iPhone Retina Icon', 'noor' ),
			'dima_customizer_global',
			esc_html__( 'Icon for Apple iPhone Retina Version (120px x 120px)', 'noor' )
		);

		$DIMA['settings'][] = array( 'dima_ipad_icon', 'refresh', 'dima_validate_image' );
		$DIMA['controls'][] = array(
			'dima_ipad_icon',
			'image',
			esc_html__( 'Apple iPad Icon', 'noor' ),
			'dima_customizer_global',
			esc_html__( 'Icon for Apple iPhone (72px x 72px)', 'noor' )
		);

		$DIMA['settings'][] = array( 'dima_ipad_retina_icon', 'refresh', 'dima_validate_image' );
		$DIMA['controls'][] = array(
			'dima_ipad_retina_icon',
			'image',
			esc_html__( 'Apple iPad Retina Icon', 'noor' ),
			'dima_customizer_global',
			esc_html__( 'Icon for Apple iPad Retina Version (144px x 144px)', 'noor' )
		);
	}

	$DIMA['settings'][] = array( 'dima_open_graph_meta_tag', 'refresh', 'dima_validate_radio' );
	$DIMA['controls'][] = array(
		'dima_open_graph_meta_tag',
		'radio_button_set',
		esc_html__( 'Open Graph', 'noor' ),
		'dima_customizer_global',
		$Choices_on_off,
		''
	);

	$DIMA['settings'][] = array( 'dima_opengraph_image', 'refresh', 'dima_validate_image' );
	$DIMA['controls'][] = array(
		'dima_opengraph_image',
		'image',
		esc_html__( 'Upload opengraph image', 'noor' ),
		'dima_customizer_global',
		esc_html__( 'Image URL which should represent your object within the graph.', 'noor' )
	);

	/* Portfolio Options */
	$DIMA['settings'][] = array( 'dima_projects_related_display', 'refresh', 'dima_validate_radio' );
	$DIMA['controls'][] = array(
		'dima_projects_related_display',
		'radio_button_set',
		esc_html__( 'Display Related Projects', 'noor' ),
		'dima_customizer_related_projects',
		$Choices_on_off,
		''
	);

	$DIMA['settings'][] = array( 'dima_projects_related_style', 'refresh', 'dima_validate_radio' );
	$DIMA['controls'][] = array(
		'dima_projects_related_style',
		'radio_button_set',
		esc_html__( 'Related Projects Style', 'noor' ),
		'dima_customizer_related_projects',
		$Choices_related_projects_style,
		''
	);

	$DIMA['settings'][] = array( 'dima_projects_related_columns', 'refresh', 'dima_return_false' );
	$DIMA['controls'][] = array(
		'dima_projects_related_columns',
		'radio',
		esc_html__( 'Related Projects Columns', 'noor' ),
		'dima_customizer_related_projects',
		$Choices_masonry_columns,
		''
	);

	$DIMA['settings'][] = array( 'dima_projects_related_count', 'refresh', 'dima_return_false' );
	$DIMA['controls'][] = array(
		'dima_projects_related_count',
		'text',
		esc_html__( 'Related Projects Count', 'noor' ),
		'dima_customizer_related_projects',
		''
	);

	$DIMA['settings'][] = array( 'dima_projects_related_elm_hover', 'refresh', 'dima_return_false' );
	$DIMA['controls'][] = array(
		'dima_projects_related_elm_hover',
		'radio',
		esc_html__( 'Related Projects Element Hover Effects ', 'noor' ),
		'dima_customizer_related_projects',
		$Choices_element_hover_related_projects,
		''
	);


	$DIMA['settings'][] = array( 'dima_projects_related_img_hover', 'refresh', 'dima_return_false' );
	$DIMA['controls'][] = array(
		'dima_projects_related_img_hover',
		'radio',
		esc_html__( 'Related Projects Image Hover Effects ', 'noor' ),
		'dima_customizer_related_projects',
		$Choices_image_hover,
		''
	);

	/*------------ Single portfolio ----------*/
	$DIMA['settings'][] = array( 'dima_projects_details', 'refresh', 'dima_validate_radio' );
	$DIMA['controls'][] = array(
		'dima_projects_details',
		'radio_button_set',
		esc_html__( 'Portfolio Details', 'noor' ),
		'dima_customizer_single_portfolio_item',
		$Choices_on_off,
		''
	);

	$DIMA['settings'][] = array( 'dima_projects_details_style', 'refresh', 'dima_validate_radio' );
	$DIMA['controls'][] = array(
		'dima_projects_details_style',
		'radio_button_set',
		esc_html__( 'Portfolio Details Style', 'noor' ),
		'dima_customizer_single_portfolio_item',
		$Choices_details_style,
		''
	);

	$DIMA['settings'][] = array( 'dima_projects_details_layout', 'refresh', 'dima_return_false' );
	$DIMA['controls'][] = array(
		'dima_projects_details_layout',
		'select',
		esc_html__( 'Portfolio Details Layout', 'noor' ),
		'dima_customizer_single_portfolio_item',
		$Choices_portfolio_details_layout,
		''
	);

	$DIMA['settings'][] = array( 'dima_layout_projects_details_content', 'refresh', 'dima_validate_radio' );
	$DIMA['controls'][] = array(
		'dima_layout_projects_details_content',
		'radio-image',
		esc_html__( 'Content Layout', 'noor' ),
		'dima_customizer_single_portfolio_item',
		$Choices_of_section_layouts,
		''
	);

	$DIMA['settings'][] = array( 'dima_projects_details_sidebar', 'refresh', 'dima_return_false' );
	$DIMA['controls'][] = array(
		'dima_projects_details_sidebar',
		'select',
		esc_html__( 'Portfolio Sidebar', 'noor' ),
		'dima_customizer_single_portfolio_item',
		$sidebars_name,
		''
	);

	$DIMA['settings'][] = array( 'dima_projects_details_pagination', 'refresh', 'dima_return_false' );
	$DIMA['controls'][] = array(
		'dima_projects_details_pagination',
		'radio_button_set',
		esc_html__( 'Inside pagination', 'noor' ),
		'dima_customizer_single_portfolio_item',
		$Choices_on_off,
		''
	);

	$DIMA['settings'][] = array( 'dima_projects_portfolio_shear_icon_and_tag', 'refresh', 'dima_return_false' );
	$DIMA['controls'][] = array(
		'dima_projects_portfolio_shear_icon_and_tag',
		'radio_button_set',
		esc_html__( 'Share icons and Tags', 'noor' ),
		'dima_customizer_single_portfolio_item',
		$Choices_on_off,
		''
	);

	/*------------ !Single portfolio ----------*/

	/*------------ Portfolio page options ----------*/

	$DIMA['settings'][] = array( 'dima_portfolio_page_columns', 'refresh', 'dima_return_false' );
	$DIMA['controls'][] = array(
		'dima_projects_related_columns',
		'radio',
		esc_html__( 'Portfolio Columns', 'noor' ),
		'dima_customizer_portfolio_page_options',
		$Choices_masonry_columns,
		''
	);

	$DIMA['settings'][] = array( 'dima_portfolio_page_elm_hover', 'refresh', 'dima_return_false' );
	$DIMA['controls'][] = array(
		'dima_projects_related_elm_hover',
		'radio',
		esc_html__( 'Portfolio Element Hover Effects ', 'noor' ),
		'dima_customizer_portfolio_page_options',
		$Choices_element_hover_related_projects,
		''
	);


	$DIMA['settings'][] = array( 'dima_portfolio_page_img_hover', 'refresh', 'dima_return_false' );
	$DIMA['controls'][] = array(
		'dima_projects_related_img_hover',
		'radio',
		esc_html__( 'Portfolio Image Hover Effects ', 'noor' ),
		'dima_customizer_portfolio_page_options',
		$Choices_image_hover,
		''
	);

	$DIMA['settings'][] = array( 'dima_portfolio_page_filters', 'refresh', 'dima_return_false' );
	$DIMA['controls'][] = array(
		'dima_portfolio_page_filters',
		'radio_button_set',
		esc_html__( 'Filters', 'noor' ),
		'dima_customizer_portfolio_page_options',
		$Choices_on_off,
		''
	);

	$DIMA['settings'][] = array( 'dima_portfolio_page_margin', 'refresh', 'dima_return_false' );
	$DIMA['controls'][] = array(
		'dima_portfolio_page_margin',
		'radio_button_set',
		esc_html__( 'No Margin', 'noor' ),
		'dima_customizer_portfolio_page_options',
		$Choices_on_off,
		''
	);

	$DIMA['settings'][] = array( 'dima_portfolio_page_count', 'refresh', 'dima_return_false' );
	$DIMA['controls'][] = array(
		'dima_portfolio_page_count',
		'text',
		esc_html__( 'Count', 'noor' ),
		'dima_customizer_portfolio_page_options',
		''
	);

	$DIMA['settings'][] = array( 'dima_portfolio_page_is_pagination', 'refresh', 'dima_return_false' );
	$DIMA['controls'][] = array(
		'dima_portfolio_page_is_pagination',
		'radio_button_set',
		esc_html__( 'Pagination', 'noor' ),
		'dima_customizer_portfolio_page_options',
		$Choices_on_off,
		''
	);


	/*------------ !Portfolio page options ----------*/

	$DIMA['settings'][] = array( 'dima_projects_slug_name', 'refresh', 'dima_validate_attr' );
	$DIMA['controls'][] = array(
		'dima_projects_slug_name',
		'text',
		esc_html__( 'Portfolio URL Slug', 'noor' ),
		'dima_customizer_base_portfolio_options',
		esc_html__( 'This option allows you to change the portfolio slug. Set your custom slug which will be displayed instead of portfolio in portfolio URL. Please do not forget to save the permalinks in Settings -> Permalinks after changing this option', 'noor' ),
	);

	$DIMA['settings'][] = array( 'dima_portfolio_page_name', 'refresh', 'dima_validate_attr' );
	$DIMA['controls'][] = array(
		'dima_portfolio_page_name',
		'text',
		esc_html__( 'Portfolio page name', 'noor' ),
		'dima_customizer_base_portfolio_options',
		esc_html__( 'Set the title for your portfolio page', 'noor' ),
	);

	$DIMA['settings'][] = array( 'dima_portfolio_top_link_source', 'refresh', 'dima_validate_radio' );
	$DIMA['controls'][] = array(
		'dima_portfolio_top_link_source',
		'radio_button_set',
		esc_html__( 'Portfolio top link source', 'noor' ),
		'dima_customizer_base_portfolio_options',
		$Choices_link_source,
		'',
	);

	$DIMA['settings'][] = array( 'dima_portfolio_page_url', 'refresh', 'dima_validate_attr' );
	$DIMA['controls'][] = array(
		'dima_portfolio_page_url',
		'text',
		esc_html__( 'Portfolio page URL', 'noor' ),
		'dima_customizer_base_portfolio_options',
		esc_html__( 'Set the portfolio page URL which will be used as main portfolio page', 'noor' ),
	);
	$DIMA['settings'][] = array( 'dima_portfolio_page', 'refresh', 'dima_return_false' );
	$DIMA['controls'][] = array(
		'dima_portfolio_page',
		'dropdown-pages',
		esc_html__( 'Portfolio page', 'noor' ),
		'dima_customizer_base_portfolio_options',
		'',
		true,
	);

	/* !Portfolio Options */

	if ( DIMA_BBPRESS_IS_ACTIVE ) {
		$DIMA['settings'][] = array( 'dima_layout_bbpress_content', 'refresh', 'dima_validate_radio' );
		$DIMA['controls'][] = array(
			'dima_layout_bbpress_content',
			'radio-image',
			esc_html__( 'Content Layout', 'noor' ),
			'dima_customizer_section_bbp',
			$Choices_of_content_layouts,
			''
		);
	}


	if ( DIMA_BUDDYPRESS_IS_ACTIVE ) {
		$DIMA['settings'][] = array( 'dima_layout_bp_content', 'refresh', 'dima_validate_radio' );
		$DIMA['controls'][] = array(
			'dima_layout_bp_content',
			'radio-image',
			esc_html__( 'Content Layout', 'noor' ),
			'dima_customizer_section_bp',
			$Choices_of_content_layouts,
			''
		);
	}

	dima_customizer_controls_list( $DIMA, $wp_customize, $dima_customizer_data );
}


GLOBAL $dima_customizer_data;
/**
 * [List of Controls]
 *
 * @param  [array 2D] $DIMA     [Content 4 type of variables 1: Settings 2: Sections 3: Controls  3: Panel]
 * @param  [type] $wp_customize [WP_Customize_Manager instance]
 */
function dima_customizer_controls_list( $DIMA, $wp_customize, $customizer_data ) {
	/** @var wpdb $wpdb */

	foreach ( $DIMA['panel'] as $panel ) {
		$wp_customize->add_panel(
			$panel[0],
			array(
				'title'       => $panel[1],
				'description' => $panel[2],
				'priority'    => $panel[3]
			)
		);
	}

	/**
	 * Out - Sections.
	 */

	foreach ( $DIMA['sections'] as $section ) {
		$wp_customize->add_section(
			$section[0],
			array(
				'title'    => $section[1],
				'priority' => $section[2],
				'panel'    => $section[3]
			)
		);
	}

	/**
	 * Out - Settings
	 */

	foreach ( $DIMA['settings'] as $setting ) {
		$wp_customize->add_setting(
			$setting[0],
			array(
				'type'              => 'option',
				'default'           => $customizer_data[ $setting[0] ],
				'transport'         => $setting[1],
				'sanitize_callback' => $setting[2]
			)
		);
	}

	foreach ( $DIMA['controls'] as $control ) {

		static $priority = 1;
		/**
		 * 1-radio-select
		 * 3-radio_button_set
		 * 4-multi_check
		 * 5-checkbox-text
		 * 6-textarea
		 * 7-slider
		 * 8-color
		 * 9-image
		 */

		switch ( $control[1] ) {
			case 'select':
			case 'radio':
				$wp_customize->add_control(
					$control[0],
					array(
						'type'        => $control[1],
						'label'       => $control[2],
						'section'     => $control[3],
						'choices'     => $control[4],
						'description' => $control[5],
						'priority'    => $priority
					)
				);
				break;

			case 'radio_button_set':
				$wp_customize->add_control(
					new dima_custom_control_radio_button_set(
						$wp_customize,
						$control[0],
						array(
							'type'        => $control[1],
							'label'       => $control[2],
							'section'     => $control[3],
							'choices'     => $control[4],
							'description' => $control[5],
							'priority'    => $priority
						)
					) );
				break;

			case 'radio-image':
				$wp_customize->add_control(
					new dima_custom_control_radio_image(
						$wp_customize,
						$control[0],
						array(
							'type'        => $control[1],
							'label'       => $control[2],
							'section'     => $control[3],
							'choices'     => $control[4],
							'description' => $control[5],
							'priority'    => $priority
						)
					) );
				break;

			case 'font_style':
				$wp_customize->add_control(
					new dima_custom_control_font_style(
						$wp_customize,
						$control[0],
						array(
							'type'        => $control[1],
							'label'       => $control[2],
							'section'     => $control[3],
							'choices'     => $control[4],
							'description' => $control[5],
							'priority'    => $priority
						)
					) );
				break;
			case 'multi_check':
				$wp_customize->add_control(
					new dima_custom_control_multi_check(
						$wp_customize,
						$control[0],
						array(
							'type'        => $control[1],
							'label'       => $control[2],
							'section'     => $control[3],
							'choices'     => $control[4],
							'description' => $control[5],
							'priority'    => $priority
						)
					)
				);
				break;
			case 'multiple-select':
				$wp_customize->add_control(
					new dima_custom_control_multiple_select(
						$wp_customize,
						$control[0],
						array(
							'type'        => $control[1],
							'label'       => $control[2],
							'section'     => $control[3],
							'choices'     => $control[4],
							'description' => $control[5],
							'priority'    => $priority
						)
					)
				);
				break;
			case 'textarea':
				$wp_customize->add_control(
					new dima_custom_control_textarea(
						$wp_customize,
						$control[0],
						array(
							'label'       => $control[2],
							'section'     => $control[3],
							'settings'    => $control[0],
							'description' => $control[4],
							'priority'    => $priority
						)
					)
				);
				break;
			case 'slider':
				$wp_customize->add_control(
					new dima_custom_control_slider(
						$wp_customize,
						$control[0],
						array(
							'label'       => $control[2],
							'section'     => $control[4],
							'settings'    => $control[0],
							'choices'     => $control[3],
							'description' => $control[5],
							'priority'    => $priority
						)
					)
				);
				break;
			case 'color':
				$wp_customize->add_control(
					new WP_Customize_Color_Control(
						$wp_customize,
						$control[0],
						array(
							'label'       => $control[2],
							'settings'    => $control[0],
							'section'     => $control[3],
							'description' => $control[4],
							'priority'    => $priority
						)
					)
				);
				break;
			case 'color-alpha':
				$wp_customize->add_control(
					new dima_custom_control_color_alpha(
						$wp_customize,
						$control[0],
						array(
							'label'       => $control[2],
							'settings'    => $control[0],
							'section'     => $control[3],
							'description' => $control[4],
							'priority'    => $priority
						)
					)
				);
				break;
			case 'image':
				$wp_customize->add_control(
					new WP_Customize_Image_Control(
						$wp_customize,
						$control[0],
						array(
							'label'       => $control[2],
							'section'     => $control[3],
							'settings'    => $control[0],
							'description' => $control[4],
							'priority'    => $priority
						)
					)
				);
				break;
			case 'dropdown-pages':
				$wp_customize->add_control(
					$control[0],
					array(
						'type'           => $control[1],
						'label'          => $control[2],
						'section'        => $control[3],
						'description'    => $control[4],
						'allow_addition' => $control[5],
						'priority'       => $priority
					)
				);
				break;
			default:
				$wp_customize->add_control(
					$control[0],
					array(
						'type'        => $control[1],
						'label'       => $control[2],
						'section'     => $control[3],
						'description' => $control[4],
						'priority'    => $priority,
					)
				);
				break;
		}

		$priority ++;
	}

	/**
	 * Selective Refresh for Custom Options
	 */
	//Logo
	$wp_customize->selective_refresh->add_partial( 'dima_header_logo', array(
		'selector'        => '.site-title a',
		'render_callback' => 'noor_customize_partial_blogname',
	) );
	//Menu
	$wp_customize->selective_refresh->add_partial( 'dima_header_search_enable', array(
		'selector'        => '.dima-navbar-wrap .dima-nav',
		'render_callback' => 'noor_customize_partial_blogname',
	) );
	//sidebar
	$wp_customize->selective_refresh->add_partial( 'dima_layout_content', array(
		'selector'        => '.dima-container',
		'render_callback' => 'noor_customize_partial_blogname',
	) );
	$wp_customize->selective_refresh->add_partial( 'dima_pagination_bg_color', array(
		'selector'        => '.dima_post_pagination',
		'render_callback' => 'noor_customize_partial_blogname',
	) );
	//copyright
	$wp_customize->selective_refresh->add_partial( 'dima_footer_content_text', array(
		'selector'        => '.dima-footer .copyright ',
		'render_callback' => 'okab_customize_partial_blogname',
	) );
	//Footer menu
	$wp_customize->selective_refresh->add_partial( 'dima_footer_menu_display', array(
		'selector'        => '.dima-footer .dima-menu ',
		'render_callback' => 'okab_customize_partial_blogname',
	) );

}

add_action( 'customize_register', 'dima_register_theme_customizer_options' );