<?php

/**
 * Sets up the options for Customizer.
 *
 * @package Dima Framework
 * @subpackage Admin customizer
 * @version   1.0.0
 * @since     1.0.0
 * @author    PixelDima <info@pixeldima.com>
 */
/************************************************************************
 * [i]: $DIMA[][] is a table 2D
 *      Content 4 type of variables
 *              1: Settings
 *              2: Sections
 *              3: Controls
 *              3: Panel
 *
 * $DIMA['sections'][] = array($sections_id,title,priority,$id_panel);
 * $DIMA['settings'][] = array($settings_id,default,transport);
 * $DIMA['controls'][] = array($controls_id,type,label,$sections_id,choices);
 * $DIMA['panel'][]    = array($id_panel, titel, description, priority);
 *
 ************************************************************************/

/**
 * Choices
 */
$list_all_font_weights         = dima_get_font_weights();
$list_all_font_weight_selected = dima_get_font_weight_selected();
$list_all_google_font_name     = dima_get_googlefonts_name();
$list_font_weights_and_name    = dima_get_googlefonts();
$list_font_subsets             = dima_get_font_subsets();

$Choices_demo = array(
	'noor_main'          => esc_html__( 'Main Demo', 'noor' ),
	'business_classic'   => esc_html__( 'Business Classic', 'noor' ),
	'business_creative'  => esc_html__( 'Business Creative', 'noor' ),
	'business_modern'    => esc_html__( 'Business Modern', 'noor' ),
	'construction'       => esc_html__( 'Construction', 'noor' ),
	'creative_agency'    => esc_html__( 'Creative Agency', 'noor' ),
	'elegant_restaurant' => esc_html__( 'Elegant Restaurant', 'noor' ),
	'help_center'        => esc_html__( 'Help Center', 'noor' ),
	'medical'            => esc_html__( 'Medical', 'noor' ),
	'modern_restaurant'  => esc_html__( 'Modern Restaurant', 'noor' ),
	'seo'                => esc_html__( 'SEO', 'noor' ),
	'single_product'     => esc_html__( 'Single Product', 'noor' ),
);

$Choices_demo_setting = array(
	'noor_main'       => esc_html__( 'Main Demo', 'noor' ),
	'creative-agency' => esc_html__( 'Creative Agency', 'noor' ),
);

$Choices_on_off = array(
	'1' => esc_html__( 'On', 'noor' ),
	'0' => esc_html__( 'Off', 'noor' )
);

$Choices_frame_size = array(
	'x10' => esc_html__( '10 px', 'noor' ),
	'x20' => esc_html__( '20 px', 'noor' ),
);

$Choices_comments_style = array(
	''              => esc_html__( 'Noor', 'noor' ),
	'matrial_style' => esc_html__( 'Material Design', 'noor' )
);

$Choices_burger = array(
	'full' => esc_html__( 'Full', 'noor' ),
	'end'  => esc_html__( 'Side', 'noor' )
);

$Choices_dark_light = array(
	'dark'  => esc_html__( 'Dark', 'noor' ),
	'light' => esc_html__( 'Light', 'noor' )
);

$Choices_shop_product_layout = array(
	'grid' => 'Grid',
	'list' => 'List'
);

$Choices_left_right_float = array(
	'start' => esc_html__( 'start', 'noor' ),
	'end'   => esc_html__( 'End', 'noor' )
);

$Choices_site_layouts = array(
	'framed'     => esc_html__( 'Framed', 'noor' ),
	'full-width' => esc_html__( 'Fullwidth', 'noor' )
);

$Choices_of_blog_styles = array(
	'standard'  => esc_html__( 'Standard', 'noor' ),
	'grid'      => esc_html__( 'Grid', 'noor' ),
	'masonry'   => esc_html__( 'Masonry', 'noor' ),
	'timeline'  => esc_html__( 'Time Line', 'noor' ),
	'minimal' => esc_html__( 'minimal Style', 'noor' ),
	'minimal_no_margin' => esc_html__( 'minimal no margin Style', 'noor' )
);

$Choices_of_section_layouts = array(
	'inherit'       => esc_html__( 'Inherit', 'noor' ),
	'mini'          => esc_html__( 'Mini', 'noor' ),
	'right-sidebar' => esc_html__( 'Right Sidebar', 'noor' ),
	'left-sidebar'  => esc_html__( 'Left Sidebar', 'noor' ),
	'no-sidebar'    => esc_html__( 'No Sidebar', 'noor' ),
);

$Choices_of_content_layouts = array(
	'no-sidebar'    => esc_html__( 'No Sidebar', 'noor' ),
	'right-sidebar' => esc_html__( 'Right Sidebar', 'noor' ),
	'left-sidebar'  => esc_html__( 'Left Sidebar', 'noor' ),
);

$Choices_of_content_shop_layouts = array(
	'inherit'       => esc_html__( 'Inherit', 'noor' ),
	'right-sidebar' => esc_html__( 'Right Sidebar', 'noor' ),
	'left-sidebar'  => esc_html__( 'Left Sidebar', 'noor' ),
	'no-sidebar'    => esc_html__( 'No Sidebar', 'noor' ),
);

$Choices_look = array(
	'light' => esc_html__( 'Light', 'noor' ),
	'dark'  => esc_html__( 'Dark', 'noor' )
);

$Choices_sizing_content_width = array(
	'min'  => '50',
	'max'  => '100',
	'step' => '1'
);

$Choices_max_sizing_content_width = array(
	'min'  => '600',
	'max'  => '1500',
	'step' => '10'
);

$Choices_boxed_margin = array(
	'min'  => '0',
	'max'  => '80',
	'step' => '5'
);

$Choices_navbar_hight = array(
	'min'  => '20',
	'max'  => '500',
	'step' => '5'
);

$Choices_details_style = array(
	'classic' => esc_html__( 'Classic', 'noor' ),
	'modern'  => esc_html__( 'Modern', 'noor' ),
);

$Choices_link_source = array(
	'page'       => esc_html__( 'Page', 'noor' ),
	'custom_url' => esc_html__( 'Custom url', 'noor' ),
);

$Choices_portfolio_details_layout = array(
	'top'    => esc_html__( 'Details on the top', 'noor' ),
	'bottom' => esc_html__( 'Details on the bottom', 'noor' ),
	'left'   => esc_html__( 'Details on the Left', 'noor' ),
	'right'  => esc_html__( 'Details on the Right', 'noor' ),
);

$Choices_masonry_columns = array(
	2 => esc_html__( 'Two', 'noor' ),
	3 => esc_html__( 'Three', 'noor' ),
	4 => esc_html__( 'Four', 'noor' )
);

$Choices_navbar_position = array(
	'fill-width'        => esc_html__( 'Full-Width', 'noor' ),
	'fixed-left'        => esc_html__( 'Fixed Left', 'noor' ),
	'fixed-right'       => esc_html__( 'Fixed Right', 'noor' ),
	'big-navegation'    => esc_html__( 'Big Navegation', 'noor' ),
	'logo-on-center'    => esc_html__( 'Logo on center', 'noor' ),
	'bottom-logo'       => esc_html__( 'Bottom Logo', 'noor' ),
	'fixed-left-small'  => esc_html__( 'Fixed Left Small', 'noor' ),
	'fixed-right-small' => esc_html__( 'Fixed Right Small', 'noor' ),
);

$Choices_navbar_animation = array(
	'static-top'       => esc_html__( 'Static Top', 'noor' ),
	'headroom'         => esc_html__( 'Headroom', 'noor' ),
	'fixed-top'        => esc_html__( 'Fixed Top', 'noor' ),
	'fixed-top-offset' => esc_html__( 'Fixed Top (Animated)', 'noor' ),
);

$Choices_logo_position = array(
	'logo-on-inline' => esc_html__( 'Logo On Inline', 'noor' ),
	'logo-on-top'    => esc_html__( 'Logo On Top', 'noor' ),
	'logo-on-center' => esc_html__( 'Logo On Center', 'noor' ),
);

$Choices_widget_areas = array(
	'one'   => esc_html__( 'One', 'noor' ),
	'two'   => esc_html__( 'Two', 'noor' ),
	'three' => esc_html__( 'Three', 'noor' ),
	'four'  => esc_html__( 'Four', 'noor' ),
);

$Choices_breadcrumb = array(
	"start"  => esc_html__( 'Start', 'noor' ),
	"center" => esc_html__( 'Center', 'noor' ),
);

$Choices_alignement = array(
	"text-start"  => esc_html__( 'Start', 'noor' ),
	"text-center" => esc_html__( 'Center', 'noor' ),
	"text-end"    => esc_html__( 'End', 'noor' ),
);

$Choices_font_style = array(
	'bold'      => esc_html__( 'Bold', 'noor' ),
	'italic'    => esc_html__( 'Italic', 'noor' ),
	'uppercase' => esc_html__( 'Uppercase', 'noor' ),
	'underline' => esc_html__( 'Underline', 'noor' ),
);

$Choices_element_hover = array(
	"none"   => esc_html__( 'None', 'noor' ),
	"main"   => esc_html__( 'Noor', 'noor' ),
	"inside" => esc_html__( 'Icon inside', 'noor' ),
);

$Choices_element_hover_related_projects = array(
	""             => esc_html__( 'default', 'noor' ),
	"op_vc_inside" => esc_html__( 'Inside', 'noor' ),
	"op_vc_none"   => esc_html__( 'None', 'noor' ),
);

$Choices_image_hover = array(
	"op_vc_none"     => esc_html__( 'none', 'noor' ),
	"op_vc_zoom-in"  => esc_html__( 'Zoom-in', 'noor' ),
	"op_vc_zoom-out" => esc_html__( 'Zoom-out', 'noor' ),
	"op_vc_gray"     => esc_html__( 'Black And White', 'noor' ),
	"op_vc_opacity"  => esc_html__( 'Opacity', 'noor' ),
);

$Choices_slide_animation = array(
	"slide" => esc_html__( 'Slide', 'noor' ),
	"fade"  => esc_html__( 'Fade', 'noor' ),
);

$Choices_related_projects_style = array(
	"slide" => esc_html__( 'Slide', 'noor' ),
	"grid"  => esc_html__( 'Grid', 'noor' ),
);

$categories_obj = get_categories( 'hide_empty=0' );
$categories     = array();
foreach ( $categories_obj as $pn_cat ) {
	$categories[ $pn_cat->cat_ID ] = $pn_cat->cat_name;
}
$Choices_category = $categories;

$sidebars_name = array( 'inherit' => esc_html__( 'Inherit', 'noor' ) );
$sidebars_id   = array( '' => '' );
foreach ( $GLOBALS['wp_registered_sidebars'] as $sidebar ) {
	$sidebars_name[ $sidebar['id'] ] = ucwords( $sidebar['name'] );
	$sidebars_id[ $sidebar['id'] ]   = ucwords( $sidebar['id'] );
}

$DIMA['panel'][] = array(
	'dima_customizer_panel_footer',
	esc_html__( 'Footer', 'noor' ),
	esc_html__( 'Footer', 'noor' ),
	12
);
/*[2]=================================================*/

/**
 * Out - Sections
 */
$i                  = 0;
$DIMA['sections'][] = array( 'dima_customizer_section_templates', esc_html__( 'Templates', 'noor' ), ++ $i, '' );
$DIMA['sections'][] = array( 'dima_customizer_section_layout', esc_html__( 'Site Layout', 'noor' ), ++ $i, '' );
$DIMA['sections'][] = array( 'dima_customizer_section_typography', esc_html__( 'Typography', 'noor' ), ++ $i, '' );
$DIMA['sections'][] = array( 'dima_customizer_section_navbar', esc_html__( 'Header', 'noor' ), ++ $i, '' );
$DIMA['sections'][] = array( 'dima_customizer_section_blog', esc_html__( 'Blog', 'noor' ), ++ $i, '' );
$DIMA['sections'][] = array( 'dima_customizer_section_sidebar', esc_html__( 'Sidebar', 'noor' ), ++ $i, '' );
$DIMA['sections'][] = array(
	'dima_customizer_section_footer',
	esc_html__( 'Footer', 'noor' ),
	++ $i,
	'dima_customizer_panel_footer'
);
$DIMA['sections'][] = array(
	'dima_customizer_section_footer_widget',
	esc_html__( 'Widgets', 'noor' ),
	++ $i,
	'dima_customizer_panel_footer'
);
 

    
