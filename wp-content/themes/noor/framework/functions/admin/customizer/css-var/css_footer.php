<?php
GLOBAL $dima_customizer_data;
if ( $var_top_footer_bg_color !== $dima_customizer_data["dima_footer_content_top_bg"] ) { ?>
    .footer-container{
    background : <?php echo esc_attr( $var_top_footer_bg_color ); ?>;
    }
<?php } ?>

<?php if ( $var_bottom_footer_text_color !== $dima_customizer_data["dima_footer_content_body_color"] ) { ?>
    .dima-footer,footer.dima-footer .copyright p{
    color: <?php echo esc_attr( $var_bottom_footer_text_color ); ?>;
    }
<?php } ?>

<?php if ( $var_bottom_footer_link_color !== $dima_customizer_data["dima_footer_content_link_color"] ) { ?>
    .dima-footer .copyright a,
    .dima-footer a{
    color: <?php echo esc_attr( $var_bottom_footer_link_color ); ?>;
    }
<?php } ?><?php if ( $var_footer_featured_border_color != $dima_customizer_data["dima_footer_featured_border_color"] ) { ?>
    .footer-container .top-footer .featured_area hr {
    border-color: <?php echo esc_attr( $var_footer_featured_border_color ); ?>;
    }
<?php } ?>
