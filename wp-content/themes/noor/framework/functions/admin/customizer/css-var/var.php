<?php
/**
 * Class and Function List:
 * Function list:
 * - navbar_padding()
 * - text_style()
 * Classes list:
 */

/**
 * Genirated CSS
 *
 * @package PHP-CSS
 * @subpackage var
 * @version   1.0.0
 * @since     1.0.0
 * @author    PixelDima <info@pixeldima.com>
 *
 */

/**
 * @param string $family
 * @param string $weight
 *
 * @return string
 */
function dima_get_font_family_and_weight( $family = '', $weight = '' ) {
	$is_custom_fonts = dima_helper::dima_get_option( 'dima_custom_font' ) == '1';
	$output          = '';
	if ( $is_custom_fonts ) {
		if ( ! empty( $family ) ) {
			$output = "font-family: \"{$family}\",\"Helvetica Neue\",Helvetica,sans-serif;";
		}
		if ( ! empty( $weight ) ) {
			$output .= "font-weight: {$weight};";
		}
	}

	return $output;
}

/**
 * @param        $styles
 * @param string $important
 *
 * @return string
 */
function dima_text_style( $styles, $important = '' ) {
	// Prepare variable
	$font_styles = "";

	if ( '' !== $styles && false !== $styles ) {
		// Convert string into array
		$styles_array = explode( '|', $styles );

		// If $important is in use, give it a space
		if ( $important && '' !== $important ) {
			$important = " " . $important;
		}

		// Use in_array to find values in strings. Otherwise, display default text

		// Font weight
		if ( in_array( 'bold', $styles_array ) ) {
			$font_styles .= "font-weight: bold{$important}; ";
		} else {
			$font_styles .= "font-weight: normal{$important}; ";
		}

		// Font style
		if ( in_array( 'italic', $styles_array ) ) {
			$font_styles .= "font-style: italic{$important}; ";
		} else {
			$font_styles .= "font-style: normal{$important}; ";
		}

		// Text-transform
		if ( in_array( 'uppercase', $styles_array ) ) {
			$font_styles .= "text-transform: uppercase{$important}; ";
		} else {
			$font_styles .= "text-transform: inherit{$important}; ";
		}

		// Text-decoration
		if ( in_array( 'underline', $styles_array ) ) {
			$font_styles .= "text-decoration: underline{$important}; ";
		} else {
			$font_styles .= "text-decoration: none{$important}; ";
		}
	}

	return esc_html( $font_styles );
}

$is_cm                   = dima_helper::dima_am_i_true( dima_helper::dima_get_meta( '_dima_meta_custom_content_width' ) );
$is_cm_sidebar           = dima_helper::dima_am_i_true( dima_helper::dima_get_meta( '_dima_meta_custom_sidebar_width' ) );
$var_container_max_width = $var_container_width = $var_dima_sidebar_width = '';

if ( ! $is_cm ) {
	$var_container_width     = dima_helper::dima_get_option( 'dima_content_width' );
	$var_container_max_width = dima_helper::dima_get_option( 'dima_content_max_width' );
} else {
	$var_container_width     = dima_helper::dima_get_meta( '_dima_meta_content_width' );
	$var_container_max_width = dima_helper::dima_get_meta( '_dima_meta_content_max_width' );
}
if ( ! $is_cm_sidebar ) {
	$var_dima_sidebar_width = (float) dima_helper::dima_get_option( 'dima_sidebar_width' );
} else {
	$var_dima_sidebar_width = (float) dima_helper::dima_get_meta( '_dima_meta_sidebar_width' );
}
if ( is_rtl() ) {
	$var_header_logo_width = dima_helper::dima_get_inherit_option( '_dima_meta_header_logo_width_rtl', 'dima_header_logo_width_rtl' );
} else {
	$var_header_logo_width = dima_helper::dima_get_inherit_option( '_dima_meta_header_logo_width', 'dima_header_logo_width' );
}
$var_body_text_color           = dima_helper::dima_get_option( 'dima_body_text_color' );
$var_body_link_color           = dima_helper::dima_get_option( 'dima_body_link_color' );
$var_body_link_color_hover     = dima_helper::dima_get_option( 'dima_body_link_color_hover' );
$var_framed_size               = dima_helper::dima_get_inherit_option( '_dima_meta_frame_size', 'dima_frame_size' );
$var_dima_secondary_main_color = dima_helper::dima_get_inherit_option( 'dima_meta_secondary_main_color', 'dima_secondary_main_color' );
$var_main_color                = dima_helper::dima_get_inherit_option( 'dima_meta_main_color', 'dima_main_color' );

/**/
$var_nav_font_size                    = dima_helper::dima_get_option( 'dima_navbar_text_size' );
$var_nav_font_Bg_color                = dima_helper::dima_get_option( 'dima_navbar_background_color' );
$var_nav_font_Bg_color_after          = dima_helper::dima_get_option( 'dima_navbar_background_color_after' );
$var_nav_logo_on_top_background_color = dima_helper::dima_get_option( 'dima_logo_on_top_background_color' );
$var_transparent_nav_font_Bg_color    = dima_helper::dima_get_option( 'dima_transparent_navbar_background_color' );
$var_transparent_nav_font_text_color  = dima_helper::dima_get_option( 'dima_transparent_navbar_text_color' );
$var_nav_font_color                   = dima_helper::dima_get_option( 'dima_navbar_text_color' );
$var_menu_txt_color                   = dima_helper::dima_get_option( 'dima_menu_hover_text_color' );
$var_nav_font_color_after             = dima_helper::dima_get_option( 'dima_navbar_text_color_after' );
$var_nav_font_color_hover             = dima_helper::dima_get_option( 'dima_navbar_text_hover_color' );
$var_nav_border_color                 = dima_helper::dima_get_option( 'dima_menu_border_color' );
$var_page_title_bg_color              = dima_helper::dima_get_inherit_option( '_dima_meta_page_title_bg_color', 'dima_page_title_bg_color' );
$var_search_bg_color                  = dima_helper::dima_get_inherit_option( '_dima_meta_search_bg', 'dima_header_burger_bg_color' );

//button
$var_nav_btn_bg_color       = dima_helper::dima_get_option( 'dima_header_navbar_button_bg_color' );
$var_nav_btn_bg_hover_color = dima_helper::dima_get_option( 'dima_header_navbar_button_bg_color_hover' );
$var_nav_btn_txt_color      = dima_helper::dima_get_option( 'dima_header_navbar_button_txt_color' );


//submenu
$var_submenu_bg_color         = dima_helper::dima_get_option( 'dima_submenu_bg_color' );
$var_submenu_text_color       = dima_helper::dima_get_option( 'dima_submenu_text_color' );
$var_submenu_text_hover_color = dima_helper::dima_get_option( 'dima_submenu_text_hover_color' );
//!submenu

$var_nav_font_uppercase     = dima_helper::dima_get_option( 'dima_navbar_text_style' );
$var_nav_font_weights       = dima_helper::dima_get_option( 'dima_navbar_weights_list' );
$var_nav_font_slected       = dima_helper::dima_get_option( 'dima_navbar_weight_selected' );
$var_nav_font_family        = dima_helper::dima_get_option( 'dima_navbar_font_list' );
$var_header_navbar_position = dima_get_header_positioning();

//BODY
$var_body_font_size            = dima_helper::dima_get_option( 'dima_body_text_size' );
$var_body_font_weights         = dima_helper::dima_get_option( 'dima_body_weights_list' );
$var_body_font_weight_selected = dima_helper::dima_get_option( 'dima_body_weight_selected' );
$var_body_font_family          = dima_helper::dima_get_option( 'dima_body_font_list' );

//BUTTON
$var_btn_font_size            = dima_helper::dima_get_option( 'dima_btn_text_size' );
$var_btn_font_weights         = dima_helper::dima_get_option( 'dima_btn_weights_list' );
$var_btn_font_weight_selected = dima_helper::dima_get_option( 'dima_btn_weight_selected' );
$var_btn_font_family          = dima_helper::dima_get_option( 'dima_btn_font_list' );

//LOGO
$var_logo_font_uppercase = dima_helper::dima_get_option( 'dima_logo_text_style' );
$var_logo_font_color     = dima_helper::dima_get_option( 'dima_logo_text_color' );
$var_logo_font_size      = dima_helper::dima_get_option( 'dima_logo_text_size' );
$var_logo_letter_spacing = dima_helper::dima_get_option( 'dima_logo_letter_spacing' );
$var_logo_font_weights   = dima_helper::dima_get_option( 'dima_logo_weights_list' );
$var_logo_font_selcted   = dima_helper::dima_get_option( 'dima_logo_weight_selected' );
$var_logo_font_family    = dima_helper::dima_get_option( 'dima_logo_font_list' );

//HEADER 1
$var_heading_font_uppercase = dima_helper::dima_get_option( 'dima_heading_text_style' );
$var_heading_font_color     = dima_helper::dima_get_option( 'dima_heading_text_color' );
$var_heading_letter_spacing = dima_helper::dima_get_option( 'dima_heading_letter_spacing' );
$var_heading_font_Weights   = dima_helper::dima_get_option( 'dima_heading_weights_list' );
$var_heading_font_selected  = dima_helper::dima_get_option( 'dima_heading_weight_selected' );
$var_heading_font_family    = dima_helper::dima_get_option( 'dima_heading_font_list' );
//HEADER 2
$var_heading_font_uppercase_2 = dima_helper::dima_get_option( 'dima_heading_text_style_2' );
$var_heading_font_family_2    = dima_helper::dima_get_option( 'dima_heading_font_list_2' );
$var_heading_font_Weights_2   = dima_helper::dima_get_option( 'dima_heading_weights_list_2' );
$var_heading_font_selected_2  = dima_helper::dima_get_option( 'dima_heading_weight_selected_2' );
$var_heading_letter_spacing_2 = dima_helper::dima_get_option( 'dima_heading_letter_spacing_2' );
$var_heading_font_color_2     = dima_helper::dima_get_option( 'dima_heading_text_color_2' );

//HEADER 3
$var_heading_font_uppercase_3 = dima_helper::dima_get_option( 'dima_heading_text_style_3' );
$var_heading_font_family_3    = dima_helper::dima_get_option( 'dima_heading_font_list_3' );
$var_heading_font_Weights_3   = dima_helper::dima_get_option( 'dima_heading_weights_list_3' );
$var_heading_font_selected_3  = dima_helper::dima_get_option( 'dima_heading_weight_selected_3' );
$var_heading_letter_spacing_3 = dima_helper::dima_get_option( 'dima_heading_letter_spacing_3' );
$var_heading_font_color_3     = dima_helper::dima_get_option( 'dima_heading_text_color_3' );

//HEADER 4
$var_heading_font_uppercase_4 = dima_helper::dima_get_option( 'dima_heading_text_style_4' );
$var_heading_font_family_4    = dima_helper::dima_get_option( 'dima_heading_font_list_4' );
$var_heading_font_Weights_4   = dima_helper::dima_get_option( 'dima_heading_weights_list_4' );
$var_heading_font_selected_4  = dima_helper::dima_get_option( 'dima_heading_weight_selected_4' );
$var_heading_letter_spacing_4 = dima_helper::dima_get_option( 'dima_heading_letter_spacing_4' );
$var_heading_font_color_4     = dima_helper::dima_get_option( 'dima_heading_text_color_4' );

//HEADER 5
$var_heading_font_uppercase_5 = dima_helper::dima_get_option( 'dima_heading_text_style_5' );
$var_heading_font_family_5    = dima_helper::dima_get_option( 'dima_heading_font_list_5' );
$var_heading_font_Weights_5   = dima_helper::dima_get_option( 'dima_heading_weights_list_5' );
$var_heading_font_selected_5  = dima_helper::dima_get_option( 'dima_heading_weight_selected_5' );
$var_heading_letter_spacing_5 = dima_helper::dima_get_option( 'dima_heading_letter_spacing_5' );
$var_heading_font_color_5     = dima_helper::dima_get_option( 'dima_heading_text_color_5' );

//HEADER 6
$var_heading_font_uppercase_6 = dima_helper::dima_get_option( 'dima_heading_text_style_6' );
$var_heading_font_family_6    = dima_helper::dima_get_option( 'dima_heading_font_list_6' );
$var_heading_font_Weights_6   = dima_helper::dima_get_option( 'dima_heading_weights_list_6' );
$var_heading_font_selected_6  = dima_helper::dima_get_option( 'dima_heading_weight_selected_6' );
$var_heading_letter_spacing_6 = dima_helper::dima_get_option( 'dima_heading_letter_spacing_6' );
$var_heading_font_color_6     = dima_helper::dima_get_option( 'dima_heading_text_color_6' );

//footer
$var_top_footer_bg_color          = dima_helper::dima_get_inherit_option( '_dima_meta_footer_content_top_bg', 'dima_footer_content_top_bg' );
$var_bottom_footer_text_color     = dima_helper::dima_get_option( 'dima_footer_content_body_color' );
$var_bottom_footer_link_color     = dima_helper::dima_get_option( 'dima_footer_content_link_color' );
$var_footer_featured_border_color = dima_helper::dima_get_inherit_option( '_dima_meta_footer_featured_border_color', 'dima_footer_featured_border_color' );
//WIDGET

$var_widget_header_text_size = dima_helper::dima_get_option( 'dima_footer_widget_header_size' );
$var_widget_header_uppercase = dima_helper::dima_get_option( 'dima_footer_widget_header_uppercase' );
$var_widget_body_text_size   = dima_helper::dima_get_option( 'dima_footer_widget_body_size' );
$var_widget_body_uppercase   = dima_helper::dima_get_option( 'dima_footer_widget_body_uppercase' );
$var_widget_header_color     = dima_helper::dima_get_inherit_option( '_dima_meta_footer_widget_header_color', 'dima_footer_widget_header_color' );;
$var_widget_body_color       = dima_helper::dima_get_inherit_option( '_dima_meta_footer_widget_body_color', 'dima_footer_widget_body_color' );
$var_widget_link_color       = dima_helper::dima_get_inherit_option( '_dima_meta_footer_widget_link_color', 'dima_footer_widget_link_color' );
$var_widget_link_hover_color = dima_helper::dima_get_inherit_option( '_dima_meta_footer_widget_link_hover_color', 'dima_footer_widget_link_hover_color' );
$var_widget_border_color     = dima_helper::dima_get_inherit_option( '_dima_meta_footer_widget_border_color', 'dima_footer_widget_border_color' );

//SIDEBAR WIDGET
$var_widget_sidebar_header_text_size = dima_helper::dima_get_option( 'dima_sidebar_widget_header_size' );
$var_widget_sidebar_header_uppercase = dima_helper::dima_get_option( 'dima_sidebar_widget_header_uppercase' );
$var_widget_sidebar_body_text_size   = dima_helper::dima_get_option( 'dima_sidebar_widget_body_size' );
$var_widget_sidebar_body_uppercase   = dima_helper::dima_get_option( 'dima_sidebar_widget_body_uppercase' );

//loading
$var_loading_logo   = dima_helper::dima_get_option( 'dima_loading_logo' );
$var_loading_bg     = dima_helper::dima_get_option( 'dima_loading_bg_color' );
$var_loading_border = dima_helper::dima_get_option( 'dima_loading_border_color' );
//blog
$var_pagination_bg_color = dima_helper::dima_get_inherit_option( '_dima_meta_pagination_bg_color', 'dima_pagination_bg_color' );

$var_website_bg_color        = dima_helper::dima_get_inherit_option( '_dima_meta_body_background_color', 'dima_body_background_color' );
$var_website_bg_img          = dima_helper::dima_get_inherit_option( '_dima_meta_body_background_image', 'dima_body_background_image' );
$var_website_back_repeat     = dima_helper::dima_get_inherit_option( '_dima_meta_body_background_image_repeat', 'dima_body_background_image_repeat' );
$var_website_back_position   = dima_helper::dima_get_inherit_option( '_dima_meta_body_background_image_position', 'dima_body_background_image_position' );
$var_website_back_size       = dima_helper::dima_get_inherit_option( '_dima_meta_body_background_image_size', 'dima_body_background_image_size' );
$var_website_back_attachment = dima_helper::dima_get_inherit_option( '_dima_meta_body_background_image_attachment', 'dima_body_background_image_attachment' );