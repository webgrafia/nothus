<?php
GLOBAL $dima_customizer_data;

if ( $var_heading_font_uppercase !== $dima_customizer_data["dima_heading_text_style"] ) { ?>
    .title_container .header-title,
    .boxed-blog.blog-list .post header .entry-title,
    .boxed-blog.blog-list article header .entry-title,
    h1, h2, h3, h4, h5, h6{
	<?php echo esc_attr( dima_text_style( $var_heading_font_uppercase ) ); ?>;
    }
<?php } ?>

<?php if ( $var_heading_letter_spacing !== $dima_customizer_data["dima_heading_letter_spacing"] ) { ?>
    h1{
    letter-spacing:<?php echo esc_attr( $var_heading_letter_spacing ); ?>;
    }
<?php } ?>

<?php if ( $var_heading_font_color !== $dima_customizer_data["dima_heading_text_color"] ) { ?>
    h1, h1 a, a h1, h1 a:hover, a:hover h1
    {
    color:<?php echo esc_attr( $var_heading_font_color ) ?>;
    }
    .di_header.dima-button.fill {
    background:<?php echo esc_attr( $var_heading_font_color ) ?>;
    }

    .di_header.dima-button.fill:hover {
    background: <?php echo DIMA_Style::dima_adjustBrightness( $var_heading_font_color, 30 ) ?>
    color:#FFFFFF;
    }
<?php } ?>

<?php if ( $var_heading_font_selected !== $dima_customizer_data["dima_heading_weight_selected"] ) { ?>
    .title_container .header-title,
    h1{
    font-weight: <?php echo esc_attr( $var_heading_font_selected ); ?>;
    }
<?php } ?>

<?php if ( $var_heading_font_family !== $dima_customizer_data["dima_heading_font_list"] ) { ?>
    h1, .posted_in label{
	<?php
	echo dima_get_font_family_and_weight( $var_heading_font_family, $var_heading_font_selected ); ?>;
    }
<?php } ?>


<?php
if ( $var_heading_font_uppercase_2 !== $dima_customizer_data["dima_heading_text_style_2"] ) { ?>
    h2{
	<?php echo esc_attr( dima_text_style( $var_heading_font_uppercase_2 ) ); ?>;
    }
<?php } ?>

<?php if ( $var_heading_letter_spacing_2 !== $dima_customizer_data["dima_heading_letter_spacing_2"] ) { ?>
    h2{
    letter-spacing:<?php echo esc_attr( $var_heading_letter_spacing_2 ); ?>;
    }
<?php } ?>

<?php if ( $var_heading_font_color_2 !== $dima_customizer_data["dima_heading_text_color_2"] ) { ?>
    h2,h2 a, a h2, h2 a:hover,a:hover h2
    {
    color:<?php echo esc_attr( $var_heading_font_color_2 ) ?>;
    }
<?php } elseif ( $var_heading_font_color_2 == $dima_customizer_data["dima_heading_text_color_2"] && $var_heading_font_color == $dima_customizer_data["dima_heading_text_color"] ) { ?>
    h2,h2 a, a h2, h2 a:hover,a:hover h2
    {
    color:<?php echo esc_attr( $var_heading_font_color ) ?>;
    }
<?php } ?>

<?php if ( $var_heading_font_selected_2 !== $dima_customizer_data["dima_heading_weight_selected_2"] ) { ?>
    h2 {
    font-weight: <?php echo esc_attr( $var_heading_font_selected_2 ); ?>;
    }
<?php } ?>

<?php if ( $var_heading_font_family_2 !== $dima_customizer_data["dima_heading_font_list_2"] ) { ?>
    h2 {
	<?php echo dima_get_font_family_and_weight( $var_heading_font_family_2, $var_heading_font_selected_2 ); ?>;
    }
<?php } ?>


<?php
if ( $var_heading_font_uppercase_3 !== $dima_customizer_data["dima_heading_text_style_3"] ) { ?>
    h3{
	<?php echo esc_attr( dima_text_style( $var_heading_font_uppercase_3 ) ); ?>;
    }
<?php } ?>

<?php if ( $var_heading_letter_spacing_3 !== $dima_customizer_data["dima_heading_letter_spacing_3"] ) { ?>
    h3{
    letter-spacing:<?php echo esc_attr( $var_heading_letter_spacing_3 ); ?>;
    }
<?php } ?>

<?php if ( $var_heading_font_color_3 !== $dima_customizer_data["dima_heading_text_color_3"] ) { ?>
    h3,h3 a, a h3, h3 a:hover,a:hover h3
    {
    color:<?php echo esc_attr( $var_heading_font_color_3 ) ?>;
    }
<?php } elseif ( $var_heading_font_color_3 == $dima_customizer_data["dima_heading_text_color_3"] && $var_heading_font_color == $dima_customizer_data["dima_heading_text_color"] ) { ?>
    h3,h3 a, a h3 h3 a:hover,a:hover h3
    {
    color:<?php echo esc_attr( $var_heading_font_color ) ?>;
    }
<?php } ?>

<?php if ( $var_heading_font_selected_3 !== $dima_customizer_data["dima_heading_weight_selected_3"] ) { ?>
    h3 {
    font-weight: <?php echo esc_attr( $var_heading_font_selected_3 ); ?>;
    }
<?php } ?>

<?php if ( $var_heading_font_family_3 !== $dima_customizer_data["dima_heading_font_list_3"] ) { ?>
    h3 {
	<?php echo dima_get_font_family_and_weight( $var_heading_font_family_3, $var_heading_font_selected_3 ); ?>;
    }
<?php } ?>


<?php
if ( $var_heading_font_uppercase_4 !== $dima_customizer_data["dima_heading_text_style_4"] ) { ?>
    h4{
	<?php echo esc_attr( dima_text_style( $var_heading_font_uppercase_4 ) ); ?>;
    }
<?php } ?>

<?php if ( $var_heading_letter_spacing_4 !== $dima_customizer_data["dima_heading_letter_spacing_4"] ) { ?>
    h4{
    letter-spacing:<?php echo esc_attr( $var_heading_letter_spacing_4 ); ?>;
    }
<?php } ?>

<?php if ( $var_heading_font_color_4 !== $dima_customizer_data["dima_heading_text_color_4"] ) { ?>
    h4,h4 a, a h4, h4 a:hover,a:hover h4
    {
    color:<?php echo esc_attr( $var_heading_font_color_4 ) ?>;
    }
<?php } elseif ( $var_heading_font_color_4 == $dima_customizer_data["dima_heading_text_color_4"] && $var_heading_font_color == $dima_customizer_data["dima_heading_text_color"] ) { ?>
    h4,h4 a, a h4, h4 a:hover,a:hover h4
    {
    color:<?php echo esc_attr( $var_heading_font_color ) ?>;
    }
<?php } ?>

<?php if ( $var_heading_font_selected_4 !== $dima_customizer_data["dima_heading_weight_selected_4"] ) { ?>
    h4 {
    font-weight: <?php echo esc_attr( $var_heading_font_selected_4 ); ?>;
    }
<?php } ?>

<?php if ( $var_heading_font_family_4 !== $dima_customizer_data["dima_heading_font_list_4"] ) { ?>
    h4 {
	<?php echo dima_get_font_family_and_weight( $var_heading_font_family_4, $var_heading_font_selected_4 ); ?>;
    }
<?php } ?>


<?php
if ( $var_heading_font_uppercase_5 !== $dima_customizer_data["dima_heading_text_style_5"] ) { ?>
    h5{
	<?php echo esc_attr( dima_text_style( $var_heading_font_uppercase_5 ) ); ?>;
    }
<?php } ?>

<?php if ( $var_heading_letter_spacing_5 !== $dima_customizer_data["dima_heading_letter_spacing_5"] ) { ?>
    h5{
    letter-spacing:<?php echo esc_attr( $var_heading_letter_spacing_5 ); ?>;
    }
<?php } ?>

<?php if ( $var_heading_font_color_5 !== $dima_customizer_data["dima_heading_text_color_5"] ) { ?>
    h5,h5 a, a h5, h5 a:hover,a:hover h5
    {
    color:<?php echo esc_attr( $var_heading_font_color_5 ) ?>;
    }
<?php } elseif ( $var_heading_font_color_5 == $dima_customizer_data["dima_heading_text_color_2"] && $var_heading_font_color == $dima_customizer_data["dima_heading_text_color"] ) { ?>
    h5,h5 a, a h5, h5 a:hover,a:hover h5
    {
    color:<?php echo esc_attr( $var_heading_font_color ) ?>;
    }
<?php } ?>

<?php if ( $var_heading_font_selected_5 !== $dima_customizer_data["dima_heading_weight_selected_5"] ) { ?>
    h5 {
    font-weight: <?php echo esc_attr( $var_heading_font_selected_5 ); ?>;
    }
<?php } ?>

<?php if ( $var_heading_font_family_5 !== $dima_customizer_data["dima_heading_font_list_5"] ) { ?>
    h5 {
	<?php echo dima_get_font_family_and_weight( $var_heading_font_family_5, $var_heading_font_selected_5 ); ?>;
    }
<?php } ?>


<?php
if ( $var_heading_font_uppercase_6 !== $dima_customizer_data["dima_heading_text_style_6"] ) { ?>
    h6{
	<?php echo esc_attr( dima_text_style( $var_heading_font_uppercase_6 ) ); ?>;
    }
<?php } ?>

<?php if ( $var_heading_letter_spacing_6 !== $dima_customizer_data["dima_heading_letter_spacing_6"] ) { ?>
    h6{
    letter-spacing:<?php echo esc_attr( $var_heading_letter_spacing_6 ); ?>;
    }
<?php } ?>

<?php if ( $var_heading_font_color_6 !== $dima_customizer_data["dima_heading_text_color_6"] ) { ?>
    h6,h6 a, a h6, h6 a:hover,a:hover h6
    {
    color:<?php echo esc_attr( $var_heading_font_color_6 ) ?>;
    }
<?php } elseif ( $var_heading_font_color_6 == $dima_customizer_data["dima_heading_text_color_6"] && $var_heading_font_color == $dima_customizer_data["dima_heading_text_color"] ) { ?>
    h6,h6 a, a h6, h6 a:hover,a:hover h6
    {
    color:<?php echo esc_attr( $var_heading_font_color ) ?>;
    }
<?php } ?>

<?php if ( $var_heading_font_selected_6 !== $dima_customizer_data["dima_heading_weight_selected_6"] ) { ?>
    h6 {
    font-weight: <?php echo esc_attr( $var_heading_font_selected_6 ); ?>;
    }
<?php } ?>

<?php if ( $var_heading_font_family_6 !== $dima_customizer_data["dima_heading_font_list_6"] ) { ?>
    h6 {
	<?php echo dima_get_font_family_and_weight( $var_heading_font_family_6, $var_heading_font_selected_6 ); ?>;
    }
<?php } ?>


<?php if ( $var_btn_font_size !== $dima_customizer_data["dima_btn_text_size"] ) { ?>
    .widget .button, [type="submit"], .dima-button{
    font-size: <?php echo esc_attr( $var_btn_font_size ); ?>;
    }
<?php } ?>

<?php if ( $var_btn_font_family !== $dima_customizer_data["dima_btn_font_list"] ) { ?>
    .widget .button, [type="submit"], .dima-button {
	<?php echo dima_get_font_family_and_weight( $var_btn_font_family, $var_btn_font_weight_selected ); ?>;
    }
<?php } ?>

<?php if ( $var_body_text_color !== $dima_customizer_data["dima_body_text_color"] ) { ?>
    body, p {
    color: <?php echo esc_attr( $var_body_text_color ); ?>;
    }
<?php } ?>

<?php if ( $var_body_link_color !== $dima_customizer_data["dima_body_link_color"] ) { ?>
    div.widget.twitter-widget a, div.widget.widget_display_topics a,
    div.widget.widget_display_replies a, div.widget.widget_recent_entries a, a {
    color: <?php echo esc_attr( $var_body_link_color ); ?>;
    }
<?php } ?>

<?php if ( $var_body_link_color_hover !== $dima_customizer_data["dima_body_link_color_hover"] ) { ?>
    div.widget.widget_categories a:hover,
    div.widget.widget_meta a:hover,
    div.widget.widget_layered_nav a:hover,
    div.widget.widget_nav_menu a:hover,
    div.widget.widget_archive a:hover,
    div.widget.widget_pages a:hover,
    div.widget.twitter-widget a:hover,
    div.widget.widget_rss a:hover,
    div.widget.widget_product_categories a:hover,
    div.widget.widget_recent_comments a:hover,
    div.widget.bbp_widget_login a:hover,
    div.widget.widget_display_forums a:hover,
    div.widget.widget_display_topics a:hover,
    div.widget.widget_display_views a:hover,
    div.widget.widget_display_replies a:hover,
    .tagcloud a:hover, .tags a:hover,
    .dima-link p a:hover,
    .boxed-blog article .post-meta a:hover, .boxed-blog .post .post-meta a:hover,
    .isotope-item .work-item .project-info .porftfolio-cat li a:hover,
    .header-link-color:hover,
    .product-details .product-shop .posted_in a:hover, .product-details .product-shop .tagged_as a:hover, .product-details .product-shop .product_meta a:hover,
    a:hover{
    color:<?php echo esc_attr( $var_body_link_color_hover ) ?>;
    }

    div.widget.widget_categories ul li > a:first-child:hover:before, div.widget.widget_categories ol li > a:first-child:hover:before, div.widget.widget_meta ul li > a:first-child:hover:before, div.widget.widget_meta ol li > a:first-child:hover:before, div.widget.widget_layered_nav ul li > a:first-child:hover:before, div.widget.widget_layered_nav ol li > a:first-child:hover:before, div.widget.widget_nav_menu ul li > a:first-child:hover:before, div.widget.widget_nav_menu ol li > a:first-child:hover:before, div.widget.widget_archive ul li > a:first-child:hover:before, div.widget.widget_archive ol li > a:first-child:hover:before, div.widget.widget_pages ul li > a:first-child:hover:before, div.widget.widget_pages ol li > a:first-child:hover:before, div.widget.twitter-widget ul li > a:first-child:hover:before, div.widget.twitter-widget ol li > a:first-child:hover:before, div.widget.widget_rss ul li > a:first-child:hover:before, div.widget.widget_rss ol li > a:first-child:hover:before, div.widget.widget_product_categories ul li > a:first-child:hover:before, div.widget.widget_product_categories ol li > a:first-child:hover:before, div.widget.widget_recent_comments ul li > a:first-child:hover:before, div.widget.widget_recent_comments ol li > a:first-child:hover:before, div.widget.bbp_widget_login ul li > a:first-child:hover:before, div.widget.bbp_widget_login ol li > a:first-child:hover:before, div.widget.widget_display_forums ul li > a:first-child:hover:before, div.widget.widget_display_forums ol li > a:first-child:hover:before, div.widget.widget_display_topics ul li > a:first-child:hover:before, div.widget.widget_display_topics ol li > a:first-child:hover:before, div.widget.widget_display_views ul li > a:first-child:hover:before, div.widget.widget_display_views ol li > a:first-child:hover:before, div.widget.widget_display_replies ul li > a:first-child:hover:before, div.widget.widget_display_replies ol li > a:first-child:hover:before {
    background: url("data:image/svg+xml;charset=utf8,%3Csvg xmlns='http://www.w3.org/2000/svg' fill='<?php echo esc_attr( $var_body_link_color_hover ) ?>'  width='18' height='18' viewBox='0 0 48 48'%3E%3Cpath d='M17.17 32.92l9.17-9.17-9.17-9.17L20 11.75l12 12-12 12z'/%3E%3C/svg%3E") no-repeat;
    }

<?php } ?>

<?php if ( $var_body_font_weight_selected !== $dima_customizer_data["dima_body_weight_selected"] ) { ?>
    p {
    font-weight: <?php echo esc_attr( $var_body_font_weight_selected ); ?>;
    }
<?php } ?>

<?php if ( $var_body_font_family !== $dima_customizer_data["dima_body_font_list"] ) { ?>
    p, html{
	<?php
	echo dima_get_font_family_and_weight( $var_body_font_family, $var_body_font_weight_selected ); ?>;
    }
<?php } ?>
<?php if ( $var_body_font_size !== $dima_customizer_data["dima_body_text_size"] ) { ?>
    p, body{
    font-size: <?php echo esc_attr( $var_body_font_size ); ?>px;
    }
<?php } ?>


