<?php
GLOBAL $dima_customizer_data;
?>
<?php if ( $var_widget_sidebar_header_text_size !== $dima_customizer_data["dima_sidebar_widget_header_size"] ) { ?>
    .dima-sidebar .widget-title{
    font-size : <?php echo esc_attr( $var_widget_sidebar_header_text_size ); ?>;
    }
<?php } ?>
<?php if ( $var_widget_sidebar_header_uppercase !== $dima_customizer_data["dima_sidebar_widget_header_uppercase"] ) { ?>
    .dima-sidebar .widget-title{
	<?php echo esc_attr( dima_text_style( $var_widget_sidebar_header_uppercase ) ); ?>;
    }
<?php } ?>


<?php if ( $var_widget_sidebar_body_text_size !== $dima_customizer_data["dima_sidebar_widget_body_size"] ) { ?>
    .dima-sidebar .widget a,
    .dima-sidebar .widget {
    font-size : <?php echo esc_attr( $var_widget_sidebar_body_text_size ); ?>;
    }
<?php }

?><?php if ( $var_widget_sidebar_body_uppercase !== $dima_customizer_data["dima_sidebar_widget_body_uppercase"] ) { ?>
    .dima-sidebar .widget a,
    .dima-sidebar .widget {
	<?php echo esc_attr( dima_text_style( $var_widget_sidebar_body_uppercase ) ); ?>;
    }
<?php } ?>