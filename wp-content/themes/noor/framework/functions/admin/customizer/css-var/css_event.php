<?php
global $dima_customizer_data;

if ( $var_main_color !== $dima_customizer_data["dima_main_color"] ) {
	?>
    body .this-week-today .tribe-this-week-widget-header-date,
    .tribe-events-venue-widget .tribe-event-featured,
    body .tribe-mini-calendar .tribe-events-present, body .tribe-mini-calendar .tribe-mini-calendar-today,
    .tribe-events-read-more,
    #tribe-events .tribe-events-button,
    #tribe-events .tribe-events-button:hover,
    #tribe_events_filters_wrapper input[type=submit],
    .tribe-events-button, .tribe-events-button.tribe-active:hover,
    .tribe-events-button.tribe-inactive, .tribe-events-button:hover,
    .tribe-events-calendar td.tribe-events-present div[id*=tribe-events-daynum-],
    .tribe-events-calendar td.tribe-events-present div[id*=tribe-events-daynum-] > a,
    .tribe-events-grid .tribe-grid-header .tribe-week-today{
    background-color:<?php echo esc_attr( $var_main_color ) ?>;
    }

    .tribe-events-read-more:hover{
    background-color: <?php echo DIMA_Style::dima_adjustBrightness( $var_main_color, 20 ) ?>
    }
    .tribe-grid-allday .tribe-event-featured.tribe-events-week-allday-single,
    .tribe-grid-allday .tribe-event-featured.tribe-events-week-hourly-single,
    .tribe-grid-body .tribe-event-featured.tribe-events-week-allday-single,
    .tribe-grid-body .tribe-event-featured.tribe-events-week-hourly-single,
    .tribe-grid-allday .tribe-events-week-allday-single,
    .tribe-grid-body .tribe-events-week-hourly-single{
    background-color: rgba(<?php echo DIMA_Style::dima_get_rgb_color( $var_main_color ); ?>, 0.7 );
    border-color: <?php echo esc_attr( $var_main_color ) ?>;
    }

    body .this-week-today .tribe-this-week-event{
    border-color: <?php echo esc_attr( $var_main_color ) ?>;
    }
    .tribe-grid-allday .tribe-event-featured.tribe-events-week-allday-single:hover,
    .tribe-grid-allday .tribe-event-featured.tribe-events-week-hourly-single:hover,
    .tribe-grid-body .tribe-event-featured.tribe-events-week-allday-single:hover,
    .tribe-grid-body .tribe-event-featured.tribe-events-week-hourly-single:hover,
    .tribe-grid-allday .tribe-events-week-allday-single:hover,
    .tribe-grid-body .type-tribe_events .tribe-events-week-hourly-single:hover {
    background-color:<?php echo esc_attr( $var_main_color ) ?>;
    }
<?php }

if ( $var_dima_secondary_main_color !== $dima_customizer_data["dima_secondary_main_color"] ) { ?>

    body .tribe-this-week-widget-header-date,
    body .tribe-mini-calendar .tribe-mini-calendar-nav td,
    body .tribe-mini-calendar th,
    #tribe-events .tribe-events-button, .tribe-events-button, #tribe-bar-form .tribe-bar-submit input[type=submit],
    .tribe-events-day .tribe-events-day-time-slot h5,
    .tribe-grid-header .tribe-grid-content-wrap > div:nth-child(odd),
    .tribe-events-calendar thead th{
    background-color:<?php echo esc_attr( $var_dima_secondary_main_color ) ?>;
    }

    #tribe-events .tribe-events-button, .tribe-events-button, #tribe-bar-form .tribe-bar-submit input[type=submit],
    .tribe-grid-header,
    .tribe-events-calendar thead th:nth-child(odd){
    background-color: <?php echo DIMA_Style::dima_adjustBrightness( $var_dima_secondary_main_color, 20 ) ?>
    }

    .tribe-this-week-widget-wrapper .entry-title, .tribe-this-week-widget-wrapper .entry-title a,
    .tribe-this-week-widget-wrapper .tribe-events-page-title,
    .single-tribe_events #tribe-events-content .tribe-events-event-meta dt,
    .tribe-events-meta-group .tribe-events-single-section-title,
    body .tribe-this-week-widget-vertical .tribe-this-week-widget-day,
    body .this-week-past .tribe-this-week-widget-header-date,
    .tribe-events-adv-list-widget .tribe-event-featured .tribe-mini-calendar-event .tribe-events-title a,
    .tribe-mini-calendar-list-wrapper .tribe-event-featured .tribe-mini-calendar-event .tribe-events-title a,
    .tribe-events-adv-list-widget .tribe-event-featured .tribe-mini-calendar-event .tribe-events-title a:hover,
    .tribe-mini-calendar-list-wrapper .tribe-event-featured .tribe-mini-calendar-event .tribe-events-title a:hover,
    body .tribe-events-countdown-widget div.tribe-countdown-number,
    .single-tribe_events .tribe-events-schedule .tribe-events-cost,
    .tribe-updated.published.time-details,
    .tribe-event-schedule-details,
    .tribe-events-list-event-title a,
    .tribe-events-event-cost span,
    .tribe-events-calendar td.tribe-events-past div[id*=tribe-events-daynum-],
    .tribe-events-calendar td.tribe-events-past div[id*=tribe-events-daynum-] > a,
    .tribe-events-calendar div[id*=tribe-events-daynum-],
    .tribe-events-calendar div[id*=tribe-events-daynum-] a{
    color:<?php echo esc_attr( $var_dima_secondary_main_color ) ?>;
    }

    body .tribe-this-week-event,
    body .tribe-mini-calendar .tribe-mini-calendar-nav td,
    body .tribe-mini-calendar th,
    .tribe-events-event-cost span{
    border-color:<?php echo esc_attr( $var_dima_secondary_main_color ) ?>;
    }
<?php }

?>
