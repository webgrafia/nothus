<?php
GLOBAL $dima_customizer_data;
$is_transparent  = dima_helper::dima_am_i_true( dima_helper::dima_get_option( 'dima_header_navbar_transparent' ) );
$is_custom_fonts = dima_helper::dima_am_i_true( dima_helper::dima_get_option( 'dima_custom_font' ) );
$is_text_logo    = dima_helper::dima_get_inherit_option( "_dima_meta_header_logo", "dima_header_logo" ) == '';

if ( $is_text_logo ) { ?>
    .dima-navbar-wrap.desk-nav .dima-navbar a.dima-brand,
    .dima-navbar-wrap.mobile-nav .dima-navbar a.dima-brand
    {
    font-size:<?php echo esc_attr( $var_logo_font_size ); ?>;
    line-height:<?php echo intval( esc_attr( $var_logo_font_size ) ) + 2; ?>px;
    letter-spacing:<?php echo esc_attr( $var_logo_letter_spacing ); ?>;
	<?php echo esc_attr( dima_text_style( $var_logo_font_uppercase ) ); ?>;
    color:<?php echo esc_attr( $var_logo_font_color ) ?>;
    }
    .dima-navbar-wrap.desk-nav .dima-navbar .logo{
	<?php echo dima_get_font_family_and_weight( $var_logo_font_family, $var_logo_font_selcted ) ?>
    }

    .dima-navbar-wrap.mobile-nav .dima-navbar a.dima-brand  {
    line-height:90px;
    color:<?php echo esc_attr( $var_logo_font_color ) ?> ;
    }
<?php }
/**
 * 1-Menu - Background Color
 */
if ( $var_nav_font_Bg_color !== $dima_customizer_data["dima_navbar_background_color"] ) { ?>
    .navbar_is_dark.vertical-menu.vertical-menu .dima-navbar-wrap.desk-nav .dima-navbar.dima-navbar-vertical .dima-copyright,
    .navbar_is_dark.vertical-menu.vertical-menu .dima-navbar-wrap.desk-nav .dima-navbar.dima-navbar-vertical .logo,
    .vertical-menu .dima-navbar-wrap.desk-nav .dima-navbar.dima-navbar-vertical .dima-copyright,
    .vertical-menu .dima-navbar-wrap.desk-nav .dima-navbar.dima-navbar-vertical .logo,
    .navbar_is_dark .dima-navbar-wrap.desk-nav .dima-navbar,
    .navbar_is_dark.vertical-menu .dima-navbar-wrap.desk-nav .dima-navbar.dima-navbar-vertical,
    .vertical-menu .dima-navbar-wrap.desk-nav .dima-navbar.dima-navbar-vertical,
    .dima-navbar {
    background-color:<?php echo esc_attr( $var_nav_font_Bg_color ) ?> ;
    }
<?php }
/**
 * 2-Menu - Background Color - after fixed
 */
if ( $var_nav_font_Bg_color_after !== $dima_customizer_data["dima_navbar_background_color_after"] ) { ?>
    .navbar_is_dark .dima-navbar-wrap.desk-nav .dima-navbar-transparent.fixed-pinned.fixed-not-top,
    .navbar_is_dark .dima-navbar-wrap.desk-nav .dima-navbar-transparent.fix_nav,
    .dima-navbar.fix_nav {
    background-color:<?php echo esc_attr( $var_nav_font_Bg_color_after ) ?> ;
    }
<?php }
/**
 * 3-Menu - Text Color
 */
if ( $var_nav_font_color !== $dima_customizer_data["dima_navbar_text_color"] ) { ?>
    .navbar_is_dark .dima-navbar-wrap .dima-nav.icon-menu .badge-number,
    .dima-navbar-wrap .dima-nav.icon-menu .badge-number,
    .dima-navbar-wrap .dima-topbar .icon_text .dima-topbar-txt,
    .navbar_is_dark .dima-navbar-wrap .dima-topbar .icon_text .dima-topbar-txt,
    .navbar_is_dark .dima-navbar-wrap .dima-nav > li > a,
    .navbar_is_dark .dima-navbar-wrap .dima-nav.nav-primary > li > a,
    .dima-nav > li > a {
    color:<?php echo esc_attr( $var_nav_font_color ) ?> ;
    }
    .dima-navbar-wrap .dima-topbar .icon_text .dima-topbar-icon svg,
    .navbar_is_dark .dima-navbar-wrap .dima-topbar .icon_text .dima-topbar-icon svg{
    fill:<?php echo esc_attr( $var_nav_font_color_after ) ?> ;
    }
<?php }

if ( $var_menu_txt_color !== $dima_customizer_data["dima_menu_hover_text_color"] ) { ?>
    .navbar_is_dark .dima-navbar-wrap .dima-nav > li > a:hover ,
    .navbar_is_dark .dima-navbar-wrap .dima-nav.nav-primary > li > a:hover ,
    .dima-nav > li > a:hover {
    color:<?php echo esc_attr( $var_menu_txt_color ) ?> ;
    }
<?php }
/**
 * 4-Menu - Text Color - Hover
 */
if ( $var_nav_font_color_hover !== $dima_customizer_data['dima_navbar_text_hover_color'] ) { ?>
    .dima-navbar.dima-navbar-line .dima-nav.nav-primary > li > a:before,
    .navbar_is_dark .dima-navbar.dima-navbar-line .dima-nav.nav-primary > li > a:before {
    background:<?php echo esc_attr( $var_nav_font_color_hover ) ?> ;
    }
<?php }
/**
 * 5-Menu - Text Color - after fixed
 */
if ( $var_nav_font_color_after !== $dima_customizer_data['dima_navbar_text_color_after'] ) { ?>
    .navbar_is_dark .dima-navbar-wrap.fixed .dima-nav.icon-menu .badge-number,
    .dima-navbar-wrap.fixed .dima-nav.icon-menu .badge-number,
    .dima-navbar-wrap.fixed .dima-topbar .icon_text .dima-topbar-txt,
    .navbar_is_dark .dima-navbar-wrap.fixed .dima-topbar .icon_text .dima-topbar-txt,
    .navbar_is_dark .dima-navbar-wrap.fixed .dima-nav > li > a,
    .dima-navbar-wrap.fixed .dima-nav > li > a {
    color:<?php echo esc_attr( $var_nav_font_color_after ) ?> ;
    }
    .dima-navbar-wrap.fixed .dima-topbar .icon_text .dima-topbar-icon svg,
    .navbar_is_dark .dima-navbar-wrap.fixed .dima-topbar .icon_text .dima-topbar-icon svg{
    fill:<?php echo esc_attr( $var_nav_font_color_after ) ?> ;
    }

<?php }

/**
 * 6-Menu - Button bg Color
 */
if ( $var_nav_btn_bg_color !== $dima_customizer_data['dima_header_navbar_button_bg_color'] || $var_nav_btn_txt_color !== $dima_customizer_data['dima_header_navbar_button_txt_color'] ) { ?>
    .dima-nav-tag.dima-tag-btn-menu .dima-button {
    background-color:<?php echo esc_attr( $var_nav_btn_bg_color ) ?> ;
    color:<?php echo esc_attr( $var_nav_btn_txt_color ) ?> ;
    }
<?php }

/**
 * 7-Menu - Button bg Color - hover
 */
if ( $var_nav_btn_bg_hover_color !== $dima_customizer_data['dima_header_navbar_button_bg_color_hover'] ) { ?>
    .dima-nav-tag.dima-tag-btn-menu .dima-button:hover {
    background-color:<?php echo esc_attr( $var_nav_btn_bg_hover_color ) ?> ;
    }
<?php }

/**
 * 8-Sub Menu - bg Color
 */
if ( $var_submenu_bg_color !== $dima_customizer_data['dima_submenu_bg_color'] ) { ?>
    .navbar_is_dark .dima-lan > ul,
    .dima-lan > ul,
    .sub_menu_is_dark .dima-navbar-wrap.desk-nav .dima-navbar .dima-nav-tag .dima-nav .sub-menu,
    .dima-navbar-wrap.desk-nav .dima-navbar .dima-nav-tag .dima-nav .sub-menu {
    background-color:<?php echo esc_attr( $var_submenu_bg_color ) ?> ;
    }
<?php }
/**
 * 9-Sub Menu - text Color
 */
if ( $var_submenu_text_color !== $dima_customizer_data['dima_submenu_text_color'] ) { ?>
    .sub_menu_is_dark .dima-navbar-wrap .dima-nav .sub-menu a,
    .dima-nav .sub-menu a {
    color:<?php echo esc_attr( $var_submenu_text_color ) ?> ;
    }
<?php }
/**
 * 9-Sub Menu - hover text Color
 */
if ( $var_submenu_text_hover_color !== $dima_customizer_data['dima_submenu_text_hover_color'] ) { ?>
    .sub_menu_is_dark .dima-navbar-wrap .dima-nav .sub-menu a:hover,
    .dima-nav .sub-menu a:hover {
    color:<?php echo esc_attr( $var_submenu_text_hover_color ) ?> ;
    }
<?php }

/**
 * 10-Menu - border color
 */
if ( $var_nav_border_color !== $dima_customizer_data["dima_menu_border_color"] ) { ?>
    .navbar_is_dark .dima-navbar-wrap .big_nav,
    .navbar_is_dark .dima-navbar-wrap.desk-nav .dima-navbar,
    .navbar_is_dark .dima-navbar-wrap .dima-navbar,
    .mobile-nav.dima-navbar-wrap .mobile-nav-head,
    .framed .title_container .page-section-content,
    .dima-navbar-wrap.desk-nav .dima-navbar .dima-nav-tag .dima-nav .sub-menu,
    .dima-navbar{
    -webkit-box-shadow: inset 0 0 0 1px <?php echo esc_attr( $var_nav_border_color ) ?>;
    box-shadow: inset 0 0 0 1px <?php echo esc_attr( $var_nav_border_color ) ?>;
    }

    .navbar_is_dark .dima-navbar-wrap.desk-nav .start-burger-menu,
    .navbar_is_dark.vertical-menu .dima-navbar-wrap.desk-nav .dima-navbar.dima-navbar-vertical .dima-copyright .copyright,
    .navbar_is_dark.vertical-menu .dima-navbar-wrap.desk-nav .dima-navbar.dima-navbar-vertical .dima-nav-tag.dima-tag-icon-menu > ul:first-child > li:last-child,
    .navbar_is_dark.vertical-menu .dima-navbar-wrap.desk-nav .dima-navbar.dima-navbar-vertical .dima-nav-tag .dima-nav.icon-menu > li,
    .navbar_is_dark.vertical-menu .dima-navbar-wrap.desk-nav .dima-navbar.dima-navbar-vertical .logo,
    .navbar_is_dark .dima-navbar-wrap .dima-nav.icon-menu > li,
    .dima-navbar-wrap .dima-nav.icon-menu > li,
    .title_container hr,
    .mobile-nav.dima-navbar-wrap .dima-btn-nav,
    .dima-navbar-wrap.desk-nav .start-burger-menu,
    .vertical-menu .dima-navbar-wrap.desk-nav .dima-navbar.dima-navbar-vertical .dima-copyright .copyright,
    .vertical-menu .dima-navbar-wrap.desk-nav .dima-navbar.dima-navbar-vertical .dima-nav-tag .dima-nav.icon-menu > li,
    .vertical-menu .dima-navbar-wrap.desk-nav .dima-navbar.dima-navbar-vertical .dima-nav-tag.dima-tag-icon-menu > ul:nth-child(2) > li:last-child,
    .vertical-menu .dima-navbar-wrap.desk-nav .dima-navbar.dima-navbar-vertical .dima-nav-tag.dima-tag-icon-menu > ul:first-child > li:last-child,
    .vertical-menu .dima-navbar-wrap.desk-nav .dima-navbar.dima-navbar-vertical .logo{
    border-color: <?php echo esc_attr( $var_nav_border_color ) ?>;
    }

    a .link-backdrop{
    background: <?php echo esc_attr( $var_nav_border_color ) ?>;
    }

    .dima-navbar-wrap.desk-nav .dima-navbar .dima-nav-tag .dima-nav .dima-mega-menu > .sub-menu > li .line-hr{
    background: <?php echo esc_attr( $var_nav_border_color ) ?>;
    }
<?php }

/**
 * 11-Page Title - bg Color
 */
if ( $var_page_title_bg_color !== $dima_customizer_data["dima_page_title_bg_color"] ) { ?>
    .bre_is_dark .title_container,
    .title_container {
    background-color:<?php echo esc_attr( $var_page_title_bg_color ) ?> ;
    }
<?php }

/**
 * 12-Search - bg Color
 */
if ( $var_search_bg_color !== $dima_customizer_data["dima_header_burger_bg_color"] ) { ?>
    .burger-menu-side .burger-menu-content,
    .full-screen-menu {
    background-color:<?php echo esc_attr( $var_search_bg_color ) ?> ;
    }
<?php } ?>

<?php if ( $var_nav_font_uppercase !== $dima_customizer_data["dima_navbar_text_style"] ) { ?>
    .dima-navbar-wrap.desk-nav .dima-navbar nav .dima-nav > li > a,
    .dima-navbar-wrap.desk-nav .dima-navbar nav .dima-nav-end > li > a{
	<?php echo esc_attr( dima_text_style( $var_nav_font_uppercase ) ) ?>;
    }

<?php } ?>

<?php if ( $var_nav_font_size !== $dima_customizer_data["dima_navbar_text_size"] ) { ?>
    .dima-navbar-wrap.desk-nav .dima-navbar nav .dima-nav > li > a,
    .dima-navbar-wrap.desk-nav .dima-navbar nav .dima-nav-end > li > a{
	<?php /*Navbar menu and submenu*/ ?>
    font-size:<?php echo esc_attr( $var_nav_font_size ); ?>;
    }
<?php } ?>

<?php if ( $var_nav_font_family !== $dima_customizer_data["dima_navbar_font_list"] ) { ?>
    .dima-navbar-wrap{
	<?php echo dima_get_font_family_and_weight( $var_nav_font_family, $var_nav_font_slected ) ?>
    }
<?php } ?>

<?php if ( $var_header_logo_width !== $dima_customizer_data["dima_header_logo_width"] ) { ?>
    .logo-bottom .logo P img,
    .dima-navbar-wrap.desk-nav .dima-navbar .logo img{
    width: <?php echo esc_attr( $var_header_logo_width ) ?>px !important;
    }
<?php } ?>
