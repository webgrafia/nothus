<?php
GLOBAL $dima_customizer_data;

if ( $var_container_width != $dima_customizer_data["dima_content_width"] ) { ?>
    .container{
    width: <?php echo esc_attr( $var_container_width ) ?>%;
    }
<?php } ?>

<?php if ( $var_container_max_width != $dima_customizer_data["dima_content_max_width"] ) { ?>
    .container{
    max-width: <?php echo esc_attr( $var_container_max_width ) ?>%;
    }
<?php } ?>


<?php if ( $var_dima_sidebar_width > $dima_customizer_data["dima_sidebar_width"] ) {
    ?>

	<?php if ( ! is_rtl() ) { ?>
        body .left-content-sidebar-active .dima-sidebar{
        margin-right: <?php echo ( 60 * 100 ) / esc_attr( $var_container_max_width ) ?>%;
        }
        body .right-content-sidebar-active .dima-sidebar{
        margin-left: <?php echo ( 60 * 100 ) / esc_attr( $var_container_max_width ) ?>%;
        }
	<?php } else { ?>
        body .left-content-sidebar-active .dima-sidebar{
        margin-left: <?php echo ( 60 * 100 ) / esc_attr( $var_container_max_width ) ?>%;
        }
        body .right-content-sidebar-active .dima-sidebar{
        margin-right: <?php echo ( 60 * 100 ) / esc_attr( $var_container_max_width ) ?>%;
        }

	<?php } ?>

    body .dima-sidebar {
    width: <?php echo esc_attr( ( $var_dima_sidebar_width * 100 ) / $var_container_max_width ) ?>%;
    }
    body .dima-container {
    width:  <?php echo esc_attr( ( ( $var_container_max_width - $var_dima_sidebar_width - 60 ) * 100 ) / $var_container_max_width ) ?>%;
    }

<?php } ?>

<?php /* Loading*/ ?>
<?php
if ( $var_loading_bg !== $dima_customizer_data["dima_loading_bg_color"] ) {
	?>
    .loader-sticker{
    background-color:<?php echo esc_attr( $var_loading_bg ) ?> ;
    }
	<?php
}
if ( $var_loading_border !== $dima_customizer_data["dima_loading_border_color"] ) {
	?>
    .loader-line-right,
    .loader-animation-container{
    background-color:<?php echo esc_attr( $var_loading_border ) ?> ;
    }
	<?php
}
?>

<?php /* !Loading*/ ?>

<?php /* Main Color*/ ?>
<?php
if ( $var_main_color !== $dima_customizer_data["dima_main_color"] ) {
	?>
    .footer-container .top-footer .widget .button,
    .footer-container .top-footer [type="submit"],
    .footer-container .top-footer .dima-button,
    .dark-bg .tabs_style_2 ul.dima-tab-nav .tab.active a,
    .dark-bg .tabs_style_1 ul.dima-tab-nav .tab.active a,
    [type="submit"]:not(.stroke),a .line-hr:before,.mfp-content .mfp-close,.tagcloud a:hover,
    form.matrial-form input[type="search"]:focus ~ .bar::before,
    form.matrial-form input[type="text"]:focus ~ .bar::before,
    form.matrial-form input[type="email"]:focus ~ .bar::before,
    form.matrial-form input[type="date"]:focus ~ .bar::before,
    form.matrial-form input[type="password"]:focus ~ .bar::before,
    form.matrial-form input[type="url"]:focus ~ .bar::before,
    form.matrial-form input[type="tel"]:focus ~ .bar::before,
    form.matrial-form textarea:focus ~ .bar::before,
    .tabs_style_1 ul.dima-tab-nav .tab.active a,
    .noor-line:before,.dropcap.dropcap-3,
    .post-icon .icons-media li a, mark.dima-mark,
    .stroke:hover,.widget .button,.fill,
    .tabs_style_5 ul.dima-tab-nav .tab.active a,
    .tabs_style_3 ul.dima-tab-nav .tab.active a,
    .dima-timeline-list .dima-iconbox:hover .icon-box-header .box-square, .dima-timeline-list .dima-iconbox:hover .icon-box-header .box-circle,
    ul.dima-accordion .dima-accordion-group .dima-accordion-header .dima-accordion-toggle {
    background-color:<?php echo esc_attr( $var_main_color ) ?>;
    }

    .dima-iconbox:hover .box-square.icon-box-hover, .dima-iconbox:hover .box-circle.icon-box-hover,
    .icon-box-header:hover .box-square.icon-box-hover, .icon-box-header:hover .box-circle.icon-box-hover{
    background-color:<?php echo esc_attr( $var_main_color ) ?> !important;
    }

    .dima-iconbox:hover .box-square.icon-box-hover, .dima-iconbox:hover .box-circle.icon-box-hover,
    .dima-iconbox:hover .box-square.icon-box-border-hover, .dima-iconbox:hover .box-circle.icon-box-border-hover,
    .icon-box-header:hover .box-square.icon-box-hover, .icon-box-header:hover .box-circle.icon-box-hover {
    -webkit-box-shadow: 0 0 0 2px <?php echo esc_attr( $var_main_color ) ?> !important;
    box-shadow: 0 0 0 2px <?php echo esc_attr( $var_main_color ) ?> !important;
    }

    .widget .button:hover, [type="submit"]:hover, .fill:hover{
    background-color: <?php echo DIMA_Style::dima_adjustBrightness( $var_main_color, 30 ) ?>
    }

    .filters-box.filters ul li a:hover span,
    .filters-box.filters ul .current a,
    .filters-box.filters ul li a:hover,
    .filters-box.filters ul li.current span,
    ul.list-style li:before,
    .dropcap.dropcap-1{
    color:<?php echo esc_attr( $var_main_color ) ?> ;
    }

    .read-more-icon:hover svg,
    blockquote > span svg,
    .dima-blockquote > span {
    fill:<?php echo esc_attr( $var_main_color ) ?> ;
    }

    .tabs_style_5 ul.dima-tab-nav .tab.active,
    blockquote{
    border-color:<?php echo esc_attr( $var_main_color ) ?> ;
    }
    .tabs_style_5 ul.dima-tab-nav .tab.active a::after
    {
    border-top-color:<?php echo esc_attr( $var_main_color ) ?> ;
    }

    .stroke {
    -webkit-box-shadow: inset 0 0 0 2px <?php echo esc_attr( $var_main_color ) ?> ;
    box-shadow: inset 0 0 0 2px <?php echo esc_attr( $var_main_color ) ?> ;
    }

    .tabs_style_2 ul.dima-tab-nav .tab.active a {
    -webkit-box-shadow: inset 0 -2px 0 <?php echo esc_attr( $var_main_color ) ?> ;
    box-shadow: inset 0 -2px 0 <?php echo esc_attr( $var_main_color ) ?> ;
    }

    .tabs_style_2.tabs_float_end ul.dima-tab-nav .tab.active a {
    -webkit-box-shadow: inset 2px 0 0 <?php echo esc_attr( $var_main_color ) ?> ;
    box-shadow: inset 2px 0 0 <?php echo esc_attr( $var_main_color ) ?> ;
    }

    .tabs_style_2.tabs_float_start ul.dima-tab-nav .tab.active a {
    -webkit-box-shadow: inset -2px 0 0 <?php echo esc_attr( $var_main_color ) ?> ;
    box-shadow: inset -2px 0 0 <?php echo esc_attr( $var_main_color ) ?> ;
    }

    .tabs_style_4 ul.dima-tab-nav .tab.active a {
    -webkit-box-shadow: inset 0 -2px 0 <?php echo esc_attr( $var_main_color ) ?> ;
    box-shadow: inset 0 -2px 0 <?php echo esc_attr( $var_main_color ) ?> ;
    }

    .tabs_style_4.tabs_float_end ul.dima-tab-nav .tab.active a {
    -webkit-box-shadow: inset 2px 0 0 <?php echo esc_attr( $var_main_color ) ?> ;
    box-shadow: inset 2px 0 0 <?php echo esc_attr( $var_main_color ) ?> ;
    }

    .tabs_style_4.tabs_float_start ul.dima-tab-nav .tab.active a {
    -webkit-box-shadow: inset -2px 0 0 <?php echo esc_attr( $var_main_color ) ?>;
    box-shadow: inset -2px 0 0 <?php echo esc_attr( $var_main_color ) ?>;
    }

    .dima-timeline-list .dima-iconbox .icon-box-header .box-square, .dima-timeline-list .dima-iconbox .icon-box-header .box-circle {
    -webkit-box-shadow: 0 0 0 2px <?php echo esc_attr( $var_main_color ) ?>;
    box-shadow: 0 0 0 2px <?php echo esc_attr( $var_main_color ) ?>;
    }

    .dima-timeline-list.process-h .dima-iconbox .icon-box-header:after {
    background-image: url("data:image/svg+xml;charset=utf8,%3Csvg xmlns='http://www.w3.org/2000/svg' fill='<?php echo esc_attr( $var_main_color ) ?>' height='9' viewBox='0 0 24 24' width='12'%3E\a     %3Cpath d='M24 24H0V0h24v24z' fill='none'/%3E\a     %3Ccircle cx='12' cy='12' fill='<?php echo esc_attr( $var_main_color ) ?>' r='8'/%3E\a%3C/svg%3E");
    }

    .dima-timeline-list.process-v .dima-iconbox .icon-box-header:after {
    background-image: url("data:image/svg+xml;charset=utf8,%3Csvg xmlns='http://www.w3.org/2000/svg' fill='rgba(51, 51, 51, 0.47)' height='13' viewBox='0 0 24 24' width='8'%3E\a     %3Cpath d='M24 24H0V0h24v24z' fill='none'/%3E\a     %3Ccircle cx='12' cy='12' fill='<?php echo esc_attr( $var_main_color ) ?>' r='8'/%3E\a%3C/svg%3E");
    }

    ::selection {
    background-color: <?php echo esc_attr( $var_main_color ) ?>
    }

    ::-moz-selection {
    background: <?php echo esc_attr( $var_main_color ) ?>
    }
	<?php
}
?>
<?php /* !Main Color*/ ?>

<?php /* Secondary Color*/ ?>
<?php
if ( $var_dima_secondary_main_color !== $dima_customizer_data["dima_secondary_main_color"] ) { ?>
    .di_white.dima-button.fill:hover,
    .social-media.fill-icon li a,
    .dima-click-dropdown > a,
    .price,
    .stroke,
    .di_white.dima-button.fill,
    .social-media.dima-show li a,
    .detail-label,
    .header-color,
    .social-media li a,
    .tagcloud .tags-title, .tags .tags-title,
    .pagination ul li .next span, .pagination ul li .dima-next span,
    .dima-pagination ul li .next span, .dima-pagination ul li .dima-next span,
    .boxed-blog article .post-meta ul li.post-on time, .boxed-blog .post .post-meta ul li.post-on time,
    ul.dima-tab-nav .tab > a,
    .icon-box-header span,
    p > label, .field > label,
    blockquote a:hover,
    blockquote a,
    .dima-data-table-wrap table thead th, table thead th,
    .navbar_is_dark .dima-navbar-wrap .social-media.fill-icon li:hover a,
    .dark-bg .social-media.fill-icon li:hover a,
    ul.dima-accordion.dima-acc-clear .dima-accordion-group .dima-accordion-header .dima-accordion-toggle.collapsed,
    ul.dima-accordion .dima-accordion-group .dima-accordion-header .dima-accordion-toggle.collapsed {
    color: <?php echo esc_attr( $var_dima_secondary_main_color ) ?>
    }

    .social-media.dima_add_hover li:hover a,
    .ui-slider .ui-slider-range,
    .widget_shopping_cart a.button:first-child,
    .dima-data-table-wrap table.zibra_table thead th, table.zibra_table thead th {
    background: <?php echo esc_attr( $var_dima_secondary_main_color ) ?>
    }
    .widget_shopping_cart a.button:first-child:hover{
    background: <?php echo DIMA_Style::dima_adjustBrightness( $var_dima_secondary_main_color, 30 ) ?>
    }
    .ui-slider .ui-slider-handle {
    background: url("data:image/svg+xml;charset=utf8,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 12 12'%3E%3Cdefs%3E%3Cstyle%3E.cls-1{fill:%23fff;}.cls-2{fill:<?php echo esc_attr( $var_dima_secondary_main_color ) ?>}%3C/style%3E%3C/defs%3E%3Ccircle class='cls-1' cx='5' cy='5' r='4'/%3E%3Cpath class='cls-2' d='M10,7a3,3,0,1,1-3,3,3,3,0,0,1,3-3m0-2a5,5,0,1,0,5,5,5,5,0,0,0-5-5Z' transform='translate(-5 -5)'/%3E%3C/svg%3E") no-repeat;
    }
    .tagcloud svg, .tags svg,
    .detail-container .detail-value svg,
    .pagination ul li .prev svg,
    .pagination ul li .dima-previous svg,
    .pagination ul li .next svg,
    .pagination ul li .dima-next svg,
    .dima-pagination ul li .prev svg,
    .dima-pagination ul li .dima-previous svg,
    .dima-pagination ul li .next svg,
    .dima-pagination ul li .dima-next svg,
    .boxed-blog article .post-meta ul li svg,
    .dima-data-table-wrap table tr td svg, table tr td svg,
    .boxed-blog .post .post-meta ul li svg {
    fill: <?php echo esc_attr( $var_dima_secondary_main_color ) ?>
    }
    .pagination ul li > span.current,
    .pagination ul li > span.current-page,
    .pagination ul li a:not(.next):not(.dima-next):not(.dima-previous):not(.prev).current,
    .pagination ul li a:not(.next):not(.dima-next):not(.dima-previous):not(.prev).current-page,
    .dima-pagination ul li > span.current, .dima-pagination ul li > span.current-page,
    .dima-pagination ul li a:not(.next):not(.dima-next):not(.dima-previous):not(.prev).current,
    .dima-pagination ul li a:not(.next):not(.dima-next):not(.dima-previous):not(.prev).current-page,
    .pagination ul li > span:hover,
    .pagination ul li a:not(.next):not(.dima-next):not(.dima-previous):not(.prev):hover,
    .dima-pagination ul li > span:hover,
    .dima-pagination ul li a:not(.next):not(.dima-next):not(.dima-previous):not(.prev):hover,
    .slick-dots li button:before,
    .wishlist-icon .wishlist-button:hover,
    .dima-data-table-wrap table thead, table thead {
    border-color: <?php echo esc_attr( $var_dima_secondary_main_color ) ?>
    }
    .social-media.outline-icon li:hover a {
    -webkit-box-shadow: inset 0 0 0 2px <?php echo esc_attr( $var_dima_secondary_main_color ) ?>;
    box-shadow: inset 0 0 0 2px <?php echo esc_attr( $var_dima_secondary_main_color ) ?>;
    }
    .dima-click-dropdown > a:before{
    border-top-color: <?php echo esc_attr( $var_dima_secondary_main_color ) ?>
    }

    .knowledge_base_search form:before, .search-form:before, .woocommerce-product-search:before {
    background: url("data:image/svg+xml;charset=utf8,%3Csvg fill='<?php echo esc_attr( $var_dima_secondary_main_color ) ?>' height='24' viewBox='0 0 24 24' width='24' xmlns='http://www.w3.org/2000/svg'%3E\a     %3Cpath d='M15.5 14h-.79l-.28-.27C15.41 12.59 16 11.11 16 9.5 16 5.91 13.09 3 9.5 3S3 5.91 3 9.5 5.91 16 9.5 16c1.61 0 3.09-.59 4.23-1.57l.27.28v.79l5 4.99L20.49 19l-4.99-5zm-6 0C7.01 14 5 11.99 5 9.5S7.01 5 9.5 5 14 7.01 14 9.5 11.99 14 9.5 14z'/%3E\a     %3Cpath d='M0 0h24v24H0z' fill='none'/%3E\a%3C/svg%3E") no-repeat;
    }
    div.widget .widget-feedburner-counter form #form-row-wide:before {
    background: url("data:image/svg+xml;charset=utf8,%3Csvg fill='<?php echo esc_attr( $var_dima_secondary_main_color ) ?>' height='24' viewBox='0 0 24 24' width='24' xmlns='http://www.w3.org/2000/svg'%3E\a     %3Cpath d='M20 4H4c-1.1 0-1.99.9-1.99 2L2 18c0 1.1.9 2 2 2h16c1.1 0 2-.9 2-2V6c0-1.1-.9-2-2-2zm0 4l-8 5-8-5V6l8 5 8-5v2z'/%3E\a     %3Cpath d='M0 0h24v24H0z' fill='none'/%3E\a%3C/svg%3E") no-repeat;;
    }
	<?php if ( DIMA_WC_IS_ACTIVE ) { ?>
        .widget_price_filter .price_slider_amount .price_label,
        .widget_shopping_cart_content .total a .title,
        .widget_shopping_cart_content li a .title,
        .product_list_widget .total a .title,
        .product_list_widget li a .title,
        .widget_shopping_cart_content .total,
        .widget_shopping_cart_content li,
        .product_list_widget .total,
        .dima-data-table-wrap table tr, table tr,
        .woocommerce .order_details .amount, .woocommerce .order-total td,
        .product-details .product-shop .posted_in, .product-details .product-shop .tagged_as, .product-details .product-shop .product_meta,
        .product_list_widget li{
        color: <?php echo esc_attr( $var_dima_secondary_main_color ) ?>
        }
        #yith-wcwl-form p svg path,
        .wishlist-icon .wishlist-button svg{
        fill: <?php echo esc_attr( $var_dima_secondary_main_color ) ?>
        }
	<?php } ?>

<?php } ?>
<?php /* !Secondary Color*/ ?>

