<?php
GLOBAL $dima_customizer_data;
?>

<?php /* Main Color*/ ?>
<?php
if ( $var_main_color !== $dima_customizer_data["dima_main_color"] ) {
	?>
    #buddypress .item-list-tabs ul li a span {
    background:<?php echo esc_attr( $var_main_color ) ?>;
    }

    #buddypress .item-list-tabs ul li a:hover, #buddypress .item-list-tabs ul li.selected a, #buddypress .item-list-tabs ul li.current a {
    color:<?php echo esc_attr( $var_main_color ) ?>;
    }

	<?php
}
?>
<?php /* !Main Color*/ ?>

<?php /* Secondary Color*/ ?>
<?php
if ( $var_dima_secondary_main_color !== $dima_customizer_data["dima_secondary_main_color"] ) { ?>

    ul.dima-accordion.dima-acc-clear .dima-accordion-group .dima-accordion-header .dima-accordion-toggle.collapsed,
    ul.dima-accordion .dima-accordion-group .dima-accordion-header .dima-accordion-toggle.collapsed {
    color: <?php echo esc_attr( $var_dima_secondary_main_color ) ?>
    }


<?php } ?>
<?php /* !Secondary Color*/ ?>

