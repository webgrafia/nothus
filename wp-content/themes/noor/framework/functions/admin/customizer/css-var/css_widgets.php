<?php
GLOBAL $dima_customizer_data;
/* > Footer Widget*/
?>

<?php if ( $var_widget_header_text_size !== $dima_customizer_data["dima_footer_widget_header_size"] ) { ?>
    .top-footer .widget .widget-title{
    font-size: <?php echo esc_attr( $var_widget_header_text_size ); ?>;
    }
<?php } ?>

<?php if ( $var_widget_header_color !== $dima_customizer_data["dima_footer_widget_header_color"] ) { ?>
    .footer-container .top-footer .widget .widget-title{
    color: <?php echo esc_attr( $var_widget_header_color ); ?>;
    }
<?php } ?>

<?php

if ( $var_widget_header_uppercase !== $dima_customizer_data["dima_footer_widget_header_uppercase"] ) { ?>
    .top-footer .widget .widget-title{
	<?php echo esc_attr( dima_text_style( $var_widget_header_uppercase ) ); ?>;
    }
<?php } ?>


<?php if ( $var_widget_body_text_size !== $dima_customizer_data["dima_footer_widget_body_size"] ) { ?>
    .top-footer .widget,.top-footer .widget p{
    font-size: <?php echo esc_attr( $var_widget_body_text_size ); ?>;
    }
<?php } ?>

<?php if ( $var_widget_body_color !== $dima_customizer_data["dima_footer_widget_body_color"] ) { ?>
    .top-footer .widget,.top-footer .widget p{
    color: <?php echo esc_attr( $var_widget_body_color ); ?>;
	<?php echo esc_attr( $var_widget_body_uppercase ); ?>;
    }
<?php } ?>

<?php if ( $var_widget_body_uppercase !== $dima_customizer_data["dima_footer_widget_body_uppercase"] ) { ?>
    .top-footer .widget,.top-footer .widget p{
	<?php echo esc_attr( dima_text_style( $var_widget_body_uppercase ) ); ?>;
    }
<?php } ?>

<?php if ( $var_widget_link_color !== $dima_customizer_data["dima_footer_widget_link_color"] ) { ?>
    .footer-container .top-footer .widget:not(.social-icons-widget) a,
    .footer-container .top-footer .widget:not(.social-icons-widget) ul li a,
    .footer-container .top-footer .widget:not(.social-icons-widget) ol li a,
    .top-footer .widget_shopping_cart_content .amount,
    .top-footer .widget_shopping_cart_content .product-title,
    .top-footer .product_list_widget .amount,
    .top-footer .product_list_widget .product-title{
    color: <?php echo esc_attr( $var_widget_link_color ); ?>;
    }
<?php } ?>

<?php if ( $var_widget_link_hover_color !== $dima_customizer_data["dima_footer_widget_link_hover_color"] ) { ?>
    .footer-container .top-footer .widget:not(.social-icons-widget) a:hover,
    .footer-container .top-footer .widget:not(.social-icons-widget) ul li a:hover,
    .footer-container .top-footer .widget:not(.social-icons-widget) ol li a:hover,
    .top-footer .widget_shopping_cart_content .amount:hover,
    .top-footer .widget_shopping_cart_content .product-title:hover,
    .top-footer .product_list_widget .amount:hover,
    .top-footer .product_list_widget .product-title:hover{
    color: <?php echo esc_attr( $var_widget_link_hover_color ); ?>;
    }
<?php } ?>

<?php if ( $var_widget_border_color !== $dima_customizer_data["dima_footer_widget_border_color"] ) { ?>
    .footer-container .top-footer hr,
    .footer-container .top-footer .widget.widget_recent_entries li,
    .dima-footer,
    .footer-container.dark-bg hr{
    border-color: <?php echo esc_attr( $var_widget_border_color ); ?>;
    }
<?php } ?>
<?php /* !Footer Widget*/ ?>