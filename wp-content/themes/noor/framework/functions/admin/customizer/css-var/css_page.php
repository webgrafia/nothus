<?php
GLOBAL $dima_customizer_data;

if ( $var_pagination_bg_color !== $dima_customizer_data["dima_pagination_bg_color"] ) {
	?>
    .nav-reveal{
    background-color:<?php echo esc_attr( $var_pagination_bg_color ) ?>;
    }
	<?php
}

$background      = array(
	'background-color'      => ( $var_website_bg_color !== $dima_customizer_data["dima_body_background_color"] ) ? $var_website_bg_color : '',
	'background-image'      => ( $var_website_bg_img !== $dima_customizer_data["dima_body_background_image"] ) ? $var_website_bg_img : '',
	'background-repeat'     => ( $var_website_back_repeat !== $dima_customizer_data["dima_body_background_image_repeat"] ) ? $var_website_back_repeat : '',
	'background-position'   => ( $var_website_back_position !== $dima_customizer_data["dima_body_background_image_position"] ) ? $var_website_back_position : '',
	'background-size'       => ( $var_website_back_size !== $dima_customizer_data["dima_body_background_image_size"] ) ? $var_website_back_size : '',
	'background-attachment' => ( $var_website_back_attachment !== $dima_customizer_data["dima_body_background_image_attachment"] ) ? $var_website_back_attachment : '',
);
$back_color      = ( isset( $background['background-color'] ) && $background['background-color'] !== '' ) ? 'background-color: ' . esc_attr( $background['background-color'] ) . ';' : '';
$back_repeat     = ( isset( $background['background-repeat'] ) && $background['background-repeat'] !== '' ) ? 'background-repeat: ' . esc_attr( $background['background-repeat'] ) . ';' : '';
$back_position   = ( isset( $background['background-position'] ) && $background['background-position'] !== '' ) ? 'background-position: ' . esc_attr( $background['background-position'] ) . ';' : '';
$back_attachment = ( isset( $background['background-attachment'] ) && $background['background-attachment'] !== '' ) ? 'background-attachment: ' . esc_attr( $background['background-attachment'] ) . ';' : '';
$back_size       = ( isset( $background['background-size'] ) && $background['background-size'] !== '' ) ? 'background-size: ' . esc_attr( $background['background-size'] ) . ';' : '';
$back_url        = ( $var_website_bg_img !== '' ) ? 'background-image: url(' . esc_url( $var_website_bg_img ) . ');' : '';
if ( strpos( $back_repeat, "repeat" ) ) {
	$back_size = "";
}
$back_image_style = ( $back_url != '' || $back_color != '' || $back_repeat != '' || $back_position != '' || $back_attachment != '' || $back_size != '' ) ? '' . $back_url . $back_repeat . $back_position . $back_attachment . $back_size . $back_color . '' : '';
if ( $back_image_style != '' ) {
	?>
    .dima-main{
	<?php echo esc_attr( $back_image_style ); ?>
    }
	<?php
}