<?php
/**
 * Setup : Include all admin pages
 *
 * @package Dima Framework
 * @subpackage Admin Setup
 * @version   1.0.0
 * @since     1.0.0
 * @author    PixelDima <info@pixeldima.com>
 *
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


$addn_path = DIMA_TEMPLATE_PATH . '/framework/functions/admin/pixeldima-setup';

function dima_setup_get_link_home() {
	return admin_url( 'admin.php?page=pixeldima-setup-plugins' );
}

// Require Files
require_once( $addn_path . '/pixeldima-setup-settings.php' );
require_once( $addn_path . '/framework-home.php' );
require_once( $addn_path . '/framework-validation.php' );
require_once( $addn_path . '/framework-notices.php' );
require_once( $addn_path . '/framework-customizer-backup.php' );
require_once( $addn_path . '/framework-demo-content.php' );
require_once( $addn_path . '/framework-plugins.php' );
require_once( $addn_path . '/framework-system-status.php' );

// Setup Menu
function dima_setup_add_menu() {
	add_theme_page(
		'Noor: Home',
		'Noor',
		'manage_options',
		'pixel-dima-dashboard',
		'dima_setup_page_home'
	);

	add_theme_page(
		'Noor: Plugins',
		esc_html__( 'Noor Plugins', 'noor' ),
		'manage_options',
		'pixeldima-setup-plugins',
		'dima_setup_plugins'
	);
}

add_action( 'admin_menu', 'dima_setup_add_menu' );


// Activation Redirect
function dima_setup_redirect() {
	if ( isset( $_GET['activated'] ) ) {
		wp_redirect( dima_setup_get_link_home() );
	}
}

add_action( 'admin_init', 'dima_setup_redirect' );