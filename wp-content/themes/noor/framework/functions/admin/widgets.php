<?php
/**
 * Register Widget.
 *
 * @package Dima Framework
 * @subpackage Admin
 * @version   1.0.0
 * @since     1.0.0
 * @author    PixelDima <info@pixeldima.com>
 *
 */

/**
 * Register Widget Areas
 */
if ( ! function_exists( 'dima_widgets_init' ) ) :
	function dima_widgets_init() {

		/**
		 * Default Sidebar
		 */

		register_sidebar( array(
			'name'          => esc_html__( 'Main Sidebar', 'noor' ),
			'id'            => 'sidebar-main',
			'description'   => esc_html__( 'Appears on posts and pages that include the sidebar.', 'noor' ),
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h5 class="widget-title">',
			'after_title'   => '</h5><span class="dima-divider line-start line-hr small-line"></span>',
		) );

		register_sidebar( array(
			'name'          => esc_html__( 'Burger Area', 'noor' ),
			'id'            => 'burger-area',
			'description'   => esc_html__( 'Appears on menu Burger.', 'noor' ),
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h5 class="widget-title">',
			'after_title'   => '</h5><span class="dima-divider line-start line-hr small-line"></span>',
		) );

		if ( class_exists( 'Woocommerce' ) ) {
			register_sidebar( array(
				'name'          => esc_html__( 'Shop - For WooCommerce Pages', 'noor' ),
				'id'            => 'shop-widget-area',
				'description'   => esc_html__( 'This widget area uses in the WooCommerce pages .', 'noor' ),
				'before_widget' => '<div id="%1$s" class="widget %2$s">',
				'after_widget'  => '</div>',
				'before_title'  => '<h5 class="widget-title">',
				'after_title'   => '</h5><span class="dima-divider line-start line-hr small-line"></span>',
			) );
		}

		if ( class_exists( 'bbPress' ) ) {
			register_sidebar( array(
				'name'          => esc_html__( 'Forums - For Forums Pages', 'noor' ),
				'id'            => 'forums-widget-area',
				'description'   => esc_html__( 'This widget area uses in the forums pages .', 'noor' ),
				'before_widget' => '<div id="%1$s" class="widget %2$s">',
				'after_widget'  => '</div>',
				'before_title'  => '<h5 class="widget-title">',
				'after_title'   => '</h5><span class="dima-divider line-start line-hr small-line"></span>',
			) );
		}

		$i = 0;
		while ( $i < 4 ) : $i ++;
			register_sidebar( array(
				'name'          => esc_html__( 'First Footer Area #', 'noor' ) . $i,
				'id'            => 'footer-' . $i,
				'description'   => esc_html__( 'Widgetized footer area.', 'noor' ),
				'before_widget' => '<div id="%1$s" class="widget %2$s">',
				'after_widget'  => '</div>',
				'before_title'  => '<h5 class="widget-title">',
				'after_title'   => '</h5><hr>',
			) );
		endwhile;
		$i = 0;

		while ( $i < 4 ) : $i ++;
			register_sidebar( array(
				'name'          => esc_html__( 'Secound Footer Area #', 'noor' ) . $i,
				'id'            => 'footer-second-' . $i,
				'description'   => esc_html__( 'Widgetized footer area.', 'noor' ),
				'before_widget' => '<div id="%1$s" class="widget %2$s">',
				'after_widget'  => '</div>',
				'before_title'  => '<h5 class="widget-title">',
				'after_title'   => '</h5><hr>',
			) );
		endwhile;

	}

	add_action( 'widgets_init', 'dima_widgets_init' );
endif;