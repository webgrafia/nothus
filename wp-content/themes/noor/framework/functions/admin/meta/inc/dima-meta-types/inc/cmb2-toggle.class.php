<?php

class CMB2_dima_toggle {
	/**
	 * CMB2_Tabs constructor.
	 */

	/**
	 * If checkbox is checked
	 *
	 * @var mixed
	 */

	public function __construct() {
		add_action( 'cmb2_render_dima_toggle', array( $this, 'cmb2_render_dima_toggle' ), 10, 5 );
	}

	function cmb2_render_dima_toggle( $field, $escaped_value, $object_id, $object_type, $field_type_object ) {
		$animation_class = 'inactive';

		$args = array(
			'type'  => 'checkbox',
			'class' => 'cmb_option cmb_list',
			'value' => '1',
			'desc'  => ''
		);

		if ( $escaped_value == "1" ) {
			$args['checked'] = 'checked';
			$animation_class = '';
		} elseif ( $escaped_value == '' && $field->dufault() ) {
			$args['checked'] = 'checked';
			$animation_class = '';
		}
		echo sprintf( '<div class="dima-switch-button">%s <label for="%s" data-value="%s"><span class="button-animation %s"></span></label></div><p>%s</p>', $field_type_object->input( $args ), $field_type_object->_id(), $args['value'], $animation_class, $field_type_object->_desc() );

	}

}