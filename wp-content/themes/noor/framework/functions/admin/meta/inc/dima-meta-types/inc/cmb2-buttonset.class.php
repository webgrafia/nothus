<?php

class CMB2_dima_buttonset {
	/**
	 * If checkbox is checked
	 *
	 * @var mixed
	 */

	public function __construct() {
		add_action( 'cmb2_render_dima_buttonset', array( $this, 'cmb2_render_dima_buttonset' ), 10, 5 );
	}

	function cmb2_render_dima_buttonset( $field, $escaped_value, $object_id, $object_type, $field_type_object ) {
		$conditional_value = ( isset( $field->args['attributes']['data-conditional-value'] ) ? 'data-conditional-value="' . esc_attr( $field->args['attributes']['data-conditional-value'] ) . '"' : '' );
		$conditional_id    = ( isset( $field->args['attributes']['data-conditional-id'] ) ? ' data-conditional-id="' . esc_attr( $field->args['attributes']['data-conditional-id'] ) . '"' : '' );
		$default_value     = $field->args['default'];

		$buttonset = '<div class="cmb2-buttonset">';
		foreach ( $field->options() as $value => $item ) {
			$selected_input = ( $value === ( $escaped_value == '' ? $default_value : $escaped_value ) ) ? 'checked="checked"' : '';
			$selected_label = ( $value === ( $escaped_value == '' ? $default_value : $escaped_value ) ) ? ' selected' : '';
			$buttonset      .= '<input ' . $conditional_value . $conditional_id . ' type="radio" id="' . $field->args['_name'] . esc_attr( $value ) . '" name="' . $field->args['_name'] . '" value="' . esc_attr( $value ) . '" ' . $selected_input . ' class="cmb2-buttonset-item">
		<label class="cmb2-buttonset-label state-default' . $selected_label . '" for="' . $field->args['_name'] . esc_attr( $value ) . '"><span class="buttonset-text">' . esc_html( $item ) . '</span></label>';
		}

		$buttonset .= '</div>';
		$buttonset .= $field_type_object->_desc( true );
		echo( $buttonset );

	}

}