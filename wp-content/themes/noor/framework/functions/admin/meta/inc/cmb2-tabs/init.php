<?php

namespace cmb2_tabs;

if ( is_admin() ) {
	include __DIR__ . '/autoloader.php';
	// run global class
	new inc\CMB2_Tabs();
}