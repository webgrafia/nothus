<?php


if ( is_admin() ) {
	include_once __DIR__ . '/inc/cmb2-toggle.class.php';
	new CMB2_dima_toggle();

	include_once __DIR__ . '/inc/cmb2-buttonset.class.php';
	new CMB2_dima_buttonset();

	include_once __DIR__ . '/inc/cmb2-image_select.php';
	new CMB2_dima_image_select();
	include_once __DIR__ . '/inc/slider_metafield.php';
	new CMB2_dima_slider();
}