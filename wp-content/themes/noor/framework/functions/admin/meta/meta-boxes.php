<?php

/**
 * Registers the meta boxes for pages-posts- portfolio.
 *
 * Display the gallery images meta box.
 *
 * @package Dima Framework
 * @subpackage Admin Meta
 * @version   1.0.0
 * @since     1.0.0
 * @author    PixelDima <info@pixeldima.com>
 *
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class dima_meta_data {
	static $image_path;

	function __construct() {
		self::$image_path = DIMA_TEMPLATE_URL . "/framework/functions/admin/meta/inc/dima-meta-types/";
	}


	static function dima_get_sidebar_list() {
		$sidebars_name = array( '' => esc_html__( 'Inherit', 'noor' ) );
		foreach ( $GLOBALS['wp_registered_sidebars'] as $sidebar ) {
			$sidebars_name[ $sidebar['id'] ] = ucwords( $sidebar['name'] );
		}

		return $sidebars_name;
	}

	static function dima_get_sliders_sr_list() {
		$sliders_list = apply_filters( 'dima_sliders_meta', array() );
		$sliders      = array( '' => esc_html__( 'none', 'noor' ) );
		foreach ( $sliders_list as $key => $value ) {
			$sliders[ esc_attr( $key ) ] = ucwords( $value['name'] );
		}

		return $sliders;
	}

	static function dima_get_meta_slider_settings() {
		$opt                 = dima_meta_data::dima_get_sliders_sr_list();
		$slider_settings_tab = array(
			array(
				'name'            => esc_html__( 'Slider : Below The Menu', 'noor' ),
				'id'              => '_dima_slider_below',
				'type'            => 'select',
				'options'         => $opt,
				'label_cb'        => 'dima_meta_add_tooltip_to_label',
				'tooltip_options' => array(
					'tooltip-class' => '',
					'desc'          => esc_html__( 'Choose Slide Template', 'noor' ),
				),
			)
		);

		return $slider_settings_tab;
	}

	static function dima_get_meta_menu_settings() {

		$url        = admin_url( '/nav-menus.php?action=edit&menu=&post_association=' );
		$link_title = esc_html__( 'Create New Menu', 'noor' );
		$link       = '<a href="' . esc_url( $url ) . '">' . esc_html( $link_title ) . '</a>';
		$output     = array(
			array(
				'name'            => esc_html__( 'Menu layout', 'noor' ),
				'id'              => '_dima_meta_header_style',
				'type'            => 'dima_image_select',
				'label_cb'        => 'dima_meta_add_tooltip_to_label',
				'tooltip_options' => array(
					'tooltip-class' => '',
					'desc'          => esc_html__( 'Use this option to choose the menu layout for this page', 'noor' ),
				),
				'options'         => array(
					'inherit'           => array(
						'title' => esc_html__( 'Inherit', 'noor' ),
						'alt'   => 'Inherit',
						'img'   => self::$image_path . 'img/inherit.png'
					),
					'fill-width'        => array(
						'title' => esc_html__( 'Full-Width', 'noor' ),
						'alt'   => 'fill-width',
						'img'   => self::$image_path . 'img/fill-width.png'
					),
					'fixed-left'        => array(
						'title' => esc_html__( 'Fixed Left', 'noor' ),
						'alt'   => 'fixed-left',
						'img'   => self::$image_path . 'img/fixed-left.png'
					),
					'fixed-right'       => array(
						'title' => esc_html__( 'Fixed Right', 'noor' ),
						'alt'   => 'fixed-right',
						'img'   => self::$image_path . 'img/fixed-right.png'
					),
					'big-navegation'    => array(
						'title' => esc_html__( 'Big Navegation', 'noor' ),
						'alt'   => 'big-navegation',
						'img'   => self::$image_path . 'img/big-navegation.png'
					),
					'logo-on-center'    => array(
						'title' => esc_html__( 'logo on center', 'noor' ),
						'alt'   => 'logo-on-center',
						'img'   => self::$image_path . 'img/logo-on-center.png'
					),
					'bottom-logo'       => array(
						'title' => esc_html__( 'Bottom logo', 'noor' ),
						'alt'   => 'bottom-logo',
						'img'   => self::$image_path . 'img/bottom-logo.png'
					),
					'fixed-left-small'  => array(
						'title' => esc_html__( 'Fixed Left Small', 'noor' ),
						'alt'   => 'fixed-left-small',
						'img'   => self::$image_path . 'img/fixed-left-small.png'
					),
					'fixed-right-small' => array(
						'title' => esc_html__( 'Fixed Right Small', 'noor' ),
						'alt'   => 'fixed-left-small',
						'img'   => self::$image_path . 'img/fixed-right-small.png'
					),
				),
				'default'         => 'inherit',
			),

			array(
				'name'       => esc_html__( 'Navbar Alignement', 'noor' ),
				'id'         => '_dima_meta_navbar_text_align',
				'type'       => 'dima_buttonset',
				'options'    => array(
					'inherit'     => esc_html__( 'Inherit', 'noor' ),
					"text-start"  => esc_html__( 'Start', 'noor' ),
					"text-center" => esc_html__( 'Center', 'noor' ),
					"text-end"    => esc_html__( 'End', 'noor' ),
				),
				'attributes' => array(
					'data-conditional-value' => wp_json_encode( array(
						'full-width',
						'fixed-left',
						'bottom-logo',
						'fixed-right'
					) ),
					'data-conditional-id'    => '_dima_meta_header_style',
				),
				'default'    => 'inherit',
			),

			array(
				'name'            => esc_html__( 'Transparent Menu?', 'noor' ),
				'id'              => '_dima_meta_transparent_menu',
				'type'            => 'dima_buttonset',
				'label_cb'        => 'dima_meta_add_tooltip_to_label',
				'tooltip_options' => array(
					'tooltip-class' => '',
					'desc'          => esc_html__( 'Enabling this option will add padding before page title.', 'noor' ),
				),
				'options'         => array(
					'inherit' => esc_html__( 'Inherit', 'noor' ),
					'on'      => esc_html__( 'On', 'noor' ),
					'off'     => esc_html__( 'Off', 'noor' )
				),
				'default'         => 'inherit',
			),

			array(
				'name'    => esc_html__( 'Navbar Animation', 'noor' ),
				'id'      => '_dima_meta_navbar_animation',
				'type'    => 'dima_buttonset',
				'options' => array(
					'inherit'          => esc_html__( 'Inherit', 'noor' ),
					'static-top'       => esc_html__( 'Static Top', 'noor' ),
					'fixed-top'        => esc_html__( 'Fixed Top', 'noor' ),
					'headroom'         => esc_html__( 'Headroom', 'noor' ),
					'fixed-top-offset' => esc_html__( 'Fixed Top (Animated)', 'noor' ),
				),
				'default' => 'inherit',
			),
			array(
				'name'       => esc_html__( 'Offset By Pixels', 'noor' ),
				'desc'       => '',
				'id'         => '_dima_meta_navbar_offset_by_px',
				'type'       => 'text',
				'attributes' => array(
					'data-conditional-value' => wp_json_encode( array( 'fixed-top-offset' ) ),
					'data-conditional-id'    => '_dima_meta_navbar_animation',
				),
			),
			array(
				'name'       => esc_html__( 'Offset By Elemnt Target', 'noor' ),
				'desc'       => '',
				'id'         => '_dima_meta_navbar_offset_by_id',
				'type'       => 'text',
				'attributes' => array(
					'data-conditional-value' => wp_json_encode( array( 'fixed-top-offset' ) ),
					'data-conditional-id'    => '_dima_meta_navbar_animation',
				),
			),

			array(
				'name'    => esc_html__( 'Display Button?', 'noor' ),
				'id'      => '_dima_meta_navbar_button',
				'type'    => 'dima_buttonset',
				'options' => array(
					'inherit' => esc_html__( 'Inherit', 'noor' ),
					'on'      => esc_html__( 'On', 'noor' ),
					'off'     => esc_html__( 'Off', 'noor' )
				),
				'default' => 'inherit',
			),
			array(
				'name'       => esc_html__( 'Button URL', 'noor' ),
				'desc'       => '',
				'id'         => '_dima_meta_navbar_button_url',
				'type'       => 'text_url',
				'attributes' => array(
					'data-conditional-value' => 'on',
					'data-conditional-id'    => '_dima_meta_navbar_button',
				),
			),
			array(
				'name'       => esc_html__( 'Button Text', 'noor' ),
				'desc'       => '',
				'id'         => '_dima_meta_navbar_button_txt',
				'type'       => 'text',
				'attributes' => array(
					'data-conditional-value' => 'on',
					'data-conditional-id'    => '_dima_meta_navbar_button',
				),
			),

			array(
				'name'    => esc_html__( 'Menu Dark?', 'noor' ),
				'id'      => '_dima_meta_navbar_menu_dark',
				'type'    => 'dima_buttonset',
				'options' => array(
					'inherit' => esc_html__( 'Inherit', 'noor' ),
					'on'      => esc_html__( 'On', 'noor' ),
					'off'     => esc_html__( 'Off', 'noor' )
				),
				'default' => 'inherit',
			),
			array(
				'name'    => esc_html__( 'Sub-Menu Dark?', 'noor' ),
				'id'      => '_dima_meta_navbar_sup_menu_dark',
				'type'    => 'dima_buttonset',
				'options' => array(
					'inherit' => esc_html__( 'Inherit', 'noor' ),
					'on'      => esc_html__( 'On', 'noor' ),
					'off'     => esc_html__( 'Off', 'noor' )
				),
				'default' => 'inherit',
			),

			array(
				'name'            => esc_html__( 'WPML language switcher?', 'noor' ),
				'id'              => '_dima_meta_header_navbar_wpml_lang_show',
				'type'            => 'dima_buttonset',
				'label_cb'        => 'dima_meta_add_tooltip_to_label',
				'tooltip_options' => array(
					'tooltip-class' => '',
					'desc'          => esc_html__( 'Allows you to have the WPML language switcher in header. Note, WPML plugin must be installed (WPML plugin is not included to the theme pack .You can find the plugin here)', 'noor' ) . ': http://wpml.org/',
				),
				'options'         => array(
					'inherit' => esc_html__( 'Inherit', 'noor' ),
					'on'      => esc_html__( 'On', 'noor' ),
					'off'     => esc_html__( 'Off', 'noor' )
				),
				'default'         => 'inherit',
			),


			array(
				'name' => esc_html__( 'Navigation settings', 'noor' ),
				'id'   => '_dima_meta_navigation',
				'type' => 'title',
			),
			array(
				'name' => esc_html__( 'One Page navigation?', 'noor' ),
				'desc' => '',
				'id'   => '_dima_meta_one_page_navigation',
				'type' => 'dima_toggle',
			),

			array(
				'name'    => esc_html__( 'Display Primary Menu?', 'noor' ),
				'id'      => '_dima_meta_header_navbar_primary_menu',
				'type'    => 'dima_buttonset',
				'options' => array(
					'inherit' => esc_html__( 'Inherit', 'noor' ),
					'on'      => esc_html__( 'On', 'noor' ),
					'off'     => esc_html__( 'Off', 'noor' )
				),
				'default' => 'inherit',
			),


			array(
				'name'    => esc_html__( 'Primary navigation', 'noor' ),
				'desc'    => '',
				'id'      => '_dima_meta_primary_navigation',
				'type'    => 'select',
				'options' => dima_helper::dima_get_menus_list_options(),
				'after'   => $link,
			),

			array(
				'name'    => esc_html__( 'Display Icon Menu?', 'noor' ),
				'id'      => '_dima_meta_header_navbar_icon_menu',
				'type'    => 'dima_buttonset',
				'options' => array(
					'inherit' => esc_html__( 'Inherit', 'noor' ),
					'on'      => esc_html__( 'On', 'noor' ),
					'off'     => esc_html__( 'Off', 'noor' )
				),
				'default' => 'inherit',
			),

			array(
				'name'    => esc_html__( 'Icon navigation', 'noor' ),
				'desc'    => '',
				'id'      => '_dima_meta_icon_navigation',
				'type'    => 'select',
				'options' => dima_helper::dima_get_menus_list_options(),
				'after'   => $link,
			),
			array(
				'name'    => esc_html__( 'Burger navigation', 'noor' ),
				'desc'    => '',
				'id'      => '_dima_meta_burger_navigation',
				'type'    => 'select',
				'options' => dima_helper::dima_get_menus_list_options(),
				'after'   => $link,
			),

			array(
				'name' => esc_html__( 'Menu-burger & Search', 'noor' ),
				'id'   => '_dima_meta_menu_and_burger_title',
				'type' => 'title',
			),

			array(
				'name'    => esc_html__( 'Navbar Burger', 'noor' ),
				'id'      => '_dima_meta_burger_display',
				'type'    => 'dima_buttonset',
				'options' => array(
					'inherit' => esc_html__( 'Inherit', 'noor' ),
					'on'      => esc_html__( 'On', 'noor' ),
					'off'     => esc_html__( 'Off', 'noor' )
				),
				'default' => 'inherit',
			),
			array(
				'name'       => esc_html__( 'Burger Style', 'noor' ),
				'id'         => '_dima_meta_burger_style',
				'type'       => 'dima_buttonset',
				'options'    => array(
					'inherit' => esc_html__( 'Inherit', 'noor' ),
					'full'    => esc_html__( 'Full', 'noor' ),
					'end'     => esc_html__( 'Side', 'noor' )
				),
				'default'    => 'inherit',
				'attributes' => array(
					'data-conditional-value' => wp_json_encode( array( 'inherit', 'on' ) ),
					'data-conditional-id'    => '_dima_meta_burger_display',
				),
			),

			array(
				'name'            => esc_html__( 'Display Desktop Burger in mobile', 'noor' ),
				'id'              => '_dima_meta_header_navbar_burger_mobile',
				'type'            => 'dima_buttonset',
				'options'         => array(
					'inherit' => esc_html__( 'Inherit', 'noor' ),
					'on'      => esc_html__( 'On', 'noor' ),
					'off'     => esc_html__( 'Off', 'noor' )
				),
				'tooltip_options' => array(
					'tooltip-class' => '',
					'desc'          => esc_html__( 'Check “On” to display the contents of Burger Menu desktop version in Mobile.', 'noor' ) . ': http://wpml.org/',
				),
				'default'         => 'inherit',
				'attributes'      => array(
					'data-conditional-value' => wp_json_encode( array( 'inherit', 'on' ) ),
					'data-conditional-id'    => '_dima_meta_burger_display',
				),
			),


			array(
				'name'       => esc_html__( 'Burger Position', 'noor' ),
				'id'         => '_dima_meta_burger_position',
				'type'       => 'dima_buttonset',
				'options'    => array(
					'inherit' => esc_html__( 'Inherit', 'noor' ),
					'start'   => esc_html__( 'Start', 'noor' ),
					'end'     => esc_html__( 'End', 'noor' )
				),
				'default'    => 'inherit',
				'attributes' => array(
					'data-conditional-value' => wp_json_encode( array( 'inherit', 'on' ) ),
					'data-conditional-id'    => '_dima_meta_burger_display',
				),
			),

			array(
				'name'       => esc_html__( 'burger background Image', 'noor' ),
				'id'         => '_dima_meta_burger_bg_img',
				'type'       => 'file',
				'attributes' => array(
					'data-conditional-value' => wp_json_encode( array( 'inherit', 'on' ) ),
					'data-conditional-id'    => '_dima_meta_search_display',
				),
				'default'    => '',
			),

			array(
				'name'    => esc_html__( 'Search', 'noor' ),
				'id'      => '_dima_meta_search_display',
				'type'    => 'dima_buttonset',
				'options' => array(
					'inherit' => esc_html__( 'Inherit', 'noor' ),
					'on'      => esc_html__( 'On', 'noor' ),
					'off'     => esc_html__( 'Off', 'noor' )
				),
				'default' => 'inherit',
			),

			array(
				'name'       => esc_html__( 'Search/burger background Color', 'noor' ),
				'id'         => '_dima_meta_search_bg',
				'type'       => 'rgba_colorpicker',
				'attributes' => array(
					'data-conditional-value' => wp_json_encode( array( 'inherit', 'on' ) ),
					'data-conditional-id'    => '_dima_meta_search_display',
				),
				'default'    => '',
			),

			array(
				'name'       => esc_html__( 'Search background Image', 'noor' ),
				'id'         => '_dima_meta_search_bg_img',
				'type'       => 'file',
				'attributes' => array(
					'data-conditional-value' => wp_json_encode( array( 'inherit', 'on' ) ),
					'data-conditional-id'    => '_dima_meta_search_display',
				),
				'default'    => '',
			),

		);

		return $output;
	}

	static function dima_get_meta_logo_settings() {
		$output = array(

			array(
				'name' => esc_html__( 'logo settings', 'noor' ),
				'id'   => '_dima_meta_logo',
				'type' => 'title',
			),
			array(
				'name'            => esc_html__( 'Logo Width (px)', 'noor' ),
				'id'              => '_dima_meta_header_logo_width',
				'label_cb'        => 'dima_meta_add_tooltip_to_label',
				'tooltip_options' => array(
					'tooltip-class' => '',
					'desc'          => esc_html__( 'You can change the logo width for this spisific page only', 'noor' ),
				),
				'type'            => 'text',
			),

			array(
				'name'            => esc_html__( 'Upload Your Logo', 'noor' ),
				'id'              => '_dima_meta_header_logo',
				'type'            => 'file',
				'text'            => array(
					'add_upload_file_text' => esc_html__( 'Add or Upload File', 'noor' ),
				),
				'label_cb'        => 'dima_meta_add_tooltip_to_label',
				'tooltip_options' => array(
					'tooltip-class' => '',
					'desc'          => esc_html__( 'Upload your Logo image, or enter logo URL lnik using the field.', 'noor' ),
				),
			),

			array(
				'name'            => esc_html__( 'Upload Your Sticky Logo', 'noor' ),
				'id'              => '_dima_meta_header_sticky_logo',
				'type'            => 'file',
				'text'            => array(
					'add_upload_file_text' => esc_html__( 'Add or Upload File', 'noor' ),
				),
				'label_cb'        => 'dima_meta_add_tooltip_to_label',
				'tooltip_options' => array(
					'tooltip-class' => '',
					'desc'          => esc_html__( 'Upload your Logo image, or enter logo URL lnik using the field.', 'noor' ),
				),
			),

			array(
				'name'            => esc_html__( 'Upload Your Mobile Logo', 'noor' ),
				'id'              => '_dima_meta_header_mobile_logo',
				'type'            => 'file',
				'text'            => array(
					'add_upload_file_text' => esc_html__( 'Add or Upload File', 'noor' ),
				),
				'label_cb'        => 'dima_meta_add_tooltip_to_label',
				'tooltip_options' => array(
					'tooltip-class' => '',
					'desc'          => esc_html__( 'Upload your Logo image, or enter logo URL lnik using the field.', 'noor' ),
				),
			),

			array(
				'name'            => esc_html__( 'Upload Your Logo (Retina Version @2x)', 'noor' ),
				'id'              => '_dima_meta_header_logo_retina',
				'type'            => 'file',
				'text'            => array(
					'add_upload_file_text' => esc_html__( 'Add or Upload File', 'noor' ),
				),
				'label_cb'        => 'dima_meta_add_tooltip_to_label',
				'tooltip_options' => array(
					'tooltip-class' => '',
					'desc'          => esc_html__( 'Upload your Logo image, or enter logo URL lnik using the field.', 'noor' ),
				),
			),

			array(
				'name'            => esc_html__( 'Upload Your Sticky Logo (Retina Version @2x)', 'noor' ),
				'id'              => '_dima_meta_header_sticky_logo_retina',
				'type'            => 'file',
				'text'            => array(
					'add_upload_file_text' => esc_html__( 'Add or Upload File', 'noor' ),
				),
				'label_cb'        => 'dima_meta_add_tooltip_to_label',
				'tooltip_options' => array(
					'tooltip-class' => '',
					'desc'          => esc_html__( 'Upload your Logo image, or enter logo URL lnik using the field.', 'noor' ),
				),
			),

			array(
				'name'            => esc_html__( 'Upload Your Mobile Logo (Retina Version @2x)', 'noor' ),
				'id'              => '_dima_meta_header_mobile_logo_retina',
				'type'            => 'file',
				'text'            => array(
					'add_upload_file_text' => esc_html__( 'Add or Upload File', 'noor' ),
				),
				'label_cb'        => 'dima_meta_add_tooltip_to_label',
				'tooltip_options' => array(
					'tooltip-class' => '',
					'desc'          => esc_html__( 'Upload your Logo image, or enter logo URL lnik using the field.', 'noor' ),
				),
			),

			array(
				'name' => esc_html__( 'RTL logo settings', 'noor' ),
				'id'   => '_dima_meta_logo_rtl',
				'type' => 'title',
			),
			array(
				'name'            => esc_html__( 'Logo Width (px)', 'noor' ),
				'id'              => '_dima_meta_header_logo_width_rtl',
				'label_cb'        => 'dima_meta_add_tooltip_to_label',
				'tooltip_options' => array(
					'tooltip-class' => '',
					'desc'          => esc_html__( 'You can change the logo width for this spisific page only', 'noor' ),
				),
				'type'            => 'text',
			),

			array(
				'name'            => esc_html__( 'Upload Your Logo', 'noor' ),
				'id'              => '_dima_meta_header_logo_rtl',
				'type'            => 'file',
				'text'            => array(
					'add_upload_file_text' => esc_html__( 'Add or Upload File', 'noor' ),
				),
				'label_cb'        => 'dima_meta_add_tooltip_to_label',
				'tooltip_options' => array(
					'tooltip-class' => '',
					'desc'          => esc_html__( 'Upload your Logo image, or enter logo URL lnik using the field.', 'noor' ),
				),
			),

			array(
				'name'            => esc_html__( 'Upload Your Sticky Logo', 'noor' ),
				'id'              => '_dima_meta_header_sticky_logo_rtl',
				'type'            => 'file',
				'text'            => array(
					'add_upload_file_text' => esc_html__( 'Add or Upload File', 'noor' ),
				),
				'label_cb'        => 'dima_meta_add_tooltip_to_label',
				'tooltip_options' => array(
					'tooltip-class' => '',
					'desc'          => esc_html__( 'Upload your Logo image, or enter logo URL lnik using the field.', 'noor' ),
				),
			),

			array(
				'name'            => esc_html__( 'Upload Your Mobile Logo', 'noor' ),
				'id'              => '_dima_meta_header_mobile_logo_rtl',
				'type'            => 'file',
				'text'            => array(
					'add_upload_file_text' => esc_html__( 'Add or Upload File', 'noor' ),
				),
				'label_cb'        => 'dima_meta_add_tooltip_to_label',
				'tooltip_options' => array(
					'tooltip-class' => '',
					'desc'          => esc_html__( 'Upload your Logo image, or enter logo URL lnik using the field.', 'noor' ),
				),
			),

			array(
				'name'            => esc_html__( 'Upload Your Logo (Retina Version @2x)', 'noor' ),
				'id'              => '_dima_meta_header_logo_retina_rtl',
				'type'            => 'file',
				'text'            => array(
					'add_upload_file_text' => esc_html__( 'Add or Upload File', 'noor' ),
				),
				'label_cb'        => 'dima_meta_add_tooltip_to_label',
				'tooltip_options' => array(
					'tooltip-class' => '',
					'desc'          => esc_html__( 'Upload your Logo image, or enter logo URL lnik using the field.', 'noor' ),
				),
			),

			array(
				'name'            => esc_html__( 'Upload Your Sticky Logo (Retina Version @2x)', 'noor' ),
				'id'              => '_dima_meta_header_sticky_logo_retina_rtl',
				'type'            => 'file',
				'text'            => array(
					'add_upload_file_text' => esc_html__( 'Add or Upload File', 'noor' ),
				),
				'label_cb'        => 'dima_meta_add_tooltip_to_label',
				'tooltip_options' => array(
					'tooltip-class' => '',
					'desc'          => esc_html__( 'Upload your Logo image, or enter logo URL lnik using the field.', 'noor' ),
				),
			),

			array(
				'name'            => esc_html__( 'Upload Your Mobile Logo (Retina Version @2x)', 'noor' ),
				'id'              => '_dima_meta_header_mobile_logo_retina_rtl',
				'type'            => 'file',
				'text'            => array(
					'add_upload_file_text' => esc_html__( 'Add or Upload File', 'noor' ),
				),
				'label_cb'        => 'dima_meta_add_tooltip_to_label',
				'tooltip_options' => array(
					'tooltip-class' => '',
					'desc'          => esc_html__( 'Upload your Logo image, or enter logo URL lnik using the field.', 'noor' ),
				),
			),

		);

		return $output;
	}

	static function dima_get_meta_header_settings() {
		$output = array(
			array(
				'name' => esc_html__( 'Page Title', 'noor' ),
				'id'   => '_dima_meta_breadcumbs_display_title',
				'type' => 'title',
			),
			array(
				'name'            => esc_html__( 'Page Title Display', 'noor' ),
				'id'              => '_dima_meta_breadcumbs_display',
				'label_cb'        => 'dima_meta_add_tooltip_to_label',
				'tooltip_options' => array(
					'tooltip-class' => '',
					'desc'          => esc_html__( 'Choose to display or hide page title bar', 'noor' ),
				),
				'type'            => 'dima_buttonset',
				'options'         => array(
					'inherit' => esc_html__( 'Inherit', 'noor' ),
					'on'      => esc_html__( 'On', 'noor' ),
					'off'     => esc_html__( 'Off', 'noor' )
				),
				'default'         => 'inherit',
			),
			array(
				'name'            => esc_html__( 'Page Title Position', 'noor' ),
				'id'              => '_dima_meta_breadcumbs_position',
				'type'            => 'dima_buttonset',
				'options'         => array(
					'inherit' => esc_html__( 'Inherit', 'noor' ),
					'start'   => esc_html__( 'start', 'noor' ),
					'center'  => esc_html__( 'center', 'noor' )
				),
				'attributes'      => array(
					'data-conditional-value' => wp_json_encode( array( 'inherit', 'on' ) ),
					'data-conditional-id'    => '_dima_meta_breadcumbs_display',
				),
				'label_cb'        => 'dima_meta_add_tooltip_to_label',
				'tooltip_options' => array(
					'tooltip-class' => '',
					'desc'          => esc_html__( 'You can change Page Title position to start (left) or center', 'noor' ),
				),
				'default'         => 'inherit',
			),
			array(
				'name'            => esc_html__( 'Page Title Dark', 'noor' ),
				'id'              => '_dima_meta_navbar_page_title_dark',
				'label_cb'        => 'dima_meta_add_tooltip_to_label',
				'tooltip_options' => array(
					'tooltip-class' => '',
					'desc'          => esc_html__( 'Apply this to highlight the text in dark backgrounds', 'noor' ),
				),
				'type'            => 'dima_buttonset',
				'options'         => array(
					'inherit' => esc_html__( 'Inherit', 'noor' ),
					'on'      => esc_html__( 'On', 'noor' ),
					'off'     => esc_html__( 'Off', 'noor' )
				),
				'default'         => 'inherit',
				'attributes'      => array(
					'data-conditional-value' => wp_json_encode( array( 'inherit', 'on' ) ),
					'data-conditional-id'    => '_dima_meta_breadcumbs_display',
				),
			),
			array(
				'name'            => esc_html__( 'Breadcrumbs Display', 'noor' ),
				'id'              => '_dima_meta_breadcumbs_list_display',
				'label_cb'        => 'dima_meta_add_tooltip_to_label',
				'tooltip_options' => array(
					'tooltip-class' => '',
					'desc'          => esc_html__( 'Choose to show or hide the breadcrumbs bar.', 'noor' ),
				),
				'type'            => 'dima_buttonset',
				'options'         => array(
					'inherit' => esc_html__( 'Inherit', 'noor' ),
					'on'      => esc_html__( 'On', 'noor' ),
					'off'     => esc_html__( 'Off', 'noor' )
				),
				'default'         => 'inherit',
				'attributes'      => array(
					'data-conditional-value' => wp_json_encode( array( 'inherit', 'on' ) ),
					'data-conditional-id'    => '_dima_meta_breadcumbs_display',
				),
			),

			array(
				'name'            => esc_html__( 'Copyright Display', 'noor' ),
				'id'              => '_dima_meta_menu_copyright_display',
				'label_cb'        => 'dima_meta_add_tooltip_to_label',
				'tooltip_options' => array(
					'tooltip-class' => '',
					'desc'          => esc_html__( 'Choose to show or hide the copyright on vertical menu.', 'noor' ),
				),
				'type'            => 'dima_buttonset',
				'options'         => array(
					'inherit' => esc_html__( 'Inherit', 'noor' ),
					'on'      => esc_html__( 'On', 'noor' ),
					'off'     => esc_html__( 'Off', 'noor' )
				),
				'default'         => 'inherit',
			),
			array(
				'name'            => esc_html__( 'Subtitle', 'noor' ),
				'id'              => '_dima_meta_breadcumbs_subtitle',
				'label_cb'        => 'dima_meta_add_tooltip_to_label',
				'tooltip_options' => array(
					'tooltip-class' => '',
					'desc'          => esc_html__( 'Add your page subtitle', 'noor' ),
				),
				'type'            => 'text',
				'attributes'      => array(
					'data-conditional-value' => wp_json_encode( array( 'inherit', 'on' ) ),
					'data-conditional-id'    => '_dima_meta_breadcumbs_display',
				),
			),
			array(
				'name'            => esc_html__( 'Page Title Background', 'noor' ),
				'id'              => '_dima_meta_breadcumbs_image',
				'type'            => 'file',
				'text'            => array(
					'add_upload_file_text' => esc_html__( 'Add or Upload File', 'noor' ),
				),
				'label_cb'        => 'dima_meta_add_tooltip_to_label',
				'tooltip_options' => array(
					'tooltip-class' => '',
					'desc'          => esc_html__( 'Upload your background image, or enter image URL lnik using the field.', 'noor' ),
				),
				'attributes'      => array(
					'data-conditional-value' => wp_json_encode( array( 'inherit', 'on' ) ),
					'data-conditional-id'    => '_dima_meta_breadcumbs_display',
				),
			),
			array(
				'name'            => esc_html__( 'Page Title Color', 'noor' ),
				'id'              => '_dima_meta_page_title_bg_color',
				'type'            => 'rgba_colorpicker',
				'label_cb'        => 'dima_meta_add_tooltip_to_label',
				'tooltip_options' => array(
					'tooltip-class' => '',
					'desc'          => '',
				),
				'attributes'      => array(
					'data-conditional-value' => wp_json_encode( array( 'inherit', 'on' ) ),
					'data-conditional-id'    => '_dima_meta_breadcumbs_display',
				),
			),

			array(
				'name'            => esc_html__( 'Page Title Color As Cover', 'noor' ),
				'id'              => '_dima_meta_page_title_bg_is_cover',
				'label_cb'        => 'dima_meta_add_tooltip_to_label',
				'tooltip_options' => array(
					'tooltip-class' => '',
					'desc'          => '',
				),
				'type'            => 'dima_buttonset',
				'options'         => array(
					'inherit' => esc_html__( 'Inherit', 'noor' ),
					'on'      => esc_html__( 'On', 'noor' ),
					'off'     => esc_html__( 'Off', 'noor' )
				),
				'default'         => 'inherit',
				'attributes'      => array(
					'data-conditional-value' => wp_json_encode( array( 'inherit', 'on' ) ),
					'data-conditional-id'    => '_dima_meta_breadcumbs_display',
				),
			),

			array(
				'name'            => esc_html__( 'Page Title Backgrounds Parallax Style', 'noor' ),
				'id'              => '_dima_meta_breadcumbs_image_style',
				'type'            => 'dima_buttonset',
				'options'         => array(
					'inherit'  => esc_html__( 'Inherit', 'noor' ),
					'none'     => esc_html__( 'None', 'noor' ),
					'parallax' => esc_html__( 'Parallax', 'noor' ),
					'fixed'    => esc_html__( 'Fixed', 'noor' )
				),
				'attributes'      => array(
					'data-conditional-value' => wp_json_encode( array( 'inherit', 'on' ) ),
					'data-conditional-id'    => '_dima_meta_breadcumbs_display',
				),
				'label_cb'        => 'dima_meta_add_tooltip_to_label',
				'tooltip_options' => array(
					'tooltip-class' => '',
					'desc'          => esc_html__( 'Choose the animation effect for the background image.', 'noor' ),
				),
			),
			array(
				'name'            => esc_html__( 'Element Shortcode Above the Sidebar', 'noor' ),
				'id'              => '_dima_meta_shortcode_above_sidebar',
				'type'            => 'wysiwyg',
				'raw'             => false,
				'options'         => array(
					'textarea_rows' => 7,
				),
				'label_cb'        => 'dima_meta_add_tooltip_to_label',
				'tooltip_options' => array(
					'tooltip-class' => '',
					'desc'          => esc_html__( 'Enter here a shortcode created by NOOR to display it above the sidebar and below page title.', 'noor' ),
				),
			),
		);

		return $output;
	}

	static function dima_get_meta_general_settings() {
		$output = array(
			array(
				'name'            => esc_html__( 'Body CSS Class(es)', 'noor' ),
				'id'              => '_dima_meta_body_css_class',
				'type'            => 'text',
				'label_cb'        => 'dima_meta_add_tooltip_to_label',
				'tooltip_options' => array(
					'tooltip-class' => '',
					'desc'          => esc_html__( 'Add a custom CSS class to the &lt;body&gt; element. Separate multiple class names with a space.', 'noor' ),
				),
			),
			array(
				'name'            => esc_html__( 'Content Layout', 'noor' ),
				'id'              => '_dima_meta_layout',
				'type'            => 'dima_image_select',
				'label_cb'        => 'dima_meta_add_tooltip_to_label',
				'tooltip_options' => array(
					'tooltip-class' => '',
					'desc'          => esc_html__( 'If your global content layout includes a sidebar, selecting this options will affect this page only.', 'noor' ),
				),
				'options'         => array(
					'inherit'       => array(
						'title' => esc_html__( 'Inherit', 'noor' ),
						'alt'   => 'Inherit',
						'img'   => self::$image_path . 'img/inherit.png'
					),
					'mini'          => array(
						'title' => esc_html__( 'Mini', 'noor' ),
						'alt'   => 'mini',
						'img'   => self::$image_path . 'img/mini.png'
					),
					'no-sidebar'    => array(
						'title' => esc_html__( 'No Sidebar', 'noor' ),
						'alt'   => 'no-sidebar',
						'img'   => self::$image_path . 'img/no-sidebar.png'
					),
					'left-sidebar'  => array(
						'title' => esc_html__( 'Left Sidebar', 'noor' ),
						'alt'   => 'left-sidebar',
						'img'   => self::$image_path . 'img/left-sidebar.png'
					),
					'right-sidebar' => array(
						'title' => esc_html__( 'Right Sidebar', 'noor' ),
						'alt'   => 'right-sidebar',
						'img'   => self::$image_path . 'img/right-sidebar.png'
					),
				),
				'default'         => 'inherit',

			),
			array(
				'name'            => esc_html__( 'Sidebar', 'noor' ),
				'id'              => '_dima_meta_sidebar',
				'label_cb'        => 'dima_meta_add_tooltip_to_label',
				'tooltip_options' => array(
					'tooltip-class' => '',
					'desc'          => esc_html__( 'Choose Sidebar.', 'noor' ),
				),
				'type'            => 'select',
				'options'         => self::dima_get_sidebar_list(),
				'attributes'      => array(
					'data-conditional-value' => wp_json_encode( array( 'left-sidebar', 'right-sidebar', 'inherit' ) ),
					'data-conditional-id'    => '_dima_meta_layout',
				),
			),

			array(
				'name'            => esc_html__( 'Sidebar sticky', 'noor' ),
				'id'              => '_dima_meta_sidebar_sticky',
				'label_cb'        => 'dima_meta_add_tooltip_to_label',
				'tooltip_options' => array(
					'tooltip-class' => '',
					'desc'          => esc_html__( 'This will make Sidebar sticky (Useful when a sidebar is too tall or too short compared to the rest of the content)', 'noor' ),
				),
				'type'            => 'dima_buttonset',
				'options'         => array(
					'inherit' => esc_html__( 'Inherit', 'noor' ),
					'on'      => esc_html__( 'On', 'noor' ),
					'off'     => esc_html__( 'Off', 'noor' )
				),
				'default'         => 'inherit',
				'attributes'      => array(
					'data-conditional-value' => wp_json_encode( array( 'left-sidebar', 'right-sidebar', 'inherit' ) ),
					'data-conditional-id'    => '_dima_meta_layout',
				),
			),

			array(
				'name'            => esc_html__( 'Custom Sidebar width', 'noor' ),
				'id'              => '_dima_meta_custom_sidebar_width',
				'type'            => 'dima_toggle',
				'label_cb'        => 'dima_meta_add_tooltip_to_label',
				'tooltip_options' => array(
					'tooltip-class' => '',
					'desc'          => esc_html__( 'Enabling this option will let you change sidebar width ', 'noor' ),
				),
			),

			array(
				'name'            => esc_html__( 'Sidebar width (px)', 'noor' ),
				'id'              => '_dima_meta_sidebar_width',
				'type'            => 'slider',
				'min'             => '240',
				'step'            => '5',
				'max'             => '350',
				'default'         => '240', // start value
				'value_label'     => 'Value:',
				'label_cb'        => 'dima_meta_add_tooltip_to_label',
				'tooltip_options' => array(
					'tooltip-class' => '',
					'desc'          => esc_html__( 'Sidebar width (%)', 'noor' ),
				),
				'attributes'      => array(
					'data-conditional-value' => wp_json_encode( array( '1', 'on' ) ),
					'data-conditional-id'    => '_dima_meta_custom_sidebar_width',
				),

				/*	'attributes'      => array(
						'data-conditional-value' => wp_json_encode( array( 'left-sidebar', 'right-sidebar', 'inherit' ) ),
						'data-conditional-id'    => '_dima_meta_layout',
					),*/
			),


			array(
				'name'            => esc_html__( 'Site Layout', 'noor' ),
				'id'              => '_dima_meta_site_layout',
				'type'            => 'dima_image_select',
				'label_cb'        => 'dima_meta_add_tooltip_to_label',
				'tooltip_options' => array(
					'tooltip-class' => '',
					'desc'          => esc_html__( 'Choose your site layout', 'noor' ),
				),
				'options'         => array(
					'inherit'    => array(
						'title' => esc_html__( 'Inherit', 'noor' ),
						'alt'   => 'Inherit',
						'img'   => self::$image_path . 'img/inherit.png'
					),
					'full-width' => array(
						'title' => esc_html__( 'Full Width', 'noor' ),
						'alt'   => 'full-width',
						'img'   => self::$image_path . 'img/full-width.png'
					),
					'framed'     => array(
						'title' => esc_html__( 'Framed', 'noor' ),
						'alt'   => 'Full-width',
						'img'   => self::$image_path . 'img/framed.png'
					),
				),
				'default'         => 'inherit',
			),
			array(
				'name'            => esc_html__( 'Frame size', 'noor' ),
				'id'              => '_dima_meta_frame_size',
				'label_cb'        => 'dima_meta_add_tooltip_to_label',
				'tooltip_options' => array(
					'tooltip-class' => '',
					'desc'          => esc_html__( 'Choose the frame size', 'noor' ),
				),
				'type'            => 'dima_buttonset',
				'options'         => array(
					'inherit' => esc_html__( 'Inherit', 'noor' ),
					'x10'     => esc_html__( '10 px', 'noor' ),
					'x20'     => esc_html__( '20 px', 'noor' ),
				),
				'attributes'      => array(
					'data-conditional-value' => wp_json_encode( array( 'inherit', 'framed', '' ) ),
					'data-conditional-id'    => '_dima_meta_site_layout',
				),
				'default'         => 'inherit',
			),
			array(
				'name'            => esc_html__( 'Frame color', 'noor' ),
				'id'              => '_dima_meta_frame_color',
				'label_cb'        => 'dima_meta_add_tooltip_to_label',
				'tooltip_options' => array(
					'tooltip-class' => '',
					'desc'          => esc_html__( 'Choose color for the frame', 'noor' ),
				),
				'attributes'      => array(
					'data-conditional-value' => wp_json_encode( array( 'inherit', 'framed', '' ) ),
					'data-conditional-id'    => '_dima_meta_site_layout',
				),
				'type'            => 'rgba_colorpicker',
				'default'         => '',
			),

			array(
				'name'            => esc_html__( 'Custom content width', 'noor' ),
				'id'              => '_dima_meta_custom_content_width',
				'type'            => 'dima_toggle',
				'label_cb'        => 'dima_meta_add_tooltip_to_label',
				'tooltip_options' => array(
					'tooltip-class' => '',
					'desc'          => esc_html__( 'Enabling this option will let you change content width ', 'noor' ),
				),
			),

			array(
				'name'            => esc_html__( 'Site content width (%)', 'noor' ),
				'id'              => '_dima_meta_content_width',
				'type'            => 'slider',
				'min'             => '50',
				'step'            => '1',
				'max'             => '100',
				'default'         => '', // start value
				'value_label'     => 'Value:',
				'label_cb'        => 'dima_meta_add_tooltip_to_label',
				'tooltip_options' => array(
					'tooltip-class' => '',
					'desc'          => esc_html__( 'Site Content Width (%)', 'noor' ),
				),
				'attributes'      => array(
					'data-conditional-value' => wp_json_encode( array( '1', 'on' ) ),
					'data-conditional-id'    => '_dima_meta_custom_content_width',
				),
			),
			array(
				'name'            => esc_html__( 'Site max content Width (px)', 'noor' ),
				'id'              => '_dima_meta_content_max_width',
				'type'            => 'slider',
				'min'             => '600',
				'step'            => '10',
				'max'             => '1500',
				'default'         => '', // start value
				'value_label'     => 'Value:',
				'label_cb'        => 'dima_meta_add_tooltip_to_label',
				'tooltip_options' => array(
					'tooltip-class' => '',
					'desc'          => esc_html__( 'Site Max Content Width (px)', 'noor' ),
				),
				'attributes'      => array(
					'data-conditional-value' => wp_json_encode( array( '1', 'on' ) ),
					'data-conditional-id'    => '_dima_meta_custom_content_width',
				),
			),
			array(
				'name'            => esc_html__( 'Loading Screen', 'noor' ),
				'id'              => '_dima_meta_loading',
				'label_cb'        => 'dima_meta_add_tooltip_to_label',
				'tooltip_options' => array(
					'tooltip-class' => '',
					'desc'          => esc_html__( 'The site preloader appears while the content is loading and hold visitor from seeing the content restructures until the page renders correctly. If you choose the default option the displaying will correspond to the theme options settings.', 'noor' ),
				),
				'type'            => 'dima_buttonset',
				'options'         => array(
					'inherit' => esc_html__( 'Inherit', 'noor' ),
					'on'      => esc_html__( 'On', 'noor' ),
					'off'     => esc_html__( 'Off', 'noor' )
				),
				'default'         => 'inherit',
			),

			array(
				'name'            => esc_html__( 'Website Background Color', 'noor' ),
				'id'              => '_dima_meta_body_background_color',
				'label_cb'        => 'dima_meta_add_tooltip_to_label',
				'tooltip_options' => array(
					'tooltip-class' => '',
					'desc'          => esc_html__( 'This color will be applied to Website body.', 'noor' ),
				),
				'type'            => 'rgba_colorpicker',
				'default'         => '',
			),

			array(
				'name'            => esc_html__( 'Website Background Image', 'noor' ),
				'id'              => '_dima_meta_body_background_image',
				'label_cb'        => 'dima_meta_add_tooltip_to_label',
				'tooltip_options' => array(
					'tooltip-class' => '',
					'desc'          => esc_html__( 'This Background will be applied to Website body.', 'noor' ),
				),
				'type'            => 'file',
				'text'            => array(
					'add_upload_file_text' => esc_html__( 'Add or Upload File', 'noor' ),
				),
				'default'         => '',
			),

			array(
				'name'            => esc_html__( 'Website Background Repeat', 'noor' ),
				'id'              => '_dima_meta_body_background_image_repeat',
				'label_cb'        => 'dima_meta_add_tooltip_to_label',
				'tooltip_options' => array(
					'tooltip-class' => '',
					'desc'          => '',
				),
				'type'            => 'select',
				'options'         => array(
					''          => esc_html__( 'background-repeat', 'noor' ),
					'no-repeat' => esc_html__( 'No Repeat', 'noor' ),
					'repeat'    => esc_html__( 'Repeat All', 'noor' ),
					'repeat-x'  => esc_html__( 'Repeat Horizontally', 'noor' ),
					'repeat-y'  => esc_html__( 'Repeat Vertically', 'noor' ),
					'inherit'   => esc_html__( 'Inherit', 'noor' ),
				),
				'default'         => '',
			),

			array(
				'name'            => esc_html__( 'Website Background Attachment', 'noor' ),
				'id'              => '_dima_meta_body_background_image_attachment',
				'label_cb'        => 'dima_meta_add_tooltip_to_label',
				'tooltip_options' => array(
					'tooltip-class' => '',
					'desc'          => '',
				),
				'type'            => 'select',
				'options'         => array(
					''        => esc_html__( 'background-attachement', 'noor' ),
					'fixed'   => esc_html__( 'Fixed', 'noor' ),
					'scroll'  => esc_html__( 'Scroll', 'noor' ),
					'inherit' => esc_html__( 'Inherit', 'noor' ),
				),
				'default'         => '',
			),

			array(
				'name'            => esc_html__( 'Website Background Position', 'noor' ),
				'id'              => '_dima_meta_body_background_image_position',
				'label_cb'        => 'dima_meta_add_tooltip_to_label',
				'tooltip_options' => array(
					'tooltip-class' => '',
					'desc'          => '',
				),
				'type'            => 'select',
				'options'         => array(
					''              => esc_html__( 'background-position', 'noor' ),
					'left top'      => esc_html__( 'Left Top', 'noor' ),
					'left center'   => esc_html__( 'Left Center', 'noor' ),
					'left bottom'   => esc_html__( 'Left Bottom', 'noor' ),
					'center top'    => esc_html__( 'Center Top', 'noor' ),
					'center center' => esc_html__( 'Center Center', 'noor' ),
					'center bottom' => esc_html__( 'Center Bottom', 'noor' ),
					'right top'     => esc_html__( 'Right Top', 'noor' ),
					'right center'  => esc_html__( 'Right Center', 'noor' ),
					'right bottom'  => esc_html__( 'Right Bottom', 'noor' ),
				),
				'default'         => '',
			),

			array(
				'name'            => esc_html__( 'Website Background Size', 'noor' ),
				'id'              => '_dima_meta_body_background_image_size',
				'label_cb'        => 'dima_meta_add_tooltip_to_label',
				'tooltip_options' => array(
					'tooltip-class' => '',
					'desc'          => wp_kses( __( "Define the background size (Default value is 'cover'). <a href='http://www.w3schools.com/cssref/css3_pr_background-size.asp' target='_blank'>Check this for reference</a>", 'noor' ),
						array(
							'a' => array(
								'href'   => array(),
								'target' => array()
							)
						) ),
				),
				'type'            => 'text',
				'default'         => '',
			),

			array(
				'name'            => esc_html__( 'Main color', 'noor' ),
				'id'              => 'dima_meta_main_color',
				'label_cb'        => 'dima_meta_add_tooltip_to_label',
				'tooltip_options' => array(
					'tooltip-class' => '',
					'desc'          => esc_html__( 'This color will be applied to call to action elements such as buttons and hover icons. It\'s preferable to be attractive color and define the main color of your website.', 'noor' ),
				),
				'type'            => 'rgba_colorpicker',
				'default'         => '',
			),

			array(
				'name'            => esc_html__( 'Secendery Main color', 'noor' ),
				'id'              => 'dima_meta_secondary_main_color',
				'label_cb'        => 'dima_meta_add_tooltip_to_label',
				'tooltip_options' => array(
					'tooltip-class' => '',
					'desc'          => esc_html__( 'This color will be applied to other elements and it\'s preferable to be like your headings color or a dark color.', 'noor' ),
				),
				'type'            => 'rgba_colorpicker',
				'default'         => '',
			),

			array(
				'name'            => esc_html__( 'Pagination background color', 'noor' ),
				'id'              => '_dima_meta_pagination_bg_color',
				'label_cb'        => 'dima_meta_add_tooltip_to_label',
				'tooltip_options' => array(
					'tooltip-class' => '',
					'desc'          => esc_html__( 'This color will be applied to other elements and it\'s preferable to be like your headings color or a dark color.', 'noor' ),
				),
				'type'            => 'rgba_colorpicker',
				'default'         => '',
			),


		);

		return $output;
	}

	static function dima_get_meta_footer_settings() {
		$output = array(
			array(
				'name' => esc_html__( 'General Settings', 'noor' ),
				'id'   => '_dima_meta_footer_general',
				'type' => 'title',
			),

			array(
				'name'            => esc_html__( 'Full Width', 'noor' ),
				'id'              => '_dima_meta_footer_full_width',
				'label_cb'        => 'dima_meta_add_tooltip_to_label',
				'tooltip_options' => array(
					'tooltip-class' => '',
					'desc'          => '',
				),
				'type'            => 'dima_buttonset',
				'options'         => array(
					'inherit' => esc_html__( 'Inherit', 'noor' ),
					'on'      => esc_html__( 'On', 'noor' ),
					'off'     => esc_html__( 'Off', 'noor' )
				),
				'default'         => 'inherit',
			),

			array(
				'name'            => esc_html__( 'Footer Parallax', 'noor' ),
				'id'              => '_dima_meta_footer_parallax',
				'label_cb'        => 'dima_meta_add_tooltip_to_label',
				'tooltip_options' => array(
					'tooltip-class' => '',
					'desc'          => '',
				),
				'type'            => 'dima_buttonset',
				'options'         => array(
					'inherit' => esc_html__( 'Inherit', 'noor' ),
					'on'      => esc_html__( 'On', 'noor' ),
					'off'     => esc_html__( 'Off', 'noor' )
				),
				'default'         => 'inherit',
			),

			array(
				'name'            => esc_html__( 'Big Footer', 'noor' ),
				'id'              => '_dima_meta_footer_big',
				'label_cb'        => 'dima_meta_add_tooltip_to_label',
				'tooltip_options' => array(
					'tooltip-class' => '',
					'desc'          => esc_html__( 'Enable or disable big footer', 'noor' ),
				),
				'type'            => 'dima_buttonset',
				'options'         => array(
					'inherit' => esc_html__( 'Inherit', 'noor' ),
					'on'      => esc_html__( 'Enable', 'noor' ),
					'off'     => esc_html__( 'Disable', 'noor' )
				),
				'default'         => 'inherit',
			),

			array(
				'name'            => esc_html__( 'Small Footer', 'noor' ),
				'id'              => '_dima_meta_small_footer_on_off',
				'label_cb'        => 'dima_meta_add_tooltip_to_label',
				'tooltip_options' => array(
					'tooltip-class' => '',
					'desc'          => '',
				),
				'type'            => 'dima_buttonset',
				'options'         => array(
					'inherit' => esc_html__( 'Inherit', 'noor' ),
					'on'      => esc_html__( 'On', 'noor' ),
					'off'     => esc_html__( 'Off', 'noor' )
				),
				'default'         => 'inherit',
			),

			array(
				'name'            => esc_html__( 'Footer Is dark', 'noor' ),
				'id'              => '_dima_meta_footer_is_dark',
				'label_cb'        => 'dima_meta_add_tooltip_to_label',
				'tooltip_options' => array(
					'tooltip-class' => '',
					'desc'          => '',
				),
				'type'            => 'dima_buttonset',
				'options'         => array(
					'inherit' => esc_html__( 'Inherit', 'noor' ),
					'on'      => esc_html__( 'On', 'noor' ),
					'off'     => esc_html__( 'Off', 'noor' )
				),
				'default'         => 'inherit',
			),

			array(
				'name'            => esc_html__( 'Footer Background Color', 'noor' ),
				'id'              => '_dima_meta_footer_content_top_bg',
				'label_cb'        => 'dima_meta_add_tooltip_to_label',
				'tooltip_options' => array(
					'tooltip-class' => '',
					'desc'          => '',
				),
				'type'            => 'rgba_colorpicker',
				'default'         => '',
			),

			array(
				'name'            => esc_html__( 'Footer Background Image', 'noor' ),
				'id'              => '_dima_meta_footer_bg_image',
				'label_cb'        => 'dima_meta_add_tooltip_to_label',
				'tooltip_options' => array(
					'tooltip-class' => '',
					'desc'          => '',
				),
				'type'            => 'file',
				'text'            => array(
					'add_upload_file_text' => esc_html__( 'Add or Upload File', 'noor' ),
				),
				'default'         => '',
			),

			/**
			 * Featured Area
			 */

			array(
				'name' => esc_html__( 'Featured Area', 'noor' ),
				'id'   => '_dima_meta_footer_featured',
				'type' => 'title',
			),

			array(
				'name'            => esc_html__( 'Featured Areas', 'noor' ),
				'id'              => '_dima_meta_footer_featured_area',
				'label_cb'        => 'dima_meta_add_tooltip_to_label',
				'tooltip_options' => array(
					'tooltip-class' => '',
					'desc'          => '',
				),
				'type'            => 'dima_buttonset',
				'options'         => array(
					'inherit' => esc_html__( 'Inherit', 'noor' ),
					'on'      => esc_html__( 'On', 'noor' ),
					'off'     => esc_html__( 'Off', 'noor' )
				),
				'default'         => 'inherit',
			),


			array(
				'name'            => esc_html__( 'Featured Background Color', 'noor' ),
				'id'              => '_dima_meta_footer_featured_top_bg',
				'label_cb'        => 'dima_meta_add_tooltip_to_label',
				'tooltip_options' => array(
					'tooltip-class' => '',
					'desc'          => '',
				),
				'type'            => 'rgba_colorpicker',
				'default'         => '',
			),

			array(
				'name'            => esc_html__( 'Featured Border Color', 'noor' ),
				'id'              => '_dima_meta_footer_featured_border_color',
				'label_cb'        => 'dima_meta_add_tooltip_to_label',
				'tooltip_options' => array(
					'tooltip-class' => '',
					'desc'          => '',
				),
				'type'            => 'rgba_colorpicker',
				'default'         => '',
			),

			array(
				'name'            => esc_html__( 'Featured Background Image', 'noor' ),
				'id'              => '_dima_meta_footer_featured_bg_image',
				'label_cb'        => 'dima_meta_add_tooltip_to_label',
				'tooltip_options' => array(
					'tooltip-class' => '',
					'desc'          => '',
				),
				'type'            => 'file',
				'text'            => array(
					'add_upload_file_text' => esc_html__( 'Add or Upload File', 'noor' ),
				),
				'default'         => '',
			),

			/**
			 * General Widgets
			 */

			array(
				'name' => esc_html__( 'General Widgets', 'noor' ),
				'id'   => '_dima_meta_footer_widgets',
				'type' => 'title',
			),

			array(
				'name'            => esc_html__( 'Footer Widget Areas', 'noor' ),
				'id'              => '_dima_meta_footer_widget_areas',
				'label_cb'        => 'dima_meta_add_tooltip_to_label',
				'tooltip_options' => array(
					'tooltip-class' => '',
					'desc'          => '',
				),
				'type'            => 'dima_image_select',
				'options'         => array(
					'inherit'               => array(
						'title' => esc_html__( 'Inherit', 'noor' ),
						'alt'   => 'Inherit',
						'img'   => self::$image_path . 'img/inherit.png'
					),
					'footer-1c'             => array(
						'title' => '',
						'alt'   => 'footer-1c',
						'img'   => self::$image_path . 'img/footer-1c.png'
					),
					'footer-2c'             => array(
						'title' => '',
						'alt'   => 'footer-2c',
						'img'   => self::$image_path . 'img/footer-2c.png'
					),
					'footer-2c-narrow-wide' => array(
						'title' => '',
						'alt'   => 'footer-2c-narrow-wide',
						'img'   => self::$image_path . 'img/footer-2c-narrow-wide.png'
					),
					'footer-2c-wide-narrow' => array(
						'title' => '',
						'alt'   => 'footer-2c-wide-narrow',
						'img'   => self::$image_path . 'img/footer-2c-wide-narrow.png'
					),
					'footer-3c'             => array(
						'title' => '',
						'alt'   => 'footer-3c',
						'img'   => self::$image_path . 'img/footer-3c.png'
					),
					'footer-3c-wide-left'   => array(
						'title' => '',
						'alt'   => 'footer-3c-wide-left',
						'img'   => self::$image_path . 'img/footer-3c-wide-left.png'
					),
					'footer-3c-wide-right'  => array(
						'title' => '',
						'alt'   => 'footer-3c-wide-right',
						'img'   => self::$image_path . 'img/footer-3c-wide-right.png'
					),
					'footer-4c'             => array(
						'title' => '',
						'alt'   => 'footer-4c',
						'img'   => self::$image_path . 'img/footer-4c.png'
					),
				),
				'default'         => 'inherit',
			),

			array(
				'name'            => esc_html__( 'Second Footer Widgets Area', 'noor' ),
				'id'              => '_dima_meta_second_footer_on_off',
				'label_cb'        => 'dima_meta_add_tooltip_to_label',
				'tooltip_options' => array(
					'tooltip-class' => '',
					'desc'          => '',
				),
				'type'            => 'dima_buttonset',
				'options'         => array(
					'inherit' => esc_html__( 'Inherit', 'noor' ),
					'on'      => esc_html__( 'On', 'noor' ),
					'off'     => esc_html__( 'Off', 'noor' )
				),
				'default'         => 'inherit',
			),

			array(
				'name'            => esc_html__( 'Second Footer Widget Areas', 'noor' ),
				'id'              => '_dima_meta_second_footer_widget_areas',
				'label_cb'        => 'dima_meta_add_tooltip_to_label',
				'tooltip_options' => array(
					'tooltip-class' => '',
					'desc'          => '',
				),
				'type'            => 'dima_image_select',
				'options'         => array(
					'inherit'               => array(
						'title' => esc_html__( 'Inherit', 'noor' ),
						'alt'   => 'Inherit',
						'img'   => self::$image_path . 'img/inherit.png'
					),
					'footer-1c'             => array(
						'title' => '',
						'alt'   => 'footer-1c',
						'img'   => self::$image_path . 'img/footer-1c.png'
					),
					'footer-2c'             => array(
						'title' => '',
						'alt'   => 'footer-2c',
						'img'   => self::$image_path . 'img/footer-2c.png'
					),
					'footer-2c-narrow-wide' => array(
						'title' => '',
						'alt'   => 'footer-2c-narrow-wide',
						'img'   => self::$image_path . 'img/footer-2c-narrow-wide.png'
					),
					'footer-2c-wide-narrow' => array(
						'title' => '',
						'alt'   => 'footer-2c-wide-narrow',
						'img'   => self::$image_path . 'img/footer-2c-wide-narrow.png'
					),
					'footer-3c'             => array(
						'title' => '',
						'alt'   => 'footer-3c',
						'img'   => self::$image_path . 'img/footer-3c.png'
					),
					'footer-3c-wide-left'   => array(
						'title' => '',
						'alt'   => 'footer-3c-wide-left',
						'img'   => self::$image_path . 'img/footer-3c-wide-left.png'
					),
					'footer-3c-wide-right'  => array(
						'title' => '',
						'alt'   => 'footer-3c-wide-right',
						'img'   => self::$image_path . 'img/footer-3c-wide-right.png'
					),
					'footer-4c'             => array(
						'title' => '',
						'alt'   => 'footer-4c',
						'img'   => self::$image_path . 'img/footer-4c.png'
					),
				),
				'attributes'      => array(
					'data-conditional-value' => wp_json_encode( array( '1', 'on', 'inherit' ) ),
					'data-conditional-id'    => '_dima_meta_second_footer_on_off',
				),
				'default'         => 'inherit',
			),

			array(
				'name'            => esc_html__( 'Widget Header Color', 'noor' ),
				'id'              => '_dima_meta_footer_widget_header_color',
				'label_cb'        => 'dima_meta_add_tooltip_to_label',
				'tooltip_options' => array(
					'tooltip-class' => '',
					'desc'          => '',
				),
				'type'            => 'rgba_colorpicker',
				'default'         => '',
			),

			array(
				'name'            => esc_html__( 'Widget Text Color', 'noor' ),
				'id'              => '_dima_meta_footer_widget_body_color',
				'label_cb'        => 'dima_meta_add_tooltip_to_label',
				'tooltip_options' => array(
					'tooltip-class' => '',
					'desc'          => '',
				),
				'type'            => 'rgba_colorpicker',
				'default'         => '',
			),

			array(
				'name'            => esc_html__( 'Widget Link Color', 'noor' ),
				'id'              => '_dima_meta_footer_widget_link_color',
				'label_cb'        => 'dima_meta_add_tooltip_to_label',
				'tooltip_options' => array(
					'tooltip-class' => '',
					'desc'          => '',
				),
				'type'            => 'rgba_colorpicker',
				'default'         => '',
			),

			array(
				'name'            => esc_html__( 'Widget Hover Link Color', 'noor' ),
				'id'              => '_dima_meta_footer_widget_link_hover_color',
				'label_cb'        => 'dima_meta_add_tooltip_to_label',
				'tooltip_options' => array(
					'tooltip-class' => '',
					'desc'          => '',
				),
				'type'            => 'rgba_colorpicker',
				'default'         => '',
			),

			array(
				'name'            => esc_html__( 'Widget Border Color', 'noor' ),
				'id'              => '_dima_meta_footer_widget_border_color',
				'label_cb'        => 'dima_meta_add_tooltip_to_label',
				'tooltip_options' => array(
					'tooltip-class' => '',
					'desc'          => '',
				),
				'type'            => 'rgba_colorpicker',
				'default'         => '',
			),


		);

		return $output;
	}

	static function dima_get_meta_woo_settings() {
		$output = array(
			array(
				'name' => esc_html__( 'WooCommerce', 'noor' ),
				'id'   => '_dima_meta_menu_woo',
				'type' => 'title',
			),

			array(
				'name'    => esc_html__( 'Cart', 'noor' ),
				'id'      => '_dima_meta_shop_menu',
				'type'    => 'dima_buttonset',
				'options' => array(
					'inherit' => esc_html__( 'Inherit', 'noor' ),
					'on'      => esc_html__( 'On', 'noor' ),
					'off'     => esc_html__( 'Off', 'noor' )
				),
				'default' => 'inherit',
			),

			array(
				'name'    => esc_html__( 'Cart Dropdown', 'noor' ),
				'id'      => '_dima_meta_shop_sub_menu',
				'type'    => 'dima_buttonset',
				'options' => array(
					'inherit' => esc_html__( 'Inherit', 'noor' ),
					'on'      => esc_html__( 'On', 'noor' ),
					'off'     => esc_html__( 'Off', 'noor' )
				),
				'default' => 'inherit',
			),

			array(
				'name'    => esc_html__( 'My Account', 'noor' ),
				'id'      => '_dima_meta_navbar_option_myaccount_display_topbar',
				'type'    => 'dima_buttonset',
				'options' => array(
					'inherit' => esc_html__( 'Inherit', 'noor' ),
					'on'      => esc_html__( 'On', 'noor' ),
					'off'     => esc_html__( 'Off', 'noor' )
				),
				'default' => 'inherit',
			),

			array(
				'name'    => esc_html__( 'Wishlist', 'noor' ),
				'id'      => '_dima_meta_navbar_option_wishlist',
				'type'    => 'dima_buttonset',
				'options' => array(
					'inherit' => esc_html__( 'Inherit', 'noor' ),
					'on'      => esc_html__( 'On', 'noor' ),
					'off'     => esc_html__( 'Off', 'noor' )
				),
				'default' => 'inherit',
			),

		);

		return $output;
	}

}


function dima_meta_add_tooltip_to_label( $field_args, $field ) {
	if ( $label = $field->label() && $field->tooltip_options( 'desc' ) != '' ) {
		$label = '<span class="tippy dima-help ' . esc_attr( $field->tooltip_options( 'tooltip-class' ) ) . '" title="' . esc_html( $field->tooltip_options( 'desc' ) ) . '">' . dima_get_svg_icon( "ic_help" ) . '</span>';
		$label .= '<span>' . wp_kses( $field->label(), dima_helper::dima_get_allowed_html_tag() ) . '</span>';
	} else {
		$label = $field->label();
	}
	echo wp_kses( $label, dima_helper::dima_get_allowed_html_tag() );
}

add_filter( 'cmb2_admin_init', 'dima_sidebar_metaboxes' );

function dima_sidebar_metaboxes() {
	$object_types = array( 'post', 'page', 'dima-portfolio', 'dimablock' );

	$box_options = array(
		'id'           => 'dima-sidebar-meta-box-fullwidth',
		'title'        => esc_html__( 'Fullwidth Section', 'noor' ),
		'object_types' => $object_types,
		'show_names'   => true,
		'context'      => 'side',
		'priority'     => 'high',
		'cmb_styles'   => false,
	);

	// Setup meta box
	$cmb = new_cmb2_box( $box_options );
	$cmb->add_field(
		array(
			'name'            => esc_html__( 'Fullwidth', 'noor' ),
			'id'              => '_dima_meta_section_fullwidth',
			'type'            => 'dima_toggle',
			'label_cb'        => 'dima_meta_add_tooltip_to_label',
			'tooltip_options' => array(
				'tooltip-class' => '',
				'desc'          => esc_html__( 'Check to apply fullwidth sections to this page (by checking this option, the sidebar will disappear)', 'noor' ),
			),
		)
	);
}

/*-----------------*/

add_filter( 'cmb2_admin_init', 'dima_sidebar_post_metaboxes' );

function dima_sidebar_post_metaboxes() {
	$object_types = array( 'post' );

	$box_options = array(
		'id'           => 'dima-sidebar-meta-box-custom-post',
		'title'        => esc_html__( 'Custom Post', 'noor' ),
		'object_types' => $object_types,
		'show_names'   => true,
		'context'      => 'side',
		'priority'     => 'high',
		'cmb_styles'   => false,
	);

	// Setup meta box
	$cmb = new_cmb2_box( $box_options );
	$cmb->add_field(
		array(
			'name'            => esc_html__( 'Hide Featured Image', 'noor' ),
			'id'              => '_dima_post_featured_Image',
			'type'            => 'dima_toggle',
			'label_cb'        => 'dima_meta_add_tooltip_to_label',
			'tooltip_options' => array(
				'tooltip-class' => '',
				'desc'          => esc_html__( 'Disabling this option will remove the featured image from single post only (Will not affect post lists, masonry and grid styles)', 'noor' ),
			),
		)
	);
}

/*-----------------*/

/*-----------------*/

add_filter( 'cmb2_admin_init', 'dima_sidebar_gallery_metaboxes' );

function dima_sidebar_gallery_metaboxes() {
	$object_types = array( 'post' );

	$box_options = array(
		'id'           => 'dima-sidebar-meta-box-gallery-post',
		'title'        => esc_html__( 'Post Gallery', 'noor' ),
		'object_types' => $object_types,
		'show_names'   => true,
		'context'      => 'side',
		'priority'     => 'default',
		'cmb_styles'   => false,
	);

	// Setup meta box
	$cmb = new_cmb2_box( $box_options );
	$cmb->add_field(
		array(
			'name' => esc_html__( 'Post Gallery', 'noor' ),
			'id'   => '_post_image_gallery',
			'type' => 'file_list',
			'text' => array(
				'add_upload_files_text' => esc_html__( 'Add gallery images', 'noor' ),
				'remove_image_text'     => esc_html__( 'Remove', 'noor' ),
				'file_text'             => esc_html__( 'File', 'noor' ),
				'file_download_text'    => esc_html__( 'Download', 'noor' ),
				'remove_text'           => esc_html__( 'Remove', 'noor' ),
			),
		)
	);
}

$_dima_meta_data = new dima_meta_data();

/*-----------------*/
require_once( 'object_types/object_pages.php' );
require_once( 'object_types/object_portfolio.php' );
require_once( 'object_types/object_post.php' );
require_once( 'object_types/object_product.php' );
