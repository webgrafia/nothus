<?php
add_filter( 'cmb2_init', 'dima_page_metaboxes' );
function dima_page_metaboxes() {
	$object_types = array( 'page' );

	$box_options = array(
		'id'           => 'dima-meta-box-page',
		'title'        => esc_html__( 'Page Settings', 'noor' ),
		'description'  => esc_html__( 'Here you will find various options you can use to create different page styles.', 'noor' ),
		'object_types' => $object_types,
		'show_names'   => true,
		'context'      => 'normal',
		'priority'     => 'default',
		'cmb_styles'   => false,
	);

	// Setup meta box
	$cmb = new_cmb2_box( $box_options );

	// setting tabs
	$tabs_setting = array(
		'config' => $box_options,
		'layout' => 'vertical', // Default : horizontal
		'tabs'   => array()
	);
	/*------------------- * General Tabs * -----------------------*/
	$general_tab = dima_meta_data::dima_get_meta_general_settings();
	/*------------------- * !General Tabs * -----------------------*/

	/*--------------- Header Tabs  ---------------*/
	$header_tabs = dima_meta_data::dima_get_meta_header_settings();
	/*--------------- Header Tabs ----------------*/

	/*--------------  Menu Tabs * ------------*/
	$menu_tabs = dima_meta_data::dima_get_meta_menu_settings();
	/*-------------- !Menu Tabs -------------*/

	/*--------------- Logo Tabs  ---------------*/
	$logo_tabs = dima_meta_data::dima_get_meta_logo_settings();
	/*--------------- Logo Tabs ----------------*/

	/*--------------- Woo Tabs  ---------------*/
	$woo_tabs = dima_meta_data::dima_get_meta_woo_settings();
	/*--------------- Woo Tabs ----------------*/

	/*------------------- * Footer Tabs * -----------------------*/
	$footer_tab = dima_meta_data::dima_get_meta_footer_settings();
	/*------------------- * !Footer Tabs * -----------------------*/


	$tabs_setting['tabs'][] = array(
		'id'     => 'meta_tab_general',
		'title'  => esc_html__( 'General', 'noor' ),
		'svg'    => 'ic_settings',
		'fields' => $general_tab
	);

	$tabs_setting['tabs'][] = array(
		'id'     => 'meta_tab_header',
		'title'  => esc_html__( 'Header', 'noor' ),
		'svg'    => 'ic_web_asset',
		'fields' => $header_tabs
	);

	$tabs_setting['tabs'][] = array(
		'id'     => 'meta_tab_menu',
		'title'  => esc_html__( 'Menu', 'noor' ),
		'svg'    => 'ic_menu',
		'fields' => $menu_tabs
	);

	$tabs_setting['tabs'][] = array(
		'id'     => 'meta_tab_logo',
		'title'  => esc_html__( 'Logo', 'noor' ),
		'svg'    => 'ic_image',
		'fields' => $logo_tabs
	);

	$tabs_setting['tabs'][] = array(
		'id'     => 'meta_tab_woo',
		'title'  => esc_html__( 'Shop', 'noor' ),
		'svg'    => 'ic_store',
		'fields' => $woo_tabs
	);

	$tabs_setting['tabs'][] = array(
		'id'     => 'meta_tab_footer',
		'title'  => esc_html__( 'Footer', 'noor' ),
		'svg'    => 'ic_call_to_action',
		'fields' => $footer_tab
	);

	if ( DIMA_REVOLUTION_SLIDER_IS_ACTIVE ) {
		$slider_settings_tab    = dima_meta_data::dima_get_meta_slider_settings();
		$tabs_setting['tabs'][] = array(
			'id'     => 'meta_tab_slider_settings',
			'title'  => esc_html__( 'Slider Settings', 'noor' ),
			'svg'    => 'ic_view_carousel',
			'fields' => $slider_settings_tab,
		);
	}

	// set tabs
	$cmb->add_field( array(
		'id'   => '__tabs',
		'type' => 'tabs',
		'tabs' => $tabs_setting
	) );
}