<?php
add_filter( 'cmb2_init', 'dima_product_metaboxes' );
function dima_product_metaboxes() {
	$object_types = array( 'product' );
	$box_options  = array(
		'id'           => 'dima-meta-box-product',
		'title'        => esc_html__( 'Product Settings', 'noor' ),
		'description'  => esc_html__( 'Here you will find various options you can use to create different product styles.', 'noor' ),
		'object_types' => $object_types,
		'show_names'   => true,
		'context'      => 'normal',
		'priority'     => 'default',
		'cmb_styles'   => false,
	);

	// Setup meta box
	$cmb = new_cmb2_box( $box_options );

	// setting tabs
	$tabs_setting = array(
		'config' => $box_options,
		'layout' => 'vertical', // Default : horizontal
		'tabs'   => array()
	);
	/*------------------- * General Tabs * -----------------------*/
	$general_tab = dima_meta_data::dima_get_meta_general_settings();

	/*------------------- * !General Tabs * -----------------------*/

	/*--------------- Header Tabs  ---------------*/
	$header_tabs = dima_meta_data::dima_get_meta_header_settings();
	/*--------------- Header Tabs ----------------*/

	/*--------------  Menu Tabs * ------------*/
	$menu_tabs = dima_meta_data::dima_get_meta_menu_settings();
	/*-------------- !Menu Tabs -------------*/

	/*--------------- Logo Tabs  ---------------*/
	$logo_tabs = dima_meta_data::dima_get_meta_logo_settings();
	/*--------------- Logo Tabs ----------------*/

	$tabs_setting['tabs'][] = array(
		'id'     => 'meta_tab_general',
		'title'  => esc_html__( 'General', 'noor' ),
		'svg'    => 'ic_settings',
		'fields' => $general_tab
	);

	$tabs_setting['tabs'][] = array(
		'id'     => 'meta_tab_header',
		'title'  => esc_html__( 'Header', 'noor' ),
		'svg'    => 'ic_web_asset',
		'fields' => $header_tabs
	);

	$tabs_setting['tabs'][] = array(
		'id'     => 'meta_tab_menu',
		'title'  => esc_html__( 'Menu', 'noor' ),
		'svg'    => 'ic_menu',
		'fields' => $menu_tabs
	);

	$tabs_setting['tabs'][] = array(
		'id'     => 'meta_tab_logo',
		'title'  => esc_html__( 'Logo', 'noor' ),
		'svg'    => 'ic_image',
		'fields' => $logo_tabs
	);

	if ( DIMA_REVOLUTION_SLIDER_IS_ACTIVE ) {
		$slider_settings_tab = dima_meta_data::dima_get_meta_slider_settings();
		$tabs_setting['tabs'][] = array(
			'id'     => 'meta_tab_slider_settings',
			'title'  => esc_html__( 'Slider Settings', 'noor' ),
			'svg'    => 'ic_view_carousel',
			'fields' => $slider_settings_tab,
		);
	}

	// set tabs
	$cmb->add_field( array(
		'id'   => '__tabs',
		'type' => 'tabs',
		'tabs' => $tabs_setting
	) );
}