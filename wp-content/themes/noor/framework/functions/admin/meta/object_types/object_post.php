<?php
add_filter( 'cmb2_admin_init', 'dima_post_metaboxes' );
function dima_post_metaboxes() {
	$object_types = array( 'post' );

	$box_options = array(
		'id'           => 'dima-meta-box-post',
		'title'        => esc_html__( 'Post Settings', 'noor' ),
		'description'  => esc_html__( 'Here you will find various options you can use to create different page styles.', 'noor' ),
		'object_types' => $object_types,
		'show_names'   => true,
		'context'      => 'normal',
		'priority'     => 'high',
		'cmb_styles'   => false,
	);

	// Setup meta box
	$cmb = new_cmb2_box( $box_options );

	// setting tabs
	$tabs_setting = array(
		'config' => $box_options,
		'layout' => 'vertical', // Default : horizontal
		'tabs'   => array()
	);
	/*------------------- * General Tabs * -----------------------*/
	$general_tab = dima_meta_data::dima_get_meta_general_settings();
	/*------------------- * !General Tabs * -----------------------*/

	/*--------------- Header Tabs  ---------------*/
	$header_tabs = dima_meta_data::dima_get_meta_header_settings();
	/*--------------- Header Tabs ----------------*/

	/*--------------  Menu Tabs * ------------*/
	$menu_tabs = dima_meta_data::dima_get_meta_menu_settings();
	/*-------------- !Menu Tabs -------------*/

	/*--------------- Logo Tabs  ---------------*/
	$logo_tabs = dima_meta_data::dima_get_meta_logo_settings();
	/*--------------- Logo Tabs ----------------*/

	/*------------------- * Footer Tabs * -----------------------*/
	$footer_tab = dima_meta_data::dima_get_meta_footer_settings();
	/*------------------- * !Footer Tabs * -----------------------*/

	/*-------------- Video Settings -------------*/
	$video_tabs = array(
		array(
			'name'    => esc_html__( 'Video source', 'noor' ),
			'id'      => '_dima_video_source',
			'type'    => 'dima_buttonset',
			'options' => array(
				'youtube'     => esc_html__( 'YouTube', 'noor' ),
				'vimeo'       => esc_html__( 'vimeo', 'noor' ),
				'embedded'    => esc_html__( 'Embedded', 'noor' ),
				'self_hosted' => esc_html__( 'Self hosted', 'noor' ),
			),
			'default' => 'youtube',
		),

		array(
			'name'            => esc_html__( 'YouTube video ID', 'noor' ),
			'id'              => '_dima_video_youtube',
			'type'            => 'text',
			'label_cb'        => 'dima_meta_add_tooltip_to_label',
			'tooltip_options' => array(
				'tooltip-class' => '',
				'desc'          => esc_html__( 'Look at the URL of the video, and at the end of it, you should see a combination of numbers and letters after an equal sign (=).', 'noor' ),
			),
			'attributes'      => array(
				'data-conditional-value' => 'youtube',
				'data-conditional-id'    => '_dima_video_source',
			),
		),

		array(
			'name'            => esc_html__( 'Vimeo video ID', 'noor' ),
			'id'              => '_dima_video_vimeo',
			'type'            => 'text',
			'label_cb'        => 'dima_meta_add_tooltip_to_label',
			'tooltip_options' => array(
				'tooltip-class' => '',
				'desc'          => esc_html__( 'Copy the numeric code that appears at the end of the video URL.', 'noor' ),
			),
			'attributes'      => array(
				'data-conditional-value' => 'vimeo',
				'data-conditional-id'    => '_dima_video_source',
			),
		),

		array(
			'name'            => esc_html__( 'M4V File URL', 'noor' ),
			'id'              => '_dima_video_m4v',
			'type'            => 'file',
			'label_cb'        => 'dima_meta_add_tooltip_to_label',
			'tooltip_options' => array(
				'tooltip-class' => '',
				'desc'          => esc_html__( 'The URL to the .m4v video file.', 'noor' ),
			),
			'attributes'      => array(
				'data-conditional-value' => 'self_hosted',
				'data-conditional-id'    => '_dima_video_source',
			),
		),

		array(
			'name'            => esc_html__( 'ogv File URL', 'noor' ),
			'id'              => '_dima_video_ogv',
			'type'            => 'file',
			'label_cb'        => 'dima_meta_add_tooltip_to_label',
			'tooltip_options' => array(
				'tooltip-class' => '',
				'desc'          => esc_html__( 'The URL to the .ogv video file.', 'noor' ),
			),
			'attributes'      => array(
				'data-conditional-value' => 'self_hosted',
				'data-conditional-id'    => '_dima_video_source',
			),
		),

		array(
			'name'       => esc_html__( 'Self hosted video file in webM format', 'noor' ),
			'id'         => '_dima_video_webm',
			'type'       => 'file',
			'attributes' => array(
				'data-conditional-value' => 'self_hosted',
				'data-conditional-id'    => '_dima_video_source',
			),
		),

		array(
			'name'            => esc_html__( 'Embedded Video Code', 'noor' ),
			'id'              => '_dima_video_embed',
			'type'            => 'textarea_code',
			'label_cb'        => 'dima_meta_add_tooltip_to_label',
			'tooltip_options' => array(
				'tooltip-class' => '',
				'desc'          => esc_html__( 'If you are using something other than self hosted video such as YouTube, Vimeo, or Wistia, paste the embed code here. This field will override the above.', 'noor' ),
			),
			'attributes'      => array(
				'data-conditional-value' => 'embedded',
				'data-conditional-id'    => '_dima_video_source',
			),
		),

		array(
			'name'            => esc_html__( 'Embedded Video URL', 'noor' ),
			'id'              => '_dima_video_embed_two',
			'type'            => 'oembed',
			'label_cb'        => 'dima_meta_add_tooltip_to_label',
			'tooltip_options' => array(
				'tooltip-class' => '',
				'desc'          => esc_html__( 'If you are using something other than self hosted video such as YouTube, Vimeo, or Wistia, paste the link here. This field will override the above.', 'noor' ),
			),
			'attributes'      => array(
				'data-conditional-value' => 'embedded',
				'data-conditional-id'    => '_dima_video_source',
			),
		),

		array(
			'name'       => esc_html__( 'Poster', 'noor' ),
			'id'         => '_dima_video_poster',
			'type'       => 'file',
			'attributes' => array(
				'data-conditional-value' => wp_json_encode( array( 'self_hosted' ) ),
				'data-conditional-id'    => '_dima_video_source',
			),
		),

		array(
			'name' => esc_html__( 'Popup', 'noor' ),
			'id'   => '_dima_video_online_title',
			'type' => 'title',
		),

		array(
			'name'            => esc_html__( 'Popup', 'noor' ),
			'id'              => '_dima_video_online',
			'type'            => 'textarea_code',
			'label_cb'        => 'dima_meta_add_tooltip_to_label',
			'tooltip_options' => array(
				'tooltip-class' => '',
				'desc'          => esc_html__( 'Add your media link ex: youtube, vimeo, ..etc, or Iframe URL. Note. This will displayed only in post list and applies only if you choose a featured image for the post.', 'noor' ),
			)
		),

	);

	/*-------------- Audio Settings -------------*/
	$audio_tabs = array(
		array(
			'name'    => esc_html__( 'Audio source', 'noor' ),
			'id'      => '_dima_audio_source',
			'type'    => 'dima_buttonset',
			'options' => array(
				'embedded'    => esc_html__( 'Embedded', 'noor' ),
				'self_hosted' => esc_html__( 'Self hosted', 'noor' ),
			),
			'default' => 'self_hosted',
		),

		array(
			'name'       => esc_html__( 'Producer(s)', 'noor' ),
			'id'         => '_dima_audio_producer',
			'type'       => 'text',
			'attributes' => array(
				'data-conditional-value' => 'self_hosted',
				'data-conditional-id'    => '_dima_audio_source',
			),
		),
		array(
			'name'       => esc_html__( 'Audio author', 'noor' ),
			'id'         => '_dima_audio_author',
			'type'       => 'text',
			'attributes' => array(
				'data-conditional-value' => 'self_hosted',
				'data-conditional-id'    => '_dima_audio_source',
			),
		),
		array(
			'name'            => esc_html__( 'MP3 File URL', 'noor' ),
			'id'              => '_dima_audio_mp3',
			'type'            => 'file',
			'label_cb'        => 'dima_meta_add_tooltip_to_label',
			'tooltip_options' => array(
				'tooltip-class' => '',
				'desc'          => esc_html__( 'The URL to the .mp3 audio file.', 'noor' ),
			),
			'attributes'      => array(
				'data-conditional-value' => 'self_hosted',
				'data-conditional-id'    => '_dima_audio_source',
			),
		),
		array(
			'name'            => esc_html__( 'OGA File URL', 'noor' ),
			'id'              => '_dima_audio_ogg',
			'type'            => 'text',
			'label_cb'        => 'dima_meta_add_tooltip_to_label',
			'tooltip_options' => array(
				'tooltip-class' => '',
				'desc'          => esc_html__( 'The URL to the .oga or .ogg audio file.', 'noor' ),
			),
			'attributes'      => array(
				'data-conditional-value' => 'self_hosted',
				'data-conditional-id'    => '_dima_audio_source',
			),
		),
		array(
			'name'            => esc_html__( 'Embedded Audio Code', 'noor' ),
			'id'              => '_dima_audio_embed',
			'label_cb'        => 'dima_meta_add_tooltip_to_label',
			'tooltip_options' => array(
				'tooltip-class' => '',
				'desc'          => esc_html__( 'If you are using something other than self hosted audio such as Soundcloud paste the embed code here. This field will override the above.', 'noor' ),
			),
			'type'            => 'textarea_code',
			'attributes'      => array(
				'data-conditional-value' => 'embedded',
				'data-conditional-id'    => '_dima_audio_source',
			),
		),
		array(
			'name'            => esc_html__( 'Embedded Audio URL', 'noor' ),
			'id'              => '_dima_audio_embed_url',
			'label_cb'        => 'dima_meta_add_tooltip_to_label',
			'tooltip_options' => array(
				'tooltip-class' => '',
				'desc'          => esc_html__( 'If you are using something other than self hosted audio such as Soundcloud paste the link here. This field will override the above.', 'noor' ),
			),
			'type'            => 'oembed',
			'attributes'      => array(
				'data-conditional-value' => 'embedded',
				'data-conditional-id'    => '_dima_audio_source',
			),
		),
	);
	/*-------------- !Audio Settings ------------*/

	/*-------------- Quote Settings -------------*/
	$quote_tabs = array(
		array(
			'name'            => esc_html__( 'The Quote', 'noor' ),
			'id'              => '_dima_quote_quote',
			'label_cb'        => 'dima_meta_add_tooltip_to_label',
			'tooltip_options' => array(
				'tooltip-class' => '',
				'desc'          => esc_html__( 'Input your quote.', 'noor' ),
			),
			'type'            => 'textarea',
		),
		array(
			'name'            => esc_html__( 'Citation', 'noor' ),
			'id'              => '_dima_quote_cite',
			'label_cb'        => 'dima_meta_add_tooltip_to_label',
			'tooltip_options' => array(
				'tooltip-class' => '',
				'desc'          => esc_html__( 'Specify who originally said the quote.', 'noor' ),
			),
			'type'            => 'text',
		),
	);
	/*-------------- !Quote Settings ------------*/

	/*-------------- Link Settings -------------*/
	$link_tabs = array(
		array(
			'name'            => esc_html__( 'URL', 'noor' ),
			'id'              => '_dima_link_url',
			'type'            => 'text_url',
			'label_cb'        => 'dima_meta_add_tooltip_to_label',
			'tooltip_options' => array(
				'tooltip-class' => '',
				'desc'          => esc_html__( 'Insert your link URL, e.g. http://www.pixeldima.com.', 'noor' ),
			),
		),
		array(
			'name' => esc_html__( 'Link text', 'noor' ),
			'id'   => '_dima_link_txt',
			'type' => 'textarea_small',
		)
	);
	/*-------------- !Quote Settings ------------*/

	/*-------------- Link Settings -------------*/
	$gallery_tabs = array(
		array(
			'name'            => esc_html__( 'Animation', 'noor' ),
			'id'              => '_dima_flex_slide_animation',
			'label_cb'        => 'dima_meta_add_tooltip_to_label',
			'tooltip_options' => array(
				'tooltip-class' => '',
				'desc'          => esc_html__( 'Controls the animation type, "fade" or "slide"', 'noor' ),
			),
			'type'            => 'dima_buttonset',
			'options'         => array(
				'slide' => esc_html__( 'Slide', 'noor' ),
				'fade'  => esc_html__( 'Fade', 'noor' ),
			),
			'default'         => 'slide',
		),
		array(
			'name'            => esc_html__( 'Speed', 'noor' ),
			'label_cb'        => 'dima_meta_add_tooltip_to_label',
			'tooltip_options' => array(
				'tooltip-class' => '',
				'desc'          => esc_html__( 'Set the speed of the slideshow cycling, in milliseconds', 'noor' ),
			),
			'id'              => '_dima_flex_slide_show_speed',
			'type'            => 'text',
			'default'         => '500'
		),
		array(
			'name'            => esc_html__( 'Auto play speed', 'noor' ),
			'label_cb'        => 'dima_meta_add_tooltip_to_label',
			'tooltip_options' => array(
				'tooltip-class' => '',
				'desc'          => esc_html__( 'Set the auto play speed, in milliseconds.', 'noor' ),
			),
			'id'              => '_dima_flex_slide_animation_speed',
			'type'            => 'text',
			'default'         => '3000'
		),
		array(
			'name'            => esc_html__( 'Display navigation dots ', 'noor' ),
			'label_cb'        => 'dima_meta_add_tooltip_to_label',
			'tooltip_options' => array(
				'tooltip-class' => '',
				'desc'          => esc_html__( 'Choose to show or hide the navigation dots.', 'noor' ),
			),
			'id'              => '_dima_flex_slide_control_nav',
			'type'            => 'dima_buttonset',
			'options'         => array(
				'on'  => esc_html__( 'On', 'noor' ),
				'off' => esc_html__( 'Off', 'noor' ),
			),
			'default'         => 'off',


		),
	);
	/*-------------- !Quote Settings ------------*/

	/*-------------- Blog ------------*/
	$blog_tabs = array(
		array(
			'name'            => esc_html__( 'Display Previous/Next Pagination', 'noor' ),
			'id'              => '_dima_meta_pagination_post',
			'label_cb'        => 'dima_meta_add_tooltip_to_label',
			'tooltip_options' => array(
				'tooltip-class' => '',
				'desc'          => '',
			),
			'type'            => 'dima_buttonset',
			'options'         => array(
				'inherit' => esc_html__( 'Inherit', 'noor' ),
				'on'      => esc_html__( 'On', 'noor' ),
				'off'     => esc_html__( 'Off', 'noor' )
			),
			'default'         => 'inherit',

		),

		array(
			'name'            => esc_html__( 'Second Title', 'noor' ),
			'id'              => '_dima_meta_second_title_on_post',
			'label_cb'        => 'dima_meta_add_tooltip_to_label',
			'tooltip_options' => array(
				'tooltip-class' => '',
				'desc'          => '',
			),
			'type'            => 'dima_buttonset',
			'options'         => array(
				'inherit' => esc_html__( 'Inherit', 'noor' ),
				'on'      => esc_html__( 'On', 'noor' ),
				'off'     => esc_html__( 'Off', 'noor' )
			),
			'default'         => 'inherit',

		),
		array(
			'name'            => esc_html__( 'Display Social Share Icons', 'noor' ),
			'id'              => '_dima_meta_shear_icons_post',
			'label_cb'        => 'dima_meta_add_tooltip_to_label',
			'tooltip_options' => array(
				'tooltip-class' => '',
				'desc'          => '',
			),
			'type'            => 'dima_buttonset',
			'options'         => array(
				'inherit' => esc_html__( 'Inherit', 'noor' ),
				'on'      => esc_html__( 'On', 'noor' ),
				'off'     => esc_html__( 'Off', 'noor' )
			),
			'default'         => 'inherit',

		),
		array(
			'name'            => esc_html__( 'Display Author Box', 'noor' ),
			'id'              => '_dima_meta_author_post',
			'label_cb'        => 'dima_meta_add_tooltip_to_label',
			'tooltip_options' => array(
				'tooltip-class' => '',
				'desc'          => '',
			),
			'type'            => 'dima_buttonset',
			'options'         => array(
				'inherit' => esc_html__( 'Inherit', 'noor' ),
				'on'      => esc_html__( 'On', 'noor' ),
				'off'     => esc_html__( 'Off', 'noor' )
			),
			'default'         => 'inherit',

		),

		array(
			'name'            => esc_html__( 'Display Related Posts', 'noor' ),
			'id'              => '_dima_meta_post_related_display',
			'label_cb'        => 'dima_meta_add_tooltip_to_label',
			'tooltip_options' => array(
				'tooltip-class' => '',
				'desc'          => '',
			),
			'type'            => 'dima_buttonset',
			'options'         => array(
				'inherit' => esc_html__( 'Inherit', 'noor' ),
				'on'      => esc_html__( 'On', 'noor' ),
				'off'     => esc_html__( 'Off', 'noor' )
			),
			'default'         => 'inherit',

		),

		array(
			'name'            => esc_html__( 'Related Posts Columns', 'noor' ),
			'id'              => '_dima_meta_post_related_columns',
			'label_cb'        => 'dima_meta_add_tooltip_to_label',
			'tooltip_options' => array(
				'tooltip-class' => '',
				'desc'          => '',
			),
			'type'            => 'dima_buttonset',
			'options'         => array(
				'inherit' => esc_html__( 'Inherit', 'noor' ),
				2         => esc_html__( 'Two', 'noor' ),
				3         => esc_html__( 'Three', 'noor' ),
				4         => esc_html__( 'Four', 'noor' )
			),
			'default'         => 'inherit',

		),

	);
	/*-------------- !Blog ------------*/

	$tabs_setting['tabs'][] = array(
		'id'     => 'meta_tab_general_post',
		'title'  => esc_html__( 'General', 'noor' ),
		'svg'    => 'ic_chrome_reader_mode',
		'fields' => $general_tab
	);

	$tabs_setting['tabs'][] = array(
		'id'     => 'meta_tab_general_blog',
		'title'  => esc_html__( 'Blog details', 'noor' ),
		'svg'    => 'ic_space_bar',
		'fields' => $blog_tabs
	);

	$tabs_setting['tabs'][] = array(
		'id'     => 'meta_tab_header',
		'title'  => esc_html__( 'Header', 'noor' ),
		'svg'    => 'ic_web_asset',
		'fields' => $header_tabs
	);

	$tabs_setting['tabs'][] = array(
		'id'     => 'meta_tab_menu',
		'title'  => esc_html__( 'Menu', 'noor' ),
		'svg'    => 'ic_menu',
		'fields' => $menu_tabs
	);

	$tabs_setting['tabs'][] = array(
		'id'     => 'meta_tab_logo',
		'title'  => esc_html__( 'Logo', 'noor' ),
		'svg'    => 'ic_image',
		'fields' => $logo_tabs
	);

	$tabs_setting['tabs'][] = array(
		'id'     => 'meta_tab_video',
		'title'  => esc_html__( 'Video Post', 'noor' ),
		'svg'    => 'ic_video_library',
		'fields' => $video_tabs
	);

	$tabs_setting['tabs'][] = array(
		'id'     => 'meta_tab_audio',
		'title'  => esc_html__( 'Audio Post', 'noor' ),
		'svg'    => 'ic_audiotrack',
		'fields' => $audio_tabs
	);

	$tabs_setting['tabs'][] = array(
		'id'     => 'meta_tab_quote',
		'title'  => esc_html__( 'Quote Post', 'noor' ),
		'svg'    => 'ic_format_quote',
		'fields' => $quote_tabs
	);

	$tabs_setting['tabs'][] = array(
		'id'     => 'meta_tab_link',
		'title'  => esc_html__( 'Link Post', 'noor' ),
		'svg'    => 'ic_link',
		'fields' => $link_tabs
	);

	$tabs_setting['tabs'][] = array(
		'id'     => 'meta_tab_gallery',
		'title'  => esc_html__( 'Gallery Post', 'noor' ),
		'svg'    => 'ic_burst_mode',
		'fields' => $gallery_tabs
	);

	$tabs_setting['tabs'][] = array(
		'id'     => 'meta_tab_footer',
		'title'  => esc_html__( 'Footer', 'noor' ),
		'svg'    => 'ic_call_to_action',
		'fields' => $footer_tab
	);

	if ( DIMA_REVOLUTION_SLIDER_IS_ACTIVE ) {
		$slider_settings_tab    = dima_meta_data::dima_get_meta_slider_settings();
		$tabs_setting['tabs'][] = array(
			'id'     => 'meta_tab_slider_settings',
			'title'  => esc_html__( 'Revolution Slider', 'noor' ),
			'svg'    => 'ic_view_carousel',
			'fields' => $slider_settings_tab
		);
	}

	// set tabs
	$cmb->add_field( array(
		'id'   => '__tabs',
		'type' => 'tabs',
		'tabs' => $tabs_setting
	) );
}