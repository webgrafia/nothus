<?php

add_filter( 'cmb2_admin_init', 'dima_portfolio_metaboxes' );
function dima_portfolio_metaboxes() {
	$object_types = array( 'dima-portfolio' );
	$image_path   = DIMA_TEMPLATE_URL . '/framework/functions/admin/meta/inc/dima-meta-types/';
	$box_options  = array(
		'id'           => 'dima-meta-box-portfolio',
		'title'        => esc_html__( 'Portfolio Settings', 'noor' ),
		'description'  => esc_html__( 'Here you will find various options you can use to create different page styles.', 'noor' ),
		'object_types' => $object_types,
		'show_names'   => true,
		'cmb_styles'   => false,
		'priority'     => 'high',
		//'context'      => 'normal',
	);

	// Setup meta box
	$cmb = new_cmb2_box( $box_options );

	// setting tabs
	$tabs_setting = array(
		'config' => $box_options,
		'layout' => 'vertical', // Default : horizontal
		'tabs'   => array()
	);
	/*------------------- * General Tabs * -----------------------*/
	$general_tab = dima_meta_data::dima_get_meta_general_settings();
	/*------------------- * !General Tabs * -----------------------*/

	/*--------------- Header Tabs  ---------------*/
	$header_tabs = dima_meta_data::dima_get_meta_header_settings();
	/*--------------- Header Tabs ----------------*/

	/*--------------  Menu Tabs * ------------*/
	$menu_tabs = dima_meta_data::dima_get_meta_menu_settings();
	/*-------------- !Menu Tabs -------------*/

	/*--------------- Logo Tabs  ---------------*/
	$logo_tabs = dima_meta_data::dima_get_meta_logo_settings();
	/*--------------- Logo Tabs ----------------*/

	/*------------------- * Footer Tabs * -----------------------*/
	$footer_tab = dima_meta_data::dima_get_meta_footer_settings();
	/*------------------- * !Footer Tabs * -----------------------*/

	/*-------------- Video Settings -------------*/
	$video_tabs = array(
		array(
			'name'    => esc_html__( 'Video source', 'noor' ),
			'id'      => '_dima_video_source',
			'type'    => 'dima_buttonset',
			'options' => array(
				'youtube'     => esc_html__( 'YouTube', 'noor' ),
				'vimeo'       => esc_html__( 'vimeo', 'noor' ),
				'embedded'    => esc_html__( 'Embedded', 'noor' ),
				'self_hosted' => esc_html__( 'Self hosted', 'noor' ),
			),
			'default' => 'youtube',
		),

		array(
			'name'            => esc_html__( 'YouTube video ID', 'noor' ),
			'id'              => '_dima_video_youtube',
			'type'            => 'text',
			'label_cb'        => 'dima_meta_add_tooltip_to_label',
			'tooltip_options' => array(
				'tooltip-class' => '',
				'desc'          => esc_html__( 'Look at the URL of the video, and at the end of it, you should see a combination of numbers and letters after an equal sign (=).', 'noor' ),
			),
			'attributes'      => array(
				'data-conditional-value' => 'youtube',
				'data-conditional-id'    => '_dima_video_source',
			),
		),

		array(
			'name'            => esc_html__( 'Vimeo video ID', 'noor' ),
			'id'              => '_dima_video_vimeo',
			'type'            => 'text',
			'label_cb'        => 'dima_meta_add_tooltip_to_label',
			'tooltip_options' => array(
				'tooltip-class' => '',
				'desc'          => esc_html__( 'Copy the numeric code that appears at the end of the video URL.', 'noor' ),
			),
			'attributes'      => array(
				'data-conditional-value' => 'vimeo',
				'data-conditional-id'    => '_dima_video_source',
			),
		),

		array(
			'name'            => esc_html__( 'M4V File URL', 'noor' ),
			'id'              => '_dima_video_m4v',
			'type'            => 'file',
			'label_cb'        => 'dima_meta_add_tooltip_to_label',
			'tooltip_options' => array(
				'tooltip-class' => '',
				'desc'          => esc_html__( 'The URL to the .m4v video file.', 'noor' ),
			),
			'attributes'      => array(
				'data-conditional-value' => 'self_hosted',
				'data-conditional-id'    => '_dima_video_source',
			),
		),

		array(
			'name'            => esc_html__( 'ogv File URL', 'noor' ),
			'id'              => '_dima_video_ogv',
			'type'            => 'file',
			'label_cb'        => 'dima_meta_add_tooltip_to_label',
			'tooltip_options' => array(
				'tooltip-class' => '',
				'desc'          => esc_html__( 'The URL to the .ogv video file.', 'noor' ),
			),
			'attributes'      => array(
				'data-conditional-value' => 'self_hosted',
				'data-conditional-id'    => '_dima_video_source',
			),
		),

		array(
			'name'       => esc_html__( 'Self hosted video file in webM format', 'noor' ),
			'id'         => '_dima_video_webm',
			'type'       => 'file',
			'attributes' => array(
				'data-conditional-value' => 'self_hosted',
				'data-conditional-id'    => '_dima_video_source',
			),
		),
		array(
			'name'       => esc_html__( 'video Width', 'noor' ),
			'id'         => '_dima_video_w',
			'type'       => 'text',
			'attributes' => array(
				'data-conditional-value' => 'self_hosted',
				'data-conditional-id'    => '_dima_video_source',
			),
		),

		array(
			'name'       => esc_html__( 'video Height', 'noor' ),
			'id'         => '_dima_video_h',
			'type'       => 'text',
			'attributes' => array(
				'data-conditional-value' => 'self_hosted',
				'data-conditional-id'    => '_dima_video_source',
			),
		),


		array(
			'name'            => esc_html__( 'Embedded Video Code', 'noor' ),
			'id'              => '_dima_video_embed',
			'type'            => 'textarea_code',
			'label_cb'        => 'dima_meta_add_tooltip_to_label',
			'tooltip_options' => array(
				'tooltip-class' => '',
				'desc'          => esc_html__( 'If you are using something other than self hosted video such as YouTube, Vimeo, or Wistia, paste the embed code here. This field will override the above.', 'noor' ),
			),
			'attributes'      => array(
				'data-conditional-value' => 'embedded',
				'data-conditional-id'    => '_dima_video_source',
			),
		),

		array(
			'name'            => esc_html__( 'Embedded Video URL', 'noor' ),
			'id'              => '_dima_video_embed_two',
			'type'            => 'oembed',
			'label_cb'        => 'dima_meta_add_tooltip_to_label',
			'tooltip_options' => array(
				'tooltip-class' => '',
				'desc'          => esc_html__( 'If you are using something other than self hosted video such as YouTube, Vimeo, or Wistia, paste the link here. This field will override the above.', 'noor' ),
			),
			'attributes'      => array(
				'data-conditional-value' => 'embedded',
				'data-conditional-id'    => '_dima_video_source',
			),
		),

	);
	/*-------------- !Video Settings -------------*/

	/*-------------- Details Tabs -------------*/

	$details_tabs = array(
		array(
			'name'            => esc_html__( 'Portfolio Details', 'noor' ),
			'label_cb'        => 'dima_meta_add_tooltip_to_label',
			'tooltip_options' => array(
				'tooltip-class' => '',
				'desc'          => esc_html__( 'Override the portfolio visibility.', 'noor' ),
			),
			'id'              => '_dima_portfolio_details',
			'type'            => 'dima_buttonset',
			'options'         => array(
				'inherit' => esc_html__( 'Inherit', 'noor' ),
				'on'      => esc_html__( 'On', 'noor' ),
				'off'     => esc_html__( 'Off', 'noor' )
			),
			'default'         => 'inherit',
		),
		array(
			'name'            => esc_html__( 'Portfolio Details Style', 'noor' ),
			'label_cb'        => 'dima_meta_add_tooltip_to_label',
			'tooltip_options' => array(
				'tooltip-class' => '',
				'desc'          => esc_html__( 'Specify the style template for the portfolio post.', 'noor' ),
			),
			'id'              => '_dima_portfolio_details_style',
			'type'            => 'dima_buttonset',
			'options'         => array(
				'inherit' => esc_html__( 'Inherit', 'noor' ),
				'classic' => esc_html__( 'Classic', 'noor' ),
				'modern'  => esc_html__( 'Modern', 'noor' )
			),
			'default'         => 'inherit',
			'attributes'      => array(
				'data-conditional-value' => wp_json_encode( array( 'on', 'inherit' ) ),
				'data-conditional-id'    => '_dima_portfolio_details',
			),

		),
		array(
			'name'            => esc_html__( 'Portfolio Details Layout', 'noor' ),
			'label_cb'        => 'dima_meta_add_tooltip_to_label',
			'tooltip_options' => array(
				'tooltip-class' => '',
				'desc'          => esc_html__( 'Specify the layout template for the portfolio post', 'noor' ),
			),
			'id'              => '_dima_portfolio_details_layout',
			'type'            => 'dima_image_select',
			'options'         => array(
				'inherit' => array(
					'title' => esc_html__( 'Inherit', 'noor' ),
					'alt'   => 'Inherit',
					'img'   => $image_path . 'img/inherit.png'
				),
				'top'     => array(
					'title' => esc_html__( 'Details on the top', 'noor' ),
					'alt'   => 'top',
					'img'   => $image_path . 'img/top.png'
				),
				'bottom'  => array(
					'title' => esc_html__( 'Details on the bottom', 'noor' ),
					'alt'   => 'bottom',
					'img'   => $image_path . 'img/bottom.png'
				),
				'left'    => array(
					'title' => esc_html__( 'Details on the left', 'noor' ),
					'alt'   => 'left',
					'img'   => $image_path . 'img/left.png'
				),
				'right'   => array(
					'title' => esc_html__( 'Details on the right', 'noor' ),
					'alt'   => 'right',
					'img'   => $image_path . 'img/right.png'
				),
			),
			'default'         => 'inherit',
			'attributes'      => array(
				'data-conditional-value' => 'classic',
				'data-conditional-id'    => '_dima_portfolio_details_style',
			),
		),
		array(
			'name'       => esc_html__( 'Client', 'noor' ),
			'id'         => '_dima_portfolio_client',
			'type'       => 'text',
			'desc'       => '',
			'attributes' => array(
				'data-conditional-value' => wp_json_encode( array( 'on', 'inherit' ) ),
				'data-conditional-id'    => '_dima_portfolio_details',
			),
		),
		array(
			'name'       => esc_html__( 'Services', 'noor' ),
			'id'         => '_dima_portfolio_services',
			'type'       => 'text',
			'desc'       => '',
			'attributes' => array(
				'data-conditional-value' => wp_json_encode( array( 'on', 'inherit' ) ),
				'data-conditional-id'    => '_dima_portfolio_details',
			),
		),
		array(
			'name'       => esc_html__( 'Skill', 'noor' ),
			'id'         => '_dima_portfolio_skill',
			'type'       => 'text',
			'desc'       => '',
			'attributes' => array(
				'data-conditional-value' => wp_json_encode( array( 'on', 'inherit' ) ),
				'data-conditional-id'    => '_dima_portfolio_details',
			),
		),
		array(
			'name'       => esc_html__( 'Year', 'noor' ),
			'id'         => '_dima_portfolio_year',
			'type'       => 'text',
			'desc'       => '',
			'attributes' => array(
				'data-conditional-value' => wp_json_encode( array( 'on', 'inherit' ) ),
				'data-conditional-id'    => '_dima_portfolio_details',
			),
		),
		array(
			'name'       => esc_html__( 'Link', 'noor' ),
			'id'         => '_dima_portfolio_url',
			'type'       => 'text_url',
			'desc'       => '',
			'attributes' => array(
				'data-conditional-value' => wp_json_encode( array( 'on', 'inherit' ) ),
				'data-conditional-id'    => '_dima_portfolio_details',
			),
		),
		array(
			'name'            => esc_html__( 'Share icons and Tags', 'noor' ),
			'label_cb'        => 'dima_meta_add_tooltip_to_label',
			'tooltip_options' => array(
				'tooltip-class' => '',
				'desc'          => esc_html__( 'Check to display Share icons and Tags', 'noor' ),
			),
			'id'              => '_dima_portfolio_shear_icon',
			'type'            => 'dima_buttonset',
			'options'         => array(
				'inherit' => esc_html__( 'Inherit', 'noor' ),
				'on'      => esc_html__( 'On', 'noor' ),
				'off'     => esc_html__( 'Off', 'noor' ),
			),
			'default'         => 'inherit',
		),

		//external link
		array(
			'name'            => esc_html__( 'External Link?', 'noor' ),
			'label_cb'        => 'dima_meta_add_tooltip_to_label',
			'tooltip_options' => array(
				'tooltip-class' => '',
				'desc'          => '',
			),
			'id'              => '_dima_portfolio_external_link',
			'type'            => 'dima_buttonset',
			'options'         => array(
				'on'  => esc_html__( 'On', 'noor' ),
				'off' => esc_html__( 'Off', 'noor' ),
			),
			'default'         => 'off',
		),
		array(
			'name'            => esc_html__( 'Open in new Tab?', 'noor' ),
			'id'              => '_dima_portfolio_external_link_target',
			'label_cb'        => 'dima_meta_add_tooltip_to_label',
			'tooltip_options' => array(
				'tooltip-class' => '',
				'desc'          => '',
			),
			'type'            => 'dima_buttonset',
			'options'         => array(
				'on'  => esc_html__( 'On', 'noor' ),
				'off' => esc_html__( 'Off', 'noor' ),
			),
			'attributes'      => array(
				'data-conditional-value' => wp_json_encode( array( 'on' ) ),
				'data-conditional-id'    => '_dima_portfolio_external_link',
			),
			'default'         => 'on',
		),

		array(
			'name'       => esc_html__( 'Link', 'noor' ),
			'id'         => '_dima_portfolio_external_link_url',
			'type'       => 'text_url',
			'desc'       => '',
			'attributes' => array(
				'data-conditional-value' => wp_json_encode( array( 'on' ) ),
				'data-conditional-id'    => '_dima_portfolio_external_link',
			),
		),
		array(
			'name'            => esc_html__( 'Display Related Projects', 'noor' ),
			'label_cb'        => 'dima_meta_add_tooltip_to_label',
			'tooltip_options' => array(
				'tooltip-class' => '',
				'desc'          => esc_html__( 'Override the related projects visibility', 'noor' ),
			),
			'id'              => '_dima_meta_projects_related_display',
			'type'            => 'dima_buttonset',
			'options'         => array(
				'inherit' => esc_html__( 'Inherit', 'noor' ),
				'on'      => esc_html__( 'On', 'noor' ),
				'off'     => esc_html__( 'Off', 'noor' ),
			),
			'default'         => 'inherit',
		),


	);

	/*-------------- !Details Tabs -------------*/

	$tabs_setting['tabs'][] = array(
		'id'     => 'meta_tab_general',
		'title'  => esc_html__( 'General', 'noor' ),
		'svg'    => 'ic_settings',
		'fields' => $general_tab
	);

	$tabs_setting['tabs'][] = array(
		'id'     => 'meta_tab_header',
		'title'  => esc_html__( 'Header', 'noor' ),
		'svg'    => 'ic_web_asset',
		'fields' => $header_tabs
	);

	$tabs_setting['tabs'][] = array(
		'id'     => 'meta_tab_menu',
		'title'  => esc_html__( 'Menu', 'noor' ),
		'svg'    => 'ic_menu',
		'fields' => $menu_tabs
	);

	$tabs_setting['tabs'][] = array(
		'id'     => 'meta_tab_logo',
		'title'  => esc_html__( 'Logo', 'noor' ),
		'svg'    => 'ic_image',
		'fields' => $logo_tabs
	);

	$tabs_setting['tabs'][] = array(
		'id'     => 'meta_tab_details',
		'title'  => esc_html__( 'Details', 'noor' ),
		'svg'    => 'ic_details',
		'fields' => $details_tabs
	);
	$tabs_setting['tabs'][] = array(
		'id'     => 'meta_tab_video',
		'title'  => esc_html__( 'Video Portfolio', 'noor' ),
		'svg'    => 'ic_video_library',
		'fields' => $video_tabs
	);

	$tabs_setting['tabs'][] = array(
		'id'     => 'meta_tab_footer',
		'title'  => esc_html__( 'Footer', 'noor' ),
		'svg'    => 'ic_call_to_action',
		'fields' => $footer_tab
	);

	if ( DIMA_REVOLUTION_SLIDER_IS_ACTIVE ) {
		$slider_settings_tab    = dima_meta_data::dima_get_meta_slider_settings();
		$tabs_setting['tabs'][] = array(
			'id'     => 'meta_tab_slider_settings',
			'title'  => esc_html__( 'Slider Settings', 'noor' ),
			'svg'    => 'ic_view_carousel',
			'fields' => $slider_settings_tab
		);
	}

	// set tabs
	$cmb->add_field( array(
		'id'   => '__tabs',
		'type' => 'tabs',
		'tabs' => $tabs_setting
	) );
}