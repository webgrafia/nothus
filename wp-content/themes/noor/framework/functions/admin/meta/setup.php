<?php

/**
 * Sets up custom meta boxes.
 *
 * @package Dima Framework
 * @subpackage Admin Meta
 * @version   1.0.0
 * @since     1.0.0
 * @author    PixelDima <info@pixeldima.com>
 *
 */


// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

$meta_path = DIMA_TEMPLATE_PATH . '/framework/functions/admin/meta';


// Include Entry and Taxonomy Meta Box Setup
// =============================================================================
require_once( $meta_path . '/meta-boxes.php' );