<?php
/**
 * Setup : DIMA Custoimzer manager page output
 *
 * @package Dima Framework
 * @subpackage Admin Setup
 * @version   1.0.0
 * @since     1.0.0
 * @author    PixelDima <info@pixeldima.com>
 *
 */


// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

ob_start();

/**
 * Page Output
 *
 * @return void
 */
function dima_page_customizer_backup() {
	$pixeldima_setup_path = DIMA_TEMPLATE_PATH . '/framework/functions/admin/pixeldima-setup';
	require_once( $pixeldima_setup_path . '/framework-dima-nav-tab.php' );
	?>
    <div class="about-wrap pixeldima-home">

        <div class="wrap">
            <h1 class="none"></h1>
        </div>

		<?php
		dima_admin_notice_message( array(
				'notice_id'   => 'theme_backup',
				'title'       => esc_html__( 'Customizer Backup', 'noor' ),
				'message'     => esc_html__( 'Easily export or import your WordPress customizer settings!.', 'noor' ),
				'dismissible' => false,
				'class'       => 'standar',
				'standard'    => false,
			)
		);
		?>

        <!-- Call import,export,reset sections -->
        <div class="pixeldima-customizer-backup-content">
			<?php dima_customizer_backup_export_output(); ?>
			<?php dima_customizer_backup_import_output(); ?>
			<?php dima_customizer_backup_reset_output(); ?>
        </div>

    </div>
	<?php
}

/**
 * Customizer backup import
 *
 * @return void
 */
function dima_customizer_backup_import_output() {
	?>

	<?php if ( isset( $_FILES['import'] ) && check_admin_referer( 'pixeldima-backup-import' ) ) {
		if ( $_FILES['import']['error'] > 0 ) {
			wp_die( esc_html__( 'ERROR: An import error occured. Please try again.', 'noor' ) );
		} else {
			$file_name      = $_FILES['import']['name'];
			$file_array     = explode( '.', $file_name );
			$file_extantion = strtolower( end( $file_array ) );
			$file_size      = $_FILES['import']['size'];
			if ( ( $file_extantion == 'json' ) && ( $file_size < 500000 ) ) {
				$encoded_options = dima_helper::get_local_file_contents( $_FILES['import']['tmp_name'] );
				$options         = json_decode( $encoded_options, true );
				foreach ( $options as $key => $value ) {
					update_option( $key, $value );
				}
				dima_admin_notice_message( array(
						'notice_id'   => 'theme_successfully_restored',
						'title'       => esc_html__( 'Done!', 'noor' ),
						'message'     => esc_html__( 'Successfully restored!.', 'noor' ),
						'dismissible' => true,
						'class'       => 'success',
						'standard'    => true,
					)
				);
			} else {
				dima_admin_notice_message( array(
						'notice_id'   => 'theme_successfully_restored',
						'title'       => esc_html__( 'Error', 'noor' ),
						'message'     => esc_html__( 'Invalid file type provided or file size too big. Please try again.', 'noor' ),
						'dismissible' => true,
						'class'       => 'success',
						'standard'    => true,
					)
				);
			}
		}
	}
	?>
    <div class="inside">
        <form method="post" enctype="multipart/form-data">
            <div class="customizer-warp">
				<?php wp_nonce_field( 'pixeldima-backup-import' ); ?>
                <h3>
                    <svg fill="#444444" height="24" viewBox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                        <path d="M0 0h24v24H0z" fill="none"/>
                        <path d="M9 16h6v-6h4l-7-7-7 7h4zm-4 2h14v2H5z"/>
                    </svg>
                    <span><?php esc_html_e( "Import", 'noor' ); ?></span>
                </h3>
                <p><?php
					echo sprintf( esc_html__( 'Upload your %s Customizer Settings (.json) file and we&apos;ll import the customizer options into
                    this site.', 'noor' ), DIMA_THEME_NAME );
					?>
                </p>
            </div>

            <div class="customizer-button">
                <p><input type="file" id="pixeldima-backup-import" name="import"></p>
                <p class="submit">
                    <input type="submit" name="submit" id="pixeldima-backup-import-submit" class="button button-primary"
                           value="<?php esc_html_e( "Upload", 'noor' ) ?>" disabled>
                </p>
            </div>

        </form>
    </div>

	<?php
}

/**
 * Customizer backup export
 *
 * @return void
 */
function dima_customizer_backup_export_output() {

	global $dima_customizer_data;

	if ( ! isset( $_POST['export'] ) ) {
		?>

        <div class="pixeldima-customizer-backup export">
        <h3 class="title"><span>Customizer Backup</span></h3>
        <div class="inside">
            <form method="post">
                <div class="customizer-warp">
                    <h3>
                        <svg fill="#444444" height="24" viewBox="0 0 24 24" width="24"
                             xmlns="http://www.w3.org/2000/svg">
                            <path d="M19 9h-4V3H9v6H5l7 7 7-7zM5 18v2h14v-2H5z"/>
                            <path d="M0 0h24v24H0z" fill="none"/>
                        </svg>
                        <span>Export</span>
                    </h3>
					<?php wp_nonce_field( 'pixeldima-backup-export' ); ?>
                    <p><?php esc_html_e( "Clicking the button below will create a JSON file that contain all customizer settings.", 'noor' ) ?></p>
                </div>
                <div class="customizer-button">
                    <p class="submit">
                        <input type="submit" name="export" class="button button-primary"
                               value="<?php esc_html_e( "Download", 'noor' ) ?>">
                    </p>
                </div>
            </form>
        </div>
		<?php
	} elseif ( check_admin_referer( 'pixeldima-backup-export' ) ) {
		$file_name = 'customizer';
		dima_admin_notice_message( array(
				'notice_id'   => 'theme_successfully_reset',
				'title'       => esc_html__( 'Done!', 'noor' ),
				'message'     => esc_html__( 'Successfully Export.', 'noor' ),
				'dismissible' => true,
				'class'       => 'success',
				'standard'    => true,
			)
		);

		$options = dima_customizer_options_list();
		foreach ( $options as $option ) {
			$value           = maybe_unserialize( dima_helper::dima_get_option( $option, $dima_customizer_data[ $option ] ) );
			$data[ $option ] = $value;
		}

		$json_data = json_encode( $data );

		//Clean (erase) the output buffer
		ob_clean();

		echo( $json_data );

		header( 'Content-Type: text/json; charset=' . get_option( 'blog_charset' ) );
		header( 'Content-Disposition: attachment; filename="' . esc_attr( $file_name ) . '.json"' );

		exit();
	}
}

/**
 * pixeldima_customizer_backup_reset_output Alert
 *
 * @return void
 */
function dima_customizer_backup_reset_output() {
	?>

	<?php
	if ( isset( $_POST['reset'] ) && check_admin_referer( 'pixeldima-backup-reset' ) ) {
		$options = dima_customizer_options_list();

		foreach ( $options as $option ) {
			delete_option( $option );
		}
		dima_admin_notice_message( array(
				'notice_id'   => 'theme_successfully_reset',
				'title'       => esc_html__( 'Success!', 'noor' ),
				'message'     => esc_html__( 'Successfully reset.', 'noor' ),
				'dismissible' => true,
				'class'       => 'success',
				'standard'    => true,
			)
		);
	} ?>
    <div class="inside reset">
        <form method="post">
			<?php wp_nonce_field( 'pixeldima-backup-reset' ); ?>
            <div class="customizer-warp">
                <h3>
                    <svg fill="#000000" height="24" viewBox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                        <path d="M17.65 6.35C16.2 4.9 14.21 4 12 4c-4.42 0-7.99 3.58-7.99 8s3.57 8 7.99 8c3.73 0 6.84-2.55 7.73-6h-2.08c-.82 2.33-3.04 4-5.65 4-3.31 0-6-2.69-6-6s2.69-6 6-6c1.66 0 3.14.69 4.22 1.78L13 11h7V4l-2.35 2.35z"/>
                        <path d="M0 0h24v24H0z" fill="none"/>
                    </svg>
                    <span><?php esc_html_e( "Reset", 'noor' ); ?></span>
                </h3>
                <p>
					<?php esc_html_e( "When you click the button below WordPress will reset your Customizer settings as if it were a new
                    installation (you can also import/export your custom configurations)", 'noor' ); ?>
                </p>
            </div>
            <div class="customizer-button">
                <p class="submit">
                    <input type="submit" id="pixeldima-backup-reset-submit"
                           class="cd-popup-trigger button button-primary"
                           value="<?php esc_html_e( "Reset", 'noor' ); ?>">
                    <input type="hidden" name="reset" value="reset">
                </p>
            </div>
        </form>
    </div>
    </div>

	<?php
}

/**
 * List Of Options for backUp
 */
function dima_customizer_options_list() {
	$options = array(
		'dima_footer_bg_image',
		'dima_footer_content_top_bg',
		'dima_footer_featured_top_bg',
		'dima_footer_featured_border_color',
		'dima_footer_featured_bg_image',
		'dima_footer_is_dark',
		'dima_footer_content_body_color',
		'dima_footer_content_link_color',
		'dima_footer_widget_header_color',
		'dima_footer_widget_body_color',
		'dima_footer_widget_link_color',
		'dima_footer_widget_link_hover_color',
		'dima_footer_widget_border_color',
		'dima_body_text_color',
		'dima_body_link_color',
		'dima_body_link_color_hover',
		'dima_navbar_text_color',
		'dima_menu_hover_text_color',
		'dima_navbar_text_color_after',
		'dima_navbar_text_hover_color',
		'dima_navbar_underline_on_off',
		'dima_menu_border_color',
		'dima_page_title_bg_color',
		'dima_submenu_bg_color',
		'dima_submenu_text_color',
		'dima_submenu_text_hover_color',
		'dima_navbar_background_color',
		'dima_logo_on_top_background_color',
		'dima_transparent_navbar_background_color',
		'dima_transparent_navbar_text_color',
		'dima_navbar_background_color_after',
		'dima_navbar_background_image',
		'dima_heading_text_color',
		'dima_logo_text_color',
		'dima_navbar_font_list',
		'dima_heading_font_list',
		'dima_logo_font_list',
		'dima_body_subsets_list',
		'dima_header_navbar_transparent',
		'dima_header_burger_menu',
		'dima_header_navbar_burger_mobile',
		'dima_header_burger_menu_style',
		'dima_header_burger_menu_float',
		'dima_body_font_list',
		'dima_layout_site',
		'dima_layout_content',
		'dima_layout_bbpress_content',
		'dima_layout_bp_content',
		'dima_header_navbar_position',
		'dima_header_navbar_text_align',
		'dima_header_navbar_shear_icon',
		'dima_header_navbar_wpml_lang_show',
		'dima_header_navbar_icon_menu',
		'dima_header_navbar_primary_menu',
		'dima_header_navbar_button',
		'dima_header_navbar_button_url',
		'dima_header_navbar_button_txt',
		'dima_header_navbar_button_bg_color',
		'dima_header_navbar_button_txt_color',
		'dima_header_navbar_button_bg_color_hover',
		'dima_header_navbar_animation',
		'dima_header_search_enable',
		'dima_header_burger_bg_color',
		'dima_header_burger_bg_img',
		'dima_header_search_bg_img',
		'dima_header_navbar_offset_by_id',
		'dima_header_navbar_offset_by_px',
		'dima_header_logo',
		'dima_header_sticky_logo',
		'dima_header_mobile_logo',
		'dima_header_logo_retina',
		'dima_header_sticky_logo_retina',
		'dima_header_mobile_logo_retina',
		'dima_header_logo_width',
		'dima_header_logo_rtl',
		'dima_page_title_display',
		'dima_breadcrumb_display',
		'dima_page_title_bg_is_cover',
		'dima_menu_copyright_display',
		'dima_breadcrumb_background_image',
		'dima_breadcrumb_position',
		'dima_navbar_option_myaccount_display_topbar',
		'dima_navbar_option_wishlist',
		'dima_navbar_option_address_text_topbar',
		'dima_navbar_lang_shortcode',
		'dima_navbar_option_tel_text_topbar',
		'dima_navbar_option_email_text_topbar',
		'dima_navbar_option_today_text_topbar',
		'dima_demo_settings',
		'dima_demo_name',
		'dima_template',
		'dima_content_width',
		'dima_content_max_width',
		'dima_sidebar_width',
		'dima_loading',
		'dima_loading_logo',
		'dima_loading_bg_color',
		'dima_loading_border_color',
		'dima_smoothscroll',
		'dima_smoothscroll',
		'dima_body_background_color',
		'dima_body_background_image_repeat',
		'dima_body_background_image_position',
		'_dima_meta_body_background_image_size',
		'dima_body_background_image_attachment',
		'dima_body_background_image',
		'dima_main_color',
		'dima_secondary_main_color',
		'dima_sticky_sidebar',
		'dima_frame_size',
		'dima_frame_color',
		'dima_custom_font',
		'dima_body_weights_list',
		'dima_btn_font_list',
		'dima_btn_weights_list',
		'dima_btn_weight_selected',
		'dima_btn_subsets_list',
		'dima_btn_text_size',
		'dima_body_text_size',
		'dima_navbar_text_style',
		'dima_navbar_weights_list',
		'dima_navbar_text_size',
		'dima_heading_text_style',
		'dima_heading_weights_list',
		'dima_heading_letter_spacing',
		'dima_logo_text_style',
		'dima_logo_weights_list',
		'dima_logo_text_size',
		'dima_logo_letter_spacing',
		'dima_blog_style',
		'dima_blog_layout',
		'dima_blog_masonry_columns',
		'dima_blog_comments_style',
		'dima_blog_enable_full_post_index',
		'dima_second_title_on_post',
		'dima_blog_blog_excerpt',
		'dima_blog_enable_post_meta',
		'dima_blog_enable_post_meta_cat',
		'dima_blog_enable_post_meta_comment',
		'dima_blog_enable_featured_image',
		'dima_elm_hover',
		'dima_img_hover',
		'dima_pagination_post',
		'dima_shear_icons_post',
		'dima_author_post',
		'dima_shop_slide_animation',
		'dima_post_related_display',
		'dima_post_related_is_slide',
		'dima_post_related_columns',
		'dima_post_related_count',
		'dima_structure_data',
		'dima_schema_type',
		'dima_pagination_bg_color',
		'dima_sidebar_widget_header_size',
		'dima_sidebar_widget_header_uppercase',
		'dima_sidebar_widget_body_size',
		'dima_sidebar_widget_body_uppercase',
		'dima_shop_menu',
		'dima_shop_sub_menu',
		'dima_shop_layout',
		'dima_shop_columns',
		'dima_shop_posts_per_page',
		'dima_shop_elm_hover',
		'dima_shop_sort',
		'dima_shop_product_layout',
		'dima_shop_product_tap_display',
		'dima_shop_description_tap_display',
		'dima_shop_info_tap_display',
		'dima_shop_reviews_tap_display',
		'dima_shop_related_products_display',
		'dima_shop_related_product_columns',
		'dima_shop_related_product_count',
		'dima_shop_upsells_display',
		'dima_shop_upsells_columns',
		'dima_shop_upsells_count',
		'dima_shop_cart_display',
		'dima_shop_cart_columns',
		'dima_shop_cart_count',
		'dima_footer_big',
		'dima_footer_full_width',
		'dima_footer_parallax',
		'dima_second_footer_on_off',
		'dima_small_footer_on_off',
		'dima_footer_bottom_center',
		'dima_footer_bottom',
		'dima_footer_menu_display',
		'dima_footer_go_to_top',
		'dima_footer_content_display',
		'dima_footer_featured_area',
		'dima_footer_content_text',
		'dima_second_footer_widget_areas',
		'dima_footer_widget_areas',
		'dima_footer_widget_header_size',
		'dima_footer_widget_header_uppercase',
		'dima_footer_widget_body_size',
		'dima_footer_widget_body_uppercase',
		'dima_open_graph_meta_tag',
		'dima_custom_style',
		'dima_custom_js',
		'dima_amp_logo',
		'dima_amp_related_posts',
		'dima_amp_enable',
		'dima_amp_share_buttons',
		'dima_amp_facebook_id',
		'dima_amp_leave_a_comment',
		'dima_amp_categories',
		'dima_amp_ago',
		'dima_amp_tags',
		'dima_amp_check_also',
		'dima_ad_blocker_detector',
		'dima_ad_above_article',
		'dima_ad1_shortcode',
		'dima_ad2_shortcode',
		'dima_ad3_shortcode',
		'dima_ad4_shortcode',
		'dima_ad5_shortcode',
		'dima_ad_below_article_custom_code',
		'dima_ad_above_article_custom_code',
		'dima_ad_below_article',
		'dima_disable_featured_gif',
		'dima_minified_files',
		'dima_lazy_image',
		'dima_fontawesome_five',
		'dima_minified_js_files',
		'dima_space_before_head',
		'dima_space_before_body',
		'dima_space_after_body',
		'dima_google_map_api_key',
		'dima_social_icons_is_colored',
		'dima_social_facebook',
		'dima_social_twitter',
		'dima_social_googleplus',
		'dima_social_linkedin',
		'dima_social_youtube',
		'dima_social_vimeo',
		'dima_social_foursquare',
		'dima_social_tumblr',
		'dima_social_instagram',
		'dima_social_dribbble',
		'dima_social_flickr',
		'dima_social_behance',
		'dima_social_pinterest',
		'dima_social_whatsapp',
		'dima_social_soundcloud',
		'dima_social_rss',
		'dima_social_vk',
		'dima_favicon',
		'dima_iphone_icon',
		'dima_iphone_retina_icon',
		'dima_ipad_icon',
		'dima_ipad_retina_icon',
		'dima_opengraph_image',
		'dima_body_weight_selected',
		'dima_body_weight_selected',
		'dima_logo_weight_selected',
		'dima_navbar_weight_selected',
		'dima_heading_weight_selected',
		'dima_logo_font_and_weight_list',
		'dima_navbar_font_and_weight_list',
		'dima_heading_font_and_weight_list',
		'dima_projects_related_display',
		'dima_projects_related_style',
		'dima_projects_related_columns',
		'dima_projects_related_count',
		'dima_projects_related_elm_hover',
		'dima_projects_related_img_hover',
		'dima_projects_details',
		'dima_projects_details_style',
		'dima_projects_details_layout',
		'dima_projects_slug_name',
		'dima_portfolio_top_link_source',
		'dima_portfolio_page_url',
		'dima_portfolio_page',
		'dima_header_logo_width_rtl',
		'dima_header_logo_rtl',
		'dima_header_sticky_logo_rtl',
		'dima_header_mobile_logo_rtl',
		'dima_header_logo_retina_rtl',
		'dima_header_sticky_logo_retina_rtl',
		'dima_header_mobile_logo_retina_rtl',
		'dima_header_navbar_menu_dark',
		'dima_header_navbar_sup_menu_dark',
		'dima_header_navbar_page_title_dark',
		'dima_projects_details_pagination',
		'dima_projects_portfolio_shear_icon_and_tag',
		'dima_layout_projects_details_content',
		'dima_portfolio_page_columns',
		'dima_portfolio_page_elm_hover',
		'dima_portfolio_page_img_hover',
		'dima_portfolio_page_filters',
		'dima_portfolio_page_margin',
		'dima_portfolio_page_is_pagination',
		'dima_portfolio_page_count',
		'dima_portfolio_page_name',
		'dima_amp_back_to_top',
		'dima_amp_footer_logo',
		'dima_amp_footer_menu',
		'dima_amp_footer_content_text',
		'dima_amp_ad_abover',
		'dima_amp_ad_below',
		'dima_amp_bg_color',
		'dima_amp_header_bg_color',
		'dima_amp_title_color',
		'dima_amp_meta_color',
		'dima_amp_link_color',
		'dima_amp_footer_bg_color',
		'dima_amp_footer_border_color',
		'dima_heading_text_style_2',
		'dima_heading_font_list_2',
		'dima_heading_font_and_weight_list_2',
		'dima_heading_weights_list_2',
		'dima_heading_weight_selected_2',
		'dima_heading_letter_spacing_2',
		'dima_heading_text_color_2',
		'dima_heading_text_style_3',
		'dima_heading_font_list_3',
		'dima_heading_font_and_weight_list_3',
		'dima_heading_weights_list_3',
		'dima_heading_weight_selected_3',
		'dima_heading_letter_spacing_3',
		'dima_heading_text_color_3',
		'dima_heading_text_style_4',
		'dima_heading_font_list_4',
		'dima_heading_font_and_weight_list_4',
		'dima_heading_weights_list_4',
		'dima_heading_weight_selected_4',
		'dima_heading_letter_spacing_4',
		'dima_heading_text_color_4',
		'dima_heading_text_style_5',
		'dima_heading_font_list_5',
		'dima_heading_font_and_weight_list_5',
		'dima_heading_weights_list_5',
		'dima_heading_weight_selected_5',
		'dima_heading_letter_spacing_5',
		'dima_heading_text_color_5',
		'dima_heading_text_style_6',
		'dima_heading_font_list_6',
		'dima_heading_font_and_weight_list_6',
		'dima_heading_weights_list_6',
		'dima_heading_weight_selected_6',
		'dima_heading_letter_spacing_6',
		'dima_heading_text_color_6',
	);

	return $options;
}
