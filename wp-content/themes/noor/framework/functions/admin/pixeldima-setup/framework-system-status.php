<?php
/**
 * Setup : system status
 *
 * @package Dima Framework
 * @subpackage Admin Setup
 * @version   1.0.0
 * @since     1.0.0
 * @author    PixelDima <info@pixeldima.com>
 *
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class dima_system_status {
	static $system_status = array();

	static function add( $section, $status_array ) {
		self::$system_status[ $section ] [] = $status_array;
	}

	static function dima_render_tables() {
		foreach ( self::$system_status as $section_name => $section_statuses ) {
			?>
            <table class="widefat dima-status-table" cellspacing="0">
                <thead>
                <tr>
                    <th colspan="4"
                        data-export-label="<?php echo esc_attr( $section_name ) ?>"><?php echo esc_attr( $section_name ) ?></th>
                </tr>
                </thead>
                <tbody>
				<?php

				foreach ( $section_statuses as $status_params ) {
					?>
                    <tr>
                        <td class="dima-system-status-name"
                            data-export-label="<?php echo esc_attr( $status_params['check_name'] ) ?>">
							<?php echo esc_attr( $status_params['check_name'] ) ?>
                        </td>
                        <td class="dima-system-status-help">
							<?php
							switch ( $status_params['status'] ) {
								case 'green':
									echo '<div class="dima-system-status-green dima-tooltip"  title="' . esc_attr( $status_params['tooltip'] ) . '"><span class="dashicons dashicons-yes"></span></div>';
									break;
								case 'yellow':
									echo '<div class="dima-system-status-yellow dima-tooltip"  title="' . esc_attr( $status_params['tooltip'] ) . '"><span class="dashicons dashicons-warning"></span></div>';
									break;
								case 'red' :
									echo '<div class="dima-system-status-red dima-tooltip"  title="' . esc_attr( $status_params['tooltip'] ) . '"><span class="dashicons dashicons-dismiss"></span></div>';
									break;
								case 'info':
									echo '<div class="dima-system-status-info dima-tooltip"  title="' . esc_attr( $status_params['tooltip'] ) . '"><span class="dashicons dashicons-editor-help"></span></div>';
									break;
							}
							?>
                        </td>
                        <td class="dima-system-status-value">
							<?php
							switch ( $status_params['status'] ) {
								case 'green':
									echo '<div class="dima-system-status-green">' . esc_attr( $status_params['value'] ) . ' </div>';
									break;
								case 'red' :
									echo '<div class="dima-system-status-red">' . esc_attr( $status_params['value'] ) . '</div>';
									break;
								default:
									echo( $status_params['value'] );
									break;
							}
							?>
                        </td>
                    </tr>
					<?php
				}

				?>
                </tbody>
            </table>
			<?php
		}
	}

	static function dima_let_to_num( $size ) {
		$l   = substr( $size, - 1 );
		$ret = substr( $size, 0, - 1 );
		switch ( strtoupper( $l ) ) {
			case 'P':
				$ret *= 1024;
			case 'T':
				$ret *= 1024;
			case 'G':
				$ret *= 1024;
			case 'M':
				$ret *= 1024;
			case 'K':
				$ret *= 1024;
		}

		return $ret;
	}

	/**
	 * _memory_limit
	 *
	 * Get the wp memory limit
	 */
	static function dima_memory_limit() {
		$wp_memory_limit = self::dima_let_to_num( WP_MEMORY_LIMIT );
		if ( function_exists( 'memory_get_usage' ) ) {
			$wp_memory_limit = max( $wp_memory_limit, self::dima_let_to_num( @ini_get( 'memory_limit' ) ) );
		}

		return $wp_memory_limit;
	}

	/**
	 * _post_request
	 *
	 * Test POST requests
	 */
	static function dima_post_request() {
		$post_response = wp_safe_remote_post( 'https://www.paypal.com/cgi-bin/webscr', array(
			'timeout'     => 60,
			'user-agent'  => 'woocommerce/',
			'httpversion' => '1.1',
			'body'        => array(
				'cmd' => '_notify-validate',
			),
		) );

		$post_response_successful = false;
		if ( ! is_wp_error( $post_response ) && $post_response['response']['code'] >= 200 && $post_response['response']['code'] < 300 ) {
			$post_response_successful = true;
		}

		return $post_response_successful;
	}

	/**
	 * _get_request
	 *
	 * Test GET requests
	 */
	static function dima_get_request() {
		$get_response            = wp_safe_remote_get( 'https://woocommerce.com/wc-api/product-key-api?request=ping&network=' . ( is_multisite() ? '1' : '0' ) );
		$get_response_successful = false;
		if ( ! is_wp_error( $get_response ) && $get_response['response']['code'] >= 200 && $get_response['response']['code'] < 300 ) {
			$get_response_successful = true;
		}

		return $get_response_successful;
	}

	/**
	 * _curl_version
	 *
	 * Figure out cURL version, if installed
	 */
	static function dima_curl_version() {
		$curl_version = '';
		if ( function_exists( 'curl_version' ) ) {
			$curl_version = curl_version();
			$curl_version = esc_attr( $curl_version['version'] ) . ', ' . esc_attr( $curl_version['ssl_version'] );
		}

		return $curl_version;
	}

	/**
	 * _theme_info
	 *
	 * Get the theme info
	 */
	static function dima_theme_info() {

		$active_theme = wp_get_theme();

		if ( is_child_theme() ) {
			$parent_theme      = wp_get_theme( $active_theme->Template );
			$parent_theme_info = array(
				'parent_name'       => $parent_theme->Name,
				'parent_version'    => $parent_theme->Version,
				'parent_author_url' => $parent_theme->{'Author URI'},
			);
		} else {
			$parent_theme_info = array(
				'parent_name'           => '',
				'parent_version'        => '',
				'parent_version_latest' => '',
				'parent_author_url'     => ''
			);
		}

		$active_theme_info = array(
			'name'           => $active_theme->Name,
			'version'        => $active_theme->Version,
			'author_url'     => esc_url_raw( $active_theme->{'Author URI'} ),
			'is_child_theme' => is_child_theme(),
		);

		return array_merge( $active_theme_info, $parent_theme_info );
	}

	/**
	 * _print_report
	 */
	static function _print_report() { ?>

        <table class="dima-status-table_ widefat" cellspacing="0">
            <tbody>
            <tr>
                <td>
                    <p><?php esc_html_e( 'Please copy and paste this information in your ticket when contacting support:', 'noor' ); ?> </p>
                    <a id="get-debug-report" href="#"
                       class="button-primary"><?php esc_html_e( 'Get system report', 'noor' ); ?></a>
                    <div id="dima-debug-report">
                        <textarea readonly="readonly"></textarea>
                    </div>
                </td>
            </tr>
            </tbody>
        </table>

		<?php
		echo '<p class="dima-message-hint">' . sprintf( esc_html__( 'Looking for a good web hosting company? %1$sCheck our recommendations!%2$s', 'noor' ), '<a target="_blank" href="https://pixeldima.com/wordpress-hosting/"><strong>', '</strong></a>' ) . '</p>';

		?>
        <script type="text/javascript">
            jQuery('#get-debug-report').click(
                function () {
                    var report = '';
                    jQuery('.dima-status-table thead, .dima-status-table tbody').each(
                        function () {
                            if (jQuery(this).is('thead')) {
                                var label = jQuery(this).find('th:eq(0)').data('export-label') || jQuery(this).text();
                                report = report + '\n### ' + jQuery.trim(label) + ' ###\n\n';
                            } else {
                                jQuery('tr', jQuery(this)).each(function () {
                                    var label = jQuery(this).find('td:eq(0)').data('export-label') || jQuery(this).find('td:eq(0)').text();
                                    var the_name = jQuery.trim(label).replace(/(<([^>]+)>)/ig, ''); // Remove HTML.

                                    var $value_icon = jQuery(this).find('td:eq(1)').clone();
                                    $value_icon.find('.dashicons-yes').replaceWith('&#10003;');
                                    $value_icon.find('.dashicons-warning').replaceWith('&#10060;');
                                    var the_icon_value = jQuery.trim($value_icon.text());
                                    var the_icon_value = the_icon_value.split(', ');

                                    if (the_icon_value.length > 1) {
                                        // If value have a list of plugins ','.
                                        // Split to add new line.
                                        var temp_line = '';
                                        jQuery.each(the_icon_value, function (key, line) {
                                            temp_line = temp_line + line + '\n';
                                        });
                                        the_icon_value = temp_line;
                                    }

                                    // Find value
                                    var $value_html = jQuery(this).find('td:eq(2)').clone();
                                    $value_html.find('.private').remove();

                                    // Format value
                                    var the_value = jQuery.trim($value_html.text());
                                    var value_array = the_value.split(', ');

                                    if (value_array.length > 1) {
                                        // If value have a list of plugins ','.
                                        // Split to add new line.
                                        var temp_line = '';
                                        jQuery.each(value_array, function (key, line) {
                                            temp_line = temp_line + line + '\n';
                                        });
                                        the_value = temp_line;
                                    }
                                    report = report + '' + the_name + ': ' + the_value + the_icon_value + '\n';
                                });
                            }
                        }
                    );

                    try {
                        jQuery(this).hide();
                        jQuery("#dima-debug-report").slideDown();
                        jQuery("#dima-debug-report textarea").val(report).focus().select();

                        return false;
                    } catch (e) {
                        console.log(e);
                    }

                    return false;
                }
            );
        </script>
		<?php

	}

	/**
	 * _environment_info
	 *
	 * All environment info
	 */
	static function dima_environment_info() {
		global $wpdb;

		$post_response = self::dima_post_request();
		$get_response  = self::dima_get_request();

		return array(
			'home_url'                  => home_url( '/' ),
			'site_url'                  => site_url( '/' ),
			'wp_version'                => get_bloginfo( 'version' ),
			'wp_multisite'              => is_multisite(),
			'wp_memory_limit'           => self::dima_memory_limit(),
			'wp_debug_mode'             => ( defined( 'WP_DEBUG' ) && WP_DEBUG ),
			'language'                  => get_locale(),
			'server_info'               => $_SERVER['SERVER_SOFTWARE'],
			'php_version'               => phpversion(),
			'php_post_max_size'         => self::dima_let_to_num( ini_get( 'post_max_size' ) ),
			'php_max_execution_time'    => ini_get( 'max_execution_time' ),
			'php_max_input_vars'        => ini_get( 'max_input_vars' ),
			'curl_version'              => self::dima_curl_version(),
			'suhosin_installed'         => extension_loaded( 'suhosin' ),
			'max_upload_size'           => wp_max_upload_size(),
			'mysql_version'             => ( ! empty( $wpdb->is_mysql ) ? $wpdb->db_version() : '' ),
			'fsockopen_or_curl_enabled' => ( function_exists( 'fsockopen' ) || function_exists( 'curl_init' ) ),
			'mbstring_enabled'          => extension_loaded( 'mbstring' ),
			'remote_post_successful'    => $post_response,
			'remote_post_response'      => ( is_wp_error( $post_response ) ? $post_response->get_error_message() : $post_response['response']['code'] ),
			'remote_get_successful'     => $get_response,
			'remote_get_response'       => ( is_wp_error( $get_response ) ? $get_response->get_error_message() : $get_response['response']['code'] ),
			'secure_connection'         => 'https' === substr( get_home_url(), 0, 5 ),
			'hide_errors'               => ! ( defined( 'WP_DEBUG' ) && defined( 'WP_DEBUG_DISPLAY' ) && WP_DEBUG && WP_DEBUG_DISPLAY ) || 0 === intval( ini_get( 'display_errors' ) ),
		);
	}
}


function dima_setup_page_system_status() {
	$pixeldima_setup_path = DIMA_TEMPLATE_PATH . '/framework/functions/admin/pixeldima-setup';
	require_once( $pixeldima_setup_path . '/framework-dima-nav-tab.php' );
	$sys_state   = new dima_system_status();
	$theme       = $sys_state->dima_theme_info();
	$environment = $sys_state->dima_environment_info();
	?>

    <div class="wrap about-wrap pixeldima-home">
        <header class="pixeldima-header">
            <h1><?php esc_html_e( 'System Status', 'noor' ) ?> </h1>
            <div class="about-text">
                <p>
					<?php
					esc_html_e( 'Here you can check the system status.', 'noor' );
					?>
                </p>
            </div>
        </header>
		<?php

		// Theme name
		dima_system_status::add( 'Theme config', array(
			'check_name' => 'Theme name',
			'tooltip'    => 'Theme name',
			'value'      => DIMA_THEME_NAME,
			'status'     => 'info'
		) );

		// Theme version
		dima_system_status::add( 'Theme config', array(
			'check_name' => 'Theme version',
			'tooltip'    => 'Theme current version',
			'value'      => DIMA_VERSION,
			'status'     => 'info'
		) );

		$noor_assistant = array(
			'noor_assistant/noor-assistant.php' => array(
				'name'   => 'Active',
				'status' => 'green',
			)
		);


		$noor_assistant_plugin        = sprintf( esc_html__( 'This plugin is required to run %s. It provides a all the shortcodes used in %s', 'noor' ), DIMA_THEME_NAME, DIMA_THEME_NAME );
		$noor_assistant_plugin_status = 'red';
		$active_plugins                = get_option( 'active_plugins' );

		foreach ( $active_plugins as $active_plugin ) {
			if ( isset( $noor_assistant[ $active_plugin ] ) ) {
				$noor_assistant_plugin        = $noor_assistant[ $active_plugin ]['name'];
				$noor_assistant_plugin_status = $noor_assistant[ $active_plugin ]['status'];
				break;
			}
		}

		$plugin_data    = get_plugin_data( WP_PLUGIN_DIR . '/noor_assistant/noor-assistant.php' );
		$plugin_version = $plugin_data['Version'];

		dima_system_status::add( 'Theme config', array(
			'check_name' => 'Dima Shortcodes',
			'tooltip'    => 'Dima Shortcodes',
			'value'      => $noor_assistant_plugin,
			'status'     => $noor_assistant_plugin_status
		) );

		dima_system_status::add( 'Theme config', array(
			'check_name' => 'Dima Shortcodes Version',
			'tooltip'    => 'Dima Shortcodes Version',
			'value'      => $plugin_version,
			'status'     => 'info'
		) );
		dima_system_status::add( 'Theme config', array(
			'check_name' => 'Child theme',
			'tooltip'    => 'Is child theme active',
			'value'      => ( $theme['is_child_theme'] ) ? '<mark class="yes"><span class="dashicons dashicons-yes"></span></mark>' : '&ndash;',
			'status'     => 'info'
		) );

		dima_system_status::add( 'Server environment', array(
			'check_name' => 'Server software',
			'tooltip'    => 'Server software version',
			'value'      => esc_html( $_SERVER['SERVER_SOFTWARE'] ),
			'status'     => 'info'
		) );

		// php version
		dima_system_status::add( 'Server environment', array(
			'check_name' => 'PHP Version',
			'tooltip'    => 'Recommended: PHP 5.4 or greater',
			'value'      => phpversion(),
			'status'     => 'info'
		) );
		dima_system_status::add( 'Server environment', array(
			'check_name' => 'PHP Post Max Size:',
			'tooltip'    => 'The largest filesize that can be contained in one post.',
			'value'      => ini_get( 'post_max_size' ),
			'status'     => 'info'
		) );
		$max_execution_time = ini_get( 'max_execution_time' );
		if ( $max_execution_time == 0 or $max_execution_time >= 60 ) {
			dima_system_status::add( 'Server environment', array(
				'check_name' => 'PHP Time Limit',
				'tooltip'    => 'The amount of time (in seconds) that your site will spend on a single operation before timing out (to avoid server lockups)',
				'value'      => $max_execution_time,
				'status'     => 'green'
			) );
		} else {
			dima_system_status::add( 'Server environment', array(
				'check_name' => 'max_execution_time',
				'tooltip'    => 'This sets the maximum time in seconds.',
				'value'      => $max_execution_time,
				'status'     => 'yellow'
			) );
		}

		$max_input_vars = ini_get( 'max_input_vars' );
		if ( $max_input_vars == 0 or $max_input_vars >= 2000 ) {
			dima_system_status::add( 'Server environment', array(
				'check_name' => 'PHP Max Input Vars',
				'tooltip'    => 'The maximum number of variables your server can use for a single function to avoid overloads.',
				'value'      => $max_input_vars,
				'status'     => 'green'
			) );
		} else {
			dima_system_status::add( 'Server environment', array(
				'check_name' => 'PHP Max Input Vars',
				'tooltip'    => 'The maximum number of variables your server can use for a single function to avoid overloads.',
				'value'      => $max_input_vars,
				'status'     => 'yellow'
			) );
		}

		if ( extension_loaded( 'suhosin' ) !== true ) {
			dima_system_status::add( 'Server environment', array(
				'check_name' => 'SUHOSIN installed',
				'tooltip'    => 'Suhosin is not installed on your server.',
				'value'      => 'SUHOSIN not installed',
				'status'     => 'info'
			) );
		} else {
			dima_system_status::add( 'Server environment', array(
				'check_name' => 'SUHOSIN Installed',
				'tooltip'    => 'Suhosin is an advanced protection system for PHP installations. It was designed to protect servers and users from known and unknown flaws in PHP applications and the PHP core. If it\'s installed on your host you have to increase the suhosin.post.max_vars and suhosin.request.max_vars parameters to 2000 or more.',
				'value'      => 'SUHOSIN is installed',
				'status'     => 'yellow'
			) );

			if ( ini_get( "suhosin.post.max_vars" ) >= 2000 ) {
				dima_system_status::add( 'Server environment', array(
					'check_name' => 'suhosin.post.max_vars',
					'tooltip'    => 'This parameter is properly set',
					'value'      => ini_get( "suhosin.post.max_vars" ),
					'status'     => 'green'
				) );
			} else {
				dima_system_status::add( 'Server environment', array(
					'check_name' => 'suhosin.post.max_vars',
					'tooltip'    => 'To avoid this increase suhosin.post.max_vars parameter to 2000 or more.',
					'value'      => ini_get( "suhosin.post.max_vars" ),
					'status'     => 'yellow'
				) );
			}

			if ( ini_get( "suhosin.request.max_vars" ) >= 2000 ) {
				dima_system_status::add( 'Server environment', array(
					'check_name' => 'suhosin.request.max_vars',
					'tooltip'    => 'This parameter is properly set',
					'value'      => ini_get( "suhosin.request.max_vars" ),
					'status'     => 'green'
				) );
			} else {
				dima_system_status::add( 'Server environment', array(
					'check_name' => 'suhosin.request.max_vars',
					'tooltip'    => 'To avoid this increase suhosin.post.max_vars parameter to 2000 or more.',
					'value'      => ini_get( "suhosin.request.max_vars" ),
					'status'     => 'yellow'
				) );
			}
		}

		dima_system_status::add( 'Server environment', array(
			'check_name' => 'cURL version',
			'tooltip'    => 'cURL version',
			'value'      => esc_html( $environment['curl_version'] ),
			'status'     => 'info'
		) );

		dima_system_status::add( 'Server environment', array(
			'check_name' => 'Max upload size',
			'tooltip'    => '',
			'value'      => size_format( $environment['max_upload_size'] ),
			'status'     => 'info'
		) );

		if ( $environment['fsockopen_or_curl_enabled'] ) {
			dima_system_status::add( 'Server environment', array(
				'check_name' => 'fsockopen/cURL',
				'tooltip'    => '',
				'value'      => 'Active',
				'status'     => 'green'
			) );
		} else {
			dima_system_status::add( 'Server environment', array(
				'check_name' => 'fsockopen/cURL',
				'tooltip'    => '',
				'value'      => '_',
				'status'     => 'yellow'
			) );
		}

		if ( $environment['mbstring_enabled'] ) {
			dima_system_status::add( 'Server environment', array(
				'check_name' => 'Multibyte string',
				'tooltip'    => '',
				'value'      => 'Active',
				'status'     => 'green'
			) );
		} else {
			dima_system_status::add( 'Server environment', array(
				'check_name' => 'Multibyte string',
				'tooltip'    => '',
				'value'      => '_',
				'status'     => 'yellow'
			) );
		}

		if ( $environment['remote_post_successful'] ) {
			dima_system_status::add( 'Server environment', array(
				'check_name' => 'Remote post',
				'tooltip'    => '',
				'value'      => 'Active',
				'status'     => 'green'
			) );
		} else {
			dima_system_status::add( 'Server environment', array(
				'check_name' => 'Remote post',
				'tooltip'    => '',
				'value'      => '_',
				'status'     => 'yellow'
			) );
		}
		if ( $environment['remote_get_successful'] ) {
			dima_system_status::add( 'Server environment', array(
				'check_name' => 'Remote get',
				'tooltip'    => '',
				'value'      => 'Active',
				'status'     => 'green'
			) );
		} else {
			dima_system_status::add( 'Server environment', array(
				'check_name' => 'Remote get',
				'tooltip'    => '',
				'value'      => '_',
				'status'     => 'yellow'
			) );
		}

		dima_system_status::add( 'WordPress and plugins', array(
			'check_name' => 'WP Home URL',
			'tooltip'    => 'WordPress Address (URL)',
			'value'      => esc_url( home_url( '/' ) ),
			'status'     => 'info'
		) );

		dima_system_status::add( 'WordPress and plugins', array(
			'check_name' => 'WP Site URL',
			'tooltip'    => 'Site Address (URL)',
			'value'      => esc_url( site_url() ),
			'status'     => 'info'
		) );

		if ( home_url() != site_url() ) {
			dima_system_status::add( 'WordPress and plugins', array(
				'check_name' => 'Home URL - Site URL',
				'tooltip'    => 'Home URL not equal to Site URL, this may indicate a problem with your WordPress configuration.',
				'value'      => 'Home URL != Site URL <span class="dima-status-small-text">Home URL not equal to Site URL, this may indicate a problem with your WordPress configuration.</span>',
				'status'     => 'yellow'
			) );
		}

		dima_system_status::add( 'WordPress and plugins', array(
			'check_name' => 'WP version',
			'tooltip'    => 'WordPress version',
			'value'      => get_bloginfo( 'version' ),
			'status'     => 'info'
		) );

		dima_system_status::add( 'WordPress and plugins', array(
			'check_name' => 'WP multisite enabled',
			'tooltip'    => 'WP multisite',
			'value'      => is_multisite() ? 'Yes' : 'No',
			'status'     => 'info'
		) );

		dima_system_status::add( 'WordPress and plugins', array(
			'check_name' => 'WP Language',
			'tooltip'    => 'WP Language',
			'value'      => get_locale(),
			'status'     => 'info'
		) );

		$memory_limit = dima_system_status::dima_let_to_num( WP_MEMORY_LIMIT );
		if ( $memory_limit < 67108864 ) {
			dima_system_status::add( 'WordPress and plugins', array(
				'check_name' => 'WP Memory Limit',
				'tooltip'    => 'By default in wordpress the PHP memory limit is set to 40MB. With some plugins this limit may be reached and this affects your website functionality. To avoid this increase the memory limit to at least 64MB.',
				'value'      => size_format( $memory_limit ) . '/request <span class="dima-status-small-text">- We recommend setting memory to at least 64MB. The theme is well tested with a 40MB/request limit, but if you are using multiple plugins that may not be enough. See: <a href="http://codex.wordpress.org/Editing_wp-config.php#Increasing_memory_allocated_to_PHP" target="_blank" rel="noopener">Increasing memory allocated to PHP</a>. You can also check our guide <a href="http://forum.tagdiv.com/system-status-parameters-guide/">here</a>.</span>',
				'status'     => 'yellow'
			) );
		} else {
			dima_system_status::add( 'WordPress and plugins', array(
				'check_name' => 'WP Memory Limit',
				'tooltip'    => 'This parameter is properly set.',
				'value'      => size_format( $memory_limit ) . '/request',
				'status'     => 'green'
			) );
		}

		if ( defined( 'WP_DEBUG' ) and WP_DEBUG === true ) {
			dima_system_status::add( 'WordPress and plugins', array(
				'check_name' => 'WP_DEBUG',
				'tooltip'    => 'The debug mode is intended for development and it may display unwanted messages. You should disable it on your side.',
				'value'      => 'WP_DEBUG is enabled.',
				'status'     => 'yellow'
			) );
		} else {
			dima_system_status::add( 'WordPress and plugins', array(
				'check_name' => 'WP_DEBUG',
				'tooltip'    => 'The debug mode is disabled.',
				'value'      => 'False',
				'status'     => 'green'
			) );
		}
		dima_system_status::dima_render_tables();
		$sys_state->_print_report();

		?>

    </div>


	<?php

	$output = ob_get_contents();
	ob_end_clean(); ?>

	<?php echo apply_filters( 'dima_addons_home_content', $output ); ?>


<?php }