<?php
/**
 * Theme setup settings.
 *
 * @package Dima Framework
 * @subpackage Admin Setup
 * @version   1.0.0
 * @since     1.0.0
 * @author    PixelDima <info@pixeldima.com>
 *
 */

defined( 'ABSPATH' ) or die( 'You cannot access this script directly' );

class DIMA_Importer_Admin_ {
	private $message;
	private $xml_old_content;
	private $debugMessage;
	const VERSION = '1.0.0';

	/**/
	protected $session_id;
	public $session_data;
	protected $registry;
	protected $processor;
	# the demo folder slug
	private $demo_folder_slug;
	private $import_content_types;
	private $fetch_attachments = false;

	function __construct() {
		add_action( 'wp_ajax_dima_import_demo', array( $this, 'ajax_dima_importer' ) );
	}

	function debug_to_console( $data ) {
		if ( is_array( $data ) ) {
			$output = "<script>console.log( 'Debug Objects: " . implode( ',', $data ) . "' );</script>";
		} else {
			$output = "<script>console.log( 'Debug Objects: " . ( $data ) . "' );</script>";
		}
		echo( $output );
	}

	/**
	 * @param $sizes
	 *
	 * @return array
	 */
	function dima_stop_image_sizes( $sizes ) {
		return array();
	}

	/**
	 * Respond to AJAX requests.
	 */
	function ajax_dima_importer() {

		if ( current_user_can( 'manage_options' ) ) {
			if ( ! defined( 'WP_LOAD_IMPORTERS' ) ) {
				define( 'WP_LOAD_IMPORTERS', true );
			} // we are loading importers


			$this->import_content_types = array();
			if ( isset( $_POST['contentTypes'] ) && is_array( $_POST['contentTypes'] ) ) {
				// @codingStandardsIgnoreLine
				$this->import_content_types = wp_unslash( $_POST['contentTypes'] );
			}

			if ( empty( $this->import_content_types ) ) {
				return;
			}

			if ( ! class_exists( 'WP_Importer' ) ) {
				$wp_importer = ABSPATH . 'wp-admin/includes/class-wp-importer.php';
				include $wp_importer;
			}

			if ( ! class_exists( 'WXR_Importer' ) ) { // If WP importer doesn't exist.
				include get_template_directory() . '/data/importer/class-logger.php';
				include get_template_directory() . '/data/importer/class-logger-html.php';
				include get_template_directory() . '/data/importer/class-wxr-importer.php';
			}

			if ( class_exists( 'WP_Importer' ) && class_exists( 'WXR_Importer' ) ) {
				if ( ! isset( $_POST['demo_type'] ) || trim( $_POST['demo_type'] ) == '' ) {
					$demo_folder_slug = 'noor_main';
				} else {
					$demo_folder_slug = $_POST['demo_type'];
				}

				if ( ! isset( $_POST['demo_home_title'] ) || trim( $_POST['demo_home_title'] ) == '' ) {
					$demo_home_title = 'Home';
				} else {
					$demo_home_title = $_POST['demo_home_title'];
				}

				if ( ! isset( $_POST['is_shop_demo'] ) || trim( $_POST['is_shop_demo'] ) == '' ) {
					$demo_is_shop_demo = true;
				} else {
					$demo_is_shop_demo = dima_helper::dima_am_i_true( $_POST['is_shop_demo'] );
				}

				if ( ! isset( $_POST['demo_revslider_exists'] ) || trim( $_POST['demo_revslider_exists'] ) == '' ) {
					$demo_revslider_exists = true;
				} else {
					$demo_revslider_exists = dima_helper::dima_am_i_true( $_POST['demo_revslider_exists'] );
				}

				$this->demo_folder_slug = $demo_folder_slug;

				//https://docs.woothemes.com/document/woocommerce-shortcodes/
				$_woopages = array(
					'woocommerce_shop_page_id'            => 'Shop',
					'woocommerce_cart_page_id'            => 'Cart',
					'woocommerce_checkout_page_id'        => 'Checkout',
					'woocommerce_pay_page_id'             => 'Checkout Pay',
					'woocommerce_thanks_page_id'          => 'Order Received',
					'woocommerce_myaccount_page_id'       => 'My Account',
					'woocommerce_edit_address_page_id'    => 'Edit My Address',
					'woocommerce_view_order_page_id'      => 'View Order',
					'woocommerce_change_password_page_id' => 'Change Password',
					'woocommerce_logout_page_id'          => 'Logout',
					'woocommerce_lost_password_page_id'   => 'Lost Password'
				);

				$is_shop_demo     = $demo_is_shop_demo;
				$woopages         = $_woopages;
				$homepage_title   = $demo_home_title;
				$revslider_exists = $demo_revslider_exists;

				add_filter( 'intermediate_image_sizes_advanced', 'dima_stop_image_sizes' );

				# Start Import All
				if ( in_array( "all", $this->import_content_types ) ) {
					if ( $is_shop_demo == false ) {
						$woopages = '';
					}
					$this->fetch_attachments = true;
					/* Import Posts, Pages, Portfolio,Media, Menus */
					$this->dima_import_all_content( $demo_folder_slug, $woopages );
					# set home & blog page
					$homepage = get_page_by_title( $homepage_title );
					if ( isset( $homepage ) && $homepage->ID ) {
						update_option( 'page_on_front', $homepage->ID );
						update_option( 'show_on_front', 'page' );
					}

					# menu
					$this->dima_import_menu( $demo_folder_slug );

				} else {

					# Pages
					if ( in_array( "pages", $this->import_content_types ) ) {
						$this->dima_import_custom_content( $demo_folder_slug, 'pages' );

						$homepage = get_page_by_title( $homepage_title );
						if ( isset( $homepage ) && $homepage->ID ) {
							update_option( 'page_on_front', $homepage->ID );
							update_option( 'show_on_front', 'page' );
						}
					}

					# Posts
					if ( in_array( "posts", $this->import_content_types ) ) {
						$this->dima_import_custom_content( $demo_folder_slug, 'posts' );
					}

					# Media
					if ( in_array( "media", $this->import_content_types ) ) {
						$this->fetch_attachments = true;
						$this->dima_import_custom_content( $demo_folder_slug, 'media' );
					}

					# Portfolio
					if ( in_array( "portfolio", $this->import_content_types ) ) {
						$this->dima_import_custom_content( $demo_folder_slug, 'portfolio' );
					}

					# Products
					if ( in_array( "products", $this->import_content_types ) ) {
						$this->dima_import_custom_content( $demo_folder_slug, 'products' );
					}

				}
				# Import Widgets
				if ( in_array( "widgets", $this->import_content_types ) || in_array( "all", $this->import_content_types ) ) {
					$import = false;
					if ( $import ) {
						$this->dima_import_widget( $demo_folder_slug );
					}
				}

				# Import Customizer
				if ( in_array( "theme_options", $this->import_content_types ) || in_array( "all", $this->import_content_types ) ) {
					$this->dima_import_customizer( $demo_folder_slug );
				}

				# Import Rvslider
				if ( in_array( "sliders", $this->import_content_types ) || in_array( "all", $this->import_content_types ) ) {
					$this->dima_import_revslider( $revslider_exists, $demo_folder_slug );
				}

				# Romove Options
				$options = array(
					'dima_space_before_head',
					'dima_space_before_body',
					'dima_space_after_body',
					'dima_google_map_api_key',
				);
				foreach ( $options as $option ) {
					delete_option( $option );
				}


				//page_for_posts
				DIMA_Demos::update_demo( $demo_folder_slug );
			}
			exit;
		}
	}

	public function message() {
		return $this->message;
	}

	public function debugMessage() {
		return $this->debugMessage;
	}

	/**
	 * imported menus
	 *
	 * @param string $demo_name
	 */
	function dima_import_menu( $demo_name = 'noor_main' ) {

		$locations = get_theme_mod( 'nav_menu_locations' );
		$menus     = wp_get_nav_menus();
		if ( $menus ) {

			foreach ( $menus as $menu ) {
				switch ( $demo_name ) {
					case 'noor_main':
						if ( $menu->name == 'primary' ) {
							$locations['primary'] = $menu->term_id;
							$locations['burger']  = $menu->term_id;
						}

						if ( $menu->name == 'icon-menu' ) {
							$locations['icon'] = $menu->term_id;
						}

						// Assign One Page Menu
						$ona_page_menu = get_page_by_title( 'Single Product' );
						if ( isset( $ona_page_menu ) && $ona_page_menu->ID && $menu->name == 'onepage' ) {
							update_post_meta( $ona_page_menu->ID, '_dima_meta_primary_navigation', $menu->term_id );
						}
						break;

					case 'creative_agency':
						if ( $menu->name == 'Creative Agency' ) {
							$locations['burger'] = $menu->term_id;
						}
						break;

					case 'help_center':
					case 'business_and_finance':
						if ( strtolower( $menu->name ) == 'primary' ) {
							$locations['burger'] = $menu->term_id;
						}
						break;

					case 'modern_restaurant':
					case 'construction':
						if ( strtolower( $menu->name ) == 'primary' ) {
							$locations['primary'] = $menu->term_id;
							$locations['burger']  = $menu->term_id;
						}
						break;

					default:
						if ( strtolower( $menu->name ) == 'primary' ) {
							$locations['primary'] = $menu->term_id;
						}
						break;
				}
			}
		}

		set_theme_mod( 'nav_menu_locations', $locations ); // set menus to locations
	}

	/**
	 * Based on our current seesion, come up with the next response
	 * to the client.
	 *
	 * @param string $demo_name
	 * @param        $woopages :array Array containing data for the client.
	 */
	function dima_import_all_content( $demo_name = '', $woopages ) {

		$theme_xml_file = self::dima_xml_replacements( $demo_name, 'all' );

		/** @noinspection PhpUndefinedClassInspection */
		$logger = new WP_Importer_Logger_HTML();
		/** @noinspection PhpUndefinedClassInspection */
		$importer = new WXR_Importer( array(
			'fetch_attachments'      => $this->fetch_attachments,
			'prefill_existing_posts' => false,
		) );

		$importer->set_logger( $logger );

		ob_start();
		$importer->import( $theme_xml_file );
		ob_end_clean();

		if ( $woopages != '' ) {
			foreach ( $woopages as $woo_page_name => $woo_page_title ) {
				$woopage = get_page_by_title( $woo_page_title );
				if ( isset( $woopage ) && $woopage->ID ) {
					update_option( $woo_page_name, $woopage->ID ); // Front Page
				}
			}
			// We no longer need to install pages
			delete_option( '_wc_needs_pages' );
			delete_transient( '_wc_activation_redirect' );
		}

		self::dima_old_xml_replacements( $demo_name, "all" );
		flush_rewrite_rules();
	}

	function dima_import_custom_content( $demo_name = '', $content ) {

		$xml_file = self::dima_xml_replacements( $demo_name, $content );
		/** @noinspection PhpUndefinedClassInspection */
		$logger = new WP_Importer_Logger_HTML();
		/** @noinspection PhpUndefinedClassInspection */
		$importer = new WXR_Importer( array(
			'fetch_attachments'      => $this->fetch_attachments,
			'prefill_existing_posts' => false,
		) );
		$importer->set_logger( $logger );

		ob_start();
		$importer->import( $xml_file );
		ob_end_clean();

		#rest old xml file after the change we made
		self::dima_old_xml_replacements( $demo_name, $content );
		flush_rewrite_rules();
	}

	function dima_import_customizer( $demo_name ) {
		$dima_customizer_file = get_template_directory() . '/data/demos/' . esc_attr( $demo_name ) . '/content/customizer.json';

		if ( ! empty( $dima_customizer_file ) ) {
			$file_name       = $dima_customizer_file;
			$encoded_options = dima_helper::get_local_file_contents( $file_name );
			$options         = json_decode( $encoded_options, true );
			foreach ( $options as $key => $value ) {
				update_option( $key, $value );
			}
		}
	}

	function dima_import_revslider( $revslider_exists, $demo_name_slug ) {

		if ( ! class_exists( 'RevSliderSlider' ) ) {
			return false;
		}

		$rev_directory = get_template_directory() . '/data/demos/' . esc_attr( $demo_name_slug ) . '/revsliders/';

		// IF revslider is activated
		if ( class_exists( 'UniteFunctionsRev' ) && $revslider_exists == true ) {

			$slider_array = $this->dima_get_import_files( $rev_directory, 'zip' );
			$slider       = new RevSlider();
			foreach ( $slider_array as $filepath ) {
				try {
					ob_start();
					$slider->importSliderFromPost( true, true, $filepath );
					ob_clean();
					ob_end_clean();

				} catch ( Exception $e ) {
					return new WP_Error( 'noor', $e->getMessage() );
				}
			}
		}
		$this->message = esc_html__( 'Revolution Slider downloaded...', 'noor' );

		return 'text';
	}

	private static function clear_widgets() {
		$sidebars = wp_get_sidebars_widgets();
		$inactive = isset( $sidebars['wp_inactive_widgets'] ) && is_array( $sidebars['wp_inactive_widgets'] ) ? $sidebars['wp_inactive_widgets'] : array();

		unset( $sidebars['wp_inactive_widgets'] );

		foreach ( $sidebars as $sidebar => $widgets ) {
			if ( is_array( $widgets ) ) {
				$inactive = array_merge( $inactive, $widgets );
			}
			$sidebars[ $sidebar ] = array();
		}

		$sidebars['wp_inactive_widgets'] = $inactive;
		wp_set_sidebars_widgets( $sidebars );
	}

	function dima_import_widget( $demo_name = '' ) {
		$import_file = get_template_directory() . '/data/demos/' . esc_attr( $demo_name ) . '/content/widget_data.json';

		if ( isset( $import_file ) && $import_file ) {
			self::clear_widgets();
			$widgets_json = dima_helper::get_local_file_contents( $import_file );
			$this->dima_import_widget_data( $widgets_json );
		}
	}

	/** ---------------------------------------------------------------------------
	 * Parse JSON import file
	 * http://wordpress.org/plugins/widget-settings-importexport/
	 *
	 * @param $json_data
	 */
	function dima_import_widget_data( $json_data ) {
		$json_data    = json_decode( $json_data, true );
		$sidebar_data = $json_data[0];
		$widget_data  = $json_data[1];

		foreach ( $widget_data as $widget_data_title => $widget_data_value ) {
			$widgets[ $widget_data_title ] = array();
			foreach ( $widget_data_value as $widget_data_key => $widget_data_array ) {
				if ( is_int( $widget_data_key ) ) {
					$widgets[ $widget_data_title ][ $widget_data_key ] = 'on';
				}
			}
		}
		unset( $widgets[''] );

		foreach ( $sidebar_data as $title => $sidebar ) {
			$count = count( $sidebar );
			for ( $i = 0; $i < $count; $i ++ ) {
				$widget               = array();
				$widget['type']       = trim( substr( $sidebar[ $i ], 0, strrpos( $sidebar[ $i ], '-' ) ) );
				$widget['type-index'] = trim( substr( $sidebar[ $i ], strrpos( $sidebar[ $i ], '-' ) + 1 ) );
				if ( ! isset( $widgets[ $widget['type'] ][ $widget['type-index'] ] ) ) {
					unset( $sidebar_data[ $title ][ $i ] );
				}
			}
			$sidebar_data[ $title ] = array_values( $sidebar_data[ $title ] );
		}

		foreach ( $widgets as $widget_title => $widget_value ) {
			foreach ( $widget_value as $widget_key => $widget_value ) {
				$widgets[ $widget_title ][ $widget_key ] = $widget_data[ $widget_title ][ $widget_key ];
			}
		}

		$sidebar_data = array( array_filter( $sidebar_data ), $widgets );
		self::dima_parse_import_data( $sidebar_data );
	}

	/** ---------------------------------------------------------------------------
	 * Import widgets
	 * http://wordpress.org/plugins/widget-settings-importexport/
	 *
	 * @param $import_array
	 *
	 * @return bool
	 */
	function dima_parse_import_data( $import_array ) {
		$sidebars_data    = $import_array[0];
		$widget_data      = $import_array[1];
		$current_sidebars = get_option( 'sidebars_widgets' );
		$new_widgets      = array();

		foreach ( $sidebars_data as $import_sidebar => $import_widgets ) :

			foreach ( $import_widgets as $import_widget ) :
				//if the sidebar exists
				if ( array_key_exists( $import_sidebar, $current_sidebars ) ) :
					$title               = trim( substr( $import_widget, 0, strrpos( $import_widget, '-' ) ) );
					$index               = trim( substr( $import_widget, strrpos( $import_widget, '-' ) + 1 ) );
					$current_widget_data = get_option( 'widget_' . $title );
					$new_widget_name     = self::dima_get_new_widget_name( $title, $index );
					$new_index           = trim( substr( $new_widget_name, strrpos( $new_widget_name, '-' ) + 1 ) );

					if ( ! empty( $new_widgets[ $title ] ) && is_array( $new_widgets[ $title ] ) ) {
						while ( array_key_exists( $new_index, $new_widgets[ $title ] ) ) {
							$new_index ++;
						}
					}
					$current_sidebars[ $import_sidebar ][] = $title . '-' . $new_index;
					if ( array_key_exists( $title, $new_widgets ) ) {
						$new_widgets[ $title ][ $new_index ] = $widget_data[ $title ][ $index ];
						$multiwidget                         = $new_widgets[ $title ]['_multiwidget'];
						unset( $new_widgets[ $title ]['_multiwidget'] );
						$new_widgets[ $title ]['_multiwidget'] = $multiwidget;
					} else {
						$current_widget_data[ $new_index ] = $widget_data[ $title ][ $index ];
						$current_multiwidget               = array_key_exists( '_multiwidget', $current_widget_data ) ? $current_widget_data['_multiwidget'] : false;
						$new_multiwidget                   = array_key_exists( '_multiwidget', $widget_data[ $title ] ) ? $widget_data[ $title ]['_multiwidget'] : false;
						$multiwidget                       = ( $current_multiwidget != $new_multiwidget ) ? $current_multiwidget : 1;
						unset( $current_widget_data['_multiwidget'] );
						$current_widget_data['_multiwidget'] = $multiwidget;
						$new_widgets[ $title ]               = $current_widget_data;
					}

				endif;
			endforeach;
		endforeach;

		if ( isset( $new_widgets ) && isset( $current_sidebars ) ) {
			update_option( 'sidebars_widgets', $current_sidebars );

			foreach ( $new_widgets as $title => $content ) {
				$content = apply_filters( 'widget_data_import', $content, $title );
				update_option( 'widget_' . $title, $content );
			}

			return true;
		}

		return false;
	}

	/**
	 * Get the new widget name.
	 *
	 * @since 1.0.0
	 *
	 * @param string $widget_name The widget-name.
	 * @param int    $widget_index The index of the widget.
	 *
	 * @return array
	 */

	/** ---------------------------------------------------------------------------
	 * Get new widget name
	 * http://wordpress.org/plugins/widget-settings-importexport/
	 *
	 * @param $widget_name
	 * @param $widget_index
	 *
	 * @return string
	 */
	function dima_get_new_widget_name( $widget_name, $widget_index ) {
		$current_sidebars = get_option( 'sidebars_widgets' );
		$all_widget_array = array();
		foreach ( $current_sidebars as $sidebar => $widgets ) {
			if ( ! empty( $widgets ) && is_array( $widgets ) && $sidebar != 'wp_inactive_widgets' ) {
				foreach ( $widgets as $widget ) {
					$all_widget_array[] = $widget;
				}
			}
		}
		while ( in_array( $widget_name . '-' . $widget_index, $all_widget_array ) ) {
			$widget_index ++;
		}
		$new_widget_name = $widget_name . '-' . $widget_index;

		return $new_widget_name;
	}

	/**
	 * Work on php 5.2.11 and above
	 *
	 * @param $directory
	 * @param $extension
	 *
	 * @return array
	 */
	function dima_get_import_files( $directory, $extension ) {
		$files              = array();
		$extension          = '/^.*\.' . esc_html( $extension ) . '$/';
		$directory_iterator = new RecursiveDirectoryIterator( $directory );
		$recusive_iterator  = new RecursiveIteratorIterator( $directory_iterator );
		$regex_iterator     = new RegexIterator( $recusive_iterator, $extension );

		foreach ( $regex_iterator as $file ) {
			$files[] = $file->getPathname();
		}

		return $files;
	}

	/**
	 * Fixes menus paths in xml files.
	 *
	 * @param        $demo
	 * @param string $type
	 *
	 * @access private
	 * @since 1.0.0
	 * @return string
	 */
	private function dima_xml_replacements( $demo, $type = 'posts' ) {

		# Get the files path.
		$xml_file_path = get_template_directory() . '/data/demos/' . esc_attr( $demo ) . '/content/' . $type . '.xml';

		# Initialize the filesystem.
		$wp_filesystem = dima_Helper::dima_init_filesystem();

		# Get the files contents.
		$xml_content           = $wp_filesystem->get_contents( $xml_file_path );
		$this->xml_old_content = $xml_content;

		# Replace placeholders.
		$home_url = untrailingslashit( get_home_url() );

		$demo = str_replace( '_', '-', $demo );

		if ( $demo == 'noor-main' ) {
			$url    = 'noor.pixeldima.com';
			$assets = 'http://noor.pixeldima.com/wp-content/';
		} else {
			$url    = 'noor.pixeldima.com/' . $demo;
			$assets = 'http://noor.pixeldima.com/' . $demo . '/wp-content/';
		}

		# Replace URLs.
		$xml_content = str_replace(
			array(
				'http://' . $url,
				'https://' . $url,
			),
			$home_url,
			$xml_content
		);

		# Change emails.
		$xml_content = str_replace(
			'@gmail.com',
			'@example.com',
			$xml_content
		);

		# Make sure assets are still from the remote server.
		# We can use http instead of https here for performance reasons
		# since static assets don't require https anyway.
		if ( $type == 'media' || $type == 'all' ) {
			$xml_content = str_replace(
				$home_url . '/wp-content/',
				$assets,
				$xml_content
			);
		}

		# Take care of assets.
		$xml_content = preg_replace_callback( '/(?<=<wp:meta_value><!\[CDATA\[)(https?:\/\/noor.pixeldima.com)+(.*?)(?=]]><)/', 'dima_importer_replace_url', $xml_content );

		# Write files.
		$wp_filesystem->put_contents( $xml_file_path, $xml_content );

		return $xml_file_path;
	}

	private function dima_old_xml_replacements( $demo, $type = 'posts' ) {
		$xml_file_path = get_template_directory() . '/data/demos/' . esc_attr( $demo ) . '/content/' . $type . '.xml';
		# Initialize the filesystem.
		$wp_filesystem = dima_Helper::dima_init_filesystem();
		# replace old content
		$wp_filesystem->put_contents( $xml_file_path, $this->xml_old_content );
	}


	/**
	 * Replaces URLs.
	 *
	 * @since 1.0.0
	 *
	 * @param array $matches The matches.
	 *
	 * @return string
	 */
	function dima_importer_replace_url( $matches ) {
		// Get the uploads folder.
		$wp_upload_dir = wp_upload_dir();
		if ( is_array( $matches ) ) {
			foreach ( $matches as $key => $match ) {
				if ( false !== strpos( $match, 'wp-content/uploads/sites/' ) ) {
					$parts = explode( 'wp-content/uploads/sites/', $match );
					if ( isset( $parts[1] ) ) {
						$sub_parts = explode( '/', $parts[1] );
						unset( $sub_parts[0] );
						$parts[1] = implode( '/', $sub_parts );

						// append the url to the uploads url.
						$parts[0] = $wp_upload_dir['baseurl'];

						return implode( '/', $parts );
					}
				}
			}
		}

		return $matches;
	}
}


class DIMA_Demo_Admin_Panle {
	const VERSION = '1.0.0';

	/**
	 * Display demo
	 */
	public function dima_display_demo( $demo ) {

		$name              = $demo['name'];
		$alise             = $demo['alise'];
		$homepage_title    = $demo['homepage_title'];
		$demo_is_shop      = $demo['is_shop_demo'];
		$revslider_exists  = $demo['revslider_exists'];
		$preview_url       = $demo['preview'];
		$screenshot        = $demo['screenshot'];
		$des               = $demo['des'];
		$is_home           = $demo['is_home'];
		$requirement       = $demo['requirement'];
		$requirement_other = $demo['requirement_other'];
		$import_exception  = isset( $demo['import_exception'] ) ? 'data-import=' . implode( ",", $demo['import_exception'] ) . '' : '';

		if ( $des != '' ) {
			$des = '<span class="tippy dima-help" title="' . esc_attr( $des ) . '">' . dima_get_svg_icon( "ic_help" ) . '</span>';
		}

		$screenshot = DIMA_TEMPLATE_URL . '/data/demos/' . sanitize_title( $alise ) . '/' . esc_attr( $screenshot ) . '.png';

		$active_theme = DIMA_Demos::get_installed_demo();
		$class        = "isactive_theme";

		$installed = '';
		if ( $active_theme == $alise ) {
			$installed = '<span class="status active">' . dima_get_svg_icon( esc_attr( "ic_done" ) ) . '</span>';
			$button    = '<input id="install" type="submit" value="' . esc_html__( "Install", 'noor' ) . '" class="standard button" data-demo-name="' . esc_attr( $alise ) . '" data-demo-home-title="' . esc_attr( $homepage_title ) . '" data-demo-is-shop="' . esc_attr( $demo_is_shop ) . '" data-demo-revslider-exists="' . esc_attr( $revslider_exists ) . '">';

			if ( $is_home ) {
				$installed = '<span class="status active is_home">' . dima_get_svg_icon( esc_attr( "ic_home" ) ) . '</span>';
			}

		} else {
			$button = '<input id="install" type="submit" value="' . esc_html__( "Install", 'noor' ) . '"  class="standard button"  data-demo-name="' . esc_attr( $alise ) . '" data-demo-home-title="' . esc_attr( $homepage_title ) . '" data-demo-is-shop="' . esc_attr( $demo_is_shop ) . '" data-demo-revslider-exists="' . esc_attr( $revslider_exists ) . '">';
			if ( $is_home ) {
				$installed = '<span class="status not_active is_home">' . dima_get_svg_icon( esc_attr( "ic_home" ) ) . '</span>';
			}
		}


		$Preview = '<a href="' . esc_url( $preview_url ) . '" target="_blank" rel="noopener" class="demo-preview button" >' . esc_html__( 'Preview', 'noor' ) . '</a>';

		$requirement_output = '';
		if ( $requirement != '' || $requirement_other != '' ) {

			$requirement_output = '<div class="demo-desc hide"  ' . esc_attr( $import_exception ) . '>';

			if ( $requirement != '' ) {
				$requirement_output .= '<h4>' . esc_html__( 'Required Plugins', 'noor' ) . '</h4>';
				$requirement_output .= '<ul class="requirement_plugins">';
				foreach ( $requirement as $req ) {
					$requirement_output .= '<li>' . wp_kses( $req, dima_helper::dima_get_allowed_html_tag() ) . '</li>';
				}
				$requirement_output .= '</ul>';
			}
			if ( $requirement_other != '' ) {
				$requirement_output .= '<br><h4>' . esc_html__( 'Demo Requirement', 'noor' ) . '</h4>';
				$requirement_output .= '<ul class="requirement_other">';
				foreach ( $requirement_other as $req ) {
					$requirement_output .= '<li>' . wp_kses( $req, dima_helper::dima_get_allowed_html_tag() ) . '</li>';
				}
				$requirement_output .= '</ul>';
			}
			$requirement_output .= '</div>';
		}
		echo ' <li class="pixeldima-demo ' . esc_attr( $class ) . '" id="dima-demo-submit">'
		     . $requirement_output
		     . '<div class="pixeldima-demo-screenshot">'
		     . '' . wp_kses( $installed, dima_helper::dima_get_allowed_html_tag() ) . ''
		     . '<img src="' . esc_url( $screenshot ) . '" class="img">'
		     . '</div>'

		     . '<div class="pixeldima-demo-title">'
		     . '<h4 class="title">' . esc_attr( $name )
		     . '</h4>'
		     . $des
		     . '</div>'

		     . '<div class="info">'
		     . '<div class="theme-actions">'
		     . '<div class="btn">'
		     . html_entity_decode( $button )
		     . '</div>'
		     . '<div class="btn">' . html_entity_decode( $Preview ) . '</div>'
		     . '</div>'
		     . '</div>'
		     . '</li>';
	}

	public function dima_display_soon_demo( $screenshot = '' ) {
		echo ' <li class="pixeldima-demo" id="dima-demo-submit">'
		     . '<div class="pixeldima-demo-screenshot">'
		     . '<img src="' . esc_url( $screenshot ) . '" class="img">'
		     . '</div>'
		     . '</li>';
	}

}


global $dima_demo_import_class;
$dima_demo_import_class = new DIMA_Demo_Admin_Panle();

$dima_demo_ajax = new DIMA_Importer_Admin_();