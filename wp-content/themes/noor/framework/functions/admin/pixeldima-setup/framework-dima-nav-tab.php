<?php
/**
 * Setup : NavTab
 *
 * @package Dima Framework
 * @subpackage Admin Setup
 * @version   1.0.0
 * @since     1.0.0
 * @author    PixelDima <info@pixeldima.com>
 *
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

#!do not rename this global variable.
global $submenu;
$dima_menu_items = '';
if ( isset( $submenu['pixel-dima-dashboard'] ) ) {
	$dima_menu_items = $submenu['pixel-dima-dashboard'];
}


/*-----------------------------------------------------------------------------------*/
# Welcome Page ARGS
/*-----------------------------------------------------------------------------------*/

function dima_head_section() {
	$welcome_args = array(
		'title'   => sprintf( esc_html__( 'Welcome to %s', 'noor' ), DIMA_THEME_NAME ),
		'about'   => '',
		'color'   => '#333333',
		'img'     => '',
		'version' => '',
	);

	$welcome_args = apply_filters( 'dima_welcome_args', $welcome_args );
	$min          = 1;
	$max          = 2;
	$class        = rand( $min, $max );
	$logo         = DIMA_TEMPLATE_URL . '/framework/images/dashboard-img/logo-2.png';

	?>
    <div class="dima-welcome-panel dima-panel-bg<?php echo esc_attr( $class ); ?>">
        <div class="pixeldima-warp-welcome">
            <span class="dima-dash-logo"><img src="<?php echo esc_url( $logo ); ?>" alt=""></span>
            <span class="dima-version">V.<?php echo DIMA_VERSION ?></span>
            <h1><?php echo esc_html( $welcome_args['title'] ) ?></h1>
            <p><?php echo sprintf( esc_html__( 'You are awesome! Thanks for using our theme, %s is now installed and ready to use! Get ready to build something beautiful :)', 'noor' ), DIMA_THEME_NAME ); ?></p>
            <br><br>
        </div>
    </div>
	<?php
}

/*-----------------------------------------------------------------------------------*/
# ! Welcome Page ARGS
/*-----------------------------------------------------------------------------------*/

if ( is_array( $dima_menu_items ) ) {
	?>
    <div class="about-wrap dima-admin-header ">
		<?php dima_head_section(); ?>
        <h2 class="nav-tab-wrapper">

			<?php
			foreach ( $dima_menu_items as $dima_menu_item ) {
				?>
                <a href="admin.php?page=<?php echo esc_attr( $dima_menu_item[2] ) ?>"
                   class="nav-tab <?php if ( isset( $_GET['page'] ) and $_GET['page'] == $dima_menu_item[2] ) {
					   echo 'nav-tab-active';
				   } ?> "><?php echo esc_attr( $dima_menu_item[0] ) ?></a>
				<?php
			}
			?>
        </h2>
    </div>
	<?php
}
?>