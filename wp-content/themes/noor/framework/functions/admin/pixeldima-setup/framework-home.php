<?php
/**
 * Setup : Welcome Page
 *
 * @package Dima Framework
 * @subpackage Admin Setup
 * @version   1.0.0
 * @since     1.0.0
 * @author    PixelDima <info@pixeldima.com>
 *
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

function dima_setup_page_home() {
	$pixeldima_setup_path = DIMA_TEMPLATE_PATH . '/framework/functions/admin/pixeldima-setup';
	require_once( $pixeldima_setup_path . '/framework-dima-nav-tab.php' );
	?>

    <div class="about-wrap pixeldima-home">

		<?php
		ob_start();
		?>
        <h2><?php printf( esc_html__( 'Need help? We\'re here %s', 'noor' ), '&#x1F60A' ); ?></h2>

        <div class="centerd-btn dima-notice dima-warning">
            <div class="dima-notice-content">
                <p><?php printf( wp_kses_post( DIMA_THEME_NAME . ' comes with 6 months of free support for every license you purchase. Support can be <a target="_blank" href="%1s">extended through subscriptions via ThemeForest</a>. All support is handled through our <a target="_blank" href="%2s">support center</a>. Below are all the resources we offer in our support center.', 'noor' ), 'https://help.market.envato.com/hc/en-us/articles/207886473-Extending-and-Renewing-Item-Support', 'https://pixeldima.com/support/' ); ?></p>
            </div>
        </div>

        <div class="changelog">
            <div class="feature-section">

                <div class="three">
                    <h3>
                        <span class="dashicons dashicons dashicons-book"></span><?php esc_html_e( 'Knowledge Base', 'noor' ); ?>
                    </h3>
                    <p><?php esc_html_e( 'We recommend you to browse through the Knowledge Base which cover most of your questions.', 'noor' ); ?></p>
                    <a href="http://pixeldima.com/knowledgebase/" target="_blank" rel="noopener"
                       class="button button-primary"><?php esc_html_e( 'Open Knowledge Base', 'noor' ); ?></a>
                </div>

                <div class="three">
                    <h3><span
                                class="dashicons dashicons-sos"></span> <?php echo esc_html__( 'Submit A Ticket', 'noor' ); ?>
                    </h3>
                    <p><?php esc_html_e( 'Need one-to-one assistance? Get in touch with our Support team.', 'noor' ) ?></p>
                    <br>
                    <a href="http://pixeldima.com/support/" target="_blank" rel="noopener"
                       class="button button-primary"><?php esc_html_e( 'Submit A Ticket', 'noor' ); ?></a>
                </div>

                <div class="three">
                    <h3>
                        <span class="dashicons dashicons-info"></span> <?php echo esc_html__( 'Troubleshooting', 'noor' ); ?>
                    </h3>
                    <p><?php esc_html_e( 'If something is not working as expected, Please try these common solutions.', 'noor' ); ?></p>
                    <br>
                    <a href="https://pixeldima.com/support/troubleshooting/" target="_blank" rel="noopener"
                       class="button button-primary"><?php esc_html_e( 'Open Troubleshooting Page', 'noor' ); ?></a>
                </div>

            </div>

        </div>

        <hr/>

        <ul id="follow-pixeldima">
            <li class="follow-pixeldima-fb">
                <div id="fb-root"></div>
                <script>(function (d, s, id) {
                        var js, fjs = d.getElementsByTagName(s)[0];
                        if (d.getElementById(id)) return;
                        js = d.createElement(s);
                        js.id = id;
                        js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.9&appId=280065775530401";
                        fjs.parentNode.insertBefore(js, fjs);
                    }(document, 'script', 'facebook-jssdk'));</script>
                <div class="fb-like" data-href="https://facebook.com/pixeldima" data-layout="button_count"
                     data-action="like" data-size="large" data-show-faces="false" data-share="false"></div>
            </li>

            <li class="follow-pixeldima-twitter">
                <a href="https://twitter.com/pixeldima" class="twitter-follow-button" data-size="large"
                   data-show-count="false">Follow @pixeldima</a>
				<?php
				wp_enqueue_script( 'twitter-widgets', '//platform.twitter.com/widgets.js' );
				?>
            </li>
        </ul>


		<?php $output = ob_get_contents();
		ob_end_clean(); ?>

		<?php echo apply_filters( 'dima_addons_home_content', $output ); ?>

    </div>

<?php }