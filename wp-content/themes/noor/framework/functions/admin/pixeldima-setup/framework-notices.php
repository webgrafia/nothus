<?php
/**
 * Dashboard Notices
 *
 * @package Noor
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly


/*-----------------------------------------------------------------------------------*/
# Notices
/*-----------------------------------------------------------------------------------*/
function dima_admin_notice_message( $args = array() ) {

	$defaults = array(
		'notice_id'      => '',
		'title'          => esc_html__( 'Howdy', 'noor' ),
		'img'            => false,
		'message'        => '',
		'dismissible'    => true,
		'color'          => '',
		'class'          => '',
		'standard'       => true,
		'button_text'    => '',
		'button_class'   => '',
		'button_url'     => '',
		'button_2_text'  => '',
		'button_2_class' => '',
		'button_2_url'   => '',
	);

	$args = wp_parse_args( $args, $defaults );


	if ( ! empty( $args['color'] ) ) {
		$args['color'] = 'background-color:' . esc_attr( $args['color'] );
	}

	if ( $args['class'] ) {
		$args['class'] = 'dima-' . esc_attr( $args['class'] );
	}

	if ( $args['standard'] ) {
		$args['class'] .= ' notice';
	}

	if ( $args['dismissible'] ) {
		$args['class'] .= ' is-dismissible';
	}

	if ( ! empty( $args['button_class'] ) ) {
		$args['button_class'] = 'dima-button-' . esc_attr( $args['button_class'] );
	}

	if ( ! empty( $args['button_2_class'] ) ) {
		$args['button_2_class'] = 'dima-button-' . esc_attr( $args['button_2_class'] );
	}

	?>

    <div id="<?php echo esc_attr( $args['notice_id'] ) ?>"
         class="centerd-btn dima-notice <?php echo esc_attr( $args['class'] ); ?>">
        <h3 style="<?php echo esc_attr( $args['color'] ); ?>"><?php echo esc_html( $args['title'] ) ?></h3>

        <div class="dima-notice-content">

			<?php
			if ( ! empty( $args['img'] ) ) { ?>
                <img src="<?php echo esc_attr( $args['img'] ); ?>" class="dima-notice-img" alt="">
				<?php
			}
			?>

			<?php

			if ( strpos( $args['message'], '<p>' ) === false ) {
				$args['message'] = '<p>' .  wp_kses( $args['message'], dima_helper::dima_get_allowed_html_tag() ) . '</p>';
			}
			echo wp_kses( $args['message'], dima_helper::dima_get_allowed_html_tag() ) ;
			?>

            <br>

			<?php
			if ( ! empty( $args['button_text'] ) ) { ?>
                <a class="dima-primary-button button button-primary button-hero <?php echo esc_attr( $args['button_class'] ) ?>"
                   href="<?php echo esc_url( $args['button_url'] ) ?>"><?php echo esc_html( $args['button_text'] ) ?></a>
				<?php
			}
			?>

			<?php
			if ( ! empty( $args['button_2_text'] ) ) { ?>
                <a class="dima-primary-button button button-primary button-hero <?php echo esc_attr( $args['button_2_class'] ) ?>"
                   href="<?php echo esc_url( $args['button_2_url'] ) ?>"><?php echo esc_html( $args['button_2_text'] ) ?></a>
				<?php
			}
			?>

        </div>
    </div>

	<?php
}
