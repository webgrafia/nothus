<?php
/**
 * Setup : DIMA Plugins
 *
 * @package Dima Framework
 * @subpackage Admin Setup
 * @version   1.0.0
 * @since     1.0.0
 * @author    PixelDima <info@pixeldima.com>
 *
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

function dima_setup_plugins() {
	$pixeldima_setup_path = DIMA_TEMPLATE_PATH . '/framework/functions/admin/pixeldima-setup';
	require_once( $pixeldima_setup_path . '/framework-dima-nav-tab.php' );
	?>
    <div class="about-wrap pixeldima-home">

		<?php
		dima_admin_notice_message( array(
				'notice_id'   => 'theme_not_authorized',
				'title'       => esc_html__( 'Plugins', 'noor' ),
				'message'     => sprintf( esc_html__( 'All the plugins are well tested to work with %s and we keep them up to date. The themes comes packed with the following premium and free plugins', 'noor' ), DIMA_THEME_NAME ),
				'dismissible' => false,
				'class'       => 'standar',
				'standard'    => false,
			)
		);

		?>

        <ul class="pixeldima-extensions-list">
			<?php
			$plugins = TGM_Plugin_Activation::$instance->plugins;

			foreach ( $plugins as $plugin ) : ?>
				<?php
				//Installed
				if ( dima_helper::dima_is_plugin_exists( $plugin['dima_plugin'] ) ) :

					if ( is_plugin_active( $plugin['dima_plugin'] ) ) {
						$status         = 'active';
						$status_message = 'Active';
					} else {
						$status         = 'inactive';
						$status_message = 'Inactive';
					}
					if ( $status == 'inactive' ) {
						$calss = 'dima-button-warning';
					} else {
						$calss = 'dima-button-green';
					}
					$button = '<a class="button dima-primary-button ' . esc_attr( $calss ) . '" href="' . admin_url( 'plugins.php' ) . '">Manage Plugin</a>';
				//Not Installed
				else :
					$install_type = 'install';

					$url            =
						wp_nonce_url(
							add_query_arg(
								array(
									'page'                               => urlencode( TGM_Plugin_Activation::$instance->menu ),
									'plugin'                             => urlencode( $plugin['slug'] ),
									'plugin_name'                        => urlencode( $plugin['name'] ),
									'plugin_source'                      => urlencode( $plugin['source'] ),
									'tgmpa-install'                      => 'install-plugin',
									'tgmpa-' . esc_attr( $install_type ) => esc_attr( $install_type ) . '-plugin',
								)
								, admin_url( TGM_Plugin_Activation::$instance->parent_slug )
							),
							'tgmpa-' . esc_attr( $install_type ),
							'tgmpa-nonce'
						);
					$text           = 'Install Plugin';
					$class          = 'button dima-primary-button button-primary';
					$status         = 'not-installed';
					$status_message = 'Not Installed';
					$button         = '<a class="' . esc_attr( $class ) . '" href="' . esc_url( $url ) . '">' . esc_attr( $text ) . '</a>';
				endif;

				?>

                <li class="pixeldima-extension <?php echo esc_attr( $status ); ?>"
                    id="<?php echo esc_attr( $plugin['slug'] ); ?>">
                    <div class="pixeldima-extension-content">

						<?php if ( isset( $plugin['required'] ) && $plugin['required'] ) : ?>
                            <div class="dima_plugin-required">
								<?php esc_html_e( 'Required', 'noor' ); ?>
                            </div>
						<?php endif; ?>

                        <img src="<?php echo esc_url( $plugin['dima_logo'] ); ?>" class="img">
                        <div class="info">
                            <h4 class="title"><?php echo esc_attr( $plugin['name'] ); ?><span
                                        class="plugin-author">-<?php echo esc_attr( $plugin['dima_author'] ); ?>-</span>
                            </h4>
                            <span
                                    class="status <?php echo esc_attr( $status ); ?>"><?php echo esc_attr( $status_message ); ?></span>
                            <p class="desc"><?php echo esc_attr( $plugin['dima_description'] ); ?></p>
                        </div>
                        <div class="btn"><?php echo html_entity_decode( $button ); ?></div>
                    </div>

                </li>

			<?php endforeach; ?>

        </ul>

    </div>

<?php }