<?php
/**
 * Theme setup settings.
 *
 * @package Dima Framework
 * @subpackage Admin Setup
 * @version   1.0.0
 * @since     1.0.0
 * @author    PixelDima <info@pixeldima.com>
 *
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Contain all demos information
 */
function dima_setup_demo_content() {
	$pixeldima_setup_path = DIMA_TEMPLATE_PATH . '/framework/functions/admin/pixeldima-setup';
	require_once( $pixeldima_setup_path . '/framework-dima-nav-tab.php' );
	global $dima_demo_import_class;
	?>

    <div class="about-wrap pixeldima-demo-wrap">
    <div class="pixeldima-header-text">
		<?php

		# is RTL ----------
		if ( is_rtl() ) {
			echo '<p class="dima-message-hint">' . esc_html__( 'All Demos Support RTL.', 'noor' ) . '</p>';
		}

		# First message
		dima_admin_notice_message( array(
				'notice_id'   => 'theme_demos',
				'title'       => sprintf( esc_html__( 'Install %s Demos', 'noor' ), DIMA_THEME_NAME ),
				'message'     => sprintf( esc_html__( "%s brings you a number of unique designs for your website. Installing a demo provides pages, posts, images, theme options, widgets, sliders and more so you don’t have to create everything from scratch. Each demo is fully customizable (fonts, colors and layouts).Please check the System Status tab to ensure your server meets all requirements for a successful import. %s", "noor" ), DIMA_THEME_NAME, '<br><a href="https://www.youtube.com/watch?v=H4oxTfsVx3E" target="_blank"> ' . esc_html__( 'Installation & Demo Import Tutorial', 'noor' ) . '</a>' ),
				'dismissible' => false,
				'class'       => 'standar',
				'standard'    => false,
			)
		);
		?>

		<?php if ( isset( $error ) && is_string( $error ) ) { ?>
            <div class="error"><p><?php echo esc_attr( $error ) ?></p></div>
		<?php }

		# RS message
		if ( ! DIMA_REVOLUTION_SLIDER_IS_ACTIVE ) {
			dima_admin_notice_message( array(
					'notice_id'   => 'theme_not_authorized',
					'title'       => esc_html__( 'Please note', 'noor' ),
					'message'     => esc_html__( 'Since Revolution Slider is not currently active, any sliders used in our Expanded demos will not be setup. If you wish for these sliders to be setup, please ensure that you have Revolution Slider installed and activated.', 'noor' ),
					'dismissible' => false,
					'class'       => 'warning',
					'standard'    => false,
				)
			);
		}
		?>

        <ul class="pixeldima-demos-list">
			<?php

			$requirement = array(
				sprintf( esc_html__( 'Noor Assistant: For all the shortcodes used in %s.', 'noor' ), DIMA_THEME_NAME ),
				esc_html__( 'Slider Revolution: For Sliders.', 'noor' ),
				esc_html__( 'Visual Composer: For page builder.', 'noor' ),
				esc_html__( 'Contact Form 7: For the contact form.', 'noor' ),
				esc_html__( 'WooCommerce: For the Shop section.', 'noor' ),
				esc_html__( 'YITH WooCommerce Wishlist : For adding a Wishlist.', 'noor' ),
				esc_html__( 'WP Page Widget : For adding custom widgets to pages ,posts or custom post type.', 'noor' ),
			);

			$requirement_other = array(
				esc_html__( 'Memory Limit of 256 MB', 'noor' ),
				esc_html__( 'Max execution time (php time limit) of 300 seconds', 'noor' ),
			);

			# 0: demo name
			# 1: demo alise (folder name )
			# 2: preview url
			# 3: screenshot name
			# 4: Info tooltip
			# 5 Is home page : true ( if demo => false
			$demos = array(

				"noor-home-1" => array(
					'name'              => "Noor - Corporate Home",
					'alise'             => "noor_main",
					'homepage_title'    => "Corporate Home",
					'preview'           => "https://noor.pixeldima.com/corporate-home/",
					'screenshot'        => "corporate",
					'des'               => sprintf( esc_html__( "When you choose one of the home pages provides, all other homepages will be installed automatically. You can find it here after you install this demo: %s", 'noor' ), get_site_url() . '/corporate-home/' ),
					'requirement'       => $requirement,
					'requirement_other' => $requirement_other,
					'is_home'           => false,
					'is_shop_demo'      => true,
					'revslider_exists'  => true,
				),

				"noor-home-2" => array(
					'name'              => "Noor - Agency Home",
					'alise'             => "noor_main",
					'slug'              => "agency",
					'preview'           => "https://noor.pixeldima.com/agency/",
					'screenshot'        => "agency",
					'des'               => sprintf( esc_html__( "When you choose one of the home pages provides, all other homepages will be installed automatically. You can find it here after you install this demo: %s", 'noor' ), get_site_url() . '/agency/' ),
					'requirement'       => $requirement,
					'requirement_other' => $requirement_other,
					'homepage_title'    => "Agency",
					'is_home'           => true,
					'is_shop_demo'      => true,
					'revslider_exists'  => true,
				),

				"noor-home-3" => array(
					'name'              => "Noor - Company Home",
					'alise'             => "noor_main",
					'homepage_title'    => "Company",
					'slug'              => "company",
					'preview'           => "https://noor.pixeldima.com/company/",
					'screenshot'        => "company",
					'des'               => sprintf( esc_html__( "When you choose one of the home pages provides, all other homepages will be installed automatically. You can find it here after you install this demo: %s", 'noor' ), get_site_url() . '/company/' ),
					'requirement'       => $requirement,
					'requirement_other' => $requirement_other,
					'is_home'           => true,
					'is_shop_demo'      => true,
					'revslider_exists'  => true,
				),

				"noor-home-4" => array(
					'name'              => "Noor - Digital Agency Home",
					'alise'             => "noor_main",
					'slug'              => "digital-agency",
					'homepage_title'    => "Digital Agency",
					'preview'           => "https://noor.pixeldima.com/digital-agency/",
					'screenshot'        => "digital-agency",
					'des'               => sprintf( esc_html__( "When you choose one of the home pages provides, all other homepages will be installed automatically. You can find it here after you install this demo: %s", 'noor' ), get_site_url() . '/digital-agency/' ),
					'requirement'       => $requirement,
					'requirement_other' => $requirement_other,
					'is_home'           => true,
					'is_shop_demo'      => true,
					'revslider_exists'  => true,
				),

				"noor-home-5" => array(
					'name'              => "Noor - Clothing Shop 1",
					'alise'             => "noor_main",
					'slug'              => "clothing-shop",
					'homepage_title'    => "Clothing Shop",
					'preview'           => "https://noor.pixeldima.com/clothing-shop/",
					'screenshot'        => "shop1",
					'des'               => sprintf( esc_html__( "When you choose one of the home pages provides, all other homepages will be installed automatically. You can find it here after you install this demo: %s", 'noor' ), get_site_url() . '/clothing-shop/' ),
					'requirement'       => $requirement,
					'requirement_other' => $requirement_other,
					'is_home'           => true,
					'is_shop_demo'      => true,
					'revslider_exists'  => true,
				),

				"noor-home-6" => array(
					'name'              => "Noor - Clothing Shop 2",
					'alise'             => "noor_main",
					'slug'              => "shop-clothing-2",
					'homepage_title'    => "Shop Clothing 2",
					'preview'           => "https://noor.pixeldima.com/shop-clothing-2/",
					'screenshot'        => "shop2",
					'des'               => sprintf( esc_html__( "When you choose one of the home pages provides, all other homepages will be installed automatically. You can find it here after you install this demo: %s", 'noor' ), get_site_url() . '/shop-clothing-2/' ),
					'requirement'       => $requirement,
					'requirement_other' => $requirement_other,
					'is_home'           => true,
					'is_shop_demo'      => true,
					'revslider_exists'  => true,
				),

				"noor-home-7" => array(
					'name'              => "Noor - Furniture Shop 1",
					'alise'             => "noor_main",
					'slug'              => "furniture-shop",
					'homepage_title'    => "Furniture Shop",
					'preview'           => "https://noor.pixeldima.com/furniture-shop/",
					'screenshot'        => "shop3",
					'des'               => sprintf( esc_html__( "When you choose one of the home pages provides, all other homepages will be installed automatically. You can find it here after you install this demo: %s", 'noor' ), get_site_url() . '/furniture-shop/' ),
					'requirement'       => $requirement,
					'requirement_other' => $requirement_other,
					'is_home'           => true,
					'is_shop_demo'      => true,
					'revslider_exists'  => true,
				),

				"noor-home-8" => array(
					'name'              => "Noor - Furniture Shop 2",
					'alise'             => "noor_main",
					'slug'              => "furniture-shop-2",
					'homepage_title'    => "Furniture Shop 2",
					'preview'           => "https://noor.pixeldima.com/furniture-shop-2/",
					'screenshot'        => "shop4",
					'des'               => sprintf( esc_html__( "When you choose one of the home pages provides, all other homepages will be installed automatically. You can find it here after you install this demo: %s", 'noor' ), get_site_url() . '/furniture-shop-2/' ),
					'requirement'       => $requirement,
					'requirement_other' => $requirement_other,
					'is_home'           => true,
					'is_shop_demo'      => true,
					'revslider_exists'  => true,
				),

				"noor-home-9" => array(
					'name'              => "Noor - Classic Blog",
					'alise'             => "noor_main",
					'slug'              => "classic-blog",
					'homepage_title'    => "Classic Blog",
					'preview'           => "https://noor.pixeldima.com/classic-blog",
					'screenshot'        => "blog1",
					'des'               => sprintf( esc_html__( "When you choose one of the home pages provides, all other homepages will be installed automatically. You can find it here after you install this demo: %s", 'noor' ), get_site_url() . '/classic-blog/' ),
					'requirement'       => $requirement,
					'requirement_other' => $requirement_other,
					'is_home'           => true,
					'is_shop_demo'      => true,
					'revslider_exists'  => true,
				),

				"noor-home-10" => array(
					'name'              => "Noor - Minimal Blog",
					'alise'             => "noor_main",
					'slug'              => "minimal-blog",
					'homepage_title'    => "Minimal Blog",
					'preview'           => "https://noor.pixeldima.com/minimal-blog",
					'screenshot'        => "blog2",
					'des'               => sprintf( esc_html__( "When you choose one of the home pages provides, all other homepages will be installed automatically. You can find it here after you install this demo: %s", 'noor' ), get_site_url() . '/minimal-blog/' ),
					'requirement'       => $requirement,
					'requirement_other' => $requirement_other,
					'is_home'           => true,
					'is_shop_demo'      => true,
					'revslider_exists'  => true,
				),

				"noor-home-11" => array(
					'name'              => "Noor - Modern Blog",
					'alise'             => "noor_main",
					'slug'              => "modern-blog",
					'homepage_title'    => "Modern Blog",
					'preview'           => "https://noor.pixeldima.com/modern-blog",
					'screenshot'        => "blog3",
					'des'               => sprintf( esc_html__( "When you choose one of the home pages provides, all other homepages will be installed automatically. You can find it here after you install this demo: %s", 'noor' ), get_site_url() . '/modern-blog/' ),
					'requirement'       => $requirement,
					'requirement_other' => $requirement_other,
					'is_home'           => true,
					'is_shop_demo'      => true,
					'revslider_exists'  => true,
				),

				"noor-home-12" => array(
					'name'              => "Noor - Creative Portfolio",
					'alise'             => "noor_main",
					'slug'              => "creative-portfolio",
					'homepage_title'    => "Creative Portfolio",
					'preview'           => "https://noor.pixeldima.com/creative-portfolio/",
					'screenshot'        => "portfolio1",
					'des'               => sprintf( esc_html__( "When you choose one of the home pages provides, all other homepages will be installed automatically. You can find it here after you install this demo: %s", 'noor' ), get_site_url() . '/creative-portfolio/' ),
					'requirement'       => $requirement,
					'requirement_other' => $requirement_other,
					'is_home'           => true,
					'is_shop_demo'      => true,
					'revslider_exists'  => true,
				),

				"noor-home-13" => array(
					'name'              => "Noor - Minimal Portfolio",
					'alise'             => "noor_main",
					'slug'              => "minimal-portfolio",
					'homepage_title'    => "Minimal Portfolio",
					'preview'           => "https://noor.pixeldima.com/",
					'screenshot'        => "portfolio2",
					'des'               => sprintf( esc_html__( "When you choose one of the home pages provides, all other homepages will be installed automatically. You can find it here after you install this demo: %s", 'noor' ), get_site_url() . '/minimal-portfolio/' ),
					'requirement'       => $requirement,
					'requirement_other' => $requirement_other,
					'is_home'           => true,
					'is_shop_demo'      => true,
					'revslider_exists'  => true,
				),

				"noor-home-14" => array(
					'name'              => "Noor - Modern Portfolio",
					'alise'             => "noor_main",
					'slug'              => "modern-portfolio",
					'homepage_title'    => "Modern Portfolio",
					'preview'           => "https://noor.pixeldima.com/modern-portfolio/",
					'screenshot'        => "portfolio3",
					'des'               => sprintf( esc_html__( "When you choose one of the home pages provides, all other homepages will be installed automatically. You can find it here after you install this demo: %s", 'noor' ), get_site_url() . '/modern-portfolio/' ),
					'requirement'       => $requirement,
					'requirement_other' => $requirement_other,
					'is_home'           => true,
					'is_shop_demo'      => true,
					'revslider_exists'  => true,
				),

				"noor-home-15" => array(
					'name'              => "Noor - Single Product",
					'alise'             => "noor_main",
					'slug'              => "single-product",
					'homepage_title'    => "Single Product",
					'preview'           => "https://noor.pixeldima.com/single-product/",
					'screenshot'        => "single-product",
					'des'               => sprintf( esc_html__( "When you choose one of the home pages provides, all other homepages will be installed automatically. You can find it here after you install this demo: %s", 'noor' ), get_site_url() . '/single-product/' ),
					'requirement'       => $requirement,
					'requirement_other' => $requirement_other,
					'is_home'           => true,
					'is_shop_demo'      => true,
					'revslider_exists'  => true,
				),

				"creative_agency" => array(
					'name'              => "Creative Agency",
					'alise'             => "creative_agency",
					'slug'              => "creative-agency",
					'homepage_title'    => "Home",
					'preview'           => "https://noor.pixeldima.com/creative-agency/",
					'screenshot'        => "creative-agency",
					'des'               => "",
					'import_exception'  => array( 'portfolio', 'products' ),
					'is_home'           => false,
					'is_shop_demo'      => false,
					'revslider_exists'  => true,
					'requirement'       => array(
						sprintf( esc_html__( 'Noor Assistant: For all the shortcodes used in %s.', 'noor' ), DIMA_THEME_NAME ),
						esc_html__( 'Visual Composer: For page builder.', 'noor' ),
						esc_html__( 'Slider Revolution: For Sliders.', 'noor' ),
					),
					'requirement_other' => array(
						esc_html__( 'Max execution time (php time limit) of 300 seconds', 'noor' ),
					),
				),

				"business_modern" => array(
					'name'              => "Business Modern",
					'alise'             => "business_modern",
					'slug'              => "business-modern",
					'preview'           => "https://noor.pixeldima.com/business-modern/",
					'screenshot'        => "business-modern",
					'des'               => "",
					'homepage_title'    => "Home",
					'is_home'           => false,
					'is_shop_demo'      => false,
					'revslider_exists'  => true,
					'import_exception'  => array( 'products' ),
					'requirement'       => array(
						sprintf( esc_html__( 'Noor Assistant: For all the shortcodes used in %s.', 'noor' ), DIMA_THEME_NAME ),
						esc_html__( 'Visual Composer: For page builder.', 'noor' ),
						esc_html__( 'Slider Revolution: For Sliders.', 'noor' ),
					),
					'requirement_other' => array(
						esc_html__( 'Memory Limit of 256 MB', 'noor' ),
						esc_html__( 'Max execution time (php time limit) of 300 seconds', 'noor' ),
					),
				),

				"business" => array(
					'name'              => "Business",
					'alise'             => "business",
					'slug'              => "business",
					'preview'           => "https://noor.pixeldima.com/business/",
					'screenshot'        => "business",
					'des'               => "",
					'homepage_title'    => "Home",
					'is_home'           => false,
					'is_shop_demo'      => false,
					'revslider_exists'  => true,
					'import_exception'  => array( 'products', 'portfolio' ),
					'requirement'       => array(
						sprintf( esc_html__( 'Noor Assistant: For all the shortcodes used in %s.', 'noor' ), DIMA_THEME_NAME ),
						esc_html__( 'Visual Composer: For page builder.', 'noor' ),
						esc_html__( 'Slider Revolution: For Sliders.', 'noor' ),
					),
					'requirement_other' => array(
						esc_html__( 'Memory Limit of 256 MB', 'noor' ),
						esc_html__( 'Max execution time (php time limit) of 300 seconds', 'noor' ),
					),
				),

				"business_and_finance" => array(
					'name'              => "Business And Finance",
					'alise'             => "business_and_finance",
					'slug'              => "business_and_finance",
					'preview'           => "https://noor.pixeldima.com/business-and-finance/",
					'screenshot'        => "business-and-finance",
					'des'               => "",
					'homepage_title'    => "Home",
					'is_home'           => false,
					'is_shop_demo'      => false,
					'revslider_exists'  => true,
					'import_exception'  => array( 'products', 'portfolio' ),
					'requirement'       => array(
						sprintf( esc_html__( 'Noor Assistant: For all the shortcodes used in %s.', 'noor' ), DIMA_THEME_NAME ),
						esc_html__( 'Visual Composer: For page builder.', 'noor' ),
						esc_html__( 'Slider Revolution: For Sliders.', 'noor' ),
					),
					'requirement_other' => array(
						esc_html__( 'Memory Limit of 256 MB', 'noor' ),
						esc_html__( 'Max execution time (php time limit) of 300 seconds', 'noor' ),
					),
				),

				"construction" => array(
					'name'              => "Construction",
					'alise'             => "construction",
					'slug'              => "construction",
					'preview'           => "https://noor.pixeldima.com/construction/",
					'screenshot'        => "construction",
					'des'               => "",
					'homepage_title'    => "Home",
					'is_home'           => false,
					'is_shop_demo'      => false,
					'revslider_exists'  => true,
					'import_exception'  => array( 'products' ),
					'requirement'       => array(
						sprintf( esc_html__( 'Noor Assistant: For all the shortcodes used in %s.', 'noor' ), DIMA_THEME_NAME ),
						esc_html__( 'Visual Composer: For page builder.', 'noor' ),
						esc_html__( 'Slider Revolution: For Sliders.', 'noor' ),
					),
					'requirement_other' => array(
						esc_html__( 'Memory Limit of 256 MB', 'noor' ),
						esc_html__( 'Max execution time (php time limit) of 300 seconds', 'noor' ),
					),
				),

				"medical" => array(
					'name'              => "Medical",
					'alise'             => "medical",
					'slug'              => "medical",
					'preview'           => "https://noor.pixeldima.com/medical/",
					'screenshot'        => "medical",
					'des'               => "",
					'homepage_title'    => "Home",
					'is_home'           => false,
					'is_shop_demo'      => false,
					'revslider_exists'  => true,
					'import_exception'  => array( 'products', 'portfolio' ),
					'requirement'       => array(
						sprintf( esc_html__( 'Noor Assistant: For all the shortcodes used in %s.', 'noor' ), DIMA_THEME_NAME ),
						esc_html__( 'Visual Composer: For page builder.', 'noor' ),
						esc_html__( 'Slider Revolution: For Sliders.', 'noor' ),
					),
					'requirement_other' => array(
						esc_html__( 'Memory Limit of 256 MB', 'noor' ),
						esc_html__( 'Max execution time (php time limit) of 300 seconds', 'noor' ),
					),
				),

				"help_center" => array(
					'name'              => "Help Center",
					'alise'             => "help_center",
					'slug'              => "help-center",
					'preview'           => "https://noor.pixeldima.com/help-center/",
					'screenshot'        => "help-center",
					'des'               => "",
					'homepage_title'    => "Home",
					'is_home'           => false,
					'is_shop_demo'      => false,
					'revslider_exists'  => false,
					'import_exception'  => array( 'products', 'portfolio' ),
					'requirement'       => array(
						sprintf( esc_html__( 'Noor Assistant: For all the shortcodes used in %s.', 'noor' ), DIMA_THEME_NAME ),
						esc_html__( 'Visual Composer: For page builder.', 'noor' ),
						esc_html__( 'Slider Revolution: For Sliders.', 'noor' ),
					),
					'requirement_other' => array(
						esc_html__( 'Max execution time (php time limit) of 300 seconds', 'noor' ),
					),
				),

				"elegant_restaurant" => array(
					'name'              => "Elegant Restaurant",
					'alise'             => "elegant_restaurant",
					'slug'              => "elegant-restaurant",
					'preview'           => "https://noor.pixeldima.com/elegant-restaurant/",
					'screenshot'        => "elegant-restaurant",
					'des'               => "",
					'homepage_title'    => "Home",
					'is_home'           => false,
					'is_shop_demo'      => false,
					'revslider_exists'  => true,
					'import_exception'  => array( 'products', 'portfolio' ),
					'requirement'       => array(
						sprintf( esc_html__( 'Noor Assistant: For all the shortcodes used in %s.', 'noor' ), DIMA_THEME_NAME ),
						esc_html__( 'Visual Composer: For page builder.', 'noor' ),
						esc_html__( 'Slider Revolution: For Sliders.', 'noor' ),
					),
					'requirement_other' => array(
						esc_html__( 'Max execution time (php time limit) of 300 seconds', 'noor' ),
					),
				),

				"modern_restaurant" => array(
					'name'              => "Modern Restaurant",
					'alise'             => "modern_restaurant",
					'slug'              => "modern-restaurant",
					'preview'           => "https://noor.pixeldima.com/modern-restaurant/",
					'screenshot'        => "modern-restaurant",
					'des'               => "",
					'homepage_title'    => "Home",
					'is_home'           => false,
					'is_shop_demo'      => false,
					'revslider_exists'  => true,
					'import_exception'  => array( 'products', 'portfolio' ),
					'requirement'       => array(
						sprintf( esc_html__( 'Noor Assistant: For all the shortcodes used in %s.', 'noor' ), DIMA_THEME_NAME ),
						esc_html__( 'Visual Composer: For page builder.', 'noor' ),
						esc_html__( 'Slider Revolution: For Sliders.', 'noor' ),
					),
					'requirement_other' => array(
						esc_html__( 'Max execution time (php time limit) of 300 seconds', 'noor' ),
					),
				),

				"seo"        => array(
					'name'              => "SEO",
					'alise'             => "seo",
					'slug'              => "seo",
					'preview'           => "https://noor.pixeldima.com/seo/",
					'screenshot'        => "seo",
					'des'               => "",
					'homepage_title'    => "Home",
					'is_home'           => false,
					'is_shop_demo'      => false,
					'revslider_exists'  => true,
					'import_exception'  => array( 'products', 'portfolio' ),
					'requirement'       => array(
						sprintf( esc_html__( 'Noor Assistant: For all the shortcodes used in %s.', 'noor' ), DIMA_THEME_NAME ),
						esc_html__( 'Visual Composer: For page builder.', 'noor' ),
						esc_html__( 'Slider Revolution: For Sliders.', 'noor' ),
					),
					'requirement_other' => array(
						esc_html__( 'Max execution time (php time limit) of 300 seconds', 'noor' ),
					),
				),
				"startup"    => array(
					'name'              => "Startup",
					'alise'             => "startup",
					'slug'              => "startup",
					'preview'           => "https://noor.pixeldima.com/startup/",
					'screenshot'        => "startup",
					'des'               => "",
					'homepage_title'    => "Home",
					'is_home'           => false,
					'is_shop_demo'      => false,
					'revslider_exists'  => true,
					'import_exception'  => array( 'products', 'portfolio', 'sliders' ),
					'requirement'       => array(
						sprintf( esc_html__( 'Noor Assistant: For all the shortcodes used in %s.', 'noor' ), DIMA_THEME_NAME ),
						esc_html__( 'Visual Composer: For page builder.', 'noor' ),
						esc_html__( 'Contact Form 7: For the contact form.', 'noor' ),
					),
					'requirement_other' => array(
						esc_html__( 'Max execution time (php time limit) of 300 seconds', 'noor' ),
					),
				),
				"consulting" => array(
					'name'              => "Consulting",
					'alise'             => "consulting",
					'slug'              => "consulting",
					'preview'           => "https://noor.pixeldima.com/consulting/",
					'screenshot'        => "consulting",
					'des'               => "",
					'homepage_title'    => "Home",
					'is_home'           => false,
					'is_shop_demo'      => false,
					'revslider_exists'  => true,
					'import_exception'  => array( 'products' ),
					'requirement'       => array(
						sprintf( esc_html__( 'Noor Assistant: For all the shortcodes used in %s.', 'noor' ), DIMA_THEME_NAME ),
						esc_html__( 'Visual Composer: For page builder.', 'noor' ),
						esc_html__( 'Slider Revolution: For Sliders.', 'noor' ),
					),
					'requirement_other' => array(
						esc_html__( 'Max execution time (php time limit) of 300 seconds', 'noor' ),
					),
				),
			);

			foreach ( $demos as $demo ) {
				$dima_demo_import_class->dima_display_demo( $demo );
			}

			$dima_demo_import_class->dima_display_soon_demo( DIMA_TEMPLATE_URL . '/data/soon.png' );
			$dima_demo_import_class->dima_display_soon_demo( DIMA_TEMPLATE_URL . '/data/soon-photographer.png' );
			?>
        </ul>
    </div>

    <div id="dima-install-demo-notes" class="pixeldima-admin-popup" role="alert">
        <div class="pixeldima-admin-popup-inner">
            <div class="theme-about wp-clearfix pixeldima-admin-popup-container">
                <div class="pixeldima-admin-popup-header">
                    <a href="#0" class="pixeldima-admin-popup-close img-replace"></a></div>
                <div class="pixeldima-admin-popup-about">
                    <div class="demo-screenshots">
                        <img src="">
                        <a class="demo-preview button" target="_blank" href="">
							<?php esc_html_e( 'Previw', 'noor' ); ?>
                        </a>

                        <div class="demo-import-form">
                            <h4 class="demo-form-title">
								<?php esc_html_e( 'Import Content', 'noor' ); ?>
                                <span style="font-size: 13px; font-style: italic; font-weight: normal"><?php esc_html_e( '( menus only import with "All" )', 'noor' ); ?></span>
                            </h4>

                            <form id="import-dima" data-demo-id="classic">
                                <p>
                                    <input type="checkbox" value="all" id="import-all-classic">
                                    <label for="import-all-classic"><?php esc_html_e( 'All', 'noor' ); ?></label>
                                </p>

                                <p>
                                    <input type="checkbox" value="posts" id="import-post-classic">
                                    <label for="import-post-classic"><?php esc_html_e( 'Posts', 'noor' ); ?></label>
                                </p>

                                <p>
                                    <input type="checkbox" value="pages" id="import-post-classic">
                                    <label for="import-post-classic"><?php esc_html_e( 'Pages', 'noor' ); ?></label>
                                </p>

                                <p>
                                    <input type="checkbox" value="portfolio" id="import-post-classic">
                                    <label for="import-post-classic"><?php esc_html_e( 'Portfolio', 'noor' ); ?></label>
                                </p>

                                <p>
                                    <input type="checkbox" value="products" id="import-post-classic">
                                    <label for="import-post-classic"><?php esc_html_e( 'Products', 'noor' ); ?></label>
                                </p>

                                <p>
                                    <input type="checkbox" value="media" id="import-media-classic">
                                    <label for="import-attachment-classic"><?php esc_html_e( 'Media', 'noor' ); ?></label>
                                </p>

                                <p>
                                    <input type="checkbox" value="sliders" id="import-sliders-classic">
                                    <label for="import-sliders-classic"><?php esc_html_e( 'Sliders', 'noor' ); ?></label>
                                </p>

                                <p>
                                    <input type="checkbox" value="theme_options" id="import-theme_options-classic">
                                    <label for="import-theme_options-classic"><?php esc_html_e( 'Customizer', 'noor' ); ?></label>
                                </p>

                                <p>
                                    <input type="checkbox" value="widgets" id="import-widgets-classic">
                                    <label for="import-widgets-classic"><?php esc_html_e( 'Widgets', 'noor' ); ?></label>
                                </p>
                            </form>
                        </div>
                        <br>
                        <div class="need_help">
                            <h4><?php esc_html_e( 'Need help ? ', 'noor' ); ?></h4>
                            <p><em><?php esc_html_e( 'Do you have trouble installing demo ? ', 'noor' ); ?></em></p>
                            <hr>
                            <ul>
                                <li><a target="_blank" href="https://pixeldima.com/knowledgebase/manual-demo-import/">
										<?php esc_html_e( 'Manual Demo Import . ', 'noor' ); ?>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="demo-info">
                        <h3></h3>
                        <hr>
                        <div class="demo-desc">
                        </div>
                        <br>
                        <div class="dima-message-hint">
                            <h4><?php esc_html_e( 'Important Notes:', 'noor' ); ?></h4>
                            <ol>
                                <li><?php esc_html_e( 'Importing demo data will overwrite your Customizer settings . ', 'noor' ); ?></li>
                                <li><?php esc_html_e( 'We recommend to run Demo Import on a clean WordPress installation . ', 'noor' ); ?></li>
                                <li><?php esc_html_e( 'The Demo Import will not import the images we have used in our live demos, due to
                                    copyright / license reasons . ', 'noor' ); ?>
                                </li>
                                <li><?php esc_html_e( 'No existing posts, pages, categories, images, custom post types or any other data
                                    will be deleted or modified . ', 'noor' ); ?>
                                </li>
                                <li><?php esc_html_e( 'Posts, pages, images, widgets and menus will get imported . ', 'noor' ); ?></li>
                                <li><?php esc_html_e( 'Before you begin, make sure all the required plugins are activated . ', 'noor' ); ?></li>
                                <li><?php esc_html_e( 'Do not run the Demo Import multiple times one after another, it will result in
                                    double content . ', 'noor' ); ?>
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
                <div class="pixeldima-admin-buttons">
                    <strong><?php esc_html_e( "Are you sure you want to proceed?", 'noor' ); ?></strong>
                    <br>
                    <em>
						<?php esc_html_e( '( It will take a few minutes to complete)', 'noor' ); ?>
                    </em>
                    <br><br>
                    <a class="yes button install" href="#0">
						<?php esc_html_e( 'Import', 'noor' ); ?>
                    </a>
                    <a class="no button" href="#0">
						<?php esc_html_e( 'Cancel', 'noor' ); ?>
                    </a>
                </div>
            </div>
        </div>
    </div>


	<?php
}