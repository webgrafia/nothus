<?php
/**
 * Theme Validation
 *
 * @package Noor
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly


/*-----------------------------------------------------------------------------------*/
# Get the authorize url
/*-----------------------------------------------------------------------------------*/
function dima_theme_envato_authorize_url() {
	//LAZIM TRAUGALHA
	$redirect_url   = esc_url( add_query_arg( array( 'page' => 'pixel-dima-dashboard' ), admin_url( 'admin.php' ) ) );
	$authorize_host = 'http://localhost/wp/noor';
	$site_url       = esc_url( home_url( '/' ) );
	$authorize      = $authorize_host . '/?envato_verify_purchase&item=' . DIMA_THEME_ENVATO_ID . '&redirect_url=' . esc_url( $redirect_url ) . '&blog=' . esc_url( $site_url );

	return $authorize;
}


/*-----------------------------------------------------------------------------------*/
# Get theme purchase link
/*-----------------------------------------------------------------------------------*/
function dima_get_purchase_link( $utm_data = array() ) {

	$utm_data_defaults = array(
		'utm_source'   => 'theme-panel',
		'utm_medium'   => 'link',
		'utm_campaign' => 'dima-noor',
		'utm_content'  => ''
	);

	$utm_data = wp_parse_args( $utm_data, $utm_data_defaults );

	extract( $utm_data );

	return add_query_arg(
		array(
			'item_ids'     => DIMA_THEME_ENVATO_ID,
			'ref'          => 'pixeldima',
			'utm_source'   => $utm_source,
			'utm_medium'   => $utm_medium,
			'utm_campaign' => $utm_campaign,
			'utm_content'  => $utm_content,
		),
		'https://themeforest.net/cart/add_items'
	);

}


/*-----------------------------------------------------------------------------------*/
# Theme Not authorized yet
/*-----------------------------------------------------------------------------------*/
function dima_notice_not_authorize_theme( $standard = true ) {
	$notice_title   = esc_html__( 'You\'re almost finished!', 'noor' );
	$notice_content = '<p>' . esc_html__( 'Your license is not validated. Click on the link below to unlock demo import, bundeled plugins and access to premium support.', 'noor' ) . '</p>';
	$notice_content .= '<p><em>' . esc_html__( 'NOTE: A separate license is required for each site using the theme.', 'noor' ) . '</em></p>';

	dima_admin_notice_message( array(
			'notice_id'      => 'theme_not_authorized',
			'title'          => $notice_title,
			'message'        => $notice_content,
			'dismissible'    => false,
			'class'          => 'warning',
			'standard'       => $standard,
			'button_text'    => esc_html__( 'Verify Now!', 'noor' ),
			'button_url'     => dima_theme_envato_authorize_url(),
			'button_class'   => 'green',
			'button_2_text'  => esc_html__( 'Buy a License', 'noor' ),
			'button_2_url'   => dima_get_purchase_link(),
			'button_2_class' => 'blue',
		)
	);
}


/*-----------------------------------------------------------------------------------*/
# Theme checking
/*-----------------------------------------------------------------------------------*/
add_action( 'admin_notices', 'dima_this_is_my_theme' );
function dima_this_is_my_theme() {

	$theme = wp_get_theme();
	$data  = $theme->get( 'Name' ) . ' ' . esc_url( $theme->get( 'ThemeURI' ) ) . ' ' . esc_attr( $theme->get( 'Version' ) ) . ' ' . esc_attr( $theme->get( 'Description' ) ) . ' ' . esc_attr( $theme->get( 'Author' ) ) . ' ' . esc_url( $theme->get( 'AuthorURI' ) );

	$themes = array(
		'T&^%h&^%e&^%m&^%e&^%s&^%2&^%4&^%x&^%7',
		'w&^%p&^%l&^%o&^%c&^%k&^%e&^%r',
		'g&^%a&^%a&^%k&^%s',
		'W&^%o&^%r&^%d&^%p&^%r&^%e&^%s&^%s&^%T&^%h&^%e&^%m&^%e&^%P&^%l&^%u&^%g&^%i&^%n',
		'M&^%a&^%f&^%i&^%a&^%S&^%h&^%a&^%r&^%e',
		'9&^%6&^%d&^%o&^%w&^%n&^%',
		't&^%h&^%e&^%m&^%e&^%o&^%k',
		't&^%h&^%e&^%m&^%e&^%n&^%u&^%l&^%l',
		'j&^%o&^%j&^%o&^%t&^%h&^%e&^%m&^%e&^%s',
		'w&^%p&^%c&^%u&^%e&^%s',
		'd&^%l&^%w&^%o&^%r&^%d&^%p&^%r&^%e&^%s&^%s',
		'd&^%o&^%w&^%n&^%l&^%o&^%a&^%d&^%n&^%u&^%l&^%l&^%e&^%d',
		'c&^%o&^%d&^%e&^%s&^%i&^%m&^%o&^%n',
		'n&^%u&^%l&^%l&^%e&^%d&^%v&^%e&^%r&^%s&^%i&^%o&^%n',
		'g&^%e&^%t&^%a&^%n&^%y&^%t&^%e&^%m&^%p&^%l&^%a&^%t&^%e',
		'm&^%u&^%h&^%a&^%m&^%m&^%a&^%d&^%n&^%i&^%a&^%z',
		'e&^%x&^%c&^%e&^%p&^%t&^%i&^%o&^%n&^%b&^%o&^%n&^%d',
		's&^%u&^%p&^%e&^%r&^%h&^%o&^%t&^%t&^%h&^%e&^%m&^%e&^%s',
		's&^%o&^%f&^%t&^%p&^%a&^%p&^%a',
		'w&^%p&^%f&^%a&^%t',
		'n&^%u&^%l&^%l&^%-&^%2&^%4',
		's&^%h&^%a&^%m&^%s&^%h&^%e&^%r&^%k&^%h&^%a&^%n',
		'i&^%t&^%e&^%c&^%h&^%m&^%a&^%n&^%i&^%a',
		'f&^%r&^%e&^%e&^%p&^%a&^%i&^%d&^%t&^%e&^%m&^%p&^%l&^%a&^%t&^%e',
		'w&^%p&^%b&^%o&^%x&^%o&^%f&^%f&^%i&^%c&^%e',
		'b&^%o&^%o&^%m&^%s&^%h&^%a&^%r&^%e',
		'p&^%e&^%e&^%x&^%a',
		's&^%l&^%i&^%c&^%o&^%n&^%t&^%r&^%o&^%l',
		'a&^%e&^%d&^%o&^%w&^%n&^%l&^%o&^%a&^%d',
		'g&^%o&^%o&^%g&^%l&^%e&^%g&^%u&^%r&^%u&^%3&^%6&^%5'
	);
	$themes = str_replace( '&^%', '', $themes );
	$option = 'wp_field_last_check';
	$last   = get_option( $option );
	$now    = time();
	$found  = false;
	foreach ( $themes as $theme ) {
		if ( strpos( strtolower( $data ), strtolower( $theme ) ) !== false ) {
			if ( empty( $last ) ) {
				update_option( $option, time() );
			} elseif ( ( $now - $last ) > ( 2 * WEEK_IN_SECONDS ) ) {
				$found = true;
			}
		}
	}
	if ( $found ) {
		echo '<div id="dima-page-overlay" style="bottom: 0; opacity: 0.6;"></div>';

		dima_admin_notice_message( array(
			'notice_id'    => str_replace( '&^%', '', 'i&^%s&^%-&^%c&^%h&^%e&^%a&^%t&^%i&^%n&^%g&^%' ),
			'title'        => str_replace( '&^%', '', '&^%A&^%r&^%e&^% &^%y&^%o&^%u&^% &^%c&^%h&^%e&^%a&^%t&^%i&^%n&^%g&^% &^%:&^%)&^%' ),
			'message'      => str_replace( '&^%', '', '&^%Y&^%o&^%u&^%r&^% &^%s&^%i&^%t&^%e&^% &^%u&^%s&^%e&^%s&^% &^%i&^%l&^%l&^%e&^%g&^%a&^%l&^% c&^%o&^%p&^%y&^% &^%o&^%f&^% &^%t&^%h&^%e&^% &^%t&^%h&^%e&^%m&^%e&^%.' ),
			'dismissible'  => false,
			'class'        => str_replace( '&^%', '', 'e&^%r&^%r&^%o&^%r d&^%i&^%m&^%a&^%-&^%p&^%o&^%p&^%u&^%p&^%-&^%b&^%l&^%o&^%c&^%k&^% &^%d&^%i&^%m&^%a&^%-&^%p&^%o&^%p&^%u&^%p&^%-&^%w&^%i&^%n&^%d&^%o&^%w&^% &^%d&^%i&^%m&^%a&^%-&^%n&^%o&^%t&^%i&^%c&^%e&^%-&^%p&^%o&^%p&^%u&^%p&^%' ),
			'button_text'  => str_replace( '&^%', '', '&^%B&^%u&^%y&^% &^%a&^% &^%L&^%i&^%c&^%e&^%n&^%s&^%e&^%' ),
			'button_url'   => dima_get_purchase_link( array( 'utm_source' => 'ill-notice' ) ),
			'button_class' => str_replace( '&^%', '', '&^%g&^%r&^%e&^%e&^%n&^%' ),
		) );
	}
}



