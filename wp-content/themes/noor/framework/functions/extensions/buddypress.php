<?php
/*Change the Default BuddyPress Avatar Sizes*/
define( 'BP_AVATAR_THUMB_HEIGHT', 100 );
define( 'BP_AVATAR_THUMB_WIDTH', 100 );

/*-----------------------------------------------------------------------------------*/
# Dequeue buddyPress Default Css files
/*-----------------------------------------------------------------------------------*/
if ( ! function_exists( 'dima_bp_dequeue_css' ) ) {

	add_action( 'wp_enqueue_scripts', 'dima_bp_dequeue_css', 10 );
	function dima_bp_dequeue_css() {

		if ( DIMA_BUDDYPRESS_IS_ACTIVE ) {

			wp_dequeue_style( 'bp-parent-css' );
			wp_dequeue_style( 'bp-parent-css-rtl' );
			wp_dequeue_style( 'bp-legacy-css' );
			wp_dequeue_style( 'bp-legacy-css-rtl' );
		}
	}

}

/*-----------------------------------------------------------------------------------*/
# Enqueue buddyPress Custom Css file
/*-----------------------------------------------------------------------------------*/
if ( ! function_exists( 'dima_bp_enqueue_css' ) ) {

	add_action( 'wp_enqueue_scripts', 'dima_bp_enqueue_css', 9 );
	function dima_bp_enqueue_css() {

		if ( DIMA_BUDDYPRESS_IS_ACTIVE ) {

			$dima_css_style_dir = '/framework/asset/site/css/styles/';
			$demo               = DIMA_Demos::get_installed_demo( "noor_main" );
			$min                = dima_helper::dima_get_option( 'dima_minified_files' ) ? '.min' : '';
			$ext                = '';
			# Register buddyPress css file ----------
			wp_enqueue_style( 'dima-buddypress', DIMA_TEMPLATE_URL . $dima_css_style_dir . $demo . '/dima-buddypress' . $ext . $min . '.css', null, DIMA_VERSION, 'screen' );
		}

	}

}


/*-----------------------------------------------------------------------------------*/
# BuddyPress Cover Image
/*-----------------------------------------------------------------------------------*/
if ( ! function_exists( 'dima_bp_cover_image_css' ) ) {

	add_filter( 'bp_before_xprofile_cover_image_settings_parse_args', 'dima_bp_cover_image_css', 10, 1 );
	add_filter( 'bp_before_groups_cover_image_settings_parse_args', 'dima_bp_cover_image_css', 10, 1 );
	function dima_bp_cover_image_css( $settings = array() ) {

		$theme_handle = 'dima-buddypress';

		$settings['callback']      = 'dima_bp_cover_image_callback';
		$settings['theme_handle']  = $theme_handle;
		$settings['width']         = 1400;
		$settings['height']        = 440;
		$settings['default_cover'] = DIMA_TEMPLATE_URL . '/images/default-cover-image.jpg';

		return $settings;
	}

}


if ( ! function_exists( 'dima_bp_cover_image_callback' ) ) {

	function dima_bp_cover_image_callback( $params = array() ) {

		if ( empty( $params ) ) {
			return;
		}

		$background_attr = '';
		if ( $params['cover_image'] == DIMA_TEMPLATE_URL . '/images/default-cover-image.jpg' ) {
			$background_attr = '
	    	background-repeat: repeat !important;
	    	background-size: 400px !important;
	    ';
		}

		return '
			#buddypress #header-cover-image {
				background-image: url(' . $params['cover_image'] . ');
				' . $background_attr . '
			}
		';
	}

}