<?php
/**
 * DIMA Framework
 * WARNING: This file is part of the DIMA Core Framework.
 * Do not edit the core files.
 *
 * @package Dima Framework
 * @subpackage Extensions
 * @version   1.0.0
 * @since     1.0.0
 * @author    PixelDima <info@pixeldima.com>
 */

// Plugin setup for theme compatibility.
// Remove Meta Boxes

if ( ! function_exists( 'dima_revolution_slider_remove_meta_boxes' ) ) :

	function dima_revolution_slider_remove_meta_boxes() {

		if ( is_admin() ) {
			foreach ( get_post_types() as $post_type ) {
				remove_meta_box( 'mymetabox_revslider_0', $post_type, 'normal' );
			}
		}

	}

	add_action( 'do_meta_boxes', 'dima_revolution_slider_remove_meta_boxes' );

endif;

$rs = new RevSlider();
global $dima_sliders_rv;
$dima_sliders_rv = $rs->getArrSliders();

function dima_revolution_slider_list() {
	global $dima_sliders_rv;
	$data = array();
	foreach ( $dima_sliders_rv as $s ) {
		$key                    = 'dima-revolution-slider-' . esc_attr( $s->getAlias() );
		$data[ $key ]['id']     = $s->getID();
		$data[ $key ]['slug']   = $s->getAlias();
		$data[ $key ]['name']   = $s->getTitle();
		$data[ $key ]['source'] = 'Revolution Slider';
	}

	return $data;
}

function dima_revolution_slider_add_slider_meta( $meta ) {
	return array_merge( $meta, dima_revolution_slider_list() );
}

add_filter( 'dima_sliders_meta', 'dima_revolution_slider_add_slider_meta' );


add_filter( 'revslider_include_libraries', 'dima_revslider_include_libraries' );
if ( ! function_exists( 'dima_revslider_include_libraries' ) ) {
	function dima_revslider_include_libraries() {
		$id          = dima_helper::dima_get_the_ID();
		$slider_name = esc_attr( get_post_meta( $id, '_dima_slider_below', true ) );
		if ( ! empty( $slider_name ) ) {
			return true;
		} else {
			return false;
		}
	}
}