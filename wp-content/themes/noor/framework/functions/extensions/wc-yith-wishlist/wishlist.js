jQuery(document).ready(function ($) {

    "use strict";

    $('.wishlist-button').on('click', function (e) {

        if ($(this).parent().find('.yith-wcwl-wishlistexistsbrowse').hasClass('show')) {
            var link = $(this).parent().find('.yith-wcwl-wishlistexistsbrowse a').attr('href');
            window.location.href = link;
            return;
        }

        $(this).addClass('loading');
        $(this).parent().find('.add_to_wishlist').click();

    });

    if ($('.yith-wcwl-wishlistexistsbrowse').hasClass('show')) {
        $('.yith-wcwl-wishlistexistsbrowse.show').each(
            function () {
                $(this).parent().parent().parent().find('.wishlist-button').addClass('wishlist-added');
                $(this).parent().parent().find('.wishlist-icon').addClass('wishlist-added');
            }
        );
    }

    var update_wishlist_count = function (el) {
        $.ajax({
            url: yith_wcwl_l10n.ajax_url,
            data: {
                action: 'yith_wcwl_update_wishlist_count'
            },
            dataType: 'json',
            beforeSend: function () {
            },
            complete: function () {
            },
            success: function () {
                if ($('.wishlist-button').hasClass('loading')) {
                    $('.wishlist-button.loading').addClass('wishlist-added');
                    $('.wishlist-button').removeClass('loading');
                }
            },

        });
    };
    $('body').on('added_to_wishlist removed_from_wishlist', update_wishlist_count);
});