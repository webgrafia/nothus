<?php

function dima_wishlist_integrations_scripts() {
	wp_deregister_style( 'yith-wcwl-font-awesome' );
	wp_deregister_style( 'yith-wcwl-font-awesome-ie7' );
	wp_deregister_style( 'yith-wcwl-main' );
	wp_deregister_style( 'yith_wcas_frontend' );
	wp_enqueue_script( 'dima-woocommerce-wishlist', DIMA_TEMPLATE_URL . '/framework/functions/extensions/wc-yith-wishlist/wishlist.js', 'dima-woocommerce-js', false, '1.0' );
}

add_action( 'wp_enqueue_scripts', 'dima_wishlist_integrations_scripts' );

// Add wishlist button to my account dropdown

function dima_wishlist_account_item( $show ) {
	$items = '';
	if ( $show ) {
		$wishlist_page = yith_wcwl_object_id( get_option( 'yith_wcwl_wishlist_page_id' ) );
		$items         = ' <li class="wishlist-account-element">' .
		                 '<a href="' . YITH_WCWL()->get_wishlist_url() . '">' .
		                 '<span class="menu_icon_item">' . dima_get_svg_icon( 'ic_favorite' ) . '</span>' .
		                 '<span class=" dima-menu-span">' . get_the_title( $wishlist_page ) . '</span>' .
		                 '</a>' .
		                 ' </li>';
	}

	return $items;
}

add_filter( 'dima_filter_wishlist_account_item', 'dima_wishlist_account_item' );


// Add wishlist Button to Product Image
if ( ! function_exists( 'flatsome_product_wishlist_button' ) ) {
	function dima_product_wishlist_button() {
		if ( DIMA_YITH_WISHLIST_IS_ACTIVE ) { ?>
            <div class="wishlist-icon">
                <div class="wishlist-button">
                    <span class="svg_favorite"><?php echo dima_get_svg_icon( "ic_favorite_border" ) ?></php></span>
                    <span class="svg_added"><?php echo dima_get_svg_icon( "ic_favorite" ) ?></span>
                    <span class="svg_circle"><?php echo dima_get_svg_icon( "ic_refresh" ) ?></span>
                </div>
                <p> <?php echo esc_attr__('Add to Wishlist','noor'); ?></p>
                <div class="dima-wishlist">
					<?php echo do_shortcode( '[yith_wcwl_add_to_wishlist]' ); ?>
                </div>
            </div>
		<?php }
	}
}
add_action( 'woocommerce_after_add_to_cart_button', 'dima_product_wishlist_button', 2 );


// Update Wishlist Count
function dima_update_wishlist_count() {
	wp_send_json( array(
		'count' => yith_wcwl_count_all_products()
	) );
}

add_action( 'wp_ajax_yith_wcwl_update_wishlist_count', 'dima_update_wishlist_count' );
add_action( 'wp_ajax_nopriv_yith_wcwl_update_wishlist_count', 'dima_update_wishlist_count' );