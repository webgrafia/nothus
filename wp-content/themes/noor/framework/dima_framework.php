<?php
/**
 * DIMA Framework
 *
 * A flexible Wordpress Framework, created by PixelDima
 *
 * This file includes the superobject class and loads the parameters neccessary for the backend pages.
 * A new $dima superobject is then created that holds all data necessary for either front or backend, depending what page you are browsing
 *
 * @package Dima Framework
 * @author      PixelDima @pixeldima
 * @copyright   Copyright (c) PixelDima
 * @link        http://pixeldima.com
 * @since       Version 1.0.0
 * @version     1.0.0
 */

define( 'DIMA_FRAMEWORK_VERSION', "1.0.0" );
/*-----------------------------------------------------------------------------------*/
# Define Constants
/*-----------------------------------------------------------------------------------*/
define( 'DIMA_THEME_NAME', "Noor" );
define( 'DIMA_THEME_FOLDER', "noor" );
define( 'DIMA_THEME_ENVATO_ID', '20759600' );
define( 'DIMA_VERSION', dima_helper::dima_get_theme( 'Version' ) );
define( 'DIMA_SITE_URL', site_url() );
define( 'DIMA_TEMPLATE_PATH', get_template_directory() );
define( 'DIMA_TEMPLATE_URL', get_template_directory_uri() );
define( 'DIMA_JETPACK_IS_ACTIVE', class_exists( 'Jetpack' ) );
define( 'DIMA_VISUAL_COMOPSER_IS_ACTIVE', defined( 'WPB_VC_VERSION' ) );
define( 'DIMA_REVOLUTION_SLIDER_IS_ACTIVE', class_exists( 'RevSlider' ) );
define( 'DIMA_YITH_WISHLIST_IS_ACTIVE', in_array( 'yith-woocommerce-wishlist/init.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) );
define( 'DIMA_GRAVITY_FORMS_IS_ACTIVE', class_exists( 'GFForms' ) );
define( 'DIMA_CONTACT_FORM_7_IS_ACTIVE', class_exists( 'WPCF7_ContactForm' ) );
define( 'DIMA_WC_IS_ACTIVE', class_exists( 'WC_API' ) );
define( 'DIMA_BUDDYPRESS_IS_ACTIVE', class_exists( 'BuddyPress' ) );
define( 'DIMA_THE_EVENTS_CALENDAR_ACTIVE', class_exists( 'Tribe__Events__Main' ) );
define( 'DIMA_KB_IS_ACTIVE', class_exists( 'Pressapps_Knowledge_Base' ) );
define( 'DIMA_AMP_IS_ACTIVE', class_exists( 'AMP_Customizer_Settings' ) );
define( 'DIMA_NOUR_ASSISTANT_IS_ACTIVE', class_exists( 'DIMA_NOUR_ASSISTANT_CLASS' ) );
define( 'DIMA_PORTFOLIO_IS_ACTIVE', class_exists( 'DIMA_NOUR_PORTFOLIO' ) );
define( 'DIMA_BBPRESS_IS_ACTIVE', class_exists( 'bbPress' ) );
define( 'DIMA_USE_LAZY', dima_helper::dima_am_i_true( dima_helper::dima_get_option( "dima_lazy_image" ) ) );
define( 'DIMA_IMG_DIR', DIMA_TEMPLATE_URL . '/framework/images' );

$uploads = wp_upload_dir();
/**
 * Upload dir
 */
define( 'DIMA_WPCONTENT_DIR', $uploads['basedir'] );

/**
 * Upload url
 */
define( 'DIMA_WPCONTENT_URL', $uploads['baseurl'] );