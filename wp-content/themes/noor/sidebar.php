<?php
/**
 * The sidebar containing the main widget area.
 * 
 * displays all of the <class="container">
 * 1 → Archive author & search: _archive-header
 * 2 → main template file: index.php
 * 3 → Sidebar
 *
 * @package Dima Framework
 * @since 1.0
 * @version 1.0
 */
?>


<?php dima_helper::dima_get_view( dima_helper::dima_get_template(), 'wp', 'sidebar' ); ?>