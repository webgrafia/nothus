<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * 
 * displays all of the <class="container">
 * 1 → Archive author & search: _archive-header
 * 2 → main template file: index.php
 * 3 → Sidebar
 *
 * @package Dima Framework
 * @since 1.0
 * @version 1.0
 */
?>


<?php
dima_helper::dima_get_view( dima_helper::dima_get_template(), 'wp', 'index' );