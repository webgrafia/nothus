<?php
/**
 * The template for displaying comments.
 *
 * The area of the page that contains both current comments
 * and the comment form.
 *
 * @package Dima Framework
 * @since 1.0
 * @version 1.0
 */
?>


<?php dima_helper::dima_get_view( dima_helper::dima_get_template(), 'wp', 'comments' ); ?>