<?php
/**
 * Cart Page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/cart.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.1.0
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly

wc_print_notices();

do_action( 'woocommerce_before_cart' );
?>
    <form class="woocommerce-cart-form" action="<?php echo esc_url( wc_get_cart_url() ); ?>" method="post">

		<?php do_action( 'woocommerce_before_cart_table' ); ?>

        <div class="dima-data-table-wrap cart-table">
            <table class="shop_table shop_table_responsive cart" cellspacing="0">
                <thead>
                <tr>
                    <th class="product-name" colspan="2"><?php esc_html_e( 'Product', 'noor' ); ?></th>
                    <th class="product-price"><?php esc_html_e( 'Price', 'noor' ); ?></th>
                    <th class="product-quantity"><?php esc_html_e( 'Quantity', 'noor' ); ?></th>
                    <th class="product-subtotal" colspan="2"><?php esc_html_e( 'Total', 'noor' ); ?></th>
                </tr>
                </thead>
                <tbody>
				<?php do_action( 'woocommerce_before_cart_contents' ); ?>
				<?php
				foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {

					$_product   = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
					$product_id = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );

					if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_cart_item_visible', true, $cart_item, $cart_item_key ) ) {

						?>

                        <tr class="<?php echo esc_attr( apply_filters( 'woocommerce_cart_item_class', 'cart_item', $cart_item, $cart_item_key ) ); ?>">

                            <td class="product-thumbnail">
								<?php
								$thumbnail = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key );
								if ( ! $_product->is_visible() ) {
									echo( $thumbnail );
								} else {
									printf( '<a href="%s">%s</a>', $_product->get_permalink( $cart_item ), $thumbnail );
								}
								?>
                            </td>

                            <td class="product-name" data-title="<?php esc_attr_e( 'Product', 'noor' ); ?>">
                                <div class="cart-item-details">
									<?php
									if ( ! $_product->is_visible() ) {
										echo apply_filters( 'woocommerce_cart_item_name', $_product->get_title(), $cart_item, $cart_item_key ) . '&nbsp;';
									} else {
										echo apply_filters( 'woocommerce_cart_item_name', sprintf( '<a  href="%s">%s</a>', $_product->get_permalink( $cart_item ), $_product->get_title() ), $cart_item, $cart_item_key );
									}

									// Meta data
									echo WC()->cart->get_item_data( $cart_item );

									// Backorder notification
									if ( $_product->backorders_require_notification() && $_product->is_on_backorder( $cart_item['quantity'] ) ) {
										echo '<p class="backorder_notification">' . esc_html__( 'Available on backorder', 'noor' ) . '</p>';
									}
									?>
                                </div>
                            </td>

                            <td class="product-price" data-title="<?php esc_attr_e( 'Price', 'noor' ); ?>">
								<?php echo apply_filters( 'woocommerce_cart_item_price', WC()->cart->get_product_price( $_product ), $cart_item, $cart_item_key ); ?>
                            </td>

                            <td class="product-quantity" data-title="<?php esc_attr_e( 'Quantity', 'noor' ); ?>">
								<?php
								if ( $_product->is_sold_individually() ) {
									$product_quantity = sprintf( '1 <input type="hidden" name="cart[%s][qty]" value="1" />', $cart_item_key );
								} else {
									$product_quantity = woocommerce_quantity_input( array(
										'input_name'  => "cart[{$cart_item_key}][qty]",
										'input_value' => $cart_item['quantity'],
										//'max_value'   => $_product->backorders_allowed() ? '' : $_product->get_stock_quantity(),
										'max_value'   => $_product->get_max_purchase_quantity(),
										'min_value'   => '0'
									), $_product, false );
								}
								echo apply_filters( 'woocommerce_cart_item_quantity', $product_quantity, $cart_item_key );
								?>
                            </td>

                            <td class="product-subtotal">
								<?php echo apply_filters( 'woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal( $_product, $cart_item['quantity'] ), $cart_item, $cart_item_key ); ?>
                            </td>
                            <td class="product-remove">
								<?php echo apply_filters( 'woocommerce_cart_item_remove_link', sprintf( '<a class="remove" href="%s" title="%s"><span>%s</span>' . dima_get_svg_icon( "ic_clear" ) . '</a>', esc_url( WC()->cart->get_remove_url( $cart_item_key ) ), esc_html__( 'Remove this item', 'noor' ), esc_html__( 'Remove this item', 'noor' ) ), $cart_item_key ); ?>
                            </td>

                        </tr>

						<?php

					}
				}

				do_action( 'woocommerce_cart_contents' );
				?>
                </tbody>

            </table>
        </div>
		<?php do_action( 'woocommerce_after_cart_contents' ); ?>


        <div class="coupon-box">

            <div class="ok-row">
                <div class="ok-md-5 ok-sd-12 ok-xsd-12">
                    <div class="actions">
						<?php if ( wc_coupons_enabled() ) { ?>

                            <div class="coupon action-group">
                                <div class="ok-row">
                                    <div class="ok-md-8 ok-sd-12 ok-xsd-12">
                                        <input name="coupon_code" type="text" class="input-text" id="coupon_code"
                                               value=""
                                               placeholder="<?php esc_attr_e( 'Coupon code', 'noor' ); ?>"/>
                                    </div>
                                    <div class="ok-md-4 ok-sd-12 ok-xsd-12">
                                        <input type="submit"
                                               class="no-bottom-margin dima-button button-block small fill"
                                               name="apply_coupon"
                                               value="<?php esc_attr_e( 'Apply Coupon', 'noor' ); ?>"/>
                                    </div>
                                </div>
								<?php do_action( 'woocommerce_cart_coupon' ); ?>
                            </div>
						<?php } ?>
						<?php do_action( 'woocommerce_after_cart_contents' ); ?>

                    </div>
                </div>

                <div class="ok-md-6 ok-sd-12 ok-xsd-12 ok-offset-md-1 no-sd-offset no-xsd-offset">
                    <div class="cart-collaterals">
                        <h5 class="box-titel"><?php esc_html_e( 'Cart Totals', 'noor' ); ?></h5>
						<?php woocommerce_cart_totals(); ?>
                        <div class="update action-group">
                            <input type="submit" class="dima-button button-block small stroke"
                                   name="update_cart" value="<?php esc_attr_e( 'Update Cart', 'noor' ); ?>"/>
							<?php do_action( 'woocommerce_cart_actions' ); ?>
                        </div>
						<?php wp_nonce_field( 'woocommerce-cart' ); ?>
                        <input type="submit"
                               class="checkout-button no-bottom-margin dima-button button-block small fill alt wc-forward"
                               name="proceed" value="<?php esc_attr_e( 'Proceed to Checkout', 'noor' ); ?>"/>
                    </div>
                </div>
            </div>
        </div>

		<?php do_action( 'woocommerce_after_cart_table' ); ?>

    </form>
    <div class="cart-collaterals">
		<?php
		/**
		 * woocommerce_cart_collaterals hook.
		 *
		 * @hooked woocommerce_cross_sell_display
		 * @hooked woocommerce_cart_totals - 10
		 */
		do_action( 'woocommerce_cart_collaterals' );
		?>
    </div>
<?php do_action( 'woocommerce_after_cart' ); ?>