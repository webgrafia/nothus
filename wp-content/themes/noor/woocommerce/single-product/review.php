<?php
/**
 * Review Comments Template
 *
 * Closing li is left out on purpose!.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/review.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.6.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
?>
<li <?php comment_class(); ?> id="li-comment-<?php comment_ID() ?>">

    <div id="comment-<?php comment_ID(); ?>" class="content-comment post-content comment_container">

        <div class="dima-comment-img">
            <span class="avatar-wrap cf circle">
			<?php
			/**
			 * The woocommerce_review_before hook
			 *
			 * @hooked woocommerce_review_display_gravatar - 10
			 */
			do_action( 'woocommerce_review_before', $comment );
			?>
            </span>
        </div>
        <div class="dima-comment-entry comment-text">

            <div class="dima-comment-header clearfix">
                <ul class="user-comment-titel float-start">
                    <li>
						<?php

						/**
						 * The woocommerce_review_meta hook.
						 *
						 * @hooked woocommerce_review_display_meta - 10
						 * @hooked WC_Structured_Data::generate_review_data() - 20
						 */
						do_action( 'woocommerce_review_meta', $comment );

						/**
						 * The woocommerce_review_before_comment_meta hook.
						 *
						 * @hooked woocommerce_review_display_rating - 10
						 */
						?>
                    </li>
                    <li class="float-end">
						<?php
						do_action( 'woocommerce_review_before_comment_meta', $comment );
						?>
                    </li>

                </ul>
                <ul class="user-comment-titel float-end">
                    <li><?php echo wp_kses( dima_get_svg_icon( "ic_mode_edit" ), dima_helper::dima_get_allowed_html_tag() );
						edit_comment_link( esc_html__( 'Edit', 'noor' ) ); ?></li>

                </ul>
            </div>
			<?php
			do_action( 'woocommerce_review_before_comment_text', $comment );

			/**
			 * The woocommerce_review_comment_text hook
			 *
			 * @hooked woocommerce_review_display_comment_text - 10
			 */
			do_action( 'woocommerce_review_comment_text', $comment );

			do_action( 'woocommerce_review_after_comment_text', $comment ); ?>

        </div>
    </div>
