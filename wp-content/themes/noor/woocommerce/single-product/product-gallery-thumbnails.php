<?php

global $post, $product, $woocommerce;

$attachment_ids = dima_woocommerce_version_check( '3.0.0' ) ? $product->get_gallery_image_ids() : $product->get_gallery_attachment_ids();
$thumb_count    = count( $attachment_ids ) + 1;

// Disable thumbnails if there is only one extra image.
if ( $thumb_count == 1 ) {
	return;
}
$columns = apply_filters( 'dima_filter_woocommerce_product_thumbnails_columns', 5 );

$js_data = array(
	'slidesToShow'   => $columns,
	'slidesToScroll' => 1,
	'infinite'       => true,
	'draggable'      => true,
	'focusOnSelect'  => true,
	'dots'           => false,
	'autoplay'       => false,
	'arrows'         => false,
	'fade'           => false,
	'adaptiveHeight' => true,
	'mobilefirst'    => true,
	'pauseOnHover'   => false,
	'asNavFor'       => ".slider-for",
	'speed'          => '500',
	'autoplaySpeed'  => '3000',
	'rtl'            => is_rtl()
);
if ( DIMA_NOUR_ASSISTANT_IS_ACTIVE ) {
	$data = dima_creat_data_attributes( 'slick_slider_nav', $js_data );
} else {
	$data = '';
}

if ( $attachment_ids ) {
	$loop = 0;
	?>
    <div class="slider-nav" <?php echo ( $data ) ?> ><?php

		if ( has_post_thumbnail() ) : ?>
            <div class="slick-item is-nav-selected first">
                <a><?php echo get_the_post_thumbnail( $post->ID, apply_filters( 'shop_thumbnail_image_size', 'shop_thumbnail' ) ) ?></a>
            </div>
		<?php endif;

		foreach ( $attachment_ids as $attachment_id ) {

			$classes       = array( '' );
			$image_title   = esc_attr( get_the_title( $attachment_id ) );
			$image_caption = esc_attr( get_post_field( 'post_excerpt', $attachment_id ) );
			$image_class   = esc_attr( implode( ' ', $classes ) );

			$image = wp_get_attachment_image( $attachment_id, apply_filters( 'shop_thumbnail_image_size', 'shop_thumbnail' ), 0, $attr = array(
				'title' => $image_title,
				'alt'   => $image_title
			) );

			echo apply_filters( 'woocommerce_single_product_image_thumbnail_html', sprintf( '<div class="slick-item"><a class="%s" title="%s" >%s</a></div>', $image_class, $image_caption, $image ), $attachment_id, $post->ID, $image_class );

			$loop ++;
		}
		?>
    </div>
	<?php
} ?>
