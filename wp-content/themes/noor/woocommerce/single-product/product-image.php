<?php
/**
 * Single Product Image
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/product-image.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// Fallback to WC.2x Versions.
if ( ! dima_woocommerce_version_check( '3.0.0' ) ) {
	wc_get_template( 'woocommerce/single-product/w2-product-image.php' );

	return;
}
global $post, $product, $woocommerce;

$columns        = apply_filters( 'dima_filter_woocommerce_product_thumbnails_columns', 5 );
$attachment_ids = dima_woocommerce_version_check( '3.0.0' ) ? $product->get_gallery_image_ids() : $product->get_gallery_attachment_ids();
$thumb_count    = count( $attachment_ids ) + 1;
$asNavFor       = '';
if ( $thumb_count > $columns ) {
	$asNavFor = ".slider-nav";
}
$animation = dima_helper::dima_get_option( 'dima_shop_slide_animation' );
$animation = ( $animation == 'fade' );
$js_data   = array(
	'dots'           => false,
	'autoplay'       => false,
	'arrows'         => true,
	'infinite'       => true,
	'fade'           => $animation,
	'draggable'      => true,
	'adaptiveHeight' => true,
	'mobileFirst'    => true,
	'pauseOnHover'   => true,
	'slidesToShow'   => 1,
	'slidesToScroll' => 1,
	'asNavFor'       => $asNavFor,
	'speed'          => '300',
	'autoplaySpeed'  => '3000',
	'rtl'            => is_rtl()
);

if ( DIMA_NOUR_ASSISTANT_IS_ACTIVE ) {
	$data = dima_creat_data_attributes( 'slick_slider', $js_data );
} else {
	$data = '';
}

global $post, $product;
$columns           = apply_filters( 'woocommerce_product_thumbnails_columns', 4 );
$post_thumbnail_id = get_post_thumbnail_id( $post->ID );
$full_size_image   = wp_get_attachment_image_src( $post_thumbnail_id, 'full' );
$placeholder       = has_post_thumbnail() ? 'with-images' : 'without-images';
$wrapper_classes   = apply_filters( 'woocommerce_single_product_image_gallery_classes', array(
	'woocommerce-product-gallery',
	'woocommerce-product-gallery--' . $placeholder,
	'woocommerce-product-gallery--columns-' . absint( $columns ),
	'images',
) );
$slider_classes    = array( 'slider-for' );

?>
<div class="ok-md-5 ok-xsd-12">
    <div class="slick-slider <?php echo esc_attr( implode( ' ', array_map( 'sanitize_html_class', $wrapper_classes ) ) ); ?>"
         data-columns="<?php echo esc_attr( $columns ); ?>" style="opacity: 0; transition: opacity .25s ease-in-out;">
        <figure class="woocommerce-product-gallery__wrapper <?php echo implode( ' ', $slider_classes ); ?>" <?php echo( $data ) ?>>
			<?php
			$attributes = array(
				'title'                   => get_post_field( 'post_title', $post_thumbnail_id ),
				'data-caption'            => get_post_field( 'post_excerpt', $post_thumbnail_id ),
				'data-src'                => $full_size_image[0],
				'data-large_image'        => $full_size_image[0],
				'data-large_image_width'  => $full_size_image[1],
				'data-large_image_height' => $full_size_image[2],
			);

			if ( has_post_thumbnail() ) {
				$html = '<div data-thumb="' . get_the_post_thumbnail_url( $post->ID, 'shop_thumbnail' ) . '" class="slick-item woocommerce-product-gallery__image"><a href="' . esc_url( $full_size_image[0] ) . '">';
				$html .= get_the_post_thumbnail( $post->ID, 'shop_single', $attributes );
				$html .= '</a></div>';
			} else {
				$html = '<div class="woocommerce-product-gallery__image--placeholder">';
				$html .= sprintf( '<img src="%s" alt="%s" class="wp-post-image" />', esc_url( wc_placeholder_img_src() ), esc_html__( 'Awaiting product image', 'noor' ) );
				$html .= '</div>';
			}

			echo apply_filters( 'woocommerce_single_product_image_thumbnail_html', $html, get_post_thumbnail_id( $post->ID ) );

			do_action( 'woocommerce_product_thumbnails' );
			?>
        </figure>
		<?php wc_get_template( 'woocommerce/single-product/product-gallery-thumbnails.php' ); ?>
    </div>
</div>
