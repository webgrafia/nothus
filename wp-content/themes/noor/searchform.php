<form method="get" id="searchform-1" class="form-small form search-form"
      action="<?php echo esc_url( home_url( '/' ) ); ?>">
	<label for="s" class="hide"><?php esc_html_e( 'Search', 'noor' ); ?></label>
	<input type="text" class="search-query" name="s" placeholder="<?php esc_attr_e( 'Search', 'noor' ); ?>"/>
</form>