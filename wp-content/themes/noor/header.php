<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="dima-main">
 *
 *
 * @package Dima Framework
 * @since 1.0
 * @version 1.0
 */



 dima_helper::dima_get_view( dima_helper::dima_get_template(), 'wp', 'header' );