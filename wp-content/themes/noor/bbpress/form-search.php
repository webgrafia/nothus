<?php

/**
 * Search 
 *
 * @package bbPress
 * @subpackage Theme
 */

?>

<form role="search" method="get" id="bbp-search-form" class="dima-bbp-search-form form-small form search-form" action="<?php bbp_search_url(); ?>">
	<div class="search-table">
		<label class="screen-reader-text hidden" for="bbp_search"><?php esc_html_e( 'Search for:', 'noor' ); ?></label>
		<input type="hidden" name="action" value="bbp-search-request" />
		<input type="text" value="<?php echo esc_attr( bbp_get_search_terms() ); ?>" name="bbp_search" id="bbp_search" placeholder="Search..." />
	</div>
</form>
