<?php

/**
 * User Registration Form
 *
 * @package bbPress
 * @subpackage Theme
 */

?>

<div class="dima-bbp-form box">
    <form method="post" action="<?php bbp_wp_login_action( array( 'context' => 'login_post' ) ); ?>"
          class="login bbp-login-form">
        <fieldset class="bbp-form">
            <legend><?php esc_html_e( 'Create an Account', 'noor' ); ?></legend>

            <div class="bbp-template-notice">
                <p><?php esc_html_e( 'Your username must be unique, and cannot be changed later. We use your email address to email you a secure password and verify your account.', 'noor' ) ?></p>
            </div>

            <p class="dima-bbp-username">
                <label for="user_login"><?php esc_html_e( 'Username', 'noor' ); ?>: </label>
                <input type="text" name="user_login" value="<?php bbp_sanitize_val( 'user_login' ); ?>" size="20"
                       id="user_login"/>
            </p>

            <p class="dima-bbp-email">
                <label for="user_email"><?php esc_html_e( 'Email', 'noor' ); ?>: </label>
                <input type="text" name="user_email" value="<?php bbp_sanitize_val( 'user_email' ); ?>" size="20"
                       id="user_email"/>
            </p>

			<?php do_action( 'register_form' ); ?>

            <div class="bbp-submit-wrapper">

                <button type="submit" name="user-submit"
                        class="button submit user-submit"><?php esc_html_e( 'Register', 'noor' ); ?></button>

				<?php bbp_user_register_fields(); ?>

            </div>
        </fieldset>
    </form>
</div>