<?php
/**
 * The template for displaying all single posts.
 *
 * @package Dima Framework
 * @since 1.0
 * @version 1.0
 */
?>


<?php dima_helper::dima_get_view( dima_helper::dima_get_template(), 'wp', 'single' ); ?>
