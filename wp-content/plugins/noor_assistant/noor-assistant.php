<?php

/*
Plugin Name: Noor Assistant
Plugin URI: http://pixeldima.com/
Description: This plugin is required to run Noor as it includes all of our shortcode functionality, which is tightly integrated into the theme.
Version: 1.0.16
Author: PixelDima
Author URI: http://pixeldima.com/
Text Domain: noor_assistant
*/

define( 'DIMA_NOOR_ASSISTANT', 'noor_assistant' );
define( 'DIMA_NOUR_ASSISTANT_VERSION', '1.0.16' );

define( 'DIMA_NOUR_ASSISTANT_URL', plugins_url( '', __FILE__ ) );
define( 'DIMA_NOUR_ASSISTANT_TEMPLATE_PATH', plugin_dir_path( __FILE__ ) );

class DIMA_NOUR_ASSISTANT_CLASS {

	/**
	 * Core singleton class
	 * @var self - pattern realization
	 */
	private static $_instance;

	/**
	 * Get the instane of dima_Theme_assistant
	 *
	 * @return self
	 */
	public static function getInstance() {
		if ( ! ( self::$_instance instanceof self ) ) {
			self::$_instance = new self();
		}

		return self::$_instance;
	}

	/**
	 * constructor.
	 */
	function __construct() {
		$theme = wp_get_theme()->get( 'Name' );
		$dir   = dirname( __FILE__ );
		$this->setPaths( array(
			'APP_DIR' => basename( $dir ),
		) );
		if ( substr_count( $theme, 'Noor' ) > 0 ) {
			add_action( 'plugins_loaded', array( $this, 'dima_pluginsLoaded', ), 10 );
			add_action( 'init', array( $this, 'dima_noor_assistant_core_init' ), 10 );
			add_action( 'after_setup_theme', array( $this, 'dima_add_vc' ) );
			register_activation_hook( __FILE__, array( $this, 'dima_activationHook', ) );
		} else {
			add_action( 'admin_notices', array( $this, '_dima_admin_notice__error' ) );
		}
	}

	/**
	 * Cloning disabled
	 */
	public function __clone() {
	}

	/**
	 * Serialization disabled
	 */
	public function __sleep() {
	}

	/**
	 * De-serialization disabled
	 */
	public function __wakeup() {
	}

	/**
	 * List of paths.
	 *
	 * @since 1.0.0
	 * @var array
	 */
	private $paths = array();

	/**
	 * Setter for paths
	 *
	 * @since 1.0.0
	 * @access protected
	 *
	 * @param $paths
	 */
	protected function setPaths( $paths ) {
		$this->paths = $paths;
	}

	/**
	 * Gets absolute path for file/directory in filesystem.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @param        $name - name of path dir
	 * @param string $file - file name or directory inside path
	 *
	 * @return string
	 */
	public function path( $name, $file = '' ) {
		$path = $this->paths[ $name ] . ( strlen( $file ) > 0 ? '/' . preg_replace( '/^\//', '', $file ) : '' );

		return apply_filters( 'dima_shortcodes_path_filter', $path );
	}

	/**
	 * Callback function WP plugin_loaded action hook. Loads locale
	 * @access public
	 */
	public function dima_pluginsLoaded() {
		do_action( 'dima_shortcodes_plugins_loaded' );
		load_plugin_textdomain( 'noor-assistant', false, $this->path( 'APP_DIR', 'languages' ) );
	}

	function dima_noor_assistant_core_init() {
		require_once( 'include/helper.php' );
		/**
		 * Custom posts type.
		 */
		require_once( 'include/shortcodes.php' );
		require_once( 'include/scripts.php' );
		require_once( 'include/pixeldima-setup/setup.php' );
		require_once( 'include/phpquery/phpquery.php' );
		require_once( 'include/portfolio/dima-portfolio.php' );
	}

	/*
	* Add custom VC elements
	*/
	public function dima_add_vc() {
		if ( class_exists( 'Vc_Manager', false ) ) {
			require_once( DIMA_NOUR_ASSISTANT_TEMPLATE_PATH . '/include/dima-extensions/vc_custom/dima-visual-composer.php' );
		}
	}

	/**
	 * Enables to add hooks in activation process.
	 *
	 */
	public function dima_activationHook() {
		do_action( 'dima_activation_hook' );
	}

	/*
	 * Admin notice text
	 */
	public function _dima_admin_notice__error() {
		echo '<div class="notice notice-error is-dismissible">';
		echo '<p>' . esc_html__( 'Noor Assistant is enabled but not effective. It requires Noor theme in order to work.', 'noor-assistant' ) . '</p>';
		echo '</div>';
	}
}

require_once dirname( __FILE__ ) . '/include/custom-post-type.php';
$DIMA_NOUR_ASSISTANT_CLASS = new DIMA_NOUR_ASSISTANT_CLASS;
