<?php
/**
 * Shortcodes
 *
 * @package    core
 * @subpackage shortcodes
 * @version    1.0.0
 * @since      1.0.0
 * @author     PixelDima <info@pixeldima.com>
 *
 */

/**
 * Dropcap
 *
 * @param $atts
 *
 * @return string
 */


function dima_shortcode_dropcap( $atts ) {
	extract(
		shortcode_atts(
			array(
				'id'        => '',
				'class'     => '',
				'character' => '',
				'type'      => '',//dropcap-1,dropcap-3,dropcap-3
				'style'     => ''
			), $atts, 'dropcap'
		)
	);

	$id        = ( $id != '' ) ? 'id="' . esc_attr( $id ) . '"' : '';
	$class     = ( $class != '' ) ? 'dropcap ' . esc_attr( $class ) : 'dropcap';
	$character = ( $character == '' ) ? '!' : esc_attr( $character );
	$type      = ( $type != '' ) ? ' ' . esc_attr( $type ) : ' dropcap-1';
	$style     = ( $style != '' ) ? 'style="' . $style . '"' : '';

	$output
		= "<span {$id} class=\"{$class}{$type}\" {$style}>{$character}</span>";

	return $output;
}

add_shortcode( 'dropcap', 'dima_shortcode_dropcap' );


/**
 * Line
 */
function dima_shortcode_line( $atts ) {
	extract(
		shortcode_atts(
			array(
				'id'    => '',
				'class' => '',
				'type'  => '',//double,dashed
				'style' => ''
			), $atts, 'line'
		)
	);

	$id    = ( $id != '' ) ? 'id="' . esc_attr( $id ) . '"' : '';
	$class = ( $class != '' ) ? 'divider ' . esc_attr( $class ) : 'divider';
	$style = ( $style != '' ) ? 'style="' . $style . '"' : '';

	if ( $type != '' ) {
		$output = "<div {$id} class=\"{$class}\" {$style}>"
		          . "<div class=\"{$type}\"></div>"
		          . "</div>";
	} else {
		$output = "<hr>";
	}

	return $output;
}

add_shortcode( 'line', 'dima_shortcode_line' );

function dima_shortcode_divider( $atts ) {
	extract(
		shortcode_atts(
			array(
				'id'               => '',
				'class'            => '',
				'icon'             => '',
				'icon_fontawesome' => '',
				'divider_style'    => '',
				'icon_svg'         => '',
				'icon_color'       => '',
				'line_color'       => '',
				'direction'        => '',
				'style'            => '',

				'animation'      => '',
				'delay'          => '',
				'delay_offset'   => '',
				'delay_duration' => '',
			), $atts, 'divider'
		)
	);

	$id               = ( $id != '' ) ? 'id="' . esc_attr( $id ) . '"' : '';
	$class            = ( $class != '' ) ? 'dima-divider ' . esc_attr( $class )
		: 'dima-divider ';
	$style            = ( $style != '' ) ? 'style="' . $style . '"' : '';
	$icon_color       = ( $icon_color != '' ) ? 'style="color:' . $icon_color
	                                            . ';"' : '';
	$line_color       = ( $line_color != '' ) ? 'data-bg-color=' . $line_color
	                                            . '' : '';
	$_icon            = ( $icon != '' ) ? $icon : '';
	$divider_style    = ( $divider_style != '' ) ? $divider_style : '';
	$icon_svg         = ( $icon_svg != '' ) ? dima_get_svg_icon( $icon_svg ) : '';
	$icon_fontawesome = ( $icon_fontawesome != '' ) ? $icon_fontawesome : '';
	$_icon            = ( $_icon == '' ) ? $icon_fontawesome : $_icon;
	$output           = '';

	$animation_data = '';
	$animation_data .= ( $animation != '' ) ? 'data-animate=' . $animation . '' : '';
	$animation_data .= ( $delay != '' ) ? ' data-delay=' . $delay . '' : '';
	$animation_data .= ( $delay_offset != '' ) ? ' data-offset=' . $delay_offset . '' : '';
	$animation_data .= ( $delay_duration != '' ) ? ' data-duration=' . $delay_duration . '' : '';

	if ( ! empty( $icon_svg ) ) {
		$_icon = '<span ' . $icon_color . '>' . $icon_svg . '<span>';
	} else if ( ! empty( $_icon ) ) {
		$_icon = '<i class="' . $_icon . '" ' . $icon_color . '></i>';
	}

	switch ( $divider_style ) {
		case 'line':
			$output
				= "<div {$id} class=\"{$class}  line-hr line-{$direction}\" {$animation_data} {$style} {$line_color}>"
				  . "</div>";
			break;

		case
		'noor_divider':
			$output
				= "<div {$id} class=\"noor-line {$class} noor-{$direction}\" {$animation_data} {$style}>"
				  . "</div>";
			break;
		case 'icon':
			$output = "<div {$id} class=\"{$class} topaz-line\" {$animation_data} {$style}>"
			          . "{$_icon}"
			          . "</div>";
			break;

	}

	return
		$output;
}

add_shortcode( 'divider', 'dima_shortcode_divider' );

if ( DIMA_KB_IS_ACTIVE ) {
	function dima_shortcode_kb_search( $atts ) {
		extract(
			shortcode_atts(
				array(
					'id'                    => '',
					'class'                 => '',
					'style'                 => '',
					'direction'             => '',
					'float'                 => '',
					'searchbox_width'       => '100%',
					'button'                => 'true',
					'searchbox_placeholder' => esc_html__( 'Search Knowledge Base', 'noor-assistant' ),
					'animation'             => '',
					'delay'                 => '',
					'delay_offset'          => '',
					'delay_duration'        => '',
				), $atts, 'dima_kb_search'
			)
		);
		global $pakb;
		$search_ptxt = $searchbox_placeholder;

		$id              = ( $id != '' ) ? 'id="' . esc_attr( $id ) . '"' : '';
		$class           = ( $class != '' ) ? 'dima-divider ' . esc_attr( $class ) : 'dima-divider ';
		$searchbox_width = ( $searchbox_width != '' ) ? 'style="width:' . $searchbox_width . '"' : '';
		$button          = dima_helper::dima_am_i_true( $button );
		$float           = ( $float != '' ) ? ' float-' . $float : ' float-start';
		$animation_data  = '';
		$animation_data  .= ( $animation != '' ) ? 'data-animate=' . $animation . '' : '';
		$animation_data  .= ( $delay != '' ) ? ' data-delay=' . $delay . '' : '';
		$animation_data  .= ( $delay_offset != '' ) ? ' data-offset=' . $delay_offset . '' : '';
		$animation_data  .= ( $delay_duration != '' ) ? ' data-duration=' . $delay_duration . '' : '';

		$get_search_query = $autosuggest = $pakbsearchicon = '';
		$home_url         = home_url( '/' );
		$search_text      = esc_attr__( 'Search', 'noor-assistant' );
		if ( $button ) {
			$button = "<input type=\"submit\" id=\"searchsubmit\" value=\"{$search_text}\"/>";
		}
		if ( is_search() ) {
			$get_search_query = get_search_query();
		}
		if ( $pakb->get( 'live_search' ) ) {
			$autosuggest = 'autosuggest';
		}
		if ( ! $pakb->get( 'search_btn' ) ) {
			$pakbsearchicon = ' pakb-search-icon';
		}

		$output = "<div class=\"dima-kb pakb-header {$float} {$class}\"  {$id} {$searchbox_width}>"
		          . "<div role=\"search\" method=\"post\" id=\"kbsearchform\" action=\"$home_url\" {$animation_data}>"
		          . "<div class=\"pakb-search\">"
		          . wp_nonce_field( 'knowedgebase-search', 'search_nonce', false )
		          . "<input class=\"{$autosuggest}{$pakbsearchicon}\" type=\"text\" value=\"{$get_search_query}\" name=\"s\" placeholder=\"$search_ptxt\" id=\"kb-s\">"
		          . "<span>"
		          . $button
		          . "</span>"
		          . "<input type=\"hidden\" name=\"post_type\" value=\"knowledgebase\"/>"
		          . "</div>"
		          . "</div>"
		          . "</form>"
		          . "</div>";

		return $output;
	}

	add_shortcode( 'dima_kb_search', 'dima_shortcode_kb_search' );

}

/**
 * Clear
 */

function dima_shortcode_clear( $atts ) {
	extract(
		shortcode_atts(
			array(
				'id'    => '',
				'class' => '',
				'by'    => ''
			), $atts, 'clear'
		)
	);

	$id    = ( $id != '' ) ? 'id="' . esc_attr( $id ) . '"' : '';
	$class = ( $class != '' ) ? 'dima-clear ' . esc_attr( $class ) : 'dima-clear';
	$by    = ( $by != '' ) ? 'style="padding-bottom:' . $by . '"' : '';

	$output = "<div {$id} class=\"{$class}\" {$by}></div>";

	return $output;
}

add_shortcode( 'clear', 'dima_shortcode_clear' );

/**
 * Highlight
 */

function dima_shortcode_highlight( $atts, $content = null ) {
	extract(
		shortcode_atts(
			array(
				'id'    => '',
				'class' => '',
				'style' => '',
			), $atts, 'highlight'
		)
	);

	$id    = ( $id != '' ) ? 'id="' . esc_attr( $id ) . '"' : '';
	$class = ( $class != '' ) ? 'dima-mark ' . esc_attr( $class ) : 'dima-mark';
	$style = ( $style != '' ) ? 'style="' . $style . '"' : '';

	$output = "<mark {$id} class=\"{$class}\" {$style}>{$content}</mark>";

	return $output;
}

add_shortcode( 'highlight', 'dima_shortcode_highlight' );


/**
 * Testimonial
 *
 * @param      $atts
 * @param null $content
 *
 * @return string
 */
function dima_shortcode_testimonial( $atts, $content = null ) { // 1
	extract(
		shortcode_atts(
			array(
				'id'                 => '',
				'class'              => '',
				'style'              => '',
				'author'             => '',
				'job'                => '',
				'image'              => '',
				'image_circle'       => '',
				'icon'               => '',
				'url'                => '',
				'target'             => '',
				'border'             => '',
				'bg_color'           => '',
				'icon_color'         => '',
				'meta_color'         => '',
				'padding_top_bottom' => '',
				'padding_left_right' => '',
				'dima_vc_add_shadow' => '',
				'icon_float'         => '',
				'text_alignment'     => '',
				'float'              => '',

				'animation'      => '',
				'delay'          => '',
				'delay_offset'   => '',
				'delay_duration' => '',
			), $atts, 'testimonial'
		)
	);

	$id                 = ( $id != '' ) ? 'id="' . esc_attr( $id ) . '"' : '';
	$style              = ( $style != '' ) ? 'style="' . $style . '"' : '';
	$image              = ( $image != '' ) ? $image : '';
	$author             = ( $author != '' ) ? $author : '';
	$text_alignment     = ( $text_alignment != '' ) ? $text_alignment : '';
	$image_circle       = ( $image_circle == 'true' ) ? ' circle' : '';
	$_class[]           = ( $class != '' ) ? ' ' . esc_attr( $class ) : '';
	$icon_color         = ( $icon_color != '' ) ? 'style="color:' . $icon_color
	                                              . ';"' : '';
	$meta_color         = ( $meta_color != '' ) ? 'style="color:' . $meta_color
	                                              . ';"' : '';
	$padding_left_right = ( $padding_left_right != '' )
		? "padding-left: $padding_left_right ;padding-right:$padding_left_right;"
		: '';
	$padding_top_bottom = ( $padding_top_bottom != '' )
		? "padding-top: $padding_top_bottom ;padding-bottom:$padding_top_bottom;"
		: '';
	$job                = ( $job != '' ) ? '<span><span> - </span>' . $job . '</span>' : '';
	$_class[]           = ( $dima_vc_add_shadow == 'true' ) ? 'box-with-shadow'
		: '';

	$animation_data = '';
	$animation_data .= ( $animation != '' ) ? 'data-animate=' . $animation . '' : '';
	$animation_data .= ( $delay != '' ) ? ' data-delay=' . $delay . '' : '';
	$animation_data .= ( $delay_offset != '' ) ? ' data-offset=' . $delay_offset . '' : '';
	$animation_data .= ( $delay_duration != '' ) ? ' data-duration=' . $delay_duration . '' : '';

	$bg_color_with_shadow = "";
	if ( ( $dima_vc_add_shadow == 'true' ) ) {
		if ( $bg_color != '' ) {
			$bg_color_with_shadow = 'data-bg-color="' . $bg_color . '"';
		}
		$bg_color = '';
	}
	$bg_color = ( $bg_color != '' ) ? "background: $bg_color ;" : '';

	if ( $bg_color != '' || $padding_left_right != ''
	     || $padding_top_bottom != ''
	) {
		$bg_color = 'style="' . $bg_color . $padding_left_right
		            . $padding_top_bottom . '"';
	}

	if ( is_numeric( $image ) ) {
		$bg_image_info = wp_get_attachment_image_src( $image, 'full' );
		$image         = $bg_image_info[0];
	}

	switch ( $icon_float ) {
		case 'end' :
			$_class[] = 'icon_quote_end';
			break;
		case 'start' :
			$_class[] = 'icon_quote_start';
			break;
	}

	switch ( $text_alignment ) {
		case 'end' :
			$_class[] = 'text-end';
			break;
		case 'start' :
			$_class[] = 'text-start';
			break;
		case 'center' :
			$_class[] = 'text-center';
			break;
	}

	if ( $text_alignment != '' ) {
		switch ( $float ) {
			case 'end' :
				$_class[] = 'quote-end';
				break;
			case 'start' :
				$_class[] = 'quote-start';
				break;
			case 'top-center' :
				$_class[] = 'quote-top-center-arrow';
				break;
			case 'top-start' :
				$_class[] = 'quote-start-top';
				break;
			case 'top-end' :
				$_class[] = 'quote-end-top';
				break;
			case 'bottom-center' :
				$_class[] = 'quote-bottom-center-arrow';
				break;
			case 'bottom-start' :
				$_class[] = 'quote-start-bottom';
				break;
			case 'bottom-end' :
				$_class[] = 'quote-end-bottom';
				break;
			default :
				$_class[] = 'quote-start';
		}
	} else {
		switch ( $float ) {
			case 'end' :
				$_class[] = 'quote-end text-end';
				break;
			case 'start' :
				$_class[] = 'quote-start text-start';
				break;
			case 'top-center' :
				$_class[] = 'quote-top-center-arrow text-center';
				break;
			case 'top-start' :
				$_class[] = 'quote-start-top text-start';
				break;
			case 'top-end' :
				$_class[] = 'quote-end-top text-end';
				break;
			case 'bottom-center' :
				$_class[] = 'quote-bottom-center-arrow text-center';
				break;
			case 'bottom-start' :
				$_class[] = 'quote-start-bottom text-start';
				break;
			case 'bottom-end' :
				$_class[] = 'quote-end-bottom text-end';
				break;
			default :
				$_class[] = 'quote-start';
		}
	}

	if ( ! empty( $url ) ) {
		$author = "<a  href=\"$url\" {$target}>$author</a>";
	}

	if ( $border == 'true' ) {
		$_class[] = " testimonial-side quote-text";
	} else {
		$_class[] = "no-arrow quote-text";
	}
	if ( ! empty( $image ) ) {
		$image
			= "<span data-element-bg=\"$image\" class=\"dima-testimonial-image{$image_circle}\">"
			  .
			  "</span>";
	} else {
		$_class[] = "no-arrow";
	}

	if ( $icon != 'false' ) {
		$icon = "<span {$icon_color}>" . dima_get_svg_icon( "ic_format_quote" )
		        . "</span>";
	} else {
		$icon = '';
	}

	if ( ! empty( $_class ) ) {
		$class = dima_short_remove_white_space( join( ' ', $_class ) );
	}
	if ( $float === "bottom-start" || $float === "bottom-end"
	     || $float === "bottom-center"
	) {
		$output
			= "<div {$id} {$bg_color_with_shadow} {$style} class=\"dima-testimonial {$class}\" {$animation_data}>"
			  .
			  "<blockquote {$bg_color}>" .
			  $icon .
			  "<div class=\"quote-content\">" .
			  "<p>" . do_shortcode( $content ) . "</p>" .
			  "<span class=\"dima-testimonial-meta\" {$meta_color}>" .
			  $author . " " . $job .
			  "</span>" .
			  "</div>" .
			  "</blockquote>" .
			  $image .
			  "</div>";

	} else {
		$output
			= "<div {$id} {$bg_color_with_shadow} {$style} class=\"dima-testimonial {$class}\" {$animation_data}>"
			  .
			  $image .
			  "<blockquote {$bg_color}>" .
			  $icon .
			  "<div class=\"quote-content\">" .
			  "<p>" . do_shortcode( $content ) . "</p>" .
			  "<span class=\"dima-testimonial-meta\" {$meta_color}>" .
			  $author . " " . $job .
			  "</span>" .
			  "</div>" .
			  "</blockquote>" .
			  "</div>";
	}

	return $output;
}

add_shortcode( 'testimonial', 'dima_shortcode_testimonial' );

/**
 * Blockquote
 */
function dima_shortcode_blockquote( $atts, $content = null ) {
	extract(
		shortcode_atts(
			array(
				'id'        => '',
				'class'     => '',
				'style'     => '',
				'cite'      => '',
				'type'      => '',
				'icon'      => '',
				'direction' => ''
			), $atts, 'blockquote'
		)
	);

	$id    = ( $id != '' ) ? 'id="' . esc_attr( $id ) . '"' : '';
	$style = ( $style != '' ) ? 'style="' . $style . '"' : '';
	$cite  = ( $cite != '' ) ? '<cite>' . $cite . '</cite>' : '';
	if ( $icon != 'false' ) {
		$icon = "<span>" . dima_get_svg_icon( "ic_format_quote" ) . "</span>";
	} else {
		$icon = '';
	}
	switch ( $direction ) {
		case 'end' :
			$direction = ' text-end';
			break;
		case 'start' :
			$direction = ' text-start';
			break;
		default :
			$direction = ' text-center';
	}
	if ( $type == "pull" ) {
		$class = ( $class != '' ) ? 'post-quote ' . esc_attr( $class )
			: 'post-quote';
		$output
		       = "<blockquote {$id} class=\"{$class}{$direction}\" {$style}>$icon<p>"
		         . do_shortcode( $content ) . "</p>" . $cite . ""
		         . "</blockquote>";

	} elseif ( $type == "sample" ) {
		$class = ( $class != '' ) ? 'sample-quote ' . esc_attr( $class )
			: 'sample-quote';
		$output
		       = "<blockquote {$id} class=\"{$class}{$direction}\" {$style}>$icon<p>"
		         . do_shortcode( $content ) . $cite . "</p>"
		         . "</blockquote>";

	} else {
		$class = ( $class != '' ) ? 'blog-style ' . esc_attr( $class )
			: 'blog-style ';
		$output
		       = "<div class=\"dima-blockquote single-blockquote\">$icon<blockquote {$id} class=\"{$class}{$direction}\" {$style}><p>"
		         . do_shortcode( $content ) . "</p>" . $cite . ""
		         . "</blockquote></div>";
	}

	return $output;
}

add_shortcode( 'blockquote', 'dima_shortcode_blockquote' );

/**
 * Alert
 *
 * @param      $atts
 * @param null $content
 *
 * @return string
 */
function dima_shortcode_alert( $atts, $content = null ) {
	extract(
		shortcode_atts(
			array(
				'id'               => '',
				'class'            => '',
				'style'            => '',
				'alert_style'      => 'one',
				'type'             => '',
				'is_icon'          => '',
				'bg_color'         => '',
				'heading'          => '',
				'icon'             => '',
				'icon_fontawesome' => '',
				'icon_svg'         => '',
				'close'            => '',

				'animation'      => '',
				'delay'          => '',
				'delay_offset'   => '',
				'delay_duration' => '',
			), $atts, 'alert'
		)
	);

	$id               = ( $id != '' ) ? 'id="' . esc_attr( $id ) . '"' : '';
	$class            = ( $class != '' ) ? 'dima-alert ' . esc_attr( $class )
		: 'dima-alert';
	$style            = ( $bg_color != '' ) ? "background:$bg_color;" : '';
	$style            = ( $style != '' ) ? 'style="' . $style . '' . $bg_color
	                                       . '"' : '';
	$type             = ( $type != '' ) ? ' dima-alert-' . $type
		: ' dima-alert-info';
	$heading          = ( $heading != '' ) ?
		$heading = '<em class="header-alert">' . $heading . '</em>'
		: $heading = '';
	$icon_fontawesome = ( $icon_fontawesome != '' ) ? '<i class="'
	                                                  . $icon_fontawesome . '"></i>' : '';
	$alert_style      = ( $alert_style != '' ) ? $alert_style : '';


	$_icon            = ( $icon != '' ) ? $icon : '';
	$icon_svg         = ( $icon_svg != '' ) ? dima_get_svg_icon( $icon_svg ) : '';
	$icon_fontawesome = ( $icon_fontawesome != '' ) ? $icon_fontawesome : '';
	$_icon            = ( $_icon == '' ) ? $icon_fontawesome : $_icon;

	$animation_data = '';
	$animation_data .= ( $animation != '' ) ? 'data-animate=' . $animation . '' : '';
	$animation_data .= ( $delay != '' ) ? ' data-delay=' . $delay . '' : '';
	$animation_data .= ( $delay_offset != '' ) ? ' data-offset=' . $delay_offset . '' : '';
	$animation_data .= ( $delay_duration != '' ) ? ' data-duration=' . $delay_duration . '' : '';

	if ( ! empty( $icon_svg ) ) {
		$_icon = $icon_svg;
	}

	if ( $is_icon == 'true' ) {

		switch ( $type ) {
			case ' dima-alert-warning':
				$is_icon = dima_get_svg_icon( "ic_warning" );
				break;
			case ' dima-alert-info':
				$is_icon = dima_get_svg_icon( "ic_info" );
				break;
			case ' dima-alert-error':
				$is_icon = dima_get_svg_icon( "ic_error" );
				break;
			case ' dima-alert-success':
				$is_icon = dima_get_svg_icon( "ic_check_circle" );
				break;
			case ' dima-alert-custom':
				$is_icon = $_icon;
				break;
			default:
				$is_icon = "";
				break;
		}
	} else {
		$is_icon = '';
	}

	if ( $close == 'true' ) {
		$close     = ' fade in';
		$close_btn = '<button type="button" class="close" data-dismiss="alert">'
		             . dima_get_svg_icon( "ic_close" ) . '</button>';
	} else {
		$close     = ' alert-close-block';
		$close_btn = '';
	}
	if ( ( $alert_style == "two" || $alert_style == "three" ) && $is_icon != '' ) {
		$output
			= "<div class='dima_alert_side{$type}' {$animation_data}><span class='dima_alert_icon'>{$is_icon}</span><div {$id} class=\"alert_style_{$alert_style} {$class} {$type}{$close}\" {$style}>{$close_btn}{$heading}"
			  . do_shortcode( $content ) . "</div></div>";
	} else {
		$output
			= "<div {$id} class=\"alert_style_{$alert_style} {$class}{$type}{$close}\" {$animation_data} {$style}>{$is_icon}{$close_btn}{$heading}"
			  . do_shortcode( $content ) . "</div>";
	}

	return $output;
}

add_shortcode( 'alert', 'dima_shortcode_alert' );


/**
 * Maps
 *   1 Embed
 *   2 Google map
 *   3 Google map marker
 */
function dima_shortcode_map( $atts, $content = null ) {
	extract(
		shortcode_atts(
			array(
				'id'    => '',
				'class' => '',
				'style' => '',
				'boxed' => ''
			), $atts, 'map'
		)
	);

	$id    = ( $id != '' ) ? 'id="' . esc_attr( $id ) . '"' : '';
	$class = ( $class != '' ) ? 'dima-maps ' . esc_attr( $class ) : 'dima-maps';
	$style = ( $style != '' ) ? 'style="' . $style . '"' : '';

	$output
		= "<div {$id} class=\"{$class}\" {$style}><div class=\"dima-map-inner\">{$content}</div></div>";

	return $output;
}

add_shortcode( 'map', 'dima_shortcode_map' );

function dima_shortcode_google_map( $atts, $content = null ) {
	extract(
		shortcode_atts(
			array(
				'class'        => '',
				'style'        => '',
				'lat'          => '',
				'lng'          => '',
				'drag'         => '',
				'zoom'         => '',
				'zoom_control' => '',
				'height'       => '',
				'width'        => '',
				'map_style'    => '',

				'animation'      => '',
				'delay'          => '',
				'delay_offset'   => '',
				'delay_duration' => '',
			), $atts, 'google_maptwo'
		)
	);

	$class          = ( $class != '' ) ? 'dima-maps dima-google-map ' . esc_attr( $class )
		: 'dima-maps dima-google-map';
	$style          = ( $style != '' ) ? 'style="' . $style . '"' : '';
	$animation_data = '';
	$animation_data .= ( $animation != '' ) ? 'data-animate=' . $animation . '' : '';
	$animation_data .= ( $delay != '' ) ? ' data-delay=' . $delay . '' : '';
	$animation_data .= ( $delay_offset != '' ) ? ' data-offset=' . $delay_offset . '' : '';
	$animation_data .= ( $delay_duration != '' ) ? ' data-duration=' . $delay_duration . '' : '';

	$js_data = array(
		'lat'         => ( $lat != '' ) ? $lat : '40.7056308',
		'lng'         => ( $lng != '' ) ? $lng : '-73.9780035',
		'zoom'        => ( $zoom != '' ) ? $zoom : '7',
		'zoomControl' => ( $zoom_control == 'true' ),
		'height'      => ( $height != '' ) ? $height : '200px',
		'width'       => ( $width != '' ) ? $width : '100%',
		'map_style'   => ( $map_style != '' ) ? $map_style : '',
		'drag'        => ( $drag == 'true' )
	);

	$data = dima_creat_data_attributes( 'google_map', $js_data );

	wp_enqueue_script( 'vendor-google-maps' );

	static $count = 0;
	$count ++;

	$output
		= "<div id=\"dima-gmap-{$count}\" class=\"{$class}\" {$animation_data} {$data} {$style}>"
		  . "<div id=\"dima-gmap-in-{$count}\" class=\"dima-gmap-in\">"
		  . "</div>" . do_shortcode( $content ) . "</div>";

	return $output;
}

add_shortcode( 'google_maptwo', 'dima_shortcode_google_map' );

/**
 * @param $atts
 *
 * @return string
 */
function dima_shortcode_google_map_marker( $atts ) {
	extract(
		shortcode_atts(
			array(
				'lat'   => '',
				'lng'   => '',
				'info'  => '',
				'image' => '',

				'animation'      => '',
				'delay'          => '',
				'delay_offset'   => '',
				'delay_duration' => '',
			), $atts, 'google_map_marker'
		)
	);

	$js_data = array(
		'lat'  => ( $lat != '' ) ? $lat : '40.7056308',
		'lng'  => ( $lng != '' ) ? $lng : '-73.9780035',
		'info' => ( $info != '' ) ? $info : ''
	);

	$animation_data = '';
	$animation_data .= ( $animation != '' ) ? 'data-animate=' . $animation . '' : '';
	$animation_data .= ( $delay != '' ) ? ' data-delay=' . $delay . '' : '';
	$animation_data .= ( $delay_offset != '' ) ? ' data-offset=' . $delay_offset . '' : '';
	$animation_data .= ( $delay_duration != '' ) ? ' data-duration=' . $delay_duration . '' : '';

	if ( is_numeric( $image ) ) {
		$image_info       = wp_get_attachment_image_src( $image, 'full' );
		$js_data['image'] = $image_info[0];
	} else if ( $image != '' ) {
		$js_data['image'] = $image;
	}

	$data = dima_creat_data_attributes( 'google_map_marker', $js_data );

	$output = "<div class=\"dima-gmap-marker\" {$animation_data} {$data}></div>";

	return $output;
}

add_shortcode( 'google_map_marker', 'dima_shortcode_google_map_marker' );


/**
 * @param $atts
 *
 * @return string
 */
function dima_shortcode_dima_contact_form_7( $atts ) {
	extract(
		shortcode_atts(
			array(
				'id'      => '',
				'class'   => '',
				'form_id' => '',
				'style'   => '',
			), $atts, 'dima_contact_form_7'
		)
	);


	$contactform_id = $form_id;
	$id             = ( $id != '' ) ? 'id="' . esc_attr( $id ) . '"' : '';
	$class          = ( $class != '' ) ? 'dima_contact_form_7 ' . esc_attr( $class )
		: 'dima_contact_form_7';
	$class          .= ' ' . $style;
	$output         = do_shortcode(
		'[contact-form-7  id="' . esc_attr( $contactform_id ) . '" html_id="'
		. esc_attr( $id ) . '" html_class="' . $class . '"]'
	);

	return $output;
}

add_shortcode( 'dima_contact_form_7', 'dima_shortcode_dima_contact_form_7' );


/**
 * Progress Bar
 * Progress Circle
 */

function dima_shortcode_progress_bar( $atts ) {
	extract(
		shortcode_atts(
			array(
				'id'               => '',
				'class'            => '',
				'style'            => '',
				'type'             => '',//style-1,style-2
				'heading'          => '',
				'color'            => '',
				'striped'          => '',//0,1
				'shadow'           => '',//0,1
				'dima_vc_gradient' => '',//0,1
				'percent'          => '',
				'animation'        => '',
				'delay'            => '',
				'delay_offset'     => '',
				'delay_duration'   => '',
			), $atts, 'progress_bar'
		)
	);

	$id               = ( $id != '' ) ? 'id="' . esc_attr( $id ) . '"' : '';
	$class            = ( $class != '' ) ? 'progress ' . esc_attr( $class )
		: 'progress';
	$style            = ( $style != '' ) ? 'style="' . $style . '"' : '';
	$color            = ( $color != '' ) ? $color : '';
	$dima_vc_gradient = ( $dima_vc_gradient != '' ) ? 'gradient-vertical '
	                                                  . $dima_vc_gradient : '';

	$animation_data = '';
	$animation_data .= ( $animation != '' ) ? 'data-animate=' . $animation . '' : '';
	$animation_data .= ( $delay != '' ) ? ' data-delay=' . $delay . '' : '';
	$animation_data .= ( $delay_offset != '' ) ? ' data-offset=' . $delay_offset . '' : '';
	$animation_data .= ( $delay_duration != '' ) ? ' data-duration=' . $delay_duration . '' : '';


	$dec     = str_replace( '%', '', $percent );
	$heading = ( $heading != '' )
		? "<h6 class=\"h-skill\" {$animation_data} >{$heading}<span class=\"percent-end\">{$percent}</span></h6>"
		: '';
	$output  = $heading;
	$output  .= "<div class=\"dima-progressbar\" {$animation_data}><div {$id} class=\"{$class}\" data-color-val=\"{$color}\" {$style} >"
	            . "<div class=\"progress-bar {$dima_vc_gradient}\" role=\"progressbar\" aria-valuenow=\"{$dec}\" aria-valuemin=\"0\" aria-valuemax=\"100\">"
	            . "</div>"
	            . "</div>"
	            . "</div>";

	return $output;
}

add_shortcode( 'progress_bar', 'dima_shortcode_progress_bar' );

function dima_shortcode_progress_circle( $atts ) {
	extract(
		shortcode_atts(
			array(
				'id'               => '',
				'class'            => '',
				'style'            => '',
				'heading'          => '',
				'color'            => '',
				'color_border'     => '',
				'color_bg'         => '',
				'percent_txt'      => '',
				'color_icon'       => '',
				'bar_text'         => '',
				'icon'             => '',
				'icon_fontawesome' => '',
				'icon_svg'         => '',
				'percent'          => ''
			), $atts, 'progress_circle'
		)
	);

	$id               = ( $id != '' ) ? 'id="' . esc_attr( $id ) . '"' : '';
	$class            = ( $class != '' ) ? 'circular-bar ' . esc_attr( $class )
		: 'circular-bar';
	$style            = ( $style != '' ) ? 'style="' . $style . '"' : '';
	$heading          = ( $heading != '' ) ? '<span class="circular-bar-heading">'
	                                         . $heading . '</span>' : '';
	$color            = ( $color != '' ) ? $color : '#333333';
	$color_bg         = ( $color_bg != '' ) ? $color_bg : '';
	$color_border     = ( $color_border != '' ) ? $color_border : '';
	$color_icon       = ( $color_icon != '' ) ? 'style="color:' . $color_icon
	                                            . '"' : '';
	$percent_txt      = ( $percent_txt != '' ) ? 'style="color:' . $percent_txt
	                                             . '"' : '';
	$size             = '150';
	$_icon            = ( $icon != '' ) ? $icon : '';
	$icon_svg         = ( $icon_svg != '' ) ? dima_get_svg_icon( $icon_svg ) : '';
	$icon_fontawesome = ( $icon_fontawesome != '' ) ? $icon_fontawesome : '';
	$_icon            = ( $_icon == '' ) ? $icon_fontawesome : $_icon;
	$Style_bg         = '';

	if ( $color_bg != '' ) {
		$Style_bg .= 'background: ' . $color_bg . ';';
	}
	if ( $color_border != '' ) {
		$Style_bg .= 'border: 1px solid ' . $color_border . ';';
	}
	if ( $Style_bg != '' ) {
		$Style_bg = 'style="' . $Style_bg . '"';
	}
	if ( ! empty( $icon_svg ) ) {
		$_icon = '<span ' . $color_icon . '>' . $icon_svg . '<span>';
	} else if ( ! empty( $_icon ) ) {
		$_icon = '<i class="' . $_icon . '" ' . $color_icon . '></i>';
	}
	$class .= ( $_icon != '' ) ? ' pc_icon' : ' no_pc_icon';

	$dec = str_replace( '%', '', $percent );
	if ( $_icon == '' ) {
		$output
			= "<div class=\"dima-progressbar text-center\"><div {$id} class=\"{$class}\" {$style}>"
			  . "<span class=\"pc-circle\" {$Style_bg}>"
			  . "<span class=\"circular-bar-content\">"
			  . "<label {$percent_txt}></label>"
			  . "</span>"
			  . "</span>"
			  . "<input type=\"text\" class=\"dial\" data-fgColor=\"{$color}\" data-width=\"{$size}\" data-height=\"{$size}\" value=\"{$dec}\">"
			  . $heading
			  . "</div>"
			  . "</div>";
	} else {
		$output
			= "<div class=\"dima-progressbar text-center\"><div {$id} class=\"{$class}\" {$style}>"
			  . "<span class=\"pc-circle\" {$Style_bg}>"
			  . "<span class=\"circular-bar-content\">"
			  . "<label {$percent_txt}></label>"
			  . "</span>"
			  . "</span>"
			  . "<input type=\"text\" class=\"dial\" data-fgColor=\"{$color}\" data-width=\"{$size}\" data-height=\"{$size}\" value=\"{$dec}\">"
			  . "<div class=\"circular-bar-icon\">"
			  . $_icon
			  . "</div>"
			  . $heading
			  . "</div>"
			  . "</div>";
	}

	return $output;
}

add_shortcode( 'progress_circle', 'dima_shortcode_progress_circle' );


/**
 * Code
 *
 * @param      $atts
 * @param null $content
 *
 * @return string
 */
function dima_shortcode_code( $atts, $content = null ) {
	extract(
		shortcode_atts(
			array(
				'id'    => '',
				'class' => '',
				'style' => '',
			), $atts, 'code'
		)
	);

	$id    = ( $id != '' ) ? 'id="' . esc_attr( $id ) . '"' : '';
	$class = ( $class != '' ) ? esc_attr( $class ) : '';
	$style = ( $style != '' ) ? 'style="' . $style . '"' : '';

	$output
		= "<pre {$id} class=\"{$class}\" {$style}><code>{$content}</code></pre>";

	return $output;
}

add_shortcode( 'code', 'dima_shortcode_code' );

/**
 * Gallery
 *   1 Gallery Item (image)
 *
 * @param      $atts
 * @param null $content
 *
 * @return string
 */
function dima_shortcode_gallery( $atts, $content = null ) { // 1
	extract(
		shortcode_atts(
			array(
				'id'     => '',
				'class'  => '',
				'column' => '',
				'style'  => ''
			), $atts, 'dima_gallery'
		)
	);

	$id    = ( $id != '' ) ? 'id="' . esc_attr( $id ) . '"' : '';
	$class = ( $class != '' ) ? 'magnific-gallery ' . esc_attr( $class )
		: 'magnific-gallery';
	$class .= ( $column != '' ) ? ' columns-' . esc_attr( $column ) : '';
	$style = ( $style != '' ) ? 'style="' . $style . '"' : '';

	$output
		= "<div data-lightbox=\"gallery\" data-effect=\"mfp-zoom-in\" {$id} class=\"{$class}\" {$style}>"
		  . do_shortcode( $content )
		  . "</div>";

	return $output;
}

add_shortcode( 'dima_gallery', 'dima_shortcode_gallery' );

function image_shortcode_image( $atts ) {
	extract(
		shortcode_atts(
			array(
				'id'              => '',
				'class'           => '',
				'style'           => '',
				'type'            => '',
				'width'           => '',
				'float'           => '',
				'src'             => '',
				'alt'             => '',
				'href'            => '',
				'shape'           => '',
				'title'           => '',
				'target'          => '',
				'popup_type'      => '',
				'popup_place'     => '',
				'popup_trigger'   => '',
				'popup_content'   => '',
				'hover'           => '',
				'lightbox'        => '',
				'is_gallert_item' => 'false',
				'apply_gray'      => 'false',
				'dima_lazyimage'  => 'true',

				'animation'      => '',
				'delay'          => '',
				'delay_offset'   => '',
				'delay_duration' => '',
			), $atts, 'image'
		)
	);

	$id          = ( $id != '' ) ? 'id="' . esc_attr( $id ) . '"' : '';
	$class       = ( $class != '' ) ? ' dima-img ' . esc_attr( $class )
		: 'dima-img ';
	$width       = ( $width != '' ) ? ' columns-' . esc_attr( $width ) : '';
	$style       = ( $style != '' ) ? 'style="' . $style . '"' : '';
	$shape       = ( $shape != '' ) ? ' ' . $shape : '';
	$float       = ( $float != '' ) ? esc_attr( $float ) : '';
	$src         = ( $src != '' ) ? $src : '';
	$alt         = ( $alt != '' ) ? 'alt="' . $alt . '"' : 'alt=""';
	$title       = ( $title != '' ) ? 'title="' . $title . '"' : '';
	$target      = ( $target == 'blank' ) ? 'target="_blank" rel="noopener"' : '';
	$apply_gray  = ( $apply_gray == 'true' ) ? ' apply-gray' : '';
	$data_effect = ( $lightbox != '' ) ? 'data-effect="mfp-zoom-in"' : '';
	$lightbox    = ( $lightbox != '' ) ? 'data-lightbox=' . $lightbox : '';

	$delay          = ( $delay != '' ) ? ' data-delay=' . $delay . '' : '';
	$delay_offset   = ( $delay_offset != '' ) ? ' data-offset=' . $delay_offset . '' : '';
	$animation      = ( $animation != '' ) ? ' data-animate=' . $animation . '' : '';
	$delay_duration = ( $delay_duration != '' ) ? ' data-duration=' . $delay_duration . '' : '';
	$old_src        = $src;
	if ( $src != '' ) {
		if ( is_numeric( $src ) ) {
			$poster_info = wp_get_attachment_image_src( $src, 'full' );
			$src         = $poster_info[0];
			$img_width   = 'width="' . $poster_info[1] . '"';
			$img_height  = 'height="' . $poster_info[2] . '"';
		} else {
			$poster_info = dima_helper::dima_get_attachment_info_by_url( $src );
			$img_width   = 'width="' . $poster_info[1] . '"';
			$img_height  = 'height="' . $poster_info[2] . '"';
		}
	} else {
		return;
	}

	$tooltip_attr = ( $popup_type != '' ) ? dima_helper::dima_tooltip_data(
		$popup_type, $popup_trigger, $popup_place, '', $popup_content
	) : '';

	switch ( $float ) {
		case 'end' :
			$float = ' text-end';
			break;
		case 'start' :
			$float = ' text-start';
			break;
		default :
			$float = ' text-center';
	}

	if ( ! empty( $lightbox ) ) {
		wp_enqueue_script( 'magnific-js' );
		$href = ( $href == '' ) ? $src : $href;
	}
	if ( $is_gallert_item == 'true' ) {
		$lightbox = '';
		$class    .= ' dima-gallery-item';
	}

	if ( ! empty( $href ) ) {

		if ( dima_helper::dima_am_i_true( $dima_lazyimage ) && ! ( DIMA_AMP_IS_ACTIVE && is_amp_endpoint() ) && DIMA_USE_LAZY ) {
			$small_src = '';
			if ( is_numeric( $old_src ) ) {
				$poster_info = wp_get_attachment_image_src( $old_src, 'dima-lazy-image' );
				$small_src   = $poster_info[0];
			}
			$img = '<img class="js-lazy-image" src="' . $small_src . '" data-src="' . $src . '" ' . $alt . ' ' . $img_width . ' ' . $img_height . '>';
		} else {
			$img = '<img src="' . $src . '" ' . $alt . ' ' . $img_width . ' ' . $img_height . '>';
		}

		if ( ! empty( $hover ) ) {

			if ( $hover == 'zoom-out' ) {
				$output
					= "<div class=\"effect-roxy\"><a {$lightbox} {$data_effect} {$id} class=\"{$class}{$float}\" {$style} href=\"{$href}\" {$title} {$target} {$tooltip_attr}>{$img}</a></div>";
			} elseif ( $hover == 'zoom-in' ) {
				$output
					= "<div class=\"effect-julia\"><a {$lightbox} {$data_effect} {$id} class=\"{$class}{$float}\" {$style} href=\"{$href}\" {$title} {$target} {$tooltip_attr}>{$img}</a></div>";
			} elseif ( $hover == 'effect-opacity' ) {
				$output
					= "<div class=\"gray-opacity\"><a {$lightbox} {$data_effect} {$id} class=\"{$class}{$float}\" {$style} href=\"{$href}\" {$title} {$target} {$tooltip_attr}>{$img}</a></div>";
			} elseif ( $hover == 'none' ) {
				$output
					= "<a {$lightbox} {$data_effect} {$id} class=\"{$class}\" {$style} href=\"{$href}\" {$title} {$target} {$tooltip_attr}>{$img}</a>";
			}

		} else {
			$output
				= "<a {$lightbox} {$data_effect} {$id} class=\"overlay {$class}{$shape}\" {$style} href=\"{$href}\" {$title} {$target} {$tooltip_attr}>{$img}</a>";
		}
	} else {

		if ( dima_helper::dima_am_i_true( $dima_lazyimage ) && ! ( DIMA_AMP_IS_ACTIVE && is_amp_endpoint() ) && DIMA_USE_LAZY ) {
			$small_src = '';
			if ( is_numeric( $old_src ) ) {
				$poster_info = wp_get_attachment_image_src( $old_src, 'dima-lazy-image' );
				$small_src   = $poster_info[0];
			}
			$img = "<img class=\"$class js-lazy-image\" {$img_width} {$img_height} {$id} src=\"{$small_src}\" data-src=\"{$src}\" {$tooltip_attr} {$style} {$alt}>";
		} else {
			$img = "<img {$img_width} {$img_height} {$id} class=\"{$class}\"  src=\"{$src}\" {$tooltip_attr} {$style} {$alt}>";
		}
		$output
			= $img;
	}

	return "<div class=\"column-item overflow-hidden{$float}{$width}{$apply_gray}{$shape}\" {$animation} {$delay} {$delay_offset} {$delay_duration}>"
	       . $output . "</div>";
}

add_shortcode( 'image', 'image_shortcode_image' );

/**
 * Link Popovers and Tooltips
 *
 * @param      $atts
 * @param null $content
 *
 * @return string
 */
function dima_shortcode_dima_link( $atts, $content = null ) {
	extract(
		shortcode_atts(
			array(
				'id'            => '',
				'class'         => '',
				'style'         => '',
				'href'          => '',
				'title'         => '',
				'target'        => '',
				'popup_type'    => '',
				'popup_place'   => '',
				'link_style'    => '',
				'popup_trigger' => '',
				'popup_content' => '',
				'lightbox'      => '',
			), $atts, 'link'
		)
	);

	$id          = ( $id != '' ) ? 'id="' . esc_attr( $id ) . '"' : '';
	$class       = ( $class != '' ) ? ' ' . esc_attr( $class ) : '';
	$class       .= ( $link_style != '' ) ? ' btm_' . esc_attr( $link_style ) : '';
	$style       = ( $style != '' ) ? 'style="' . $style . '"' : '';
	$title       = ( $title != '' ) ? 'title="' . $title . '"' : '';
	$target      = ( $target == 'blank' ) ? 'target="_blank" rel="noopener"' : '';
	$data_effect = ( $lightbox != '' ) ? 'data-effect="mfp-zoom-in"' : '';
	$lightbox    = ( $lightbox != '' ) ? 'data-lightbox=' . $lightbox : '';

	$tooltip_attr = ( $popup_type != '' ) ? dima_helper::dima_tooltip_data(
		$popup_type, $popup_trigger, $popup_place, '', $popup_content
	) : '';

	if ( ! empty( $lightbox ) ) {
		wp_enqueue_script( 'magnific-js' );
		$href = ( $href == '' ) ? $src : $href;
	}

	$output
		= "<a {$lightbox} {$data_effect} {$id} class=\"{$class}\" href=\"{$href}\" {$target} {$tooltip_attr} {$title} {$style}>"
		  . do_shortcode( $content ) . "</a>";

	return $output;
}

add_shortcode( 'link', 'dima_shortcode_dima_link' );


/**
 * Button
 */
/**
 * Button
 *
 * @param $atts
 *
 * @return string
 */
function dima_shortcode_button( $atts ) {
	extract(
		shortcode_atts(
			array(
				'id'                 => '',
				'class'              => '',
				'style'              => '',
				'type'               => '',
				'color_class'        => '',
				'size'               => '',
				'float'              => '',
				'block'              => '',
				'shape'              => '',
				'icon'               => '',
				'href'               => '',
				'dima_vc_disabled'   => '',
				'title'              => '',
				'text'               => '',
				'target'             => '',
				'popup_type'         => '',
				'popup_place'        => '',
				'popup_trigger'      => '',
				'popup_content'      => '',
				'lightbox'           => '',
				'icon_fontawesome'   => '',
				'icon_svg'           => '',
				'dima_vc_add_shadow' => '',

				'animation'      => '',
				'delay'          => '',
				'delay_offset'   => '',
				'delay_duration' => '',
			), $atts, 'button'
		)
	);

	$id               = ( $id != '' ) ? 'id="' . esc_attr( $id ) . '"' : '';
	$_class[]         = ( $class != '' ) ? 'dima-button dima-waves ' . esc_attr(
			$class
		) : 'dima-button dima-waves';
	$_class[]         = ( $color_class != '' ) ? '' . esc_attr( $color_class ) . ' '
		: '';
	$style            = ( $style != '' ) ? 'style="' . $style . '"' : '';
	$_class[]         = ( $type != '' ) ? $type : 'fill';
	$_class[]         = ( $size != '' ) ? 'dima-btn-' . $size : 'dima-btn-small';
	$data_effect      = ( $lightbox != '' ) ? 'data-effect="mfp-zoom-in"' : '';
	$lightbox         = ( $lightbox != '' ) ? 'data-lightbox=' . $lightbox : '';
	$_class[]         = ( $icon != '' ) ? ' icon' : '';
	$_class[]         = ( $dima_vc_add_shadow == 'true' ) ? 'dima-shadow' : '';
	$_icon            = ( $icon != '' ) ? $icon : '';
	$icon_svg         = ( $icon_svg != '' ) ? dima_get_svg_icon( $icon_svg ) : '';
	$icon_fontawesome = ( $icon_fontawesome != '' ) ? $icon_fontawesome : '';

	$animation_data = '';
	$animation_data .= ( $animation != '' ) ? 'data-animate=' . $animation . '' : '';
	$animation_data .= ( $delay != '' ) ? ' data-delay=' . $delay . '' : '';
	$animation_data .= ( $delay_offset != '' ) ? ' data-offset=' . $delay_offset . '' : '';
	$animation_data .= ( $delay_duration != '' ) ? ' data-duration=' . $delay_duration . '' : '';

	if ( ! empty( $lightbox ) ) {
		wp_enqueue_script( 'magnific-js' );
	}

	switch ( $float ) {
		case 'end' :
			$_class[] = 'float-end';
			break;
		case 'start' :
			$_class[] = 'float-start';
			break;
		case 'center' :
			$_class[] = 'float-center';
			break;
		default :
			$_class[] = '';
	}
	$_icon = ( $_icon == '' ) ? $icon_fontawesome : $_icon;

	if ( ! empty( $icon_svg ) ) {
		$_icon = $icon_svg;
	} else if ( ! empty( $_icon ) ) {
		$_icon = '<i class="' . $_icon . '"></i>';
	}

	$_class[]     = ( $block == 'true' ) ? 'button-block' : '';
	$_class[]     = ( $dima_vc_disabled == 'true' ) ? 'disabled' : '';
	$_class[]     = ( $shape != '' ) ? ' ' . $shape . ' ' : '';
	$href         = ( $href != '' ) ? 'href="' . esc_url( $href ) . '"' : 'href="#"';
	$title        = ( $title != '' ) ? 'title="' . $title . '"' : '';
	$target       = ( $target == 'blank' ) ? 'target="_blank" rel="noopener"' : '';
	$tooltip_attr = ( $popup_type != '' ) ? dima_helper::dima_tooltip_data(
		$popup_type, $popup_trigger, $popup_place, '', $popup_content
	) : '';

	if ( ! empty( $_class ) ) {
		$class = dima_short_remove_white_space( join( ' ', $_class ) );
	}

	$output
		= "<a {$lightbox} {$data_effect} {$id} {$href}  class=\"{$class}\" {$animation_data} {$style} {$title} {$target} {$tooltip_attr} >{$_icon}{$text}</a>";

	return $output;
}

add_shortcode( 'button', 'dima_shortcode_button' );


/**
 * List
 * 1 List Item
 */
function dima_shortcode_list( $atts, $content = null ) { // 1
	extract(
		shortcode_atts(
			array(
				'id'    => '',
				'class' => '',
				'type'  => '',
				'style' => '',

				'animation'              => '',
				'delay'                  => '',
				'delay_offset'           => '',
				'delay_duration'         => '',
				'data_dima_animate_item' => '',

			), $atts, 'list'
		)
	);

	$id    = ( $id != '' ) ? 'id="' . esc_attr( $id ) . '"' : '';
	$class = ( $class != '' ) ? 'list-style ' . esc_attr( $class ) : 'list-style';
	$type  = ( $type != '' ) ? ' ' . esc_attr( $type ) : '';
	$style = ( $style != '' ) ? 'style="' . $style . '"' : '';

	$animation_data = '';
	if ( $animation != '' ) {
		$animation_data .= ( $animation != '' ) ? 'data-animate=' . $animation . '' : '';
		$animation_data .= ( $delay != '' ) ? ' data-delay=' . $delay . '' : ' data-delay="30"';
		$animation_data .= ( $delay_offset != '' ) ? ' data-offset=' . $delay_offset . '' : '';
		$animation_data .= ( $delay_duration != '' ) ? ' data-duration=' . $delay_duration . '' : '';
		$animation_data .= ( $data_dima_animate_item ) ? ' data-dima-animate-item=' . $data_dima_animate_item . '' : ' data-dima-animate-item="li"';
	}

	$output = "<ul {$id} class=\"{$class}{$type}\" {$animation_data} {$style}>" . do_shortcode(
			$content
		) . "</ul>";

	return $output;
}

add_shortcode( 'list', 'dima_shortcode_list' );

function dima_shortcode_list_item( $atts, $content = null ) {
	extract(
		shortcode_atts(
			array(
				'id'    => '',
				'class' => '',
				'style' => '',
			), $atts, 'list_item'
		)
	);

	$id    = ( $id != '' ) ? 'id="' . esc_attr( $id ) . '"' : '';
	$class = ( $class != '' ) ? 'class="' . esc_attr( $class ) . '"' : '';
	$style = ( $style != '' ) ? 'style="' . $style . '"' : '';

	$output = "<li {$id} {$class} {$style}>" . do_shortcode( $content ) . "</li>";

	return $output;
}

add_shortcode( 'list_item', 'dima_shortcode_list_item' );


/**
 * Icon List
 *   1 Icon List Item
 */

function dima_shortcode_icon_list( $atts, $content = null ) {
	extract(
		shortcode_atts(
			array(
				'id'    => '',
				'class' => '',
				'style' => ''
			), $atts, 'icon_list'
		)
	);

	$id    = ( $id != '' ) ? 'id="' . esc_attr( $id ) . '"' : '';
	$class = ( $class != '' ) ? 'icon-list ' . esc_attr( $class ) : 'icon-list';
	$style = ( $style != '' ) ? 'style="' . $style . '"' : '';

	$output = "<ul {$id} class=\"{$class}\" {$style}>" . do_shortcode( $content )
	          . "</ul>";

	return $output;
}

add_shortcode( 'icon_list', 'dima_shortcode_icon_list' );

function dima_shortcode_icon_list_item( $atts, $content = null ) { // 2
	extract(
		shortcode_atts(
			array(
				'id'               => '',
				'class'            => '',
				'style'            => '',
				'icon'             => '',
				'icon_fontawesome' => '',
				'icon_color'       => '',
				'icon_svg'         => ''
			), $atts, 'icon_list_item'
		)
	);

	$id               = ( $id != '' ) ? 'id="' . esc_attr( $id ) . '"' : '';
	$class            = ( $class != '' ) ? 'class="' . esc_attr( $class ) . '"'
		: '';
	$style            = ( $style != '' ) ? 'style="' . $style . '"' : '';
	$_icon            = ( $icon != '' ) ? $icon : '';
	$icon_svg         = ( $icon_svg != '' ) ? dima_get_svg_icon( $icon_svg ) : '';
	$icon_fontawesome = ( $icon_fontawesome != '' ) ? $icon_fontawesome : '';
	$_icon            = ( $_icon == '' ) ? $icon_fontawesome : $_icon;
	$icon_color       = ( $icon_color != '' ) ? 'style="color:' . $icon_color
	                                            . ';"' : '';

	if ( ! empty( $icon_svg ) ) {
		$_icon = "<span " . $icon_color . ">" . $icon_svg . "</span>";
	} else if ( ! empty( $_icon ) ) {
		$_icon = '<i ' . $icon_color . ' class="' . $_icon . '"></i>';
	}

	$output = "<li {$id} {$class} {$style}>{$_icon}" . do_shortcode( $content )
	          . "</li>";

	return $output;
}

add_shortcode( 'icon_list_item', 'dima_shortcode_icon_list_item' );

/**
 * Video
 *   1 Embed Video
 */

function dima_shortcode_video_player( $atts ) {
	extract(
		shortcode_atts(
			array(
				'id'            => '',
				'class'         => '',
				'style'         => '',
				'type'          => '',
				'm4v'           => '',
				'ogv'           => '',
				'webm'          => '',
				'poster_img'    => '',
				'preload'       => '',
				'hide_controls' => 'false',
				'autoplay'      => 'false',
				'loop'          => 'false',
				'muted'         => 'false',
			), $atts, 'dima_video_player'
		)
	);

	$id            = ( $id != '' ) ? 'id="' . esc_attr( $id ) . '"' : '';
	$class         = ( $class != '' ) ? 'dima-video player ' . esc_attr( $class )
		: 'dima-video player';
	$style         = ( $style != '' ) ? 'style="' . $style . '"' : '';
	$type          = '';
	$m4v           = ( $m4v != '' ) ? '<source src="' . $m4v . '" type="video/mp4">' : '';
	$ogv           = ( $ogv != '' ) ? '<source src="' . $ogv . '" type="video/ogg">' : '';
	$webm          = ( $webm != '' ) ? '<source src="' . $webm . '" type="video/webm">' : '';
	$poster        = ( $poster_img != '' ) ? $poster_img : '';
	$preload       = ( $preload != '' ) ? ' preload="' . $preload . '"' : ' preload="none"';
	$hide_controls = ( $hide_controls != 'false' ) ? ' hide-controls' : '';
	$autoplay      = ( $autoplay != 'false' ) ? ' autoplay' : '';
	$loop          = ( $loop != 'false' ) ? ' loop' : '';
	$muted         = ( $muted != 'false' ) ? ' muted' : '';

	if ( is_numeric( $poster ) ) {
		$poster_info = wp_get_attachment_image_src( $poster, 'full' );
		$poster      = $poster_info[0];
	}
	$poster_attr = ( $poster != '' ) ? ' poster="' . $poster . '"' : '';

	wp_enqueue_script( 'wp-mediaelement' );
	wp_enqueue_style( 'wp-mediaelement' );

	$data = dima_creat_data_attributes( 'audio-video' );

	$output
		= "<div {$id} class=\"{$class}{$hide_controls}\" {$data} {$style}>"
		  . "<div class=\"dima-video-wrapper{$type}\">"
		  . "<video class=\"audio-video\"{$poster_attr}{$preload}{$autoplay}{$loop}{$muted}>"
		  . $m4v
		  . $ogv
		  . $webm
		  . '</video>'
		  . '</div>'
		  . '</div>';


	return $output;
}

add_shortcode( 'dima_video_player', 'dima_shortcode_video_player' );

function dima_shortcode_embed_video( $atts, $content = null ) {
	extract(
		shortcode_atts(
			array(
				'id'     => '',
				'class'  => '',
				'style'  => '',
				'poster' => '',
			), $atts, 'dima_embed_video'
		)
	);

	$id     = ( $id != '' ) ? 'id="' . esc_attr( $id ) . '"' : '';
	$class  = ( $class != '' ) ? 'dima-video embed ' . esc_attr( $class ) : 'dima-video embed';
	$style  = ( $style != '' ) ? 'style="' . $style . '"' : '';
	$poster = ( $poster != '' ) ? $poster : '';

	static $count = 0;
	$count ++;
	if ( ! empty( $poster ) ) {
		$output = "<div class='video-overlay' data-bg=\"{$poster}\">"
		          . "<div class='video-overlay-hover'>"
		          . "<a href='#' class='video-play-button'></a>"
		          . "</div>"
		          . "</div>"
		          . "<div {$id}  class=\"{$class}\" {$style}><div class=\"embed-responsive embed-responsive-16by9\">{$content}</div>"
		          . "</div>";
	} else {
		$output
			= "<div {$id}  class=\"{$class}\" {$style}><div class=\"embed-responsive embed-responsive-16by9\">{$content}</div>"
			  . "</div>";

	}

	return $output;
}

add_shortcode( 'dima_embed_video', 'dima_shortcode_embed_video' );


/**
 * 1. Accordion.
 *   2. Accordion item.
 */

function dima_shortcode_accordion( $atts, $content = null ) {
	extract(
		shortcode_atts(
			array(
				'id'    => '',
				'class' => '',
				'model' => '',
				'open'  => '',
				'style' => '',

				'animation'              => '',
				'delay'                  => '',
				'delay_offset'           => '',
				'delay_duration'         => '',
				'data_dima_animate_item' => '',
			), $atts, 'accordion'
		)
	);

	$id    = ( $id != '' ) ? 'id="' . esc_attr( $id ) . '"' : '';
	$class = ( $class != '' ) ? 'dima-accordion ' . esc_attr( $class ) : 'dima-accordion';
	$model = ( $model == '' ) ? '' : $model;
	$class .= ( $open == 'true' ) ? ' no-active-accordion' : '';

	$animation_data = '';
	$animation_data .= ( $animation != '' ) ? 'data-animate=' . $animation . '' : 'data-animate=transition.slideUpIn';
	$animation_data .= ( $delay != '' ) ? ' data-delay=' . $delay . '' : ' data-delay="70"';
	$animation_data .= ( $delay_offset != '' ) ? ' data-offset=' . $delay_offset . '' : ' data-offset="98%"';
	$animation_data .= ( $delay_duration != '' ) ? ' data-duration=' . $delay_duration . '' : '';
	$animation_data .= ( $data_dima_animate_item ) ? ' data-dima-animate-item=' . $data_dima_animate_item . '' : ' data-dima-animate-item=".dima-accordion-group"';

	switch ( $model ) {
		case 'line':
			$class .= ' dima-acc-clear';
			break;
		case 'border':
			$class .= ' acc-with-border';
			break;
		default:
			$class .= ' acc-default';
	}
	$style = ( $style != '' ) ? 'style="' . $style . '"' : '';

	$output = "<ul {$id} class=\"{$class}\" {$animation_data} {$style}>" . do_shortcode( $content )
	          . "</ul>";

	return $output;
}

add_shortcode( 'accordion', 'dima_shortcode_accordion' );


function dima_shortcode_accordion_item( $atts, $content = null ) { // 2
	extract(
		shortcode_atts(
			array(
				'id'                   => '',
				'class'                => '',
				'style'                => '',
				'parent_id'            => '',
				'title'                => '',
				'icon'                 => '',
				'icon_svg'             => '',
				'icon_fontawesome'     => '',
				'icon_two'             => '',
				'icon_svg_two'         => '',
				'icon_fontawesome_two' => '',
				'bg_color'             => '',
				'icon_color'           => '',
				'open'                 => ''
			), $atts, 'accordion_item'
		)
	);

	$id                   = ( $id != '' ) ? 'id="' . esc_attr( $id ) . '"' : '';
	$class                = ( $class != '' ) ? 'panel dima-accordion-group '
	                                           . esc_attr(
		                                           $class
	                                           ) : 'panel dima-accordion-group';
	$style                = ( $style != '' ) ? 'style="' . $style . '"' : '';
	$parent_id            = ( $parent_id != '' ) ? 'data-parent="#' . $parent_id
	                                               . '"' : '';
	$title                = ( $title != '' ) ? '<span class="dima-accordion-title">' . $title . '</span>' : 'Make Sure to Set a Title';
	$open                 = ( $open == 'true' ) ? 'collapse in' : 'collapse';
	$_icon                = ( $icon != '' ) ? $icon : '';
	$icon_svg             = ( $icon_svg != '' ) ? dima_get_svg_icon( $icon_svg )
		: '';
	$icon_fontawesome     = ( $icon_fontawesome != '' ) ? $icon_fontawesome : '';
	$bg_color             = ( $bg_color != '' ) ? 'background-color:' . $bg_color
	                                              . ';' : '';
	$icon_color           = ( $icon_color != '' ) ? 'color:' . $icon_color
	                                                . ';fill: ' . $icon_color . '' : '';
	$_icon_two            = ( $icon_two != '' ) ? $icon_two : '';
	$icon_svg_two         = ( $icon_svg_two != '' ) ? dima_get_svg_icon(
		$icon_svg_two
	) : '';
	$icon_fontawesome_two = ( $icon_fontawesome_two != '' )
		? $icon_fontawesome_two : '';
	static $count = 0;
	$count ++;
	$icon_style = '';
	if ( $bg_color != '' || $icon_color != '' ) {
		$icon_style = "style=\"$bg_color$icon_color\"";
	}

	$_icon = ( $_icon == '' ) ? $icon_fontawesome : $_icon;
	if ( ! empty( $icon_svg ) ) {
		$_icon = $icon_svg;
	} else if ( ! empty( $_icon ) ) {
		$_icon = '<i class="' . $_icon . '"></i>';
	}
	if ( ! empty( $_icon ) ) {
		$_icon = "<span class='icon_one' $icon_style>{$_icon}</span>";
	}

	$_icon_two = ( $_icon_two == '' ) ? $icon_fontawesome_two : $_icon_two;
	if ( ! empty( $icon_svg_two ) ) {
		$_icon_two = $icon_svg_two;
	} else if ( ! empty( $_icon_two ) ) {
		$_icon_two = '<i class="' . $_icon_two . '"></i>';
	}
	if ( ! empty( $_icon_two ) ) {
		$_icon_two = "<span class='icon_two' $icon_style>{$_icon_two}</span>";
	}
	if ( ! empty( $_icon_two ) && ! empty( $_icon_two ) ) {
		$class .= ' double_icon';
	}

	if ( $open == 'collapse in' ) {
		$output = "<li {$id} class=\"{$class}\" {$style}>"
		          . '<div class="dima-accordion-header">'
		          . "<a class=\"dima-accordion-toggle\" data-toggle=\"collapse\" {$parent_id} href=\"#collapse-{$count}\">{$title}{$_icon}{$_icon_two}</a>"
		          . '</div>'
		          . "<div id=\"collapse-{$count}\" class=\"dima-accordion-content {$open}\">"
		          . '<div class="dima-accordion-inner">'
		          . wpb_js_remove_wpautop( $content, true )
		          . '</div>'
		          . '</div>'
		          . '</li>';

	} else {

		$output = "<li {$id} class=\"{$class}\" {$style}>"
		          . '<div class="dima-accordion-header">'
		          . "<a class=\"dima-accordion-toggle collapsed\" data-toggle=\"collapse\" {$parent_id} href=\"#collapse-{$count}\">{$title}{$_icon}{$_icon_two}</a>"
		          . '</div>'
		          . "<div id=\"collapse-{$count}\" class=\"dima-accordion-content {$open}\">"
		          . '<div class="dima-accordion-inner">'
		          . wpb_js_remove_wpautop( $content, true )
		          . '</div>'
		          . '</div>'
		          . '</li>';

	}

	return $output;
}

add_shortcode( 'accordion_item', 'dima_shortcode_accordion_item' );

/**
 * Tab nav
 *   1 Tab nav item
 *   2 Tabs
 *   3 Tab
 */

function dima_shortcode_tab_nav( $atts, $content = null ) { // 1
	extract(
		shortcode_atts(
			array(
				'id'    => '',
				'class' => '',
				'style' => '',
				'clm'   => '',
				'model' => '',
				'float' => ''
			), $atts, 'tab_nav'
		)
	);

	$id       = ( $id != '' ) ? 'id="' . esc_attr( $id ) . '"' : '';
	$_class[] = ( $class != '' ) ? 'dima-tabs' . esc_attr( $class ) : 'dima-tabs ';
	$_class[] = ( $model != '' ) ? $model : 'tabs_style_1';
	$style    = ( $style != '' ) ? 'style="' . $style . '"' : '';

	$float = ( $float != '' ) ? $float : 'top';

	switch ( $float ) {
		case 'end':
			$float    = "float-end";
			$_class[] = "tabs_float_end";
			$clm      = '';
			break;
		case 'start':
			$float    = "float-start";
			$_class[] = "tabs_float_start";
			$clm      = '';
			break;

		default:
			$float    = "top";
			$_class[] = "tabs_on_top";
			break;
	}
	$_class[] = ( $clm != '' ) ? $clm : '';

	if ( ! empty( $_class ) ) {
		$class = join( ' ', $_class );
	}

	$output = "<div {$id} class=\"{$class}\" {$style}>"
	          . "<ul class=\"dima-tab-nav {$float}\">" . do_shortcode( $content )
	          . "</ul>"
	          . "";

	return $output;
}

add_shortcode( 'tab_nav', 'dima_shortcode_tab_nav' );


function dima_shortcode_tab_nav_item( $atts ) {
	extract(
		shortcode_atts(
			array(
				'id'               => '',
				'class'            => '',
				'style'            => '',
				'icon'             => '',
				'icon_fontawesome' => '',
				'icon_svg'         => '',
				'title'            => '',
				'active'           => ''
			), $atts, 'tab_nav_item'
		)
	);

	$id               = ( $id != '' ) ? 'id="' . esc_attr( $id ) . '"' : '';
	$class            = ( $class != '' ) ? 'tab ' . esc_attr( $class ) : 'tab';
	$style            = ( $style != '' ) ? 'style="' . $style . '"' : '';
	$title            = ( $title != '' ) ? $title : 'Title Here';
	$active           = ( $active == 'true' ) ? ' active' : '';
	$_icon            = ( $icon != '' ) ? $icon : '';
	$icon_svg         = ( $icon_svg != '' ) ? dima_get_svg_icon( $icon_svg ) : '';
	$icon_fontawesome = ( $icon_fontawesome != '' ) ? $icon_fontawesome : '';
	$_icon            = ( $_icon == '' ) ? $icon_fontawesome : $_icon;

	if ( ! empty( $icon_svg ) ) {
		$_icon = $icon_svg;
	} else if ( ! empty( $_icon ) ) {
		$_icon = '<i class="' . $_icon . '"></i>';
	}
	static $count = 0;
	$count ++;

	$output
		= "<li {$id} class=\"{$class}{$active}\" {$style}><a href=\"#tab-{$count}\" data-toggle=\"tab\">{$_icon}{$title}</a></li>";

	return $output;
}

add_shortcode( 'tab_nav_item', 'dima_shortcode_tab_nav_item' );

function dima_shortcode_tabs( $atts, $content = null ) {
	extract(
		shortcode_atts(
			array(
				'id'    => '',
				'class' => '',
				'style' => ''
			), $atts, 'tabs'
		)
	);

	$id     = ( $id != '' ) ? 'id="' . esc_attr( $id ) . '"' : '';
	$class  = ( $class != '' ) ? 'dima-tab-content ' . esc_attr( $class )
		: 'dima-tab-content';
	$style  = ( $style != '' ) ? 'style="' . $style . '"' : '';
	$output = "<div {$id} class=\"{$class}\" {$style}>" . do_shortcode( $content )
	          . "</div></div>";

	return $output;
}

add_shortcode( 'tabs', 'dima_shortcode_tabs' );


function dima_shortcode_tab( $atts, $content = null ) { // 4
	extract(
		shortcode_atts(
			array(
				'class'  => '',
				'style'  => '',
				'active' => ''
			), $atts, 'tab'
		)
	);

	$class  = ( $class != '' ) ? 'tab-pane fade in clearfix dima-tab_content '
	                             . esc_attr( $class ) : 'tab-pane fade clearfix dima-tab_content';
	$style  = ( $style != '' ) ? 'style="' . $style . '"' : '';
	$active = ( $active == 'true' ) ? ' fade in active' : '';

	static $count = 0;
	$count ++;

	$output = "<div id=\"tab-{$count}\" class=\"{$class}{$active}\" {$style}>"
	          . do_shortcode( $content ) . "</div>";

	return $output;
}

add_shortcode( 'tab', 'dima_shortcode_tab' );


/**
 * OK Grids
 *   1 Container
 *   2 Row
 *   3 Column
 */
/**
 * @param      $atts
 * @param null $content
 *
 * @return string
 */
function dima_shortcode_container( $atts, $content = null ) {
	extract(
		shortcode_atts(
			array(
				'id'        => '',
				'class'     => '',
				'style'     => '',
				'no_margin' => '',
			), $atts, 'container'
		)
	);

	$id        = ( $id != '' ) ? 'id="' . esc_attr( $id ) . '"' : '';
	$class     = ( esc_attr( $class ) ) ? 'container ' . esc_attr( $class )
		: 'container';
	$style     = ( $style != '' ) ? 'style=' . $style : '';
	$no_margin = ( $no_margin == 'false' ) ? ' no-margin' : '';

	$output
		= "<div {$id} class=\"{$class}\" {$style}><div class=\"ok-row{$no_margin}\">"
		  . do_shortcode( $content ) . "</div></div>";

	return $output;
}

add_shortcode( 'container', 'dima_shortcode_container' );


/*function dima_shortcode_content_row( $atts, $content = null ) {
	extract( shortcode_atts( array(
		'id'     => '',
		'class'  => '',
		'style'  => '',
		'margin' => '',
	), $atts, 'row' ) );

	$id     = ( $id != '' ) ? 'id="' . esc_attr( $id ) . '"' : '';
	$class  = ( esc_attr( $class ) ) ? 'ok-row ' . esc_attr( $class ) : 'ok-row';
	$style  = ( $style != '' ) ? 'style=' . $style : '';
	$margin = ( $margin == 'false' ) ? ' no-margin' : '';

	$output = "<div {$id} class=\"{$class}{$margin}\" {$style}>" . do_shortcode( $content ) . "</div>";

	return $output;
}

add_shortcode( 'row', 'dima_shortcode_content_row' );*/

function dima_shortcode_content_columns( $atts, $content = null ) {
	extract(
		shortcode_atts(
			array(
				'id'             => '',
				'class'          => '',
				'style'          => '',
				'xld'            => '',
				'ld'             => '',
				'md'             => '',
				'sd'             => '6',
				'xsd'            => '12',
				'visibility_xld' => '',
				'visibility_ld'  => '',
				'visibility_md'  => '',
				'visibility_sd'  => '',
				'visibility_xsd' => '',
				'offset_xld'     => '',
				'offset_ld'      => '',
				'offset_md'      => '',
				'offset_sd'      => '',
				'offset_xsd'     => '',
				'animation'      => '',
				'delay'          => '',
				'offset'         => '',
				'bg_image'       => '',
			), $atts, 'column'
		)
	);

	$id    = ( $id != '' ) ? 'id="' . esc_attr( $id ) . '"' : '';
	$class = ( esc_attr( $class ) ) ? esc_attr( $class ) : '';
	$class .= ( esc_attr( $xld ) ) ? ' ok-xld-' . esc_attr( $xld ) : '';
	$class .= ( esc_attr( $ld ) ) ? ' ok-ld-' . esc_attr( $ld ) : '';
	$class .= ( esc_attr( $md ) ) ? ' ok-md-' . esc_attr( $md ) : '';
	$class .= ( esc_attr( $sd ) ) ? ' ok-sd-' . esc_attr( $sd ) : '';
	$class .= ( esc_attr( $xsd ) ) ? ' ok-xsd-' . esc_attr( $xsd ) : '';

	if ( $visibility_xld != '' ) {
		$visibility_xld = explode( ',', $visibility_xld );
		$class          .= ( esc_attr( $visibility_xld[0] ) != '' ) ? ' '
		                                                              . esc_attr( $visibility_xld[0] ) . '-xld' : '';
	}
	if ( $visibility_ld != '' ) {
		$visibility_ld = explode( ',', $visibility_ld );
		$class         .= ( esc_attr( $visibility_ld[0] ) != '' ) ? ' ' . esc_attr(
				$visibility_ld[0]
			) . '-ld' : '';
	}
	if ( $visibility_md != '' ) {
		$visibility_md = explode( ',', $visibility_md );
		$class         .= ( esc_attr( $visibility_md[0] ) != '' ) ? ' ' . esc_attr(
				$visibility_md[0]
			) . '-md' : '';
	}
	if ( $visibility_sd != '' ) {
		$visibility_sd = explode( ',', $visibility_sd );
		$class         .= ( esc_attr( $visibility_sd[0] ) != '' ) ? ' ' . esc_attr(
				$visibility_sd[0]
			) . '-sd' : '';
	}
	if ( $visibility_xsd != '' ) {
		$visibility_xsd = explode( ',', $visibility_xsd );
		$class          .= ( esc_attr( $visibility_xsd[0] ) != '' ) ? ' '
		                                                              . esc_attr( $visibility_xsd[0] ) . '-xsd' : '';
	}

	$class     .= ( esc_attr( $offset_xld ) != '' ) ? ' ok-offset-xld-' . esc_attr(
			$offset_xld
		) : '';
	$class     .= ( esc_attr( $offset_ld ) != '' ) ? ' ok-offset-ld-' . esc_attr(
			$offset_ld
		) : '';
	$class     .= ( esc_attr( $offset_md ) != '' ) ? ' ok-offset-md-' . esc_attr(
			$offset_md
		) : '';
	$style     = ( $style != '' ) ? 'style="' . $style . '"' : '';
	$animation = ( $animation != '' ) ? 'data-animate=' . $animation . '' : '';
	$delay     = ( $delay != '' ) ? ' data-delay=' . $delay . '' : '';
	$offset    = ( $offset != '' ) ? ' data-offset=' . $offset . '' : '';
	$bg_image  = ( $bg_image != '' ) ? $bg_image : '';
	$bg_holder = '';
	if ( ! empty( $bg_image ) ) {
		$bg_holder = '<div class="background-image-holder">'
		             . '<img src="' . $bg_image . '" alt="">'
		             . '</div>';
		$class     .= ' set-parent-height';
	}
	$output
		= "<div {$id} class=\"{$class}\" {$style} {$animation}{$delay}{$offset}>"
		  . $bg_holder
		  . do_shortcode( $content )
		  . "</div>";


	return $output;
}

add_shortcode( 'column', 'dima_shortcode_content_columns' );

function dima_shortcode_slick_slider( $atts, $content = null ) {
	extract(
		shortcode_atts(
			array(
				'id'              => '',
				'class'           => '',
				'style'           => '',
				'pagination'      => '',
				'auto_play'       => '',
				'auto_play_speed' => '',
				'navigation'      => '',
				'dots_style'      => 'false',
				'inner'           => '',
				'loop'            => 'true',
				'centermode'      => '',
				'centerpadding'   => '',
				'adaptiveheight'  => '',
				'variablewidth'   => '',
				'_draggable'      => 'true',
				'lazyload'        => '',
				'mobilefirst'     => '',
				'pauseonfocus'    => '',
				'pauseonhover'    => '',
				'fade'            => '',
				'verticalswiping' => '',
				'_vertical'       => '',
				'speed'           => '',
				'dark'            => '',
				'items'           => '',
				'items_phone'     => '',
				'items_tablet'    => '',
				'slidestoscroll'  => 1,
				'items_margin'    => '',
			), $atts, 'slick_slider'
		)
	);

	static $count = 0;
	$count ++;
	wp_enqueue_script( 'dima-slick' );

	$id    = ( $id != '' ) ? 'id="' . esc_attr( $id ) . '"' : '';
	$class = ( $class != '' ) ? esc_attr( $class ) : "slick-slider";
	$class .= ( $centermode == 'true' ) ? " center_zoom_opacity" : "";
	$style = ( $style != '' ) ? 'style="' . $style . '"' : '';

	$js_data = array(
		'dots'            => ( $pagination == 'true' ),
		'autoplay'        => ( $auto_play == 'true' ),
		'arrows'          => ( $navigation == '1' ),
		'slidesToScroll'  => ( $slidestoscroll == '' ) ? 1 : $slidestoscroll,
		'infinite'        => ( $loop == 'true' ),
		'fade'            => ( $fade == 'true' ),
		'centerMode'      => ( $centermode == 'true' ),
		'centerPadding'   => ( $centerpadding == '' ) ? '50px' : $centerpadding,
		'variableWidth'   => ( $variablewidth == 'true' ),
		'draggable'       => ( $_draggable == 'true' ),
		'asNavFor'        => null,
		'adaptiveHeight'  => ( $adaptiveheight == 'true' ),
		'mobileFirst'     => ( $mobilefirst == 'true' ),
		'pauseOnHover'    => ( $pauseonhover == 'true' ),
		'pauseOnFocus'    => ( $pauseonfocus == 'true' ),
		'vertical'        => ( $_vertical == 'true' ),
		'verticalSwiping' => ( $verticalswiping == 'true' ),
		'slidesToShow'    => ( $items == '' ) ? 1 : $items,
		'speed'           => ( $speed == '' ) ? '300' : $speed,
		'autoplaySpeed'   => ( $auto_play_speed == '' ) ? '3000'
			: $auto_play_speed,
		'lazyLoad'        => ( $lazyload == '' ) ? 'Ondemand' : $lazyload,
		'items_phone'     => ( $items_phone == '' ) ? 1 : $items_phone,
		'items_tablet'    => ( $items_tablet == '' ) ? 1 : $items_tablet,
		'rtl'             => is_rtl()
	);

	$data = dima_creat_data_attributes( 'slick_slider', $js_data );

	if ( $dark == "true" ) {
		$dark = " slick-darck";
	} else {
		$dark = "";
	}

	if ( $dots_style == "true" ) {
		$class .= " slick_side_dots";
	}

	if ( $items_margin == "true" ) {
		$dark .= " slick-with-margin";
	} else {
		$dark .= "";
	}

	$output = "<div {$id} class=\" {$class}\" {$style}>"
	          . "<div class=\"{$dark}\" {$data}>"
	          . do_shortcode( $content )
	          . "</div>"
	          . '</div>';

	return $output;
}

add_shortcode( 'slick_slider', 'dima_shortcode_slick_slider' );

function dima_shortcode_slick_slide( $atts, $content = null ) {
	extract(
		shortcode_atts(
			array(
				'id'    => '',
				'class' => '',
				'thumb' => '',
				'style' => ''
			), $atts, 'slick_slide'
		)
	);

	$id    = ( $id != '' ) ? 'id="' . esc_attr( $id ) . '"' : '';
	$class = ( $class != '' ) ? 'slick-item ' . esc_attr( $class ) : 'slick-item';
	$style = ( $style != '' ) ? 'style="' . $style . '"' : '';
	$thumb = ( $thumb != '' ) ? 'data-thumb="' . $thumb . '"' : '';

	$output = "<div {$thumb} {$id} class=\"{$class}\" {$style}>" . do_shortcode(
			$content
		) . "</div>";

	return $output;
}

add_shortcode( 'slick_slide', 'dima_shortcode_slick_slide' );


/**
 * clients
 *   1 client
 */

function dima_shortcode_clients( $atts, $content = null ) { // 2
	extract(
		shortcode_atts(
			array(
				'id'      => '',
				'class'   => '',
				'columns' => '',//7,6,5,4,3
				'style'   => ''
			), $atts, 'clients'
		)
	);

	$id      = ( $id != '' ) ? 'id="' . esc_attr( $id ) . '"' : '';
	$class   = ( $class != '' ) ? 'clients-wrapper grid ' . esc_attr( $class )
		: 'clients-wrapper grid';
	$columns = ( $columns != '' ) ? '  client-clm-' . esc_attr( $columns )
		: ' client-clm-6';
	$style   = ( $style != '' ) ? 'style="' . $style . '"' : '';

	$output = "<div {$id} class=\"{$class}{$columns}\" {$style}>"
	          . "<ul>" . do_shortcode( $content ) . "</ul>"
	          . "</div>";

	return $output;
}

add_shortcode( 'clients', 'dima_shortcode_clients' );

function dima_shortcode_client( $atts, $content = null ) { // 2
	extract(
		shortcode_atts(
			array(
				'id'     => '',
				'class'  => '',
				'href'   => '',
				'target' => '',
				'src'    => '',
				'alt'    => '',
				'style'  => ''
			), $atts, 'client'
		)
	);

	$id     = ( $id != '' ) ? 'id="' . esc_attr( $id ) . '"' : '';
	$class  = ( $class != '' ) ? 'slide-item ' . esc_attr( $class ) : 'slide-item';
	$style  = ( $style != '' ) ? 'style="' . $style . '"' : '';
	$href   = ( $href != '' ) ? $href : '';
	$alt    = ( $alt != '' ) ? esc_attr( $alt ) : '';
	$src    = ( $src != '' ) ? $src : '';
	$target = ( $target == 'blank' ) ? 'target="_blank" rel="noopener"' : '';

	if ( is_numeric( $src ) ) {
		$src_info = wp_get_attachment_image_src( $src, 'full' );
		$src      = $src_info[0];
	}

	if ( empty( $href ) ) {
		$output = "<li {$id} class=\"{$class}\" {$style}><img src=\"{$src}\" alt=\"{$alt}\"></li>";
	} else {
		$output = "<li {$id} class=\"{$class}\" {$style}><a href=\"$href\" {$target}><img src=\"{$src}\" alt=\"{$alt}\"></a></li>";
	}

	return $output;
}

add_shortcode( 'client', 'dima_shortcode_client' );

function dima_shortcode_audio_player( $atts ) { // 1
	extract(
		shortcode_atts(
			array(
				'id'       => '',
				'class'    => '',
				'style'    => '',
				'mp3'      => '',
				'oga'      => '',
				'preload'  => '',
				'autoplay' => '',

				'producers' => '',
				'author'    => '',

				'loop' => ''
			), $atts, 'dima_audio_player'
		)
	);

	$id       = ( $id != '' ) ? 'id="' . esc_attr( $id ) . '"' : '';
	$class    = ( $class != '' ) ? 'dima-audio player ' . esc_attr( $class )
		: 'dima-audio player';
	$style    = ( $style != '' ) ? 'style="' . $style . '"' : '';
	$mp3      = ( $mp3 != '' ) ? '<source src="' . $mp3 . '" type="audio/mpeg">'
		: '';
	$oga      = ( $oga != '' ) ? '<source src="' . $oga . '" type="audio/ogg">'
		: '';
	$preload  = ( $preload != '' ) ? ' preload="' . $preload . '"'
		: ' preload="none"';
	$autoplay = ( $autoplay == 'true' ) ? ' autoplay' : '';
	$loop     = ( $loop == 'true' ) ? ' loop' : '';

	wp_enqueue_script( 'wp-mediaelement' );
	wp_enqueue_style( 'wp-mediaelement' );

	$data = dima_creat_data_attributes( 'audio-video' );

	$comp     = '';
	$producer = ( $producers != '' ) ? $producers : '';
	$author   = ( $author != '' ) ? $author : '';
	$out      = ( $author != '' ) ? "<span><h6>" . esc_attr( $author ) . "</h6></span>" : '';
	if ( $producer != '' && $author != '' ) {
		$out .= "<span class=\"sep\">|</span>";
	}
	$out .= ( $producer != '' ) ? "<span><h6>" . esc_attr( $producer ) . "</h6></span>" : '';
	if ( $out != '' ) {
		$comp = "<div class='clearfix dima-composition'>
        			 $out
    			 </div>";

	}

	$output
		= "<div {$id} class=\"{$class}{$autoplay}{$loop}\" {$data} {$style}>"
		  . "<audio class=\"audio-video\" {$preload} {$autoplay}{$loop}>"
		  . $mp3
		  . $oga
		  . '</audio>'
		  . '</div>'
		  . $comp;

	return $output;
}

add_shortcode( 'dima_audio_player', 'dima_shortcode_audio_player' );


function dima_shortcode_embed_audio( $atts, $content = null ) {
	extract(
		shortcode_atts(
			array(
				'id'    => '',
				'class' => '',
				'style' => ''
			), $atts, 'dima_embed_audio'
		)
	);

	$id    = ( $id != '' ) ? 'id="' . esc_attr( $id ) . '"' : '';
	$class = ( $class != '' ) ? 'dima-audio embed ' . esc_attr( $class )
		: 'x-audio embed';
	$style = ( $style != '' ) ? 'style="' . $style . '"' : '';

	$output = "<div {$id} class=\"{$class}\" {$style}>{$content}</div>";

	return $output;
}

add_shortcode( 'dima_embed_audio', 'dima_shortcode_embed_audio' );


/**
 * @param      $atts
 * @param null $content
 *
 * @return string
 */
function dima_shortcode_pricing_table_column( $atts, $content = null ) {
	extract(
		shortcode_atts(
			array(
				'id'                     => '',
				'class'                  => '',
				'style'                  => '',
				'featured'               => '',
				'shadow'                 => '',
				'interval'               => '',
				'title'                  => '',
				'href_callout'           => '',
				'target'                 => '',
				'currency'               => '',
				'icon'                   => '',
				'icon_svg'               => '',
				'icon_fontawesome'       => '',
				'price'                  => '',
				'dima_vc_add_shadow'     => '',
				'dima_vc_border_color'   => '',
				'dima_vc_add_border_btm' => '',
				'type'                   => '',//two,three,offers
			), $atts, 'pricing_table_column'
		)
	);

	$_class[]             = "dima-pricing-col";
	$id                   = ( $id != '' ) ? 'id="' . esc_attr( $id ) . '"' : '';
	$class                = ( $class != '' ) ? 'dima-pricing-table ' . esc_attr(
			$class
		) : 'dima-pricing-table';
	$style                = ( $style != '' ) ? 'style="' . $style . '"' : '';
	$interval             = ( $interval != '' ) ? '' . $interval : '';
	$type                 = ( $type != '' ) ? $type : '';
	$title                = ( $title != '' ) ? $title : '';
	$currency             = ( $currency != '' ) ? $currency : '';
	$price                = ( $price != '' ) ? $price : '';
	$href_callout         = ( $href_callout == '' ) ? '' : $href_callout;
	$target               = ( $target == 'blank' ) ? 'target="_blank" rel="noopener"' : '';
	$dima_vc_add_shadow   = ( $dima_vc_add_shadow == 'true' ) ? ' box-with-shadow'
		: '';
	$dima_vc_add_shadow   .= ( $dima_vc_add_border_btm == 'true' )
		? " add_border_btm" : '';
	$dima_vc_border_color = ( $dima_vc_border_color != '' )
		? $dima_vc_border_color : '';
	$_icon                = ( $icon != '' ) ? $icon : '';
	$icon_svg             = ( $icon_svg != '' ) ? dima_get_svg_icon( $icon_svg )
		: '';
	$icon_fontawesome     = ( $icon_fontawesome != '' ) ? $icon_fontawesome : '';

	$table_class = $featured_icon = $table_style = $_price_under = "";
	if ( ! empty( $dima_vc_border_color ) ) {
		$dima_vc_border_color = "border-top-color:$dima_vc_border_color;";
		$table_style          = "style=\"$dima_vc_border_color\"";
	}

	$_icon = ( $_icon == '' ) ? $icon_fontawesome : $_icon;

	if ( ! empty( $icon_svg ) ) {
		$_icon = $icon_svg;
	} else if ( ! empty( $_icon ) ) {
		$_icon = '<i class="' . $_icon . '"></i>';
	}

	switch ( $type ) {
		case 'callout':
			$_class[] = "pricing-style-callout";
			break;
		case 'offers':
			$_class[] = "dima-offers";
			break;
		default:
			$_class[] = "";
			break;
	}
	switch ( $featured ) {
		case 'true':
			$class         .= " featured";
			$featuredicon
			               = "<svg xmlns=\"http://www.w3.org/2000/svg\" fill=\"#000000\" height=\"24\" viewBox=\"0 0 24 24\" width=\"24\">
    							<path d=\"M0 0h24v24H0z\" fill=\"none\"/>
   								<path d=\"M12 21.35l-1.45-1.32C5.4 15.36 2 12.28 2 8.5 2 5.42 4.42 3 7.5 3c1.74 0 3.41.81 4.5 2.09C13.09 3.81 14.76 3 16.5 3 19.58 3 22 5.42 22 8.5c0 3.78-3.4 6.86-8.55 11.54L12 21.35z\"/>
							  </svg>";
			$featured_icon = "<span class='featured_icon'>$featuredicon</span>";
			break;
		default:
			$_class[] = "";
			break;
	}
	if ( ! empty( $_class ) ) {
		$table_class = dima_short_remove_white_space( join( ' ', $_class ) );
	}
	$_price
		= '<h2 class="dima-pricing-row"><span class="pricing"><span class="currency">'
		  . $currency . '</span>' . $price . '</span><span class="interval">'
		  . $interval . '</span></h2>';

	$_price = ( $price != '' ) ? $_price : '';

	if ( $_icon != '' ) {
		$class        .= " table_icon";
		$_price_under = $_price;
		$_price       = $_icon;
	}

	if ( $type != "dima-offers" ) {
		$output = "<div {$id} class=\"{$table_class}\">"
		          . '<div class="dima-pricing-col-info' . $dima_vc_add_shadow . '" '
		          . $table_style . '>'
		          . '<div class="dima-header-col-info">'
		          . '<h6 class="dima-table-title">' . $title . '</h6>'
		          . $_price
		          . '</div>'
		          . $_price_under
		          . '<div class="dima-table-content">'
		          . do_shortcode( $content )
		          . '</div>'
		          . '</div>'
		          . '</div>';
	} else {
		$output = "<div {$id} class=\"{$table_class}\">"
		          . '<div class="dima-pricing-col-info">'
		          . do_shortcode( $content )
		          . '</div>'
		          . '</div>';
	}

	if ( ! empty( $href_callout ) && $type == ' pricing-style-callout' ) {
		$output = "<a href=\"{$href_callout}\" {$target}>" . $output . "</a>";
	}

	$output
		= "<div {$id} class=\"{$class}\" {$style}> $featured_icon$output </div>";

	return $output;
}

add_shortcode( 'pricing_table_column', 'dima_shortcode_pricing_table_column' );

function dima_shortcode_iconbox_header( $atts ) {
	extract(
		shortcode_atts(
			array(
				'id'                 => '',
				'class'              => '',
				'style'              => '',
				'icon'               => '',
				'icon_svg'           => '',
				'icon_fontawesome'   => '',
				'image'              => '',
				'hover'              => '',
				'image_size'         => '',
				'alt'                => '',
				'size'               => '',
				'icon_bg'            => '',
				'icon_color'         => '',
				'shadow_color'       => '',
				'dima_vc_position'   => '',
				'dima_vc_gradient'   => '',
				'type'               => '',
				'icon_svg_size'      => '',
				'icon_svg_animation' => '',

				'animation'      => '',
				'delay'          => '',
				'delay_offset'   => '',
				'delay_duration' => '',
			), $atts, 'iconbox_header'
		)
	);


	$id                 = ( $id != '' ) ? 'id="' . esc_attr( $id ) . '"' : '';
	$_class[]           = ( $class != '' ) ? 'icon-box-header' . esc_attr( $class )
	                                         . '"' : 'icon-box-header';
	$style              = ( $style != '' ) ? 'style="' . $style . '"' : '';
	$icon               = ( $icon != '' ) ? $icon : '';
	$icon_svg           = ( $icon_svg != '' ) ? dima_get_svg_icon( $icon_svg ) : '';
	$alt                = ( $alt != '' ) ? esc_attr( $alt ) : '';
	$icon_fontawesome   = ( $icon_fontawesome != '' ) ? $icon_fontawesome : '';
	$type               = ( $type != '' ) ? ' box-' . $type : '';
	$hover              = ( $hover == 'none' ) ? '' : ' ' . $hover;
	$size               = ( $size != '' ) ? ' icon-box-' . $size
		: ' icon-box-medium';
	$image_size         = ( $image_size != '' ) ? ' ' . $image_size : ' medium';
	$icon_bg            = ( $icon_bg != '' ) ? $icon_bg : '';
	$_class[]           = ( $dima_vc_position != '' ) ? $dima_vc_position : '';
	$dima_vc_gradient   = ( $dima_vc_gradient != '' ) ? ' ' . $dima_vc_gradient
		: '';
	$icon_color         = ( $icon_color != '' ) ? $icon_color : '';
	$shadow_color       = ( $shadow_color != '' ) ? $shadow_color : '';
	$image              = ( $image != '' ) ? $image : '';
	$icon_svg_size      = ( $icon_svg_size != '' ) ? $icon_svg_size : '';
	$icon_svg_animation = ( $icon_svg_animation == 'on' ) ? 'svg_animated' : '';

	if ( $icon_svg_size != '' ) {
		$icon_svg_size = 'data-svg-size="' . $icon_svg_size . '"';
	}

	$animation_data = '';
	$animation_data .= ( $animation != '' ) ? 'data-animate=' . $animation . '' : '';
	$animation_data .= ( $delay != '' ) ? ' data-delay=' . $delay . '' : '';
	$animation_data .= ( $delay_offset != '' ) ? ' data-offset=' . $delay_offset . '' : '';
	$animation_data .= ( $delay_duration != '' ) ? ' data-duration=' . $delay_duration . '' : '';

	$style_bg    = '';
	$style_color = '';
	$icon_style  = '';
	if ( empty( $type ) ) {
		$_class[] = "box-none";
	}

	if ( ! empty( $dima_vc_gradient ) ) {
		$_class[] = "dima_grd";
	}

	if ( ! empty( $icon_bg ) ) {
		$style_bg = "background-color:$icon_bg;";
	}
	if ( ! empty( $shadow_color ) ) {
		$style_bg .= "box-shadow: 0 0 0 2px $shadow_color;";
	}

	if ( ! empty( $icon_color ) ) {
		$style_color = "color:$icon_color;";
	}

	if ( ! empty( $style_bg ) || ! empty( $style_color ) ) {
		$icon_style = "style=\"$style_bg$style_color\"";
	}
	if ( $image != '' ) {
		if ( is_numeric( $image ) ) {
			$src_info   = wp_get_attachment_image_src( $image, 'full' );
			$image      = $src_info[0];
			$img_width  = 'width="' . $src_info[1] . '"';
			$img_height = 'height="' . $src_info[2] . '"';
		} else {
			$poster_info = dima_helper::dima_get_attachment_info_by_url( $image );
			$img_width   = 'width="' . $poster_info[1] . '"';
			$img_height  = 'height="' . $poster_info[2] . '"';
		}
	}

	$icon = ( $icon == '' ) ? $icon_fontawesome : $icon;

	if ( ! empty( $image ) ) {
		$icon = '<div class="image-cropper thumb ' . $image_size . ' ' . $type
		        . '' . $dima_vc_gradient . '' . $hover . ' " ' . $icon_style . '>'
		        . '<img class="background-image" src="' . $image . '" ' . $img_width
		        . ' ' . $img_height . ' alt="' . $alt . '">'
		        . '</div>';
	} else if ( ! empty( $icon_svg ) ) {
		$icon = '<span ' . $icon_style . ' class="svg_icon ' . $icon_svg_animation . '' . $icon . '' . $type . $size
		        . ' ' . $hover . '' . $dima_vc_gradient . '" ' . $icon_svg_size . '>'
		        . $icon_svg
		        . '</span>';
	} else {
		$i    = '<i class="' . $icon . '"></i>';
		$icon = '<span ' . $icon_style . ' class="' . $type . ' ' . $hover
		        . $size . '' . $dima_vc_gradient . '">'
		        . $i
		        . '</span>';
	}

	if ( ! empty( $_class ) ) {
		$class = join( ' ', $_class );
	}
	$output = '<header ' . $id . ' class="' . $class . '" ' . $animation_data . ' ' . $style . '>'
	          . $icon
	          . '</header>';

	return $output;

}

add_shortcode( 'iconbox_header', 'dima_shortcode_iconbox_header' );

function dima_shortcode_iconbox_content( $atts, $content = null ) {
	extract(
		shortcode_atts(
			array(
				'id'          => '',
				'class'       => '',
				'style'       => '',
				'title'       => '',
				'title_color' => '',

				'animation'      => '',
				'delay'          => '',
				'delay_offset'   => '',
				'delay_duration' => '',
			), $atts, 'iconbox_content'
		)
	);

	$id             = ( $id != '' ) ? 'id="' . esc_attr( $id ) . '"' : '';
	$class          = ( $class != '' ) ? esc_attr( $class ) . ' ' : '';
	$style          = ( $style != '' ) ? 'style="' . $style . '"' : '';
	$title          = ( $title != '' ) ? $title : '';
	$title_color    = ( $title_color != '' ) ? 'style="color:' . $title_color . ';"'
		: '';
	$animation_data = '';
	$animation_data .= ( $animation != '' ) ? 'data-animate=' . $animation . '' : '';
	$animation_data .= ( $delay != '' ) ? ' data-delay=' . $delay . '' : '';
	$animation_data .= ( $delay_offset != '' ) ? ' data-offset=' . $delay_offset . '' : '';
	$animation_data .= ( $delay_duration != '' ) ? ' data-duration=' . $delay_duration . '' : '';

	$output = '<div ' . $id . ' class="' . $class . 'features-content" ' . $animation_data . ' '
	          . $style . '>'
	          . '<h5 class="features-title" ' . $title_color . '>'
	          . $title
	          . '</h5><span class="dima-divider line-center line-hr small-line"></span>';
	$output .= do_shortcode( shortcode_unautop( $content ) );
	$output .= '</div>';

	return $output;
}

add_shortcode( 'iconbox_content', 'dima_shortcode_iconbox_content' );

/**
 * TimeLine
 *   Process
 *   Date TimeLine
 */

function dima_shortcode_process( $atts, $content = null ) {
	extract(
		shortcode_atts(
			array(
				'id'      => '',
				'class'   => '',
				'style'   => '',
				'float'   => '',
				'vh'      => '',
				'columns' => '',
			), $atts, 'process'
		)
	);

	$id    = ( $id != '' ) ? 'id="' . esc_attr( $id ) . '"' : '';
	$class = ( $class != '' ) ? 'dima-timeline-list ' . esc_attr( $class )
		: 'dima-timeline-list';
	$style = ( $style != '' ) ? 'style="' . $style . '"' : '';
	$float = ( $float == 'end' ) ? ' timeline-end' : '';
	$class .= ( $columns != '' ) ? ' ' . $columns : '';
	$class .= ( $vh != '' ) ? ' ' . $vh : '';

	$output = "<div {$id} class=\"{$class}{$float}\" {$style}>"
	          . do_shortcode( $content )
	          . "</div>";

	return $output;
}

add_shortcode( 'process', 'dima_shortcode_process' );

function dima_shortcode_date_timeline( $atts, $content = null ) {
	extract(
		shortcode_atts(
			array(
				'id'    => '',
				'class' => '',
				'style' => '',
			), $atts, 'date_timeline'
		)
	);

	$id    = ( $id != '' ) ? 'id="' . esc_attr( $id ) . '"' : '';
	$class = ( $class != '' ) ? 'dima-timeline clearfix dima-timeline-date '
	                            . esc_attr( $class ) : 'dima-timeline clearfix dima-timeline-date';
	$style = ( $style != '' ) ? 'style="' . $style . '"' : '';

	$output = "<div {$id} class=\"{$class}\" {$style}>"
	          . "<div class=\"h-line\"></div>"
	          . do_shortcode( $content )
	          . "</div>";

	return $output;
}

add_shortcode( 'date_timeline', 'dima_shortcode_date_timeline' );

function dima_shortcode_date_timeline_item( $atts, $content = null ) {
	extract(
		shortcode_atts(
			array(
				'id'    => '',
				'class' => '',
				'style' => '',
				'float' => '',
				'boxed' => '',
				'date'  => '',
			), $atts, 'date_timeline_item'
		)
	);

	$id    = ( $id != '' ) ? 'id="' . esc_attr( $id ) . '"' : '';
	$class = ( $class != '' ) ? 'dima-timeline clearfix dima-timeline-date '
	                            . esc_attr( $class ) : 'dima-timeline clearfix dima-timeline-date';
	$style = ( $style != '' ) ? 'style="' . $style . '"' : '';
	$float = ( $float != '' ) ? 'timeline_element_' . $float
		: 'timeline_element_start';
	$date  = ( $date != '' ) ? $date : '';
	$boxed = ( $boxed == 'true' ) ? ' dima-no-box ' : '';

	$output = '<div ' . $id . ' class="' . $class . ' ' . $float . ' " '
	          . $style . '>'
	          . '<article>'
	          . '<div class="timeline_element-content">'
	          . '<div class="post box' . $boxed . '">'
	          . '<p> ' . do_shortcode( $content ) . ' </p>'
	          . '</div>'
	          . '<div class="date">'
	          . '<h5>' . $date . '</h5>'
	          . '</div>'
	          . '</div>'
	          . '</article>'
	          . '</div>';

	return $output;
}

add_shortcode( 'date_timeline_item', 'dima_shortcode_date_timeline_item' );

/**
 * Menu
 */
/**
 * @param      $atts
 * @param null $content
 *
 * @return string
 */
function dima_shortcode_product_menu( $atts, $content = null ) {
	extract(
		shortcode_atts(
			array(
				'id'       => '',
				'class'    => '',
				'style'    => '',
				'bg_image' => '',
				'bg_color' => '',
				//'boxed'    => 'true',
				'title'    => 'Add Title Here',
			), $atts, 'product_menu'
		)
	);

	$id       = ( $id != '' ) ? 'id="' . esc_attr( $id ) . '"' : '';
	$class    = ( $class != '' ) ? 'last-menu ' . esc_attr( $class ) : 'last-menu';
	$bg_color = ( $bg_color != '' ) ? 'background-color:' . $bg_color . ';' : '';
	$style    = $style . $bg_color;
	$style    = ( $style != '' ) ? 'style="' . $style . '"' : '';
	$title    = ( $title != '' ) ? $title : '';
	//$boxed    = ( $boxed == 'true' ) ? ' box' : ' no-box';

	$bg_image = ( $bg_image != '' ) ? $bg_image : '';

	if ( is_numeric( $bg_image ) ) {
		$bg_image_info = wp_get_attachment_image_src( $bg_image, 'full' );
		$bg_image      = "data-element-bg=\"{$bg_image_info[0]}\"";
	}

	if ( $title != '' ) {
		$title = "<h3 class=\"box-titel\">{$title}</h3>";
	}

	$output = "<div {$id} class=\"{$class}\" {$style} {$bg_image}>"
	          . $title
	          . "<ul>"
	          . do_shortcode( $content )
	          . "</ul>"
	          . "</div>";

	return $output;
}

add_shortcode( 'product_menu', 'dima_shortcode_product_menu' );

function dima_shortcode_product_menu_item( $atts, $content = null ) {
	extract(
		shortcode_atts(
			array(
				'id'           => '',
				'class'        => '',
				'style'        => '',
				'price'        => '',
				'title'        => 'Title Here',
				'notification' => '',
				'noti_color'   => '',
				'href'         => '',
				'target'       => '',
			), $atts, 'product_menu_item'
		)
	);

	$id           = ( $id != '' ) ? 'id="' . esc_attr( $id ) . '"' : '';
	$class        = ( $class != '' ) ? 'clearfix ' . esc_attr( $class )
		: 'clearfix';
	$style        = ( $style != '' ) ? 'style="' . $style . '"' : '';
	$target       = ( $target == 'blank' ) ? 'target="_blank" rel="noopener"' : '';
	$title        = ( $title == '' ) ? '' : $title;
	$noti_color   = ( $noti_color == '' ) ? 'red_color' : $noti_color . "_color";
	$href         = ( $href == '' ) ? '#' : $href;
	$price        = ( $price == '' ) ? ''
		: '<span class="menu-price" >' . $price . '</span >';
	$notification = ( $notification == '' )
		? ''
		: '<span class="menu-notification ' . $noti_color . '">' . $notification
		  . '</span >';

	if ( $href != '' ) {
		$title
			= "<a class=\"menu-title\" href = \"{$href}\" {$target}>{$title}</a >";
	} else {
		$title = '<span class="menu-title" >' . $title . '</span>';
	}


	$header = "<h5>"
	          . $title
	          . $notification
	          . "<span class=\"menu_dots\"></span>"
	          . $price
	          . "</h5>";
	$output = ' <li ' . $id . ' class="' . $class . '"  ' . $style . ' > '
	          . $header
	          . do_shortcode( $content )
	          . '</li > ';

	return $output;
}

add_shortcode( 'product_menu_item', 'dima_shortcode_product_menu_item' );


/**
 * Post Author
 */

function dima_shortcode_author( $atts ) {
	extract(
		shortcode_atts(
			array(
				'id'        => '',
				'class'     => '',
				'style'     => '',
				'title'     => '',
				'author_id' => ''
			), $atts, 'author'
		)
	);

	$id        = ( $id != '' ) ? 'id = "' . esc_attr( $id ) . '"' : '';
	$class     = ( $class != '' ) ? 'dima-author-box clearfix ' . esc_attr( $class )
		: 'dima-author-box clearfix';
	$style     = ( $style != '' ) ? 'style = "' . $style . '"' : '';
	$title     = ( $title != '' ) ? $title : __( 'Author: ', 'noor-assistant' );
	$author_id = ( $author_id != '' ) ? $author_id : get_the_author_meta( 'ID' );

	$description  = get_the_author_meta( 'description', $author_id );
	$display_name = get_the_author_meta( 'display_name', $author_id );
	$facebook     = get_the_author_meta( 'facebook', $author_id );
	$instagram    = get_the_author_meta( 'instagram', $author_id );
	$linkedin     = get_the_author_meta( 'linkedin', $author_id );
	$twitter      = get_the_author_meta( 'twitter', $author_id );
	$googleplus   = get_the_author_meta( 'googleplus', $author_id );
	$flickr       = get_the_author_meta( 'flickr', $author_id );
	$youtube      = get_the_author_meta( 'youtube', $author_id );
	$pinterest    = get_the_author_meta( 'pinterest', $author_id );
	$behance      = get_the_author_meta( 'behance', $author_id );
	$dribbble     = get_the_author_meta( 'dribbble', $author_id );

	$facebook_output   = ( $facebook ) ? "<li><a href=\"{$facebook}\"  title=\"" . esc_attr__( 'Visit the Facebook Profile for', 'noor-assistant' ) . " {$display_name}\" target=\"_blank\"><i class=\"fa fa-facebook\"></i></a></li>" : '';
	$twitter_output    = ( $twitter ) ? "<li><a href=\"{$twitter}\"  title=\"" . esc_attr__( 'Visit the Twitter Profile for', 'noor-assistant' ) . " {$display_name}\" target=\"_blank\"><i class=\"fa fa-twitter\"></i></a></li>" : '';
	$googleplus_output = ( $googleplus ) ? "<li><a href=\"{$googleplus}\"  title=\"" . esc_attr__( 'Visit the Google+ Profile for', 'noor-assistant' ) . " {$display_name}\" target=\"_blank\"><i class=\"fa fa-google-plus\"></i></a></li>" : '';
	$instagram_output  = ( $instagram ) ? "<li><a href=\"{$instagram}\"  title=\"" . esc_attr__( 'Visit the instagram Profile for', 'noor-assistant' ) . " {$display_name}\" target=\"_blank\"><i class=\"fa fa-instagram\"></i></a></li>" : '';
	$linkedin_output   = ( $linkedin ) ? "<li><a href=\"{$linkedin}\"  title=\"" . esc_attr__( 'Visit the linkedin Profile for', 'noor-assistant' ) . " {$display_name}\" target=\"_blank\"><i class=\"fa fa-linkedin\"></i></a></li>" : '';
	$flickr_output     = ( $flickr ) ? "<li><a href=\"{$flickr}\"  title=\"" . esc_attr__( 'Visit the flickr Profile for', 'noor-assistant' ) . " {$display_name}\" target=\"_blank\"><i class=\"fa fa-flickr\"></i></a></li>" : '';
	$youtube_output    = ( $youtube ) ? "<li><a href=\"{$youtube}\"  title=\"" . esc_attr__( 'Visit the youtube Profile for', 'noor-assistant' ) . " {$display_name}\" target=\"_blank\"><i class=\"fa fa-youtube\"></i></a></li>" : '';
	$pinterest_output  = ( $pinterest ) ? "<li><a href=\"{$pinterest}\"  title=\"" . esc_attr__( 'Visit the pinterest Profile for', 'noor-assistant' ) . " {$display_name}\" target=\"_blank\"><i class=\"fa fa-pinterest\"></i></a></li>" : '';
	$behance_output    = ( $behance ) ? "<li><a href=\"{$behance}\"  title=\"" . esc_attr__( 'Visit the behance Profile for', 'noor-assistant' ) . " {$display_name}\" target=\"_blank\"><i class=\"fa fa-behance\"></i></a></li>" : '';
	$dribbble_output   = ( $dribbble ) ? "<li><a href=\"{$dribbble}\"  title=\"" . esc_attr__( 'Visit the dribbble Profile for', 'noor-assistant' ) . " {$display_name}\" target=\"_blank\"><i class=\"fa fa-dribbble\"></i></a></li>" : '';

	$output = "<div {$id} class=\"{$class}\" {$style}>"
	          . "<div class=\"dima-about-image circle\">"
	          . get_avatar( $author_id, 150 )
	          . "</div>"
	          . "<div class=\"dima-author-info\">"
	          . "<h5 class=\"dima-author-name\">{$title}<a href=\""
	          . get_author_posts_url( get_the_author_meta( 'ID' ) )
	          . "\">{$display_name}</a></h5>"
	          . "<p>{$description}</p>"
	          . "<div class=\"social-media text-start fill-icon dima_add_hover social-small circle-social\">"
	          . "<ul class=\"inline clearfix\">"
	          . $facebook_output
	          . $twitter_output
	          . $googleplus_output
	          . $instagram_output
	          . $linkedin_output
	          . $flickr_output
	          . $youtube_output
	          . $pinterest_output
	          . $behance_output
	          . $dribbble_output
	          . "</ul>"
	          . "</div>"
	          . "</div >"
	          . "</section >";

	return $output;
}

add_shortcode( 'author', 'dima_shortcode_author' );


/**
 * Share
 */

function dima_shortcode_share_post( $atts ) {
	extract(
		shortcode_atts(
			array(
				'id'          => '',
				'class'       => '',
				'style'       => '',
				'float'       => 'center',
				'size'        => '',
				'circle'      => '',
				'facebook'    => '',
				'twitter'     => '',
				'google_plus' => '',
				'linkedin'    => '',
				'pinterest'   => '',
				'reddit'      => '',
				'vk'          => '',
				'email'       => ''
			), $atts, 'share'
		)
	);

	$share_url        = urlencode( get_permalink() );
	$share_title      = urlencode( get_the_title() );
	$share_source     = urlencode( get_bloginfo( 'name' ) );
	$share_content    = urlencode( get_the_excerpt() );
	$share_image_info = wp_get_attachment_image_src(
		get_post_thumbnail_id(), 'full'
	);
	$share_image      = ( function_exists(
		'dima_helper::dima_get_featured_image_url'
	) )
		? urlencode( dima_helper::dima_get_featured_image_url() )
		: urlencode(
			$share_image_info[0]
		);

	$tooltip_attr = '';

	$id     = ( $id != '' ) ? 'id = "' . esc_attr( $id ) . '"' : '';
	$class  = ( $class != '' ) ? 'dima-share-icon ' . esc_attr( $class )
		: 'dima-share-icon';
	$size   = ( $size != '' ) ? ' social-' . esc_attr( $size )
		: ' social-small';
	$circle = ( $circle == true ) ? ' circle-social' : '';
	$style  = ( $style != '' ) ? 'style = "' . $style . '"' : '';

	$facebook    = ( $facebook == 'true' )
		? "<li><a href=\"#share\" {$tooltip_attr} onclick=\"window.open('http://www.facebook.com/sharer.php?u={$share_url}&amp;t={$share_title}', 'popupFacebook', 'width=650, height=270, resizable=0, toolbar=0, menubar=0, status=0, location=0, scrollbars=0'); return false;\"><i class=\"fa fa-facebook\"></i></a></li>"
		: '';
	$twitter     = ( $twitter == 'true' )
		? "<li><a href=\"#share\" {$tooltip_attr} onclick=\"window.open('https://twitter.com/intent/tweet?text={$share_title}&amp;url={$share_url}', 'popupTwitter', 'width=500, height=370, resizable=0, toolbar=0, menubar=0, status=0, location=0, scrollbars=0'); return false;\"><i class=\"fa fa-twitter\"></i></a></li>"
		: '';
	$google_plus = ( $google_plus == 'true' )
		? "<li><a href=\"#share\" {$tooltip_attr} onclick=\"window.open('https://plus.google.com/share?url={$share_url}', 'popupGooglePlus', 'width=650, height=226, resizable=0, toolbar=0, menubar=0, status=0, location=0, scrollbars=0'); return false;\"><i class=\"fa fa-google-plus\"></i></a></li>"
		: '';
	$linkedin    = ( $linkedin == 'true' )
		? "<li><a href=\"#share\" {$tooltip_attr} onclick=\"window.open('http://www.linkedin.com/shareArticle?mini=true&amp;url={$share_url}&amp;title={$share_title}&amp;summary={$share_content}&amp;source={$share_source}', 'popupLinkedIn', 'width=610, height=480, resizable=0, toolbar=0, menubar=0, status=0, location=0, scrollbars=0'); return false;\"><i class=\"fa fa-linkedin\"></i></a></li>"
		: '';
	$pinterest   = ( $pinterest == 'true' )
		? "<li><a href=\"#share\" {$tooltip_attr}  onclick=\"window.open('http://pinterest.com/pin/create/button/?url={$share_url}&amp;media={$share_image}&amp;description={$share_title}', 'popupPinterest', 'width=750, height=265, resizable=0, toolbar=0, menubar=0, status=0, location=0, scrollbars=0'); return false;\"><i class=\"fa fa-pinterest\"></i></a></li>"
		: '';
	$reddit      = ( $reddit == 'true' )
		? "<li><a href=\"#share\" {$tooltip_attr} onclick=\"window.open('http://www.reddit.com/submit?url={$share_url}', 'popupReddit', 'width=875, height=450, resizable=0, toolbar=0, menubar=0, status=0, location=0, scrollbars=0'); return false;\"><i class=\"fa fa-reddit\"></i></a></li>"
		: '';
	$vk          = ( $vk != 'true' ) ? "<li><a href=\"#share\" {$tooltip_attr} title=\"" . __( 'Share on Vk', 'noor-assistant' ) . "\" onclick=\"window.open('http://vk.com/share.php?url={$share_url}&amp;title={$share_title}&amp;description={$share_content}', 'popupVk', 'width=875, height=450, resizable=0, toolbar=0, menubar=0, status=0, location=0, scrollbars=0'); return false;\"><i class=\"fa fa-vk\"></i></a></li>" : '';

	$email = ( $email == 'true' ) ? "<li><a href=\"mailto:?subject="
	                                . get_the_title() . "&amp;body=" . __(
		                                'Hey, thought you might enjoy this! Check it out when you have a chance:',
		                                'noor-assistant'
	                                ) . " " . get_permalink()
	                                . "\" {$tooltip_attr} class=\"email\" title=\"" . __(
		                                'Share via Email', 'noor-assistant'
	                                ) . "\"><span><i class=\"fa fa-envelope\"></i></span></a>" : '';


	switch ( $float ) {
		case 'end' :
			$float = ' text-end';
			break;
		case 'start' :
			$float = ' text-start';
			break;
		case 'center' :
			$float = ' text-center';
			break;
		default :
			$float = '';

	}

	//social-media text-start fill-icon dima_add_hover social-small circle-social
	$output = "<div {$id} class=\"{$class}\" {$style}>"
	          . "<div class=\"social-media fill-icon dima-social-post dima_add_hover{$size}{$circle}{$float}\">"
	          . "<ul class=\"inline clearfix\">"
	          . $facebook . $twitter . $google_plus . $linkedin . $vk . $pinterest . $reddit
	          . $email
	          . "</ul>"
	          . "</div>"
	          . "</div>";

	return $output;
}

add_shortcode( 'share', 'dima_shortcode_share_post' );

/**
 * Search
 */
function dima_shortcode_search( $atts ) {
	extract(
		shortcode_atts(
			array(
				'id'    => '',
				'class' => '',
				'style' => ''
			), $atts, 'search'
		)
	);

	$id    = ( $id != '' ) ? 'id="' . esc_attr( $id ) . '"' : '';
	$class = ( $class != '' ) ? 'search-query ' . esc_attr( $class )
		: 'search-query';
	$style = ( $style != '' ) ? 'style="' . $style . '"' : '';

	$output = "<div {$id} class=\"{$class}\" {$style}>" . get_search_form( false )
	          . '</div>';

	return $output;
}

add_shortcode( 'search', 'dima_shortcode_search' );

/**
 * Counter
 */

function dima_shortcode_counter( $atts ) {
	extract(
		shortcode_atts(
			array(
				'id'               => '',
				'class'            => '',
				'style'            => '',
				'icon'             => '',
				'icon_fontawesome' => '',
				'icon_svg'         => '',
				'num_color'        => '',
				'text_color'       => '',
				'bg_color'         => '',
				'icon_color'       => '',
				'num_start'        => '',
				'num_end'          => '',
				'num_speed'        => '',
				'num_before'       => '',
				'num_after'        => '',
				'border'           => '',
				'dima_box_shadow'  => 'false',
				'float'            => 'center',
				'text'             => '',

				'animation'      => '',
				'delay'          => '',
				'delay_offset'   => '',
				'delay_duration' => '',
			), $atts, 'counter'
		)
	);

	$id               = ( $id != '' ) ? 'id="' . esc_attr( $id ) . '"' : '';
	$class            = ( $class != '' ) ? 'countUp ' . esc_attr( $class )
		: 'countUp ';
	$style            = ( $style != '' ) ? 'style="' . $style . '"' : '';
	$num_before       = ( $num_before != '' ) ? $num_before : '';
	$num_after        = ( $num_after != '' ) ? $num_after : '';
	$class            .= ( $float != '' ) ? 'text-' . $float : 'text-center';
	$num_color        = ( $num_color != '' ) ? 'style="color: ' . $num_color
	                                           . ';"' : '';
	$bg_color         = ( $bg_color != '' ) ? 'style="background: ' . $bg_color
	                                          . ';"' : '';
	$num_start        = ( $num_start != '' ) ? $num_start : 0;
	$num_end          = ( $num_end != '' ) ? $num_end : 0;
	$num_speed        = ( $num_speed != '' ) ? $num_speed : 1500;
	$text_color       = ( $text_color != '' ) ? 'style="color: ' . $text_color
	                                            . ';"' : '';
	$icon_color       = ( $icon_color != '' ) ? 'style="color: ' . $icon_color
	                                            . ';"' : '';
	$class            .= ( $dima_box_shadow == 'true' ) ? ' dima-box-shadow' : '';
	$_icon            = ( $icon != '' ) ? $icon : '';
	$icon_svg         = ( $icon_svg != '' ) ? dima_get_svg_icon( $icon_svg ) : '';
	$icon_fontawesome = ( $icon_fontawesome != '' ) ? $icon_fontawesome : '';
	$_icon            = ( $_icon == '' ) ? $icon_fontawesome : $_icon;

	$animation_data = '';
	$animation_data .= ( $animation != '' ) ? 'data-animate=' . $animation . '' : '';
	$animation_data .= ( $delay != '' ) ? ' data-delay=' . $delay . '' : '';
	$animation_data .= ( $delay_offset != '' ) ? ' data-offset=' . $delay_offset . '' : '';
	$animation_data .= ( $delay_duration != '' ) ? ' data-duration=' . $delay_duration . '' : '';

	if ( ! empty( $icon_svg ) ) {
		$_icon = '<sapn ' . $icon_color . '  class="icon-count">' . $icon_svg
		         . '</sapn>';
	} else if ( ! empty( $_icon ) ) {
		if ( ! empty( $icon_fontawesome ) ) {
			$_icon = '<sapn ' . $icon_color . '  class="icon-count">' .
			         '<i ' . $icon_color . ' class="' . $_icon . '"></i>' .
			         '</sapn>';
		} else {
			$_icon = '<i ' . $icon_color . ' class="icon-count ' . $_icon
			         . '"></i>';
		}
	}

	$text       = ( $text != '' ) ? '<div class="text" ' . $text_color . '>'
	                                . $text . '</div>' : '';
	$icon_class = ( $_icon != '' ) ? " with-icon" : '';
	$border     = ( $border == true ) ? " add-border" : '';

	$output
		= "<div {$id} {$animation_data} class=\"{$class}{$icon_class}{$border}\" {$style} {$bg_color}>"
		  . $_icon
		  . "<div class=\"number\" {$num_color}>"
		  . "$num_before"
		  . "<span class=\"number-count\" data-from=\"{$num_start}\" data-to=\"{$num_end}\" data-speed=\"{$num_speed}\" data-refresh-interval=\"50\">{$num_start}</span>"
		  . "$num_after"
		  . "<span class=\"dima-divider line-{$float} line-hr small-line\"></span>"
		  . $text
		  . '</div>'
		  . '</div>';

	return $output;
}

add_shortcode( 'counter', 'dima_shortcode_counter' );


/**
 * LABELS
 *
 * @param      $atts
 * @param null $content
 *
 * @return string
 */
function dima_shortcode_labels( $atts, $content = null ) {
	extract(
		shortcode_atts(
			array(
				'id'       => '',
				'class'    => '',
				'style'    => '',
				'bg_color' => '',
				'type'     => '',//success,default,warning,info,error
				'text'     => 'Add Text Here',
			), $atts, 'product_menu'
		)
	);

	$id       = ( $id != '' ) ? 'id="' . esc_attr( $id ) . '"' : '';
	$class    = ( $class != '' ) ? 'label  ' . esc_attr( $class ) : 'label ';
	$bg_color = ( $bg_color != '' ) ? 'background-color:' . $bg_color . ';' : '';
	$style    = $style . $bg_color;
	$style    = ( $style != '' ) ? 'style="' . $style . '"' : '';
	$text     = ( $text != '' ) ? $text : '';
	$type     = ( $type != '' ) ? $type : '';

	if ( $type != '' )
		switch ( $type ) {
			case 'success':
				$type = 'dima-alert-success';
				break;
			case 'info':
				$type = 'dima-alert-info';
				break;
			case 'warning':
				$type = 'dima-alert-warning';
				break;
			case 'dima-error':
				$type = 'dima-alert-error';
				break;
			case 'dima':
				$type = 'dima-alert-error';
				break;
			default:
				$type = '';
				break;
		}

	$class  .= $type;
	$output = "<span {$id} class=\"{$class}\" {$style}>"
	          . $text
	          . "</span>";

	return $output;
}

add_shortcode( 'labels', 'dima_shortcode_labels' );

/**
 * Text
 *
 * @param null $content
 *
 * @return string
 */
function dima_shortcode_text( $atts, $content = null ) {

	extract(
		shortcode_atts(
			array(
				'animation'      => '',
				'delay'          => '',
				'delay_offset'   => '',
				'delay_duration' => '',
				'class'          => '',
				'css'            => '',
			), $atts, 'text'
		)
	);

	$animation_data = '';
	$animation_data .= ( $animation != '' ) ? 'data-animate=' . $animation . '' : '';
	$animation_data .= ( $delay != '' ) ? ' data-delay=' . $delay . '' : '';
	$animation_data .= ( $delay_offset != '' ) ? ' data-offset=' . $delay_offset . '' : '';
	$animation_data .= ( $delay_duration != '' ) ? ' data-duration=' . $delay_duration . '' : '';
	$output         = '';
	$css_class      = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, 'wpb_content_element ' . $class . vc_shortcode_custom_css_class( $css, ' ' ), $atts );

	//$output = do_shortcode( wpautop( $content ) );

	$output .= "\n\t" . '<div class="' . esc_attr( $css_class ) . '" ' . $animation_data . '>';
	$output .= "\n\t\t" . '<div class="wpb_wrapper">';
	$output .= "\n\t\t\t" . wpb_js_remove_wpautop( $content, true );
	$output .= "\n\t\t" . '</div> ';
	$output .= "\n\t" . '</div> ';

	return $output;

}

add_shortcode( 'text', 'dima_shortcode_text' );


function dima_shortcode_custom_heading( $atts, $content = null ) {
	extract(
		shortcode_atts(
			array(
				'id'                 => '',
				'class'              => '',
				'style'              => '',
				'float'              => '',
				'level'              => '',
				'icon'               => '',
				'icon_svg'           => '',
				'icon_fontawesome'   => '',
				'dima_heading_color' => '',

				'animation'      => '',
				'delay'          => '',
				'delay_offset'   => '',
				'delay_duration' => '',
			), $atts, 'custom_heading'
		)
	);
	$uniq_id = uniqid();
	$id      = ( $id != '' ) ? 'id="' . esc_attr( $id ) . '"' : '';
	$class   = ( $class != '' ) ? 'dima-custom-heading ' . esc_attr( $class ) : 'dima-custom-heading';

	if ( $dima_heading_color != '' ) {
		$dima_heading_color = 'color:' . $dima_heading_color . '';
		$class              = ( $class != '' ) ? 'dima-custom-heading heading-' . $uniq_id . ' ' . esc_attr( $class ) : 'dima-custom-heading heading-' . $uniq_id . '';
		$el                 = '.heading-' . $uniq_id . '';
		DIMA_Style::dima_addCSS( $el . '{'
		                         . $dima_heading_color
		                         . '}', $uniq_id );
	}

	$style            = ( $style != '' ) ? 'style="' . $style . '"' : '';
	$icon_svg         = ( $icon_svg != '' ) ? dima_get_svg_icon( $icon_svg ) : '';
	$icon             = ( $icon != '' ) ? '<i  class="' . $icon . '"></i>' : '';
	$icon_fontawesome = ( $icon_fontawesome != '' ) ? '<i class="' . $icon_fontawesome . '"></i> ' : '';
	$icon             = ( $icon == '' ) ? $icon_fontawesome : $icon;

	if ( ! empty( $icon_svg ) ) {
		$icon = $icon_svg;
	}

	$level = ( $level != '' ) ? $level : 'h1';
	$float = ( $float != '' ) ? $float : '';

	$delay          = ( $delay != '' ) ? ' data-delay=' . $delay . '' : '';
	$delay_offset   = ( $delay_offset != '' ) ? ' data-offset=' . $delay_offset . '' : '';
	$animation      = ( $animation != '' ) ? ' data-animate=' . $animation . '' : '';
	$delay_duration = ( $delay_duration != '' ) ? ' data-duration=' . $delay_duration . '' : '';


	switch ( $float ) {
		case 'end' :
			$float = ' text-end';
			break;
		case 'start' :
			$float = ' text-start';
			break;
		case 'center' :
			$float = ' text-center';
			break;
		default :
			$float = '';

	}

	$output = "<{$level} {$id} class=\"{$class}{$float}\" {$style} {$animation} {$delay} {$delay_offset} {$delay_duration}>{$icon}" . do_shortcode( $content ) . "</{$level}>";

	return $output;

}

add_shortcode( 'custom_heading', 'dima_shortcode_custom_heading' );


/**
 * Protected Content
 *
 *
 * @param      $atts
 * @param null $content
 *
 * @return string
 */
function dima_shortcode_protected( $atts, $content = null ) {
	extract(
		shortcode_atts(
			array(
				'id'    => '',
				'class' => '',
				'style' => ''
			), $atts, 'protect'
		)
	);

	GLOBAL $user_login;

	$id    = ( $id != '' ) ? 'id="' . esc_attr( $id ) . '"' : '';
	$class = ( $class != '' ) ? 'dima-protect ' . esc_attr( $class )
		: 'dima-protect';
	$style = ( $style != '' ) ? 'style="' . $style . '"' : '';

	if ( is_user_logged_in() ) {
		$output = do_shortcode( $content );
	} else {
		$output = "<div {$id} class=\"box {$class}\" {$style}>"
		          . '<form action="' . esc_url( site_url( 'wp-login.php' ) )
		          . '" method="post" class="mbn">'
		          . '<h3>' . esc_html__( 'Restricted Content Login', 'noor-assistant' )
		          . '</h3>'
		          . '<p class="form-row-wide">'
		          . '<label for="username">' . esc_html__(
			          'Username or email address', 'noor-assistant'
		          ) . ' <span class="required">*</span></label>'
		          . '<input class="input-text" name="username" id="username" value="'
		          . esc_attr( $user_login ) . '" type="text">'
		          . '</p>'

		          . '<p class="form-row-wide">'
		          . '  <label for="password">' . esc_html__(
			          'Password', 'noor-assistant'
		          ) . ' <span class="required">*</span></label>'
		          . '  <input class="input-text" name="password" id="password" type="password">'
		          . '</p>'
		          . '<p class="form-row no-bottom-margin">'
		          . '<input type="hidden" name="redirect_to" value="' . esc_url(
			          get_permalink()
		          ) . '">'
		          . '<input class="button" name="login" value="' . esc_html__(
			          'Login', 'noor-assistant'
		          ) . '" type="submit">'
		          . '</p>'
		          . '</form>'
		          . '</div>';
	}

	return $output;
}

add_shortcode( 'protect', 'dima_shortcode_protected' );

/*-----------------------------------------------------------------------------------*/
# [ads1] Shortcode
/*-----------------------------------------------------------------------------------*/
add_shortcode( 'ads1', 'dima_extensions_sc_ads1' );
function dima_extensions_sc_ads1( $atts, $content = null ) {
	return '
		<div class="stream-item e3lan-in-post_one">
		' . dima_helper::dima_get_option( 'dima_ad1_shortcode' ) . '
		</div>
	';
}

/*-----------------------------------------------------------------------------------*/
# [ads2] Shortcode
/*-----------------------------------------------------------------------------------*/
add_shortcode( 'ads2', 'dima_extensions_sc_ads2' );
function dima_extensions_sc_ads2( $atts, $content = null ) {
	return '
		<div class="stream-item e3lan-in-post_two">' .
	       dima_helper::dima_get_option( 'dima_ad2_shortcode' ) . '
		</div>
	';
}

/*-----------------------------------------------------------------------------------*/
# [ads3] Shortcode
/*-----------------------------------------------------------------------------------*/
add_shortcode( 'ads3', 'dima_extensions_sc_ads3' );
function dima_extensions_sc_ads3( $atts, $content = null ) {
	return '
		<div class="stream-item e3lan-in-post_three">' .
	       dima_helper::dima_get_option( 'dima_ad3_shortcode' ) . '
		</div>
	';
}

/*-----------------------------------------------------------------------------------*/
# [ads4] Shortcode
/*-----------------------------------------------------------------------------------*/
add_shortcode( 'ads4', 'dima_extensions_sc_ads4' );
function dima_extensions_sc_ads4( $atts, $content = null ) {
	return '
		<div class="stream-item e3lan-in-post_four">' .
	       dima_helper::dima_get_option( 'dima_ad4_shortcode' ) . '
		</div>
	';
}

/*-----------------------------------------------------------------------------------*/
# [ads5] Shortcode
/*-----------------------------------------------------------------------------------*/
add_shortcode( 'ads5', 'dima_extensions_sc_ads5' );
function dima_extensions_sc_ads5( $atts, $content = null ) {
	return '
		<div class="stream-item e3lan-in-post_five">' .
	       dima_helper::dima_get_option( 'dima_ad5_shortcode' ) . '
		</div>
	';
}

if ( ! function_exists( 'dima_add_contact_methods' ) ) :
	function dima_add_contact_methods( $user_contactmethods ) {

		unset( $user_contactmethods['yim'] );
		unset( $user_contactmethods['aim'] );
		unset( $user_contactmethods['jabber'] );

		$user_contactmethods['facebook']   = esc_html__( 'Facebook Profile', 'noor-assistant' );
		$user_contactmethods['twitter']    = esc_html__( 'Twitter Profile', 'noor-assistant' );
		$user_contactmethods['googleplus'] = esc_html__( 'Google+ Profile', 'noor-assistant' );
		$user_contactmethods['linkedin']   = esc_html__( 'Linkedin Profile', 'noor-assistant' );
		$user_contactmethods['instagram']  = esc_html__( 'Instagram Profile', 'noor-assistant' );

		$user_contactmethods['flickr']    = esc_html__( 'Flickr Profile', 'noor-assistant' );
		$user_contactmethods['youtube']   = esc_html__( 'YouTube Profile', 'noor-assistant' );
		$user_contactmethods['pinterest'] = esc_html__( 'Pinterest Profile', 'noor-assistant' );
		$user_contactmethods['behance']   = esc_html__( 'Behance Profile', 'noor-assistant' );
		$user_contactmethods['dribbble']  = esc_html__( 'Dribbble Profile', 'noor-assistant' );

		return $user_contactmethods;

	}

	add_filter( 'user_contactmethods', 'dima_add_contact_methods' );
endif;


function dima_shortcode_recent_posts( $atts ) {
	ob_start();
	extract(
		shortcode_atts(
			array(
				'id' => ''
			), $atts, 'recent_posts'
		)
	);

	?>
    <div class="boxed-blog blog-list"><?php
	dima_helper::dima_get_view( 'global', '_content-none' );
	?></div><?php
	return ob_get_clean();
}

foreach ( glob( DIMA_NOUR_ASSISTANT_TEMPLATE_PATH . '/include/shortcodes/*.php' ) as $shortcode ) {
	require_once( $shortcode );
}

