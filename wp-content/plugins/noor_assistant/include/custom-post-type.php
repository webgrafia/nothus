<?php

/**
 * Initialize CPT
 */

function dima_custom_post_type() {
	$base  = 'dimablock';
	$label = esc_html__( 'Section Block', 'noor-assistant' );

	// creating (registering) the custom type
	register_post_type( $base,
		// let's now add all the options for this post type
		array(
			'labels'              => array(
				'name'               => $label,
				/* This is the Title of the Group */
				'singular_name'      => sprintf( esc_html__( '%s Post', 'noor-assistant' ), $label ),
				/* This is the individual type */
				'all_items'          => sprintf( esc_html__( 'All %s', 'noor-assistant' ), $label ),
				/* the all items menu item */
				'add_new'            => esc_html__( 'Add New', 'noor-assistant' ),
				/* The add new menu item */
				'add_new_item'       => sprintf( esc_html__( 'Add New %s', 'noor-assistant' ), $label ),
				/* Add New Display Title */
				'edit'               => esc_html__( 'Edit', 'noor-assistant' ),
				/* Edit Dialog */
				'edit_item'          => sprintf( esc_html__( 'Edit %s', 'noor-assistant' ), $label ),
				/* Edit Display Title */
				'new_item'           => sprintf( esc_html__( 'New %s', 'noor-assistant' ), $label ),
				/* New Display Title */
				'view_item'          => sprintf( esc_html__( 'View %s', 'noor-assistant' ), $label ),
				/* View Display Title */
				'search_items'       => sprintf( esc_html__( 'Search %s', 'noor-assistant' ), $label ),
				/* Search Custom Type Title */
				'not_found'          => esc_html__( 'Nothing found in the Database.', 'noor-assistant' ),
				/* This displays if there are no entries yet */
				'not_found_in_trash' => esc_html__( 'Nothing found in Trash', 'noor-assistant' ),
				/* This displays if there is nothing in the trash */
				'parent_item_colon'  => ''
			),
			/* end of arrays */
			'public'              => is_user_logged_in() ? true : false,
			'publicly_queryable'  => is_user_logged_in() ? true : false,
			'exclude_from_search' => true,
			'show_ui'             => true,
			'query_var'           => true,
			'menu_position'       => 9,
			'menu_icon'           => 'dashicons-grid-view',
			/* you can specify its url slug */
			'has_archive'         => false,
			'capability_type'     => 'post',
			'hierarchical'        => true,
			'rewrite'             => false,
			/* the next one is important, it tells what's enabled in the post editor */
			'supports'            => array(
				'title',
				'editor',
				'author',
				'revisions',
			)
		)
	/* end of options */
	);
	/* end of register post type */

	// now let's add custom categories (these act like categories)
	register_taxonomy(
		'dimablock_category',
		'dimablock',
		array(
			'hierarchical'      => true,
			'public'            => false,
			'show_ui'           => true,
			'show_admin_column' => true,
			'show_in_nav_menus' => true,
			'labels'            => array(
				'name'              => sprintf( esc_html__( '%s Categories', 'noor-assistant' ), $label ),
				/* name of the custom taxonomy */
				'singular_name'     => sprintf( esc_html__( '%s Category', 'noor-assistant' ), $label ),
				/* single taxonomy name */
				'search_items'      => sprintf( esc_html__( 'Search %s Categories', 'noor-assistant' ), $label ),
				/* search title for taxomony */
				'all_items'         => sprintf( esc_html__( 'All %s Categories', 'noor-assistant' ), $label ),
				/* all title for taxonomies */
				'parent_item'       => sprintf( esc_html__( 'Parent %s Category', 'noor-assistant' ), $label ),
				/* parent title for taxonomy */
				'parent_item_colon' => sprintf( esc_html__( 'Parent %s Category:', 'noor-assistant' ), $label ),
				/* parent taxonomy title */
				'edit_item'         => sprintf( esc_html__( 'Edit %s Category', 'noor-assistant' ), $label ),
				/* edit custom taxonomy title */
				'update_item'       => sprintf( esc_html__( 'Update %s Category', 'noor-assistant' ), $label ),
				/* update title for taxonomy */
				'add_new_item'      => sprintf( esc_html__( 'Add New %s Category', 'noor-assistant' ), $label ),
				/* add new title for taxonomy */
				'new_item_name'     => sprintf( esc_html__( 'New %s Category Name', 'noor-assistant' ), $label )
				/* name title for taxonomy */
			),
			'rewrite'           => array( 'slug' => $base . '_cat' ),
		)
	);
}

add_action( 'ot_after_theme_options_save', 'dima_custom_portfolio_slug' );

//runs only when the theme is set up
function dima_custom_flush_rules() {
	//defines the post type so the rules can be flushed.
	dima_custom_post_type();
	//and flush the rules.
	flush_rewrite_rules();
}

register_activation_hook( __FILE__, 'dima_custom_flush_rules' );
add_action( 'init', 'dima_custom_post_type' );