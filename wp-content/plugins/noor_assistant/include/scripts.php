<?php

function dima_shortcodes_site_scripts() {
	$protocol = is_ssl() ? 'https' : 'http';
	$APIKEY   = dima_helper::dima_get_option( 'dima_google_map_api_key' );
	wp_register_script( 'vendor-google-maps', '' . $protocol . '://maps.googleapis.com/maps/api/js?key=' . $APIKEY . '', array( 'jquery' ), DIMA_NOUR_ASSISTANT_VERSION, true );
	wp_register_script( 'dima-shortcodes-site', DIMA_NOUR_ASSISTANT_URL . '/js/dima-shortcodes.js', array( 'jquery' ), DIMA_NOUR_ASSISTANT_VERSION, true );
	wp_enqueue_script( 'dima-shortcodes-site' );
}

add_action( 'wp_enqueue_scripts', 'dima_shortcodes_site_scripts' );
