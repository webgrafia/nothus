<?php

/*
Description: Add Portfolio posts to your existing site in minutes.
Author: PixelDima
Author URI: http://pixeldima.com/
*/

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 */
if ( ! class_exists( 'DIMA_NOUR_PORTFOLIO' ) ) {

	class DIMA_NOUR_PORTFOLIO {

		protected $slug_name;

		public function __construct() {
			$this->slug_name = 'dima-portfolio';
			add_action( 'init', array( $this, 'dima_portfolio_create_portfolio' ), 11 );
			add_action( 'init', array( $this, 'dima_portfolio_category_taxonomies' ), 11 );
			add_action( 'init', array( $this, 'dima_portfolio_tag_taxonomies' ), 11 );
			add_action( 'init', array( $this, 'dima_portfolio_flush_rewrite_rules' ), 11 );

			add_filter( 'manage_dima-portfolio_posts_columns', array(
				$this,
				'dima_portfolio_add_thumbnail_admin'
			), 10, 1 );
			add_action( 'manage_dima-portfolio_posts_custom_column', array(
				$this,
				'dima_portfolio_add_thumbnail_admin_content'
			), 10, 1 );
			add_filter( 'post_updated_messages', array( $this, 'dima_portfolio_updated_messages' ) );
		}

		/**
		 * Enable the Portfolio custom post type.
		 * @link http://codex.wordpress.org/Function_Reference/register_post_type
		 */
		public function dima_portfolio_create_portfolio() {
			$THEME_ARRY = array( 'noor child', 'OKAB', 'Noor', 'PixelDima' );
			if ( in_array( wp_get_theme(), $THEME_ARRY ) ) {
				$this->slug_name = dima_helper::dima_get_option( 'dima_projects_slug_name' );
			} else {
				$this->slug_name = "dima-portfolio";
			}
			$gallery_icon = ( floatval( get_bloginfo( 'version' ) ) >= '3.8' ) ? 'dashicons-portfolio' : null;

			$labels = array(
				'name'               => esc_html__( 'Portfolio', 'noor-assistant' ),
				'singular_name'      => esc_html__( 'Portfolio Item', 'noor-assistant' ),
				'add_new'            => esc_html__( 'Add New Item', 'noor-assistant' ),
				'add_new_item'       => esc_html__( 'Add New Portfolio Item', 'noor-assistant' ),
				'edit_item'          => esc_html__( 'Edit Portfolio Item', 'noor-assistant' ),
				'new_item'           => esc_html__( 'Add New Portfolio Item', 'noor-assistant' ),
				'all_items'          => esc_html__( 'All Portfolio items', 'noor-assistant' ),
				'view_item'          => esc_html__( 'View Item', 'noor-assistant' ),
				'search_items'       => esc_html__( 'Search Portfolio', 'noor-assistant' ),
				'not_found'          => esc_html__( 'No portfolio items found', 'noor-assistant' ),
				'not_found_in_trash' => esc_html__( 'No products found in the Trash', 'noor-assistant' ),
				'parent_item_colon'  => '',
				'menu_name'          => esc_html__( 'Portfolios', 'noor-assistant' ),
			);

			$args = array(
				'labels'              => $labels,
				'public'              => true,
				'show_ui'             => true,
				'publicly_queryable'  => true,
				'exclude_from_search' => false,
				'query_var'           => true,
				'supports'            => array(
					'title',
					'editor',
					'excerpt',
					'thumbnail',
					'trackbacks',
					'comments',
					'author',
					'sticky',
					'page-attributes',
					'custom-fields',
					'revisions'
				),/* ,'custom-fields' */
				'capability_type'     => 'post',
				'hierarchical'        => false,
				'rewrite'             => array( 'slug' => $this->slug_name, 'with_front' => false ),
				'menu_position'       => 5,
				'menu_icon'           => $gallery_icon,
				'has_archive'         => true
			);

			$args = apply_filters( 'dima_portfolio_post_type_args', $args );

			register_post_type( 'dima-portfolio', $args );

			// different post FORMAT support for different post TYPES
			add_post_type_support( 'dima-portfolio', 'post-formats', array( 'audio', 'gallery', 'image', 'video' ) );

		}

		function dima_portfolio_updated_messages( $messages ) {
			global $post, $post_ID;
			$messages[ $this->slug_name ] = array(
				0  => '',
				1  => sprintf( esc_html__( 'Portfolio updated.', 'noor-assistant' ) . ' <a href="%s">' . esc_html__( 'View product', 'noor-assistant' ) . '</a>', esc_url( get_permalink( $post_ID ) ) ),
				2  => esc_html__( 'Custom field updated.', 'noor-assistant' ),
				3  => esc_html__( 'Custom field deleted.', 'noor-assistant' ),
				4  => esc_html__( 'Portfolio updated.', 'noor-assistant' ),
				5  => isset( $_GET['revision'] ) ? sprintf( esc_html__( 'Portfolio restored to revision from %s', 'noor-assistant' ), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
				6  => sprintf( esc_html__( 'Portfolio published.', 'noor-assistant' ) . ' <a href="%s">' . esc_html__( 'View product', 'noor-assistant' ) . '</a>', esc_url( get_permalink( $post_ID ) ) ),
				7  => esc_html__( 'Portfolio saved.', 'noor-assistant' ),
				8  => sprintf( esc_html__( 'Portfolio submitted.', 'noor-assistant' ) . ' <a target="_blank" rel="noopener" href="%s">', esc_html__( 'Preview Project', 'noor-assistant' ) . '</a>', esc_url( add_query_arg( 'preview', 'true', get_permalink( $post_ID ) ) ) ),
				9  => sprintf( esc_html__( 'Portfolio scheduled for:', 'noor-assistant' ) . '<strong>' . esc_html__( '%1$s.', 'noor-assistant' ) . '</strong> <a target="_blank" rel="noopener" href="%2$s">' . esc_html__( 'Preview Project', 'noor-assistant' ) . '</a>', date_i18n( esc_html__( 'M j, Y @ G:i', 'noor-assistant' ), strtotime( $post->post_date ) ), esc_url( get_permalink( $post_ID ) ) ),
				10 => sprintf( esc_html__( 'Portfolio draft updated.', 'noor-assistant' ) . '<a target="_blank" rel="noopener" href="%s">' . esc_html__( 'Preview Project', 'noor-assistant' ) . '</a>', esc_url( add_query_arg( 'preview', 'true', get_permalink( $post_ID ) ) ) ),
			);

			return $messages;
		}

		/**
		 *  Create the Portfolio Category Taxonomy
		 */
		public function dima_portfolio_category_taxonomies() {

			// Create the Portfolio Category Taxonomy.
			$taxonomy_portfolio_category_labels = array(
				'name'                       => esc_html__( 'Portfolio Categories', 'noor-assistant' ),
				'singular_name'              => esc_html__( 'Portfolio Category', 'noor-assistant' ),
				'search_items'               => esc_html__( 'Search Portfolio Categories', 'noor-assistant' ),
				'popular_items'              => esc_html__( 'Popular Portfolio Categories', 'noor-assistant' ),
				'all_items'                  => esc_html__( 'All Portfolio Categories', 'noor-assistant' ),
				'parent_item'                => esc_html__( 'Parent Portfolio Category', 'noor-assistant' ),
				'parent_item_colon'          => esc_html__( 'Parent Portfolio Category:', 'noor-assistant' ),
				'edit_item'                  => esc_html__( 'Edit Portfolio Category', 'noor-assistant' ),
				'update_item'                => esc_html__( 'Update Portfolio Category', 'noor-assistant' ),
				'add_new_item'               => esc_html__( 'Add New Portfolio Category', 'noor-assistant' ),
				'new_item_name'              => esc_html__( 'New Portfolio Category Name', 'noor-assistant' ),
				'separate_items_with_commas' => esc_html__( 'Separate portfolio categories with commas', 'noor-assistant' ),
				'add_or_remove_items'        => esc_html__( 'Add or remove portfolio categories', 'noor-assistant' ),
				'choose_from_most_used'      => esc_html__( 'Choose from the most used portfolio categories', 'noor-assistant' ),
				'menu_name'                  => esc_html__( 'Portfolio Categories', 'noor-assistant' ),
			);

			$taxonomy_portfolio_category_args = array(
				'labels'            => $taxonomy_portfolio_category_labels,
				'public'            => true,
				'show_in_nav_menus' => true,
				'show_ui'           => true,
				'show_admin_column' => true,
				'show_tagcloud'     => true,
				'hierarchical'      => true,
				'rewrite'           => array( 'slug' => $this->slug_name . '-category', 'with_front' => false ),
				'query_var'         => true
			);

			register_taxonomy( 'portfolio-category', 'dima-portfolio', $taxonomy_portfolio_category_args );
		}


		/**
		 * Create the Portfolio Tag Taxonomy.
		 */
		public function dima_portfolio_tag_taxonomies() {
			// Create the Portfolio Tag Taxonomy.
			$taxonomy_portfolio_tag_labels = array(
				'name'                       => esc_html__( 'Portfolio Tags', 'noor-assistant' ),
				'singular_name'              => esc_html__( 'Portfolio Tag', 'noor-assistant' ),
				'search_items'               => esc_html__( 'Search Portfolio Tags', 'noor-assistant' ),
				'popular_items'              => esc_html__( 'Popular Portfolio Tags', 'noor-assistant' ),
				'all_items'                  => esc_html__( 'All Portfolio Tags', 'noor-assistant' ),
				'parent_item'                => esc_html__( 'Parent Portfolio Tag', 'noor-assistant' ),
				'parent_item_colon'          => esc_html__( 'Parent Portfolio Tag:', 'noor-assistant' ),
				'edit_item'                  => esc_html__( 'Edit Portfolio Tag', 'noor-assistant' ),
				'update_item'                => esc_html__( 'Update Portfolio Tag', 'noor-assistant' ),
				'add_new_item'               => esc_html__( 'Add New Portfolio Tag', 'noor-assistant' ),
				'new_item_name'              => esc_html__( 'New Portfolio Tag Name', 'noor-assistant' ),
				'separate_items_with_commas' => esc_html__( 'Separate portfolio tags with commas', 'noor-assistant' ),
				'add_or_remove_items'        => esc_html__( 'Add or remove portfolio tags', 'noor-assistant' ),
				'choose_from_most_used'      => esc_html__( 'Choose from the most used portfolio tags', 'noor-assistant' ),
				'menu_name'                  => esc_html__( 'Portfolio Tags', 'noor-assistant' )
			);

			$taxonomy_portfolio_tag_args = array(
				'labels'            => $taxonomy_portfolio_tag_labels,
				'public'            => true,
				'show_in_nav_menus' => true,
				'show_ui'           => true,
				'show_tagcloud'     => true,
				'hierarchical'      => false,
				'rewrite'           => array( 'slug' => $this->slug_name . '-tag', 'with_front' => false ),
				'show_admin_column' => true,
				'query_var'         => true
			);

			register_taxonomy( 'portfolio-tag', 'dima-portfolio', $taxonomy_portfolio_tag_args );
		}

		/**
		 * [dima_portfolio_add_thumbnail_admin Thumbnails to the Admin Screen]
		 */
		public function dima_portfolio_add_thumbnail_admin( $columns ) {
			$thumb   = array( 'thumb' => esc_html__( 'Thumbnail', 'noor-assistant' ) );
			$columns = array_slice( $columns, 0, 2 ) + $thumb + array_slice( $columns, 1 );

			return $columns;
		}

		public function dima_portfolio_add_thumbnail_admin_content( $column ) {
			if ( $column == 'thumb' ) {
				echo '<a href="' . get_edit_post_link() . '">' . get_the_post_thumbnail( get_the_ID(), array(
						200,
						200
					) ) . '</a>';
			}
		}

		public function dima_portfolio_flush_rewrite_rules() {
			// Flush rewrite rules if portfolio slug is updated.
			if ( get_transient( 'dima_portfolio_slug_before' ) != get_transient( 'dima_portfolio_slug_after' ) ) {
				flush_rewrite_rules( false );
				delete_transient( 'dima_portfolio_slug_before' );
				delete_transient( 'dima_portfolio_slug_after' );
			}
		}
	}

	new DIMA_NOUR_PORTFOLIO;

}

/**
 * Fix preview for portfolio items
 *
 * @param $link
 * @param $post
 *
 * @return string
 */
function dima_change_preview_post_link( $link, $post ) {
	$screen = get_current_screen();
	if ( $screen->post_type == 'dima-portfolio' ) {
		$post_link = str_replace( array(
			'&post_format=standard',
			'&post_format=link',
			'&post_format=image',
			'&post_format=audio',
			'&post_format=video'
		), '', $link );
		$link      = $post_link . '?&preview=true';

		return $link;
	} else {
		return $link;
	}
}

add_filter( 'preview_post_link', 'dima_change_preview_post_link', 10, 2 );
