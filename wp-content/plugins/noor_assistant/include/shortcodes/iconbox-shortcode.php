<?php

/**
 * Class and Function List:
 * Function list:
 *-dima_shortcode_portfolio()
 * Classes list:
 */
class DIMA_Iconbox_Shortcode {

	public function __construct() {
		add_shortcode( 'iconbox', array( $this, 'dima_shortcode_iconbox' ) );
	}

	function dima_shortcode_iconbox( $atts, $content = null ) {
		extract(
			shortcode_atts(
				array(
					'id'                      => '',
					'class'                   => '',
					'style'                   => '',
					'align'                   => '',
					'href'                    => '',
					'target'                  => '',
					'dima_vc_add_shadow'      => '',
					'dima_vc_shadow_on_hover' => '',
					'boxed'                   => '',
					'box_bg'                  => '',
					'dima_vc_border_color'    => '',

					'animation'      => '',
					'delay'          => '',
					'delay_offset'   => '',
					'delay_duration' => '',
				), $atts, 'iconbox'
			)
		);

		$id                   = ( $id != '' ) ? 'id="' . esc_attr( $id ) . '"' : '';
		$_class[]             = "dima-iconbox";
		$_class[]             = ( $class != '' ) ? esc_attr( $class ) : '';
		$_class[]             = ( $dima_vc_add_shadow == 'true' )
			? 'dima-iconbox-shadow' : '';
		$_class[]             = ( $dima_vc_shadow_on_hover == 'true' )
			? 'shadow-hover' : '';
		$_class[]             = ( $boxed == 'true' ) ? 'features-box' : 'no-box';
		$box_bg               = ( $box_bg != '' ) ? $box_bg : '';
		$dima_vc_border_color = ( $dima_vc_border_color != '' )
			? $dima_vc_border_color : '';
		$style                = ( $style != '' ) ? $style : '';
		$href                 = ( $href == '' ) ? '' : $href;
		$target               = ( $target == 'blank' ) ? 'target="_blank" rel="noopener"' : '';

		$animation_data = '';
		$animation_data .= ( $animation != '' ) ? 'data-animate=' . $animation . '' : '';
		$animation_data .= ( $delay != '' ) ? ' data-delay=' . $delay . '' : '';
		$animation_data .= ( $delay_offset != '' ) ? ' data-offset=' . $delay_offset . '' : '';
		$animation_data .= ( $delay_duration != '' ) ? ' data-duration=' . $delay_duration . '' : '';

		if ( ! empty( $box_bg ) ) {
			$box_bg = "background-color:$box_bg;";
		}
		if ( ! empty( $dima_vc_border_color ) ) {
			$dima_vc_border_color = "border-top-color:$dima_vc_border_color;";
		}

		$style = "style=\"$box_bg$style$dima_vc_border_color\"";

		switch ( $align ) {
			case 'end':
				$_class[] = "text-end";
				$_class[] = "features-end";
				break;
			case 'start':
				$_class[] = "text-start";
				$_class[] = "features-start";
				break;
			default:
				$_class[] = "text-center";
				break;
		}

		if ( ! empty( $_class ) ) {
			$class = dima_short_remove_white_space( join( ' ', $_class ) );
		}

		if ( $href != '' ) {
			$output
				= "<a href='{$href}' {$target}><div {$id} class=\"{$class}\" {$animation_data} {$style}>"
				  . do_shortcode( $content )
				  . "</div></a>";

		} else {
			$output = "<div {$id} class=\"{$class}\" {$animation_data} {$style}>"
			          . do_shortcode( $content )
			          . "</div>";
		}

		return $output;
	}
}

new DIMA_Iconbox_Shortcode();