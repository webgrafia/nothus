<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

function dima_shortcode_dima_image_layers( $atts ) {

	$output = $uniqid = $link_css = $el_class = $dima_lazyimage = $animation_duration = $translate_step = $list_fields = $animation_data = $padding = $padding_css = $periodicity = $alignment = '';

	$atts = vc_map_get_attributes( 'dima_image_layers', $atts );
	extract( $atts );


	$uniqid = uniqid( 'dima-image-layers-' ) . '-' . rand( 1, 9999 );

	if ( isset( $alignment ) ) {
		$el_class .= ' ' . $alignment;
	}
	$lazy_class = '';
	if ( dima_helper::dima_am_i_true( $dima_lazyimage ) ) {
		$lazy_class = 'dima-block-lazy';
	}

	$output .= '<div id="' . esc_attr( $uniqid ) . '" class="dima-image-layers-wrap ' . $lazy_class . '' . esc_attr( $el_class ) . '">';

	if ( empty( $translate_step ) ) {
		$translate_step = 150;
	}
	if ( empty( $animation_duration ) ) {
		$animation_duration = 750;
	}

	$animate_delay = $periodicity;

	if ( isset( $list_fields ) && ! empty( $list_fields ) && function_exists( 'vc_param_group_parse_atts' ) ) {
		$list_fields = (array) vc_param_group_parse_atts( $list_fields );

		foreach ( $list_fields as $fields ) {
			$offset_x_css = $offset_y_css = '';

			if ( isset( $fields['image_id'] ) && ! empty( $fields['image_id'] ) ) {

				$animate_delay  += $translate_step;
				$animation_data = '';
				if ( isset( $fields['layer_animation'] ) && ! empty( $fields['layer_animation'] ) ) {
					$animation_data .= 'data-animate=' . esc_attr( $fields['layer_animation'] ) . '';
					$animation_data .= ' data-delay="' . $animate_delay . '"';
					$animation_data .= ( $animation_duration != '' ) ? ' data-duration=' . $animation_duration . '' : '';

				}

				if ( ! isset( $fields['offcet_x'] ) ) {
					$fields['offcet_x'] = 0;
				}
				if ( ! isset( $fields['offcet_y'] ) ) {
					$fields['offcet_y'] = 0;
				}
				if ( $fields['offcet_x'] >= 100 ) {
					$fields['offcet_x'] = 100;
				}
				if ( $fields['offcet_x'] <= - 100 ) {
					$fields['offcet_x'] = - 100;
				}
				if ( $fields['offcet_y'] >= 100 ) {
					$fields['offcet_y'] = 100;
				}
				if ( $fields['offcet_y'] <= - 100 ) {
					$fields['offcet_y'] = - 100;
				}

				if ( ( isset( $fields['offcet_x'] ) && strcmp( $fields['offcet_x'], '' ) != 0 ) || ( isset( $fields['offcet_y'] ) && strcmp( $fields['offcet_y'], '' ) != 0 ) ) {
					$offset_x_css = '-webkit-transform: translate(' . esc_attr( $fields['offcet_x'] ) . '%, ' . esc_attr( $fields['offcet_y'] ) . '%); 
					-moz-transform: translate(' . esc_attr( $fields['offcet_x'] ) . '%, ' . esc_attr( $fields['offcet_y'] ) . '%); 
					-o-transform: translate(' . esc_attr( $fields['offcet_x'] ) . '%, ' . esc_attr( $fields['offcet_y'] ) . '%); 
					transform: translate(' . esc_attr( $fields['offcet_x'] ) . '%, ' . esc_attr( $fields['offcet_y'] ) . '%);';
				}

				$image = wp_get_attachment_image_src( $fields['image_id'], 'full' );

				$img_atts = dima_helper::dima_get_image_attrs( $image[0], $fields['image_id'], $image[1], $image[2], esc_attr__( 'Layer', 'noor' ) );

				if ( dima_helper::dima_am_i_true( $dima_lazyimage ) && DIMA_USE_LAZY ) {
					$poster_info = wp_get_attachment_image_src( $fields['image_id'], 'dima-lazy-image' );
					$small_src   = $poster_info[0];
					$img_width   = 'width="' . $image[1] . '"';
					$img_height  = 'height="' . $image[2] . '"';

					$img = '<img class="js-lazy-image" src="' . $small_src . '" data-src="' . $image[0] . '" ' . $img_atts . ' ' . $img_width . ' ' . $img_height . '>';
				} else {
					$img = '<img src="' . esc_url( $image[0] ) . '" ' . $img_atts . '>';
				}

				$output .= '<div class="dima-layer-container" ' . $animation_data . '>';
				$output .= '<div class="dima-layer-centered" style="' . $offset_x_css . ' ' . $offset_y_css . '">';
				$output .= '<div class="dima-layer-item">';
				$output .= $img;
				$output .= '</div>';
				$output .= '</div>';
				$output .= '</div>';
			}
		}
	}

	$output .= '</div>';

	return $output;
}

add_shortcode( 'dima_image_layers', 'dima_shortcode_dima_image_layers' );