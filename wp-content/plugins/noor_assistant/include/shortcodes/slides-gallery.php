<?php
/*

Plugin Name: DIMA-Shortcodes
Plugin URI: http://pixeldima.com/
Version: 1.0.0
Author: PixelDima
Author URI: http://pixeldima.com/
Text Domain: dima-shortcodes

*/

/**
 * Class DIMA_SlidesGallery
 */
class DIMA_SlidesGallery {

	public function __construct() {
		add_shortcode( 'dima_slides_gallery', array( $this, 'dima_gallery' ) );
	}

	function dima_gallery( $atts ) {
		extract( shortcode_atts( array(
			'id'                  => '',
			'class'               => '',
			'style'               => '',
			'pagination'          => 'true',
			'auto_play'           => '',
			'auto_play_speed'     => '',
			'navigation'          => '',
			'dots_style'          => 'false',
			'inner'               => '',
			'loop'                => '',
			'centermode'          => '',
			'centerpadding'       => '',
			'adaptiveheight'      => '',
			'variablewidth'       => '',
			'_draggable'          => 'true',
			'lazyload'            => '',
			'mobilefirst'         => '',
			'pauseonfocus'        => '',
			'pauseonhover'        => '',
			'fade'                => '',
			'verticalswiping'     => '',
			'_vertical'           => '',
			'speed'               => '',
			'dark'                => '',
			'items'               => '',
			'items_phone'         => '',
			'items_tablet'        => '',
			'slidestoscroll'      => 1,
			'items_margin'        => '',
			'thumbnail_style'     => 'dots',
			'thumbnail_clm'       => 3,
			'custom_links_target' => '_self',
			'dvc_source'          => 'media_library',
			'custom_srcs'         => '',
			'images'              => '',
			'img_size'            => 'full',
			'external_img_size'   => '',
			'onclick'             => 'link_image',
			'custom_links'        => '',

		), $atts, 'dima_slides_gallery' ) );

		wp_enqueue_script( 'dima-slick' );

		$id         = ( $id != '' ) ? 'id="' . esc_attr( $id ) . '"' : '';
		$class      = ( $class != '' ) ? esc_attr( $class ) : "slick-slider";
		$class      .= ( $centermode == 'true' ) ? " center_zoom_opacity" : "";
		$style      = ( $style != '' ) ? 'style="' . $style . '"' : '';
		$asNavFor   = $asNavFor_thumb = null;
		$gal_images = $gal_nav_images = $thumbnail = '';


		if ( $dark == "true" ) {
			$dark = " slick-darck";
		} else {
			$dark = "";
		}

		if ( $dots_style == "true" ) {
			$class .= " slick_side_dots";
		}

		if ( $items_margin == "true" ) {
			$dark .= " slick-with-margin";
		} else {
			$dark .= "";
		}

		if ( 'custom_link' === $onclick ) {
			$custom_links = vc_value_from_safe( $custom_links );
			$custom_links = explode( ',', $custom_links );
		}
		/**/
		$el_start     = "";
		$el_end       = "";
		$el_nav_start = "<div class=\"slick-item slick-slide\">";
		$el_nav_end   = "</div>";
		/**/

		switch ( $dvc_source ) {
			case 'media_library':
				$images = explode( ',', $images );
				break;

			case 'external_link':
				$images = vc_value_from_safe( $custom_srcs );
				$images = explode( ',', $images );

				break;
		}

		if ( $thumbnail_style == 'thumbnail_image' ) {
			$asNavFor     = ".slider-nav";
			$asNavFor_nav = ".slider-for";
			$pagination   = "false";
		}
		if ( count( $images ) <= (int) $thumbnail_clm ) {
			$asNavFor = "";
		}

		foreach ( $images as $i => $image ) {
			switch ( $dvc_source ) {
				case 'media_library':
					if ( $image > 0 ) {
						$img           = wpb_getImageBySize( array(
							'attach_id'  => $image,
							'thumb_size' => $img_size,
						) );
						$thumbnail     = $img['thumbnail'];
						$large_img_src = $img['p_img_large'][0];
					} else {
						$default_src   = vc_asset_url( 'vc/no_image.png' );
						$large_img_src = $default_src;
						$thumbnail     = '<img src="' . $default_src . '" />';
					}
					break;

				case 'external_link':
					$image         = esc_attr( $image );
					$dimensions    = vcExtractDimensions( $external_img_size );
					$hwstring      = $dimensions ? image_hwstring( $dimensions[0], $dimensions[1] ) : '';
					$thumbnail     = '<img ' . $hwstring . ' src="' . $image . '" />';
					$large_img_src = $image;
					break;
			}

			$link_start        = $link_end = '';
			$pretty_rel_random = ' data-rel="prettyPhoto[rel-' . get_the_ID() . '-' . rand() . ']"';

			switch ( $onclick ) {
				case 'img_link_large':
					$link_start = '<a href="' . $large_img_src . '" target="' . $custom_links_target . '">';
					$link_end   = '</a>';
					break;

				case 'link_image':
					$link_start = '<a data-lightbox="image" data-effect="mfp-zoom-in" href="' . $large_img_src . '"' . $pretty_rel_random . '>';
					$link_end   = '</a>';
					break;

				case 'custom_link':
					if ( ! empty( $custom_links[ $i ] ) ) {
						$link_start = '<a href="' . $custom_links[ $i ] . '"' . ( ! empty( $custom_links_target ) ? ' target="' . $custom_links_target . '"' : '' ) . '>';
						$link_end   = '</a>';
					}
					break;
			}

			$gal_images .= $el_start . $link_start . $thumbnail . $link_end . $el_end;
			if ( $thumbnail_style == 'thumbnail_image' ) {
				$link_start = '<a ' . ( ! empty( $custom_links_target ) ? ' target="' . $custom_links_target . '"' : '' ) . '>';
				$link_end   = '</a>';

				$gal_nav_images .= $el_nav_start . $link_start . $thumbnail . $link_end . $el_nav_end;
			}
		}

		$js_data = array(
			'dots'            => ( $pagination == 'true' ),
			'autoplay'        => ( $auto_play == 'true' ),
			'arrows'          => ( $navigation == '1' ),
			'slidesToScroll'  => ( $slidestoscroll == '' ) ? 1 : $slidestoscroll,
			'infinite'        => ( $loop == 'true' ),
			'fade'            => ( $fade == 'true' ),
			'centerMode'      => ( $centermode == 'true' ),
			'centerPadding'   => ( $centerpadding == '' ) ? '50px' : $centerpadding,
			'variableWidth'   => ( $variablewidth == 'true' ),
			'draggable'       => ( $_draggable == 'true' ),
			'adaptiveHeight'  => ( $adaptiveheight == 'true' ),
			'mobileFirst'     => ( $mobilefirst == 'true' ),
			'pauseOnHover'    => ( $pauseonhover == 'true' ),
			'pauseOnFocus'    => ( $pauseonfocus == 'true' ),
			'vertical'        => ( $_vertical == 'true' ),
			'verticalSwiping' => ( $verticalswiping == 'true' ),
			'slidesToShow'    => ( $items == '' ) ? 1 : $items,
			'speed'           => ( $speed == '' ) ? '300' : $speed,
			'autoplaySpeed'   => ( $auto_play_speed == '' ) ? '3000'
				: $auto_play_speed,
			'lazyLoad'        => ( $lazyload == '' ) ? 'Ondemand' : $lazyload,
			'items_phone'     => ( $items_phone == '' ) ? 1 : $items_phone,
			'items_tablet'    => ( $items_tablet == '' ) ? 1 : $items_tablet,
			'asNavFor'        => $asNavFor,
			'rtl'             => is_rtl()
		);

		$data = dima_creat_data_attributes( 'slick_slider', $js_data );


		$output = "<div {$id} class=\"slick-slider {$class}\" {$style}>"
		          . "<div class=\"slider-for {$dark}\" {$data}>"
		          . $gal_images
		          . '</div>'
		          . '</div>';
		/*thumbnail*/
		if ( $thumbnail_style == 'thumbnail_image' ) {
			$js_nav_data = array(
				'slidesToShow'   => $thumbnail_clm,
				'slidesToScroll' => 1,
				'infinite'       => true,
				'draggable'      => true,
				'focusOnSelect'  => true,
				'dots'           => false,
				'autoplay'       => false,
				'arrows'         => false,
				'fade'           => false,
				'adaptiveHeight' => true,
				'mobilefirst'    => true,
				'pauseOnHover'   => false,
				'asNavFor'       => $asNavFor_nav,
				'speed'          => '500',
				'autoplaySpeed'  => '3000',
				'rtl'            => is_rtl()
			);
			$data_nav    = dima_creat_data_attributes( 'slick_slider_nav', $js_nav_data );

			$output .= "<div class=\"slider-nav\" {$data_nav} >"
			           . $gal_nav_images
			           . '</div>';

		}

		return $output;
	}

}

new DIMA_SlidesGallery();