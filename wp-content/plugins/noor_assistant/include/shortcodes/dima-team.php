<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * TEAM
 *   1 Meat The Team
 *   2 Team Member
 *
 * @param      $atts
 * @param null $content
 *
 * @return string
 */
function dima_shortcode_meet_the_team( $atts, $content = null ) {
	extract(
		shortcode_atts(
			array(
				'id'          => '',
				'class'       => '',
				'bg_color'    => '',
				'text_color'  => '',
				'name_color'  => '',
				'name'        => '',
				'image'       => '',
				'job'         => '',
				'link'        => '',
				'img_hover'   => '',
				'elm_hover'   => '',
				'description' => '',
				'boxed'       => 'false',
				'style'       => '',
				'target'      => 'false',
				'animation'      => '',
				'delay'          => '',
				'delay_offset'   => '',
				'delay_duration' => '',
			), $atts, 'meet_the_team'
		)
	);

	$id          = ( $id != '' ) ? 'id="' . esc_attr( $id ) . '"' : '';
	$class       = ( $class != '' ) ? 'dima-team ' . esc_attr( $class ) : 'dima-team';
	$style       = ( $style != '' ) ? 'style="' . $style . '"' : '';
	$job         = ( $job != '' )
		? "<span class=\"dima-divider line-center line-hr small-line\"></span><span class=\"member-function\">"
		  . $job . "</span>" : '';
	$boxed       = ( $boxed == 'false' ) ? '' : ' box-with-shadow';
	$bg_color    = ( $bg_color != '' ) ? 'background-color:' . $bg_color . ''
		: '';
	$target      = ( $target != 'false' ) ? 'target="_blank" rel="noopener"' : '';
	$text_color  = ( $text_color != '' ) ? 'color:' . $text_color . '' : '';
	$name_color  = ( $name_color != '' ) ? 'color:' . $name_color . '' : '';
	$info_style  = 'style="' . $text_color . ';' . $bg_color . '"';
	$name_style  = 'style="' . $name_color . ';"';
	$text_name   = $name;
	$name        = ( $name != '' ) ? "<div class=\"member-name\"><h5 "
	                                 . $name_style . ">" . $name . "</h5></div>" : '';
	$description = ( $description != '' ) ? "<p " . $info_style . ">"
	                                        . $description . "</p>" : '';
	$image       = ( $image != '' ) ? $image : '';
	$elm_hover   = ( $elm_hover == 'op_vc_inside' ) ? ' dima_go_inside' : '';

	$animation_data = '';
	$animation_data .= ( $animation != '' ) ? 'data-animate=' . $animation . '' : '';
	$animation_data .= ( $delay != '' ) ? ' data-delay=' . $delay . '' : '';
	$animation_data .= ( $delay_offset != '' ) ? ' data-offset=' . $delay_offset . '' : '';
	$animation_data .= ( $delay_duration != '' ) ? ' data-duration=' . $delay_duration . '' : '';

	if ( $link != '' ) {
		$link = '<a class="all-over-thumb-link" href="' . $link . '" ' . $target . '></a>';
	}
	switch ( $img_hover ) {
		case 'op_vc_zoom-out':
			$img_hover = "effect-roxy";
			break;
		case 'op_vc_zoom-in':
			$img_hover = "effect-julia";
			break;
		case 'op_vc_gray':
			$img_hover = "apply-gray";
			break;
		case 'op_vc_opacity':
			$img_hover = "apply-opacity";
			break;
		case 'op_vc_none':
		default:
			$img_hover = "post-feature";
			break;
	}
	if ( is_numeric( $image ) ) {
		$bg_image_info = wp_get_attachment_image_src( $image, 'full' );
		$image         = $bg_image_info[0];
	}

	$team_image = "<div class=\"$img_hover dima-team-member$elm_hover\">"
	              . $link
	              . "<div class=\"team-img\">"
	              . "<div class=\"fix-chrome\">"
	              . "<figure>"
	              . "<img src=\"{$image}\" alt=\"{$text_name}\">"
	              . "</figure>"
	              . "<div class=\"post-icon\">"
	              . "<ul class=\"icons-media\">"
	              . do_shortcode( $content )
	              . "</ul>";
	$team_image .= "</div>"
	               . "</div>"
	               . "</div>"
	               . "</div>";

	$output = "<div {$id} class=\"{$class}{$boxed}\" {$animation_data} {$style}>"
	          . $team_image
	          . "<div class=\"dima-team-content text-center\" {$info_style}>"
	          . $name
	          . $job
	          . $description
	          . "</div>"
	          . "</div>";

	return $output;
}

add_shortcode( 'meet_the_team', 'dima_shortcode_meet_the_team' );
