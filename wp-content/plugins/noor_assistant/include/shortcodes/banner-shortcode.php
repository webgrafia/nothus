<?php

/**
 * Class and Function List:
 * Function list:
 *-dima_shortcode_portfolio()
 * Classes list:
 */
class DIMA_Banner_Shortcode {

	public function __construct() {
		add_shortcode( 'dima_banner', array( $this, 'dima_shortcode_banner' ) );
	}

	function dima_shortcode_banner( $atts, $content = null ) {
		extract( shortcode_atts( array(
			'bg_image'   => '',
			'link'       => '',
			'target'     => '',
			'text_color' => 'light',
			'text_align' => 'center',
			'flaot'      => 'center',
			'text_width' => '60%',
			'height'     => '300px',
			'parallax'   => false,
			'bg_video'   => false,
			'id'         => '',
			'style'      => '',
			'class'      => '',

			'cover'       => false,
			'cover_color' => '',
			'bg_gradient' => 'false',
			'gr_end'      => '',
			'gr_opacity'  => '0.6',

			'animation'      => '',
			'delay'          => '',
			'delay_offset'   => '',
			'delay_duration' => '',

		), $atts, 'dima_banner' ) );

		ob_start();

		$uniq_id = uniqid();

		$id             = ( $id != '' ) ? 'id="' . esc_attr( $id ) . '" ' : '';
		$class          = ( $class != '' ) ? ' ' . esc_attr( $class ) : '';
		$bg_image       = ( $bg_image != '' ) ? '' . esc_attr( $bg_image ) . '' : '';
		$style          = ( $style != '' ) ? 'style="' . $style . '"' : '';
		$bg_class       = ' background-image-hide background-cover';
		$text_width     = ( $text_width != '' ) ? 'style="width:' . $text_width . '"' : '';
		$flaot          = ( $flaot != '' ) ? ' ' . $flaot : 'center';
		$banner_class   = 'dima-banner';
		$animation_data = '';
		$animation_data .= ( $animation != '' ) ? 'data-animate=' . $animation . '' : '';
		$animation_data .= ( $delay != '' ) ? ' data-delay=' . $delay . '' : '';
		$animation_data .= ( $delay_offset != '' ) ? ' data-offset=' . $delay_offset . '' : '';
		$animation_data .= ( $delay_duration != '' ) ? ' data-duration=' . $delay_duration . '' : '';

		$CSS_STYLE = '';
		/*------------------------------*/
		# Gradient Overlay Orientation
		/*------------------------------*/
		if ( $cover == 'true' && $bg_gradient == 'false' ) {
			$banner_class = 'dima-banner dima-banner-' . $uniq_id . '';
			$el           = '.dima-banner-' . $uniq_id . ' .dima-color-mask-' . $uniq_id . '';
			$cover        = '<div class="dima-section-cover background-image-holder dima-color-mask-' . $uniq_id . '"></div>';
			$gr_start     = $cover_color;

			$bgcolor   = "background: " . $gr_start . ";";
			$CSS_STYLE .= DIMA_Style::dima_addCSS( $el . '{'
			                                       . $bgcolor
			                                       . '}', $uniq_id, true );

		} elseif ( $bg_gradient != 'false' ) {
			$banner_class = 'dima-banner dima-banner-' . $uniq_id . '';
			$el           = '.dima-banner-' . $uniq_id . ' .dima-color-mask-' . $uniq_id . '';
			$vertical     = $horizontal = $left_top = $left_bottom = $radial = '';
			$cover        = '<div class="dima-section-cover background-image-holder dima-color-mask-' . $uniq_id . '"></div>';

			$gr_start = dima_is_gradient_stop_transparent( $cover_color ) ? 'transparent' : $cover_color;
			$gr_end   = dima_is_gradient_stop_transparent( $gr_end ) ? 'transparent' : $gr_end;

			if ( $bg_gradient == 'vertical' ) {
				$vertical = "
            background: " . $gr_start . "; /* Old browsers */
            background: -moz-linear-gradient(top,  " . $gr_start . " 0%, " . $gr_end . " 100%); /* FF3.6+ */
            background: -webkit-gradient(linear, left top, left bottom, color-stop(0%," . $gr_start . "), color-stop(100%," . $gr_end . ")); /* Chrome,Safari4+ */
            background: -webkit-linear-gradient(top,  " . $gr_start . " 0%," . $gr_end . " 100%); /* Chrome10+,Safari5.1+ */
            background: -o-linear-gradient(top,  " . $gr_start . " 0%," . $gr_end . " 100%); /* Opera 11.10+ */
            background: -ms-linear-gradient(top,  " . $gr_start . " 0%," . $gr_end . " 100%); /* IE10+ */
            background: linear-gradient(to bottom,  " . $gr_start . " 0%," . $gr_end . " 100%); /* W3C */
        ";
			}

			if ( $bg_gradient == 'horizontal' ) {
				$horizontal = "
            background: " . $gr_start . "; /* Old browsers */
            background: -moz-linear-gradient(left,  " . $gr_start . " 0%, " . $gr_end . " 100%); /* FF3.6+ */
            background: -webkit-gradient(linear, left top, right top, color-stop(0%," . $gr_start . "), color-stop(100%," . $gr_end . ")); /* Chrome,Safari4+ */
            background: -webkit-linear-gradient(left,  " . $gr_start . " 0%," . $gr_end . " 100%); /* Chrome10+,Safari5.1+ */
            background: -o-linear-gradient(left,  " . $gr_start . " 0%," . $gr_end . " 100%); /* Opera 11.10+ */
            background: -ms-linear-gradient(left,  " . $gr_start . " 0%," . $gr_end . " 100%); /* IE10+ */
            background: linear-gradient(to right,  " . $gr_start . " 0%," . $gr_end . " 100%); /* W3C */
        ";
			}

			if ( $bg_gradient == 'left_top' ) {
				$left_top = "
            background: " . $gr_start . "; /* Old browsers */
            background: -moz-linear-gradient(-45deg,  " . $gr_start . " 0%, " . $gr_end . " 100%); /* FF3.6+ */
            background: -webkit-gradient(linear, left top, right bottom, color-stop(0%," . $gr_start . "), color-stop(100%," . $gr_end . ")); /* Chrome,Safari4+ */
            background: -webkit-linear-gradient(-45deg,  " . $gr_start . " 0%," . $gr_end . " 100%); /* Chrome10+,Safari5.1+ */
            background: -o-linear-gradient(-45deg,  " . $gr_start . " 0%," . $gr_end . " 100%); /* Opera 11.10+ */
            background: -ms-linear-gradient(-45deg,  " . $gr_start . " 0%," . $gr_end . " 100%); /* IE10+ */
            background: linear-gradient(135deg,  " . $gr_start . " 0%," . $gr_end . " 100%); /* W3C */
        ";
			}

			if ( $bg_gradient == 'left_bottom' ) {
				$left_bottom = "
            background: " . $gr_start . "; /* Old browsers */
            background: -moz-linear-gradient(45deg,  " . $gr_start . " 0%, " . $gr_end . " 100%); /* FF3.6+ */
            background: -webkit-gradient(linear, left bottom, right top, color-stop(0%," . $gr_start . "), color-stop(100%," . $gr_end . ")); /* Chrome,Safari4+ */
            background: -webkit-linear-gradient(45deg,  " . $gr_start . " 0%," . $gr_end . " 100%); /* Chrome10+,Safari5.1+ */
            background: -o-linear-gradient(45deg,  " . $gr_start . " 0%," . $gr_end . " 100%); /* Opera 11.10+ */
            background: -ms-linear-gradient(45deg,  " . $gr_start . " 0%," . $gr_end . " 100%); /* IE10+ */
            background: linear-gradient(45deg,  " . $gr_start . " 0%," . $gr_end . " 100%); /* W3C */
        ";
			}

			if ( $bg_gradient == 'radial' ) {
				$radial = "
            background: " . $gr_start . "; /* Old browsers */
            background: -moz-radial-gradient(center, ellipse cover,  " . $gr_start . " 0%, " . $gr_end . " 100%); /* FF3.6+ */
            background: -webkit-gradient(radial, center center, 0px, center center, 100%, color-stop(0%," . $gr_start . "), color-stop(100%," . $gr_end . ")); /* Chrome,Safari4+ */
            background: -webkit-radial-gradient(center, ellipse cover,  " . $gr_start . " 0%," . $gr_end . " 100%); /* Chrome10+,Safari5.1+ */
            background: -o-radial-gradient(center, ellipse cover,  " . $gr_start . " 0%," . $gr_end . " 100%); /* Opera 12+ */
            background: -ms-radial-gradient(center, ellipse cover,  " . $gr_start . " 0%," . $gr_end . " 100%); /* IE10+ */
            background: radial-gradient(ellipse at center,  " . $gr_start . " 0%," . $gr_end . " 100%); /* W3C */
        ";
			}

			$opacity   = 'opacity:' . $gr_opacity . ';';
			$CSS_STYLE = DIMA_Style::dima_addCSS( $el . '{'
			                                      . $vertical
			                                      . $horizontal
			                                      . $left_top
			                                      . $left_bottom
			                                      . $radial
			                                      . $opacity
			                                      . '}', $uniq_id, true );
		}
//--------------------------

		if ( is_numeric( $bg_image ) ) {
			$bg_image_info = wp_get_attachment_image_src( $bg_image, 'full' );
			$bg_image      = $bg_image_info[0];
		}

		$start_link = "";
		$end_link   = "";
		if ( $target ) {
			$target = 'target="' . $target . '"';
		}
		if ( $link ) {
			$start_link = '<a href="' . $link . '" ' . $target . '>';
			$end_link   = '</a>';
		};

		//=========
		$color = ( $text_color != 'light' ) ? 'bg-light' : 'bg-dark';

		switch ( $text_align ) {
			case 'end' :
				$text_align = ' text-end';
				break;
			case 'start' :
				$text_align = ' text-start';
				break;
			case 'center' :
				$text_align = ' text-center';
				break;
			default :
				$text_align = '';
		}

		//=========
		$content_global = '' . $start_link
		                  . '<div class="' . $banner_class . ' page-section ' . $color . '" ' . $animation_data . ' data-height=' . $height . '> '
		                  . $cover
		                  . '<div class="banner-inner text-vertical-center ' . $flaot . '' . $text_align . '" ' . $text_width . '  > '
		                  . '<div class="dima-inner-wrap"> '
		                  . do_shortcode( $content )
		                  . '</div > '
		                  . '</div > '
		                  . '</div > '
		                  . $end_link;

		if ( empty( $bg_image ) && empty( $bg_pattern ) ) {
			$output = '<div ' . $id . ' class="' . $class . 'section" ' . $animation_data . ' ' . $style . ' > '
			          . '<div class="overflow-hidden" > '
			          . $content_global
			          . '</div > '
			          . '</div > ';
		} else {
			$bg_image = ( $bg_image == '' ) ? $bg_pattern : $bg_image;

			if ( $parallax == 'parallax' ) {
				$output = ' <div ' . $id . ' class="' . $class . 'section" ' . $animation_data . ' ' . $style . ' > '
				          . '<div class="overflow-hidden" > '
				          . '<div class="' . $bg_class . ' parallax-background" data-bg-image="' . $bg_image . '"> '
				          . '</div > '
				          . $content_global
				          . '</div > '
				          . '</div > ';
			} elseif ( $parallax == 'fixed_parallax' ) {
				$output = '<div ' . $id . ' class="' . $class . 'section" ' . $animation_data . ' ' . $style . ' > '
				          . '<div class="overflow-hidden" > '
				          . '<div class="fixed-parallax ' . $bg_class . '" data-bg-image="' . $bg_image . '"> '
				          . '</div > '
				          . $content_global
				          . '</div > '
				          . '</div > ';
			} else {
				$output = '<div ' . $id . ' class="' . $class . 'section" ' . $animation_data . ' ' . $style . ' > '
				          . '<div class="overflow-hidden" > '
				          . '<div class="' . $bg_class . '" data-bg-image="' . $bg_image . '"> '
				          . '</div > '
				          . $content_global
				          . '</div > '
				          . '</div > ';
			}
		}

		if ( ! empty( $bg_video ) ) {
			$back_metadata   = dima_media_support::dima_get_metadata( $bg_video );
			$back_attributes = dima_media_support::dima_get_thumb_url( $bg_video );
			$provider        = dima_media_support::dima_detect_video_service( $bg_video );
			$video_orig_w    = ( $back_metadata->width == 0 ) ? 800 : $back_metadata->width;
			$video_orig_h    = ( $back_metadata->height == 0 ) ? 800 : $back_metadata->height;
			$video_ratio     = ( $video_orig_h === 0 ) ? 1.777 : $video_orig_w / $video_orig_h;

			$output = '<div ' . $id . ' class="background-element section" >'
			          . '<div class="' . $class . ' overflow-hidden">'
			          . '<div class="' . $bg_class . ' video dima-video-container" data-ratio="' . $video_ratio . '" data-provider="' . $provider . '" data-video="' . $bg_video . '" data-id="' . rand( 10000, 99999 ) . '" data-img-wrap="' . $back_attributes . '">'
			          . '</div>'
			          . $content_global
			          . '</div>'
			          . '</div>';
		}

		$output .= $CSS_STYLE;

		return $output;
	}
}

new DIMA_Banner_Shortcode();