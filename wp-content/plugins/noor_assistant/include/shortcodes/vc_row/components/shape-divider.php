<?php

$html = '<div class="dima-shape-divider dima-shape-divider--stick">
			<div class="shape__container">
				<div class="shape">
				</div>
			</div>
		</div>';


$style       = $view_params['style'];
$size        = $view_params['size'];
$shape_color = $view_params['shape_color'];
$bg_color    = $view_params['bg_color'];
$el_class    = $view_params['el_class'];

$html = phpQuery::newDocument( $html );
$id   = uniqid();

$pattern_path    = $pattern_width = $pattern_width_viewbox = $pattern_height = '';
$shape_box       = pq( '.dima-shape-divider' );
$shape_container = $shape_box->find( '.shape__container' );

$shape_box->attr( 'id', 'dima-shape-divider-' . $id );

$shape_box->addClass( $style . '-style' );
$shape_box->addClass( $size . '-size' );
$shape_box->addClass( $el_class );

$is_top     = ( strpos( $style, '-top' ) !== false ) ? false : true;
$stickClass = ( $is_top ) ? 'dima-shape-divider--stick-bottom' : 'dima-shape-divider--stick-top';
$shape_box->filter( '.dima-shape-divider--stick' )->addClass( $stickClass );

$shape_color = $shape_color !== '' ? $shape_color : 'transparent';

// Shape Svg Settings


/*--------*/

if ( $style == 'big-wave-top' ) {
	$pattern_path          = '<path d="M0,3.66998291 C190.003296,79.0462443 390.323954,116.734375 600.961975,116.734375 C916.919006,116.734375 971.891663,1.859375 1330.14557,1.859375 C1568.98151,1.859375 1765.62669,40.1178385 1920.08112,116.634766 L1919.9971,0 L0,1.39220029e-16 L0,3.66998291 Z" fill="' . $shape_color . '"></path>';
	$pattern_width         = '100%';
	$pattern_width_viewbox = '100';
	$pattern_height        = '115';
} else if ( $style == 'big-wave-bottom' ) {
	$pattern_path          = '<path d=" M 1920.081 113.064 C 1730.078 37.688 1529.757 0 1319.119 0 C 1003.162 0 948.189 114.875 589.936 114.875 C 351.1 114.875 154.454 76.617 0 0.1 L 0.084 116.734 L 1920.081 116.734 L 1920.081 113.064 Z " fill-rule="evenodd" fill="' . $shape_color . '"/>';
	$pattern_width         = '100%';
	$pattern_width_viewbox = '100';
	$pattern_height        = '115';
} else if ( $style == 'diagonal-bottom' ) {
	if ( $size == 'small' ) {
		$pattern_path          = '<polygon fill="' . $shape_color . '" points="100,70 100,0 0,70 "/>';
		$pattern_width         = '100%';
		$pattern_width_viewbox = '100';
		$pattern_height        = '70';
	} else if ( $size == 'big' ) {
		$pattern_path          = '<polygon fill="' . $shape_color . '" points="100,0 100,130 0,130 "/>';
		$pattern_width         = '100%';
		$pattern_width_viewbox = '100';
		$pattern_height        = '130';
	}
} else if ( $style == 'diagonal-top' ) {
	if ( $size == 'small' ) {
		$pattern_path          = '<polygon fill="' . $shape_color . '" points="0,0 0,70 100,0 "/>';
		$pattern_width         = '100%';
		$pattern_width_viewbox = '100';
		$pattern_height        = '70';
	} else if ( $size == 'big' ) {
		$pattern_path          = '<polygon fill="' . $shape_color . '" points="0,130 0,0 100,0 "/>';
		$pattern_width         = '100%';
		$pattern_width_viewbox = '100';
		$pattern_height        = '130';
	}
} else if ( $style == 'peaked-top' ) {
	if ( $size == 'small' ) {
		$pattern_path          = '<polygon points="25,25,0,0,50,0" fill="' . $shape_color . '"/>';
		$pattern_width         = '50px';
		$pattern_width_viewbox = '50';
		$pattern_height        = '25';
	} else if ( $size == 'big' ) {
		$pattern_path          = '<polygon points="50,50,0,0,100,0" fill="' . $shape_color . '"/>';
		$pattern_width         = '100px';
		$pattern_width_viewbox = '100';
		$pattern_height        = '50';
	}
} else if ( $style == 'peaked-bottom' ) {
	if ( $size == 'small' ) {
		$pattern_path          = '<polygon points="25,0,50,25,0,25" fill="' . $shape_color . '"/>';
		$pattern_width         = '50px';
		$pattern_width_viewbox = '50';
		$pattern_height        = '25';
	} else if ( $size == 'big' ) {
		$pattern_path          = '<polygon points="50,0,100,50,0,50" fill="' . $shape_color . '"/>';
		$pattern_width         = '100px';
		$pattern_width_viewbox = '100';
		$pattern_height        = '50';
	}
} else if ( $style == 'small-waves-top' ) {
	if ( $size == 'small' ) {
		$pattern_path          = '<path d=" M -0.004 0.008 Q 8.25 15.021 22.494 15.021 Q 36.738 15.021 44.992 0.008 L -0.004 0.008 Z " fill="' . $shape_color . '"/>';
		$pattern_width         = '45px';
		$pattern_width_viewbox = '45';
		$pattern_height        = '15';
	} else if ( $size == 'big' ) {
		$pattern_path          = '<path d=" M 90 0 Q 73.491 30 45 30 Q 16.509 30 0 0 L 90 0 Z " fill="' . $shape_color . '"/>';
		$pattern_width         = '90px';
		$pattern_width_viewbox = '90';
		$pattern_height        = '30';
	}
} else if ( $style == 'small-waves-bottom' ) {
	if ( $size == 'small' ) {
		$pattern_path          = '<path d=" M 44.992 15.021 Q 36.738 0.008 22.494 0.008 Q 8.25 0.008 -0.004 15.021 L 44.992 15.021 Z " fill="' . $shape_color . '"/>';
		$pattern_width         = '45px';
		$pattern_width_viewbox = '45';
		$pattern_height        = '15';
	} else if ( $size == 'big' ) {
		$pattern_path          = '<path d=" M 0 30 Q 16.509 0 45 0 Q 73.491 0 90 30 L 0 30 Z " fill="' . $shape_color . '"/>';
		$pattern_width         = '90px';
		$pattern_width_viewbox = '90';
		$pattern_height        = '30';
	}
} /*----------*/

if ( $style == 'drop-top' || $style == 'drop-bottom' ) {
	$shape_container->find( '.shape' )->append( '
		<div class="drop-left"></div>
		<div class="drop-right"></div>
		<div class="clearfix"></div>
	' );
} else if ( $style != 'big-wave-top' && $style != 'big-wave-bottom' ) {
	$shape_container->find( '.shape' )->append( '
		<svg width="100%" height="' . $pattern_height . 'px">
			<defs>
			    <pattern id="shapeDividerPattern-' . $id . '" preserveAspectRatio="none" style="background-repeat: none;" patternUnits="userSpaceOnUse" x="0" y="0" width="' . $pattern_width . '" height="' . $pattern_height . '0px" viewBox="0 0 ' . $pattern_width_viewbox . ' ' . $pattern_height . '0" >
			        ' . $pattern_path . '
			    </pattern>
			</defs>

			<!-- Background -->
			<rect x="0" y="0" width="100%" height="' . $pattern_height . 'px" fill="url(#shapeDividerPattern-' . $id . ')" />
		</svg>
	' );
} else {
	$shape_container->find( '.shape' )->append( '
		<svg width="100%" height="' . $pattern_height . 'px">
			    <g id="shapeDividerPattern-' . $id . '" preserveAspectRatio="none" style="background-repeat: none;" patternUnits="userSpaceOnUse" x="0" y="0" width="' . $pattern_width . '" height="' . $pattern_height . '0px" viewBox="0 0 ' . $pattern_width_viewbox . ' ' . $pattern_height . '0" >
			        ' . $pattern_path . '
			    </g>

			<!-- Background -->
			<rect x="0" y="0" width="100%" height="' . $pattern_height . 'px" fill="url(#shapeDividerPattern-' . $id . ')" />
		</svg>
	' );
}


/**
 * Custom CSS Output
 * ==================================================================================*/
DIMA_Style::dima_addCSS( '
	#dima-shape-divider-' . $id . ' .shape__container {
 		background-color: ' . $bg_color . ';
 	}
	/* @-moz-document url-prefix() { */
		#dima-shape-divider-' . $id . ' .shape__container .shape {
			overflow: hidden;
			height: ' . $pattern_height . ';
		}
	/* } */
 ', $id );

if ( $is_top == true ) {
	DIMA_Style::dima_addCSS( '
		/* @-moz-document url-prefix() { */
			#dima-shape-divider-' . $id . ' .shape__container .shape svg {
				position: relative;
				top: 0.6px;
			}
		/* } */
	', $id );
} else {
	DIMA_Style::dima_addCSS( '
		/* @-moz-document url-prefix() { */
			#dima-shape-divider-' . $id . ' .shape__container .shape svg {
				position: relative;
				top: -0.6px;
			}
		/* } */
	', $id );
}

if ( $style == 'drop-bottom' || $style == 'drop-top' ) {
	DIMA_Style::dima_addCSS( '
		#dima-shape-divider-' . $id . ' .shape__container .shape .drop-left,
		#dima-shape-divider-' . $id . ' .shape__container .shape .drop-right {
			background-color: ' . $shape_color . ';
		}
	', $id );
}
print $html;
