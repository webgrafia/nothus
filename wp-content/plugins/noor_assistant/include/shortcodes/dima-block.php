<?php
function dima_shortcode_dima_block( $atts ) {

	extract( shortcode_atts( array(
		'id' => '',
	), $atts ) );

	$id          = apply_filters( 'wpml_object_id', $id, 'post' );
	$the_content = get_post_field( 'post_content', $id );

	if ( class_exists( 'Vc_Base' ) ) {
		$vc = new Vc_Base();
		$vc->addShortcodesCustomCss( $id );
	}
	echo dima_helper::dima_remove_wpautop( $the_content );
}

add_shortcode( 'dima_block', 'dima_shortcode_dima_block' );