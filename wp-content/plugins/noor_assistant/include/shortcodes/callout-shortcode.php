<?php

/**
 * Class and Function List:
 * Function list:
 *-dima_shortcode_portfolio()
 * Classes list:
 */
class DIMA_Callout_Shortcode {

	public function __construct() {
		add_shortcode( 'callout', array( $this, 'dima_shortcode_callout' ) );
	}

	function dima_shortcode_callout( $atts, $content = null ) {
		extract(
			shortcode_atts(
				array(
					'id'              => '',
					'class'           => '',
					'style'           => '',
					'direction'       => '',
					'type'            => '',
					'title'           => '',
					'message'         => '',
					'parallax'        => false,
					'no_border'       => 'false',
					'dima_box_shadow' => 'false',
					'href'            => '',
					'bg_image'        => '',
					'text_color'      => '',
					'title_color'     => '',
					'full'            => '',
					'target'          => '',

					'cover'       => false,
					'cover_color' => '',
					'bg_gradient' => 'false',
					'gr_end'      => '',
					'gr_opacity'  => '0.6',

					'animation'      => '',
					'delay'          => '',
					'delay_offset'   => '',
					'delay_duration' => '',
				), $atts, 'callout'
			)
		);

		$uniq_id = uniqid();

		$id              = ( $id != '' ) ? 'id="' . esc_attr( $id ) . '"' : '';
		$class           = ( $class != '' ) ? 'dima-callout ' . esc_attr( $class )
			: 'dima-callout';
		$title_color     = ( $title_color != '' ) ? 'style="color:' . $title_color . ';"' : '';
		$text_color      = ( $text_color != '' ) ? 'style="color:' . $text_color . ';"' : '';
		$style           = ( $style != '' ) ? 'style="' . $style . '"' : '';
		$title           = ( $title != '' ) ? $title : '';
		$message         = ( $message != '' ) ? $message : 'Don\'t forget to enter in your text.';
		$href            = ( $href != '' ) ? $href : '#';
		$target          = ( $target == 'blank' ) ? 'target="_blank" rel="noopener"' : '';
		$bg_image        = ( $bg_image != '' ) ? $bg_image : '';
		$no_border       = ( $no_border == 'true' ) ? 'no-border ' : '';
		$dima_box_shadow = ( $dima_box_shadow == 'true' ) ? ' dima-box-shadow' : '';
		$class           .= ( $full == 'true' ) ? ' dima-full-callout' : '';
		$bg              = '';
		$CSS_STYLE       = '';

		/*------------------------------*/
		# Gradient Overlay Orientation
		/*------------------------------*/
		if ( $cover == 'true' && $bg_gradient == 'false' ) {
			$class    .= ' dima-callout-' . $uniq_id . '';
			$el       = '.dima-callout-' . $uniq_id . ' .dima-color-mask-' . $uniq_id . '';
			$cover    = '<div class="dima-section-cover background-image-holder dima-color-mask-' . $uniq_id . '"></div>';
			$gr_start = $cover_color;

			$bgcolor   = "background: " . $gr_start . ";";
			$CSS_STYLE .= DIMA_Style::dima_addCSS( $el . '{'
			                                       . $bgcolor
			                                       . '}', $uniq_id, true );

		} elseif ( $bg_gradient != 'false' ) {
			$class    .= ' dima-callout-' . $uniq_id . '';
			$el       = '.dima-callout-' . $uniq_id . ' .dima-color-mask-' . $uniq_id . '';
			$vertical = $horizontal = $left_top = $left_bottom = $radial = '';
			$cover    = '<div class="dima-section-cover background-image-holder dima-color-mask-' . $uniq_id . '"></div>';

			$gr_start = dima_is_gradient_stop_transparent( $cover_color ) ? 'transparent' : $cover_color;
			$gr_end   = dima_is_gradient_stop_transparent( $gr_end ) ? 'transparent' : $gr_end;

			if ( $bg_gradient == 'vertical' ) {
				$vertical = "
            background: " . $gr_start . "; /* Old browsers */
            background: -moz-linear-gradient(top,  " . $gr_start . " 0%, " . $gr_end . " 100%); /* FF3.6+ */
            background: -webkit-gradient(linear, left top, left bottom, color-stop(0%," . $gr_start . "), color-stop(100%," . $gr_end . ")); /* Chrome,Safari4+ */
            background: -webkit-linear-gradient(top,  " . $gr_start . " 0%," . $gr_end . " 100%); /* Chrome10+,Safari5.1+ */
            background: -o-linear-gradient(top,  " . $gr_start . " 0%," . $gr_end . " 100%); /* Opera 11.10+ */
            background: -ms-linear-gradient(top,  " . $gr_start . " 0%," . $gr_end . " 100%); /* IE10+ */
            background: linear-gradient(to bottom,  " . $gr_start . " 0%," . $gr_end . " 100%); /* W3C */
        ";
			}

			if ( $bg_gradient == 'horizontal' ) {
				$horizontal = "
            background: " . $gr_start . "; /* Old browsers */
            background: -moz-linear-gradient(left,  " . $gr_start . " 0%, " . $gr_end . " 100%); /* FF3.6+ */
            background: -webkit-gradient(linear, left top, right top, color-stop(0%," . $gr_start . "), color-stop(100%," . $gr_end . ")); /* Chrome,Safari4+ */
            background: -webkit-linear-gradient(left,  " . $gr_start . " 0%," . $gr_end . " 100%); /* Chrome10+,Safari5.1+ */
            background: -o-linear-gradient(left,  " . $gr_start . " 0%," . $gr_end . " 100%); /* Opera 11.10+ */
            background: -ms-linear-gradient(left,  " . $gr_start . " 0%," . $gr_end . " 100%); /* IE10+ */
            background: linear-gradient(to right,  " . $gr_start . " 0%," . $gr_end . " 100%); /* W3C */
        ";
			}

			if ( $bg_gradient == 'left_top' ) {
				$left_top = "
            background: " . $gr_start . "; /* Old browsers */
            background: -moz-linear-gradient(-45deg,  " . $gr_start . " 0%, " . $gr_end . " 100%); /* FF3.6+ */
            background: -webkit-gradient(linear, left top, right bottom, color-stop(0%," . $gr_start . "), color-stop(100%," . $gr_end . ")); /* Chrome,Safari4+ */
            background: -webkit-linear-gradient(-45deg,  " . $gr_start . " 0%," . $gr_end . " 100%); /* Chrome10+,Safari5.1+ */
            background: -o-linear-gradient(-45deg,  " . $gr_start . " 0%," . $gr_end . " 100%); /* Opera 11.10+ */
            background: -ms-linear-gradient(-45deg,  " . $gr_start . " 0%," . $gr_end . " 100%); /* IE10+ */
            background: linear-gradient(135deg,  " . $gr_start . " 0%," . $gr_end . " 100%); /* W3C */
        ";
			}

			if ( $bg_gradient == 'left_bottom' ) {
				$left_bottom = "
            background: " . $gr_start . "; /* Old browsers */
            background: -moz-linear-gradient(45deg,  " . $gr_start . " 0%, " . $gr_end . " 100%); /* FF3.6+ */
            background: -webkit-gradient(linear, left bottom, right top, color-stop(0%," . $gr_start . "), color-stop(100%," . $gr_end . ")); /* Chrome,Safari4+ */
            background: -webkit-linear-gradient(45deg,  " . $gr_start . " 0%," . $gr_end . " 100%); /* Chrome10+,Safari5.1+ */
            background: -o-linear-gradient(45deg,  " . $gr_start . " 0%," . $gr_end . " 100%); /* Opera 11.10+ */
            background: -ms-linear-gradient(45deg,  " . $gr_start . " 0%," . $gr_end . " 100%); /* IE10+ */
            background: linear-gradient(45deg,  " . $gr_start . " 0%," . $gr_end . " 100%); /* W3C */
        ";
			}

			if ( $bg_gradient == 'radial' ) {
				$radial = "
            background: " . $gr_start . "; /* Old browsers */
            background: -moz-radial-gradient(center, ellipse cover,  " . $gr_start . " 0%, " . $gr_end . " 100%); /* FF3.6+ */
            background: -webkit-gradient(radial, center center, 0px, center center, 100%, color-stop(0%," . $gr_start . "), color-stop(100%," . $gr_end . ")); /* Chrome,Safari4+ */
            background: -webkit-radial-gradient(center, ellipse cover,  " . $gr_start . " 0%," . $gr_end . " 100%); /* Chrome10+,Safari5.1+ */
            background: -o-radial-gradient(center, ellipse cover,  " . $gr_start . " 0%," . $gr_end . " 100%); /* Opera 12+ */
            background: -ms-radial-gradient(center, ellipse cover,  " . $gr_start . " 0%," . $gr_end . " 100%); /* IE10+ */
            background: radial-gradient(ellipse at center,  " . $gr_start . " 0%," . $gr_end . " 100%); /* W3C */
        ";
			}

			$opacity   = 'opacity:' . $gr_opacity . ';';
			$CSS_STYLE = DIMA_Style::dima_addCSS( $el . '{'
			                                      . $vertical
			                                      . $horizontal
			                                      . $left_top
			                                      . $left_bottom
			                                      . $radial
			                                      . $opacity
			                                      . '}', $uniq_id, true );
		}
		//--------------------------


		$animation_data = '';
		$animation_data .= ( $animation != '' ) ? 'data-animate=' . $animation . '' : '';
		$animation_data .= ( $delay != '' ) ? ' data-delay=' . $delay . '' : '';
		$animation_data .= ( $delay_offset != '' ) ? ' data-offset=' . $delay_offset . '' : '';
		$animation_data .= ( $delay_duration != '' ) ? ' data-duration=' . $delay_duration . '' : '';

		if ( is_numeric( $bg_image ) ) {
			$bg_image_info = wp_get_attachment_image_src( $bg_image, 'full' );
			$bg_image      = $bg_image_info[0];
		}

		switch ( $direction ) {
			case 'center' :
				$direction = ' text-center';
				break;
			case 'end' :
				$direction = ' text-end';
				break;
			default :
				$direction = ' text-start';
		}
		if ( $bg_image != '' ) {
			if ( $parallax == 'parallax' ) {
				$bg = '<div class="background-image-hide background-cover parallax-background" data-bg-image="' . $bg_image . '"></div>';
			} elseif ( $parallax == 'fixed_parallax' ) {
				$bg = '<div class="background-image-hide background-cover fixed-parallax" data-bg-image="' . $bg_image . '"></div>';
			} else {
				$bg = '<div class="background-image-hide background-cover" data-bg-image="' . $bg_image . '"></div>';
			}
		}
		if ( ! empty( $title ) ) {
			$title = "<h4 class=\"h-callout\" {$title_color}>{$title}</h4>";
		}

		switch ( $type ) {
			case 'style_two' :
				$output
					= "<div {$id} class=\"{$no_border}{$class}{$direction}{$dima_box_shadow}\" {$animation_data} {$style}>"
					  . $cover
					  . $bg
					  . "<div class=\"dima-callout-clm1\">"
					  . $title
					  . "<p class=\"p-callout\" {$text_color}>{$message}</p>"
					  . "</div>"
					  . "<div class=\"dima-callout-clm2 text-end\">"
					  . do_shortcode( $content )
					  . '</div></div>';
				break;
			case 'style_three' :
				$output = "<a href=\"{$href}\" {$animation_data} {$target}>"
				          . "<div {$id} class=\"hover-callout {$no_border}{$class}{$direction}{$dima_box_shadow}\" {$style}>"
				          . $cover
				          . $bg
				          . $title
				          . "<p class=\"p-callout\" {$text_color}>{$message}</p>"
				          . '</div></a>';
				break;
			default :
				$output
					= "<div {$id} class=\"dima-callout-btn-bottom {$no_border}{$class}{$direction}{$dima_box_shadow}\" {$animation_data} {$style}>"
					  . $cover
					  . $bg
					  . $title
					  . "<p class=\"p-callout\" {$text_color}>{$message}</p>"
					  . do_shortcode( $content )
					  . '</div>';

		};

		$output .= $CSS_STYLE;

		return $output;
	}
}

new DIMA_Callout_Shortcode();