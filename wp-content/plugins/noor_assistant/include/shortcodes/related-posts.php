<?php

/**
 * Class and Function List:
 * Function list:
 * - dima_shortcode_related_posts()
 * Classes list:
 */
class DIMA_RelatedPosts {

	public function __construct() {
		add_shortcode( 'related_posts', array( $this, 'dima_shortcode_related_posts' ) );
	}

	function dima_shortcode_related_posts( $atts ) {

		extract( shortcode_atts( array(
			'id'           => '',
			'is_slide'     => false,
			'class'        => '',
			'style'        => '',
			'column'       => 3,
			'items_margin' => 30,
			'count'        => 3,
		), $atts, 'related_posts' ) );

		$id    = ( $id != '' ) ? 'id="' . esc_attr( $id ) . '"' : '';
		$class = ( $class != '' ) ? 'boxed-blog blog-list ' . esc_attr( $class ) : 'boxed-blog blog-list';
		$style = ( $style != '' ) ? 'style="' . $style . '"' : '';

		$clm = dima_helper::dima_get_clm( $column );
		if ( $is_slide ) {
			wp_enqueue_script( 'dima-slick' );
		}
		$ARG_ARRAY = array(
			'is_slide'     => $is_slide,
			'column'       => $column,
			'clm'          => $clm,
			'count'        => $count,
			'pagination'   => 'true',
			'auto_play'    => 'true',
			'navigation'   => 'false',
			'loop'         => 'false',
			'items'        => $column,
			'items_phone'  => '',
			'items_tablet' => '',
			'items_margin' => $items_margin,
		);
		ob_start();

		$related_posts = dima_helper::dima_get_post_related_posts( $ARG_ARRAY['count'] );
		$slick_class   = '';
		$data          = '';
		$ok_row        = 'ok-row ';

		if ( $related_posts ) {
			$js_data = array(
				'dots'           => ( $ARG_ARRAY['pagination'] == 'true' ),
				'autoplay'       => ( $ARG_ARRAY['auto_play'] == 'true' ),
				'arrows'         => ( $ARG_ARRAY['navigation'] == 'true' ),
				'infinite'       => ( $ARG_ARRAY['loop'] == 'true' ),
				'slidesToScroll' => 1,
				'slidesToShow'   => ( $ARG_ARRAY['items'] == '' ) ? 1 : $ARG_ARRAY['items'],
				'items_phone'    => ( $ARG_ARRAY['items_phone'] == '' ) ? 1 : $ARG_ARRAY['items_phone'],
				'items_tablet'   => ( $ARG_ARRAY['items_tablet'] == '' ) ? 2 : $ARG_ARRAY['items_tablet'],
				'items_margin'   => ( $ARG_ARRAY['items_margin'] == '' ) ? 0 : $ARG_ARRAY['items_margin'],
				'rtl'            => is_rtl()
			);

			if ( $ARG_ARRAY['is_slide'] ) {
				$slick_class      = 'dima_slick_slider ';
				$data             = dima_creat_data_attributes( 'slick_slider', $js_data );
				$ARG_ARRAY['clm'] = '';
				$ok_row           = '';
			}
			$slick_class .= $ok_row;

			$is_slide_class = '';
			if ( $is_slide ) {
				$is_slide_class = " related-posts-slide";
			}
			?>
            <div class="related-posts related-posts-slide<?php echo esc_attr( $is_slide_class ); ?> clearfix">
                <h4 class="related-posts-title"><?php esc_html_e( 'Related Posts', 'noor' ); ?></h4>
                <div class="dima-divider noor-line noor-start"></div>

                <div class="ok-row <?php echo "$slick_class"; ?>related-entry clearfix" <?php echo $data; ?>>
					<?php while ( $related_posts->have_posts() ) : $related_posts->the_post(); ?>
                        <div class='related-post <?php echo $ARG_ARRAY['clm']; ?>'>
                            <div class="related-entry-media">
                                <div class="related-entry-thumbnail">
									<?php
									echo dima_helper::dima_get_post_thumb( array(
										'size'                       => 'dima-related-image',
										'a_class'                    => array( 'overlay' ),
										'post_format_thumb_fallback' => true,
									) );
									?>
                                </div>
                            </div>
                            <div class="related-entry-title">
                                <h5 class="title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h5>
                            </div>
                            <p class="date"><?php echo esc_html( get_the_date() ); ?></p>
                        </div>
					<?php endwhile; ?>
					<?php wp_reset_postdata(); ?>
                </div>
            </div>

			<?php
		}

		return ob_get_clean();
	}

}

new DIMA_RelatedPosts();
?>