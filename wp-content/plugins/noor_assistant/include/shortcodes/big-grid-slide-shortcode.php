<?php
/*

Plugin Name: DIMA-Shortcodes
Plugin URI: http://pixeldima.com/
Version: 1.0.0
Author: PixelDima
Author URI: http://pixeldima.com/
Text Domain: dima-shortcodes

*/


/**
 * Class and Function List:
 * Function list:
 * - dima_shortcode_blog()
 * Classes list:
 */
class DIMA_BigGridSlide {
	private $query = '';
	const POST_LIMIT = 5;

	public function __construct() {
		add_shortcode( 'big-grid-slide', array( $this, 'dima_shortcode_blog' ) );
	}

	function dima_shortcode_blog( $atts ) {

		$atts = vc_map_get_attributes( 'big-grid-slide', $atts );
		extract( $atts );

		wp_enqueue_script( 'dima-slick' );

		$template    = dima_helper::dima_get_template();
		$blog_style  = "slide_one";
		$items_phone = $items_tablet = 1;
		$POST_ARRAY  = array(
			'template'      => $template,
			'blog_style'    => $blog_style,
			'count'         => $count,
			'class'         => ' dima-big-grid-slide-one',
			'dots'          => ( $dots == 'true' ),
			'navigation'    => ( $navigation == 'true' ),
			'auto_play'     => ( $auto_play == 'true' ),
			'loop'          => ( $loop == 'true' ),
			'items'         => $column,
			'items_phone'   => $items_phone,
			'items_tablet'  => $items_tablet,
			'animation'     => $animation,
			'dark'          => ( $dark == 'true' ),
			'is_responsive' => $is_responsive,

			'screen_xld_resolution'  => $screen_xld_resolution,
			'screen_xld_spacer_size' => $screen_xld_spacer_size,
			'screen_ld_resolution'   => $screen_ld_resolution,
			'screen_ld_spacer_size'  => $screen_ld_spacer_size,
			'screen_md_resolution'   => $screen_md_resolution,
			'screen_md_spacer_size'  => $screen_md_spacer_size,
			'screen_sd_resolution'   => $screen_sd_resolution,
			'screen_sd_spacer_size'  => $screen_sd_spacer_size,
			'screen_xsd_resolution'  => $screen_xsd_resolution,
			'screen_xsd_spacer_size' => $screen_xsd_spacer_size,
		);

		switch ( $order ) {
			case 'top-view':
				$order_array = array(
					'meta_key' => 'dima_post_views_count',
					'orderby'  => 'meta_value_num',
					'order'    => 'DESC'
				);
				break;
			case 'popular':
				$order_array = array(
					'orderby' => 'comment_count',
					'order'   => 'DESC'
				);
				break;
			default:
				$order_array = array( 'orderby' => 'date' );
				break;
		}

		if ( ! empty( $category ) ) {
			$category = dima_helper::dima_get_slug_by_ids( $category, 'term_id', 'category' );
		}

		if ( ! empty( $tag ) ) {
			$tag = dima_helper::dima_get_slug_by_ids( $tag, 'term_id', 'post_tag' );
		}

		$array_query = array(
			'post_type'      => "post",
			'posts_per_page' => "{$count}",
			'offset'         => "{$offset}",
			'category_name'  => "{$category}",
			'tag'            => "{$tag}",
		);
		$merge       = array_merge( $array_query, $order_array );
		$dima_query  = new WP_Query( $merge );
		$this->query = $dima_query;

		if ( is_archive() ) {
			global $wp_query;
			$dima_query = $wp_query;
		}
		$ARG_ARRAY = array(
			'blog_style'  => $blog_style,
			'cover_color' => $cover_color,
		);

		ob_start();
		$this->Blog_Style( $POST_ARRAY, $dima_query, $ARG_ARRAY );

		return ob_get_clean();
	}

	public function Blog_Style( $POST_ARRAY, $wp_query, $ARG_ARRAY ) {
		if ( empty( $wp_query ) ) {
			$wp_query = new WP_Query( array( 'orderby' => 'date' ) );
		}
		if ( $ARG_ARRAY['blog_style'] == 'slide_one' ) {
			$this->slide_one( $POST_ARRAY, $wp_query, $ARG_ARRAY );
		} else {
			$this->slide_global( $POST_ARRAY, $wp_query, $ARG_ARRAY );
		}
	}

	function slide_one( $POST_ARRAY, $wp_query, $ARG_ARRAY ) {
		$responsive_height_data = '';
		$slick_class            = '';
		$js_data                = array(
			'dots'         => $POST_ARRAY['dots'],
			'arrows'       => $POST_ARRAY['navigation'],
			'autoplay'     => ( $POST_ARRAY['auto_play'] == 'false' ),
			'infinite'     => ( $POST_ARRAY['loop'] == 'true' ),
			'slidesToShow' => ( $POST_ARRAY['items'] == '' ) ? 1 : $POST_ARRAY['items'],
			'items_phone'  => ( $POST_ARRAY['items_phone'] == '' ) ? 1 : $POST_ARRAY['items_phone'],
			'items_tablet' => ( $POST_ARRAY['items_tablet'] == '' ) ? 2 : $POST_ARRAY['items_tablet'],
			'rtl'          => is_rtl()
		);

		if ( $POST_ARRAY['is_responsive'] == 'true' ) {
			$responsive_height_data .= ' data-xld_resolution="' . esc_attr( $POST_ARRAY['screen_xld_resolution'] ) . '"';
			$responsive_height_data .= ' data-xld_size="' . esc_attr( $POST_ARRAY['screen_xld_spacer_size'] ) . '"';
			$responsive_height_data .= ' data-ld_resolution="' . esc_attr( $POST_ARRAY['screen_ld_resolution'] ) . '"';
			$responsive_height_data .= ' data-ld_size="' . esc_attr( $POST_ARRAY['screen_ld_spacer_size'] ) . '"';
			$responsive_height_data .= ' data-md_resolution="' . esc_attr( $POST_ARRAY['screen_md_resolution'] ) . '"';
			$responsive_height_data .= ' data-md_size="' . esc_attr( $POST_ARRAY['screen_md_spacer_size'] ) . '"';
			$responsive_height_data .= ' data-sd_resolution="' . esc_attr( $POST_ARRAY['screen_sd_resolution'] ) . '"';
			$responsive_height_data .= ' data-sd_size="' . esc_attr( $POST_ARRAY['screen_sd_spacer_size'] ) . '"';
			$responsive_height_data .= ' data-xsd_resolution="' . esc_attr( $POST_ARRAY['screen_xsd_resolution'] ) . '"';
			$responsive_height_data .= ' data-xsd_size="' . esc_attr( $POST_ARRAY['screen_xsd_spacer_size'] ) . '"';
			$slick_class            .= "dima-height-responsive ";
		}

		if ( $POST_ARRAY['dark'] ) {
			$slick_class .= "slick-darck ";
		}

		$data = dima_creat_data_attributes( 'slick_slider', $js_data );
		?>

        <div class="<?php echo $slick_class; ?>slick-no-margin dima-block-slide" <?php echo "$data"; ?> <?php echo $responsive_height_data; ?>>
			<?php
			if ( $wp_query->have_posts() ) {
				while ( $wp_query->have_posts() ):
					$wp_query->the_post();
					dima_helper::dima_get_view_with_args( $POST_ARRAY['template'], 'big-grid-content', 'main', $ARG_ARRAY );
				endwhile;
				wp_reset_postdata();
			} else {
				dima_helper::dima_get_view( 'global', '_content-none' );
			} ?>
        </div>
		<?php
		wp_reset_query();
	}
}

new DIMA_BigGridSlide();
?>