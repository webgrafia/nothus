<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

function dima_shortcode_dima_ads( $atts ) {

	$output = $data_atts = $units = '';
	$atts   = vc_map_get_attributes( 'dima_ads', $atts );
	extract( $atts );

	$id             = ( $id != '' ) ? 'id="' . esc_attr( $id ) . '"' : '';
	$class          = ( $class != '' ) ? ' dima-img ' . esc_attr( $class )
		: 'dima-img ';
	$style          = ( $style != '' ) ? 'style="' . $style . '"' : '';
	$float          = ( $float != '' ) ? esc_attr( $float ) : '';
	$src            = ( $src != '' ) ? $src : '';
	$alt            = ( $alt != '' ) ? 'alt="' . $alt . '"' : 'alt=""';
	$target         = ( $target == 'blank' ) ? 'target="_blank" rel="noopener"' : '';
	$nofollow       = ( $nofollow == 'true' ) ? 'rel="nofollow"' : '';
	$delay          = ( $delay != '' ) ? ' data-delay=' . $delay . '' : '';
	$delay_offset   = ( $delay_offset != '' ) ? ' data-offset=' . $delay_offset . '' : '';
	$animation      = ( $animation != '' ) ? ' data-animate=' . $animation . '' : '';
	$delay_duration = ( $delay_duration != '' ) ? ' data-duration=' . $delay_duration . '' : '';

	switch ( $float ) {
		case 'end' :
			$float = ' text-end';
			break;
		case 'start' :
			$float = ' text-start float-start';
			break;
		default :
			$float = ' text-center float-end';
	}
	if ( $custom_ad != '' ) {
		$output = rawurldecode( base64_decode( strip_tags( $custom_ad ) ) );;
	} else {
		if ( $src != '' ) {
			if ( is_numeric( $src ) ) {
				$poster_info = wp_get_attachment_image_src( $src, 'full' );
				$src         = $poster_info[0];
				$img_width   = 'width="' . $poster_info[1] . '"';
				$img_height  = 'height="' . $poster_info[2] . '"';
			} else {
				$poster_info = dima_helper::dima_get_attachment_info_by_url( $src );
				$img_width   = 'width="' . $poster_info[1] . '"';
				$img_height  = 'height="' . $poster_info[2] . '"';
			}
		} else {
			return '';
		}
		if ( ! empty( $href ) ) {
			$output
				= "<a {$id} class=\"overlay {$class}\" {$style} href=\"{$href}\" {$target} {$nofollow}><img src=\"{$src}\" {$alt} $img_width $img_height></a>";
		} else {
			$output
				= "<img $img_width $img_height {$id} class=\"{$class}\" src=\"{$src}\" {$style} {$alt}>";
		}
	}

	return "<div class=\"stream-item mag-elms{$float}\" {$animation} {$delay} {$delay_offset} {$delay_duration}>"
	       . $output . "</div>";

}

add_shortcode( 'dima_ads', 'dima_shortcode_dima_ads' );