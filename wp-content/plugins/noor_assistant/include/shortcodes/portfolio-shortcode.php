<?php
/*

Plugin Name: DIMA-Shortcodes
Plugin URI: http://pixeldima.com/
Version: 1.0.0
Author: PixelDima
Author URI: http://pixeldima.com/
Text Domain: dima-shortcodes

*/

/**
 * Class and Function List:
 * Function list:
 * - dima_shortcode_portfolio()
 * Classes list:
 */

/**
 * Class DIMA_Portfolio_Shortcode
 */
class DIMA_Portfolio_Shortcode {

	private $G_pagination = true;

	public function __construct() {
		add_shortcode( 'portfolio', array( $this, 'dima_shortcode_portfolio' ) );
	}

	function dima_shortcode_portfolio( $atts ) {
		ob_start();
		extract( shortcode_atts( array(
			'id'               => '',
			'style'            => '',
			'post_class'       => '',
			'portfolio_style'  => '',
			'count'            => 6,
			'column'           => 2,
			'img_hover'        => '',
			'elm_hover'        => '',
			'no_margin'        => 'false',
			'category'         => '',
			'dark'             => '',
			'slide_pagination' => true,
			'slide_arrows'     => true,
			'auto_play'        => false,
			'loop'             => false,
			'filters'          => 'true',
			'hide_all'         => 'true',
			'offset'           => '',
			'ajax'             => '',
			'paging'           => 'false',
		), $atts, 'portfolio' ) );

		wp_enqueue_script( 'magnific-js' );

		$template        = dima_helper::dima_get_template();
		$post_class      = ( $post_class != '' ) ? ' ' . esc_attr( $post_class ) : '';
		$category        = ( $category != '' ) ? $category : '';
		$elm_hover       = ( $elm_hover != '' ) ? $elm_hover : '';
		$img_hover       = ( $img_hover != '' ) ? $img_hover : '';
		$paging          = dima_helper::dima_am_i_true( $paging );
		$dark            = ( $dark == '' ) ? 'true' : $dark;
		$is_slide        = false;
		$no_margin       = dima_helper::dima_am_i_true( $no_margin );
		$filters         = dima_helper::dima_am_i_true( $filters );
		$hide_all        = dima_helper::dima_am_i_true( $hide_all );
		$this->is_paging = $paging;
		$tag             = '';

		if ( $portfolio_style == 'slide' ) {
			$portfolio_style = "grid";
			$is_slide        = true;
			$filters         = 'false';
			wp_enqueue_script( 'dima-slick' );
		}

		if ( get_query_var( 'paged' ) ) { $paged = get_query_var( 'paged' ); }
        elseif ( get_query_var( 'page' ) ) { $paged = get_query_var( 'page' ); }
		else { $paged = 1; }

		if ( $offset != '' && $paging == true ) {
			$offset = ( $paged - $offset ) * $count;
		}
		$order_array = array( 'orderby' => 'date' );

		$filters_array = array(
			'filters'  => $filters,
			'category' => $category,
			'hide_all' => $hide_all,
		);

		if ( is_archive() ) {
			$page_object = get_queried_object();
			if ( ! empty( $page_object ) && isset( $page_object->taxonomy ) ) {
				if ( $page_object->taxonomy == 'portfolio-category' ) {
					$category = $page_object->slug;
				} elseif ( $page_object->taxonomy == 'portfolio-tag' ) {
					$tag = $page_object->slug;
				}
			}
		} else {
			if ( ! empty( $category ) ) {
				$category = dima_helper::dima_get_slug_by_ids( $category, 'term_id', 'portfolio-category' );
			}
		}


		if ( $paging ) {
			$array_query = array(
				'post_type'          => 'dima-portfolio',
				'posts_per_page'     => "{$count}",
				'paged'              => "{$paged}",
				'offset'             => "{$offset}",
				'portfolio-category' => "{$category}",
				'portfolio-tag'      => "{$tag}"
			);
			$merge       = array_merge( $array_query, $order_array );
			$WP_Query    = new WP_Query( $merge );
			$this->query = $WP_Query;

		} else {
			$WP_Query           = new WP_Query( array(
				'post_type'          => 'dima-portfolio',
				'posts_per_page'     => "{$count}",
				'offset'             => "{$offset}",
				'portfolio-category' => "{$category}",
				'portfolio-tag'      => "{$tag}"
			) );
			$this->G_pagination = false;
		}

		/*if ( is_archive() ) {
			global $wp_query;
			$WP_Query = $wp_query;
		}*/

		$ARG_ARRAY = array(
			'no_margin'              => $no_margin,
			'elm_hover'              => $elm_hover,
			'img_hover'              => $img_hover,
			'blog_type'              => $portfolio_style,
			'post_class'             => $post_class,
			'animation'              => 'transition.slideLeftBigIn',
			'delay'                  => 30,
			'delay_offset'           => '98%',
			'delay_duration'         => 750,
			'data-dima-animate-item' => '.isotope-item',
		);

		$clm        = dima_helper::dima_get_clm( $column );
		$POST_ARRAY = array(
			'template'     => $template,
			'is_slide'     => $is_slide,
			'column'       => $column,
			'clm'          => $clm,
			'count'        => $count,
			'pagination'   => $slide_pagination,
			'auto_play'    => $auto_play,
			'arrows'       => $slide_arrows,
			'loop'         => $loop,
			'items'        => $column,
			'items_phone'  => '',
			'items_tablet' => '',
			'no_margin'    => $no_margin,
			'dark'         => $dark,
		);

		$this->portfolio_type( $POST_ARRAY, $WP_Query, $ARG_ARRAY, $filters_array );

		return ob_get_clean();

	}

	/**
	 * @param $POST_ARRAY
	 * @param $WP_Query
	 * @param $ARG_ARRAY
	 * @param $filters_array
	 */
	public function portfolio_type( $POST_ARRAY, $WP_Query, $ARG_ARRAY, $filters_array ) {
		$portfolio_type = $ARG_ARRAY['blog_type'];
		if ( empty( $WP_Query ) ) {
			$WP_Query = new WP_Query( array( 'orderby' => 'date' ) );
		}
		if ( $portfolio_type != '' ) {
			$this->$portfolio_type( $POST_ARRAY, $WP_Query, $ARG_ARRAY, $filters_array );
		} else {
			$this->grid( $POST_ARRAY, $WP_Query, $ARG_ARRAY, $filters_array );
		}
	}


	/**
	 * @param $POST_ARRAY
	 * @param $portfolio
	 * @param $ARG_ARRAY
	 * @param $filters_array
	 */
	function grid( $POST_ARRAY, $portfolio, $ARG_ARRAY, $filters_array ) {

		$slick_class   = '';
		$data          = '';
		$iso_container = '';

		if ( is_archive() || is_home() || ( is_singular() && is_page() ) ) {
			$ARG_ARRAY['post_class'] .= " isotope-item";
			if ( $POST_ARRAY['is_slide'] != 'true' ) {
				$ARG_ARRAY['post_class'] .= ' ' . $POST_ARRAY['clm'];
				$iso_container           = 'dima-isotope-container ';
			}
		}

		$js_data = array(
			'dots'         => ( $POST_ARRAY['pagination'] == 'true' ) ? true : false,
			'autoplay'     => ( $POST_ARRAY['auto_play'] == 'true' ) ? true : false,
			'arrows'       => ( $POST_ARRAY['arrows'] == 'true' ) ? true : false,
			'infinite'     => ( $POST_ARRAY['loop'] == 'true' ),
			'slidesToShow' => ( $POST_ARRAY['items'] == '' ) ? 1 : $POST_ARRAY['items'],
			'items_phone'  => ( $POST_ARRAY['items_phone'] == '' ) ? 1 : $POST_ARRAY['items_phone'],
			'items_tablet' => ( $POST_ARRAY['items_tablet'] == '' ) ? 2 : $POST_ARRAY['items_tablet'],
			'draggable'    => true,
			'rtl'          => is_rtl()
		);

		$animation_data = '';
		$animation_data .= ( $ARG_ARRAY['animation'] != '' ) ? 'data-animate=' . $ARG_ARRAY['animation'] . '' : '';
		$animation_data .= ( $ARG_ARRAY['delay'] != '' ) ? ' data-delay=' . $ARG_ARRAY['delay'] . '' : '';
		$animation_data .= ( $ARG_ARRAY['delay_offset'] != '' ) ? ' data-offset=' . $ARG_ARRAY['delay_offset'] . '' : '';
		$animation_data .= ( $ARG_ARRAY['data-dima-animate-item'] != '' ) ? ' data-dima-animate-item=' . $ARG_ARRAY['data-dima-animate-item'] . '' : '';
		$animation_data .= ( $ARG_ARRAY['delay_duration'] != '' ) ? ' data-duration=' . $ARG_ARRAY['delay_duration'] . '' : '';

		if ( $POST_ARRAY['is_slide'] == 'true' ) {
			$slick_class       .= '' . $POST_ARRAY['dark'] . ' ';
			$data              = dima_creat_data_attributes( 'slick_slider', $js_data );
			$POST_ARRAY['clm'] = '';
		}
		if ( $POST_ARRAY['no_margin'] ) {
			$slick_class .= " slick-no-margin slick-noor-margin";
		} else {
			$slick_class .= " slick-with-margin clm-" . $POST_ARRAY['column'];
		}
		//$animation_data='';
		?>
        <div class="dima-isotope-wrapper">
			<?php
            dima_portfolio_filters( $filters_array );
            ?>
            <div id="dima-isotope-container" class="<?php echo $slick_class;
			echo " ";
			echo $iso_container; ?>" <?php echo $data;
			echo $animation_data ?> >
				<?php
				if ( $portfolio->have_posts() ):
					while ( $portfolio->have_posts() ):
						$portfolio->the_post();
						dima_helper::dima_get_view_with_args( $POST_ARRAY['template'], 'content', 'portfolio', $ARG_ARRAY );
					endwhile;
					wp_reset_postdata();
				else:
					dima_helper::dima_get_view( 'global', '_content-none' );
				endif;
				?>
            </div>
        </div>
		<?php
		if ( $this->G_pagination ) {
			dima_pagination( $portfolio );
		}
		?>
		<?php wp_reset_query(); ?>
		<?php
	}

	/**
	 * @param $POST_ARRAY
	 * @param $portfolio
	 * @param $ARG_ARRAY
	 * @param $filters_array
	 */
	function masonry( $POST_ARRAY, $portfolio, $ARG_ARRAY, $filters_array ) {
		if ( is_archive() || is_home() || ( is_singular() && is_page() ) ) {
			$ARG_ARRAY['post_class'] .= " isotope-item";
			$ARG_ARRAY['post_class'] .= ' ' . $POST_ARRAY['clm'];
		}

		if ( $ARG_ARRAY['no_margin'] == 'true' ) {
			$ARG_ARRAY['no_margin'] = " isotope-no-margin ";
		}
		$animation_data = '';
		$animation_data .= ( $ARG_ARRAY['animation'] != '' ) ? 'data-animate=' . $ARG_ARRAY['animation'] . '' : '';
		$animation_data .= ( $ARG_ARRAY['delay'] != '' ) ? ' data-delay=' . $ARG_ARRAY['delay'] . '' : '';
		$animation_data .= ( $ARG_ARRAY['delay_offset'] != '' ) ? ' data-offset=' . $ARG_ARRAY['delay_offset'] . '' : '';
		$animation_data .= ( $ARG_ARRAY['data-dima-animate-item'] != '' ) ? ' data-dima-animate-item=' . $ARG_ARRAY['data-dima-animate-item'] . '' : '';
		$animation_data .= ( $ARG_ARRAY['delay_duration'] != '' ) ? ' data-duration=' . $ARG_ARRAY['delay_duration'] . '' : '';

		?>
        <div class="dima-isotope-wrapper">
			<?php dima_portfolio_filters( $filters_array ); ?>
            <div class="boxed-blog blog-list" <?php echo $animation_data; ?>>
                <div id="dima-isotope-container"
                     class="<?php echo $ARG_ARRAY['no_margin'] ?>dima-isotope-container isotope masonry boxed-protfolio">
					<?php
					if ( $portfolio->have_posts() ):
						while ( $portfolio->have_posts() ):
							$portfolio->the_post();
							dima_helper::dima_get_view_with_args( $POST_ARRAY['template'], 'content', 'portfolio', $ARG_ARRAY );
						endwhile;
						wp_reset_postdata();
					else:
						dima_helper::dima_get_view( 'global', '_content-none' );
					endif;
					?>
                </div>
            </div>
        </div>
		<?php
		if ( $this->G_pagination ) {
			dima_pagination( $portfolio );
		}
		?>
		<?php wp_reset_query(); ?>
		<?php
	}


}

new DIMA_Portfolio_Shortcode();
?>