<?php

/**
 * Class and Function List:
 * Function list:
 *-dima_shortcode_portfolio()
 * Classes list:
 */
class dima_shortcode_inline_icons {

	public function __construct() {
		add_shortcode( 'inline_icons', array( $this, 'dima_shortcode_inline_icons' ) );
	}

	function dima_shortcode_inline_icons( $atts, $content = null ) {
		extract(
			shortcode_atts(
				array(
					'id'                     => '',
					'class'                  => '',
					'size'                   => '',//big,medium,small
					'circle'                 => 'false',//0,1
					'float'                  => '',//0,1
					'icon_style'             => '',
					'style'                  => '',
					'animation'              => '',
					'delay'                  => 30,
					'delay_offset'           => '98%',
					'delay_duration'         => 750,
					'data_dima_animate_item' => '.inline_item_icon',
				), $atts, 'inline_icons'
			)
		);

		$id     = ( $id != '' ) ? 'id="' . esc_attr( $id ) . '"' : '';
		$class  = ( $class != '' ) ? 'social-media ' . esc_attr( $class )
			: 'social-media';
		$class  .= ( $float == '' ) ? '' : ' ' . $float;
		$class  .= ( $icon_style != '' ) ? ' ' . $icon_style : '';
		$style  = ( $style != '' ) ? 'style="' . $style . '"' : '';
		$size   = ( $size != '' ) ? ' social-' . esc_attr( $size ) : ' social-medium';
		$circle = ( $circle == 'true' ) ? " circle-social" : '';


		$animation_data = '';
		if ( $animation != '' ) {
			$animation_data .= ( $animation != '' ) ? 'data-animate=' . $animation . '' : '';
			$animation_data .= ( $delay != '' ) ? ' data-delay=' . $delay . '' : ' data-delay="70" ';
			$animation_data .= ( $delay_offset != '' ) ? ' data-offset=' . $delay_offset . '' : '';
			$animation_data .= ( $delay_duration != '' ) ? ' data-duration=' . $delay_duration . '' : '';
			$animation_data .= ( $data_dima_animate_item ) ? ' data-dima-animate-item=' . $data_dima_animate_item . '' : ' data-dima-animate-item=".inline_item_icon"';
		}

		if ( $icon_style != 'outline-icon' ) {
			$class .= " dima_add_hover";
		}
		if ( $icon_style != 'outline-icon' ) {
			$class .= " fill-icon";
		}
		$output
			= "<div {$id} class=\"{$class}{$size}{$circle}\" {$animation_data} {$style}><ul class=\"inline clearfix\" >"
			  . do_shortcode( $content ) . "</ul></div>";

		return $output;
	}
}

new dima_shortcode_inline_icons();

//------------------------------------

class dima_shortcode_inline_item_icon {

	public function __construct() {
		add_shortcode( 'inline_item_icon', array( $this, 'dima_shortcode_inline_item_icon' ) );
	}

	function dima_shortcode_inline_item_icon( $atts, $content = null ) {
		extract(
			shortcode_atts(
				array(
					'id'               => '',
					'class'            => '',
					'style'            => '',
					'href'             => '',
					'title'            => '',
					'icon'             => '',
					'sicon_color'      => '',
					'icon_fontawesome' => '',
					'icon_svg'         => '',
					'icon_bg'          => '',
					'icon_color'       => '',
				), $atts, 'inline_item_icon'
			)
		);

		$tooltip_attr     = dima_helper::dima_tooltip_data( 'tooltip', 'hover', 'bottom' );
		$class            .= $sicon_color . '_icon inline_item_icon';
		$id               = ( $id != '' ) ? 'id="' . esc_attr( $id ) . '"' : '';
		$class            = ( $class != '' ) ? 'class="' . esc_attr( $class ) . '"'
			: '';
		$style            = ( $style != '' ) ? 'style="' . $style . '"' : '';
		$href             = ( $href != '' ) ? $href : '';
		$title            = ( $title != '' ) ? $title : '';
		$_icon            = ( $icon != '' ) ? $icon : '';
		$icon_svg         = ( $icon_svg != '' ) ? dima_get_svg_icon( $icon_svg ) : '';
		$icon_fontawesome = ( $icon_fontawesome != '' ) ? $icon_fontawesome : '';
		$_icon            = ( $_icon == '' ) ? $icon_fontawesome : $_icon;
		$icon_bg          = ( $icon_bg != '' ) ? 'background-color:' . $icon_bg . ';' : '';
		$icon_color       = ( $icon_color != '' ) ? 'color:' . $icon_color . ';' : '';
		$icon_style       = '';
		if ( $icon_bg != '' || $icon_color != '' ) {
			$icon_style = 'style="' . $icon_bg . ' ' . $icon_color . '"';
		}

		if ( ! empty( $icon_svg ) ) {
			$_icon = $icon_svg;
		} else if ( ! empty( $_icon ) ) {
			$_icon = '<i class="' . $_icon . '"></i>';
		}

		$output
			= "<li {$id} {$class} {$style} ><a {$icon_style} href=\"{$href}\" {$tooltip_attr} title=\"{$title}\">{$_icon}</a></li>";

		return $output;
	}
}

new dima_shortcode_inline_item_icon();

//------------------------------------

class dima_shortcode_icon {

	public function __construct() {
		add_shortcode( 'icon', array( $this, 'dima_shortcode_icon' ) );
	}

	function dima_shortcode_icon( $atts, $content = null ) {
		extract(
			shortcode_atts(
				array(
					'id'    => '',
					'class' => '',
					'style' => '',
					'href'  => '',
					'size'  => '',
					'type'  => '',
					'shape' => '',//square,circle
					'icon'  => ''
				), $atts, 'icon'
			)
		);

		$id    = ( $id != '' ) ? 'id="' . esc_attr( $id ) . '"' : '';
		$class = ( $class != '' ) ? esc_attr( $class ) : '';
		$style = ( $style != '' ) ? 'style="' . $style . '"' : '';
		$href  = ( $href != '' ) ? $href : '';
		$shape = ( $shape != '' ) ? 'box-' . esc_attr( $shape ) : 'box-circle';
		$size  = ( $size != '' ) ? ' icon-box-' . $size : ' icon-box-medium';
		$icon  = ( $icon != '' ) ? $icon : '';
		$space = ' ';

		if ( $type == "dima-icon" ) {
			$output
				= "<div class=\"box-square {$class}{$space}{$shape}\" {$style} >"
				  . "<header>"
				  . "<a href=\"$href\"><i {$id} class=\"{$icon}{$size}\" ></i></a>"
				  . "</header>"
				  . "</div>";
		} else {
			if ( ! empty( $href ) ) {
				$output
					= "<a href=\"$href\"><i {$id} class=\"{$class}{$space}{$icon}\" {$style}></a></i>";
			} else {
				$output
					= "<i {$id} class=\"{$class}{$space}{$icon}\" {$style}></i>";
			}
		}

		return $output;
	}
}

new dima_shortcode_icon();