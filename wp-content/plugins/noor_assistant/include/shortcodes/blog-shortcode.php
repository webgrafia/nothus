<?php
/*

Plugin Name: DIMA-Shortcodes
Plugin URI: http://pixeldima.com/
Version: 1.0.0
Author: PixelDima
Author URI: http://pixeldima.com/
Text Domain: dima-shortcodes

*/


/**
 * Class and Function List:
 * Function list:
 * - dima_shortcode_blog()
 * Classes list:
 */


class DIMA_RecentPosts {
private $query = '';
private $is_paging = true;

public function __construct() {
  add_shortcode( 'blog',array( $this, 'dima_shortcode_blog' ));
}

function dima_shortcode_blog( $atts ){

  extract( shortcode_atts( array(
    'id'               => '',
    'post_class'       => '',
    'style'            => '',
    'blog_style'       => 'standard',
    'count'            => 3,
    'column'           => 3,
    'show_meta'        => '',
    'img_hover'        => '',
    'elm_hover'        => '',
    'words'            => '',
    'items_margin'     => 'false',
    'dark'             => '',
    'category'         => '',
    'dots'             => '',
    'tag'              => '',
    'offset'           => '' ,
    'paging'           => 'false',
    'order'            => '',
    'show_image'       => 'true'
  ) , $atts, 'blog' ) );

  $template      = dima_helper::dima_get_template();
  $id            =( $id          != ''      ) ? 'id="' . esc_attr( $id ) . '"' : '';
  $style         =( $style       != ''      ) ? 'style="' . $style . '"' : '';
  $post_class    =( $post_class  != ''      ) ? ' ' . esc_attr( $post_class ) : '';
  $category      =( $category    != ''      ) ? $category : '';
  $tag           =( $tag         != ''      ) ? $tag : '';
  $elm_hover     =( $elm_hover   != ''      ) ? $elm_hover : '';
  $img_hover     =( $img_hover   != ''      ) ? $img_hover : '';
  $show_image    =( $show_image  == 'true'  ) ? true : false;
  $show_meta     =( $show_meta   == 'false' ) ? '0' : '1';
  $items_margin  =( $items_margin   == 'false' ) ? false : true;
  $paging        =( $paging      != 'true'  ) ? false : true;
  $dots          =( $dots        != 'true'  ) ? false : true;
  $words         =( $words       != ''      ) ? $words : '150';
  $column        =( $column      != ''      ) ? $column : 3;
  $count         =( $count       != ''      ) ? $count : 3;
  $order         =( $order       != ''      ) ? $order : '';

  $this->is_paging = $paging;

  if ( get_query_var( 'paged' ) ) { $paged = get_query_var( 'paged' ); }
    elseif ( get_query_var( 'page' ) ) { $paged = get_query_var( 'page' ); }
    else { $paged = 1; }

  if($offset != '' && $paging == true)
  {$offset = ( $paged - $offset ) * $count;}

  switch ($order) {
    case 'top-view':
    $order_array = array(
      'meta_key' => 'dima_post_views_count',
      'orderby' => 'meta_value_num',
      'order' => 'DESC');
      break;
    case 'popular':
    $order_array = array(
      'orderby' => 'comment_count',
      'order' => 'DESC');
      break;
    default:
      $order_array = array('orderby' => 'date');
    break;
  }

  if ( ! empty( $category ) ) {
	$category = dima_helper::dima_get_slug_by_ids( $category, 'term_id', 'category' );
  }

  if ( ! empty( $tag ) ) {
	$tag = dima_helper::dima_get_slug_by_ids( $tag, 'term_id', 'post_tag' );
  }

  //Search
  if(isset( $_GET['s'] )){
      $array_query =array(
        's'                 => "{$_GET['s']}",
        'paged'             => "{$paged}",
        'offset'            => "{$offset}",
        'category_name'     => "{$category}",
        'tag'               => "{$tag}",
      );
      $merge = array_merge($array_query, $order_array);
      $dima_query  = new WP_Query($merge);
  //Blog With page
  }elseif ($paging) {
    $array_query=array(
        'post_type'         => "post",
        'posts_per_page'    => "{$count}",
        'paged'             => "{$paged}",
        'offset'            => "{$offset}",
        'category_name'     => "{$category}",
        'tag'               => "{$tag}",
    );
    $merge = array_merge($array_query, $order_array);
    $dima_query  = new WP_Query($merge);
    $this->query = $dima_query;
  }else{
    $array_query = array(
        'post_type'         => "post",
        'posts_per_page'    => "{$count}",
        'offset'            => "{$offset}",
        'category_name'     => "{$category}",
        'tag'               => "{$tag}",
    );
    $merge = array_merge($array_query, $order_array);
    $dima_query  = new WP_Query($merge);
    $this->query = $dima_query;
  }

  if(is_archive()){
      global $wp_query;
      $dima_query  = $wp_query;
  }

  $ARG_ARRAY = array(
      'show_meta'   => $show_meta,
      'show_image'  => $show_image,
      'elm_hover'   => $elm_hover,
      'img_hover'   => $img_hover,
      'words'       => $words,
      'blog_style'  => $blog_style,
      'post_class'  => $post_class,

      'animation'              => 'transition.slideUpBigIn',
	  'delay'                  => 70,
	  'delay_offset'           => '98%',
	  'delay_duration'         => 750,
      'data-dima-animate-item' => '.post',
  );

  $clm = dima_helper::dima_get_clm($column);

  $POST_ARRAY = array(
      'template'      => $template,
      'blog_style'    => $blog_style,
      'column'        =>  $column,
      'clm'           =>  $clm,
      'count'         =>  $count,
      'pagination'    => 'true',
      'dots'          => $dots,
      'auto_play'     => 'true',
      'navigation'    => 'false',
      'loop'          => 'false',
      'items'         => $column,
      'items_phone'   => '',
      'items_tablet'  => '',
      'items_margin'  => $items_margin,
      'dark'         => $dark,
  );
  ob_start();
    $this->Blog_Style($POST_ARRAY,$dima_query,$ARG_ARRAY);
  return ob_get_clean();
}

public function Blog_Style($POST_ARRAY,$wp_query,$ARG_ARRAY){
 if (empty($wp_query)) {
    $wp_query = new WP_Query(array('orderby' => 'date'));
  }

  if ($POST_ARRAY['blog_style'] !='') {
      if ($POST_ARRAY['blog_style']=='grid') {
          $this->masonry($POST_ARRAY,$wp_query,$ARG_ARRAY,'dima-layout-grid');
      }else {
          $this->{$POST_ARRAY['blog_style']}($POST_ARRAY,$wp_query,$ARG_ARRAY);
      }
  }else{
    $this->standard($POST_ARRAY,$wp_query,$ARG_ARRAY);
  }
}

function masonry($POST_ARRAY,$wp_query,$ARG_ARRAY,$class_="dima-layout-masonry"){
    	$animation_data = '';
		$animation_data .= ( $ARG_ARRAY['animation'] != '' ) ? 'data-animate=' . $ARG_ARRAY['animation'] . '' : '';
		$animation_data .= ( $ARG_ARRAY['delay'] != '' ) ? ' data-delay=' . $ARG_ARRAY['delay'] . '' : '';
		$animation_data .= ( $ARG_ARRAY['delay_offset'] != '' ) ? ' data-offset=' . $ARG_ARRAY['delay_offset'] . '' : '';
		$animation_data .= ( $ARG_ARRAY['data-dima-animate-item'] != '' ) ? ' data-dima-animate-item=' . $ARG_ARRAY['data-dima-animate-item'] . '' : '';
		$animation_data .= ( $ARG_ARRAY['delay_duration'] != '' ) ? ' data-duration=' . $ARG_ARRAY['delay_duration'] . '' : '';

   if (is_archive() || is_home() || (is_singular() && is_page())) {
       $ARG_ARRAY['post_class'].=" isotope-item";
   }
  ?>
    <div class="boxed-blog blog-list <?php echo $class_?>" <?php  echo $animation_data; ?>>
      <div id="dima-isotope-container" class="dima-isotope-container isotope masonry boxed-protfolio columns-<?php echo $POST_ARRAY['column']; ?>">
      <?php
         if ($wp_query ->have_posts()):
           while ($wp_query ->have_posts()):
                 $wp_query->the_post();
                 dima_helper::dima_get_view_with_args($POST_ARRAY['template'], 'content', get_post_format(),$ARG_ARRAY);
           endwhile;
                wp_reset_postdata();
           else:
                dima_helper::dima_get_view('global', '_content-none');
           endif;
        ?>
      </div>
    </div>
  <?php
  if ($this->is_paging) {
     ob_start();
     dima_pagination($wp_query);
     $pagination = ob_get_contents();
     ob_get_clean();
     echo $pagination;
   }
     wp_reset_query();
}

function standard($POST_ARRAY,$wp_query,$ARG_ARRAY){
    	$animation_data = '';
		$animation_data .= ( $ARG_ARRAY['animation'] != '' ) ? 'data-animate=' . $ARG_ARRAY['animation'] . '' : '';
		$animation_data .= ( $ARG_ARRAY['delay'] != '' ) ? ' data-delay=' . $ARG_ARRAY['delay'] . '' : '';
		$animation_data .= ( $ARG_ARRAY['delay_offset'] != '' ) ? ' data-offset=' . $ARG_ARRAY['delay_offset'] . '' : '';
		$animation_data .= ( $ARG_ARRAY['data-dima-animate-item'] != '' ) ? ' data-dima-animate-item=' . $ARG_ARRAY['data-dima-animate-item'] . '' : '';
		$animation_data .= ( $ARG_ARRAY['delay_duration'] != '' ) ? ' data-duration=' . $ARG_ARRAY['delay_duration'] . '' : '';
    ?><div class="boxed-blog blog-list dima-layout-standard" <?php echo $animation_data; ?>><?php
    if ($wp_query ->have_posts()){

        while ($wp_query ->have_posts()):
            $wp_query->the_post();
            dima_helper::dima_get_view_with_args($POST_ARRAY['template'], 'content', get_post_format(),$ARG_ARRAY);

        endwhile;
        wp_reset_postdata();
    }else{
     dima_helper::dima_get_view('global', '_content-none');
    }
    ?></div><?php
    if ($this->is_paging) {
          ob_start();
          dima_pagination($wp_query);
          $pagination = ob_get_contents();
          ob_get_clean();
          echo $pagination;
        }
     wp_reset_query();
}

function minimal($POST_ARRAY,$wp_query,$ARG_ARRAY){
        $animation_data = '';
		$animation_data .= ( $ARG_ARRAY['animation'] != '' ) ? 'data-animate=' . $ARG_ARRAY['animation'] . '' : '';
		$animation_data .= ( $ARG_ARRAY['delay'] != '' ) ? ' data-delay=' . $ARG_ARRAY['delay'] . '' : '';
		$animation_data .= ( $ARG_ARRAY['delay_offset'] != '' ) ? ' data-offset=' . $ARG_ARRAY['delay_offset'] . '' : '';
		$animation_data .= ( $ARG_ARRAY['data-dima-animate-item'] != '' ) ? ' data-dima-animate-item=' . $ARG_ARRAY['data-dima-animate-item'] . '' : '';
		$animation_data .= ( $ARG_ARRAY['delay_duration'] != '' ) ? ' data-duration=' . $ARG_ARRAY['delay_duration'] . '' : '';

    $root_class=' dima-minimal-style';
	$ARG_ARRAY['post_class'].= $POST_ARRAY['clm'] ;
	if (is_archive() || is_home() || (is_singular() && is_page())) {
       $ARG_ARRAY['post_class'].=" isotope-item";
    }
	if(!empty($ARG_ARRAY['no_margin'])){
	   $root_class.= " minimal_no_margin";
	}
    ?>
    <div id="dima-isotope-container" class="boxed-blog blog-list dima-isotope-container isotope masonry boxed-protfolio columns-<?php echo $POST_ARRAY['column'].$root_class; ?>" <?php echo $animation_data; ?>>
    <?php
    if ($wp_query ->have_posts()){
        while ($wp_query ->have_posts()):
            $wp_query->the_post();
        dima_helper::dima_get_view_with_args($POST_ARRAY['template'], 'content', 'minimal-post-grid',$ARG_ARRAY);
        endwhile;
        wp_reset_postdata();
    }else{
     dima_helper::dima_get_view('global', '_content-none');
    }?>
    </div>
    <?php
    if ($this->is_paging) {
          ob_start();
          dima_pagination($wp_query);
          $pagination = ob_get_contents();
          ob_get_clean();
          echo $pagination;
        }
    wp_reset_query();
}

function minimal_no_margin($POST_ARRAY,$wp_query,$ARG_ARRAY){

        $animation_data = '';
		$animation_data .= ( $ARG_ARRAY['animation'] != '' ) ? 'data-animate=' . $ARG_ARRAY['animation'] . '' : '';
		$animation_data .= ( $ARG_ARRAY['delay'] != '' ) ? ' data-delay=' . $ARG_ARRAY['delay'] . '' : '';
		$animation_data .= ( $ARG_ARRAY['delay_offset'] != '' ) ? ' data-offset=' . $ARG_ARRAY['delay_offset'] . '' : '';
		$animation_data .= ( $ARG_ARRAY['data-dima-animate-item'] != '' ) ? ' data-dima-animate-item=' . $ARG_ARRAY['data-dima-animate-item'] . '' : '';
		$animation_data .= ( $ARG_ARRAY['delay_duration'] != '' ) ? ' data-duration=' . $ARG_ARRAY['delay_duration'] . '' : '';

    $root_class=' dima-minimal-style minimal_no_margin';
	$ARG_ARRAY['post_class'].= $POST_ARRAY['clm'] ;

    ?>
    <div class="<?php echo $root_class. " clm-" . $POST_ARRAY['column']; ?> boxed-blog blog-list" <?php echo $animation_data; ?>>
    <?php
    if ($wp_query ->have_posts()){
        while ($wp_query ->have_posts()):
            $wp_query->the_post();
        dima_helper::dima_get_view_with_args($POST_ARRAY['template'], 'content', 'minimal-post-grid',$ARG_ARRAY);
        endwhile;
        wp_reset_postdata();
    }else{
     dima_helper::dima_get_view('global', '_content-none');
    }?>
    </div>
    <?php
    if ($this->is_paging) {
          ob_start();
          dima_pagination($wp_query);
          $pagination = ob_get_contents();
          ob_get_clean();
          echo $pagination;
        }
    wp_reset_query();

}

function minimalslide($POST_ARRAY,$wp_query,$ARG_ARRAY){
        $animation_data = '';
		$animation_data .= ( $ARG_ARRAY['animation'] != '' ) ? 'data-animate=' . $ARG_ARRAY['animation'] . '' : '';
		$animation_data .= ( $ARG_ARRAY['delay'] != '' ) ? ' data-delay=' . $ARG_ARRAY['delay'] . '' : '';
		$animation_data .= ( $ARG_ARRAY['delay_offset'] != '' ) ? ' data-offset=' . $ARG_ARRAY['delay_offset'] . '' : '';
		$animation_data .= ( $ARG_ARRAY['data-dima-animate-item'] != '' ) ? ' data-dima-animate-item=' . $ARG_ARRAY['data-dima-animate-item'] . '' : '';
		$animation_data .= ( $ARG_ARRAY['delay_duration'] != '' ) ? ' data-duration=' . $ARG_ARRAY['delay_duration'] . '' : '';

    $js_data = array(
      'dots'             => ( $POST_ARRAY['dots']               == 'true' ),
      'autoplay'         => ( $POST_ARRAY['auto_play']          == 'false' ),
      'arrows'           => false,
      'infinite'         => ( $POST_ARRAY['loop']               == 'true' ),
      'slidesToShow'     => ( $POST_ARRAY['items']              == '' )?  1:$POST_ARRAY['items'],
      'items_phone'      => ( $POST_ARRAY['items_phone']        == '' )?  1:$POST_ARRAY['items_phone'],
      'items_tablet'     => ( $POST_ARRAY['items_tablet']       == '' )?  2:$POST_ARRAY['items_tablet'],
   	  'rtl'              => is_rtl()
    );
     $slick_class ='';
     if($POST_ARRAY['dark']){
         $slick_class .='slick-darck';
     }
     $data = dima_creat_data_attributes( 'slick_slider', $js_data );

     if($POST_ARRAY['items_margin']){
	   $slick_class.= " slick-no-margin";
	}else{
        $slick_class.= " slick-with-margin";
	 }
    ?>

    <div class="<?php echo $slick_class. " clm-" . $POST_ARRAY['column'];  ?> dima-minimal-style boxed-blog blog-list" <?php echo "$data"; ?> <?php echo $animation_data; ?>>
    <?php
    if ($wp_query ->have_posts()){
        while ($wp_query ->have_posts()):
            $wp_query->the_post();
        dima_helper::dima_get_view_with_args($POST_ARRAY['template'], 'content', 'minimal-post-grid',$ARG_ARRAY);
        endwhile;
        wp_reset_postdata();
    }else{
     dima_helper::dima_get_view('global', '_content-none');
    }?>
    </div>
    <?php
    if ($this->is_paging) {
          ob_start();
          dima_pagination($wp_query);
          $pagination = ob_get_contents();
          ob_get_clean();
          echo $pagination;
        }
    wp_reset_query();

}

function slide($POST_ARRAY,$wp_query,$ARG_ARRAY){

    $js_data = array(
      'dots'             => ( $POST_ARRAY['dots']               == 'true' ),
      'autoplay'         => ( $POST_ARRAY['auto_play']          == 'false' ),
      'arrows'           => false,
      'infinite'         => ( $POST_ARRAY['loop']               == 'true' ),
      'slidesToShow'     => ( $POST_ARRAY['items']              == '' )?  1:$POST_ARRAY['items'],
      'items_phone'      => ( $POST_ARRAY['items_phone']        == '' )?  1:$POST_ARRAY['items_phone'],
      'items_tablet'     => ( $POST_ARRAY['items_tablet']       == '' )?  2:$POST_ARRAY['items_tablet'],
   	  'rtl'               => is_rtl()
    );

     $slick_class ='' . $POST_ARRAY['dark'] . ' ';
     $data = dima_creat_data_attributes( 'slick_slider', $js_data );

     if($POST_ARRAY['items_margin']){
	   $slick_class.= " slick-no-margin";
	 }else{
        $slick_class.= " slick-with-margin";
	 }
    ?>

    <div class="<?php echo $slick_class; ?> boxed-blog blog-lis dima-layout-slide" <?php echo "$data"; ?>>
    <?php
    if ($wp_query ->have_posts()){
        while ($wp_query ->have_posts()):
            $wp_query->the_post();
                 dima_helper::dima_get_view_with_args($POST_ARRAY['template'], 'content', get_post_format(),$ARG_ARRAY);
        endwhile;
        wp_reset_postdata();
    }else{
     dima_helper::dima_get_view('global', '_content-none');
    }?>
    </div>
    <?php
    if ($this->is_paging) {
          ob_start();
          dima_pagination($wp_query);
          $pagination = ob_get_contents();
          ob_get_clean();
          echo $pagination;
        }
    wp_reset_query();

}

function timeline($POST_ARRAY,$wp_query,$ARG_ARRAY){
    	$animation_data = '';
		$animation_data .= ( $ARG_ARRAY['animation'] != '' ) ? 'data-animate=' . $ARG_ARRAY['animation'] . '' : '';
		$animation_data .= ( $ARG_ARRAY['delay'] != '' ) ? ' data-delay=' . $ARG_ARRAY['delay'] . '' : '';
		$animation_data .= ( $ARG_ARRAY['delay_offset'] != '' ) ? ' data-offset=' . $ARG_ARRAY['delay_offset'] . '' : '';
		$animation_data .= ( $ARG_ARRAY['data-dima-animate-item'] != '' ) ? ' data-dima-animate-item=' . $ARG_ARRAY['data-dima-animate-item'] . '' : '';
		$animation_data .= ( $ARG_ARRAY['delay_duration'] != '' ) ? ' data-duration=' . $ARG_ARRAY['delay_duration'] . '' : '';

  ?>
  <div class="dima-timeline clearfix dima-timeline-blog boxed-blog blog-list" <?php echo $animation_data; ?>>
  <div class="h-line"></div>
  <?php
  if ($wp_query ->have_posts()):
      $i = 2;
      while ($wp_query ->have_posts()): $wp_query->the_post();
      if ($i %2 == 0): ?>
        <div class="timeline_element_start ">
        <?php else: ?>
          <div class="timeline_element_end ">
        <?php endif ?>
        <?php
        dima_helper::dima_get_view_with_args($POST_ARRAY['template'], 'content', 'minimal-post',$ARG_ARRAY);
        ?>
      </div>
      <?php
      $i++;
      endwhile;
      wp_reset_postdata();
      ?>
      </div>
      <?php
      else:
        dima_helper::dima_get_view('global', '_content-none');
      endif;
        if ($this->is_paging) {
          ob_start();
          dima_pagination($wp_query);
          $pagination = ob_get_contents();
          ob_get_clean();
          echo $pagination;
        }
     wp_reset_query();
}

/**
* @param $POST_ARRAY
* @param $wp_query
* @param $ARG_ARRAY
 * We use it in popular widget.
 */
function small_pos_tslist($POST_ARRAY,$wp_query,$ARG_ARRAY){
    ?><div class="vertical-posts-list boxed-blog blog-list"><?php
    if ($wp_query ->have_posts()){
         ?>
         <ul class="posts-list no-box with-border first">
        <?php
        while ($wp_query ->have_posts()):
            $wp_query->the_post();
            dima_helper::dima_get_view_with_args($POST_ARRAY['template'], 'content','list',$ARG_ARRAY);

        endwhile;
         ?>
         </ul>
        <?php
        wp_reset_postdata();
    }else{
     dima_helper::dima_get_view('global', '_content-none');
    }
    ?></div><?php
    if ($this->is_paging) {
          ob_start();
          dima_pagination($wp_query);
          $pagination = ob_get_contents();
          ob_get_clean();
          echo $pagination;
        }
    wp_reset_query();
}

function search($POST_ARRAY,$wp_query,$ARG_ARRAY){
    ?><div class="boxed-blog blog-list dima-layout-search"><?php
    if ($wp_query ->have_posts()){

        while ($wp_query ->have_posts()):
            $wp_query->the_post();
            dima_helper::dima_get_view_with_args($POST_ARRAY['template'], 'content', 'search',$ARG_ARRAY);

        endwhile;
        wp_reset_postdata();
    }else{
     dima_helper::dima_get_view('global', '_content-none');
    }
    ?></div><?php
    if ($this->is_paging) {
          ob_start();
          dima_pagination($wp_query);
          $pagination = ob_get_contents();
          ob_get_clean();
          echo $pagination;
        }
     wp_reset_query();
}
}
new DIMA_RecentPosts();
?>