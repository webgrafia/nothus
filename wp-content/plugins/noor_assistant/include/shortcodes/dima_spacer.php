<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

function dima_shortcode_dima_spacer( $atts ) {

	$output = $data_atts = $units = '';
	$atts   = vc_map_get_attributes( 'dima_spacer', $atts );
	extract( $atts );

	$id        = ( $id != '' ) ? 'id="' . esc_attr( $id ) . '"' : '';
	$data_atts .= ' data-units="' . esc_attr( $units ) . '"';

	$data_atts .= ' data-all_size="' . esc_attr( $screen_all_spacer_size ) . '"';

	$data_atts .= ' data-xld_resolution="' . esc_attr( $screen_xld_resolution ) . '"';
	$data_atts .= ' data-xld_size="' . esc_attr( $screen_xld_spacer_size ) . '"';

	$data_atts .= ' data-ld_resolution="' . esc_attr( $screen_ld_resolution ) . '"';
	$data_atts .= ' data-ld_size="' . esc_attr( $screen_ld_spacer_size ) . '"';

	$data_atts .= ' data-md_resolution="' . esc_attr( $screen_md_resolution ) . '"';
	$data_atts .= ' data-md_size="' . esc_attr( $screen_md_spacer_size ) . '"';

	$data_atts .= ' data-sd_resolution="' . esc_attr( $screen_sd_resolution ) . '"';
	$data_atts .= ' data-sd_size="' . esc_attr( $screen_sd_spacer_size ) . '"';

	$data_atts .= ' data-xsd_resolution="' . esc_attr( $screen_xsd_resolution ) . '"';
	$data_atts .= ' data-xsd_size="' . esc_attr( $screen_xsd_spacer_size ) . '"';

	$output .= '<div ' . $id . ' class="dima-spacer-module" ' . $data_atts . ' style="height: ' . esc_attr( $screen_all_spacer_size ) . 'px;"></div>';

	return $output;
}

add_shortcode( 'dima_spacer', 'dima_shortcode_dima_spacer' );