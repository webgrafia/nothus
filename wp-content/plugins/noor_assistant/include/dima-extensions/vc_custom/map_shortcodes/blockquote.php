<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/*
* Add-on Name: Blockquote
*/

class WPBakeryShortCode_Blockquote extends WPBakeryShortCode {
}

/**
 * Blockquote
 */
vc_map(
	array(
		'base'        => 'blockquote',
		'name'        => esc_html__( 'Blockquote', 'noor-assistant' ),
		'weight'      => 810,
		'class'       => 'dima-vc-element dima-vc-element-blockquote',
		'icon'        => 'blockquote',
		'category'    => esc_html__( 'Typography', 'noor-assistant' ),
		'description' => esc_html__( 'Include a blockquote in your content', 'noor-assistant' ),
		'params'      => array(
			array(
				'param_name'  => 'content',
				'heading'     => esc_html__( 'Text', 'noor-assistant' ),
				'description' => esc_html__( 'Enter your text.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textarea_html',
				'holder'      => 'div',
				'value'       => ''
			),
			array(
				'param_name'  => 'icon',
				'heading'     => esc_html__( 'Hide quotre icon', 'noor-assistant' ),
				'description' => esc_html__( 'Check to hide Quote icon', 'noor-assistant' ),
				'type'        => 'checkbox',
				'holder'      => 'div',
				'value'       => array(
					'' => 'false'
				)
			),
			array(
				'param_name'  => 'cite',
				'heading'     => esc_html__( 'Cite', 'noor-assistant' ),
				'description' => esc_html__( 'Cite the person you are quoting.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'type',
				'heading'     => esc_html__( 'Blockquote Style', 'noor-assistant' ),
				'description' => esc_html__( 'Select blockquote style ', 'noor-assistant' ),
				'type'        => 'dropdown',
				'holder'      => 'div',
				'value'       => array(
					esc_html__( '- Select Option -', 'noor-assistant' ) => '',
					esc_html__( 'Inherit', 'noor-assistant' )           => '',
					esc_html__( 'Pull', 'noor-assistant' )              => 'pull',
					esc_html__( 'Simple', 'noor-assistant' )            => 'sample',
				)
			),
			array(
				'param_name'  => 'direction',
				'heading'     => esc_html__( 'Alignment', 'noor-assistant' ),
				'description' => esc_html__( 'Select the alignment of the blockquote contents.', 'noor-assistant' ),
				'type'        => 'dropdown',
				'holder'      => 'div',
				'value'       => array(
					esc_html__( '- Select Option -', 'noor-assistant' ) => '',
					esc_html__( 'Start', 'noor-assistant' )             => 'start',
					esc_html__( 'Center', 'noor-assistant' )            => 'center',
					esc_html__( 'End', 'noor-assistant' )               => 'end'
				)
			),
			array(
				'param_name'  => 'id',
				'heading'     => esc_html__( 'ID', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter a unique ID.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'class',
				'heading'     => esc_html__( 'Class', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter a unique class name.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'style',
				'heading'     => esc_html__( 'Style', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter inline CSS.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'tutorials',
				'type'        => 'dima_doc_link_param',
				'heading'     => '<span class="tippy dima-help dima-vc-toolip">' . dima_get_svg_icon( "ic_help" )
				                 . '<span class="dima-vc-tooltip-text">'
				                 . esc_html__( 'Video tutorial and theme documentation article', 'noor-assistant' ) . '</span></span>'
				                 . esc_html__( 'Tutorial', 'noor-assistant' ),
				'doc_link'    => '',
				'video_link'  => 'https://www.youtube.com/watch?v=bfAXkcvg1cY&index=9&list=PLnqcQSxXL3f2bEptqV0P2eteIxZi62Ynv',
				'doc_example' => 'https://noor.pixeldima.com/blockquote/',
			),
		)
	)
);