<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/*
* Add-on Name: divider
*/

class WPBakeryShortCode_Divider extends WPBakeryShortCode {
}

/**
 * Divider.
 */
vc_map(
	array(
		'base'        => 'divider',
		'name'        => esc_html__( 'Divider', 'noor-assistant' ),
		'weight'      => 980,
		'class'       => 'dima-vc-element dima-vc-element-divider',
		'icon'        => 'divider',
		'category'    => esc_html__( 'Structure', 'noor-assistant' ),
		'description' => esc_html__( 'Place a horizontal divider in your content', 'noor-assistant' ),
		'params'      => array(
			array(
				'param_name'  => 'divider_style',
				'heading'     => esc_html__( 'Divider Style', 'noor-assistant' ),
				'description' => esc_html__( 'Select the Position of the divider and icon.', 'noor-assistant' ),
				'type'        => 'dropdown',
				'save_always' => true,
				'value'       => array(
					esc_html__( 'Line', 'noor-assistant' )       => 'line',
					esc_html__( 'Icon', 'noor-assistant' )       => 'icon',
					esc_html__( 'Noor Style', 'noor-assistant' ) => 'noor_divider',
				),
				'holder'      => 'div'
			),

			array(
				'param_name'  => 'direction',
				'heading'     => esc_html__( 'Divider Position', 'noor-assistant' ),
				'description' => esc_html__( 'Select the Position of the divider and icon.', 'noor-assistant' ),
				'type'        => 'dropdown',
				'save_always' => true,
				'value'       => array(
					esc_html__( 'Select Option', 'noor-assistant' ) => '',
					esc_html__( 'Start', 'noor-assistant' )         => 'start',
					esc_html__( 'Center', 'noor-assistant' )        => 'center',
					esc_html__( 'End', 'noor-assistant' )           => 'end',
				),
				'dependency'  => array(
					'element' => 'divider_style',
					'value'   => array( 'line', 'noor_divider' ),
				),
				'holder'      => 'div'
			),

			array(
				'param_name'  => 'line_color',
				'heading'     => esc_html__( 'Line Color', 'noor-assistant' ),
				'description' => esc_html__( 'Choose Line color', 'noor-assistant' ),
				'type'        => 'colorpicker',
				'dependency'  => array(
					'element' => 'divider_style',
					'value'   => 'line',
				),
				'holder'      => 'div'
			),

			array(
				'param_name'  => 'icon_color',
				'heading'     => esc_html__( 'Icon Color', 'noor-assistant' ),
				'description' => esc_html__( 'Choose Icon color', 'noor-assistant' ),
				'type'        => 'colorpicker',
				'dependency'  => array(
					'element' => 'divider_style',
					'value'   => 'icon',
				),
				'holder'      => 'div'
			),

			array(
				'type'        => 'dropdown',
				'heading'     => esc_html__( 'Icons library', 'noor-assistant' ),
				'value'       => array(
					esc_html__( 'Font Awesome', 'noor-assistant' )                                     => 'fontawesome',
					esc_html__( 'Custom Font', 'noor-assistant' )                                      => 'customfont',
					esc_html__( 'SVG Icons (Stroke Gap Icons And Material Icons) ', 'noor-assistant' ) => 'svgicon',
				),
				'param_name'  => 'icon_type',
				'description' => esc_html__( 'Select icons library.', 'noor-assistant' ),
				'dependency'  => array(
					'element' => 'divider_style',
					'value'   => array( 'icon' ),
				)
			),
			array(
				'param_name'  => 'icon_svg',
				'heading'     => esc_html__( 'Icon SVG', 'noor-assistant' ),
				'description' => esc_html__( 'Enter SVG Icon name', 'noor-assistant' ),
				'value'       => '',
				'dependency'  => array(
					'element' => 'icon_type',
					'value'   => 'svgicon',
				),
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'icon',
				'heading'     => esc_html__( 'Icon Class', 'noor-assistant' ),
				'description' => esc_html__( 'Enter Icon Class from Font Awesome or Simple Line Icons.', 'noor-assistant' ),
				'value'       => 'simple-icon-user',
				'dependency'  => array(
					'element' => 'icon_type',
					'value'   => 'customfont',
				),
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'type'        => 'iconpicker',
				'heading'     => esc_html__( 'Icon', 'noor-assistant' ),
				'param_name'  => 'icon_fontawesome',
				'value'       => 'simple-icon-user', // default value to backend editor admin_label
				'settings'    => array(
					'emptyIcon'    => true,
					// default true, display an "EMPTY" icon?
					'value'        => 'fontawesome',
					'iconsPerPage' => 4000,
					// default 100, how many icons per/page to display, we use (big number) to display all icons in single page
				),
				'dependency'  => array(
					'element' => 'icon_type',
					'value'   => 'fontawesome',
				),
				'description' => esc_html__( 'Select icon from library.', 'noor-assistant' ),
			),
			array(
				'param_name'  => 'style',
				'heading'     => esc_html__( 'Style', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter inline CSS.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'id',
				'heading'     => esc_html__( 'ID', 'noor-assistant' ),
				'description' => $dima_vc->id_des,
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'class',
				'heading'     => esc_html__( 'Class', 'noor-assistant' ),
				'description' => $dima_vc->class_des,
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			//**************Animation***************//
			array(
				'param_name'  => 'animation',
				'group'       => esc_html__( 'Animation', 'noor-assistant' ),
				'heading'     => esc_html__( 'Animation', 'noor-assistant' ),
				'description' => esc_html__( 'Select the type of animation you want to use.', 'noor-assistant' ),
				'type'        => 'dropdown',
				'holder'      => 'div',
				'value'       => $dima_vc->dima_animate_list_velocity,
			),

			array(
				'param_name'  => 'delay',
				'group'       => esc_html__( 'Animation', 'noor-assistant' ),
				'heading'     => esc_html__( 'Delay Animation', 'noor-assistant' ),
				'description' => esc_html__( 'Set when the animation start (milliseconds)', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div',
				'value'       => ''
			),

			array(
				'param_name'  => 'delay_duration',
				'group'       => esc_html__( 'Animation', 'noor-assistant' ),
				'heading'     => esc_html__( 'duration Animation', 'noor-assistant' ),
				'description' => esc_html__( 'Animation duration of each element  (milliseconds).', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div',
				'value'       => ''
			),

			array(
				'param_name'  => 'delay_offset',
				'group'       => esc_html__( 'Animation', 'noor-assistant' ),
				'heading'     => esc_html__( 'Delay Animation Offset', 'noor-assistant' ),
				'description' => esc_html__( 'Set the number ( px,%..) from the top of the viewport where the animation start.', 'noor-assistant' ) . "<a href='http://imakewebthings.com/waypoints/api/offset-option/' target='_blank' >" . esc_html__( 'Read More', 'noor-assistant' ) . "</a>",
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div',
				'value'       => ''
			),
			//**************!Animation***************//
			array(
				'param_name'  => 'tutorials',
				'type'        => 'dima_doc_link_param',
				'heading'     => '<span class="tippy dima-help dima-vc-toolip">' . dima_get_svg_icon( "ic_help" )
				                 . '<span class="dima-vc-tooltip-text">'
				                 . esc_html__( 'Video tutorial and theme documentation article', 'noor-assistant' ) . '</span></span>'
				                 . esc_html__( 'Tutorial', 'noor-assistant' ),
				'doc_link'    => '',
				'video_link'  => 'https://www.youtube.com/watch?v=j27nhLT_njc',
				'doc_example' => 'https://noor.pixeldima.com/separator/',
			),
		)
	)
);