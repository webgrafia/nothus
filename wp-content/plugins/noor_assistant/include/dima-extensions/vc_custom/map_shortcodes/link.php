<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/*
* Add-on Name: Link
*/

class WPBakeryShortCode_Link extends WPBakeryShortCode {
}

/**
 * Link
 */
vc_map(
	array(
		'base'        => 'link',
		'name'        => esc_html__( 'link', 'noor-assistant' ),
		'weight'      => 700,
		'class'       => 'dima-vc-element dima-vc-element-link',
		'icon'        => 'link',
		'category'    => esc_html__( 'Media', 'noor-assistant' ),
		'description' => esc_html__( 'Add text link to your content', 'noor-assistant' ),
		'params'      => array(
			array(
				'param_name'  => 'content',
				'heading'     => esc_html__( 'Text', 'noor-assistant' ),
				'description' => esc_html__( 'Enter your text.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textarea_html',
				'holder'      => 'div',
				'value'       => ''
			),
			array(
				'param_name'  => 'href',
				'heading'     => esc_html__( 'URL Link', 'noor-assistant' ),
				'description' => esc_html__( 'Enter in the URL you want your link to link to.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'target',
				'heading'     => esc_html__( 'Open link in a new tab', 'noor-assistant' ),
				'description' => esc_html__( 'Check to open your image link in a new tab.', 'noor-assistant' ),
				'type'        => 'checkbox',
				'holder'      => 'div',
				'value'       => array(
					'' => 'blank'
				)
			),
			array(
				'param_name'  => 'link_style',
				'heading'     => esc_html__( 'Link Style', 'noor-assistant' ),
				'description' => esc_html__( 'Select your link style', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'dropdown',
				'value'       => array(
					esc_html__( '- Select Option -', 'noor-assistant' ) => '',
					esc_html__( 'None', 'noor-assistant' )              => '',
					esc_html__( 'Solid', 'noor-assistant' )             => 'solid',
					esc_html__( 'Dashed', 'noor-assistant' )            => 'dashed',
					esc_html__( 'Dotted', 'noor-assistant' )            => 'dotted',
				),
				'holder'      => 'div'
			),

			array(
				'param_name'  => 'lightbox',
				'heading'     => esc_html__( 'Lightbox Type', 'noor-assistant' ),
				'description' => esc_html__( 'Select your lightbox type (iframe: for youtube, map,..)', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'dropdown',
				'value'       => array(
					esc_html__( '- Select Option -', 'noor-assistant' ) => '',
					'None'                                              => '',
					'image'                                             => 'image',
					'iframe'                                            => 'iframe',
					'ajax'                                              => 'ajax'
				),
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'popup_type',
				'heading'     => esc_html__( 'Popup Info Type', 'noor-assistant' ),
				'description' => esc_html__( 'Select to choose the type of popup info', 'noor-assistant' ),
				'type'        => 'dropdown',
				'holder'      => 'div',
				'value'       => array(
					esc_html__( '- Select Option -', 'noor-assistant' ) => '',
					'None'                                              => '',
					'Popover'                                           => 'popover',
					'Tooltip'                                           => 'tooltip'
				)
			),
			array(
				'param_name'  => 'title',
				'heading'     => esc_html__( 'Popup Info Title', 'noor-assistant' ),
				'description' => esc_html__( 'Enter in the title attribute for your URL (This will apply as title for popover or tooltip if you have active it from -Popup Info type-).', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'popup_trigger',
				'heading'     => esc_html__( 'Popup Trigger', 'noor-assistant' ),
				'description' => esc_html__( 'Select what actions you want to trigger the popover or tooltip.', 'noor-assistant' ),
				'type'        => 'dropdown',
				'holder'      => 'div',
				'value'       => array(
					esc_html__( '- Select Option -', 'noor-assistant' ) => '',
					'Hover'                                             => 'hover',
					'Click'                                             => 'click',
					'Focus'                                             => 'focus'
				)
			),
			array(
				'param_name'  => 'popup_place',
				'heading'     => esc_html__( 'Popup Position', 'noor-assistant' ),
				'description' => esc_html__( 'Select the popup position.', 'noor-assistant' ),
				'type'        => 'dropdown',
				'holder'      => 'div',
				'value'       => array(
					esc_html__( '- Select Option -', 'noor-assistant' ) => '',
					'Top'                                               => 'top',
					'Left'                                              => 'left',
					'Right'                                             => 'right',
					'Bottom'                                            => 'bottom'
				)
			),
			array(
				'param_name'  => 'popup_content',
				'heading'     => esc_html__( 'Info Content', 'noor-assistant' ),
				'description' => esc_html__( 'Extra content for the popover.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'id',
				'heading'     => esc_html__( 'ID', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter a unique ID.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'class',
				'heading'     => esc_html__( 'Class', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter a unique class name.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'style',
				'heading'     => esc_html__( 'Style', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter inline CSS.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
		)
	)
);