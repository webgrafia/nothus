<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/*
* Add-on Name: Counter
*/

class WPBakeryShortCode_Counter extends WPBakeryShortCode {
}

/**
 * Counter.
 */
vc_map(
	array(
		'base'        => 'counter',
		'name'        => esc_html__( 'Counter', 'noor-assistant' ),
		'weight'      => 960,
		'class'       => 'dima-vc-element dima-vc-element-item_icon',
		'icon'        => 'counter',
		'category'    => esc_html__( 'Structure', 'noor-assistant' ),
		'description' => esc_html__( 'Add counters to your page', 'noor-assistant' ),
		'params'      => array(
			array(
				'param_name'  => 'float',
				'heading'     => esc_html__( 'Counter Contents Position', 'noor-assistant' ),
				'description' => esc_html__( 'Optionally choose the position of counter contents.', 'noor-assistant' ),
				'type'        => 'dropdown',
				'holder'      => 'div',
				'value'       => array(
					esc_html__( 'Center', 'noor-assistant' ) => 'center',
					esc_html__( 'Start', 'noor-assistant' )  => 'start',
					esc_html__( 'End', 'noor-assistant' )    => 'end',
				)
			),

			array(
				'param_name'  => 'icon_type',
				'heading'     => esc_html__( 'Icons library', 'noor-assistant' ),
				'type'        => 'dropdown',
				'value'       => array(
					esc_html__( 'Font Awesome', 'noor-assistant' )                                     => 'fontawesome',
					esc_html__( 'Custom Font', 'noor-assistant' )                                      => 'customfont',
					esc_html__( 'SVG Icons (Stroke Gap Icons And Material Icons) ', 'noor-assistant' ) => 'svgicon',
				),
				'description' => esc_html__( 'Select icons library.', 'noor-assistant' ),
			),
			array(
				'param_name'  => 'icon',
				'heading'     => esc_html__( 'Icon Class', 'noor-assistant' ),
				'description' => esc_html__( 'Enter Icon Class from Font Awesome or Simple Line Icons.', 'noor-assistant' ),
				'value'       => 'simple-icon-user',
				'dependency'  => array(
					'element' => 'icon_type',
					'value'   => 'customfont',
				),
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'type'        => 'iconpicker',
				'heading'     => esc_html__( 'Icon', 'noor-assistant' ),
				'param_name'  => 'icon_fontawesome',
				'value'       => 'simple-icon-user', // default value to backend editor admin_label
				'settings'    => array(
					'emptyIcon'    => true,
					// default true, display an "EMPTY" icon?
					'value'        => 'fontawesome',
					'iconsPerPage' => 4000,
					// default 100, how many icons per/page to display, we use (big number) to display all icons in single page
				),
				'dependency'  => array(
					'element' => 'icon_type',
					'value'   => 'fontawesome',
				),
				'description' => esc_html__( 'Select icon from library.', 'noor-assistant' ),
			),
			array(
				'param_name'  => 'icon_svg',
				'heading'     => esc_html__( 'Icon SVG', 'noor-assistant' ),
				'description' => esc_html__( 'Enter SVG Icon name', 'noor-assistant' ),
				'value'       => '',
				'dependency'  => array(
					'element' => 'icon_type',
					'value'   => 'svgicon',
				),
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'icon_color',
				'group'       => esc_html__( 'Style', 'noor-assistant' ),
				'heading'     => esc_html__( 'Counter Icon Color', 'noor-assistant' ),
				'description' => esc_html__( 'Choose the icon color', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'colorpicker',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'bg_color',
				'group'       => esc_html__( 'Style', 'noor-assistant' ),
				'heading'     => esc_html__( 'Background Color', 'noor-assistant' ),
				'description' => esc_html__( 'Choose the background color', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'colorpicker',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'num_start',
				'heading'     => esc_html__( 'Counter Starting Number', 'noor-assistant' ),
				'description' => esc_html__( 'Set the counter value to start from', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'num_end',
				'heading'     => esc_html__( 'Counter Ending Number', 'noor-assistant' ),
				'description' => esc_html__( 'Set the counter value to end in', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'num_color',
				'group'       => esc_html__( 'Style', 'noor-assistant' ),
				'heading'     => esc_html__( 'Counter Number Color', 'noor-assistant' ),
				'description' => esc_html__( 'Choose the counter number\'s color', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'colorpicker',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'text',
				'heading'     => esc_html__( 'Counter Description', 'noor-assistant' ),
				'description' => esc_html__( 'Add description text', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'text_color',
				'group'       => esc_html__( 'Style', 'noor-assistant' ),
				'heading'     => esc_html__( 'Counter Description Color', 'noor-assistant' ),
				'description' => esc_html__( 'Choose the description\'s color', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'colorpicker',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'num_speed',
				'heading'     => esc_html__( 'Counter Speed', 'noor-assistant' ),
				'description' => esc_html__( 'Set the counter speed(ms)', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'num_before',
				'heading'     => esc_html__( 'Text Before the Counter Number', 'noor-assistant' ),
				'description' => esc_html__( 'Add text before the number', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'num_after',
				'heading'     => esc_html__( 'Text After the Counter Number', 'noor-assistant' ),
				'description' => esc_html__( 'Add text after the number', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'border',
				'group'       => esc_html__( 'Style', 'noor-assistant' ),
				'heading'     => esc_html__( 'Add Border', 'noor-assistant' ),
				'description' => esc_html__( 'Check to add the border', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'checkbox',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'dima_box_shadow',
				'group'       => esc_html__( 'Style', 'noor-assistant' ),
				'heading'     => esc_html__( 'Drop Shadow', 'noor-assistant' ),
				'description' => esc_html__( 'Check to add drop shadow effect to the counter', 'noor-assistant' ),
				'type'        => 'checkbox',
				'holder'      => 'div',
				'value'       => array( '' => 'true' )
			),
			array(
				'param_name'  => 'id',
				'heading'     => esc_html__( 'ID', 'noor-assistant' ),
				'description' => $dima_vc->id_des,
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'class',
				'heading'     => esc_html__( 'Class', 'noor-assistant' ),
				'description' => $dima_vc->class_des,
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'style',
				'heading'     => esc_html__( 'Style', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter inline CSS.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			//**************Animation***************//
			array(
				'param_name'  => 'animation',
				'group'       => esc_html__( 'Animation', 'noor-assistant' ),
				'heading'     => esc_html__( 'Animation', 'noor-assistant' ),
				'description' => esc_html__( 'Select the type of animation you want to use.', 'noor-assistant' ),
				'type'        => 'dropdown',
				'holder'      => 'div',
				'value'       => $dima_vc->dima_animate_list_velocity,
			),

			array(
				'param_name'  => 'delay',
				'group'       => esc_html__( 'Animation', 'noor-assistant' ),
				'heading'     => esc_html__( 'Delay Animation', 'noor-assistant' ),
				'description' => esc_html__( 'Set when the animation start (milliseconds)', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div',
				'value'       => ''
			),

			array(
				'param_name'  => 'delay_duration',
				'group'       => esc_html__( 'Animation', 'noor-assistant' ),
				'heading'     => esc_html__( 'duration Animation', 'noor-assistant' ),
				'description' => esc_html__( 'Animation duration of each element  (milliseconds).', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div',
				'value'       => ''
			),

			array(
				'param_name'  => 'delay_offset',
				'group'       => esc_html__( 'Animation', 'noor-assistant' ),
				'heading'     => esc_html__( 'Delay Animation Offset', 'noor-assistant' ),
				'description' => esc_html__( 'Set the number ( px,%..) from the top of the viewport where the animation start.', 'noor-assistant' ) . "<a href='http://imakewebthings.com/waypoints/api/offset-option/' target='_blank' >" . esc_html__( 'Read More', 'noor-assistant' ) . "</a>",
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div',
				'value'       => ''
			),
			//**************!Animation***************//
		)
	)
);