<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/*
* Add-on Name: Product Menu
*/

class WPBakeryShortCode_Product_Menu extends WPBakeryShortCodesContainer {
}

/**
 * Product Menu
 */
vc_map(
	array(
		'base'            => 'product_menu',
		'name'            => esc_html__( 'Product Menu', 'noor-assistant' ),
		'weight'          => 780,
		'class'           => 'dima-vc-element dima-vc-element-list',
		'icon'            => 'icon-list',
		'category'        => esc_html__( 'Marketing', 'noor-assistant' ),
		'description'     => esc_html__( 'Include a product menu in your content', 'noor-assistant' ),
		'as_parent'       => array( 'only' => 'product_menu_item' ),
		'content_element' => true,
		'js_view'         => 'VcColumnView',
		'params'          => array(
			array(
				'param_name'  => 'title',
				'heading'     => esc_html__( 'Menu item title', 'noor-assistant' ),
				'description' => esc_html__( 'Add Menu Title', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'bg_color',
				'heading'     => esc_html__( 'Background color', 'noor-assistant' ),
				'description' => esc_html__( 'Choose the background color for your callout', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'colorpicker',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'bg_image',
				'heading'     => esc_html__( 'Background Image', 'noor-assistant' ),
				'description' => esc_html__( 'Add a background image', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'attach_image',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'id',
				'heading'     => esc_html__( 'ID', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter a unique ID.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'class',
				'heading'     => esc_html__( 'Class', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter a unique class name.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'style',
				'heading'     => esc_html__( 'Style', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter inline CSS.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			)
		)
	)
);


/**
 * Product Menu item
 */
vc_map(
	array(
		'base'            => 'product_menu_item',
		'name'            => esc_html__( 'Menu item', 'noor-assistant' ),
		'weight'          => 770,
		'class'           => 'dima-vc-element dima-vc-element-protect',
		'icon'            => 'icon-list',
		'category'        => esc_html__( 'Marketing', 'noor-assistant' ),
		'description'     => esc_html__( 'Include a "Menu item" in your icon list', 'noor-assistant' ),
		'as_child'        => array( 'only' => 'product_menu' ),
		'content_element' => true,
		'params'          => array(
			array(
				'param_name'  => 'content',
				'heading'     => esc_html__( 'Text', 'noor-assistant' ),
				'description' => esc_html__( 'Enter your text.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textarea_html',
				'holder'      => 'div',
				'value'       => ''
			),
			array(
				'param_name'  => 'title',
				'heading'     => esc_html__( 'Menu item title', 'noor-assistant' ),
				'description' => esc_html__( 'Add Menu Title', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'href',
				'heading'     => esc_html__( 'URL Link', 'noor-assistant' ),
				'description' => esc_html__( 'Enter in the URL Link you want your title button to link to', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'target',
				'heading'     => esc_html__( 'Open link in a new tab', 'noor-assistant' ),
				'description' => esc_html__( 'Check to open your title link in a new tab.', 'noor-assistant' ),
				'type'        => 'checkbox',
				'holder'      => 'div',
				'value'       => array(
					'' => 'blank'
				)
			),
			array(
				'param_name'  => 'notification',
				'heading'     => esc_html__( 'Notification', 'noor-assistant' ),
				'description' => esc_html__( 'Add a notification word.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'noti_color',
				'heading'     => esc_html__( 'Color', 'noor-assistant' ),
				'description' => esc_html__( 'Select the notification color', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'dropdown',
				'value'       => array(
					esc_html__( 'Red', 'noor-assistant' )    => 'red',
					esc_html__( 'Orange', 'noor-assistant' ) => 'orange',
				),
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'price',
				'heading'     => esc_html__( 'Price', 'noor-assistant' ),
				'description' => esc_html__( 'Enter the price for this product.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),

			array(
				'param_name'  => 'id',
				'heading'     => esc_html__( 'ID', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter a unique ID.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'class',
				'heading'     => esc_html__( 'Class', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter a unique class name.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'style',
				'heading'     => esc_html__( 'Style', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter inline CSS.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			)
		)
	)
);