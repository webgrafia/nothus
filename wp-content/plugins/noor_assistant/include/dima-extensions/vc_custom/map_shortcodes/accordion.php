<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/*
* Add-on Name: Accordion
*/

class WPBakeryShortCode_Accordion extends WPBakeryShortCodesContainer {
}


/**
 * Accordion
 */
vc_map(
	array(
		'base'            => 'accordion',
		'name'            => esc_html__( 'Accordion', 'noor-assistant' ),
		'weight'          => 930,
		'class'           => 'dima-vc-element dima-vc-element-accordion',
		'icon'            => 'accordion',
		'category'        => esc_html__( 'Content', 'noor-assistant' ),
		'description'     => esc_html__( 'Include an accordion into your content', 'noor-assistant' ),
		'as_parent'       => array( 'only' => 'accordion_item' ),
		'content_element' => true,
		'js_view'         => 'VcColumnView',
		'params'          => array(
			array(
				'param_name'  => 'model',
				'heading'     => esc_html__( 'Accordion Style', 'noor-assistant' ),
				'description' => '',
				'save_always' => true,
				'type'        => 'dropdown',
				'value'       => array(
					esc_html__( 'Default', 'noor-assistant' )     => 'default',
					esc_html__( 'With border', 'noor-assistant' ) => 'border',
					esc_html__( 'Line', 'noor-assistant' )        => 'line',
				),
				'holder'      => 'div'
			),

			array(
				'param_name'  => 'open',
				'heading'     => esc_html__( 'No background in active accordion', 'noor-assistant' ),
				'description' => esc_html__( 'Disable background color in active accordion', 'noor-assistant' ),
				'type'        => 'checkbox',
				'holder'      => 'div',
				'dependency'  => array(
					'element' => 'model',
					'value'   => 'border',
				),
				'value'       => array(
					'' => 'true'
				)
			),

			array(
				'param_name'  => 'id',
				'heading'     => esc_html__( 'ID', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter a unique ID.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),

			array(
				'param_name'  => 'class',
				'heading'     => esc_html__( 'Class', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter a unique class name.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'style',
				'heading'     => esc_html__( 'Style', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter inline CSS.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'tutorials',
				'type'        => 'dima_doc_link_param',
				'heading'     => '<span class="tippy dima-help dima-vc-toolip">' . dima_get_svg_icon( "ic_help" )
				                 . '<span class="dima-vc-tooltip-text">'
				                 . esc_html__( 'Video tutorial and theme documentation article', 'noor-assistant' ) . '</span></span>'
				                 . esc_html__( 'Tutorial', 'noor-assistant' ),
				'doc_link'    => '',
				'video_link'  => 'https://www.youtube.com/watch?v=QOk68fi9Ucg',
				'doc_example' => 'https://noor.pixeldima.com/accordion/',
			),
		)
	)
);


/**
 * Accordion item
 */
vc_map(
	array(
		'base'            => 'accordion_item',
		'name'            => esc_html__( 'Accordion Individual Item', 'noor-assistant' ),
		'weight'          => 940,
		'class'           => 'dima-vc-element dima-vc-element-accordion-item',
		'icon'            => 'accordion',
		'category'        => esc_html__( 'Content', 'noor-assistant' ),
		'description'     => esc_html__( 'Include an accordion individual item in your accordion', 'noor-assistant' ),
		'as_child'        => array( 'only' => 'accordion' ),
		'content_element' => true,
		'params'          => array(
			array(
				'param_name'  => 'content',
				'heading'     => esc_html__( 'Text', 'noor-assistant' ),
				'description' => esc_html__( 'Enter your text.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textarea_html',
				'holder'      => 'div',
				'value'       => ''
			),
			array(
				'param_name'  => 'parent_id',
				'heading'     => esc_html__( 'Parent ID', 'noor-assistant' ),
				'description' => esc_html__( 'Optionally include an ID given to the parent accordion to only allow one toggle to be open at a time.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'title',
				'heading'     => esc_html__( 'Individual Accordion title.', 'noor-assistant' ),
				'description' => esc_html__( 'Include a title for your individual accordion item.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'open',
				'heading'     => esc_html__( 'Open the Individual Accordion', 'noor-assistant' ),
				'description' => esc_html__( 'Check for the individual accordion item to be open by default.', 'noor-assistant' ),
				'type'        => 'checkbox',
				'holder'      => 'div',
				'value'       => array(
					'' => 'true'
				)
			),
			array(
				'type'        => 'dropdown',
				'heading'     => esc_html__( 'Icons Library', 'noor-assistant' ),
				'group'       => esc_html__( 'Icons', 'noor-assistant' ),
				'value'       => array(
					esc_html__( 'Font Awesome', 'noor-assistant' )                                     => 'fontawesome',
					esc_html__( 'Custom Font', 'noor-assistant' )                                      => 'customfont',
					esc_html__( 'SVG Icons (Stroke Gap Icons And Material Icons) ', 'noor-assistant' ) => 'svgicon',
				),
				'param_name'  => 'icon_type',
				'description' => esc_html__( 'Select icon for the closed accordion item', 'noor-assistant' ),
			),
			array(
				'param_name'  => 'icon_svg',
				'group'       => esc_html__( 'Icons', 'noor-assistant' ),
				'heading'     => esc_html__( 'Icon SVG', 'noor-assistant' ),
				'description' => esc_html__( 'Enter SVG Icon name', 'noor-assistant' ),
				'value'       => '',
				'dependency'  => array(
					'element' => 'icon_type',
					'value'   => 'svgicon',
				),
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'icon',
				'group'       => esc_html__( 'Icons', 'noor-assistant' ),
				'heading'     => esc_html__( 'Icon Class', 'noor-assistant' ),
				'description' => esc_html__( 'Enter Icon Class from Font Awesome or Simple Line Icons.', 'noor-assistant' ),
				'value'       => 'simple-icon-user',
				'dependency'  => array(
					'element' => 'icon_type',
					'value'   => 'customfont',
				),
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'type'        => 'iconpicker',
				'group'       => esc_html__( 'Icons', 'noor-assistant' ),
				'heading'     => esc_html__( 'Icon', 'noor-assistant' ),
				'param_name'  => 'icon_fontawesome',
				'value'       => '', // default value to backend editor admin_label
				'settings'    => array(
					'emptyIcon'    => true,
					// default true, display an "EMPTY" icon?
					'value'        => 'fontawesome',
					'iconsPerPage' => 4000,
					// default 100, how many icons per/page to display, we use (big number) to display all icons in single page
				),
				'dependency'  => array(
					'element' => 'icon_type',
					'value'   => 'fontawesome',
				),
				'description' => esc_html__( 'Select icon from library.', 'noor-assistant' ),
			),
			array(
				'type'        => 'dropdown',
				'heading'     => esc_html__( 'Icons Library', 'noor-assistant' ),
				'group'       => esc_html__( 'Icons', 'noor-assistant' ),
				'value'       => array(
					esc_html__( 'Font Awesome', 'noor-assistant' )                                     => 'fontawesome',
					esc_html__( 'Custom Font', 'noor-assistant' )                                      => 'customfont',
					esc_html__( 'SVG Icons (Stroke Gap Icons And Material Icons) ', 'noor-assistant' ) => 'svgicon',
				),
				'param_name'  => 'icon_type_two',
				'description' => esc_html__( 'Select icon for the opened accordion item', 'noor-assistant' ),
			),
			array(
				'param_name'  => 'icon_svg_two',
				'group'       => esc_html__( 'Icons', 'noor-assistant' ),
				'heading'     => esc_html__( 'Icon SVG', 'noor-assistant' ),
				'description' => esc_html__( 'Enter SVG Icon name', 'noor-assistant' ),
				'value'       => '',
				'dependency'  => array(
					'element' => 'icon_type_two',
					'value'   => 'svgicon',
				),
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'icon_two',
				'group'       => esc_html__( 'Icons', 'noor-assistant' ),
				'heading'     => esc_html__( 'Icon Class', 'noor-assistant' ),
				'description' => esc_html__( 'Enter Icon Class from Font Awesome or Simple Line Icons.', 'noor-assistant' ),
				'value'       => 'simple-icon-user',
				'dependency'  => array(
					'element' => 'icon_type_two',
					'value'   => 'customfont',
				),
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'type'        => 'iconpicker',
				'group'       => esc_html__( 'Icons', 'noor-assistant' ),
				'heading'     => esc_html__( 'Icon', 'noor-assistant' ),
				'param_name'  => 'icon_fontawesome_two',
				'value'       => '', // default value to backend editor admin_label
				'settings'    => array(
					'emptyIcon'    => true,
					// default true, display an "EMPTY" icon?
					'value'        => 'fontawesome',
					'iconsPerPage' => 4000,
					// default 100, how many icons per/page to display, we use (big number) to display all icons in single page
				),
				'dependency'  => array(
					'element' => 'icon_type_two',
					'value'   => 'fontawesome',
				),
				'description' => esc_html__( 'Select icon from library.', 'noor-assistant' ),
			),

			array(
				'param_name'  => 'bg_color',
				'group'       => esc_html__( 'Icons', 'noor-assistant' ),
				'heading'     => esc_html__( 'Background Color (for the icon)', 'noor-assistant' ),
				'description' => esc_html__( 'Choose a background color for your individual accordion item icon', 'noor-assistant' ),
				'type'        => 'colorpicker',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'icon_color',
				'group'       => esc_html__( 'Icons', 'noor-assistant' ),
				'heading'     => esc_html__( 'Icon Color', 'noor-assistant' ),
				'description' => esc_html__( 'Choose a color for your individual accordion item icon', 'noor-assistant' ),
				'type'        => 'colorpicker',
				'holder'      => 'div'
			),

			array(
				'param_name'  => 'id',
				'heading'     => esc_html__( 'ID', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter a unique ID.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'class',
				'heading'     => esc_html__( 'Class', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter a unique class name.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'style',
				'heading'     => esc_html__( 'Style', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter inline CSS.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			)
		)
	)
);