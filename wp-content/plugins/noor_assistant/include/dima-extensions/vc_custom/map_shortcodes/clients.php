<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/*
* Add-on Name: clients
*/

class WPBakeryShortCode_Client extends WPBakeryShortCode {
}

class WPBakeryShortCode_Clients extends WPBakeryShortCodesContainer {
}

/**
 * Clinets
 */
vc_map(
	array(
		'base'            => 'clients',
		'name'            => esc_html__( 'Clients', 'noor-assistant' ),
		'weight'          => 900,
		'class'           => 'dima-vc-element dima-vc-element-tabs',
		'icon'            => 'clients',
		'category'        => esc_html__( 'Content', 'noor-assistant' ),
		'description'     => esc_html__( 'Add clients logos to your content', 'noor-assistant' ),
		'as_parent'       => array( 'only' => 'client' ),
		'content_element' => true,
		'js_view'         => 'VcColumnView',
		'params'          => array(
			array(
				'param_name'  => 'columns',
				'heading'     => esc_html__( 'Clients Columns', 'noor-assistant' ),
				'description' => esc_html__( 'Select number of columns for the clients images.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'dropdown',
				'value'       => array(
					esc_html__( '- Select Option -', 'noor-assistant' ) => '3',
					'1'                                                 => '1',
					'2'                                                 => '2',
					'3'                                                 => '3',
					'4'                                                 => '4',
					'5'                                                 => '5',
					'6'                                                 => '6',
					'7'                                                 => '7',
				),
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'id',
				'heading'     => esc_html__( 'ID', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter a unique ID.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'class',
				'heading'     => esc_html__( 'Class', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter a unique class name.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'style',
				'heading'     => esc_html__( 'Style', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter inline CSS.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			)
		)
	)
);

/**
 * Clinet
 */
vc_map(
	array(
		'base'            => 'client',
		'name'            => esc_html__( 'Client Element', 'noor-assistant' ),
		'weight'          => 960,
		'class'           => 'dima-vc-element dima-vc-element-clients',
		'icon'            => 'client',
		'as_child'        => array( 'only' => 'clients' ),
		'category'        => esc_html__( 'Content', 'noor-assistant' ),
		'description'     => esc_html__( 'Add Client Element', 'noor-assistant' ),
		'content_element' => true,
		'params'          => array(
			array(
				'param_name'  => 'src',
				'heading'     => esc_html__( 'Client Image', 'noor-assistant' ),
				'description' => esc_html__( 'Add client image.', 'noor-assistant' ),
				'type'        => 'attach_image',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'alt',
				'heading'     => esc_html__( 'Alt text ', 'noor-assistant' ),
				'description' => '',
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),

			array(
				'param_name'  => 'href',
				'heading'     => esc_html__( 'URL Link ', 'noor-assistant' ),
				'description' => esc_html__( 'Add client URL link', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'target',
				'heading'     => esc_html__( 'Open link in a new tab', 'noor-assistant' ),
				'description' => esc_html__( 'Check to open the link in a new tab.', 'noor-assistant' ),
				'type'        => 'checkbox',
				'holder'      => 'div',
				'value'       => array(
					'' => 'blank'
				)
			),
			array(
				'param_name'  => 'id',
				'heading'     => esc_html__( 'ID', 'noor-assistant' ),
				'description' => $dima_vc->id_des,
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'class',
				'heading'     => esc_html__( 'Class', 'noor-assistant' ),
				'description' => $dima_vc->class_des,
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'style',
				'heading'     => esc_html__( 'Style', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter inline CSS.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			)
		)
	)
);