<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/*
* Add-on Name: alert
*/

class WPBakeryShortCode_Alert extends WPBakeryShortCode {
}

/**
 * Alert
 */
vc_map(
	array(
		'base'        => 'alert',
		'name'        => esc_html__( 'Alert Box', 'noor-assistant' ),
		'weight'      => 650,
		'class'       => 'dima-vc-element dima-vc-element-alert',
		'icon'        => 'alert',
		'category'    => esc_html__( 'Information', 'noor-assistant' ),
		'description' => esc_html__( 'Show information to users with alerts', 'noor-assistant' ),
		'params'      => array(
			array(
				'param_name'  => 'alert_style',
				'heading'     => esc_html__( 'Alert Box Style', 'noor-assistant' ),
				'description' => esc_html__( 'Select the alert box type.', 'noor-assistant' ),
				'type'        => 'dropdown',
				'holder'      => 'div',
				'value'       => array(
					esc_html__( 'Style One', 'noor-assistant' )   => 'one',
					esc_html__( 'Style Two', 'noor-assistant' )   => 'two',
					esc_html__( 'Style Three', 'noor-assistant' ) => 'three',
					esc_html__( 'Style Four', 'noor-assistant' )  => 'four'
				)
			),
			array(
				'param_name'  => 'content',
				'heading'     => esc_html__( 'Text', 'noor-assistant' ),
				'description' => esc_html__( 'Enter your text.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textarea_html',
				'holder'      => 'div',
				'value'       => ''
			),
			array(
				'param_name'  => 'heading',
				'heading'     => esc_html__( 'Alert Box Title', 'noor-assistant' ),
				'description' => esc_html__( 'Enter the title of your alert box.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'type',
				'heading'     => esc_html__( 'Alert Box Type', 'noor-assistant' ),
				'description' => esc_html__( 'Select the alert box type.', 'noor-assistant' ),
				'type'        => 'dropdown',
				'holder'      => 'div',
				'value'       => array(
					esc_html__( 'Info', 'noor-assistant' )    => 'info',
					esc_html__( 'Success', 'noor-assistant' ) => 'success',
					esc_html__( 'Warning', 'noor-assistant' ) => 'warning',
					esc_html__( 'Error', 'noor-assistant' )   => 'error',
					esc_html__( 'Custom', 'noor-assistant' )  => 'custom',
				)
			),
			array(
				'param_name'  => 'is_icon',
				'heading'     => esc_html__( 'Alert Box Icon', 'noor-assistant' ),
				'description' => esc_html__( 'Check to display alert icon.', 'noor-assistant' ),
				'type'        => 'checkbox',
				'holder'      => 'div',
				'value'       => array(
					'' => 'true'
				)
			),
			array(
				'type'        => 'dropdown',
				'heading'     => esc_html__( 'icons library', 'noor-assistant' ),
				'value'       => array(
					esc_html__( 'Font Awesome', 'noor-assistant' )                                     => 'fontawesome',
					esc_html__( 'Custom Font', 'noor-assistant' )                                      => 'customfont',
					esc_html__( 'SVG Icons (Stroke Gap Icons And Material Icons) ', 'noor-assistant' ) => 'svgicon',
				),
				'dependency'  => array(
					'element' => 'type',
					'value'   => 'custom',
				),
				'param_name'  => 'icon_type',
				'description' => esc_html__( 'Select icons library.', 'noor-assistant' ),
			),
			array(
				'param_name'  => 'icon_svg',
				'heading'     => esc_html__( 'Icon SVG', 'noor-assistant' ),
				'description' => esc_html__( 'Enter SVG Icon name', 'noor-assistant' ),
				'value'       => '',
				'dependency'  => array(
					'element' => 'icon_type',
					'value'   => 'svgicon',
				),
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'icon',
				'heading'     => esc_html__( 'Icon Class', 'noor-assistant' ),
				'description' => esc_html__( 'Enter Icon Class from Font Awesome or Simple Line Icons.', 'noor-assistant' ),
				'value'       => 'simple-icon-user',
				'dependency'  => array(
					'element' => 'icon_type',
					'value'   => 'customfont',
				),
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'type'        => 'iconpicker',
				'heading'     => esc_html__( 'Icon', 'noor-assistant' ),
				'param_name'  => 'icon_fontawesome',
				'value'       => '',
				'settings'    => array(
					'emptyIcon'    => true,
					'value'        => 'fontawesome',
					'iconsPerPage' => 4000,
				),
				'dependency'  => array(
					'element' => 'icon_type',
					'value'   => 'fontawesome',
				),
				'description' => esc_html__( 'Select icon from library.', 'noor-assistant' ),
			),
			array(
				'param_name'  => 'bg_color',
				'heading'     => esc_html__( 'Background Color', 'noor-assistant' ),
				'description' => esc_html__( 'Choose background color for the text', 'noor-assistant' ),
				'type'        => 'colorpicker',
				'dependency'  => array(
					'element' => 'type',
					'value'   => 'custom',
				),
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'close',
				'heading'     => esc_html__( 'Alert box Close Icon', 'noor-assistant' ),
				'description' => esc_html__( 'Check to display the close icon.', 'noor-assistant' ),
				'type'        => 'checkbox',
				'holder'      => 'div',
				'value'       => array(
					'' => 'true'
				)
			),
			array(
				'param_name'  => 'id',
				'heading'     => esc_html__( 'ID', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter a unique ID.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'class',
				'heading'     => esc_html__( 'Class', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter a unique class name.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'style',
				'heading'     => esc_html__( 'Style', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter inline CSS.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			//**************Animation***************//
			array(
				'param_name'  => 'animation',
				'group'       => esc_html__( 'Animation', 'noor-assistant' ),
				'heading'     => esc_html__( 'Animation', 'noor-assistant' ),
				'description' => esc_html__( 'Select the type of animation you want to use.', 'noor-assistant' ),
				'type'        => 'dropdown',
				'holder'      => 'div',
				'value'       => $dima_vc->dima_animate_list_velocity,
			),

			array(
				'param_name'  => 'delay',
				'group'       => esc_html__( 'Animation', 'noor-assistant' ),
				'heading'     => esc_html__( 'Delay Animation', 'noor-assistant' ),
				'description' => esc_html__( 'Set when the animation start (milliseconds)', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div',
				'value'       => ''
			),

			array(
				'param_name'  => 'delay_duration',
				'group'       => esc_html__( 'Animation', 'noor-assistant' ),
				'heading'     => esc_html__( 'duration Animation', 'noor-assistant' ),
				'description' => esc_html__( 'Animation duration of each element  (milliseconds).', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div',
				'value'       => ''
			),

			array(
				'param_name'  => 'delay_offset',
				'group'       => esc_html__( 'Animation', 'noor-assistant' ),
				'heading'     => esc_html__( 'Delay Animation Offset', 'noor-assistant' ),
				'description' => esc_html__( 'Set the number ( px,%..) from the top of the viewport where the animation start.', 'noor-assistant' ) . "<a href='http://imakewebthings.com/waypoints/api/offset-option/' target='_blank' >" . esc_html__( 'Read More', 'noor-assistant' ) . "</a>",
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div',
				'value'       => ''
			),
			//**************!Animation***************//
		)
	)
);