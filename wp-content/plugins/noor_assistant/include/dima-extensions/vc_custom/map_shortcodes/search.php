<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/*
* Add-on Name: Search
*/

class WPBakeryShortCode_Search extends WPBakeryShortCode {
}

/**
 * Search
 */
vc_map(
	array(
		'base'        => 'search',
		'name'        => esc_html__( 'Search', 'noor-assistant' ),
		'weight'      => 480,
		'class'       => 'dima-vc-element dima-vc-element-search',
		'icon'        => 'search',
		'category'    => esc_html__( 'Content', 'noor-assistant' ),
		'description' => esc_html__( 'Include a search field in your content', 'noor-assistant' ),
		'params'      => array(
			array(
				'param_name'  => 'id',
				'heading'     => esc_html__( 'ID', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter a unique ID.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'class',
				'heading'     => esc_html__( 'Class', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter a unique class name.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'style',
				'heading'     => esc_html__( 'Style', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter inline CSS.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			)
		)
	)
);
