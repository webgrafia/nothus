<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/*
* Add-on Name: Image
*/

class WPBakeryShortCode_Image extends WPBakeryShortCode {
}

/**
 * Images
 */
vc_map(
	array(
		'base'        => 'image',
		'name'        => esc_html__( 'Image', 'noor-assistant' ),
		'weight'      => 700,
		'class'       => 'dima-vc-element dima-vc-element-image',
		'icon'        => 'image',
		'category'    => esc_html__( 'Media', 'noor-assistant' ),
		'description' => esc_html__( 'Include an image in your content', 'noor-assistant' ),
		'js_view'     => 'DimaImageView',
		'params'      => array(
			array(
				'param_name'  => 'src',
				'heading'     => esc_html__( 'Image', 'noor-assistant' ),
				'description' => esc_html__( 'Upload your image.', 'noor-assistant' ),
				'type'        => 'attach_image',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'dima_lazyimage',
				'heading'     => esc_html__( 'Applay lazy Image', 'noor-assistant' ),
				'description' => esc_html__( 'lazy Image mean: Images outside of viewport will not be loaded before user scrolls to them.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'checkbox',
				'holder'      => 'div',
				'value'       => array(
					'' => 'true'
				)
			),

			array(
				'param_name'  => 'float',
				'heading'     => esc_html__( 'Image Float', 'noor-assistant' ),
				'description' => esc_html__( 'Optionally float the image.', 'noor-assistant' ),
				'type'        => 'dropdown',
				'holder'      => 'div',
				'value'       => array(
					esc_html__( '- Select Option -', 'noor-assistant' ) => '',
					esc_html__( 'Start', 'noor-assistant' )             => 'start',
					esc_html__( 'Center', 'noor-assistant' )            => 'center',
					esc_html__( 'End', 'noor-assistant' )               => 'end'
				)
			),
			array(
				'param_name'  => 'alt',
				'heading'     => esc_html__( 'Alt', 'noor-assistant' ),
				'description' => esc_html__( 'Enter in the alt text for your image.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'is_gallert_item',
				'heading'     => esc_html__( 'Gallery Item', 'noor-assistant' ),
				'description' => esc_html__( 'Check to display the images as a gallery.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'checkbox',
				'holder'      => 'div'
			),

			array(
				'param_name'  => 'lightbox',
				'heading'     => esc_html__( 'Lightbox Type', 'noor-assistant' ),
				'description' => esc_html__( 'Select your lightbox type (iframe: for youtube, map,..)', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'dropdown',
				'value'       => array(
					esc_html__( '- Select Option -', 'noor-assistant' ) => '',
					'None'                                              => '',
					'image'                                             => 'image',
					'iframe'                                            => 'iframe',
					'ajax'                                              => 'ajax'
				),
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'href',
				'heading'     => esc_html__( 'URL Link', 'noor-assistant' ),
				'description' => esc_html__( 'If using this for a lightbox, enter the URL link of your media here (e.g. YouTube URL, Image URL,...). Leave it blank if you want to link to the image uploaded.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'target',
				'heading'     => esc_html__( 'Open link in a new tab', 'noor-assistant' ),
				'description' => esc_html__( 'Check to open your image link in a new tab.', 'noor-assistant' ),
				'type'        => 'checkbox',
				'holder'      => 'div',
				'value'       => array(
					'' => 'blank'
				)
			),
			array(
				'param_name'  => 'title',
				'heading'     => esc_html__( 'Popup Info Title', 'noor-assistant' ),
				'description' => esc_html__( 'Enter in the title attribute you want for your image (This will apply as title for popover or tooltip if you have active it from -Popup Info type-).', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'popup_type',
				'heading'     => esc_html__( 'Popup Info type', 'noor-assistant' ),
				'description' => esc_html__( 'Select to choose the type of popup info', 'noor-assistant' ),
				'type'        => 'dropdown',
				'holder'      => 'div',
				'value'       => array(
					esc_html__( '- Select Option -', 'noor-assistant' ) => '',
					'None'                                              => '',
					'Popover'                                           => 'popover',
					'Tooltip'                                           => 'tooltip'
				)
			),
			array(
				'param_name'  => 'popup_trigger',
				'heading'     => esc_html__( 'Popup Trigger', 'noor-assistant' ),
				'description' => esc_html__( 'Select what actions you want to trigger the popover or tooltip.', 'noor-assistant' ),
				'type'        => 'dropdown',
				'holder'      => 'div',
				'value'       => array(
					esc_html__( '- Select Option -', 'noor-assistant' ) => '',
					'Hover'                                             => 'hover',
					'Click'                                             => 'click',
					'Focus'                                             => 'focus'
				)
			),
			array(
				'param_name'  => 'popup_place',
				'heading'     => esc_html__( 'Popup Position', 'noor-assistant' ),
				'description' => esc_html__( 'Select the popup position.', 'noor-assistant' ),
				'type'        => 'dropdown',
				'holder'      => 'div',
				'value'       => array(
					esc_html__( '- Select Option -', 'noor-assistant' ) => '',
					'Top'                                               => 'top',
					'Left'                                              => 'left',
					'Right'                                             => 'right',
					'Bottom'                                            => 'bottom'
				)
			),
			array(
				'param_name'  => 'popup_content',
				'heading'     => esc_html__( 'Info Content', 'noor-assistant' ),
				'description' => esc_html__( 'Extra content for the popover.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'id',
				'heading'     => esc_html__( 'ID', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter a unique ID.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'class',
				'heading'     => esc_html__( 'Class', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter a unique class name.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'style',
				'heading'     => esc_html__( 'Style', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter inline CSS.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'width',
				'heading'     => esc_html__( 'Image Width', 'noor-assistant' ),
				'description' => esc_html__( 'Select to choose image width', 'noor-assistant' ),
				'save_always' => true,
				'group'       => esc_html__( 'Style & hover', 'noor-assistant' ),
				'type'        => 'dropdown',
				'value'       => array(
					esc_html__( '- Select Option -', 'noor-assistant' ) => '',
					esc_html__( '1/1 (100%)', 'noor-assistant' )        => '1',
					esc_html__( '1/2 (50%)', 'noor-assistant' )         => '2',
					esc_html__( '1/3 (33.33%)', 'noor-assistant' )      => '3',
					esc_html__( '1/4 (25%)', 'noor-assistant' )         => '4',
					esc_html__( '1/5 (20%)', 'noor-assistant' )         => '5',
					esc_html__( '1/6 (16.66%)', 'noor-assistant' )      => '6',
				),
				'holder'      => 'div'
			),

			array(
				'param_name'  => 'shape',
				'heading'     => esc_html__( 'Image Shape', 'noor-assistant' ),
				'description' => esc_html__( 'Select the image shape.', 'noor-assistant' ),
				'type'        => 'dropdown',
				'holder'      => 'div',
				'group'       => esc_html__( 'Style & hover', 'noor-assistant' ),
				'value'       => array(
					esc_html__( 'None', 'noor-assistant' )   => '',
					esc_html__( 'Box', 'noor-assistant' )    => 'img-boxed',
					esc_html__( 'Radius', 'noor-assistant' ) => 'rounded',
					esc_html__( 'Circle', 'noor-assistant' ) => 'circle'
				)
			),
			array(
				'param_name'  => 'hover',
				'heading'     => esc_html__( 'Image Hover', 'noor-assistant' ),
				'description' => esc_html__( 'Select the image hover.', 'noor-assistant' ),
				'type'        => 'dropdown',
				'group'       => esc_html__( 'Style & hover', 'noor-assistant' ),
				'holder'      => 'div',
				'value'       => array(
					esc_html__( '- Select Option -', 'noor-assistant' ) => '',
					esc_html__( 'None', 'noor-assistant' )              => 'none',
					esc_html__( 'Inherit', 'noor-assistant' )           => '',
					esc_html__( 'Zoom-out', 'noor-assistant' )          => 'zoom-out',
					esc_html__( 'Zoom-in', 'noor-assistant' )           => 'zoom-in',
					esc_html__( 'Opacity', 'noor-assistant' )           => 'effect-opacity',
				)
			),
			array(
				'param_name'  => 'apply_gray',
				'heading'     => esc_html__( 'Grayscale Effect', 'noor-assistant' ),
				'description' => esc_html__( 'Check to apply grayscale image effect', 'noor-assistant' ),
				'type'        => 'checkbox',
				'group'       => esc_html__( 'Style & hover', 'noor-assistant' ),
				'holder'      => 'div',
				'value'       => array(
					'' => 'true'
				)
			),
			//**************Animation***************//
			array(
				'param_name'  => 'animation',
				'group'       => esc_html__( 'Animation', 'noor-assistant' ),
				'heading'     => esc_html__( 'Animation', 'noor-assistant' ),
				'description' => esc_html__( 'Select the type of animation you want to use.', 'noor-assistant' ),
				'type'        => 'dropdown',
				'holder'      => 'div',
				'value'       => $dima_vc->dima_animate_list_velocity,
			),

			array(
				'param_name'  => 'delay',
				'group'       => esc_html__( 'Animation', 'noor-assistant' ),
				'heading'     => esc_html__( 'Delay Animation', 'noor-assistant' ),
				'description' => esc_html__( 'Set when the animation start (milliseconds)', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div',
				'value'       => ''
			),

			array(
				'param_name'  => 'delay_duration',
				'group'       => esc_html__( 'Animation', 'noor-assistant' ),
				'heading'     => esc_html__( 'duration Animation', 'noor-assistant' ),
				'description' => esc_html__( 'Animation duration of each element  (milliseconds).', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div',
				'value'       => ''
			),

			array(
				'param_name'  => 'delay_offset',
				'group'       => esc_html__( 'Animation', 'noor-assistant' ),
				'heading'     => esc_html__( 'Delay Animation Offset', 'noor-assistant' ),
				'description' => esc_html__( 'Set the number ( px,%..) from the top of the viewport where the animation start.', 'noor-assistant' ) . "<a href='http://imakewebthings.com/waypoints/api/offset-option/' target='_blank' >" . esc_html__( 'Read More', 'noor-assistant' ) . "</a>",
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div',
				'value'       => ''
			),
			//**************!Animation***************//
			array(
				'param_name'  => 'tutorials',
				'type'        => 'dima_doc_link_param',
				'heading'     => '<span class="tippy dima-help dima-vc-toolip">' . dima_get_svg_icon( "ic_help" )
				                 . '<span class="dima-vc-tooltip-text">'
				                 . esc_html__( 'Video tutorial and theme documentation article', 'noor-assistant' ) . '</span></span>'
				                 . esc_html__( 'Tutorial', 'noor-assistant' ),
				'doc_link'    => '',
				'video_link'  => 'https://www.youtube.com/watch?v=wyMKGn0eDQc',
				'doc_example' => 'https://noor.pixeldima.com/images/',
			),

		)
	)
);