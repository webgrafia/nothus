<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/*
* Add-on Name: Dropcap
*/

class WPBakeryShortCode_Dropcap extends WPBakeryShortCode {
}

/**
 * Dropcap
 */
vc_map(
	array(
		'base'        => 'dropcap',
		'name'        => esc_html__( 'Drop cap', 'noor-assistant' ),
		'weight'      => 510,
		'class'       => 'dima-vc-element dima-vc-element-dropcap',
		'icon'        => 'dropcap',
		'category'    => esc_html__( 'Social', 'noor-assistant' ),
		'description' => esc_html__( 'Add drop cap to your paragraph', 'noor-assistant' ),
		'params'      => array(
			array(
				'param_name'  => 'type',
				'heading'     => esc_html__( 'Drop cap Style', 'noor-assistant' ),
				'description' => esc_html__( 'Select drop cap style', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'dropdown',
				'value'       => array(
					esc_html__( 'Select Option', 'noor-assistant' ) => '',
					esc_html__( 'Outline', 'noor-assistant' )       => 'dropcap-1',
					esc_html__( 'Fill', 'noor-assistant' )          => 'dropcap-3',
				),
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'character',
				'heading'     => esc_html__( 'Character', 'noor-assistant' ),
				'description' => esc_html__( 'Type the first character of the paragraph ', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'id',
				'heading'     => esc_html__( 'ID', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter a unique ID.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'class',
				'heading'     => esc_html__( 'Class', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter a unique class name.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'style',
				'heading'     => esc_html__( 'Style', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter inline CSS.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			)
		)
	)
);
