<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/*
* Add-on Name: Pricing table
*/

class WPBakeryShortCode_Pricing_Table_Column extends WPBakeryShortCodesContainer {
}

/**
 * Pricing table
 */
vc_map(
	array(
		'base'            => 'pricing_table_column',
		'name'            => esc_html__( 'Pricing Table', 'noor-assistant' ),
		'weight'          => 670,
		'class'           => 'dima-vc-element dima-vc-element-pricing-table-column',
		'icon'            => 'pricing-table',
		'category'        => esc_html__( 'Marketing', 'noor-assistant' ),
		'as_parent'       => array( 'only' => 'button,icon_list,clear,iconbox_header,google_maptwo,text' ),
		'js_view'         => 'VcColumnView',
		'description'     => esc_html__( 'Include a pricing table column', 'noor-assistant' ),
		'content_element' => true,
		'params'          => array(
			array(
				'param_name'  => 'type',
				'heading'     => esc_html__( 'Pricing Table Style', 'noor-assistant' ),
				'description' => esc_html__( 'Select the style of your pricing table', 'noor-assistant' ),
				'type'        => 'dropdown',
				'holder'      => 'div',
				'value'       => array(
					'Inherit' => '',
					'Callout' => 'callout',
					'Offers'  => 'offers',
				)
			),
			array(
				'param_name'  => 'dima_vc_add_shadow',
				'heading'     => esc_html__( 'Add Shadow', 'noor-assistant' ),
				'description' => esc_html__( 'Check to add drop shadow effect to the table', 'noor-assistant' ),
				'type'        => 'checkbox',
				'holder'      => 'div',
				'value'       => array(
					'' => 'true'
				)
			),
			array(
				'param_name'  => 'dima_vc_add_border_btm',
				'heading'     => esc_html__( 'Add border after content', 'noor-assistant' ),
				'description' => esc_html__( 'Check to add border after content', 'noor-assistant' ),
				'type'        => 'checkbox',
				'holder'      => 'div',
				'value'       => array(
					'' => 'true'
				)
			),
			array(
				'param_name'  => 'dima_vc_border_color',
				'heading'     => esc_html__( 'Top border color', 'noor-assistant' ),
				'description' => esc_html__( 'Choose the color of top border', 'noor-assistant' ),
				'type'        => 'colorpicker',
			),
			array(
				'param_name'  => 'href_callout',
				'heading'     => esc_html__( 'URL Link', 'noor-assistant' ),
				'description' => esc_html__( 'Enter in the URL you want your pricing table to link to.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'dependency'  => array(
					'element' => 'type',
					'value'   => 'callout',
				),

				'holder' => 'div'
			),

			array(
				'param_name'  => 'target',
				'heading'     => esc_html__( 'Open link in a new tab', 'noor-assistant' ),
				'description' => esc_html__( 'Select to open your button link in a new tab.', 'noor-assistant' ),
				'type'        => 'checkbox',
				'holder'      => 'div',
				'dependency'  => array(
					'element' => 'type',
					'value'   => 'callout',
				),
				'value'       => array(
					'' => 'blank'
				)
			),
			array(
				'param_name'  => 'title',
				'heading'     => esc_html__( 'Pricing Table Title', 'noor-assistant' ),
				'description' => esc_html__( 'Include a title for your pricing table.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'dependency'  => array(
					'element' => 'type',
					'value'   => array( 'callout', 'zibra', '' ),
				),
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'icon_type',
				'type'        => 'dropdown',
				'heading'     => esc_html__( 'Icons library', 'noor-assistant' ),
				'value'       => array(
					esc_html__( 'Font Awesome', 'noor-assistant' )                                     => 'fontawesome',
					esc_html__( 'Custom Font', 'noor-assistant' )                                      => 'customfont',
					esc_html__( 'SVG Icons (Stroke Gap Icons And Material Icons) ', 'noor-assistant' ) => 'svgicon',
				),
				'description' => esc_html__( 'Select icons library.', 'noor-assistant' ),
			),
			array(
				'type'        => 'iconpicker',
				'heading'     => esc_html__( 'Icon', 'noor-assistant' ),
				'param_name'  => 'icon_fontawesome',
				'value'       => '',
				'settings'    => array(
					'emptyIcon'    => true,
					'value'        => 'fontawesome',
					'iconsPerPage' => 4000,
				),
				'dependency'  => array(
					'element' => 'icon_type',
					'value'   => 'fontawesome',
				),
				'description' => esc_html__( 'Select icon from library.', 'noor-assistant' ),
			),
			array(
				'param_name'  => 'icon',
				'heading'     => esc_html__( 'Icon Class', 'noor-assistant' ),
				'description' => esc_html__( 'Enter Icon Class from Font Awesome or Simple Line Icons.', 'noor-assistant' ),
				'value'       => 'simple-icon-user',
				'dependency'  => array(
					'element' => 'icon_type',
					'value'   => 'customfont',
				),
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'icon_svg',
				'heading'     => esc_html__( 'Icon SVG', 'noor-assistant' ),
				'description' => esc_html__( 'Enter SVG Icon name', 'noor-assistant' ),
				'value'       => '',
				'dependency'  => array(
					'element' => 'icon_type',
					'value'   => 'svgicon',
				),
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'featured',
				'heading'     => esc_html__( 'Popular Pricing Table', 'noor-assistant' ),
				'description' => esc_html__( 'Choose to make this your popular offer.', 'noor-assistant' ),
				'type'        => 'checkbox',
				'holder'      => 'div',
				'value'       => array(
					'' => 'true'
				)
			),
			array(
				'param_name'  => 'price',
				'heading'     => esc_html__( 'Price', 'noor-assistant' ),
				'description' => esc_html__( 'Enter the price for this pricing table.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'dependency'  => array(
					'element' => 'type',
					'value'   => array( 'callout', 'zibra', '' ),
				),
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'currency',
				'heading'     => esc_html__( 'Currency Symbol', 'noor-assistant' ),
				'description' => esc_html__( 'Enter the currency symbol you want to use.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'dependency'  => array(
					'element' => 'type',
					'value'   => array( 'callout', 'zibra', '' ),
				),
				'value'       => '$',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'interval',
				'heading'     => esc_html__( 'Interval', 'noor-assistant' ),
				'description' => esc_html__( 'Enter the time period that this pricing table is for.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'dependency'  => array(
					'element' => 'type',
					'value'   => array( 'callout', 'zibra', '' ),
				),
				'holder'      => 'div',
				'value'       => esc_html__( 'Per Month', 'noor-assistant' )
			),
			array(
				'param_name'  => 'id',
				'heading'     => esc_html__( 'ID', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter a unique ID.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'class',
				'heading'     => esc_html__( 'Class', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter a unique class name.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'style',
				'heading'     => esc_html__( 'Style', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter inline CSS.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'tutorials',
				'type'        => 'dima_doc_link_param',
				'heading'     => '<span class="tippy dima-help dima-vc-toolip">' . dima_get_svg_icon( "ic_help" )
				                 . '<span class="dima-vc-tooltip-text">'
				                 . esc_html__( 'Video tutorial and theme documentation article', 'noor-assistant' ) . '</span></span>'
				                 . esc_html__( 'Tutorial', 'noor-assistant' ),
				'doc_link'    => '',
				'video_link'  => 'https://www.youtube.com/watch?v=R5KeNOzVCP8',
				'doc_example' => 'https://noor.pixeldima.com/pricing-tables/',
			),
		)
	)
);