<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/*
* Add-on Name: Progress Bar
*/

class WPBakeryShortCode_Progress_Bar extends WPBakeryShortCode {
}

/**
 * Progress Bar.
 */
vc_map(
	array(
		'base'        => 'progress_bar',
		'name'        => esc_html__( 'Progress Bar', 'noor-assistant' ),
		'weight'      => 760,
		'class'       => 'dima-vc-element dima-vc-element-skill-bar',
		'icon'        => 'progress-bar',
		'category'    => esc_html__( 'Information', 'noor-assistant' ),
		'description' => esc_html__( 'Include an informational skill bar', 'noor-assistant' ),
		'params'      => array(
			array(
				'param_name'  => 'type',
				'heading'     => esc_html__( 'Progress Bar Style', 'noor-assistant' ),
				'description' => esc_html__( 'Select the style of your progress bar.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'dropdown',
				'value'       => array(
					esc_html__( 'Select Style', 'noor-assistant' )   => '',
					esc_html__( 'Default Style', 'noor-assistant' )  => 'style-1',
					esc_html__( 'Gradient Style', 'noor-assistant' ) => 'style-2',
				),

				'holder' => 'div'
			),
			array(
				'param_name'  => 'dima_vc_gradient',
				'heading'     => esc_html__( 'Gradient Border', 'noor-assistant' ),
				'description' => esc_html__( 'Select gradient color', 'noor-assistant' ),
				'type'        => 'dropdown',
				'value'       => array(
					esc_html__( '- Select Option -', 'noor-assistant' ) => '',
					esc_html__( 'Orange', 'noor-assistant' )            => 'v_dima_grd_orange',
					esc_html__( 'Blue', 'noor-assistant' )              => 'v_dima_grd_blue',
					esc_html__( 'Purple', 'noor-assistant' )            => 'v_dima_grd_burple',
					esc_html__( 'Green', 'noor-assistant' )             => 'v_dima_grd_green',
				),
				'dependency'  => array(
					'element' => 'type',
					'value'   => array( 'style-2' ),
				),
			),
			array(
				'param_name'  => 'heading',
				'heading'     => esc_html__( 'Progress Bar Title', 'noor-assistant' ),
				'description' => esc_html__( 'Enter the title of your progress bar.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'percent',
				'heading'     => esc_html__( 'Percent', 'noor-assistant' ),
				'description' => esc_html__( 'Enter the percentage of your progress (i.e. 77%).', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'color',
				'heading'     => esc_html__( 'Color', 'noor-assistant' ),
				'description' => esc_html__( 'Choose the color of the progress bar.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'colorpicker',
				'holder'      => 'div',
				'dependency'  => array(
					'element' => 'type',
					'value'   => array( 'style-1' ),
				),
			),
			array(
				'param_name'  => 'id',
				'heading'     => esc_html__( 'ID', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter a unique ID.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'class',
				'heading'     => esc_html__( 'Class', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter a unique class name.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'style',
				'heading'     => esc_html__( 'Style', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter inline CSS.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			//**************Animation***************//
			array(
				'param_name'  => 'animation',
				'group'       => esc_html__( 'Animation', 'noor-assistant' ),
				'heading'     => esc_html__( 'Animation', 'noor-assistant' ),
				'description' => esc_html__( 'Select the type of animation you want to use.', 'noor-assistant' ),
				'type'        => 'dropdown',
				'holder'      => 'div',
				'value'       => $dima_vc->dima_animate_list_velocity,
			),

			array(
				'param_name'  => 'delay',
				'group'       => esc_html__( 'Animation', 'noor-assistant' ),
				'heading'     => esc_html__( 'Delay Animation', 'noor-assistant' ),
				'description' => esc_html__( 'Set when the animation start (milliseconds)', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div',
				'value'       => ''
			),

			array(
				'param_name'  => 'delay_duration',
				'group'       => esc_html__( 'Animation', 'noor-assistant' ),
				'heading'     => esc_html__( 'duration Animation', 'noor-assistant' ),
				'description' => esc_html__( 'Animation duration of each element  (milliseconds).', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div',
				'value'       => ''
			),

			array(
				'param_name'  => 'delay_offset',
				'group'       => esc_html__( 'Animation', 'noor-assistant' ),
				'heading'     => esc_html__( 'Delay Animation Offset', 'noor-assistant' ),
				'description' => esc_html__( 'Set the number ( px,%..) from the top of the viewport where the animation start.', 'noor-assistant' ) . "<a href='http://imakewebthings.com/waypoints/api/offset-option/' target='_blank' >" . esc_html__( 'Read More', 'noor-assistant' ) . "</a>",
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div',
				'value'       => ''
			),
			//**************!Animation***************//
			array(
				'param_name'  => 'tutorials',
				'type'        => 'dima_doc_link_param',
				'heading'     => '<span class="tippy dima-help dima-vc-toolip">' . dima_get_svg_icon( "ic_help" )
				                 . '<span class="dima-vc-tooltip-text">'
				                 . esc_html__( 'Video tutorial and theme documentation article', 'noor-assistant' ) . '</span></span>'
				                 . esc_html__( 'Tutorial', 'noor-assistant' ),
				'doc_link'    => '',
				'video_link'  => 'https://www.youtube.com/watch?v=VA_NMC53DPc',
				'doc_example' => 'https://noor.pixeldima.com/progress-bar/',
			),

		)
	)
);
