<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/*
* Add-on Name: dima woo
*/

class WPBakeryShortCode_Woo extends WPBakeryShortCode {
}

/**
 * Woo
 */
vc_map(
	array(
		'base'        => 'dima_woo',
		'name'        => esc_html__( 'Noor Products', 'noor-assistant' ),
		'weight'      => 490,
		'class'       => 'dima-vc-element dima-vc-element-portfolio',
		'icon'        => 'shopping',
		'category'    => esc_html__( 'Content', 'noor-assistant' ),
		'description' => esc_html__( 'Show multiple products', 'noor-assistant' ),
		'params'      => array(
			array(
				'param_name'  => 'woo_style',
				'heading'     => esc_html__( 'Product Style', 'noor-assistant' ),
				'description' => esc_html__( 'Choose your style.', 'noor-assistant' ),
				'type'        => 'dropdown',
				'holder'      => 'div',
				'value'       => array(
					esc_html__( '- Select Option -', 'noor-assistant' ) => 'masonry',
					esc_html__( 'Grid', 'noor-assistant' )              => 'grid',
					esc_html__( 'List', 'noor-assistant' )              => 'list_style',
					esc_html__( 'Slide', 'noor-assistant' )             => 'slide',
				)
			),
			array(
				'param_name'  => 'woo_elm_hover',
				'heading'     => esc_html__( 'Element hover', 'noor-assistant' ),
				'description' => esc_html__( 'Select the element hover style', 'noor-assistant' ),
				'type'        => 'dropdown',
				'holder'      => 'div',
				'value'       => array(
					esc_html__( '- Select Option -', 'noor-assistant' ) => '',
					esc_html__( 'default', 'noor-assistant' )           => '',
					esc_html__( 'Inside', 'noor-assistant' )            => 'op_vc_inside',
				)
			),
			array(
				'param_name'  => 'dark',
				'heading'     => esc_html__( 'Dark background', 'noor-assistant' ),
				'description' => esc_html__( 'Check if you use dark background section', 'noor-assistant' ),
				'type'        => 'checkbox',
				'holder'      => 'div',
				'dependency'  => array(
					'element' => 'woo_style',
					'value'   => array( 'slide' ),
				),
				'value'       => array(
					'' => 'false'
				)
			),
			array(
				'param_name'  => 'auto_play',
				'heading'     => esc_html__( 'AutoPlay', 'noor-assistant' ),
				'description' => esc_html__( 'Check to animate the slides automatically', 'noor-assistant' ),
				'type'        => 'checkbox',
				'holder'      => 'div',
				'dependency'  => array(
					'element' => 'woo_style',
					'value'   => array( 'slide' ),
				),
				'value'       => array(
					'' => 'true'
				)
			),
			array(
				'param_name'  => 'slide_pagination',
				'heading'     => esc_html__( 'Slider Bullets', 'noor-assistant' ),
				'description' => esc_html__( 'Check to remove slider Bullets', 'noor-assistant' ),
				'type'        => 'checkbox',
				'holder'      => 'div',
				'dependency'  => array(
					'element' => 'woo_style',
					'value'   => array( 'slide' ),
				),
				'value'       => array(
					'' => 'false'
				)
			),
			array(
				'param_name'  => 'count',
				'heading'     => esc_html__( 'Products Count', 'noor-assistant' ),
				'description' => esc_html__( 'Select how many products display.', 'noor-assistant' ),
				'type'        => 'textfield',
				'holder'      => 'div',
				'value'       => '6',
			),
			array(
				'param_name'  => 'column',
				'heading'     => esc_html__( 'Product Columns', 'noor-assistant' ),
				'description' => esc_html__( 'Select columns number', 'noor-assistant' ),
				'type'        => 'dropdown',
				'holder'      => 'div',
				'value'       => array(
					esc_html__( '- Select Option -', 'noor-assistant' ) => '2',
					'2'                                                 => '2',
					'3'                                                 => '3',
					'4'                                                 => '4'
				)
			),
			array(
				'param_name'  => 'filters',
				'heading'     => esc_html__( 'Hide filters', 'noor-assistant' ),
				'description' => esc_html__( 'Check to remove the filters.', 'noor-assistant' ),
				'type'        => 'checkbox',
				'holder'      => 'div',
				'dependency'  => array(
					'element' => 'woo_style',
					'value'   => array( 'list_style', 'grid' ),
				),
				'value'       => array(
					'' => 'false'
				)
			),

			array(
				'param_name'  => 'hide_all',
				'heading'     => esc_html__( 'Hide All', 'noor-assistant' ),
				'description' => esc_html__( 'Check to hide "All" the first element in the filters list.', 'noor-assistant' ),
				'type'        => 'checkbox',
				'holder'      => 'div',
				'dependency'  => array(
					'element' => 'portfolio_style',
					'value'   => array( 'list_style', 'grid' ),
				),
				'value'       => array(
					'' => 'false'
				)
			),

			array(
				'param_name'  => 'category',
				'heading'     => esc_html__( 'Category', 'noor-assistant' ),
				'description' => esc_html__( 'To filter your products by category, enter in the category ID of your desired category. To filter by multiple categories, enter in your IDs separated by a comma.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'paging',
				'heading'     => esc_html__( 'Pagination', 'noor-assistant' ),
				'description' => esc_html__( 'Check to activate pagination', 'noor-assistant' ),
				'type'        => 'checkbox',
				'holder'      => 'div',
				'dependency'  => array(
					'element' => 'woo_style',
					'value'   => array( 'list_style', 'grid' ),
				),
				'value'       => array(
					'' => 'true'
				)
			),
			array(
				'param_name'  => 'orderby',
				'heading'     => esc_html__( 'Order by', 'noor-assistant' ),
				'description' => esc_html__( 'Choose what products you want to display first', 'noor-assistant' ),
				'type'        => 'dropdown',
				'holder'      => 'div',
				'dependency'  => array(
					'element' => 'woo_style',
					'value'   => array( 'list_style', 'grid' ),
				),
				'value'       => array(
					'',
					__( 'Date', 'noor-assistant' )          => 'date',
					__( 'ID', 'noor-assistant' )            => 'ID',
					__( 'Author', 'noor-assistant' )        => 'author',
					__( 'Title', 'noor-assistant' )         => 'title',
					__( 'Modified', 'noor-assistant' )      => 'modified',
					__( 'Random', 'noor-assistant' )        => 'rand',
					__( 'Comment count', 'noor-assistant' ) => 'comment_count',
					__( 'Menu order', 'noor-assistant' )    => 'menu_order',
				)
			),
			array(
				'param_name'  => 'order',
				'heading'     => esc_html__( 'Sort By', 'noor-assistant' ),
				'description' => esc_html__( 'Choose what products you want to display first', 'noor-assistant' ),
				'type'        => 'dropdown',
				'holder'      => 'div',
				'dependency'  => array(
					'element' => 'woo_style',
					'value'   => array( 'list_style', 'grid' ),
				),
				'value'       => array(
					'',
					__( 'Descending', 'noor-assistant' ) => 'DESC',
					__( 'Ascending', 'noor-assistant' )  => 'ASC',
				)
			),
			array(
				'param_name'  => 'post_class',
				'heading'     => esc_html__( 'Class', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter a unique class name.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'tutorials',
				'type'        => 'dima_doc_link_param',
				'heading'     => '<span class="tippy dima-help dima-vc-toolip">' . dima_get_svg_icon( "ic_help" )
				                 . '<span class="dima-vc-tooltip-text">'
				                 . esc_html__( 'Video tutorial and theme documentation article', 'noor-assistant' ) . '</span></span>'
				                 . esc_html__( 'Tutorial', 'noor-assistant' ),
				'doc_link'    => '',
				'video_link'  => 'https://www.youtube.com/watch?v=oaz-w7908VY',
				'doc_example' => 'https://noor.pixeldima.com/shop-three-columns-right-sidebar/',
			),
		)
	)
);