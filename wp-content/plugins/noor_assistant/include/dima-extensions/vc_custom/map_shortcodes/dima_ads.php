<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/*
* Add-on Name: Image
*/

class WPBakeryShortCode_Dima_Ads extends WPBakeryShortCode {
}

/**
 * Images
 */
vc_map(
	array(
		'base'        => 'dima_ads',
		'name'        => esc_html__( 'Ad Block', 'noor-assistant' ),
		'weight'      => 700,
		'class'       => 'dima-vc-element dima-vc-element-stream',
		'icon'        => 'stream',
		'category'    => esc_html__( 'Media', 'noor-assistant' ),
		'description' => '',
		'params'      => array(
			array(
				'param_name'  => 'src',
				'heading'     => esc_html__( 'Image', 'noor-assistant' ),
				'description' => esc_html__( 'Upload your image.', 'noor-assistant' ),
				'type'        => 'attach_image',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'href',
				'heading'     => esc_html__( 'URL Link', 'noor-assistant' ),
				'description' => esc_html__( 'If using this for a lightbox, enter the URL link of your media here (e.g. YouTube URL, Image URL,...). Leave it blank if you want to link to the image uploaded.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'alt',
				'heading'     => esc_html__( 'Alt', 'noor-assistant' ),
				'description' => esc_html__( 'Enter in the alt text for your image.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'target',
				'heading'     => esc_html__( 'Open link in a new tab', 'noor-assistant' ),
				'description' => esc_html__( 'Check to open your image link in a new tab.', 'noor-assistant' ),
				'type'        => 'checkbox',
				'holder'      => 'div',
				'value'       => array(
					'' => 'blank'
				)
			),
			array(
				'param_name'  => 'nofollow',
				'heading'     => esc_html__( 'Nofollow?', 'noor-assistant' ),
				'description' => '',
				'type'        => 'checkbox',
				'holder'      => 'div',
				'value'       => array(
					'' => 'true'
				)
			),

			array(
				'param_name'  => 'custom_ad',
				'heading'     => esc_html__( 'Custom Ad Code', 'noor-assistant' ),
				'description' => '',
				'save_always' => true,
				'type'        => 'textarea_raw_html',
				'holder'      => 'div'
			),

			array(
				'param_name'  => 'float',
				'heading'     => esc_html__( 'Image Float', 'noor-assistant' ),
				'description' => esc_html__( 'Optionally float the image.', 'noor-assistant' ),
				'type'        => 'dropdown',
				'holder'      => 'div',
				'value'       => array(
					esc_html__( '- Select Option -', 'noor-assistant' ) => '',
					esc_html__( 'Start', 'noor-assistant' )             => 'start',
					esc_html__( 'Center', 'noor-assistant' )            => 'center',
					esc_html__( 'End', 'noor-assistant' )               => 'end'
				)
			),
			array(
				'param_name'  => 'id',
				'heading'     => esc_html__( 'ID', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter a unique ID.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'class',
				'heading'     => esc_html__( 'Class', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter a unique class name.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'style',
				'heading'     => esc_html__( 'Style', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter inline CSS.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),

			//**************Animation***************//
			array(
				'param_name'  => 'animation',
				'group'       => esc_html__( 'Animation', 'noor-assistant' ),
				'heading'     => esc_html__( 'Animation', 'noor-assistant' ),
				'description' => esc_html__( 'Select the type of animation you want to use.', 'noor-assistant' ),
				'type'        => 'dropdown',
				'holder'      => 'div',
				'value'       => $dima_vc->dima_animate_list_velocity,
			),

			array(
				'param_name'  => 'delay',
				'group'       => esc_html__( 'Animation', 'noor-assistant' ),
				'heading'     => esc_html__( 'Delay Animation', 'noor-assistant' ),
				'description' => esc_html__( 'Set when the animation start (milliseconds)', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div',
				'value'       => ''
			),

			array(
				'param_name'  => 'delay_duration',
				'group'       => esc_html__( 'Animation', 'noor-assistant' ),
				'heading'     => esc_html__( 'duration Animation', 'noor-assistant' ),
				'description' => esc_html__( 'Animation duration of each element  (milliseconds).', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div',
				'value'       => ''
			),

			array(
				'param_name'  => 'delay_offset',
				'group'       => esc_html__( 'Animation', 'noor-assistant' ),
				'heading'     => esc_html__( 'Delay Animation Offset', 'noor-assistant' ),
				'description' => esc_html__( 'Set the number ( px,%..) from the top of the viewport where the animation start.', 'noor-assistant' ) . "<a href='http://imakewebthings.com/waypoints/api/offset-option/' target='_blank' >" . esc_html__( 'Read More', 'noor-assistant' ) . "</a>",
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div',
				'value'       => ''
			),
			//**************!Animation***************//
		)
	)
);