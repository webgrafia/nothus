<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/*
* Add-on Name: Meat The team
*/

class WPBakeryShortCode_Meet_The_Team extends WPBakeryShortCodesContainer {
}

/**
 * Meat The team.
 */
vc_map(
	array(
		'base'            => 'meet_the_team',
		'name'            => esc_html__( 'Team member', 'noor-assistant' ),
		'weight'          => 960,
		'class'           => 'dima-vc-element dima-vc-element-team',
		'icon'            => 'team',
		'as_parent'       => array( 'only' => 'inline_item_icon' ),
		'content_element' => true,
		'category'        => esc_html__( 'Content', 'noor-assistant' ),
		'description'     => esc_html__( 'Add team members in your content', 'noor-assistant' ),
		'params'          => array(
			array(
				'param_name'  => 'name',
				'heading'     => esc_html__( 'Member Name', 'noor-assistant' ),
				'description' => esc_html__( 'Enter the member\'s name', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'description',
				'heading'     => esc_html__( 'Member Description', 'noor-assistant' ),
				'description' => esc_html__( 'Enter the member\'s description', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),

			array(
				'param_name'  => 'job',
				'heading'     => esc_html__( 'Member Position', 'noor-assistant' ),
				'description' => esc_html__( 'Enter the member\'s position', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),

			array(
				'param_name'  => 'link',
				'heading'     => esc_html__( 'Member Link', 'noor-assistant' ),
				'description' => esc_html__( 'Enter the member\'s page URL', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),

			array(
				'param_name'  => 'target',
				'heading'     => esc_html__( 'Open link in a new tab', 'noor-assistant' ),
				'description' => esc_html__( 'Check to open your image link in a new tab.', 'noor-assistant' ),
				'type'        => 'toggle',
				'holder'      => 'div',
				'value'       => array(
					'' => 'false'
				)
			),

			array(
				'param_name'  => 'image',
				'heading'     => esc_html__( 'Member Photo', 'noor-assistant' ),
				'description' => esc_html__( 'Add member\'s photo', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'attach_image',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'bg_color',
				'group'       => esc_html__( 'Style', 'noor-assistant' ),
				'heading'     => esc_html__( 'Background Color', 'noor-assistant' ),
				'description' => esc_html__( 'Choose background color for the text', 'noor-assistant' ),
				'type'        => 'colorpicker',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'name_color',
				'group'       => esc_html__( 'Style', 'noor-assistant' ),
				'heading'     => esc_html__( 'Name Text Color', 'noor-assistant' ),
				'description' => esc_html__( 'Choose color for the text', 'noor-assistant' ),
				'type'        => 'colorpicker',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'text_color',
				'group'       => esc_html__( 'Style', 'noor-assistant' ),
				'heading'     => esc_html__( 'Description Text Color', 'noor-assistant' ),
				'description' => esc_html__( 'Choose color for the text', 'noor-assistant' ),
				'type'        => 'colorpicker',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'boxed',
				'group'       => esc_html__( 'Style', 'noor-assistant' ),
				'heading'     => esc_html__( 'Boxed', 'noor-assistant' ),
				'description' => esc_html__( 'Check to display the member\'s content in a box.', 'noor-assistant' ),
				'type'        => 'toggle',
				'holder'      => 'div',
				'value'       => 'false'
			),
			array(
				'param_name'  => 'img_hover',
				'group'       => esc_html__( 'Style', 'noor-assistant' ),
				'heading'     => esc_html__( 'Featured Image hover', 'noor-assistant' ),
				'description' => esc_html__( 'Select the image hover style', 'noor-assistant' ),
				'type'        => 'dropdown',
				'holder'      => 'div',
				'value'       => array(
					esc_html__( '- Select Option -', 'noor-assistant' ) => '',
					esc_html__( 'none', 'noor-assistant' )              => 'op_vc_none',
					esc_html__( 'default', 'noor-assistant' )           => '',
					esc_html__( 'Opacity', 'noor-assistant' )           => 'op_vc_opacity',
					esc_html__( 'zoom-out', 'noor-assistant' )          => 'op_vc_zoom-out',
					esc_html__( 'zoom-in', 'noor-assistant' )           => 'op_vc_zoom-in',
					esc_html__( 'gray', 'noor-assistant' )              => 'op_vc_gray',
				)
			),
			array(
				'param_name'  => 'elm_hover',
				'group'       => esc_html__( 'Style', 'noor-assistant' ),
				'heading'     => esc_html__( 'Element hover', 'noor-assistant' ),
				'description' => esc_html__( 'Select the element hover style', 'noor-assistant' ),
				'type'        => 'dropdown',
				'holder'      => 'div',
				'value'       => array(
					esc_html__( '- Select Option -', 'noor-assistant' ) => '',
					esc_html__( 'default', 'noor-assistant' )           => '',
					esc_html__( 'Inside', 'noor-assistant' )            => 'op_vc_inside',
					esc_html__( 'none', 'noor-assistant' )              => 'op_vc_none',
				)
			),
			array(
				'param_name'  => 'id',
				'heading'     => esc_html__( 'ID', 'noor-assistant' ),
				'description' => $dima_vc->id_des,
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'class',
				'heading'     => esc_html__( 'Class', 'noor-assistant' ),
				'description' => $dima_vc->class_des,
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'style',
				'heading'     => esc_html__( 'Style', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter inline CSS.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			//**************Animation***************//
			array(
				'param_name'  => 'animation',
				'group'       => esc_html__( 'Animation', 'noor-assistant' ),
				'heading'     => esc_html__( 'Animation', 'noor-assistant' ),
				'description' => esc_html__( 'Select the type of animation you want to use.', 'noor-assistant' ),
				'type'        => 'dropdown',
				'holder'      => 'div',
				'value'       => $dima_vc->dima_animate_list_velocity,
			),

			array(
				'param_name'  => 'delay',
				'group'       => esc_html__( 'Animation', 'noor-assistant' ),
				'heading'     => esc_html__( 'Delay Animation', 'noor-assistant' ),
				'description' => esc_html__( 'Set when the animation start (milliseconds)', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div',
				'value'       => ''
			),

			array(
				'param_name'  => 'delay_duration',
				'group'       => esc_html__( 'Animation', 'noor-assistant' ),
				'heading'     => esc_html__( 'duration Animation', 'noor-assistant' ),
				'description' => esc_html__( 'Animation duration of each element  (milliseconds).', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div',
				'value'       => ''
			),

			array(
				'param_name'  => 'delay_offset',
				'group'       => esc_html__( 'Animation', 'noor-assistant' ),
				'heading'     => esc_html__( 'Delay Animation Offset', 'noor-assistant' ),
				'description' => esc_html__( 'Set the number ( px,%..) from the top of the viewport where the animation start.', 'noor-assistant' ) . "<a href='http://imakewebthings.com/waypoints/api/offset-option/' target='_blank' >" . esc_html__( 'Read More', 'noor-assistant' ) . "</a>",
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div',
				'value'       => ''
			),
			//**************!Animation***************//
			array(
				'param_name'  => 'tutorials',
				'type'        => 'dima_doc_link_param',
				'heading'     => '<span class="tippy dima-help dima-vc-toolip">' . dima_get_svg_icon( "ic_help" )
				                 . '<span class="dima-vc-tooltip-text">'
				                 . esc_html__( 'Video tutorial and theme documentation article', 'noor-assistant' ) . '</span></span>'
				                 . esc_html__( 'Tutorial', 'noor-assistant' ),
				'doc_link'    => '',
				'video_link'  => 'https://www.youtube.com/watch?v=UHJInee7VGU',
				'doc_example' => 'https://noor.pixeldima.com/team/',
			),
		),
		'js_view'         => 'VcColumnView',

	)
);