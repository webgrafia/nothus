<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/*
* Add-on Name: Protected content
*/

class WPBakeryShortCode_Protect extends WPBakeryShortCode {
}

/**
 * Protected content
 */
vc_map(
	array(
		'base'        => 'protect',
		'name'        => esc_html__( 'Protect', 'noor-assistant' ),
		'weight'      => 840,
		'class'       => 'dima-vc-element dima-vc-element-protect',
		'icon'        => 'protect',
		'category'    => esc_html__( 'Content', 'noor-assistant' ),
		'description' => esc_html__( 'Protect content from non logged in users', 'noor-assistant' ),
		'params'      => array(
			array(
				'param_name'  => 'content',
				'heading'     => esc_html__( 'Text', 'noor-assistant' ),
				'description' => esc_html__( 'Enter your text.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textarea_html',
				'holder'      => 'div',
				'value'       => ''
			),
			array(
				'param_name'  => 'id',
				'heading'     => esc_html__( 'ID', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter a unique ID.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'class',
				'heading'     => esc_html__( 'Class', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter a unique class name.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'style',
				'heading'     => esc_html__( 'Style', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter inline CSS.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			)
		)
	)
);