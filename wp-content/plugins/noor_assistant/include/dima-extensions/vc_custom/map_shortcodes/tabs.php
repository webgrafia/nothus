<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/*
* Add-on Name: Tabs
*/

class WPBakeryShortCode_Tab_Nav extends WPBakeryShortCodesContainer {
}

class WPBakeryShortCode_Tabs extends WPBakeryShortCodesContainer {
}

/**
 * Tabs
 */
vc_map(
	array(
		'base'            => 'tabs',
		'name'            => esc_html__( 'Tabs', 'noor-assistant' ),
		'weight'          => 900,
		'class'           => 'dima-vc-element dima-vc-element-tabs',
		'icon'            => 'tabs',
		'category'        => esc_html__( 'Content', 'noor-assistant' ),
		'description'     => esc_html__( 'Include a tabs container after your tab nav', 'noor-assistant' ),
		'as_parent'       => array( 'only' => 'tab' ),
		'content_element' => true,
		'js_view'         => 'VcColumnView',
		'params'          => array(
			array(
				'param_name'  => 'id',
				'heading'     => esc_html__( 'ID', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter a unique ID.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'class',
				'heading'     => esc_html__( 'Class', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter a unique class name.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'style',
				'heading'     => esc_html__( 'Style', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter inline CSS.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			)
		)
	)
);


/**
 * Tab
 */
vc_map(
	array(
		'base'            => 'tab',
		'name'            => esc_html__( 'Tab', 'noor-assistant' ),
		'weight'          => 890,
		'class'           => 'dima-vc-element dima-vc-element-tab',
		'icon'            => 'tabs',
		'category'        => esc_html__( 'Content', 'noor-assistant' ),
		'description'     => esc_html__( 'Include a tab into your tabs container', 'noor-assistant' ),
		'as_child'        => array( 'only' => 'tabs' ),
		'content_element' => true,
		'params'          => array(
			array(
				'param_name'  => 'content',
				'heading'     => esc_html__( 'Text', 'noor-assistant' ),
				'description' => esc_html__( 'Enter your text.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textarea_html',
				'holder'      => 'div',
				'value'       => ''
			),
			array(
				'param_name'  => 'active',
				'heading'     => esc_html__( 'Active', 'noor-assistant' ),
				'description' => esc_html__( 'Select to make this tab active.', 'noor-assistant' ),
				'type'        => 'checkbox',
				'holder'      => 'div',
				'value'       => array(
					'' => 'true'
				)
			),
			array(
				'param_name'  => 'class',
				'heading'     => esc_html__( 'Class', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter a unique class name.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'style',
				'heading'     => esc_html__( 'Style', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter inline CSS.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'tutorials',
				'type'        => 'dima_doc_link_param',
				'heading'     => '<span class="tippy dima-help dima-vc-toolip">' . dima_get_svg_icon( "ic_help" )
				                 . '<span class="dima-vc-tooltip-text">'
				                 . esc_html__( 'Video tutorial and theme documentation article', 'noor-assistant' ) . '</span></span>'
				                 . esc_html__( 'Tutorial', 'noor-assistant' ),
				'doc_link'    => '',
				'video_link'  => 'https://www.youtube.com/watch?v=oxjOt0-Bpi8',
				'doc_example' => 'https://noor.pixeldima.com/tabs/',
			),
		)
	)
);


/**
 * Tab nav
 */
vc_map(
	array(
		'base'            => 'tab_nav',
		'name'            => esc_html__( 'Tab Nav', 'noor-assistant' ),
		'weight'          => 920,
		'class'           => 'dima-vc-element dima-vc-element-tab-nav',
		'icon'            => 'tabs',
		'category'        => esc_html__( 'Content', 'noor-assistant' ),
		'description'     => esc_html__( 'Include a tab nav into your content', 'noor-assistant' ),
		'as_parent'       => array( 'only' => 'tab_nav_item' ),
		'content_element' => true,
		'js_view'         => 'VcColumnView',
		'params'          => array(
			array(
				'param_name'  => 'model',
				'heading'     => esc_html__( 'Tab Style', 'noor-assistant' ),
				'description' => esc_html__( 'Select your tab nav style', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'dropdown',
				'value'       => array(
					esc_html__( 'Select Option', 'noor-assistant' ) => 'tabs_style_1',
					esc_html__( 'Style 1', 'noor-assistant' )       => 'tabs_style_1',
					esc_html__( 'Style 2', 'noor-assistant' )       => 'tabs_style_2',
					esc_html__( 'Style 3', 'noor-assistant' )       => 'tabs_style_3',
					esc_html__( 'Style 4', 'noor-assistant' )       => 'tabs_style_4',
					esc_html__( 'Style 5', 'noor-assistant' )       => 'tabs_style_5',
				),
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'float',
				'heading'     => esc_html__( 'Tab Nav Position', 'noor-assistant' ),
				'description' => esc_html__( 'Select the position of your tab nav.', 'noor-assistant' ),
				'type'        => 'dropdown',
				'holder'      => 'div',
				'dependency'  => array(
					'element' => 'model',
					'value'   => array( 'tabs_style_1', 'tabs_style_2', 'tabs_style_3', 'tabs_style_4' )
				),
				'value'       => array(
					esc_html__( 'Horizontal', 'noor-assistant' )     => '',
					esc_html__( 'Start Vertical', 'noor-assistant' ) => 'start',
					esc_html__( 'End Vertical', 'noor-assistant' )   => 'end'
				)
			),
			array(
				'param_name'  => 'clm',
				'heading'     => esc_html__( 'Tab Nav Items Per Row', 'noor-assistant' ),
				'description' => esc_html__( 'If your tab nav is on top, select how many tab nav items you want per row.', 'noor-assistant' ),
				'type'        => 'dropdown',
				'holder'      => 'div',
				'dependency'  => array(
					'element' => 'model',
					'value'   => array( 'tabs_style_1', 'tabs_style_2', 'tabs_style_3', 'tabs_style_4' ),
				),
				'value'       => array(
					esc_html__( 'Select Option', 'noor-assistant' ) => '',
					esc_html__( 'Two', 'noor-assistant' )           => 'columns-2-tab',
					esc_html__( 'Three', 'noor-assistant' )         => 'columns-3-tab',
					esc_html__( 'Four', 'noor-assistant' )          => 'columns-4-tab',
					esc_html__( 'Five', 'noor-assistant' )          => 'columns-5-tab',
					esc_html__( 'Six', 'noor-assistant' )           => 'columns-6-tab'
				)
			),
			array(
				'param_name'  => 'id',
				'heading'     => esc_html__( 'ID', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter a unique ID.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'class',
				'heading'     => esc_html__( 'Class', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter a unique class name.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'style',
				'heading'     => esc_html__( 'Style', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter inline CSS.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'tutorials',
				'type'        => 'dima_doc_link_param',
				'heading'     => '<span class="tippy dima-help dima-vc-toolip">' . dima_get_svg_icon( "ic_help" )
				                 . '<span class="dima-vc-tooltip-text">'
				                 . esc_html__( 'Video tutorial and theme documentation article', 'noor-assistant' ) . '</span></span>'
				                 . esc_html__( 'Tutorial', 'noor-assistant' ),
				'doc_link'    => '',
				'video_link'  => 'https://www.youtube.com/watch?v=oxjOt0-Bpi8',
				'doc_example' => 'https://noor.pixeldima.com/tabs/',
			),
		)
	)
);

/**
 * Tab nav item
 */
vc_map(
	array(
		'base'            => 'tab_nav_item',
		'name'            => esc_html__( 'Tab Nav Item', 'noor-assistant' ),
		'weight'          => 910,
		'class'           => 'dima-vc-element dima-vc-element-tab-nav-item',
		'icon'            => 'tabs',
		'category'        => esc_html__( 'Content', 'noor-assistant' ),
		'description'     => esc_html__( 'Include a tab nav item into your tab nav', 'noor-assistant' ),
		'as_child'        => array( 'only' => 'tab_nav' ),
		'content_element' => true,

		'params' => array(
			array(
				'param_name'  => 'title',
				'heading'     => esc_html__( 'Title', 'noor-assistant' ),
				'description' => esc_html__( 'Include a title for your tab nav item.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'active',
				'heading'     => esc_html__( 'Active', 'noor-assistant' ),
				'description' => esc_html__( 'Check to make this tab nav item active.', 'noor-assistant' ),
				'type'        => 'checkbox',
				'holder'      => 'div',
				'value'       => array(
					'' => 'true'
				)
			),
			array(
				'type'        => 'dropdown',
				'heading'     => esc_html__( 'Icons Library', 'noor-assistant' ),
				'value'       => array(
					esc_html__( 'Font Awesome', 'noor-assistant' )                                     => 'fontawesome',
					esc_html__( 'Custom Font', 'noor-assistant' )                                      => 'customfont',
					esc_html__( 'SVG Icons (Stroke Gap Icons And Material Icons) ', 'noor-assistant' ) => 'svgicon',
				),
				'param_name'  => 'icon_type',
				'description' => esc_html__( 'Select icons library.', 'noor-assistant' ),
			),
			array(
				'param_name'  => 'icon_svg',
				'heading'     => esc_html__( 'Icon SVG', 'noor-assistant' ),
				'description' => esc_html__( 'Enter SVG Icon name', 'noor-assistant' ),
				'value'       => '',
				'dependency'  => array(
					'element' => 'icon_type',
					'value'   => 'svgicon',
				),
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'icon',
				'heading'     => esc_html__( 'Icon Class', 'noor-assistant' ),
				'description' => esc_html__( 'Enter Icon Class from Font Awesome or Simple Line Icons.', 'noor-assistant' ),
				'value'       => 'simple-icon-user',
				'dependency'  => array(
					'element' => 'icon_type',
					'value'   => 'customfont',
				),
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'type'        => 'iconpicker',
				'heading'     => esc_html__( 'Icon', 'noor-assistant' ),
				'param_name'  => 'icon_fontawesome',
				'value'       => '', // default value to backend editor admin_label
				'settings'    => array(
					'emptyIcon'    => true,
					// default true, display an "EMPTY" icon?
					'value'        => 'fontawesome',
					'iconsPerPage' => 4000,
					// default 100, how many icons per/page to display, we use (big number) to display all icons in single page
				),
				'dependency'  => array(
					'element' => 'icon_type',
					'value'   => 'fontawesome',
				),
				'description' => esc_html__( 'Select icon from library.', 'noor-assistant' ),
			),
			array(
				'param_name'  => 'id',
				'heading'     => esc_html__( 'ID', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter a unique ID.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'class',
				'heading'     => esc_html__( 'Class', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter a unique class name.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'style',
				'heading'     => esc_html__( 'Style', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter inline CSS.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'tutorials',
				'type'        => 'dima_doc_link_param',
				'heading'     => '<span class="tippy dima-help dima-vc-toolip">' . dima_get_svg_icon( "ic_help" )
				                 . '<span class="dima-vc-tooltip-text">'
				                 . esc_html__( 'Video tutorial and theme documentation article', 'noor-assistant' ) . '</span></span>'
				                 . esc_html__( 'Tutorial', 'noor-assistant' ),
				'doc_link'    => '',
				'video_link'  => 'https://www.youtube.com/watch?v=oxjOt0-Bpi8',
				'doc_example' => 'https://noor.pixeldima.com/tabs/',
			),
		)
	)
);