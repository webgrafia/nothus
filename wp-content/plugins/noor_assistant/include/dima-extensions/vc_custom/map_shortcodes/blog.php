<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/*
* Add-on Name: Blog posts
*/

class WPBakeryShortCode_Blog extends WPBakeryShortCode {
}

/**
 * Blog
 */
vc_map(
	array(
		'base'        => 'blog',
		'name'        => esc_html__( 'Blog', 'noor-assistant' ),
		'weight'      => 490,
		'class'       => 'dima-vc-element dima-vc-element-recent-posts',
		'icon'        => 'blog',
		'category'    => esc_html__( 'Content', 'noor-assistant' ),
		'description' => esc_html__( 'Display your blog posts or recent posts', 'noor-assistant' ),
		'params'      => array(
			array(
				'param_name'  => 'blog_style',
				'heading'     => esc_html__( 'Blog Style', 'noor-assistant' ),
				'description' => esc_html__( 'Choose your blog style to show.', 'noor-assistant' ),
				'type'        => 'dropdown',
				'holder'      => 'div',
				'value'       => array(
					esc_html__( '- Select Option -', 'noor-assistant' ) => 'standard',
					esc_html__( 'Standard', 'noor-assistant' )          => 'standard',
					esc_html__( 'Masonry', 'noor-assistant' )           => 'masonry',
					esc_html__( 'Grid', 'noor-assistant' )              => 'grid',
					esc_html__( 'Minimal', 'noor-assistant' )           => 'minimal',
					esc_html__( 'Minimal No Margin', 'noor-assistant' ) => 'minimal_no_margin',
					esc_html__( 'Minimal Slide', 'noor-assistant' )     => 'minimalslide',
					esc_html__( 'Timeline', 'noor-assistant' )          => 'timeline',
					esc_html__( 'Slide', 'noor-assistant' )             => 'slide',
				)
			),
			array(
				'param_name'  => 'dark',
				'heading'     => esc_html__( 'Dark background', 'noor-assistant' ),
				'description' => esc_html__( 'Check if you use dark background section', 'noor-assistant' ),
				'type'        => 'checkbox',
				'holder'      => 'div',
				'dependency'  => array(
					'element' => 'blog_style',
					'value'   => array( 'minimalslide', 'slide' ),
				),
				'value'       => array(
					'' => 'true'
				)
			),
			array(
				'param_name'  => 'count',
				'heading'     => esc_html__( 'Post Count', 'noor-assistant' ),
				'description' => esc_html__( 'Type how many posts to display.', 'noor-assistant' ),
				'type'        => 'textfield',
				'holder'      => 'div',
			),
			array(
				'param_name'  => 'column',
				'heading'     => esc_html__( 'Post Columns', 'noor-assistant' ),
				'description' => esc_html__( 'Select columns number', 'noor-assistant' ),
				'type'        => 'dropdown',
				'holder'      => 'div',
				'dependency'  => array(
					'element' => 'blog_style',
					'value'   => array(
						'grid',
						'minimal',
						'masonry',
						'minimal_no_margin',
						'slide',
						'minimalslide'
					),
				),
				'value'       => array(
					esc_html__( '- Select Option -', 'noor-assistant' ) => '',
					'1'                                                 => '1',
					'2'                                                 => '2',
					'3'                                                 => '3',
					'4'                                                 => '4'
				)
			),
			array(
				'param_name'  => 'dots',
				'heading'     => esc_html__( 'Display dots', 'noor-assistant' ),
				'description' => esc_html__( 'Check to display dots', 'noor-assistant' ),
				'type'        => 'checkbox',
				'holder'      => 'div',
				'dependency'  => array(
					'element' => 'blog_style',
					'value'   => array(
						'slide',
						'minimalslide',
					),
				),
				'value'       => array(
					'' => 'true'
				)
			),
			array(
				'param_name'  => 'show_meta',
				'heading'     => esc_html__( 'Hide Meta box', 'noor-assistant' ),
				'description' => esc_html__( 'Check to hide the metabox.', 'noor-assistant' ),
				'type'        => 'checkbox',
				'holder'      => 'div',
				'dependency'  => array(
					'element' => 'blog_style',
					'value'   => array(
						'grid',
						'standard',
						'masonry',
						''
					),
				),
				'value'       => array(
					'' => 'false'
				)
			),
			array(
				'param_name'  => 'img_hover',
				'heading'     => esc_html__( 'Featured Image hover', 'noor-assistant' ),
				'description' => esc_html__( 'Select the image hover style', 'noor-assistant' ),
				'type'        => 'dropdown',
				'holder'      => 'div',
				'value'       => array(
					esc_html__( '- Select Option -', 'noor-assistant' ) => '',
					esc_html__( 'none', 'noor-assistant' )              => 'op_vc_none',
					esc_html__( 'default', 'noor-assistant' )           => '',
					esc_html__( 'Opacity', 'noor-assistant' )           => 'op_vc_opacity',
					esc_html__( 'zoom-out', 'noor-assistant' )          => 'op_vc_zoom-out',
					esc_html__( 'zoom-in', 'noor-assistant' )           => 'op_vc_zoom-in',
					esc_html__( 'gray', 'noor-assistant' )              => 'op_vc_gray',
				)
			),
			array(
				'param_name'  => 'elm_hover',
				'heading'     => esc_html__( 'Element hover', 'noor-assistant' ),
				'description' => esc_html__( 'Select the element hover style', 'noor-assistant' ),
				'type'        => 'dropdown',
				'holder'      => 'div',
				'value'       => array(
					esc_html__( '- Select Option -', 'noor-assistant' ) => '',
					esc_html__( 'default', 'noor-assistant' )           => '',
					esc_html__( 'Inside', 'noor-assistant' )            => 'op_vc_inside',
					esc_html__( 'none', 'noor-assistant' )              => 'op_vc_none',
				)
			),
			array(
				'param_name'  => 'words',
				'heading'     => esc_html__( 'Excerpts length', 'noor-assistant' ),
				'description' => esc_html__( 'Enter number of words in excerpt', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'number',
				'std'         => 100,
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'items_margin',
				'heading'     => esc_html__( 'Add Margin', 'noor-assistant' ),
				'description' => esc_html__( 'Check to add Margin between item', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'checkbox',
				'holder'      => 'div',
				'dependency'  => array(
					'element' => 'blog_style',
					'value'   => array(
						'minimalslide',
						'slide',
					),
				),
				'value'       => array(
					'' => 'false'
				)
			),
			array(
				'param_name'  => 'category',
				'heading'     => esc_html__( 'Category', 'noor-assistant' ),
				'description' => esc_html__( 'To filter your posts by category, enter the slug of your desired category. To filter by multiple categories, enter your slugs separated by a comma.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'tag',
				'heading'     => esc_html__( 'Tag', 'noor-assistant' ),
				'description' => esc_html__( 'To filter your posts by tag, enter the slug of your desired tag. To filter by multiple tags, enter your slugs separated by a comma.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'offset',
				'heading'     => esc_html__( 'Offset', 'noor-assistant' ),
				'description' => esc_html__( 'Accepts a numerical value to show how many posts to skip.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'paging',
				'heading'     => esc_html__( 'Display Pagination', 'noor-assistant' ),
				'description' => esc_html__( 'Check to display pagination', 'noor-assistant' ),
				'type'        => 'checkbox',
				'holder'      => 'div',
				'value'       => array(
					'' => 'true'
				),
				'dependency'  => array(
					'element' => 'blog_style',
					'value'   => array(
						'grid',
						'standard',
						'masonry',
						'minimal',
						'minimal_no_margin',
						'timeline',
						''
					),
				),
			),
			array(
				'param_name'  => 'order',
				'heading'     => esc_html__( 'Sort by', 'noor-assistant' ),
				'description' => esc_html__( 'Choose what posts you want to display first', 'noor-assistant' ),
				'type'        => 'dropdown',
				'holder'      => 'div',
				'value'       => array(
					esc_html__( '- Select Option -', 'noor-assistant' ) => '',
					esc_html__( 'Popular', 'noor-assistant' )           => 'popular',
					esc_html__( 'Top view', 'noor-assistant' )          => 'top-view',
				)
			),
			array(
				'param_name'  => 'show_image',
				'heading'     => esc_html__( 'Featured Image', 'noor-assistant' ),
				'description' => esc_html__( 'Check to remove the featured image.', 'noor-assistant' ),
				'type'        => 'checkbox',
				'holder'      => 'div',
				'dependency'  => array(
					'element' => 'blog_style',
					'value'   => array(
						'grid',
						'standard',
						'masonry',
						''
					),
				),
				'value'       => array(
					'' => 'false'
				)
			),
			array(
				'param_name'  => 'id',
				'heading'     => esc_html__( 'ID', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter a unique ID.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'post_class',
				'heading'     => esc_html__( 'Class', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter a unique class name.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'style',
				'heading'     => esc_html__( 'Style', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter inline CSS.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'tutorials',
				'type'        => 'dima_doc_link_param',
				'heading'     => '<span class="tippy dima-help dima-vc-toolip">' . dima_get_svg_icon( "ic_help" )
				                 . '<span class="dima-vc-tooltip-text">'
				                 . esc_html__( 'Video tutorial and theme documentation article', 'noor-assistant' ) . '</span></span>'
				                 . esc_html__( 'Tutorial', 'noor-assistant' ),
				'doc_link'    => '',
				'video_link'  => 'https://www.youtube.com/watch?v=_A8EbHgCXb0',
				'doc_example' => 'https://noor.pixeldima.com/posts-list-left-sidebar/',
			),
		)
	)
);
