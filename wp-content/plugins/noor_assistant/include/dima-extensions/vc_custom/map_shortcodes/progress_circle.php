<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/*
* Add-on Name: circle Bar
*/

class WPBakeryShortCode_Progress_Circle extends WPBakeryShortCode {
}

/**
 * circle Bar.
 */
vc_map(
	array(
		'base'        => 'progress_circle',
		'name'        => esc_html__( 'Progress Circle', 'noor-assistant' ),
		'weight'      => 760,
		'class'       => 'dima-vc-element dima-vc-element-skill-bar',
		'icon'        => 'progress-circle',
		'category'    => esc_html__( 'Information', 'noor-assistant' ),
		'description' => esc_html__( 'Include an informational skill bar', 'noor-assistant' ),
		'params'      => array(
			array(
				'param_name'  => 'heading',
				'heading'     => esc_html__( 'Progress Circle Title', 'noor-assistant' ),
				'description' => esc_html__( 'Enter the title of your progress circle.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'percent',
				'heading'     => esc_html__( 'Percent', 'noor-assistant' ),
				'description' => esc_html__( 'Enter the percentage of your progress (i.e. 77%).', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'color',
				'group'       => esc_html__( 'Style', 'noor-assistant' ),
				'heading'     => esc_html__( 'Progress circle color', 'noor-assistant' ),
				'description' => esc_html__( 'Choose the color of the progress circle', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'colorpicker',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'color_bg',
				'group'       => esc_html__( 'Style', 'noor-assistant' ),
				'heading'     => esc_html__( 'Progress circle background color', 'noor-assistant' ),
				'description' => esc_html__( 'Choose the background color', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'colorpicker',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'color_border',
				'group'       => esc_html__( 'Style', 'noor-assistant' ),
				'heading'     => esc_html__( 'Progress circle border color', 'noor-assistant' ),
				'description' => esc_html__( 'Choose the border color', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'colorpicker',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'color_icon',
				'group'       => esc_html__( 'Style', 'noor-assistant' ),
				'heading'     => esc_html__( 'Icon color', 'noor-assistant' ),
				'description' => esc_html__( 'Choose the icon color', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'colorpicker',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'Percent_txt',
				'group'       => esc_html__( 'Style', 'noor-assistant' ),
				'heading'     => esc_html__( 'Percent color', 'noor-assistant' ),
				'description' => esc_html__( 'Choose the percent color', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'colorpicker',
				'holder'      => 'div'
			),
			array(
				'type'        => 'dropdown',
				'heading'     => esc_html__( 'Icons library', 'noor-assistant' ),
				'value'       => array(
					esc_html__( 'Font Awesome', 'noor-assistant' )                                     => 'fontawesome',
					esc_html__( 'Custom Font', 'noor-assistant' )                                      => 'customfont',
					esc_html__( 'SVG Icons (Stroke Gap Icons And Material Icons) ', 'noor-assistant' ) => 'svgicon',
				),
				'param_name'  => 'icon_type',
				'description' => esc_html__( 'Select icons library.', 'noor-assistant' ),
			),
			array(
				'param_name'  => 'icon',
				'heading'     => esc_html__( 'Icon Class', 'noor-assistant' ),
				'description' => esc_html__( 'Enter Icon Class from Font Awesome or Simple Line Icons.', 'noor-assistant' ),
				'value'       => 'simple-icon-user',
				'dependency'  => array(
					'element' => 'icon_type',
					'value'   => 'customfont',
				),
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'icon_svg',
				'heading'     => esc_html__( 'Icon SVG', 'noor-assistant' ),
				'description' => esc_html__( 'Enter SVG Icon name', 'noor-assistant' ),
				'value'       => '',
				'dependency'  => array(
					'element' => 'icon_type',
					'value'   => 'svgicon',
				),
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'type'        => 'iconpicker',
				'heading'     => esc_html__( 'Icon', 'noor-assistant' ),
				'param_name'  => 'icon_fontawesome',
				'value'       => '', // default value to backend editor admin_label
				'settings'    => array(
					'emptyIcon'    => true,
					// default true, display an "EMPTY" icon?
					'value'        => 'fontawesome',
					'iconsPerPage' => 4000,
					// default 100, how many icons per/page to display, we use (big number) to display all icons in single page
				),
				'dependency'  => array(
					'element' => 'icon_type',
					'value'   => 'fontawesome',
				),
				'description' => esc_html__( 'Select icon from library.', 'noor-assistant' ),
			),

			array(
				'param_name'  => 'id',
				'heading'     => esc_html__( 'ID', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter a unique ID.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'class',
				'heading'     => esc_html__( 'Class', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter a unique class name.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'style',
				'heading'     => esc_html__( 'Style', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter inline CSS.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'tutorials',
				'type'        => 'dima_doc_link_param',
				'heading'     => '<span class="tippy dima-help dima-vc-toolip">' . dima_get_svg_icon( "ic_help" )
				                 . '<span class="dima-vc-tooltip-text">'
				                 . esc_html__( 'Video tutorial and theme documentation article', 'noor-assistant' ) . '</span></span>'
				                 . esc_html__( 'Tutorial', 'noor-assistant' ),
				'doc_link'    => '',
				'video_link'  => 'https://youtu.be/VA_NMC53DPc?t=127',
				'doc_example' => 'https://noor.pixeldima.com/progress-bar/',
			),

		)
	)
);