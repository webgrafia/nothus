<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/*
* Add-on Name: Line
*/

class WPBakeryShortCode_Dima_Audio_Player extends WPBakeryShortCode {
}

/**
 * Audio player
 */
vc_map(
	array(
		'base'        => 'dima_audio_player',
		'name'        => esc_html__( 'Audio (Self Hosted)', 'noor-assistant' ),
		'weight'      => 550,
		'class'       => 'dima-vc-element dima-vc-element-audio-player',
		'icon'        => 'audio-player',
		'category'    => esc_html__( 'Media', 'noor-assistant' ),
		'description' => esc_html__( 'Place audio files into your content', 'noor-assistant' ),
		'params'      => array(
			array(
				'param_name'  => 'mp3',
				'heading'     => esc_html__( 'MP3', 'noor-assistant' ),
				'description' => esc_html__( 'Enter URL Link .mp3 version of your audio.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'oga',
				'heading'     => esc_html__( 'OGA', 'noor-assistant' ),
				'description' => esc_html__( 'Enter URL Link .oga version of your audio for additional native browser support.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'loop',
				'heading'     => esc_html__( 'Auto Replay', 'noor-assistant' ),
				'description' => esc_html__( 'Check to activate auto replay for your self-hosted audio', 'noor-assistant' ),
				'type'        => 'checkbox',
				'holder'      => 'div',
				'value'       => array(
					'' => 'true'
				)
			),
			array(
				'param_name'  => 'autoplay',
				'heading'     => esc_html__( 'Autoplay', 'noor-assistant' ),
				'description' => esc_html__( 'Check to automatically play your self-hosted audio.', 'noor-assistant' ),
				'type'        => 'checkbox',
				'holder'      => 'div',
				'value'       => array(
					'' => 'true'
				)
			),
			array(
				'param_name'  => 'id',
				'heading'     => esc_html__( 'ID', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter a unique ID.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'class',
				'heading'     => esc_html__( 'Class', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter a unique class name.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'style',
				'heading'     => esc_html__( 'Style', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter inline CSS.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'tutorials',
				'type'        => 'dima_doc_link_param',
				'heading'     => '<span class="tippy dima-help dima-vc-toolip">' . dima_get_svg_icon( "ic_help" )
				                 . '<span class="dima-vc-tooltip-text">'
				                 . esc_html__( 'Video tutorial and theme documentation article', 'noor-assistant' ) . '</span></span>'
				                 . esc_html__( 'Tutorial', 'noor-assistant' ),
				'doc_link'    => '',
				'video_link'  => 'https://www.youtube.com/watch?v=y3O3G7PbhMw',
				'doc_example' => 'https://noor.pixeldima.com/multimedia/',
			),

		)
	)
);
