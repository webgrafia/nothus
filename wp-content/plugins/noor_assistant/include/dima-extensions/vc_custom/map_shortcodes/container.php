<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/*
* Add-on Name: Container
*/

class WPBakeryShortCode_Container extends WPBakeryShortCodesContainer {
}


/**
 * Container.
 */
vc_map(
	array(
		'base'            => 'container',
		'name'            => esc_html__( 'Container', 'noor-assistant' ),
		'weight'          => 990,
		'class'           => 'dima-vc-element',
		'icon'            => 'container',
		'category'        => esc_html__( 'Structure', 'noor-assistant' ),
		'description'     => esc_html__( 'Include a container in your content to work with a custom columns', 'noor-assistant' ),
		'as_parent'       => array(
			'only' => 'column'
		),
		'content_element' => true,
		'js_view'         => 'VcColumnView',
		'params'          => array(
			array(
				'param_name'  => 'no_margin',
				'heading'     => esc_html__( 'Marginless Columns', 'noor-assistant' ),
				'description' => esc_html__( 'Select to remove the spacing between columns.', 'noor-assistant' ),
				'type'        => 'checkbox',
				'holder'      => 'div',
				'value'       => array(
					'' => 'false'
				)
			),
			array(
				'param_name'  => 'id',
				'heading'     => esc_html__( 'ID', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter a unique ID.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'class',
				'heading'     => esc_html__( 'Class', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter a unique class name.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'style',
				'heading'     => esc_html__( 'Style', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter inline CSS.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textarea',
				'holder'      => 'div'
			)
		)
	)
);
