<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/*
* Add-on Name: Share
*/

class WPBakeryShortCode_Share extends WPBakeryShortCode {
}

/**
 * Share
 */
vc_map(
	array(
		'base'        => 'share',
		'name'        => esc_html__( 'Social Sharing', 'noor-assistant' ),
		'weight'      => 500,
		'class'       => 'dima-vc-element dima-vc-element-share',
		'icon'        => 'share',
		'category'    => esc_html__( 'Social', 'noor-assistant' ),
		'description' => esc_html__( 'Include social sharing into your content', 'noor-assistant' ),
		'params'      => array(
			array(
				'param_name'  => 'facebook',
				'heading'     => esc_html__( 'Facebook', 'noor-assistant' ),
				'description' => esc_html__( 'Check to activate Facebook sharing link.', 'noor-assistant' ),
				'type'        => 'checkbox',
				'holder'      => 'div',
				'value'       => array(
					'' => 'true'
				)
			),
			array(
				'param_name'  => 'twitter',
				'heading'     => esc_html__( 'Twitter', 'noor-assistant' ),
				'description' => esc_html__( 'Check to activate Twitter sharing link.', 'noor-assistant' ),
				'type'        => 'checkbox',
				'holder'      => 'div',
				'value'       => array(
					'' => 'true'
				)
			),
			array(
				'param_name'  => 'google_plus',
				'heading'     => esc_html__( 'Google Plus', 'noor-assistant' ),
				'description' => esc_html__( 'Check to activate Google Plus sharing link.', 'noor-assistant' ),
				'type'        => 'checkbox',
				'holder'      => 'div',
				'value'       => array(
					'' => 'true'
				)
			),
			array(
				'param_name'  => 'linkedin',
				'heading'     => esc_html__( 'LinkedIn', 'noor-assistant' ),
				'description' => esc_html__( 'Check to activate LinkedIn sharing link.', 'noor-assistant' ),
				'type'        => 'checkbox',
				'holder'      => 'div',
				'value'       => array(
					'' => 'true'
				)
			),
			array(
				'param_name'  => 'pinterest',
				'heading'     => esc_html__( 'Pinterest', 'noor-assistant' ),
				'description' => esc_html__( 'Check to activate Pinterest sharing link.', 'noor-assistant' ),
				'type'        => 'checkbox',
				'holder'      => 'div',
				'value'       => array(
					'' => 'true'
				)
			),
			array(
				'param_name'  => 'reddit',
				'heading'     => esc_html__( 'Reddit', 'noor-assistant' ),
				'description' => esc_html__( 'Check to activate Reddit sharing link.', 'noor-assistant' ),
				'type'        => 'checkbox',
				'holder'      => 'div',
				'value'       => array(
					'' => 'true'
				)
			),
			array(
				'param_name'  => 'email',
				'heading'     => esc_html__( 'Email', 'noor-assistant' ),
				'description' => esc_html__( 'Check to activate email sharing link.', 'noor-assistant' ),
				'type'        => 'checkbox',
				'holder'      => 'div',
				'value'       => array(
					'' => 'true'
				)
			),
			array(
				'param_name'  => 'size',
				'heading'     => esc_html__( 'Size', 'noor-assistant' ),
				'description' => esc_html__( 'Choose social icon size', 'noor-assistant' ),
				'type'        => 'dropdown',
				'value'       => array(
					esc_html__( '- Select Option -', 'noor-assistant' ) => 'medium',
					esc_html__( 'Small', 'noor-assistant' )             => 'small',
					esc_html__( 'Big', 'noor-assistant' )               => 'big',
					esc_html__( 'Medium', 'noor-assistant' )            => 'medium',
				)
			),
			array(
				'param_name'  => 'id',
				'heading'     => esc_html__( 'ID', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter a unique ID.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'class',
				'heading'     => esc_html__( 'Class', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter a unique class name.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'style',
				'heading'     => esc_html__( 'Style', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter inline CSS.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			)
		)
	)
);
