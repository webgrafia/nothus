<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/*
* Add-on Name: LABELS
*/

class WPBakeryShortCode_Labels extends WPBakeryShortCode {
}

/**
 * LABELS
 */
vc_map(
	array(
		'base'        => 'labels',
		'name'        => esc_html__( 'Labels', 'noor-assistant' ),
		'weight'      => 740,
		'class'       => 'dima-vc-element dima-vc-element-code',
		'icon'        => 'tag',
		'category'    => esc_html__( 'Typography', 'noor-assistant' ),
		'description' => esc_html__( 'Add a Labels to your content', 'noor-assistant' ),
		'params'      => array(
			array(
				'param_name'  => 'text',
				'heading'     => esc_html__( 'Text', 'noor-assistant' ),
				'description' => esc_html__( 'Enter your text', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'type',
				'heading'     => esc_html__( 'Type', 'noor-assistant' ),
				'description' => esc_html__( 'Choose your label style to show.', 'noor-assistant' ),
				'type'        => 'dropdown',
				'holder'      => 'div',
				'value'       => array(
					esc_html__( '- Select Option -', 'noor-assistant' ) => '',
					esc_html__( 'Inherit', 'noor-assistant' )           => 'default',
					esc_html__( 'Warning', 'noor-assistant' )           => 'warning',
					esc_html__( 'Info', 'noor-assistant' )              => 'info',
					esc_html__( 'Error', 'noor-assistant' )             => 'error',
				)
			),
			array(
				'param_name'  => 'bg_color',
				'heading'     => esc_html__( 'Background Color', 'noor-assistant' ),
				'description' => esc_html__( 'Choose background color for the text', 'noor-assistant' ),
				'type'        => 'colorpicker',
				'holder'      => 'div'
			),

			array(
				'param_name'  => 'id',
				'heading'     => esc_html__( 'ID', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter a unique ID.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'class',
				'heading'     => esc_html__( 'Class', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter a unique class name.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'style',
				'heading'     => esc_html__( 'Style', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter inline CSS.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			)
		)
	)
);
