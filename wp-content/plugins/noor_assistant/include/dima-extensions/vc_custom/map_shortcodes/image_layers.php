<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/*
* Add-on Name: Dima Image Layers for Visual Composer
*/

class WPBakeryShortCode_Dima_Image_Layers extends WPBakeryShortCode {
}

vc_map(
	array(
		'name'     => esc_html__( 'Image Layers', 'noor-assistant' ),
		'base'     => 'dima_image_layers',
		'class'    => 'dima-vc-element dima-vc-element-item_icon',
		'icon'     => 'gallery',
		'category' => esc_html__( 'Media', 'noor-assistant' ),
		'params'   => array(
			array(
				'type'       => 'dima_radio_advanced',
				'heading'    => esc_html__( 'Alignment', 'noor' ),
				'param_name' => 'alignment',
				'value'      => 'layers-center',
				'options'    => array(
					esc_html__( 'Start', 'noor-assistant' )  => 'layers-start',
					esc_html__( 'Center', 'noor-assistant' ) => 'layers-center',
					esc_html__( 'End', 'noor-assistant' )    => 'layers-end',
				),
			),

			array(
				'param_name'  => 'dima_lazyimage',
				'heading'     => esc_html__( 'Applay lazy Image', 'noor-assistant' ),
				'description' => esc_html__( 'lazy Image mean: Images outside of viewport will not be loaded before user scrolls to them.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'checkbox',
				'holder'      => 'div',
				'value'       => array(
					'' => 'true'
				)
			),

			array(
				'type'             => 'number',
				'heading'          => esc_html__( 'Delay', 'noor-assistant' ),
				'param_name'       => 'periodicity',
				'value'            => 300,
				'edit_field_class' => 'vc_column vc_col-sm-6 no-top-padding crum_vc dima-number-second',
			),
			array(
				'type'             => 'number',
				'heading'          => esc_html__( 'Translate Step', 'noor-assistant' ),
				'param_name'       => 'translate_step',
				'value'            => 200,
				'edit_field_class' => 'vc_column vc_col-sm-6 no-top-padding crum_vc dima-number-second',
			),
			array(
				'type'             => 'number',
				'heading'          => esc_html__( 'Duration ', 'noor-assistant' ),
				'param_name'       => 'animation_duration',
				'value'            => 750,
				'edit_field_class' => 'vc_column vc_col-sm-6 no-top-padding crum_vc dima-number-second',
			),
			array(
				'type'             => 'textfield',
				'heading'          => esc_html__( 'Custom CSS Class', 'noor-assistant' ),
				'param_name'       => 'el_class',
				'edit_field_class' => 'vc_column vc_col-sm-6 no-top-padding crum_vc dima-number-second',
			),
			array(
				'type'       => 'param_group',
				'heading'    => esc_html__( 'List of layers', 'noor-assistant' ),
				'param_name' => 'list_fields',
				'params'     => array(
					array(
						'type'       => 'attach_image',
						'heading'    => esc_html__( 'Upload Image:', 'noor-assistant' ),
						'param_name' => 'image_id',
					),
					array(
						'type'             => 'number',
						'heading'          => esc_html__( 'Horizontal offset', 'noor-assistant' ),
						'param_name'       => 'offcet_x',
						'edit_field_class' => 'vc_column vc_col-sm-6 crum_vc dima-number-percent',
					),
					array(
						'type'             => 'number',
						'heading'          => esc_html__( 'Vertical offset', 'noor-assistant' ),
						'param_name'       => 'offcet_y',
						'edit_field_class' => 'vc_column vc_col-sm-6 crum_vc dima-number-percent',
					),
					array(
						'type'       => 'dropdown',
						'heading'    => esc_html__( 'Animation', 'noor-assistant' ),
						'param_name' => 'layer_animation',
						'value'      => $dima_vc->dima_animate_list_velocity,

					),
				),
				'group'      => esc_html__( 'Layers', 'noor-assistant' ),
			),
			array(
				'param_name'  => 'tutorials',
				'type'        => 'dima_doc_link_param',
				'heading'     => '<span class="tippy dima-help dima-vc-toolip">' . dima_get_svg_icon( "ic_help" )
				                 . '<span class="dima-vc-tooltip-text">'
				                 . esc_html__( 'Video tutorial and theme documentation article', 'noor-assistant' ) . '</span></span>'
				                 . esc_html__( 'Tutorial', 'noor-assistant' ),
				'doc_link'    => '',
				'video_link'  => 'https://www.youtube.com/watch?v=Z358D7DsKy4',
				'doc_example' => 'https://noor.pixeldima.com/image-layers/',
			),
		),
	)
);