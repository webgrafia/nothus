<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/*
* Add-on Name: Author
*/

class WPBakeryShortCode_Author extends WPBakeryShortCode {
}

/**
 * Author
 */
vc_map(
	array(
		'base'        => 'author',
		'name'        => esc_html__( 'Author', 'noor-assistant' ),
		'weight'      => 510,
		'class'       => 'dima-vc-element dima-vc-element-author',
		'icon'        => 'author',
		'category'    => esc_html__( 'Social', 'noor-assistant' ),
		'description' => esc_html__( 'Include post author information', 'noor-assistant' ),
		'params'      => array(
			array(
				'param_name'  => 'id',
				'heading'     => esc_html__( 'ID', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter a unique ID.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'class',
				'heading'     => esc_html__( 'Class', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter a unique class name.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'style',
				'heading'     => esc_html__( 'Style', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter inline CSS.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'author_id',
				'heading'     => esc_html__( 'Author ID', 'noor-assistant' ),
				'description' => esc_html__( 'By default the author of the post or page will be output by leaving this input blank. If you would like to output the information of another author, enter in their user ID here.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			)
		)
	)
);