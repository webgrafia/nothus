<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/*
* Add-on Name: Line
*/

class WPBakeryShortCode_Date_Timeline extends WPBakeryShortCodesContainer {
}

class WPBakeryShortCode_Date_Timeline_Item extends WPBakeryShortCode {
}

/**
 * Date Timeline
 */
vc_map(
	array(
		'base'            => 'date_timeline',
		'name'            => esc_html__( 'Timeline', 'noor-assistant' ),
		'weight'          => 530,
		'class'           => 'dima-vc-element dima-vc-element-date_timeline',
		'icon'            => 'date-timeline',
		'category'        => esc_html__( 'Media', 'noor-assistant' ),
		'description'     => esc_html__( 'Add timeline to your content', 'noor-assistant' ),
		'as_parent'       => array( 'only' => 'date_timeline_item' ),
		'content_element' => true,
		'js_view'         => 'VcColumnView',
		'params'          => array(
			array(
				'param_name'  => 'id',
				'heading'     => esc_html__( 'ID', 'noor-assistant' ),
				'description' => $dima_vc->id_des,
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'class',
				'heading'     => esc_html__( 'Class', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter a unique class name.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'style',
				'heading'     => esc_html__( 'Style', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter inline CSS.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
		)
	)
);

/**
 * Date Timeline Item
 */
vc_map(
	array(
		'base'            => 'date_timeline_item',
		'name'            => esc_html__( 'Timeline Item', 'noor-assistant' ),
		'weight'          => 530,
		'class'           => 'dima-vc-element dima-vc-element-date_timeline_item',
		'icon'            => 'date-timeline',
		'category'        => esc_html__( 'Media', 'noor-assistant' ),
		'description'     => esc_html__( 'Date timtline item settings', 'noor-assistant' ),
		'as_child'        => array( 'only' => 'date_timeline' ),
		'content_element' => true,
		'params'          => array(
			array(
				'param_name'  => 'content',
				'heading'     => esc_html__( 'Code', 'noor-assistant' ),
				'description' => esc_html__( 'Add timeline item content', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textarea_html',
				'holder'      => 'div',
				'value'       => ''
			),
			array(
				'param_name'  => 'date',
				'heading'     => esc_html__( 'Date', 'noor-assistant' ),
				'description' => esc_html__( 'Add date to display in timeline item.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'float',
				'heading'     => esc_html__( 'Float', 'noor-assistant' ),
				'description' => esc_html__( 'Select the position of timeline item.', 'noor-assistant' ),
				'type'        => 'dropdown',
				'holder'      => 'div',
				'value'       => array(
					esc_html__( '- Select Option -', 'noor-assistant' ) => '',
					esc_html__( 'None', 'noor-assistant' )              => '',
					esc_html__( 'Start', 'noor-assistant' )             => 'start',
					esc_html__( 'End', 'noor-assistant' )               => 'end',
				)
			),
			array(
				'param_name'  => 'boxed',
				'heading'     => esc_html__( 'Remove Border', 'noor-assistant' ),
				'description' => esc_html__( 'Check to remove the border around the timeline item', 'noor-assistant' ),
				'type'        => 'checkbox',
				'holder'      => 'div',
				'value'       => array(
					'' => 'true'
				)
			),

			array(
				'param_name'  => 'id',
				'heading'     => esc_html__( 'ID', 'noor-assistant' ),
				'description' => $dima_vc->id_des,
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'class',
				'heading'     => esc_html__( 'Class', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter a unique class name.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'style',
				'heading'     => esc_html__( 'Style', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter inline CSS.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
		)
	)
);