<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/*
* Add-on Name: Noor Gallery
*/

class WPBakeryShortCode_Dima_Slides_Gallery extends WPBakeryShortCode {
}

/**
 * Noor Gallery
 */
vc_map(
	array(
		'base'            => 'dima_slides_gallery',
		'name'            => esc_html__( 'Slides Gallery', 'noor-assistant' ),
		'weight'          => 960,
		'class'           => 'dima-vc-element dima-vc-element-item_icon',
		'icon'            => 'gallery',
		'as_parent'       => array( 'only' => 'image', 'vc_row' ),
		'category'        => esc_html__( 'Media', 'noor-assistant' ),
		'description'     => esc_html__( 'Include an images gallery in your content', 'noor-assistant' ),
		'content_element' => true,
		'params'          => array(
			array(
				'param_name'  => 'dvc_source',
				'heading'     => esc_html__( 'Image Source', 'noor-assistant' ),
				'description' => esc_html__( 'Select image source..', 'noor-assistant' ),
				'type'        => 'dropdown',
				'value'       => array(
					esc_html__( 'Media library', 'noor-assistant' )  => 'media_library',
					esc_html__( 'External links', 'noor-assistant' ) => 'external_link'
				),
				'stf'         => 'media_library',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'custom_srcs',
				'type'        => 'exploded_textarea_safe',
				'heading'     => __( 'External links', 'noor-assistant' ),
				'description' => __( 'Enter external link for each gallery image (Note: divide links with linebreaks (Enter)).', 'noor-assistant' ),
				'dependency'  => array(
					'element' => 'dvc_source',
					'value'   => 'external_link',
				),
			),

			array(
				'param_name'  => 'images',
				'heading'     => esc_html__( 'Images', 'noor-assistant' ),
				'description' => esc_html__( 'Upload your image.', 'noor-assistant' ),
				'type'        => 'attach_images',
				'dependency'  => array(
					'element' => 'dvc_source',
					'value'   => 'media_library',
				),
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'img_size',
				'heading'     => esc_html__( 'Image Size', 'noor-assistant' ),
				'description' => esc_html__( 'Enter image size. Example: thumbnail, medium, large, full or other sizes defined by current theme. Alternatively enter image size in pixels: 200x100 (Width x Height). Leave mpty to use "thumbnail" size.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'value'       => 'full',
				'dependency'  => array(
					'element' => 'dvc_source',
					'value'   => 'media_library',
				),
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'external_img_size',
				'heading'     => esc_html__( 'Image Size', 'noor-assistant' ),
				'description' => esc_html__( 'Enter image size in pixels. Example: 200x100 (Width x Height).', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'dependency'  => array(
					'element' => 'dvc_source',
					'value'   => 'external_link',
				),
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'onclick',
				'type'        => 'dropdown',
				'heading'     => __( 'On click action', 'noor-assistant' ),
				'value'       => array(
					__( 'None', 'noor-assistant' )                => '',
					__( 'Link to large image', 'noor-assistant' ) => 'img_link_large',
					__( 'Open LightBox', 'noor-assistant' )       => 'link_image',
					__( 'Open custom link', 'noor-assistant' )    => 'custom_link',
				),
				'description' => __( 'Select action for click action.', 'noor-assistant' ),
				'std'         => 'link_image',
			),
			array(
				'param_name'  => 'custom_links',
				'type'        => 'exploded_textarea_safe',
				'heading'     => __( 'Custom links', 'noor-assistant' ),
				'description' => __( 'Enter links for each slide (Note: divide links with linebreaks (Enter)).', 'noor-assistant' ),
				'dependency'  => array(
					'element' => 'onclick',
					'value'   => array( 'custom_link' ),
				),
			),
			array(
				'param_name'  => 'custom_links_target',
				'type'        => 'dropdown',
				'heading'     => __( 'Custom link target', 'noor-assistant' ),
				'description' => __( 'Select where to open  custom links.', 'noor-assistant' ),
				'dependency'  => array(
					'element' => 'onclick',
					'value'   => array(
						'custom_link',
						'img_link_large',
					),
				),
				'value'       => $dima_vc->dima_target_param_list(),
			),
			array(
				'param_name'  => 'id',
				'heading'     => esc_html__( 'ID', 'noor-assistant' ),
				'description' => $dima_vc->id_des,
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'class',
				'heading'     => esc_html__( 'Class', 'noor-assistant' ),
				'description' => $dima_vc->class_des,
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'style',
				'heading'     => esc_html__( 'Style', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter inline CSS.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),

			//*******************Slider Option******************//

			array(
				'param_name'  => 'thumbnail_style',
				'type'        => 'dropdown',
				'group'       => esc_html__( 'Slide', 'noor-assistant' ),
				'heading'     => esc_html__( 'Thumbnail Style ', 'noor-assistant' ),
				'description' => esc_html__( 'Check to display slider Bullets', 'noor-assistant' ),
				'holder'      => 'div',
				'value'       => array(
					esc_html__( 'None', 'noor-assistant' )      => '',
					esc_html__( 'Dots', 'noor-assistant' )      => 'dots',
					esc_html__( 'thumbnail', 'noor-assistant' ) => 'thumbnail_image'
				),
				'std'         => 'dots'
			),
			array(
				'param_name'  => 'thumbnail_clm',
				'group'       => esc_html__( 'Slide', 'noor-assistant' ),
				'heading'     => esc_html__( 'Thumbnail Columns', 'noor-assistant' ),
				'description' => esc_html__( 'Chose how many columns to display bellow the large image', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'dependency'  => array(
					'element' => 'thumbnail_style',
					'value'   => 'thumbnail_image',
				),
				'std'         => 5,
				'holder'      => 'div'
			),

			array(
				'param_name'  => 'dots_style',
				'group'       => esc_html__( 'Slide', 'noor-assistant' ),
				'heading'     => esc_html__( 'side bullets', 'noor-assistant' ),
				'description' => esc_html__( 'Check if you want to show dots on the top-right corner ', 'noor-assistant' ),
				'type'        => 'checkbox',
				'holder'      => 'div',
				'dependency'  => array(
					'element' => 'thumbnail_style',
					'value'   => 'dots',
				),
				'value'       => array(
					'' => 'true'
				)
			),
			array(
				'param_name'  => 'navigation',
				'group'       => esc_html__( 'Slide', 'noor-assistant' ),
				'heading'     => esc_html__( 'Slider Arrows', 'noor-assistant' ),
				'description' => esc_html__( 'Check to display navigation arrows', 'noor-assistant' ),
				'type'        => 'checkbox',
				'holder'      => 'div',
				'value'       => array(
					'' => '1'
				)
			),

			array(
				'param_name'  => 'dark',
				'group'       => esc_html__( 'Slide', 'noor-assistant' ),
				'heading'     => esc_html__( 'Dark background', 'noor-assistant' ),
				'description' => esc_html__( 'Check if you use dark background section', 'noor-assistant' ),
				'type'        => 'checkbox',
				'holder'      => 'div',
				'value'       => array(
					'' => 'true'
				)
			),
			array(
				'param_name'  => 'speed',
				'group'       => esc_html__( 'Slide', 'noor-assistant' ),
				'heading'     => esc_html__( 'speed', 'noor-assistant' ),
				'description' => esc_html__( 'Slide/Fade animation speed (milliseconds)', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'std'         => '300',
				'holder'      => 'div'
			),

			array(
				'param_name'  => 'fade',
				'group'       => esc_html__( 'Slide', 'noor-assistant' ),
				'heading'     => esc_html__( 'Fade', 'noor-assistant' ),
				'description' => '',
				'type'        => 'checkbox',
				'holder'      => 'div',
				'value'       => array(
					'' => 'true'
				)
			),

			array(
				'param_name'  => 'items_margin',
				'group'       => esc_html__( 'Slide', 'noor-assistant' ),
				'heading'     => esc_html__( 'Add Elements Margin', 'noor-assistant' ),
				'description' => '',
				'type'        => 'checkbox',
				'holder'      => 'div',
				'value'       => array(
					'' => 'true'
				)
			),
			array(
				'param_name'  => 'auto_play',
				'group'       => esc_html__( 'Slide', 'noor-assistant' ),
				'heading'     => esc_html__( 'AutoPlay', 'noor-assistant' ),
				'description' => esc_html__( 'Check to animate the slides automatically', 'noor-assistant' ),
				'type'        => 'checkbox',
				'holder'      => 'div',
				'value'       => array(
					'' => 'true'
				)
			),

			array(
				'param_name'  => 'auto_play_speed',
				'group'       => esc_html__( 'Slide', 'noor-assistant' ),
				'heading'     => esc_html__( 'Auto Play Speed', 'noor-assistant' ),
				'description' => esc_html__( 'Autoplay Speed in milliseconds', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'dependency'  => array(
					'element' => 'auto_play',
					'value'   => 'true',
				),
				'std'         => '3000',
				'holder'      => 'div'
			),

			array(
				'param_name'  => 'loop',
				'group'       => esc_html__( 'Slide', 'noor-assistant' ),
				'heading'     => esc_html__( 'Loop', 'noor-assistant' ),
				'description' => esc_html__( 'Check to activate slider loop', 'noor-assistant' ),
				'type'        => 'checkbox',
				'holder'      => 'div',
				'value'       => array(
					'' => 'true'
				)
			),

			array(
				'param_name'  => 'lazyload',
				'group'       => esc_html__( 'Slide', 'noor-assistant' ),
				'heading'     => esc_html__( 'LazyLoad', 'noor-assistant' ),
				'description' => esc_html__( 'Set lazy loading technique.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'dropdown',
				'value'       => array(
					esc_html__( 'Ondemand', 'noor-assistant' )    => 'ondemand',
					esc_html__( 'Progressive', 'noor-assistant' ) => 'progressive',
				),
				'holder'      => 'div'
			),

			array(
				'param_name'  => 'centermode',
				'group'       => esc_html__( 'Slide', 'noor-assistant' ),
				'heading'     => esc_html__( 'Center Mode', 'noor-assistant' ),
				'description' => esc_html__( 'Enables centered view with partial prev/next slides.', 'noor-assistant' ),
				'type'        => 'checkbox',
				'holder'      => 'div',
				'value'       => array(
					'' => 'true'
				)
			),

			array(
				'param_name'  => 'centerpadding',
				'group'       => esc_html__( 'Slide', 'noor-assistant' ),
				'heading'     => esc_html__( 'Center Padding', 'noor-assistant' ),
				'description' => esc_html__( ' Side padding when in center mode (px or %)', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),

			array(
				'param_name'  => 'pauseonfocus',
				'group'       => esc_html__( 'Slide', 'noor-assistant' ),
				'heading'     => esc_html__( 'Pause On Focus', 'noor-assistant' ),
				'description' => esc_html__( 'Autoplay On Focus', 'noor-assistant' ),
				'type'        => 'checkbox',
				'holder'      => 'div',
				'std'         => 'true',
				'value'       => array(
					'' => 'true'
				)
			),

			array(
				'param_name'  => 'pauseonhover',
				'group'       => esc_html__( 'Slide', 'noor-assistant' ),
				'heading'     => esc_html__( 'Pause On Hover', 'noor-assistant' ),
				'description' => esc_html__( 'Pause Autoplay On Hover', 'noor-assistant' ),
				'type'        => 'checkbox',
				'holder'      => 'div',
				'std'         => 'true',
				'value'       => array(
					'' => 'true'
				)
			),

			array(
				'param_name'  => '_draggable',
				'group'       => esc_html__( 'Slide', 'noor-assistant' ),
				'heading'     => esc_html__( 'Draggable', 'noor-assistant' ),
				'description' => esc_html__( 'Enable mouse dragging', 'noor-assistant' ),
				'type'        => 'checkbox',
				'holder'      => 'div',
				'value'       => array(
					'' => 'true'
				),
				'std'         => 'true'
			),

			array(
				'param_name'  => 'variablewidth',
				'group'       => esc_html__( 'Slide', 'noor-assistant' ),
				'heading'     => esc_html__( 'Variabl Width', 'noor-assistant' ),
				'description' => esc_html__( 'Allow variabl Width', 'noor-assistant' ),
				'type'        => 'checkbox',
				'holder'      => 'div',
				'value'       => array(
					'' => 'true'
				)
			),
			array(
				'param_name'  => 'adaptiveheight',
				'group'       => esc_html__( 'Slide', 'noor-assistant' ),
				'heading'     => esc_html__( 'Adaptive Height', 'noor-assistant' ),
				'description' => '',
				'type'        => 'checkbox',
				'holder'      => 'div',
				'value'       => array(
					'' => 'true'
				)
			),

			array(
				'param_name'  => '_vertical',
				'group'       => esc_html__( 'Slide', 'noor-assistant' ),
				'heading'     => esc_html__( 'vertical', 'noor-assistant' ),
				'description' => esc_html__( 'Vertical slide mode', 'noor-assistant' ),
				'type'        => 'checkbox',
				'holder'      => 'div',
				'value'       => array(
					'' => 'true'
				)
			),

			array(
				'param_name'  => 'verticalswiping',
				'group'       => esc_html__( 'Slide', 'noor-assistant' ),
				'heading'     => esc_html__( 'Vertical Swiping', 'noor-assistant' ),
				'description' => esc_html__( 'Vertical swipe mode', 'noor-assistant' ),
				'type'        => 'checkbox',
				'holder'      => 'div',
				'value'       => array(
					'' => 'true'
				)
			),

			array(
				'param_name'  => 'mobilefirst',
				'group'       => esc_html__( 'Slide', 'noor-assistant' ),
				'heading'     => esc_html__( 'Mobile First', 'noor-assistant' ),
				'description' => esc_html__( 'Responsive settings use mobile first calculation', 'noor-assistant' ),
				'type'        => 'checkbox',
				'holder'      => 'div',
				'value'       => array(
					'' => 'true'
				)
			),

			array(
				'param_name'  => 'items',
				'group'       => esc_html__( 'Slide', 'noor-assistant' ),
				'heading'     => esc_html__( 'Elements Number', 'noor-assistant' ),
				'description' => esc_html__( 'Enter the number of elements to display(Numeric value only)', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'slidestoscroll',
				'group'       => esc_html__( 'Slide', 'noor-assistant' ),
				'heading'     => esc_html__( 'Slides', 'noor-assistant' ),
				'description' => esc_html__( '# of slides to scroll', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),

			array(
				'param_name'  => 'items_phone',
				'group'       => esc_html__( 'Slide', 'noor-assistant' ),
				'heading'     => esc_html__( 'Elements Number in Phone', 'noor-assistant' ),
				'description' => esc_html__( 'Enter the number of elements to display in phone (Numeric value only)', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),

			array(
				'param_name'  => 'items_tablet',
				'group'       => esc_html__( 'Slide', 'noor-assistant' ),
				'heading'     => esc_html__( 'Elements Number in Tablet', 'noor-assistant' ),
				'description' => esc_html__( 'Enter the number of elements to display in tablet (Numeric value only)', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'tutorials',
				'type'        => 'dima_doc_link_param',
				'heading'     => '<span class="tippy dima-help dima-vc-toolip">' . dima_get_svg_icon( "ic_help" )
				                 . '<span class="dima-vc-tooltip-text">'
				                 . esc_html__( 'Video tutorial and theme documentation article', 'noor-assistant' ) . '</span></span>'
				                 . esc_html__( 'Tutorial', 'noor-assistant' ),
				'doc_link'    => '',
				'video_link'  => 'https://youtu.be/u-pd_21ICXE?t=212',
				'doc_example' => 'https://noor.pixeldima.com/lightbox-gallery/',
			),

		)
	)
);
