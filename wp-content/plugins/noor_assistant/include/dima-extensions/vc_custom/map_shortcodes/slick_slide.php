<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/*
* Add-on Name: slick SLIDER
*/

class WPBakeryShortCode_Slick_Slider extends WPBakeryShortCodesContainer {
}

class WPBakeryShortCode_Slick_Slide extends WPBakeryShortCodesContainer {
}

/**
 * slick SLIDER
 */
vc_map(
	array(
		'base'            => 'slick_slider',
		'name'            => esc_html__( 'Carousel Slider', 'noor-assistant' ),
		'weight'          => 900,
		'class'           => 'dima-vc-element dima-vc-element-slider',
		'icon'            => 'slider',
		'category'        => esc_html__( 'Slideshow', 'noor-assistant' ),
		'description'     => esc_html__( 'Add carousel slider to your contents', 'noor-assistant' ),
		'as_parent'       => array( 'only' => 'slick_slide' ),
		'content_element' => true,
		'js_view'         => 'VcColumnView',
		'params'          => array(

			array(
				'param_name'  => 'dark',
				'heading'     => esc_html__( 'Dark background', 'noor-assistant' ),
				'description' => esc_html__( 'Check if you use dark background section', 'noor-assistant' ),
				'type'        => 'checkbox',
				'holder'      => 'div',
				'value'       => array(
					'' => 'true'
				)
			),
			array(
				'param_name'  => 'speed',
				'heading'     => esc_html__( 'speed', 'noor-assistant' ),
				'description' => esc_html__( 'Slide/Fade animation speed (milliseconds)', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'std'         => '300',
				'holder'      => 'div'
			),

			array(
				'param_name'  => 'fade',
				'heading'     => esc_html__( 'Fade', 'noor-assistant' ),
				'description' => '',
				'type'        => 'checkbox',
				'holder'      => 'div',
				'value'       => array(
					'' => 'true'
				)
			),

			array(
				'param_name'  => 'items_margin',
				'heading'     => esc_html__( 'Add Elements Margin', 'noor-assistant' ),
				'description' => '',
				'type'        => 'checkbox',
				'holder'      => 'div',
				'value'       => array(
					'' => 'true'
				)
			),
			array(
				'param_name'  => 'pagination',
				'heading'     => esc_html__( 'Slider Bullets ', 'noor-assistant' ),
				'description' => esc_html__( 'Check to display slider Bullets', 'noor-assistant' ),
				'type'        => 'checkbox',
				'holder'      => 'div',
				'value'       => array(
					'' => 'true'
				)
			),

			array(
				'param_name'  => 'dots_style',
				'heading'     => esc_html__( 'Side Bullets', 'noor-assistant' ),
				'description' => esc_html__( 'Check if you want to show dots on the top-right corner ', 'noor-assistant' ),
				'type'        => 'checkbox',
				'holder'      => 'div',
				'value'       => array(
					'' => 'true'
				)
			),

			array(
				'param_name'  => 'auto_play',
				'heading'     => esc_html__( 'AutoPlay', 'noor-assistant' ),
				'description' => esc_html__( 'Check to animate the slides automatically', 'noor-assistant' ),
				'type'        => 'checkbox',
				'holder'      => 'div',
				'value'       => array(
					'' => 'true'
				)
			),

			array(
				'param_name'  => 'auto_play_speed',
				'heading'     => esc_html__( 'Auto Play Speed', 'noor-assistant' ),
				'description' => esc_html__( 'Autoplay Speed in milliseconds', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'dependency'  => array(
					'element' => 'auto_play',
					'value'   => 'true',
				),
				'std'         => '3000',
				'holder'      => 'div'
			),


			array(
				'param_name'  => 'navigation',
				'heading'     => esc_html__( 'Slider Arrows', 'noor-assistant' ),
				'description' => esc_html__( 'Check to display navigation arrows', 'noor-assistant' ),
				'type'        => 'checkbox',
				'holder'      => 'div',
				'value'       => array(
					'' => '1'
				)
			),

			array(
				'param_name'  => 'loop',
				'heading'     => esc_html__( 'Loop', 'noor-assistant' ),
				'description' => esc_html__( 'Check to activate slider loop', 'noor-assistant' ),
				'type'        => 'checkbox',
				'holder'      => 'div',
				'value'       => array(
					'' => 'true'
				)
			),

			array(
				'param_name'  => 'lazyload',
				'heading'     => esc_html__( 'LazyLoad', 'noor-assistant' ),
				'description' => esc_html__( 'Set lazy loading technique.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'dropdown',
				'value'       => array(
					esc_html__( 'Ondemand', 'noor-assistant' )    => 'ondemand',
					esc_html__( 'Progressive', 'noor-assistant' ) => 'progressive',
				),
				'holder'      => 'div'
			),

			array(
				'param_name'  => 'centermode',
				'heading'     => esc_html__( 'Center Mode', 'noor-assistant' ),
				'description' => esc_html__( 'Enables centered view with partial prev/next slides.', 'noor-assistant' ),
				'type'        => 'checkbox',
				'holder'      => 'div',
				'value'       => array(
					'' => 'true'
				)
			),

			array(
				'param_name'  => 'centerpadding',
				'heading'     => esc_html__( 'Center Padding', 'noor-assistant' ),
				'description' => esc_html__( ' Side padding when in center mode (px or %)', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),

			array(
				'param_name'  => 'pauseonfocus',
				'heading'     => esc_html__( 'Pause On Focus', 'noor-assistant' ),
				'description' => esc_html__( 'Pause Autoplay On Focus', 'noor-assistant' ),
				'type'        => 'checkbox',
				'holder'      => 'div',
				'value'       => array(
					'' => 'true',
				),
				//'std'         => 'true',
			),

			array(
				'param_name'  => 'pauseonhover',
				'heading'     => esc_html__( 'Pause On Hover', 'noor-assistant' ),
				'description' => esc_html__( 'Pause Autoplay On Hover', 'noor-assistant' ),
				'type'        => 'checkbox',
				'holder'      => 'div',
				'value'       => array(
					'' => 'true'
				),
				//'std'         => 'true',
			),

			array(
				'param_name'  => '_draggable',
				'heading'     => esc_html__( 'Draggable', 'noor-assistant' ),
				'description' => esc_html__( 'Enable mouse dragging', 'noor-assistant' ),
				'type'        => 'checkbox',
				'holder'      => 'div',
				'value'       => array(
					'' => 'true'
				),
			),

			array(
				'param_name'  => 'variablewidth',
				'heading'     => esc_html__( 'Variable Width', 'noor-assistant' ),
				'description' => esc_html__( 'Allow variable Width', 'noor-assistant' ),
				'type'        => 'checkbox',
				'holder'      => 'div',
				'value'       => array(
					'' => 'true'
				)
			),
			array(
				'param_name'  => 'adaptiveheight',
				'heading'     => esc_html__( 'Adaptive Height', 'noor-assistant' ),
				'description' => '',
				'type'        => 'checkbox',
				'holder'      => 'div',
				'value'       => array(
					'' => 'true'
				)
			),

			array(
				'param_name'  => '_vertical',
				'heading'     => esc_html__( 'vertical', 'noor-assistant' ),
				'description' => esc_html__( 'Vertical slide mode', 'noor-assistant' ),
				'type'        => 'checkbox',
				'holder'      => 'div',
				'value'       => array(
					'' => 'true'
				)
			),

			array(
				'param_name'  => 'verticalswiping',
				'heading'     => esc_html__( 'Vertical Swiping', 'noor-assistant' ),
				'description' => esc_html__( 'Vertical swipe mode', 'noor-assistant' ),
				'type'        => 'checkbox',
				'holder'      => 'div',
				'value'       => array(
					'' => 'true'
				)
			),

			array(
				'param_name'  => 'mobilefirst',
				'heading'     => esc_html__( 'Mobile First', 'noor-assistant' ),
				'description' => esc_html__( 'Responsive settings use mobile first calculation', 'noor-assistant' ),
				'type'        => 'checkbox',
				'holder'      => 'div',
				'value'       => array(
					'' => 'true'
				)
			),

			array(
				'param_name'  => 'items',
				'heading'     => esc_html__( 'Elements Number', 'noor-assistant' ),
				'description' => esc_html__( 'Enter the number of elements to display(Numeric value only)', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'slidestoscroll',
				'heading'     => esc_html__( 'Slides', 'noor-assistant' ),
				'description' => esc_html__( '# of slides to scroll', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),

			array(
				'param_name'  => 'items_phone',
				'heading'     => esc_html__( 'Elements Number in Phone', 'noor-assistant' ),
				'description' => esc_html__( 'Enter the number of elements to display in phone (Numeric value only)', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),

			array(
				'param_name'  => 'items_tablet',
				'heading'     => esc_html__( 'Elements Number in Tablet', 'noor-assistant' ),
				'description' => esc_html__( 'Enter the number of elements to display in tablet (Numeric value only)', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),

			array(
				'param_name'  => 'id',
				'heading'     => esc_html__( 'ID', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter a unique ID.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'class',
				'heading'     => esc_html__( 'Class', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter a unique class name.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'style',
				'heading'     => esc_html__( 'Style', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter inline CSS.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'tutorials',
				'type'        => 'dima_doc_link_param',
				'heading'     => '<span class="tippy dima-help dima-vc-toolip">' . dima_get_svg_icon( "ic_help" )
				                 . '<span class="dima-vc-tooltip-text">'
				                 . esc_html__( 'Video tutorial and theme documentation article', 'noor-assistant' ) . '</span></span>'
				                 . esc_html__( 'Tutorial', 'noor-assistant' ),
				'doc_link'    => '',
				'video_link'  => 'https://www.youtube.com/watch?v=Fvob7sIngro',
				'doc_example' => 'https://noor.pixeldima.com/drag-slider/',
			),
		)
	)
);

/**
 * slick_slide
 */
vc_map(
	array(
		'base'            => 'slick_slide',
		'name'            => esc_html__( 'Carousel Element', 'noor-assistant' ),
		'weight'          => 960,
		'class'           => 'dima-vc-element dima-vc-element-slick_slide',
		'icon'            => 'slider',
		'as_child'        => array( 'only' => 'slick_slider' ),
		'as_parent'       => array( 'except' => 'slick_slide' ),
		'category'        => esc_html__( 'Slideshow', 'noor-assistant' ),
		'description'     => esc_html__( 'Slide element settings', 'noor-assistant' ),
		'content_element' => true,
		'js_view'         => 'VcColumnView',
		'params'          => array(
			array(
				'param_name'  => 'id',
				'heading'     => esc_html__( 'ID', 'noor-assistant' ),
				'description' => $dima_vc->id_des,
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'class',
				'heading'     => esc_html__( 'Class', 'noor-assistant' ),
				'description' => $dima_vc->class_des,
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'style',
				'heading'     => esc_html__( 'Style', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter inline CSS.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'tutorials',
				'type'        => 'dima_doc_link_param',
				'heading'     => '<span class="tippy dima-help dima-vc-toolip">' . dima_get_svg_icon( "ic_help" )
				                 . '<span class="dima-vc-tooltip-text">'
				                 . esc_html__( 'Video tutorial and theme documentation article', 'noor-assistant' ) . '</span></span>'
				                 . esc_html__( 'Tutorial', 'noor-assistant' ),
				'doc_link'    => '',
				'video_link'  => 'https://www.youtube.com/watch?v=Fvob7sIngro',
				'doc_example' => 'https://noor.pixeldima.com/drag-slider/',
			),
		)
	)
);