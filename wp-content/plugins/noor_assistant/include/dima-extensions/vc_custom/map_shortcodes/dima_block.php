<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/*
* Add-on Name: Image
*/

class WPBakeryShortCode_Dima_Block extends WPBakeryShortCode {
}

/* Content Block
 ---------------------------------------------------------- */
$current_post_type = dima_helper::dima_get_current_post_type();
if ( $current_post_type !== 'dimablock' ) {
	$cblock = get_posts( 'post_type="dimablock"&numberposts=-1&suppress_filters=0' );

	$conten_blocks = array();
	if ( $cblock ) {
		foreach ( $cblock as $cform ) {
			$conten_blocks[ $cform->post_title ] = $cform->ID;
		}
	} else {
		$conten_blocks[ __( 'No Content Block found', 'noor-assistant' ) ] = 0;
	}
	vc_map( array(
		'base'           => 'dima_block',
		'php_class_name' => 'dima_block',
		'weight'         => 97,
		'class'          => 'dima-vc-element dima-vc-element-section_block',
		'name'           => __( 'Section Block', 'noor-assistant' ),
		'icon'           => 'section_block',
		'category'       => __( 'Content', 'noor-assistant' ),
		'description'    => __( 'Place Section Block', 'noor-assistant' ),
		'params'         => array(
			array(
				'type'        => 'dropdown',
				'heading'     => __( 'Section Block', 'noor-assistant' ),
				'param_name'  => 'id',
				'value'       => $conten_blocks,
				'admin_label' => true,
				'save_always' => true,
				'description' => __( 'Choose Section Block from the drop down list.', 'noor-assistant' ),
			)
		),
		array(
			'param_name'  => 'tutorials',
			'type'        => 'dima_doc_link_param',
			'heading'     => '<span class="tippy dima-help dima-vc-toolip">' . dima_get_svg_icon( "ic_help" )
			                 . '<span class="dima-vc-tooltip-text">'
			                 . esc_html__( 'Video tutorial and theme documentation article', 'noor-assistant' ) . '</span></span>'
			                 . esc_html__( 'Tutorial', 'noor-assistant' ),
			'doc_link'    => '',
			'video_link'  => 'https://www.youtube.com/watch?v=ynXM_TMyJCE',
			'doc_example' => 'https://noor.pixeldima.com/',
		),
		'js_view'        => 'DimaBlockView'
	) );
}