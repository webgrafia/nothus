<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/*
* Add-on Name: Icon List
*/

class WPBakeryShortCode_Icon_List extends WPBakeryShortCodesContainer {
}

/**
 * Icon List
 */
vc_map(
	array(
		'base'            => 'icon_list',
		'name'            => esc_html__( 'Icon List', 'noor-assistant' ),
		'weight'          => 780,
		'class'           => 'dima-vc-element dima-vc-element-protect',
		'icon'            => 'icon-list',
		'category'        => esc_html__( 'Typography', 'noor-assistant' ),
		'description'     => esc_html__( 'Include an icon list in your content', 'noor-assistant' ),
		'as_parent'       => array( 'only' => 'icon_list_item' ),
		'content_element' => true,
		'js_view'         => 'VcColumnView',
		'params'          => array(
			array(
				'param_name'  => 'id',
				'heading'     => esc_html__( 'ID', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter a unique ID.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'class',
				'heading'     => esc_html__( 'Class', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter a unique class name.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'style',
				'heading'     => esc_html__( 'Style', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter inline CSS.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			)
		)
	)
);

/**
 * Icon list item
 */
vc_map(
	array(
		'base'            => 'icon_list_item',
		'name'            => esc_html__( 'Icon List Item', 'noor-assistant' ),
		'weight'          => 770,
		'class'           => 'dima-vc-element dima-vc-element-protect',
		'icon'            => 'icon-list',
		'category'        => esc_html__( 'Typography', 'noor-assistant' ),
		'description'     => esc_html__( 'Include an icon list item in your icon list', 'noor-assistant' ),
		'as_child'        => array( 'only' => 'icon_list' ),
		'content_element' => true,
		'params'          => array(
			array(
				'param_name'  => 'content',
				'heading'     => esc_html__( 'Text', 'noor-assistant' ),
				'description' => esc_html__( 'Enter your text.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textarea_html',
				'holder'      => 'div',
				'value'       => ''
			),

			array(
				'type'        => 'dropdown',
				'heading'     => esc_html__( 'Icons library', 'noor-assistant' ),
				'value'       => array(
					esc_html__( 'Font Awesome', 'noor-assistant' )                                     => 'fontawesome',
					esc_html__( 'Custom Font', 'noor-assistant' )                                      => 'customfont',
					esc_html__( 'SVG Icons (Stroke Gap Icons And Material Icons) ', 'noor-assistant' ) => 'svgicon',
				),
				'param_name'  => 'icon_type',
				'description' => esc_html__( 'Select icons library.', 'noor-assistant' ),
			),
			array(
				'param_name'  => 'icon_svg',
				'heading'     => esc_html__( 'Icon SVG', 'noor-assistant' ),
				'description' => esc_html__( 'Enter SVG Icon name', 'noor-assistant' ),
				'value'       => '',
				'dependency'  => array(
					'element' => 'icon_type',
					'value'   => 'svgicon',
				),
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'icon',
				'heading'     => esc_html__( 'Icon Class', 'noor-assistant' ),
				'description' => esc_html__( 'Enter Icon Class from Font Awesome or Simple Line Icons.', 'noor-assistant' ),
				'value'       => 'simple-icon-user',
				'dependency'  => array(
					'element' => 'icon_type',
					'value'   => 'customfont',
				),
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'type'        => 'iconpicker',
				'heading'     => esc_html__( 'Icon', 'noor-assistant' ),
				'param_name'  => 'icon_fontawesome',
				'value'       => '', // default value to backend editor admin_label
				'settings'    => array(
					'emptyIcon'    => true,
					// default true, display an "EMPTY" icon?
					'value'        => 'fontawesome',
					'iconsPerPage' => 4000,
					// default 100, how many icons per/page to display, we use (big number) to display all icons in single page
				),
				'dependency'  => array(
					'element' => 'icon_type',
					'value'   => 'fontawesome',
				),
				'description' => esc_html__( 'Select icon from library.', 'noor-assistant' ),
			),
			array(
				'param_name'  => 'icon_color',
				'heading'     => esc_html__( 'Icon color', 'noor-assistant' ),
				'description' => esc_html__( 'Choose the icon color for your callout', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'colorpicker',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'id',
				'heading'     => esc_html__( 'ID', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter a unique ID.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'class',
				'heading'     => esc_html__( 'Class', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter a unique class name.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'style',
				'heading'     => esc_html__( 'Style', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter inline CSS.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			)
		)
	)
);
