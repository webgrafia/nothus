<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/*
* Add-on Name: Blog posts
*/

class WPBakeryShortCode_Column extends WPBakeryShortCodesContainer {
}

/**
 * Column.
 */
vc_map(
	array(
		'base'            => 'column',
		'name'            => esc_html__( 'column', 'noor-assistant' ),
		'weight'          => 990,
		'class'           => 'dima-vc-element dima-vc-element-columns',
		'icon'            => 'columns',
		'category'        => esc_html__( 'Structure', 'noor-assistant' ),
		'description'     => esc_html__( 'Create a custom columns', 'noor-assistant' ),
		'as_child'        => array( 'only' => 'container' ),
		'as_parent'       => array(
			'except' => 'container,vc_row_inner,vc_row,accordion_item,tab_nav_item,client'
		),
		'content_element' => true,
		'js_view'         => 'VcColumnView',
		'params'          => array(
			//**************Animation***************//
			array(
				'param_name'  => 'animation',
				'group'       => esc_html__( 'Animation', 'noor-assistant' ),
				'heading'     => esc_html__( 'Animation', 'noor-assistant' ),
				'description' => esc_html__( 'Select the type of animation you want to use.', 'noor-assistant' ),
				'type'        => 'dropdown',
				'holder'      => 'div',
				'value'       => $dima_vc->dima_animate_list_velocity,
			),
			array(
				'param_name'  => 'delay',
				'group'       => esc_html__( 'Animation', 'noor-assistant' ),
				'heading'     => esc_html__( 'Delay Animation', 'noor-assistant' ),
				'description' => esc_html__( 'Set when the animation start (milliseconds)', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div',
				'value'       => ''
			),
			array(
				'param_name'  => 'delay_duration',
				'group'       => esc_html__( 'Animation', 'noor-assistant' ),
				'heading'     => esc_html__( 'duration Animation', 'noor-assistant' ),
				'description' => esc_html__( 'Animation duration of each element  (milliseconds).', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div',
				'value'       => ''
			),
			array(
				'param_name'  => 'delay_offset',
				'group'       => esc_html__( 'Animation', 'noor-assistant' ),
				'heading'     => esc_html__( 'Delay Animation Offset', 'noor-assistant' ),
				'description' => esc_html__( 'Set the number ( px,%..) from the top of the viewport where the animation start.', 'noor-assistant' ) . "<a href='http://imakewebthings.com/waypoints/api/offset-option/' target='_blank' >" . esc_html__( 'Read More', 'noor-assistant' ) . "</a>",
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div',
				'value'       => ''
			),
			//**************!Animation***************//
			array(
				'param_name'  => 'xld',
				'heading'     => esc_html__( 'Extra Large devices', 'noor-assistant' ),
				'group'       => esc_html__( 'Responsive Width', 'noor-assistant' ),
				'description' => esc_html__( 'Select column width for Extra Large devices Desktops ( ≥1600px ).', 'noor-assistant' ),
				'type'        => 'dropdown',
				'holder'      => 'div',
				'value'       => $dima_vc->vc_column_width_list,
				'std'         => ''
			),
			array(
				'param_name'  => 'ld',
				'heading'     => esc_html__( 'Large devices', 'noor-assistant' ),
				'group'       => esc_html__( 'Responsive Width', 'noor-assistant' ),
				'description' => esc_html__( 'Select column width for Large devices Desktops ( ≥1140px ).', 'noor-assistant' ),
				'type'        => 'dropdown',
				'holder'      => 'div',
				'value'       => $dima_vc->vc_column_width_list,
				'std'         => ''
			),
			array(
				'param_name'  => 'md',
				'heading'     => esc_html__( 'Medium Devise', 'noor-assistant' ),
				'group'       => esc_html__( 'Responsive Width', 'noor-assistant' ),
				'description' => esc_html__( 'Select column width for medium devise ( ≥989px ).', 'noor-assistant' ),
				'type'        => 'dropdown',
				'holder'      => 'div',
				'value'       => $dima_vc->vc_column_width_list,
				'std'         => ''
			),
			array(
				'param_name'  => 'sd',
				'heading'     => esc_html__( 'Small devices Tablets', 'noor-assistant' ),
				'group'       => esc_html__( 'Responsive Width', 'noor-assistant' ),
				'description' => esc_html__( 'Select column width for Small devices Tablets ( ≥768px ).', 'noor-assistant' ),
				'type'        => 'dropdown',
				'holder'      => 'div',
				'value'       => $dima_vc->vc_column_width_list,
				'std'         => '6'
			),
			array(
				'param_name'  => 'xsd',
				'heading'     => esc_html__( 'Extra small devices Phones', 'noor-assistant' ),
				'group'       => esc_html__( 'Responsive Width', 'noor-assistant' ),
				'description' => esc_html__( 'Select column width for Extra small devices Phones ( <768px ).', 'noor-assistant' ),
				'type'        => 'dropdown',
				'holder'      => 'div',
				'value'       => $dima_vc->vc_column_width_list,
				'std'         => '12'
			),
			//***offset
			array(
				'param_name'  => 'offset_xld',
				'heading'     => esc_html__( 'Extra Large devices', 'noor-assistant' ),
				'group'       => esc_html__( 'Responsive Offset', 'noor-assistant' ),
				'description' => esc_html__( 'Select column offset for Extra Large devices Desktops ( ≥1600px ).', 'noor-assistant' ),
				'type'        => 'dropdown',
				'holder'      => 'div',
				'value'       => $dima_vc->vc_column_width_list,
				'std'         => ''
			),
			array(
				'param_name'  => 'offset_ld',
				'heading'     => esc_html__( 'Large devices Desktops  ', 'noor-assistant' ),
				'group'       => esc_html__( 'Responsive Offset', 'noor-assistant' ),
				'description' => esc_html__( 'Select column offset for Large devices Desktops ( ≥1140px ).', 'noor-assistant' ),
				'type'        => 'dropdown',
				'holder'      => 'div',
				'value'       => $dima_vc->vc_column_width_list,
				'std'         => ''
			),
			array(
				'param_name'  => 'offset_md',
				'heading'     => esc_html__( 'Medium Devise', 'noor-assistant' ),
				'group'       => esc_html__( 'Responsive Offset', 'noor-assistant' ),
				'description' => esc_html__( 'Select column offset for medium devise ( ≥989px ).', 'noor-assistant' ),
				'type'        => 'dropdown',
				'holder'      => 'div',
				'value'       => $dima_vc->vc_column_width_list,
				'std'         => ''
			),
			//***visibility
			array(
				'param_name'  => 'visibility_xld',
				'heading'     => esc_html__( 'Extra Large devices', 'noor-assistant' ),
				'group'       => esc_html__( 'Responsive Visibility', 'noor-assistant' ),
				'description' => esc_html__( 'Choose column visibility for Extra Large devices Desktops ( ≥1600px ).', 'noor-assistant' ),
				'type'        => 'checkbox',
				'holder'      => 'div',
				'value'       => $dima_vc->vc_column_visibility,
				'std'         => ''
			),
			array(
				'param_name'  => 'visibility_ld',
				'heading'     => esc_html__( 'Large devices', 'noor-assistant' ),
				'group'       => esc_html__( 'Responsive Visibility', 'noor-assistant' ),
				'description' => esc_html__( 'Choose column visibility for Large devices Desktops ( ≥1140px ).', 'noor-assistant' ),
				'type'        => 'checkbox',
				'holder'      => 'div',
				'value'       => $dima_vc->vc_column_visibility,
				'std'         => ''
			),
			array(
				'param_name'  => 'visibility_md',
				'heading'     => esc_html__( 'Medium Devise', 'noor-assistant' ),
				'group'       => esc_html__( 'Responsive Visibility', 'noor-assistant' ),
				'description' => esc_html__( 'Choose column visibility for medium devise ( ≥989px ).', 'noor-assistant' ),
				'type'        => 'checkbox',
				'holder'      => 'div',
				'value'       => $dima_vc->vc_column_visibility,
				'std'         => ''
			),
			array(
				'param_name'  => 'visibility_sd',
				'heading'     => esc_html__( 'Small devices', 'noor-assistant' ),
				'group'       => esc_html__( 'Responsive Visibility', 'noor-assistant' ),
				'description' => esc_html__( 'Choose column visibility for Small devices Tablets ( ≥768px ).', 'noor-assistant' ),
				'type'        => 'checkbox',
				'holder'      => 'div',
				'value'       => $dima_vc->vc_column_visibility,
				'std'         => ''
			),
			array(
				'param_name'  => 'visibility_xsd',
				'heading'     => esc_html__( 'Extra small devices', 'noor-assistant' ),
				'group'       => esc_html__( 'Responsive Visibility', 'noor-assistant' ),
				'description' => esc_html__( 'Choose column visibility for Extra small devices Phones ( <768px ).', 'noor-assistant' ),
				'type'        => 'checkbox',
				'holder'      => 'div',
				'value'       => $dima_vc->vc_column_visibility,
				'std'         => ''
			),
			///****
			array(
				'param_name'  => 'id',
				'heading'     => esc_html__( 'ID', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter a unique ID.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'class',
				'heading'     => esc_html__( 'Class', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter a unique class name.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'style',
				'heading'     => esc_html__( 'Style', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter inline CSS.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textarea',
				'holder'      => 'div'
			)
		)
	)
);
