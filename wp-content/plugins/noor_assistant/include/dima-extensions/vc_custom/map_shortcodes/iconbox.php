<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/*
* Add-on Name: Iconbox
*/

class WPBakeryShortCode_Iconbox extends WPBakeryShortCodesContainer {
}

class WPBakeryShortCode_Iconbox_Content extends WPBakeryShortCode {
}

/**
 * iconbox
 */
vc_map(
	array(
		'base'            => 'iconbox',
		'name'            => esc_html__( 'Iconbox', 'noor-assistant' ),
		'weight'          => 530,
		'class'           => 'dima-vc-element dima-vc-element-iconbox',
		'icon'            => 'iconbox',
		'category'        => esc_html__( 'Media', 'noor-assistant' ),
		'description'     => esc_html__( 'Add iconbox to your content', 'noor-assistant' ),
		'as_parent'       => array( 'only' => 'iconbox_header,iconbox_content,google_maptwo' ),
		'content_element' => true,
		'js_view'         => 'VcColumnView',
		'params'          => array(
			array(
				'param_name'  => 'boxed',
				'group'       => esc_html__( 'Style', 'noor-assistant' ),
				'heading'     => esc_html__( 'Add box', 'noor-assistant' ),
				'description' => esc_html__( 'Check to add box around the iconbox', 'noor-assistant' ),
				'type'        => 'checkbox',
				'holder'      => 'div',
				'value'       => array(
					'' => 'true'
				)
			),

			array(
				'param_name'  => 'dima_vc_add_shadow',
				'group'       => esc_html__( 'Style', 'noor-assistant' ),
				'heading'     => esc_html__( 'Add Shadow', 'noor-assistant' ),
				'description' => esc_html__( 'Check to add drop shadow effect', 'noor-assistant' ),
				'type'        => 'checkbox',
				'holder'      => 'div',
				'dependency'  => array(
					'element' => 'boxed',
					'value'   => 'true'
				),
				'value'       => array(
					'' => 'true'
				)
			),
			array(
				'param_name'  => 'dima_vc_shadow_on_hover',
				'group'       => esc_html__( 'Style', 'noor-assistant' ),
				'heading'     => esc_html__( 'Applay Shadow on Hover', 'noor-assistant' ),
				'description' => esc_html__( 'Check to apply drop shadow effect on hover', 'noor-assistant' ),
				'type'        => 'checkbox',
				'holder'      => 'div',
				'dependency'  => array(
					'element' => 'boxed',
					'value'   => 'true'
				),
				'value'       => array(
					'' => 'true'
				)
			),
			array(
				'param_name'  => 'box_bg',
				'group'       => esc_html__( 'Style', 'noor-assistant' ),
				'heading'     => esc_html__( 'Background Color', 'noor-assistant' ),
				'description' => esc_html__( 'Choose box background color.', 'noor-assistant' ),
				'type'        => 'colorpicker',
				'dependency'  => array(
					'element' => 'boxed',
					'value'   => 'true',
				)
			),
			array(
				'param_name'  => 'dima_vc_border_color',
				'group'       => esc_html__( 'Style', 'noor-assistant' ),
				'heading'     => esc_html__( 'Top border color', 'noor-assistant' ),
				'description' => esc_html__( 'Choose the color of top border', 'noor-assistant' ),
				'type'        => 'colorpicker',
				'dependency'  => array(
					'element' => 'boxed',
					'value'   => 'true',
				)
			),
			array(
				'param_name'  => 'align',
				'heading'     => esc_html__( 'Icon Alignment', 'noor-assistant' ),
				'description' => esc_html__( 'Select the alignment of the icon', 'noor-assistant' ),
				'type'        => 'dropdown',
				'holder'      => 'div',
				'value'       => array(
					esc_html__( 'Select Options', 'noor-assistant' ) => '',
					esc_html__( 'Start', 'noor-assistant' )          => 'start',
					esc_html__( 'Center', 'noor-assistant' )         => 'center',
					esc_html__( 'End', 'noor-assistant' )            => 'end',
				)
			),
			array(
				'param_name'  => 'href',
				'heading'     => esc_html__( 'URL Link', 'noor-assistant' ),
				'description' => esc_html__( 'Enter in the URL you want your iconbox to link to.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),

			array(
				'param_name'  => 'target',
				'heading'     => esc_html__( 'Open link in a new tab', 'noor-assistant' ),
				'description' => esc_html__( 'Check to open your image link in a new tab.', 'noor-assistant' ),
				'type'        => 'checkbox',
				'holder'      => 'div',
				'value'       => array(
					'' => 'blank'
				)
			),

			array(
				'param_name'  => 'id',
				'heading'     => esc_html__( 'ID', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter a unique ID.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'class',
				'heading'     => esc_html__( 'Class', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter a unique class name.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'style',
				'heading'     => esc_html__( 'Style', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter inline CSS.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),

			//**************Animation***************//
			array(
				'param_name'  => 'animation',
				'group'       => esc_html__( 'Animation', 'noor-assistant' ),
				'heading'     => esc_html__( 'Animation', 'noor-assistant' ),
				'description' => esc_html__( 'Select the type of animation you want to use.', 'noor-assistant' ),
				'type'        => 'dropdown',
				'holder'      => 'div',
				'value'       => $dima_vc->dima_animate_list_velocity,
			),

			array(
				'param_name'  => 'delay',
				'group'       => esc_html__( 'Animation', 'noor-assistant' ),
				'heading'     => esc_html__( 'Delay Animation', 'noor-assistant' ),
				'description' => esc_html__( 'Set when the animation start (milliseconds)', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div',
				'value'       => ''
			),

			array(
				'param_name'  => 'delay_duration',
				'group'       => esc_html__( 'Animation', 'noor-assistant' ),
				'heading'     => esc_html__( 'duration Animation', 'noor-assistant' ),
				'description' => esc_html__( 'Animation duration of each element  (milliseconds).', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div',
				'value'       => ''
			),

			array(
				'param_name'  => 'delay_offset',
				'group'       => esc_html__( 'Animation', 'noor-assistant' ),
				'heading'     => esc_html__( 'Delay Animation Offset', 'noor-assistant' ),
				'description' => esc_html__( 'Set the number ( px,%..) from the top of the viewport where the animation start.', 'noor-assistant' ) . "<a href='http://imakewebthings.com/waypoints/api/offset-option/' target='_blank' >" . esc_html__( 'Read More', 'noor-assistant' ) . "</a>",
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div',
				'value'       => ''
			),
			//**************!Animation***************//
			array(
				'param_name'  => 'tutorials',
				'type'        => 'dima_doc_link_param',
				'heading'     => '<span class="tippy dima-help dima-vc-toolip">' . dima_get_svg_icon( "ic_help" )
				                 . '<span class="dima-vc-tooltip-text">'
				                 . esc_html__( 'Video tutorial and theme documentation article', 'noor-assistant' ) . '</span></span>'
				                 . esc_html__( 'Tutorial', 'noor-assistant' ),
				'doc_link'    => '',
				'video_link'  => 'https://www.youtube.com/watch?v=sJopxWqRQ-o',
				'doc_example' => 'https://noor.pixeldima.com/icon-boxes/',
			),
		)
	)
);

/**
 * Iconbox header
 */
vc_map(
	array(
		'base'        => 'iconbox_header',
		'name'        => esc_html__( 'Iconbox header', 'noor-assistant' ),
		'weight'      => 530,
		'class'       => 'dima-vc-element dima-vc-element-iconbox_header',
		'icon'        => 'iconbox-header',
		'category'    => esc_html__( 'Media', 'noor-assistant' ),
		'description' => esc_html__( 'Add header to the iconbox', 'noor-assistant' ),
		'params'      => array(
			array(
				'type'        => 'dropdown',
				'heading'     => esc_html__( 'Header Type', 'noor-assistant' ),
				'value'       => array(
					esc_html__( 'Icon', 'noor-assistant' )  => 'icon',
					esc_html__( 'Image', 'noor-assistant' ) => 'image',
				),
				'param_name'  => 'icon_or_image',
				'description' => esc_html__( 'Select type of iconbox header', 'noor-assistant' ),
			),
			array(
				'type'        => 'dropdown',
				'heading'     => esc_html__( 'Icons library', 'noor-assistant' ),
				'value'       => array(
					esc_html__( 'Font Awesome', 'noor-assistant' )                                     => 'fontawesome',
					esc_html__( 'Custom Font', 'noor-assistant' )                                      => 'customfont',
					esc_html__( 'SVG Icons (Stroke Gap Icons And Material Icons) ', 'noor-assistant' ) => 'svgicon',
				),
				'dependency'  => array(
					'element' => 'icon_or_image',
					'value'   => 'icon',
				),
				'param_name'  => 'icon_type',
				'description' => esc_html__( 'Select icons library.', 'noor-assistant' ),
			),
			array(
				'param_name'  => 'icon',
				'heading'     => esc_html__( 'Icon Class', 'noor-assistant' ),
				'description' => esc_html__( 'Enter Icon Class from Font Awesome or Simple Line Icons.', 'noor-assistant' ),
				'value'       => 'simple-icon-user',
				'dependency'  => array(
					'element' => 'icon_type',
					'value'   => 'customfont',
				),
				'type'        => 'textfield',
				'holder'      => 'div'
			),

			array(
				'param_name'  => 'icon_svg',
				'heading'     => esc_html__( 'Icon SVG', 'noor-assistant' ),
				'description' => esc_html__( 'Enter SVG Icon name', 'noor-assistant' ),
				'value'       => '',
				'dependency'  => array(
					'element' => 'icon_type',
					'value'   => 'svgicon',
				),
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'icon_svg_size',
				'heading'     => esc_html__( 'SVG Size', 'noor-assistant' ),
				'description' => esc_html__( 'Enter SVG Icon size', 'noor-assistant' ),
				'value'       => '',
				'dependency'  => array(
					'element' => 'icon_type',
					'value'   => 'svgicon',
				),
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'icon_svg_animation',
				'heading'     => esc_html__( 'SVG Animation', 'noor-assistant' ),
				'description' => '',
				'type'        => 'dropdown',
				'dependency'  => array(
					'element' => 'icon_type',
					'value'   => 'svgicon',
				),
				'value'       => array(
					esc_html__( 'Off', 'noor-assistant' ) => 'off',
					esc_html__( 'On', 'noor-assistant' )  => 'on',
				)
			),

			array(
				'type'        => 'iconpicker',
				'heading'     => esc_html__( 'Icon', 'noor-assistant' ),
				'param_name'  => 'icon_fontawesome',
				'value'       => '', // default value to backend editor admin_label
				'settings'    => array(
					'emptyIcon'    => true,
					'value'        => 'fontawesome',
					'iconsPerPage' => 4000,
				),
				'dependency'  => array(
					'element' => 'icon_type',
					'value'   => 'fontawesome',
				),
				'description' => esc_html__( 'Select icon from library.', 'noor-assistant' ),
			),
			array(
				'param_name'  => 'hover',
				'heading'     => esc_html__( 'Hover', 'noor-assistant' ),
				'description' => esc_html__( 'select the hover style', 'noor-assistant' ),
				'type'        => 'dropdown',
				'holder'      => 'div',
				'value'       => array(
					esc_html__( '- Select Option -', 'noor-assistant' ) => '',
					esc_html__( 'Default', 'noor-assistant' )           => 'icon-box-hover',
					esc_html__( 'Border Only', 'noor-assistant' )       => 'icon-box-border-hover',
					esc_html__( 'None', 'noor-assistant' )              => 'none',
				)
			),
			array(
				'param_name'  => 'size',
				'heading'     => esc_html__( 'Icon Size', 'noor-assistant' ),
				'description' => esc_html__( 'Choose size of the icon', 'noor-assistant' ),
				'type'        => 'dropdown',
				'dependency'  => array(
					'element' => 'icon_or_image',
					'value'   => 'icon',
				),
				'value'       => array(
					esc_html__( '- Select Option -', 'noor-assistant' ) => 'normal',
					esc_html__( 'Inherit', 'noor-assistant' )           => 'normal',
					esc_html__( 'Small', 'noor-assistant' )             => 'small',
					esc_html__( 'Medium', 'noor-assistant' )            => 'medium',
					esc_html__( 'Large', 'noor-assistant' )             => 'larg',
				)
			),
			array(
				'param_name'  => 'image',
				'heading'     => esc_html__( 'Use Image', 'noor-assistant' ),
				'description' => esc_html__( 'Use image for the iconbox', 'noor-assistant' ),
				'type'        => 'attach_image',
				'dependency'  => array(
					'element' => 'icon_or_image',
					'value'   => 'image',
				),
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'alt',
				'heading'     => esc_html__( 'Alt Text', 'noor-assistant' ),
				'description' => '',
				'type'        => 'textfield',
				'dependency'  => array(
					'element' => 'icon_or_image',
					'value'   => 'image',
				),
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'image_size',
				'heading'     => esc_html__( 'Image size', 'noor-assistant' ),
				'description' => esc_html__( 'Choose size of the image', 'noor-assistant' ),
				'type'        => 'dropdown',
				'dependency'  => array(
					'element' => 'icon_or_image',
					'value'   => 'image',
				),
				'value'       => array(
					esc_html__( '- Select Option -', 'noor-assistant' ) => 'medium',
					esc_html__( 'Small', 'noor-assistant' )             => 'small',
					esc_html__( 'Medium', 'noor-assistant' )            => 'medium',
					esc_html__( 'Large', 'noor-assistant' )             => 'larg',
				)
			),
			array(
				'param_name'  => 'type',
				'group'       => esc_html__( 'Style', 'noor-assistant' ),
				'heading'     => esc_html__( 'Shape', 'noor-assistant' ),
				'description' => esc_html__( 'Select the shape around the icon/Image', 'noor-assistant' ),
				'type'        => 'dropdown',
				'value'       => array(
					esc_html__( '- Select Option -', 'noor-assistant' ) => '',
					esc_html__( 'None', 'noor-assistant' )              => '',
					esc_html__( 'Circle', 'noor-assistant' )            => 'circle',
					esc_html__( 'Square', 'noor-assistant' )            => 'square',
				)
			),
			array(
				'param_name'  => 'dima_vc_position',
				'group'       => esc_html__( 'Style', 'noor-assistant' ),
				'heading'     => esc_html__( 'Position', 'noor-assistant' ),
				'description' => esc_html__( 'Select the header-icon position ( this option work if the icon box is boxed )', 'noor-assistant' ),
				'type'        => 'dropdown',
				'value'       => array(
					esc_html__( '- Select Option -', 'noor-assistant' ) => '',
					esc_html__( 'Start', 'noor-assistant' )             => 'icon_pos_start',
					esc_html__( 'End', 'noor-assistant' )               => 'icon_pos_end',
				)
			),
			array(
				'param_name'  => 'dima_vc_gradient',
				'group'       => esc_html__( 'Style', 'noor-assistant' ),
				'heading'     => esc_html__( 'Gradient Border', 'noor-assistant' ),
				'description' => esc_html__( 'Select gradient color', 'noor-assistant' ),
				'type'        => 'dropdown',
				'value'       => array(
					esc_html__( '- Select Option -', 'noor-assistant' ) => '',
					esc_html__( 'Orange', 'noor-assistant' )            => 'dima_grd_orange',
					esc_html__( 'blue', 'noor-assistant' )              => 'dima_grd_blue',
					esc_html__( 'purple', 'noor-assistant' )            => 'dima_grd_burple',
					esc_html__( 'green', 'noor-assistant' )             => 'dima_grd_green',
				)
			),
			array(
				'param_name'  => 'icon_color',
				'group'       => esc_html__( 'Style', 'noor-assistant' ),
				'heading'     => esc_html__( 'Icon Color', 'noor-assistant' ),
				'description' => esc_html__( 'Choose icon color.', 'noor-assistant' ),
				'dependency'  => array(
					'element' => 'icon_or_image',
					'value'   => 'icon',
				),
				'type'        => 'colorpicker',
			),
			array(
				'param_name'  => 'icon_bg',
				'group'       => esc_html__( 'Style', 'noor-assistant' ),
				'heading'     => esc_html__( 'Icon Background', 'noor-assistant' ),
				'description' => esc_html__( 'Choose icon background color.', 'noor-assistant' ),
				'type'        => 'colorpicker',
				'dependency'  => array(
					'element' => 'type',
					'value'   => array( 'circle', 'square' ),
				)
			),
			array(
				'param_name'  => 'shadow_color',
				'group'       => esc_html__( 'Style', 'noor-assistant' ),
				'heading'     => esc_html__( 'Icon Border', 'noor-assistant' ),
				'description' => esc_html__( 'Choose icon border color.', 'noor-assistant' ),
				'type'        => 'colorpicker',
				'dependency'  => array(
					'element' => 'type',
					'value'   => array( 'circle', 'square' ),
				)
			),
			//**************Animation***************//
			array(
				'param_name'  => 'animation',
				'group'       => esc_html__( 'Animation', 'noor-assistant' ),
				'heading'     => esc_html__( 'Animation', 'noor-assistant' ),
				'description' => esc_html__( 'Select the type of animation you want to use.', 'noor-assistant' ),
				'type'        => 'dropdown',
				'holder'      => 'div',
				'value'       => $dima_vc->dima_animate_list_velocity,
			),

			array(
				'param_name'  => 'delay',
				'group'       => esc_html__( 'Animation', 'noor-assistant' ),
				'heading'     => esc_html__( 'Delay Animation', 'noor-assistant' ),
				'description' => esc_html__( 'Set when the animation start (milliseconds)', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div',
				'value'       => ''
			),

			array(
				'param_name'  => 'delay_duration',
				'group'       => esc_html__( 'Animation', 'noor-assistant' ),
				'heading'     => esc_html__( 'duration Animation', 'noor-assistant' ),
				'description' => esc_html__( 'Animation duration of each element  (milliseconds).', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div',
				'value'       => ''
			),

			array(
				'param_name'  => 'delay_offset',
				'group'       => esc_html__( 'Animation', 'noor-assistant' ),
				'heading'     => esc_html__( 'Delay Animation Offset', 'noor-assistant' ),
				'description' => esc_html__( 'Set the number ( px,%..) from the top of the viewport where the animation start.', 'noor-assistant' ) . "<a href='http://imakewebthings.com/waypoints/api/offset-option/' target='_blank' >" . esc_html__( 'Read More', 'noor-assistant' ) . "</a>",
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div',
				'value'       => ''
			),
			//**************!Animation***************//
			array(
				'param_name'  => 'tutorials',
				'type'        => 'dima_doc_link_param',
				'heading'     => '<span class="tippy dima-help dima-vc-toolip">' . dima_get_svg_icon( "ic_help" )
				                 . '<span class="dima-vc-tooltip-text">'
				                 . esc_html__( 'Video tutorial and theme documentation article', 'noor-assistant' ) . '</span></span>'
				                 . esc_html__( 'Tutorial', 'noor-assistant' ),
				'doc_link'    => '',
				'video_link'  => 'https://www.youtube.com/watch?v=zoHTL477dfs',
				'doc_example' => 'https://noor.pixeldima.com/noor-icons/',
			),
		)
	)
);


/**
 * Iconbox content
 */
vc_map(
	array(
		'base'            => 'iconbox_content',
		'name'            => esc_html__( 'Iconbox Content', 'noor-assistant' ),
		'weight'          => 530,
		'class'           => 'dima-vc-element dima-vc-element-iconbox_content',
		'icon'            => 'iconbox',
		'category'        => esc_html__( 'Media', 'noor-assistant' ),
		'description'     => esc_html__( 'Iconbox content settings', 'noor-assistant' ),
		'as_child'        => array( 'only' => 'iconbox,process' ),
		'content_element' => true,
		'params'          => array(
			array(
				'param_name'  => 'content',
				'heading'     => esc_html__( 'Text', 'noor-assistant' ),
				'description' => esc_html__( 'Enter your text', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textarea_html',
				'holder'      => 'div',
				'value'       => ''
			),
			array(
				'param_name'  => 'title',
				'heading'     => esc_html__( 'Iconbox Title', 'noor-assistant' ),
				'description' => esc_html__( 'Add iconbox title', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'title_color',
				'heading'     => esc_html__( 'Title Color ', 'noor-assistant' ),
				'description' => esc_html__( 'Choose header title color', 'noor-assistant' ),
				'type'        => 'colorpicker',
			),
			//**************Animation***************//
			array(
				'param_name'  => 'animation',
				'group'       => esc_html__( 'Animation', 'noor-assistant' ),
				'heading'     => esc_html__( 'Animation', 'noor-assistant' ),
				'description' => esc_html__( 'Select the type of animation you want to use.', 'noor-assistant' ),
				'type'        => 'dropdown',
				'holder'      => 'div',
				'value'       => $dima_vc->dima_animate_list_velocity,
			),

			array(
				'param_name'  => 'delay',
				'group'       => esc_html__( 'Animation', 'noor-assistant' ),
				'heading'     => esc_html__( 'Delay Animation', 'noor-assistant' ),
				'description' => esc_html__( 'Set when the animation start (milliseconds)', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div',
				'value'       => ''
			),

			array(
				'param_name'  => 'delay_duration',
				'group'       => esc_html__( 'Animation', 'noor-assistant' ),
				'heading'     => esc_html__( 'duration Animation', 'noor-assistant' ),
				'description' => esc_html__( 'Animation duration of each element  (milliseconds).', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div',
				'value'       => ''
			),

			array(
				'param_name'  => 'delay_offset',
				'group'       => esc_html__( 'Animation', 'noor-assistant' ),
				'heading'     => esc_html__( 'Delay Animation Offset', 'noor-assistant' ),
				'description' => esc_html__( 'Set the number ( px,%..) from the top of the viewport where the animation start.', 'noor-assistant' ) . "<a href='http://imakewebthings.com/waypoints/api/offset-option/' target='_blank' >" . esc_html__( 'Read More', 'noor-assistant' ) . "</a>",
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div',
				'value'       => ''
			),
			//**************!Animation***************//
		)
	)
);