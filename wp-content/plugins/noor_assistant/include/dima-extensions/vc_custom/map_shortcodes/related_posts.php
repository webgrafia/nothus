<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/*
* Add-on Name: related posts
*/

class WPBakeryShortCode_Related_Posts extends WPBakeryShortCode {
}

/**
 * related posts
 */
vc_map(
	array(
		'base'        => 'related_posts',
		'name'        => esc_html__( 'Related posts', 'noor-assistant' ),
		'weight'      => 490,
		'class'       => 'dima-vc-element dima-vc-element-related_posts',
		'icon'        => 'related-posts',
		'category'    => esc_html__( 'Content', 'noor-assistant' ),
		'description' => esc_html__( 'Add related posts to your post content', 'noor-assistant' ),
		'params'      => array(
			array(
				'param_name'  => 'is_slide',
				'heading'     => esc_html__( 'Slide', 'noor-assistant' ),
				'description' => esc_html__( 'Check to display related posts as a slide', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'checkbox',
				'value'       => array(
					'' => 'true'
				),
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'column',
				'heading'     => esc_html__( 'Column', 'noor-assistant' ),
				'description' => esc_html__( 'Select columns number', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'dropdown',
				'holder'      => 'div',
				'value'       => array(
					esc_html__( '- Select Option -', 'noor-assistant' ) => '2',
					esc_html__( 'Two', 'noor-assistant' )               => '2',
					esc_html__( 'Three', 'noor-assistant' )             => '3',
					esc_html__( 'Four', 'noor-assistant' )              => '4',
					esc_html__( 'Five', 'noor-assistant' )              => '5',
					esc_html__( 'Six', 'noor-assistant' )               => '6'
				)
			),
			array(
				'param_name'  => 'count',
				'heading'     => esc_html__( 'Count', 'noor-assistant' ),
				'description' => esc_html__( 'Select how many posts to display.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'items_margin',
				'heading'     => esc_html__( 'Items Margin', 'noor-assistant' ),
				'description' => esc_html__( 'Enter the margin value between elements (px)', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'id',
				'heading'     => esc_html__( 'ID', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter a unique ID.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'class',
				'heading'     => esc_html__( 'Class', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter a unique class name.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'style',
				'heading'     => esc_html__( 'Style', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter inline CSS.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			)
		)
	)
);