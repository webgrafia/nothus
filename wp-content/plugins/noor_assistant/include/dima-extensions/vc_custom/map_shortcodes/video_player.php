<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/*
* Add-on Name: Video player
*/

class WPBakeryShortCode_Dima_Video_Player extends WPBakeryShortCode {
}

/**
 * Video player.
 */
vc_map(
	array(
		'base'        => 'dima_video_player',
		'name'        => esc_html__( 'Video (Self Hosted)', 'noor-assistant' ),
		'weight'      => 570,
		'class'       => 'dima-vc-element dima-vc-element-video-player',
		'icon'        => 'video-player',
		'category'    => esc_html__( 'Media', 'noor-assistant' ),
		'description' => esc_html__( 'Include responsive video into your content', 'noor-assistant' ),
		'params'      => array(
			array(
				'param_name'  => 'm4v',
				'heading'     => esc_html__( 'M4V', 'noor-assistant' ),
				'description' => esc_html__( 'Enter URL Link .m4v version of your video.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'ogv',
				'heading'     => esc_html__( 'OGV', 'noor-assistant' ),
				'description' => esc_html__( 'Enter URL Link .ogv version of your video for additional native browser support.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'webm',
				'heading'     => esc_html__( 'WebM', 'noor-assistant' ),
				'description' => esc_html__( 'Enter URL Link .webm version of your video for additional native browser support.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'poster_img',
				'heading'     => esc_html__( 'Poster Image', 'noor-assistant' ),
				'description' => esc_html__( 'Add a poster image for your self-hosted video.', 'noor-assistant' ),
				'type'        => 'attach_image',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'hide_controls',
				'heading'     => esc_html__( 'Hide Controls', 'noor-assistant' ),
				'description' => esc_html__( 'Check to hide the controls on your self-hosted video.', 'noor-assistant' ),
				'type'        => 'toggle',
				'holder'      => 'div',
				'value'       => 'false'
			),
			array(
				'param_name'  => 'muted',
				'heading'     => esc_html__( 'Muted', 'noor-assistant' ),
				'description' => esc_html__( 'Check to mute the video', 'noor-assistant' ),
				'type'        => 'toggle',
				'holder'      => 'div',
				'value'       => 'false'
			),
			array(
				'param_name'  => 'loop',
				'heading'     => esc_html__( 'Auto Replay', 'noor-assistant' ),
				'description' => esc_html__( 'Check to activate auto replay for your self-hosted video', 'noor-assistant' ),
				'type'        => 'toggle',
				'holder'      => 'div',
				'value'       => 'false'
			),
			array(
				'param_name'  => 'autoplay',
				'heading'     => esc_html__( 'Autoplay', 'noor-assistant' ),
				'description' => esc_html__( 'Check to automatically play your self-hosted video.', 'noor-assistant' ),
				'type'        => 'toggle',
				'holder'      => 'div',
				'value'       => 'false'
			),
			array(
				'param_name'  => 'id',
				'heading'     => esc_html__( 'ID', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter a unique ID.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'class',
				'heading'     => esc_html__( 'Class', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter a unique class name.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'style',
				'heading'     => esc_html__( 'Style', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter inline CSS.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'tutorials',
				'type'        => 'dima_doc_link_param',
				'heading'     => '<span class="tippy dima-help dima-vc-toolip">' . dima_get_svg_icon( "ic_help" )
				                 . '<span class="dima-vc-tooltip-text">'
				                 . esc_html__( 'Video tutorial and theme documentation article', 'noor-assistant' ) . '</span></span>'
				                 . esc_html__( 'Tutorial', 'noor-assistant' ),
				'doc_link'    => '',
				'video_link'  => 'https://www.youtube.com/watch?v=y3O3G7PbhMw',
				'doc_example' => 'https://noor.pixeldima.com/multimedia/',
			),

		)
	)
);

