<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/*
* Add-on Name: Line
*/

class WPBakeryShortCode_Google_Maptwo extends WPBakeryShortCodesContainer {
}

class WPBakeryShortCode_Google_Map_Marker extends WPBakeryShortCode {
}


/**
 * Google Map
 */
vc_map(
	array(
		'base'            => 'google_maptwo',
		'name'            => esc_html__( 'Google Map', 'noor-assistant' ),
		'weight'          => 530,
		'class'           => 'dima-vc-element dima-vc-element-google-map',
		'icon'            => 'google-map',
		'category'        => esc_html__( 'Media', 'noor-assistant' ),
		'description'     => esc_html__( 'Add a Google map', 'noor-assistant' ),
		'as_parent'       => array( 'only' => 'google_map_marker' ),
		'content_element' => true,
		'js_view'         => 'VcColumnView',
		'params'          => array(
			array(
				'param_name'  => 'lat',
				'heading'     => esc_html__( 'Latitude', 'noor-assistant' ),
				'description' => esc_html__( 'Enter the center latitude of your map.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'lng',
				'heading'     => esc_html__( 'Longitude', 'noor-assistant' ),
				'description' => esc_html__( 'Enter the center longitude of your map.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'drag',
				'heading'     => esc_html__( 'Draggable', 'noor-assistant' ),
				'description' => esc_html__( 'Check to allow users drag the map view.', 'noor-assistant' ),
				'type'        => 'checkbox',
				'holder'      => 'div',
				'value'       => array(
					'' => 'true'
				)
			),
			array(
				'param_name'  => 'zoom',
				'heading'     => esc_html__( 'Zoom Level', 'noor-assistant' ),
				'description' => esc_html__( 'Choose the initial zoom level of your map. This value should be between 1 and 18. 1 is fully zoomed out and 18 is right at street level.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'zoom_control',
				'heading'     => esc_html__( 'Zoom Control', 'noor-assistant' ),
				'description' => esc_html__( 'Check to activate the zoom control for the map.', 'noor-assistant' ),
				'type'        => 'checkbox',
				'holder'      => 'div',
				'value'       => array(
					'' => 'true'
				)
			),
			array(
				'param_name'  => 'height',
				'heading'     => esc_html__( 'Height', 'noor-assistant' ),
				'description' => esc_html__( 'Choose an optional height for your map. If no height is selected, a responsive, proportional unit will be used. Any type of unit is acceptable (e.g. 450px, 30em, 40%, et cetera).', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'width',
				'heading'     => esc_html__( 'Width', 'noor-assistant' ),
				'description' => esc_html__( 'Choose an optional width for your map. If no height is selected, a responsive, proportional unit will be used. Any type of unit is acceptable (e.g. 450px, 30em, 40%, et cetera).', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),

			array(
				'param_name'  => 'map_style',
				'heading'     => esc_html__( 'Map Style', 'noor-assistant' ),
				'description' => '',
				'type'        => 'dropdown',
				'value'       => array(
					'Standard'     => '',
					'Silver'       => 'silver',
					'Retro'        => 'retro',
					'Dark'         => 'dark',
					'Night'        => 'night',
					'Gray'         => 'gray',
					'Legacy North' => 'legacy_north',
					'Aubergine'    => 'aubergine'
				)
			),
			array(
				'param_name'  => 'class',
				'heading'     => esc_html__( 'Class', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter a unique class name.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'style',
				'heading'     => esc_html__( 'Style', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter inline CSS.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			//**************Animation***************//
			array(
				'param_name'  => 'animation',
				'group'       => esc_html__( 'Animation', 'noor-assistant' ),
				'heading'     => esc_html__( 'Animation', 'noor-assistant' ),
				'description' => esc_html__( 'Select the type of animation you want to use.', 'noor-assistant' ),
				'type'        => 'dropdown',
				'holder'      => 'div',
				'value'       => $dima_vc->dima_animate_list_velocity,
			),

			array(
				'param_name'  => 'delay',
				'group'       => esc_html__( 'Animation', 'noor-assistant' ),
				'heading'     => esc_html__( 'Delay Animation', 'noor-assistant' ),
				'description' => esc_html__( 'Set when the animation start (milliseconds)', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div',
				'value'       => ''
			),

			array(
				'param_name'  => 'delay_duration',
				'group'       => esc_html__( 'Animation', 'noor-assistant' ),
				'heading'     => esc_html__( 'duration Animation', 'noor-assistant' ),
				'description' => esc_html__( 'Animation duration of each element  (milliseconds).', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div',
				'value'       => ''
			),

			array(
				'param_name'  => 'delay_offset',
				'group'       => esc_html__( 'Animation', 'noor-assistant' ),
				'heading'     => esc_html__( 'Delay Animation Offset', 'noor-assistant' ),
				'description' => esc_html__( 'Set the number ( px,%..) from the top of the viewport where the animation start.', 'noor-assistant' ) . "<a href='http://imakewebthings.com/waypoints/api/offset-option/' target='_blank' >" . esc_html__( 'Read More', 'noor-assistant' ) . "</a>",
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div',
				'value'       => ''
			),
			//**************!Animation***************//
			array(
				'param_name'  => 'tutorials',
				'type'        => 'dima_doc_link_param',
				'heading'     => '<span class="tippy dima-help dima-vc-toolip">' . dima_get_svg_icon( "ic_help" )
				                 . '<span class="dima-vc-tooltip-text">'
				                 . esc_html__( 'Video tutorial and theme documentation article', 'noor-assistant' ) . '</span></span>'
				                 . esc_html__( 'Tutorial', 'noor-assistant' ),
				'doc_link'    => '',
				'video_link'  => 'https://www.youtube.com/watch?v=iHVimogrwAA',
				'doc_example' => 'https://noor.pixeldima.com/google-map/',
			),
		)
	)
);

/**
 * Google Map Marker
 */
vc_map(
	array(
		'base'            => 'google_map_marker',
		'name'            => esc_html__( 'Google Map Marker', 'noor-assistant' ),
		'weight'          => 530,
		'class'           => 'dima-vc-element dima-vc-element-google-map-marker',
		'icon'            => 'google-map-marker',
		'category'        => esc_html__( 'Media', 'noor-assistant' ),
		'description'     => esc_html__( 'Place a location marker on your Google map', 'noor-assistant' ),
		'as_child'        => array( 'only' => 'google_map' ),
		'content_element' => true,
		'params'          => array(
			array(
				'param_name'  => 'lat',
				'heading'     => esc_html__( 'Marker Latitude', 'noor-assistant' ),
				'description' => esc_html__( 'Enter the latitude of your marker.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'lng',
				'heading'     => esc_html__( 'Marker Longitude', 'noor-assistant' ),
				'description' => esc_html__( 'Enter the longitude of your marker.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'info',
				'heading'     => esc_html__( 'Additional Information', 'noor-assistant' ),
				'description' => esc_html__( 'Optional description text to appear in a popup when your marker is clicked on.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'image',
				'heading'     => esc_html__( 'Custom Marker Image', 'noor-assistant' ),
				'description' => esc_html__( 'Utilize a custom marker image instead of the default provided by Google.', 'noor-assistant' ),
				'type'        => 'attach_image',
				'holder'      => 'div'
			),
			//**************Animation***************//
			array(
				'param_name'  => 'animation',
				'group'       => esc_html__( 'Animation', 'noor-assistant' ),
				'heading'     => esc_html__( 'Animation', 'noor-assistant' ),
				'description' => esc_html__( 'Select the type of animation you want to use.', 'noor-assistant' ),
				'type'        => 'dropdown',
				'holder'      => 'div',
				'value'       => $dima_vc->dima_animate_list_velocity,
			),

			array(
				'param_name'  => 'delay',
				'group'       => esc_html__( 'Animation', 'noor-assistant' ),
				'heading'     => esc_html__( 'Delay Animation', 'noor-assistant' ),
				'description' => esc_html__( 'Set when the animation start (milliseconds)', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div',
				'value'       => ''
			),

			array(
				'param_name'  => 'delay_duration',
				'group'       => esc_html__( 'Animation', 'noor-assistant' ),
				'heading'     => esc_html__( 'duration Animation', 'noor-assistant' ),
				'description' => esc_html__( 'Animation duration of each element  (milliseconds).', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div',
				'value'       => ''
			),

			array(
				'param_name'  => 'delay_offset',
				'group'       => esc_html__( 'Animation', 'noor-assistant' ),
				'heading'     => esc_html__( 'Delay Animation Offset', 'noor-assistant' ),
				'description' => esc_html__( 'Set the number ( px,%..) from the top of the viewport where the animation start.', 'noor-assistant' ) . "<a href='http://imakewebthings.com/waypoints/api/offset-option/' target='_blank' >" . esc_html__( 'Read More', 'noor-assistant' ) . "</a>",
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div',
				'value'       => ''
			),
			//**************!Animation***************//
		)
	)
);