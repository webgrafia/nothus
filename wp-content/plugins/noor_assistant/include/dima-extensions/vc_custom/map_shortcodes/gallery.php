<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/*
* Add-on Name: Gallery
*/

class WPBakeryShortCode_Dima_Gallery extends WPBakeryShortCodesContainer {
}

/**
 * Gallery
 */
vc_map(
	array(
		'base'            => 'dima_gallery',
		'name'            => esc_html__( 'Gallery', 'noor-assistant' ),
		'weight'          => 960,
		'class'           => 'dima-vc-element dima-vc-element-item_icon',
		'icon'            => 'gallery',
		'as_parent'       => array( 'only' => 'image', 'vc_row' ),
		'category'        => esc_html__( 'Structure', 'noor-assistant' ),
		'description'     => esc_html__( 'Include an images gallery in your content', 'noor-assistant' ),
		'content_element' => true,
		'js_view'         => 'VcColumnView',
		'params'          => array(
			array(
				'param_name'  => 'column',
				'heading'     => esc_html__( 'Columns', 'noor-assistant' ),
				'description' => esc_html__( 'Select number of columns for the image gallery.', 'noor-assistant' ),
				'type'        => 'dropdown',
				'holder'      => 'div',
				'value'       => array(
					esc_html__( '- Select Option -', 'noor-assistant' ) => '',
					esc_html__( '1 column', 'noor-assistant' )          => '1',
					esc_html__( '2 columns', 'noor-assistant' )         => '2',
					esc_html__( '3 columns', 'noor-assistant' )         => '3',
					esc_html__( '4 columns', 'noor-assistant' )         => '4',
					esc_html__( '5 columns', 'noor-assistant' )         => '5',
					esc_html__( '6 columns', 'noor-assistant' )         => '6',
				)
			),

			array(
				'param_name'  => 'id',
				'heading'     => esc_html__( 'ID', 'noor-assistant' ),
				'description' => $dima_vc->id_des,
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'class',
				'heading'     => esc_html__( 'Class', 'noor-assistant' ),
				'description' => $dima_vc->class_des,
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'style',
				'heading'     => esc_html__( 'Style', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter inline CSS.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'tutorials',
				'type'        => 'dima_doc_link_param',
				'heading'     => '<span class="tippy dima-help dima-vc-toolip">' . dima_get_svg_icon( "ic_help" )
				                 . '<span class="dima-vc-tooltip-text">'
				                 . esc_html__( 'Video tutorial and theme documentation article', 'noor-assistant' ) . '</span></span>'
				                 . esc_html__( 'Tutorial', 'noor-assistant' ),
				'doc_link'    => '',
				'video_link'  => 'https://www.youtube.com/watch?v=u-pd_21ICXE',
				'doc_example' => 'https://noor.pixeldima.com/lightbox-gallery/',
			),
		)
	)
);