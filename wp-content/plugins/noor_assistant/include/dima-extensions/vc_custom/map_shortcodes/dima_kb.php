<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/*
* Add-on Name: divider
*/


if ( DIMA_KB_IS_ACTIVE ) {
	class WPBakeryShortCode_Dima_Kb_search extends WPBakeryShortCode {
	}

	vc_map(
		array(
			'base'        => 'dima_kb_search',
			'name'        => esc_html__( 'Knowledgebase Search', 'noor-assistant' ),
			'weight'      => 980,
			'class'       => 'dima-vc-element dima-vc-element-search',
			'icon'        => 'divider',
			'category'    => esc_html__( 'Content', 'noor-assistant' ),
			'description' => esc_html__( 'Add a Knowledgebase Search', 'noor-assistant' ),
			'params'      => array(
				array(
					'param_name'  => 'float',
					'heading'     => esc_html__( 'Float', 'noor-assistant' ),
					'description' => esc_html__( 'Select the Position of the divider and icon.', 'noor-assistant' ),
					'type'        => 'dropdown',
					'save_always' => true,
					'value'       => array(
						esc_html__( 'Start', 'noor-assistant' )  => 'start',
						esc_html__( 'center', 'noor-assistant' ) => 'center_two',
						esc_html__( 'End', 'noor-assistant' )    => 'end',
					),
					'holder'      => 'div'
				),

				array(
					'param_name'  => 'searchbox_placeholder',
					'heading'     => esc_html__( 'Search box Placeholder', 'noor-assistant' ),
					'description' => '',
					'save_always' => true,
					'type'        => 'textfield',
					'std'         => esc_html__( 'Search Knowledge Base', 'noor-assistant' ),
					'holder'      => 'div'
				),

				array(
					'param_name'  => 'searchbox_width',
					'heading'     => esc_html__( 'Search box width', 'noor-assistant' ),
					'description' => esc_html__( 'Enter search box width (%)', 'noor-assistant' ),
					'save_always' => true,
					'type'        => 'textfield',
					'std'         => '100%',
					'holder'      => 'div'
				),

				array(
					'param_name'  => 'button',
					'heading'     => esc_html__( 'Hide search button', 'noor-assistant' ),
					'description' => esc_html__( 'Check to hide button.', 'noor-assistant' ),
					'type'        => 'checkbox',
					'holder'      => 'div',
					'value'       => array(
						'' => 'false'
					)
				),

				array(
					'param_name'  => 'id',
					'heading'     => esc_html__( 'ID', 'noor-assistant' ),
					'description' => $dima_vc->id_des,
					'save_always' => true,
					'type'        => 'textfield',
					'holder'      => 'div'
				),

				array(
					'param_name'  => 'class',
					'heading'     => esc_html__( 'Class', 'noor-assistant' ),
					'description' => $dima_vc->class_des,
					'save_always' => true,
					'type'        => 'textfield',
					'holder'      => 'div'
				),

				//**************Animation***************//
				array(
					'param_name'  => 'animation',
					'group'       => esc_html__( 'Animation', 'noor-assistant' ),
					'heading'     => esc_html__( 'Animation', 'noor-assistant' ),
					'description' => esc_html__( 'Select the type of animation you want to use.', 'noor-assistant' ),
					'type'        => 'dropdown',
					'holder'      => 'div',
					'value'       => $dima_vc->dima_animate_list_velocity,
				),

				array(
					'param_name'  => 'delay',
					'group'       => esc_html__( 'Animation', 'noor-assistant' ),
					'heading'     => esc_html__( 'Delay Animation', 'noor-assistant' ),
					'description' => esc_html__( 'Set when the animation start (milliseconds)', 'noor-assistant' ),
					'save_always' => true,
					'type'        => 'textfield',
					'holder'      => 'div',
					'value'       => ''
				),

				array(
					'param_name'  => 'delay_duration',
					'group'       => esc_html__( 'Animation', 'noor-assistant' ),
					'heading'     => esc_html__( 'duration Animation', 'noor-assistant' ),
					'description' => esc_html__( 'Animation duration of each element  (milliseconds).', 'noor-assistant' ),
					'save_always' => true,
					'type'        => 'textfield',
					'holder'      => 'div',
					'value'       => ''
				),

				array(
					'param_name'  => 'delay_offset',
					'group'       => esc_html__( 'Animation', 'noor-assistant' ),
					'heading'     => esc_html__( 'Delay Animation Offset', 'noor-assistant' ),
					'description' => esc_html__( 'Set the number ( px,%..) from the top of the viewport where the animation start.', 'noor-assistant' ) . "<a href='http://imakewebthings.com/waypoints/api/offset-option/' target='_blank' >" . esc_html__( 'Read More', 'noor-assistant' ) . "</a>",
					'save_always' => true,
					'type'        => 'textfield',
					'holder'      => 'div',
					'value'       => ''
				),
				//**************!Animation***************//
			)
		)
	);
}
/**
 * Divider.
 */
