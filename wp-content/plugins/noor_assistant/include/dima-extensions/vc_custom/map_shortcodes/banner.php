<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/*
* Add-on Name: Banner
*/

class WPBakeryShortCode_Dima_Banner extends WPBakeryShortCodesContainer {
}

/**
 * Banner
 */
vc_map(
	array(
		'base'            => 'dima_banner',
		'name'            => esc_html__( 'Banner', 'noor-assistant' ),
		'weight'          => 710,
		'class'           => 'dima-vc-element dima-vc-element-callout',
		'icon'            => 'callout',
		'category'        => esc_html__( 'Marketing', 'noor-assistant' ),
		'as_parent'       => array(
			'except' => 'container,vc_row_inner,vc_row,accordion_item,tab_nav_item,client'
		),
		'content_element' => true,
		'js_view'         => 'VcColumnView',
		'description'     => esc_html__( 'Include a Banner into your content', 'noor-assistant' ),
		'params'          => array(

			array(
				'param_name'  => 'height',
				'heading'     => esc_html__( 'Banner Height', 'noor-assistant' ),
				'description' => esc_html__( 'Set banner height (%,px,em)', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div',
				'std'         => '150px'
			),

			array(
				'param_name'  => 'bg_type',
				'group'       => esc_html__( 'Style', 'noor-assistant' ),
				'heading'     => esc_html__( 'Banner Background Type', 'noor-assistant' ),
				'description' => esc_html__( 'Select background type.', 'noor-assistant' ),
				'type'        => 'dropdown',
				'save_always' => true,
				'value'       => array(
					esc_html__( 'Covered Background Image', 'noor-assistant' ) => 'bg_image',
					esc_html__( 'Background Video', 'noor-assistant' )         => 'bg_video',
				),
				'holder'      => 'div',
			),
			array(
				'param_name'  => 'bg_image',
				'group'       => esc_html__( 'Style', 'noor-assistant' ),
				'heading'     => esc_html__( 'Banner Background Image', 'noor-assistant' ),
				'description' => esc_html__( 'Add a background image to your Banner', 'noor-assistant' ),
				'save_always' => true,
				'dependency'  => array(
					'element' => 'bg_type',
					'value'   => 'bg_image',
				),
				'type'        => 'attach_image',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'bg_video',
				'group'       => esc_html__( 'Style', 'noor-assistant' ),
				'heading'     => esc_html__( 'Background Video', 'noor-assistant' ),
				'description' => esc_html__( 'Include the path to your background video.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'dependency'  => array(
					'element' => 'bg_type',
					'value'   => 'bg_video',
				),
				'holder'      => 'div'
			),

			array(
				'param_name'  => 'parallax',
				'group'       => esc_html__( 'Style', 'noor-assistant' ),
				'heading'     => esc_html__( 'Parallax', 'noor-assistant' ),
				'description' => esc_html__( 'Select to activate the parallax effect (Applied only for background patterns and images)', 'noor-assistant' ),
				'type'        => 'dropdown',
				'holder'      => 'div',
				'value'       => array(
					esc_html__( '- Select Option -', 'noor-assistant' ) => '',
					esc_html__( 'Parallax', 'noor-assistant' )          => 'parallax',
					esc_html__( 'Fixed Parallax', 'noor-assistant' )    => 'fixed_parallax',
				)
			),

			array(
				'param_name'  => 'cover',
				'group'       => esc_html__( 'Style', 'noor-assistant' ),
				'heading'     => esc_html__( 'Cover', 'noor-assistant' ),
				'description' => esc_html__( 'Select to activate the cover above the background patterns and images.', 'noor-assistant' ),
				'type'        => 'toggle',
				'holder'      => 'div',
				'value'       => 'false'
			),

			array(
				"type"        => "dropdown",
				"heading"     => __( "Gradient Overlay Orientation", "noor-assistant" ),
				'group'       => esc_html__( 'Style', 'noor-assistant' ),
				"param_name"  => "bg_gradient",
				"width"       => 150,
				"value"       => array(
					__( '-- No Gradient --', "noor-assistant" ) => "false",
					__( 'Vertical ↓', "noor-assistant" )        => "vertical",
					__( 'Horizontal →', "noor-assistant" )      => "horizontal",
					__( 'Diagonal ↘', "noor-assistant" )        => "left_top",
					__( 'Diagonal ↗', "noor-assistant" )        => "left_bottom",
					__( 'Radial ○', "noor-assistant" )          => "radial",
				),
				"description" => __( "Choose the orientation of gradient overlay", "noor-assistant" ),
				'dependency'  => array(
					'element' => 'cover',
					'value'   => 'true',
				),
			),
			array(
				"type"             => "colorpicker",
				"heading"          => __( "Overlay Color Start", "noor-assistant" ),
				'group'            => esc_html__( 'Style', 'noor-assistant' ),
				"param_name"       => "cover_color",
				"value"            => "",
				'dependency'       => array(
					'element' => 'cover',
					'value'   => 'true',
				),
				"description"      => __( "Primary overlay color. Start color if used with gradient option selected.", "noor-assistant" ),
				'edit_field_class' => 'no-border-bottom vc_column vc_col-sm-6'
			),
			array(
				"type"             => "colorpicker",
				"heading"          => __( "Overlay Color End", "noor-assistant" ),
				'group'            => esc_html__( 'Style', 'noor-assistant' ),
				"param_name"       => "gr_end",
				"value"            => "",
				"description"      => __( "The ending color for gradient fill overlay. Use only with gradient option selected.", "noor-assistant" ),
				"dependency"       => array(
					'element' => "bg_gradient",
					'value'   => array(
						"vertical",
						"horizontal",
						"left_top",
						"left_bottom",
						"radial"
					)
				),
				'edit_field_class' => 'no-border-bottom vc_column vc_col-sm-6'
			),
			array(
				"type"        => "range",
				"heading"     => __( "Overlay Color Mask Opacity", "noor-assistant" ),
				'group'       => esc_html__( 'Style', 'noor-assistant' ),
				"param_name"  => "gr_opacity",
				"value"       => "0.6",
				"min"         => "0",
				"max"         => "1",
				"step"        => "0.1",
				"unit"        => 'alpha',
				"dependency"  => array(
					'element' => "bg_gradient",
					'value'   => array(
						"vertical",
						"horizontal",
						"left_top",
						"left_bottom",
						"radial"
					)
				),
				"description" => __( "The opacity of overlay layer which you set above. ", "noor-assistant" )
			),

			array(
				'param_name'  => 'link',
				'heading'     => esc_html__( 'URL Link', 'noor-assistant' ),
				'description' => esc_html__( 'Enter in the URL you want your banner link to.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'target',
				'heading'     => esc_html__( 'Open link in a new tab', 'noor-assistant' ),
				'description' => esc_html__( 'Check to open your link in a new tab.', 'noor-assistant' ),
				'type'        => 'checkbox',
				'holder'      => 'div',
				'value'       => array(
					'' => 'blank'
				)
			),
			array(
				'param_name'  => 'text_color',
				'heading'     => esc_html__( 'Text Color', 'noor-assistant' ),
				'description' => '',
				'type'        => 'dropdown',
				'holder'      => 'div',
				'value'       => array(
					esc_html__( '- Select Option -', 'noor-assistant' ) => '',
					esc_html__( 'light', 'noor-assistant' )             => 'light',
					esc_html__( 'dark', 'noor-assistant' )              => 'dark',
				)
			),
			array(
				'param_name'  => 'text_width',
				'heading'     => esc_html__( 'Text Width', 'noor-assistant' ),
				'description' => esc_html__( 'Set banner text width (%,px,em)', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'text_align',
				'heading'     => esc_html__( 'Contents Alignment', 'noor-assistant' ),
				'description' => esc_html__( 'Select the alignment for your banner contents.', 'noor-assistant' ),
				'type'        => 'dropdown',
				'holder'      => 'div',
				'value'       => array(
					esc_html__( '- Select Option -', 'noor-assistant' ) => '',
					esc_html__( 'Start', 'noor-assistant' )             => 'start',
					esc_html__( 'Center', 'noor-assistant' )            => 'center',
					esc_html__( 'End', 'noor-assistant' )               => 'end'
				)
			),
			array(
				'param_name'  => 'flaot',
				'heading'     => esc_html__( 'Content Float', 'noor-assistant' ),
				'description' => esc_html__( 'Select the float for your banner contents.', 'noor-assistant' ),
				'type'        => 'dropdown',
				'holder'      => 'div',
				'value'       => array(
					esc_html__( '- Select Option -', 'noor-assistant' ) => '',
					esc_html__( 'Start', 'noor-assistant' )             => 'start',
					esc_html__( 'Center', 'noor-assistant' )            => 'center',
					esc_html__( 'End', 'noor-assistant' )               => 'end'
				)
			),

			array(
				'param_name'  => 'id',
				'heading'     => esc_html__( 'ID', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter a unique ID.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'class',
				'heading'     => esc_html__( 'Class', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter a unique class name.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'style',
				'heading'     => esc_html__( 'Style', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter inline CSS.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			//**************Animation***************//
			array(
				'param_name'  => 'animation',
				'group'       => esc_html__( 'Animation', 'noor-assistant' ),
				'heading'     => esc_html__( 'Animation', 'noor-assistant' ),
				'description' => esc_html__( 'Select the type of animation you want to use.', 'noor-assistant' ),
				'type'        => 'dropdown',
				'holder'      => 'div',
				'value'       => $dima_vc->dima_animate_list_velocity,
			),

			array(
				'param_name'  => 'delay',
				'group'       => esc_html__( 'Animation', 'noor-assistant' ),
				'heading'     => esc_html__( 'Delay Animation', 'noor-assistant' ),
				'description' => esc_html__( 'Set when the animation start (milliseconds)', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div',
				'value'       => ''
			),

			array(
				'param_name'  => 'delay_duration',
				'group'       => esc_html__( 'Animation', 'noor-assistant' ),
				'heading'     => esc_html__( 'duration Animation', 'noor-assistant' ),
				'description' => esc_html__( 'Animation duration of each element  (milliseconds).', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div',
				'value'       => ''
			),

			array(
				'param_name'  => 'delay_offset',
				'group'       => esc_html__( 'Animation', 'noor-assistant' ),
				'heading'     => esc_html__( 'Delay Animation Offset', 'noor-assistant' ),
				'description' => esc_html__( 'Set the number ( px,%..) from the top of the viewport where the animation start.', 'noor-assistant' ) . "<a href='http://imakewebthings.com/waypoints/api/offset-option/' target='_blank' >" . esc_html__( 'Read More', 'noor-assistant' ) . "</a>",
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div',
				'value'       => ''
			),
			//**************!Animation***************//

			array(
				'param_name'  => 'tutorials',
				'type'        => 'dima_doc_link_param',
				'heading'     => '<span class="tippy dima-help dima-vc-toolip">' . dima_get_svg_icon( "ic_help" )
				                 . '<span class="dima-vc-tooltip-text">'
				                 . esc_html__( 'Video tutorial and theme documentation article', 'noor-assistant' ) . '</span></span>'
				                 . esc_html__( 'Tutorial', 'noor-assistant' ),
				'doc_link'    => '',
				'video_link'  => 'https://www.youtube.com/watch?v=MP3VjMfwBqo',
				'doc_example' => 'https://noor.pixeldima.com/banners/',
			),

		)
	)
);