<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/*
* Add-on Name: Custom Contact Form 7
*/

class WPBakeryShortCode_Dima_Contact_Form_7 extends WPBakeryShortCode {
}

/**
 * Contact Form
 */
if ( ( is_plugin_active( 'contact-form-7/wp-contact-form-7.php' ) || defined( 'WPCF7_PLUGIN' ) ) ) {
	vc_map(

		array(
			'base'        => 'dima_contact_form_7',
			'name'        => esc_html__( 'Contact Form 7 (PixelDima)', 'noor-assistant' ),
			'weight'      => 510,
			'class'       => 'dima-vc-element dima-vc-element-contact-form-7',
			'icon'        => 'contact-form-7',
			'category'    => esc_html__( 'Social', 'noor-assistant' ),
			'description' => esc_html__( 'Place one of your contact forms into your content', 'noor-assistant' ),
			'params'      => array(
				array(
					'param_name'  => 'style',
					'heading'     => esc_html__( 'Style', 'noor-assistant' ),
					'description' => esc_html__( '(Optional) Enter inline CSS.', 'noor-assistant' ),
					'save_always' => true,
					'type'        => 'dropdown',
					'value'       => array(
						esc_html__( 'Inherit', 'noor-assistant' )         => '',
						esc_html__( 'Material Design', 'noor-assistant' ) => 'matrial-form',
					),
					'holder'      => 'div'
				),
				array(
					'type'        => 'dropdown',
					'heading'     => __( 'Select contact form', 'noor-assistant' ),
					'param_name'  => 'form_id',
					'value'       => $dima_vc->contact_forms,
					'save_always' => true,
					'description' => __( 'Choose previously created contact form from the drop down list.', 'noor-assistant' ),
				),
				array(
					'param_name'  => 'id',
					'heading'     => esc_html__( 'ID', 'noor-assistant' ),
					'description' => esc_html__( '(Optional) Enter a unique ID.', 'noor-assistant' ),
					'save_always' => true,
					'type'        => 'textfield',
					'holder'      => 'div'
				),
				array(
					'param_name'  => 'class',
					'heading'     => esc_html__( 'Class', 'noor-assistant' ),
					'description' => esc_html__( '(Optional) Enter a unique class name.', 'noor-assistant' ),
					'save_always' => true,
					'type'        => 'textfield',
					'holder'      => 'div'
				),
				array(
					'param_name'  => 'tutorials',
					'type'        => 'dima_doc_link_param',
					'heading'     => '<span class="tippy dima-help dima-vc-toolip">' . dima_get_svg_icon( "ic_help" )
					                 . '<span class="dima-vc-tooltip-text">'
					                 . esc_html__( 'Video tutorial and theme documentation article', 'noor-assistant' ) . '</span></span>'
					                 . esc_html__( 'Tutorial', 'noor-assistant' ),
					'doc_link'    => '',
					'video_link'  => 'https://www.youtube.com/watch?v=SiS4AcgEmF8',
					'doc_example' => 'https://noor.pixeldima.com/contact-forms/',
				),
			)
		)
	);
}

