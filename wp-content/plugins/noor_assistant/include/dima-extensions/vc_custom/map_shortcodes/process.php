<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/*
* Add-on Name: Line
*/

class WPBakeryShortCode_Process extends WPBakeryShortCodesContainer {
}

/**
 * process
 */
vc_map(
	array(
		'base'            => 'process',
		'name'            => esc_html__( 'Process', 'noor-assistant' ),
		'weight'          => 530,
		'class'           => 'dima-vc-element dima-vc-element-process',
		'icon'            => 'icon-timeline',
		'category'        => esc_html__( 'Media', 'noor-assistant' ),
		'description'     => esc_html__( 'Add icon timeline to your content', 'noor-assistant' ),
		'as_parent'       => array( 'only' => 'iconbox' ),
		'js_view'         => 'VcColumnView',
		'content_element' => true,
		'params'          => array(
			array(
				'param_name'  => 'vh',
				'heading'     => esc_html__( 'Process Style', 'noor-assistant' ),
				'description' => '',
				'type'        => 'dropdown',
				'holder'      => 'div',
				'value'       => array(
					esc_html__( '- Select Option -', 'noor-assistant' ) => '',
					esc_html__( 'Vertical', 'noor-assistant' )          => 'process-v',
					esc_html__( 'Horizontal', 'noor-assistant' )        => 'process-h',
				)
			),
			array(
				'param_name'  => 'columns',
				'heading'     => esc_html__( 'Columns', 'noor-assistant' ),
				'description' => '',
				'type'        => 'dropdown',
				'holder'      => 'div',
				'dependency'  => array(
					'element' => 'vh',
					'value'   => 'process-h',
				),
				'value'       => array(
					esc_html__( '- Select Option -', 'noor-assistant' ) => '',
					esc_html__( 'Three', 'noor-assistant' )             => 'columns-3',
					esc_html__( 'Four', 'noor-assistant' )              => 'columns-4',
					esc_html__( 'Five', 'noor-assistant' )              => 'columns-5',
				)
			),

			array(
				'param_name'  => 'float',
				'heading'     => esc_html__( 'Float', 'noor-assistant' ),
				'description' => esc_html__( 'Select the position of the process', 'noor-assistant' ),
				'type'        => 'dropdown',
				'holder'      => 'div',
				'dependency'  => array(
					'element' => 'vh',
					'value'   => 'process-v',
				),
				'value'       => array(
					esc_html__( '- Select Option -', 'noor-assistant' ) => '',
					esc_html__( 'None', 'noor-assistant' )              => '',
					esc_html__( 'Start', 'noor-assistant' )             => 'start',
					esc_html__( 'End', 'noor-assistant' )               => 'end',
				)
			),
			array(
				'param_name'  => 'id',
				'heading'     => esc_html__( 'ID', 'noor-assistant' ),
				'description' => $dima_vc->id_des,
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'class',
				'heading'     => esc_html__( 'Class', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter a unique class name.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'style',
				'heading'     => esc_html__( 'Style', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter inline CSS.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'tutorials',
				'type'        => 'dima_doc_link_param',
				'heading'     => '<span class="tippy dima-help dima-vc-toolip">' . dima_get_svg_icon( "ic_help" )
				                 . '<span class="dima-vc-tooltip-text">'
				                 . esc_html__( 'Video tutorial and theme documentation article', 'noor-assistant' ) . '</span></span>'
				                 . esc_html__( 'Tutorial', 'noor-assistant' ),
				'doc_link'    => '',
				'video_link'  => 'https://www.youtube.com/watch?v=2WuAKsb4fas',
				'doc_example' => 'https://noor.pixeldima.com/process/',
			),

		)
	)
);
