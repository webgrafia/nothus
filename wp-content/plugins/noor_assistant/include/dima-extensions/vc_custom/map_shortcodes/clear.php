<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/*
* Add-on Name: clear
*/

class WPBakeryShortCode_Clear extends WPBakeryShortCode {
}

/**
 * Clear.
 */
vc_map(
	array(
		'base'        => 'clear',
		'name'        => esc_html__( 'Clear', 'noor-assistant' ),
		'weight'      => 960,
		'class'       => 'dima-vc-element dima-vc-element-clear',
		'icon'        => 'clear',
		'category'    => esc_html__( 'Structure', 'noor-assistant' ),
		'description' => esc_html__( 'Clear floated elements in your content', 'noor-assistant' ),
		'params'      => array(
			array(
				'param_name'  => 'by',
				'heading'     => esc_html__( 'Size', 'noor-assistant' ),
				'description' => esc_html__( 'Enter the space size (Pixels, ems or percentages).', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'value'       => '15px',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'id',
				'heading'     => esc_html__( 'ID', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter a unique ID.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'class',
				'heading'     => esc_html__( 'Class', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter a unique class name.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			)
		)
	)
);