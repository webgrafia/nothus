<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/*
* Add-on Name: Portfolio
*/

class WPBakeryShortCode_Portfolio extends WPBakeryShortCode {
}

/**
 * Portfolio
 */
vc_map(
	array(
		'base'        => 'portfolio',
		'name'        => esc_html__( 'Portfolio', 'noor-assistant' ),
		'weight'      => 490,
		'class'       => 'dima-vc-element dima-vc-element-portfolio',
		'icon'        => 'portfolio',
		'category'    => esc_html__( 'Content', 'noor-assistant' ),
		'description' => esc_html__( 'Display your Portfolio posts', 'noor-assistant' ),
		'params'      => array(
			array(
				'param_name'  => 'portfolio_style',
				'heading'     => esc_html__( 'Portfolio Style', 'noor-assistant' ),
				'description' => esc_html__( 'Choose your portfolio style.', 'noor-assistant' ),
				'type'        => 'dropdown',
				'holder'      => 'div',
				'value'       => array(
					esc_html__( '- Select Option -', 'noor-assistant' ) => 'masonry',
					esc_html__( 'Grid', 'noor-assistant' )              => 'grid',
					esc_html__( 'Masonry', 'noor-assistant' )           => 'masonry',
					esc_html__( 'Slide', 'noor-assistant' )             => 'slide',
				)
			),
			array(
				'param_name'  => 'auto_play',
				'heading'     => esc_html__( 'AutoPlay', 'noor-assistant' ),
				'description' => esc_html__( 'Check to animate the slides automatically', 'noor-assistant' ),
				'type'        => 'checkbox',
				'holder'      => 'div',
				'dependency'  => array(
					'element' => 'portfolio_style',
					'value'   => array( 'slide' ),
				),
				'value'       => array(
					'' => 'true'
				)
			),
			array(
				'param_name'  => 'loop',
				'heading'     => esc_html__( 'Loop', 'noor-assistant' ),
				'description' => esc_html__( 'Check to activate slider loop', 'noor-assistant' ),
				'type'        => 'checkbox',
				'holder'      => 'div',
				'dependency'  => array(
					'element' => 'portfolio_style',
					'value'   => array( 'slide' ),
				),
				'value'       => array(
					'' => 'true'
				)
			),

			array(
				'param_name'  => 'dark',
				'heading'     => esc_html__( 'Dark background', 'noor-assistant' ),
				'description' => esc_html__( 'Check if you use dark background section', 'noor-assistant' ),
				'type'        => 'checkbox',
				'holder'      => 'div',
				'dependency'  => array(
					'element' => 'portfolio_style',
					'value'   => array( 'slide' ),
				),
				'value'       => array(
					'' => 'false'
				)
			),
			array(
				'param_name'  => 'slide_pagination',
				'heading'     => esc_html__( 'Hide Slider Bullets', 'noor-assistant' ),
				'description' => esc_html__( 'Check to remove slider Bullets', 'noor-assistant' ),
				'type'        => 'checkbox',
				'holder'      => 'div',
				'dependency'  => array(
					'element' => 'portfolio_style',
					'value'   => array( 'slide' ),
				),
				'value'       => array(
					'' => 'false'
				)
			),
			array(
				'param_name'  => 'slide_arrows',
				'heading'     => esc_html__( 'Hide Slider Arrows', 'noor-assistant' ),
				'description' => esc_html__( 'Check to remove slider Arrows', 'noor-assistant' ),
				'type'        => 'checkbox',
				'holder'      => 'div',
				'dependency'  => array(
					'element' => 'portfolio_style',
					'value'   => array( 'slide' ),
				),
				'value'       => array(
					'' => 'false'
				)
			),
			array(
				'param_name'  => 'count',
				'heading'     => esc_html__( 'Portfolio Post Count', 'noor-assistant' ),
				'description' => esc_html__( 'Select how many posts to display.', 'noor-assistant' ),
				'type'        => 'textfield',
				'holder'      => 'div',
				'value'       => '6',
			),
			array(
				'param_name'  => 'column',
				'heading'     => esc_html__( 'Portfolio Post Columns', 'noor-assistant' ),
				'description' => esc_html__( 'Select columns number', 'noor-assistant' ),
				'type'        => 'dropdown',
				'holder'      => 'div',
				'value'       => array(
					esc_html__( '- Select Option -', 'noor-assistant' ) => '2',
					'1'                                                 => '1',
					'2'                                                 => '2',
					'3'                                                 => '3',
					'4'                                                 => '4'
				)
			),
			array(
				'param_name'  => 'filters',
				'heading'     => esc_html__( 'Hide filters', 'noor-assistant' ),
				'description' => esc_html__( 'Check to hide the filters.', 'noor-assistant' ),
				'type'        => 'checkbox',
				'holder'      => 'div',
				'dependency'  => array(
					'element' => 'portfolio_style',
					'value'   => array( 'masonry', 'grid' ),
				),
				'value'       => array(
					'' => 'false'
				)
			),

			array(
				'param_name'  => 'hide_all',
				'heading'     => esc_html__( 'Hide All', 'noor-assistant' ),
				'description' => esc_html__( 'Check to hide "All" the first element in the filters list.', 'noor-assistant' ),
				'type'        => 'checkbox',
				'holder'      => 'div',
				'dependency'  => array(
					'element' => 'portfolio_style',
					'value'   => array( 'masonry', 'grid' ),
				),
				'value'       => array(
					'' => 'false'
				)
			),

			array(
				'param_name'  => 'no_margin',
				'heading'     => esc_html__( 'No margin', 'noor-assistant' ),
				'description' => esc_html__( 'Check to remove the margin between portfolio posts', 'noor-assistant' ),
				'type'        => 'checkbox',
				'holder'      => 'div',
				'value'       => array(
					'' => 'true'
				)
			),
			array(
				'param_name'  => 'img_hover',
				'heading'     => esc_html__( 'Image hover style', 'noor-assistant' ),
				'description' => esc_html__( 'Select your image hover style', 'noor-assistant' ),
				'type'        => 'dropdown',
				'holder'      => 'div',
				'value'       => array(
					esc_html__( '- Select Option -', 'noor-assistant' ) => '',
					esc_html__( 'none', 'noor-assistant' )              => 'op_vc_none',
					esc_html__( 'default', 'noor-assistant' )           => '',
					esc_html__( 'zoom-out', 'noor-assistant' )          => 'op_vc_zoom-out',
					esc_html__( 'zoom-in', 'noor-assistant' )           => 'op_vc_zoom-in',
					esc_html__( 'gray', 'noor-assistant' )              => 'op_vc_gray',
					esc_html__( 'Opacity', 'noor-assistant' )           => 'op_vc_opacity',
				)
			),
			array(
				'param_name'  => 'elm_hover',
				'heading'     => esc_html__( 'Element hover', 'noor-assistant' ),
				'description' => esc_html__( 'Select portfolio elements hover style', 'noor-assistant' ),
				'type'        => 'dropdown',
				'holder'      => 'div',
				'value'       => array(
					esc_html__( '- Select Option -', 'noor-assistant' ) => '',
					esc_html__( 'default', 'noor-assistant' )           => '',
					esc_html__( 'Inside', 'noor-assistant' )            => 'op_vc_inside',
					esc_html__( 'none', 'noor-assistant' )              => 'op_vc_none',
				)
			),
			array(
				'param_name'  => 'category',
				'heading'     => esc_html__( 'Category', 'noor-assistant' ),
				'description' => esc_html__( 'To filter your posts by category, enter in the category ID of your desired category. To filter by multiple categories, enter in your IDs separated by a comma.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'offset',
				'heading'     => esc_html__( 'Offset', 'noor-assistant' ),
				'description' => esc_html__( 'Accepts a numerical value to show how many posts to skip.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'paging',
				'heading'     => esc_html__( 'Pagination', 'noor-assistant' ),
				'description' => esc_html__( 'Check to activate pagination', 'noor-assistant' ),
				'type'        => 'checkbox',
				'holder'      => 'div',
				'value'       => array(
					'' => 'true'
				)
			),
			array(
				'param_name'  => 'post_class',
				'heading'     => esc_html__( 'Class', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter a unique class name.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'tutorials',
				'type'        => 'dima_doc_link_param',
				'heading'     => '<span class="tippy dima-help dima-vc-toolip">' . dima_get_svg_icon( "ic_help" )
				                 . '<span class="dima-vc-tooltip-text">'
				                 . esc_html__( 'Video tutorial and theme documentation article', 'noor-assistant' ) . '</span></span>'
				                 . esc_html__( 'Tutorial', 'noor-assistant' ),
				'doc_link'    => '',
				'video_link'  => 'https://www.youtube.com/watch?v=PEyqCYKHc9g',
				'doc_example' => 'https://noor.pixeldima.com/masonry-two-columns-with-margin/',
			),
		)
	)
);