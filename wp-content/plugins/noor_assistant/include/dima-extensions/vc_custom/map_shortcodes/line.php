<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/*
* Add-on Name: Line
*/

class WPBakeryShortCode_Line extends WPBakeryShortCode {
}


/**
 * Line.
 */
vc_map(
	array(
		'base'        => 'line',
		'name'        => esc_html__( 'Line', 'noor-assistant' ),
		'weight'      => 980,
		'class'       => 'dima-vc-element dima-vc-element-line',
		'icon'        => 'line',
		'category'    => esc_html__( 'Structure', 'noor-assistant' ),
		'description' => esc_html__( 'Place a horizontal line in your content', 'noor-assistant' ),
		'params'      => array(
			array(
				'param_name'  => 'type',
				'heading'     => esc_html__( 'Line Style', 'noor-assistant' ),
				'description' => esc_html__( 'Choose line style.', 'noor-assistant' ),
				'type'        => 'dropdown',
				'save_always' => true,
				'value'       => array(
					esc_html__( 'Inherit', 'noor-assistant' ) => '',
					esc_html__( 'Double', 'noor-assistant' )  => 'double',
					esc_html__( 'Dashed', 'noor-assistant' )  => 'dashed',
				),
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'id',
				'heading'     => esc_html__( 'ID', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter a unique ID.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'class',
				'heading'     => esc_html__( 'Class', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter a unique class name.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'style',
				'heading'     => esc_html__( 'Style', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter inline CSS.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'tutorials',
				'type'        => 'dima_doc_link_param',
				'heading'     => '<span class="tippy dima-help dima-vc-toolip">' . dima_get_svg_icon( "ic_help" )
				                 . '<span class="dima-vc-tooltip-text">'
				                 . esc_html__( 'Video tutorial and theme documentation article', 'noor-assistant' ) . '</span></span>'
				                 . esc_html__( 'Tutorial', 'noor-assistant' ),
				'doc_link'    => '',
				'video_link'  => 'https://youtu.be/j27nhLT_njc?t=165',
				'doc_example' => 'https://noor.pixeldima.com/separator/',
			),
		)
	)
);
