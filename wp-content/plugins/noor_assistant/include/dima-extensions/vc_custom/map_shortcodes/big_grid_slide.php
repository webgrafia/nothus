<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/*
* Add-on Name: alert
*/

class WPBakeryShortCode_Big_Grid_Slide extends WPBakeryShortCode {
}

/**
 * big grid slide
 */
vc_map(
	array(
		'base'        => 'big-grid-slide',
		'name'        => esc_html__( 'Big Grid Slide', 'noor-assistant' ),
		'weight'      => 490,
		'class'       => 'dima-vc-element dima-vc-element-recent-posts',
		'icon'        => 'blog',
		'category'    => esc_html__( 'Content', 'noor-assistant' ),
		'description' => esc_html__( 'Display your blog posts', 'noor-assistant' ),
		'params'      => array(
			array(
				'param_name'  => 'column',
				'heading'     => esc_html__( 'Column', 'noor-assistant' ),
				'description' => esc_html__( 'Select columns number', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'dropdown',
				'holder'      => 'div',
				'std'         => '1',
				'value'       => array(
					esc_html__( '- Select Option -', 'noor-assistant' ) => '1',
					esc_html__( 'One', 'noor-assistant' )               => '1',
					esc_html__( 'Two', 'noor-assistant' )               => '2',
					esc_html__( 'Three', 'noor-assistant' )             => '3',
					esc_html__( 'Four', 'noor-assistant' )              => '4',
					esc_html__( 'Five', 'noor-assistant' )              => '5',
					esc_html__( 'Six', 'noor-assistant' )               => '6'
				)
			),
			array(
				'param_name'  => 'count',
				'heading'     => esc_html__( 'Post Count', 'noor-assistant' ),
				'description' => esc_html__( 'Type how many posts to display.', 'noor-assistant' ),
				'type'        => 'textfield',
				'holder'      => 'div',
			),
			array(
				'param_name'  => 'category',
				'heading'     => esc_html__( 'Category', 'noor-assistant' ),
				'description' => esc_html__( 'To filter your posts by category, enter the slug of your desired category. To filter by multiple categories, enter your slugs separated by a comma.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'tag',
				'heading'     => esc_html__( 'Tag', 'noor-assistant' ),
				'description' => esc_html__( 'To filter your posts by tag, enter the slug of your desired tag. To filter by multiple tags, enter your slugs separated by a comma.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'offset',
				'heading'     => esc_html__( 'Offset', 'noor-assistant' ),
				'description' => esc_html__( 'Accepts a numerical value to show how many posts to skip.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'order',
				'heading'     => esc_html__( 'Sort by', 'noor-assistant' ),
				'description' => esc_html__( 'Choose what posts you want to display first', 'noor-assistant' ),
				'type'        => 'dropdown',
				'holder'      => 'div',
				'value'       => array(
					esc_html__( '- Select Option -', 'noor-assistant' ) => '',
					esc_html__( 'Popular', 'noor-assistant' )           => 'popular',
					esc_html__( 'Top view', 'noor-assistant' )          => 'top-view',
				)
			),
			array(
				'param_name'  => 'animation',
				'group'       => esc_html__( 'Slide Options', 'noor-assistant' ),
				'heading'     => esc_html__( 'Animation', 'noor-assistant' ),
				'description' => esc_html__( 'Controls the animation type, "fade" or "slide"', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'dropdown',
				'dependency'  => array(
					'element' => 'blog_style',
					'value'   => 'slide_one',
				),
				'value'       => array(
					esc_html__( '- Select Option -', 'noor-assistant' ) => '',
					esc_html__( 'Fade', 'noor-assistant' )              => 'fade',
					esc_html__( 'Slide', 'noor-assistant' )             => 'slide',
				),
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'dark',
				'group'       => esc_html__( 'Slide Options', 'noor-assistant' ),
				'heading'     => esc_html__( 'Dark Background', 'noor-assistant' ),
				'description' => esc_html__( 'Check if you will work in dark background section (This will affect elements style)', 'noor-assistant' ),
				'type'        => 'checkbox',
				'holder'      => 'div',
				'value'       => array(
					'' => 'true'
				),
			),

			array(
				'param_name'  => 'navigation',
				'group'       => esc_html__( 'Slide Options', 'noor-assistant' ),
				'heading'     => esc_html__( 'Slider Arrows', 'noor-assistant' ),
				'description' => esc_html__( 'Check to display navigation arrows', 'noor-assistant' ),
				'type'        => 'checkbox',
				'holder'      => 'div',
				'value'       => array(
					'' => 'true'
				)
			),
			array(
				'param_name'  => 'dots',
				'group'       => esc_html__( 'Slide Options', 'noor-assistant' ),
				'heading'     => esc_html__( 'Dots', 'noor-assistant' ),
				'description' => esc_html__( 'Check to display slider Dots', 'noor-assistant' ),
				'type'        => 'checkbox',
				'holder'      => 'div',
				'std'         => 'true',
				'value'       => array(
					'' => 'true'
				)
			),

			array(
				'param_name'  => 'loop',
				'heading'     => esc_html__( 'Loop', 'noor-assistant' ),
				'group'       => esc_html__( 'Slide Options', 'noor-assistant' ),
				'description' => esc_html__( 'Check to activate slider loop', 'noor-assistant' ),
				'type'        => 'checkbox',
				'std'         => 'true',
				'holder'      => 'div',
				'value'       => array(
					'' => 'true'
				)
			),
			array(
				'param_name'  => 'auto_play',
				'heading'     => esc_html__( 'Autoplay', 'noor-assistant' ),
				'group'       => esc_html__( 'Slide Options', 'noor-assistant' ),
				'description' => esc_html__( 'Check to animate the slides automatically', 'noor-assistant' ),
				'type'        => 'checkbox',
				'holder'      => 'div',
				'value'       => array(
					'' => 'true'
				)
			),

			array(
				'param_name'  => 'cover_color',
				'group'       => esc_html__( 'Slide Options', 'noor-assistant' ),
				'heading'     => esc_html__( 'Cover Background Color', 'noor-assistant' ),
				'description' => esc_html__( 'Specify an overlay color for the background.', 'noor-assistant' ),
				'type'        => 'colorpicker',
				'value'       => 'rgba(51,51,51,0.5)',
				'holder'      => 'div'
			),

			//*Responsive*//
			/*
			 *'dependency'  => array(
					'element' => 'is_responsive',
					'value'   => 'true',
				),
			*/

			array(
				'param_name'  => 'is_responsive',
				'group'       => esc_html__( 'Height', 'noor-assistant' ),
				'heading'     => esc_html__( 'Is Responsive', 'noor-assistant' ),
				'description' => '',
				'type'        => 'checkbox',
				'holder'      => 'div',
				'value'       => array(
					'' => 'true'
				)
			),

			array(
				'type'             => 'dima_heading_vc',
				'group'            => esc_html__( 'Height', 'noor-assistant' ),
				'text'             => esc_html__( 'Height', 'noor-assistant' ),
				'param_name'       => 'sizing',
				'dependency'       => array(
					'element' => 'is_responsive',
					'value'   => 'true',
				),
				'edit_field_class' => 'dima-heading-param-wrapper no-top-margin vc_column vc_col-sm-12',
			),
			array(
				'type'             => 'number',
				'group'            => esc_html__( 'Height', 'noor-assistant' ),
				'heading'          => esc_html__( 'Default height (px)', 'noor-assistant' ),
				'param_name'       => 'screen_all_spacer_size',
				'value'            => 700,
				'admin_label'      => true,
				'dependency'       => array(
					'element' => 'is_responsive',
					'value'   => 'true',
				),
				'edit_field_class' => 'no-border-bottom vc_column vc_col-sm-6 crum_vc '
			),

			/*Extra Large Devices*/
			array(
				'type'             => 'dima_heading_vc',
				'group'            => esc_html__( 'Height', 'noor-assistant' ),
				'text'             => esc_html__( 'Extra Large Devices', 'noor-assistant' ),
				'param_name'       => 'sizing_xld',
				'dependency'       => array(
					'element' => 'is_responsive',
					'value'   => 'true',
				),
				'edit_field_class' => 'dima-heading-param-wrapper vc_column vc_col-sm-12',
			),
			array(
				'type'             => 'number',
				'group'            => esc_html__( 'Height', 'noor-assistant' ),
				'heading'          => esc_html__( 'Screen resolution (px)', 'noor-assistant' ),
				'param_name'       => 'screen_xld_resolution',
				'value'            => 1600,
				'dependency'       => array(
					'element' => 'is_responsive',
					'value'   => 'true',
				),
				'edit_field_class' => 'no-border-bottom vc_column vc_col-sm-6 crum_vc dima-number-wrap'
			),
			array(
				'type'             => 'number',
				'group'            => esc_html__( 'Height', 'noor-assistant' ),
				'heading'          => esc_html__( 'Height (px)', 'noor-assistant' ),
				'admin_label'      => true,
				'value'            => 700,
				'dependency'       => array(
					'element' => 'is_responsive',
					'value'   => 'true',
				),
				'param_name'       => 'screen_xld_spacer_size',
				'edit_field_class' => 'no-border-bottom vc_column vc_col-sm-6 crum_vc '
			),

			/* Large Devices*/
			array(
				'type'             => 'dima_heading_vc',
				'group'            => esc_html__( 'Height', 'noor-assistant' ),
				'text'             => esc_html__( 'Large Devices', 'noor-assistant' ),
				'param_name'       => 'sizing_ld',
				'edit_field_class' => 'dima-heading-param-wrapper vc_column vc_col-sm-12',
			),
			array(
				'type'             => 'number',
				'group'            => esc_html__( 'Height', 'noor-assistant' ),
				'heading'          => esc_html__( 'Screen resolution (px)', 'noor-assistant' ),
				'param_name'       => 'screen_ld_resolution',
				'value'            => 1400,
				'dependency'       => array(
					'element' => 'is_responsive',
					'value'   => 'true',
				),
				'edit_field_class' => 'no-border-bottom vc_column vc_col-sm-6 crum_vc dima-number-wrap'
			),
			array(
				'type'             => 'number',
				'group'            => esc_html__( 'Height', 'noor-assistant' ),
				'heading'          => esc_html__( 'Height (px)', 'noor-assistant' ),
				'admin_label'      => true,
				'value'            => 700,
				'dependency'       => array(
					'element' => 'is_responsive',
					'value'   => 'true',
				),
				'param_name'       => 'screen_ld_spacer_size',
				'edit_field_class' => 'no-border-bottom vc_column vc_col-sm-6 crum_vc '
			),

			/* Medium Devise*/
			array(
				'type'             => 'dima_heading_vc',
				'group'            => esc_html__( 'Height', 'noor-assistant' ),
				'text'             => esc_html__( 'Medium Devise', 'noor-assistant' ),
				'param_name'       => 'sizing_md',
				'edit_field_class' => 'dima-heading-param-wrapper vc_column vc_col-sm-12',
			),
			array(
				'type'             => 'number',
				'group'            => esc_html__( 'Height', 'noor-assistant' ),
				'heading'          => esc_html__( 'Screen resolution (px)', 'noor-assistant' ),
				'param_name'       => 'screen_md_resolution',
				'value'            => 1170,
				'dependency'       => array(
					'element' => 'is_responsive',
					'value'   => 'true',
				),
				'edit_field_class' => 'no-border-bottom vc_column vc_col-sm-6 crum_vc dima-number-wrap'
			),
			array(
				'type'             => 'number',
				'group'            => esc_html__( 'Height', 'noor-assistant' ),
				'heading'          => esc_html__( 'Height (px)', 'noor-assistant' ),
				'admin_label'      => true,
				'value'            => 700,
				'dependency'       => array(
					'element' => 'is_responsive',
					'value'   => 'true',
				),
				'param_name'       => 'screen_md_spacer_size',
				'edit_field_class' => 'no-border-bottom vc_column vc_col-sm-6 crum_vc '
			),

			/*Small Devices Tablets*/
			array(
				'type'             => 'dima_heading_vc',
				'group'            => esc_html__( 'Height', 'noor-assistant' ),
				'text'             => esc_html__( 'Small Devices ( Tablets )', 'noor-assistant' ),
				'param_name'       => 'sizing_sd',
				'edit_field_class' => 'dima-heading-param-wrapper vc_column vc_col-sm-12',
			),
			array(
				'type'             => 'number',
				'group'            => esc_html__( 'Height', 'noor-assistant' ),
				'heading'          => esc_html__( 'Screen resolution (px)', 'noor-assistant' ),
				'param_name'       => 'screen_sd_resolution',
				'value'            => 969,
				'dependency'       => array(
					'element' => 'is_responsive',
					'value'   => 'true',
				),
				'edit_field_class' => 'no-border-bottom vc_column vc_col-sm-6 crum_vc dima-number-wrap'
			),
			array(
				'type'             => 'number',
				'group'            => esc_html__( 'Height', 'noor-assistant' ),
				'heading'          => esc_html__( 'Height (px)', 'noor-assistant' ),
				'admin_label'      => true,
				'value'            => 500,
				'dependency'       => array(
					'element' => 'is_responsive',
					'value'   => 'true',
				),
				'param_name'       => 'screen_sd_spacer_size',
				'edit_field_class' => 'no-border-bottom vc_column vc_col-sm-6 crum_vc '
			),

			/*Extra Small Devices ( Mobile phones )*/
			array(
				'type'             => 'dima_heading_vc',
				'group'            => esc_html__( 'Height', 'noor-assistant' ),
				'text'             => esc_html__( 'Small Devices ( Mobile phones )', 'noor-assistant' ),
				'param_name'       => 'sizing_xsd',
				'edit_field_class' => 'dima-heading-param-wrapper vc_column vc_col-sm-12',
			),
			array(
				'type'             => 'number',
				'group'            => esc_html__( 'Height', 'noor-assistant' ),
				'heading'          => esc_html__( 'Screen resolution (px)', 'noor-assistant' ),
				'param_name'       => 'screen_xsd_resolution',
				'value'            => 480,
				'dependency'       => array(
					'element' => 'is_responsive',
					'value'   => 'true',
				),
				'edit_field_class' => 'no-border-bottom vc_column vc_col-sm-6 crum_vc dima-number-wrap'
			),
			array(
				'type'             => 'number',
				'group'            => esc_html__( 'Height', 'noor-assistant' ),
				'heading'          => esc_html__( 'Height (px)', 'noor-assistant' ),
				'admin_label'      => true,
				'value'            => 400,
				'dependency'       => array(
					'element' => 'is_responsive',
					'value'   => 'true',
				),
				'param_name'       => 'screen_xsd_spacer_size',
				'edit_field_class' => 'no-border-bottom vc_column vc_col-sm-6 crum_vc '
			),


		)
	)
);