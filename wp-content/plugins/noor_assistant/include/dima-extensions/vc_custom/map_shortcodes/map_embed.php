<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/*
* Add-on Name: Google Map Embed 
*/

class WPBakeryShortCode_Map extends WPBakeryShortCode {
}

/**
 * Google Map Embed
 */
vc_map(
	array(
		'base'        => 'map',
		'name'        => esc_html__( 'Map', 'noor-assistant' ),
		'weight'      => 530,
		'class'       => 'dima-vc-element dima-vc-element-map',
		'icon'        => 'map',
		'category'    => esc_html__( 'Media', 'noor-assistant' ),
		'description' => esc_html__( 'Embed a map from a third-party provider', 'noor-assistant' ),
		'params'      => array(
			array(
				'param_name'  => 'content',
				'heading'     => esc_html__( 'Code', 'noor-assistant' ),
				'description' => esc_html__( 'Switch to the "text" editor and do not place anything else here other than your &lsaquo;iframe&rsaquo; or &lsaquo;embed&rsaquo; code.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textarea_html',
				'holder'      => 'div',
				'value'       => ''
			),
			array(
				'param_name'  => 'id',
				'heading'     => esc_html__( 'ID', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter a unique ID.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'class',
				'heading'     => esc_html__( 'Class', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter a unique class name.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'style',
				'heading'     => esc_html__( 'Style', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter inline CSS.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'tutorials',
				'type'        => 'dima_doc_link_param',
				'heading'     => '<span class="tippy dima-help dima-vc-toolip">' . dima_get_svg_icon( "ic_help" )
				                 . '<span class="dima-vc-tooltip-text">'
				                 . esc_html__( 'Video tutorial and theme documentation article', 'noor-assistant' ) . '</span></span>'
				                 . esc_html__( 'Tutorial', 'noor-assistant' ),
				'doc_link'    => '',
				'video_link'  => 'https://www.youtube.com/watch?v=iHVimogrwAA',
				'doc_example' => 'https://noor.pixeldima.com/google-map/',
			),
		)
	)
);