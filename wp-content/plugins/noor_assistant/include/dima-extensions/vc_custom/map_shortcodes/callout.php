<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/*
* Add-on Name: CallOut
*/

class WPBakeryShortCode_Callout extends WPBakeryShortCodesContainer {
}

/**
 * CallOut
 */
vc_map(
	array(
		'base'            => 'callout',
		'name'            => esc_html__( 'Callout', 'noor-assistant' ),
		'weight'          => 710,
		'class'           => 'dima-vc-element dima-vc-element-callout',
		'icon'            => 'callout',
		'category'        => esc_html__( 'Marketing', 'noor-assistant' ),
		'as_parent'       => array( 'only' => 'button,clear' ),
		'content_element' => true,
		'js_view'         => 'VcColumnView',
		'description'     => esc_html__( 'Include a callout into your content', 'noor-assistant' ),
		'params'          => array(
			array(
				'param_name'  => 'type',
				'heading'     => esc_html__( 'Callout Type', 'noor-assistant' ),
				'description' => esc_html__( 'Select the callout type.', 'noor-assistant' ),
				'type'        => 'dropdown',
				'holder'      => 'div',
				'value'       => array(
					esc_html__( '- Select Option -', 'noor-assistant' )    => '',
					esc_html__( 'Button on the bottom', 'noor-assistant' ) => 'style_one',
					esc_html__( 'Button on the side', 'noor-assistant' )   => 'style_two',
					esc_html__( 'No button', 'noor-assistant' )            => 'style_three',
				)
			),
			array(
				'param_name'  => 'direction',
				'heading'     => esc_html__( 'Contents Alignment', 'noor-assistant' ),
				'description' => esc_html__( 'Select the alignment for your callout contents.', 'noor-assistant' ),
				'type'        => 'dropdown',
				'holder'      => 'div',
				'value'       => array(
					esc_html__( '- Select Option -', 'noor-assistant' ) => '',
					esc_html__( 'Start', 'noor-assistant' )             => 'start',
					esc_html__( 'Center', 'noor-assistant' )            => 'center',
					esc_html__( 'End', 'noor-assistant' )               => 'end'
				)
			),
			array(
				'param_name'  => 'title',
				'heading'     => esc_html__( 'Callout Title', 'noor-assistant' ),
				'description' => esc_html__( 'Enter the title for your callout.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'message',
				'heading'     => esc_html__( 'Callout text', 'noor-assistant' ),
				'description' => esc_html__( 'Enter the text for your callout.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textarea',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'title_color',
				'group'       => esc_html__( 'Style', 'noor-assistant' ),
				'heading'     => esc_html__( 'Callout Title Color', 'noor-assistant' ),
				'description' => esc_html__( 'Choose the title color for your callout', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'colorpicker',
				'holder'      => 'div'
			),

			array(
				'param_name'  => 'text_color',
				'group'       => esc_html__( 'Style', 'noor-assistant' ),
				'heading'     => esc_html__( 'Callout Text color', 'noor-assistant' ),
				'description' => esc_html__( 'Choose the text color for your callout', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'colorpicker',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'bg_image',
				'group'       => esc_html__( 'Style', 'noor-assistant' ),
				'heading'     => esc_html__( 'Callout Background Image', 'noor-assistant' ),
				'description' => esc_html__( 'Add a background image to your callout', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'attach_image',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'parallax',
				'group'       => esc_html__( 'Style', 'noor-assistant' ),
				'heading'     => esc_html__( 'Parallax', 'noor-assistant' ),
				'description' => esc_html__( 'Select to activate the parallax effect (Applied only for background patterns and images)', 'noor-assistant' ),
				'type'        => 'dropdown',
				'holder'      => 'div',
				'value'       => array(
					esc_html__( '- Select Option -', 'noor-assistant' ) => '',
					esc_html__( 'Parallax', 'noor-assistant' )          => 'parallax',
					esc_html__( 'Fixed Parallax', 'noor-assistant' )    => 'fixed_parallax',
				)
			),
			//---------------
			array(
				'param_name'  => 'cover',
				'group'       => esc_html__( 'Style', 'noor-assistant' ),
				'heading'     => esc_html__( 'Cover', 'noor-assistant' ),
				'description' => esc_html__( 'Select to activate the cover above the background patterns and images.', 'noor-assistant' ),
				'type'        => 'toggle',
				'holder'      => 'div',
				'value'       => 'false'
			),

			array(
				"type"        => "dropdown",
				"heading"     => __( "Gradient Overlay Orientation", "noor-assistant" ),
				'group'       => esc_html__( 'Style', 'noor-assistant' ),
				"param_name"  => "bg_gradient",
				"width"       => 150,
				"value"       => array(
					__( '-- No Gradient --', "noor-assistant" ) => "false",
					__( 'Vertical ↓', "noor-assistant" )        => "vertical",
					__( 'Horizontal →', "noor-assistant" )      => "horizontal",
					__( 'Diagonal ↘', "noor-assistant" )        => "left_top",
					__( 'Diagonal ↗', "noor-assistant" )        => "left_bottom",
					__( 'Radial ○', "noor-assistant" )          => "radial",
				),
				"description" => __( "Choose the orientation of gradient overlay", "noor-assistant" ),
				'dependency'  => array(
					'element' => 'cover',
					'value'   => 'true',
				),
			),
			array(
				"type"             => "colorpicker",
				"heading"          => __( "Overlay Color Start", "noor-assistant" ),
				'group'            => esc_html__( 'Style', 'noor-assistant' ),
				"param_name"       => "cover_color",
				"value"            => "",
				'dependency'       => array(
					'element' => 'cover',
					'value'   => 'true',
				),
				"description"      => __( "Primary overlay color. Start color if used with gradient option selected.", "noor-assistant" ),
				'edit_field_class' => 'no-border-bottom vc_column vc_col-sm-6'
			),
			array(
				"type"             => "colorpicker",
				"heading"          => __( "Overlay Color End", "noor-assistant" ),
				'group'            => esc_html__( 'Style', 'noor-assistant' ),
				"param_name"       => "gr_end",
				"value"            => "",
				"description"      => __( "The ending color for gradient fill overlay. Use only with gradient option selected.", "noor-assistant" ),
				"dependency"       => array(
					'element' => "bg_gradient",
					'value'   => array(
						"vertical",
						"horizontal",
						"left_top",
						"left_bottom",
						"radial"
					)
				),
				'edit_field_class' => 'no-border-bottom vc_column vc_col-sm-6'
			),
			array(
				"type"        => "range",
				"heading"     => __( "Overlay Color Mask Opacity", "noor-assistant" ),
				'group'       => esc_html__( 'Style', 'noor-assistant' ),
				"param_name"  => "gr_opacity",
				"value"       => "0.6",
				"min"         => "0",
				"max"         => "1",
				"step"        => "0.1",
				"unit"        => 'alpha',
				"dependency"  => array(
					'element' => "bg_gradient",
					'value'   => array(
						"vertical",
						"horizontal",
						"left_top",
						"left_bottom",
						"radial"
					)
				),
				"description" => __( "The opacity of overlay layer which you set above. ", "noor-assistant" )
			),
			//---------------
			array(
				'param_name'  => 'no_border',
				'group'       => esc_html__( 'Style', 'noor-assistant' ),
				'heading'     => esc_html__( 'Remove Border', 'noor-assistant' ),
				'description' => esc_html__( 'Check to remove the border of the callout', 'noor-assistant' ),
				'type'        => 'checkbox',
				'holder'      => 'div',
				'value'       => array( '' => 'true' )
			),
			array(
				'param_name'  => 'dima_box_shadow',
				'group'       => esc_html__( 'Style', 'noor-assistant' ),
				'heading'     => esc_html__( 'Drop Shadow', 'noor-assistant' ),
				'description' => esc_html__( 'Check to add drop shadow effect to the callout', 'noor-assistant' ),
				'type'        => 'checkbox',
				'holder'      => 'div',
				'value'       => array( '' => 'true' )
			),
			array(
				'param_name'  => 'full',
				'group'       => esc_html__( 'Style', 'noor-assistant' ),
				'heading'     => esc_html__( 'Full-Width', 'noor-assistant' ),
				'description' => esc_html__( 'Check to make the callout full-width', 'noor-assistant' ),
				'type'        => 'checkbox',
				'holder'      => 'div',
				'value'       => array( '' => 'true' )
			),
			array(
				'param_name'  => 'href',
				'heading'     => esc_html__( 'URL Link', 'noor-assistant' ),
				'description' => esc_html__( 'Enter in the URL you want your callout button to link to.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'target',
				'heading'     => esc_html__( 'Open link in a new tab', 'noor-assistant' ),
				'description' => esc_html__( 'Check to open your callout link button in a new tab.', 'noor-assistant' ),
				'type'        => 'checkbox',
				'holder'      => 'div',
				'value'       => array(
					'' => 'blank'
				)
			),
			array(
				'param_name'  => 'id',
				'heading'     => esc_html__( 'ID', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter a unique ID.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'class',
				'heading'     => esc_html__( 'Class', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter a unique class name.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'style',
				'heading'     => esc_html__( 'Style', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter inline CSS.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			//**************Animation***************//
			array(
				'param_name'  => 'animation',
				'group'       => esc_html__( 'Animation', 'noor-assistant' ),
				'heading'     => esc_html__( 'Animation', 'noor-assistant' ),
				'description' => esc_html__( 'Select the type of animation you want to use.', 'noor-assistant' ),
				'type'        => 'dropdown',
				'holder'      => 'div',
				'value'       => $dima_vc->dima_animate_list_velocity,
			),

			array(
				'param_name'  => 'delay',
				'group'       => esc_html__( 'Animation', 'noor-assistant' ),
				'heading'     => esc_html__( 'Delay Animation', 'noor-assistant' ),
				'description' => esc_html__( 'Set when the animation start (milliseconds)', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div',
				'value'       => ''
			),

			array(
				'param_name'  => 'delay_duration',
				'group'       => esc_html__( 'Animation', 'noor-assistant' ),
				'heading'     => esc_html__( 'duration Animation', 'noor-assistant' ),
				'description' => esc_html__( 'Animation duration of each element  (milliseconds).', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div',
				'value'       => ''
			),

			array(
				'param_name'  => 'delay_offset',
				'group'       => esc_html__( 'Animation', 'noor-assistant' ),
				'heading'     => esc_html__( 'Delay Animation Offset', 'noor-assistant' ),
				'description' => esc_html__( 'Set the number ( px,%..) from the top of the viewport where the animation start.', 'noor-assistant' ) . "<a href='http://imakewebthings.com/waypoints/api/offset-option/' target='_blank' >" . esc_html__( 'Read More', 'noor-assistant' ) . "</a>",
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div',
				'value'       => ''
			),
			//**************!Animation***************//
			array(
				'param_name'  => 'tutorials',
				'type'        => 'dima_doc_link_param',
				'heading'     => '<span class="tippy dima-help dima-vc-toolip">' . dima_get_svg_icon( "ic_help" )
				                 . '<span class="dima-vc-tooltip-text">'
				                 . esc_html__( 'Video tutorial and theme documentation article', 'noor-assistant' ) . '</span></span>'
				                 . esc_html__( 'Tutorial', 'noor-assistant' ),
				'doc_link'    => '',
				'video_link'  => 'https://www.youtube.com/watch?v=XF12O6thuDM',
				'doc_example' => 'https://noor.pixeldima.com/call-to-action/',
			),
		)
	)
);