<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/*
* Add-on Name: Line
*/

class WPBakeryShortCode_Button extends WPBakeryShortCode {
}

/**
 * Buttons
 */
vc_map(
	array(
		'base'        => 'button',
		'name'        => esc_html__( 'Button', 'noor-assistant' ),
		'weight'      => 720,
		'class'       => 'dima-vc-element dima-vc-element-button',
		'icon'        => 'dima-button',
		'category'    => esc_html__( 'Marketing', 'noor-assistant' ),
		'description' => esc_html__( 'Add a clickable button to your content', 'noor-assistant' ),
		'params'      => array(
			array(
				'param_name'  => 'text',
				'heading'     => esc_html__( 'Button Text', 'noor-assistant' ),
				'description' => esc_html__( 'Enter your button text.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div',
				'value'       => 'Enter your text'
			),
			array(
				'param_name'  => 'type',
				'group'       => esc_html__( 'Style', 'noor-assistant' ),
				'heading'     => esc_html__( 'Button Style', 'noor-assistant' ),
				'description' => esc_html__( 'Select the button style.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'dropdown',
				'value'       => array(
					esc_html__( '- Select Option -', 'noor-assistant' ) => '',
					'Fill'                                              => 'fill',
					'Outline'                                           => 'stroke',
				),
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'size',
				'heading'     => esc_html__( 'Button Size', 'noor-assistant' ),
				'description' => esc_html__( 'Select the button size.', 'noor-assistant' ),
				'type'        => 'dropdown',
				'holder'      => 'div',
				'value'       => array(
					esc_html__( '- Select Option -', 'noor-assistant' ) => '',
					esc_html__( 'Mini', 'noor-assistant' )              => 'mini',
					esc_html__( 'Small', 'noor-assistant' )             => 'small',
					esc_html__( 'Large', 'noor-assistant' )             => 'large',
					esc_html__( 'Big', 'noor-assistant' )               => 'big',
					esc_html__( 'Huge', 'noor-assistant' )              => 'huge',
				)
			),
			array(
				'param_name'  => 'shape',
				'group'       => esc_html__( 'Style', 'noor-assistant' ),
				'heading'     => esc_html__( 'Button Shape', 'noor-assistant' ),
				'description' => esc_html__( 'Select the button shape.', 'noor-assistant' ),
				'type'        => 'dropdown',
				'holder'      => 'div',
				'value'       => array(
					esc_html__( '- Select Option -', 'noor-assistant' ) => '',
					esc_html__( 'Square', 'noor-assistant' )            => 'dima-btn-no-rounded',
					esc_html__( 'Radius', 'noor-assistant' )            => 'dima-btn-rounded',
					esc_html__( 'Pill', 'noor-assistant' )              => 'dima-btn-pill'
				)
			),
			array(
				'param_name'  => 'color_class',
				'group'       => esc_html__( 'Style', 'noor-assistant' ),
				'heading'     => esc_html__( 'Button Color', 'noor-assistant' ),
				'description' => esc_html__( 'Choose button color', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'dropdown',
				'value'       => array(
					esc_html__( '- Select Option -', 'noor-assistant' ) => '',
					esc_html__( 'Inherit', 'noor-assistant' )           => '',
					esc_html__( 'Header', 'noor-assistant' )            => 'di_header',
					esc_html__( 'White', 'noor-assistant' )             => 'di_white',
					esc_html__( 'Green', 'noor-assistant' )             => 'di_green',
					esc_html__( 'Blue', 'noor-assistant' )              => 'di_blue',
					esc_html__( 'Yellow', 'noor-assistant' )            => 'di_yellow',
					esc_html__( 'Orange', 'noor-assistant' )            => 'di_orange',
					esc_html__( 'Red', 'noor-assistant' )               => 'di_red',
					esc_html__( 'Purple', 'noor-assistant' )            => 'di_purple',
				),
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'dima_vc_add_shadow',
				'group'       => esc_html__( 'Style', 'noor-assistant' ),
				'heading'     => esc_html__( 'Add Shadow', 'noor-assistant' ),
				'description' => esc_html__( 'Add drop shadow effect to the button', 'noor-assistant' ),
				'type'        => 'checkbox',
				'holder'      => 'div',
				'value'       => array(
					'' => 'true'
				)
			),
			array(
				'param_name'  => 'float',
				'heading'     => esc_html__( 'Button Float', 'noor-assistant' ),
				'description' => esc_html__( 'Optionally float the button.', 'noor-assistant' ),
				'type'        => 'dropdown',
				'holder'      => 'div',
				'value'       => array(
					esc_html__( '- Select Option -', 'noor-assistant' ) => '',
					esc_html__( 'None', 'noor-assistant' )              => 'none',
					esc_html__( 'Start', 'noor-assistant' )             => 'start',
					esc_html__( 'Center', 'noor-assistant' )            => 'center',
					esc_html__( 'End', 'noor-assistant' )               => 'end'
				)
			),
			array(
				'param_name'  => 'href',
				'heading'     => esc_html__( 'URL Link', 'noor-assistant' ),
				'description' => esc_html__( 'Enter in the URL you want your button to link to.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'target',
				'heading'     => esc_html__( 'Open link in a new tab', 'noor-assistant' ),
				'description' => esc_html__( 'Check to open your button link in a new tab.', 'noor-assistant' ),
				'type'        => 'checkbox',
				'holder'      => 'div',
				'value'       => array(
					'' => 'blank'
				)
			),

			array(
				'param_name'  => 'block',
				'heading'     => esc_html__( 'Block', 'noor-assistant' ),
				'description' => esc_html__( 'Check to make the button occupy the column width.', 'noor-assistant' ),
				'type'        => 'checkbox',
				'holder'      => 'div',
				'value'       => array(
					'' => 'true'
				)
			),
			array(
				'param_name'  => 'dima_vc_disabled',
				'heading'     => esc_html__( 'Disabled Button', 'noor-assistant' ),
				'description' => esc_html__( 'Check to activate disabled button (Applied only for button fill style.)', 'noor-assistant' ),
				'type'        => 'checkbox',
				'holder'      => 'div',
				'dependency'  => array(
					'element' => 'type',
					'value'   => 'fill',
				),
				'value'       => array(
					'' => 'true'
				)
			),
			array(
				'type'        => 'dropdown',
				'heading'     => esc_html__( 'Icons library', 'noor-assistant' ),
				'value'       => array(
					esc_html__( 'Font Awesome', 'noor-assistant' )                                     => 'fontawesome',
					esc_html__( 'Custom Font', 'noor-assistant' )                                      => 'customfont',
					esc_html__( 'SVG Icons (Stroke Gap Icons And Material Icons) ', 'noor-assistant' ) => 'svgicon',
				),
				'param_name'  => 'icon_type',
				'description' => esc_html__( 'Select icons library.', 'noor-assistant' ),
			),
			array(
				'param_name'  => 'icon',
				'heading'     => esc_html__( 'Icon Class', 'noor-assistant' ),
				'description' => esc_html__( 'Enter Icon Class from Font Awesome or Simple Line Icons.', 'noor-assistant' ),
				'value'       => 'simple-icon-user',
				'dependency'  => array(
					'element' => 'icon_type',
					'value'   => 'customfont',
				),
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'icon_svg',
				'heading'     => esc_html__( 'Icon SVG', 'noor-assistant' ),
				'description' => esc_html__( 'Enter SVG Icon name', 'noor-assistant' ),
				'value'       => '',
				'dependency'  => array(
					'element' => 'icon_type',
					'value'   => 'svgicon',
				),
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'type'        => 'iconpicker',
				'heading'     => esc_html__( 'Icon', 'noor-assistant' ),
				'param_name'  => 'icon_fontawesome',
				'value'       => '', // default value to backend editor admin_label
				'settings'    => array(
					'emptyIcon'    => true,
					// default true, display an "EMPTY" icon?
					'value'        => 'fontawesome',
					'iconsPerPage' => 4000,
					// default 100, how many icons per/page to display, we use (big number) to display all icons in single page
				),
				'dependency'  => array(
					'element' => 'icon_type',
					'value'   => 'fontawesome',
				),
				'description' => esc_html__( 'Select icon from library.', 'noor-assistant' ),
			),
			array(
				'param_name'  => 'lightbox',
				'heading'     => esc_html__( 'Lightbox Type', 'noor-assistant' ),
				'description' => esc_html__( 'Select your lightbox type (iframe: for youtube, map,..)', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'dropdown',
				'value'       => array(
					esc_html__( '- Select Option -', 'noor-assistant' ) => '',
					'None'                                              => '',
					'image'                                             => 'image',
					'iframe'                                            => 'iframe',
					'ajax'                                              => 'ajax'
				),
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'popup_type',
				'heading'     => esc_html__( 'Popup Info type', 'noor-assistant' ),
				'description' => esc_html__( 'Select to choose the type of popup info.', 'noor-assistant' ),
				'type'        => 'dropdown',
				'holder'      => 'div',
				'value'       => array(
					esc_html__( '- Select Option -', 'noor-assistant' ) => '',
					'None'                                              => '',
					'Popover'                                           => 'popover',
					'Tooltip'                                           => 'tooltip'
				)
			),

			array(
				'param_name'  => 'title',
				'heading'     => esc_html__( 'Popup Info Title', 'noor-assistant' ),
				'description' => esc_html__( 'Enter in the title attribute you want for your button (This will apply as title for popover or tooltip if you have active it from -Popup Info type-).', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),

			array(
				'param_name'  => 'popup_trigger',
				'heading'     => esc_html__( 'Popup Trigger', 'noor-assistant' ),
				'description' => esc_html__( 'Select what actions you want to trigger the popover or tooltip.', 'noor-assistant' ),
				'type'        => 'dropdown',
				'holder'      => 'div',
				'value'       => array(
					esc_html__( '- Select Option -', 'noor-assistant' ) => '',
					'Hover'                                             => 'hover',
					'Click'                                             => 'click',
					'Focus'                                             => 'focus'
				)
			),
			array(
				'param_name'  => 'popup_place',
				'heading'     => esc_html__( 'Popup Position', 'noor-assistant' ),
				'description' => esc_html__( 'Select the popup position.', 'noor-assistant' ),
				'type'        => 'dropdown',
				'holder'      => 'div',
				'value'       => array(
					esc_html__( '- Select Option -', 'noor-assistant' ) => '',
					'Top'                                               => 'top',
					'Left'                                              => 'left',
					'Right'                                             => 'right',
					'Bottom'                                            => 'bottom'
				)
			),
			array(
				'param_name'  => 'popup_content',
				'heading'     => esc_html__( 'Info Content', 'noor-assistant' ),
				'description' => esc_html__( 'Extra content for the popover.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),

			array(
				'param_name'  => 'id',
				'heading'     => esc_html__( 'ID', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter a unique ID.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'class',
				'heading'     => esc_html__( 'Class', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter a unique class name.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'style',
				'heading'     => esc_html__( 'Style', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter inline CSS.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			//**************Animation***************//
			array(
				'param_name'  => 'animation',
				'group'       => esc_html__( 'Animation', 'noor-assistant' ),
				'heading'     => esc_html__( 'Animation', 'noor-assistant' ),
				'description' => esc_html__( 'Select the type of animation you want to use.', 'noor-assistant' ),
				'type'        => 'dropdown',
				'holder'      => 'div',
				'value'       => $dima_vc->dima_animate_list_velocity,
			),

			array(
				'param_name'  => 'delay',
				'group'       => esc_html__( 'Animation', 'noor-assistant' ),
				'heading'     => esc_html__( 'Delay Animation', 'noor-assistant' ),
				'description' => esc_html__( 'Set when the animation start (milliseconds)', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div',
				'value'       => ''
			),

			array(
				'param_name'  => 'delay_duration',
				'group'       => esc_html__( 'Animation', 'noor-assistant' ),
				'heading'     => esc_html__( 'duration Animation', 'noor-assistant' ),
				'description' => esc_html__( 'Animation duration of each element  (milliseconds).', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div',
				'value'       => ''
			),

			array(
				'param_name'  => 'delay_offset',
				'group'       => esc_html__( 'Animation', 'noor-assistant' ),
				'heading'     => esc_html__( 'Delay Animation Offset', 'noor-assistant' ),
				'description' => esc_html__( 'Set the number ( px,%..) from the top of the viewport where the animation start.', 'noor-assistant' ) . "<a href='http://imakewebthings.com/waypoints/api/offset-option/' target='_blank' >" . esc_html__( 'Read More', 'noor-assistant' ) . "</a>",
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div',
				'value'       => ''
			),
			//**************!Animation***************//

			array(
				'param_name'  => 'tutorials',
				'type'        => 'dima_doc_link_param',
				'heading'     => '<span class="tippy dima-help dima-vc-toolip">' . dima_get_svg_icon( "ic_help" )
				                 . '<span class="dima-vc-tooltip-text">'
				                 . esc_html__( 'Video tutorial and theme documentation article', 'noor-assistant' ) . '</span></span>'
				                 . esc_html__( 'Tutorial', 'noor-assistant' ),
				'doc_link'    => '',
				'video_link'  => 'https://www.youtube.com/watch?v=dsJfcrUsG8c',
				'doc_example' => 'https://noor.pixeldima.com/buttons/',
			),
		)
	)
);

