<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/*
* Add-on Name: Spacer
*/

class WPBakeryShortCode_Dima_Spacer extends WPBakeryShortCode {
}

vc_map(
	array(
		'name'        => esc_html__( 'Responsive Spacer', 'noor-assistant' ),
		'base'        => 'dima_spacer',
		'class'       => 'dima-vc-element dima-vc-element-dima_spacer',
		'icon'        => 'clear',
		'category'    => esc_html__( 'Content', 'noor-assistant' ),
		'description' => esc_html__( 'Add the space between the elements', 'noor-assistant' ),
		'params'      => array(
			array(
				'type'             => 'dima_heading_vc',
				'text'             => esc_html__( 'Spacer units', 'noor-assistant' ),
				'param_name'       => 'sizing',
				'edit_field_class' => 'dima-heading-param-wrapper no-top-margin vc_column vc_col-sm-12',
			),
			array(
				'type'             => 'dima_radio_advanced',
				'heading'          => esc_html__( 'Units', 'noor-assistant' ),
				'param_name'       => 'units',
				'value'            => 'px',
				'options'          => array(
					esc_html__( 'Pixel', 'noor-assistant' )   => 'px',
					esc_html__( 'Percent', 'noor-assistant' ) => '%',
				),
				'edit_field_class' => 'no-border-bottom vc_column vc_col-sm-6'
			),
			array(
				'type'             => 'number',
				'heading'          => esc_html__( 'Spacer size (px)', 'noor-assistant' ),
				'param_name'       => 'screen_all_spacer_size',
				'value'            => 150,
				'admin_label'      => true,
				'edit_field_class' => 'no-border-bottom vc_column vc_col-sm-6 crum_vc '
			),

			/*Extra Large Devices*/
			array(
				'type'             => 'dima_heading_vc',
				'text'             => esc_html__( 'Extra Large Devices', 'noor-assistant' ),
				'param_name'       => 'sizing_xld',
				'edit_field_class' => 'dima-heading-param-wrapper vc_column vc_col-sm-12',
			),
			array(
				'type'             => 'number',
				'heading'          => esc_html__( 'Screen resolution (px)', 'noor-assistant' ),
				'param_name'       => 'screen_xld_resolution',
				'value'            => 1600,
				'edit_field_class' => 'no-border-bottom vc_column vc_col-sm-6 crum_vc dima-number-wrap'
			),
			array(
				'type'             => 'number',
				'heading'          => esc_html__( 'Spacer size', 'noor-assistant' ),
				'admin_label'      => true,
				'value'            => 150,
				'param_name'       => 'screen_xld_spacer_size',
				'edit_field_class' => 'no-border-bottom vc_column vc_col-sm-6 crum_vc '
			),

			/* Large Devices*/
			array(
				'type'             => 'dima_heading_vc',
				'text'             => esc_html__( 'Large Devices', 'noor-assistant' ),
				'param_name'       => 'sizing_ld',
				'edit_field_class' => 'dima-heading-param-wrapper vc_column vc_col-sm-12',
			),
			array(
				'type'             => 'number',
				'heading'          => esc_html__( 'Screen resolution (px)', 'noor-assistant' ),
				'param_name'       => 'screen_ld_resolution',
				'value'            => 1400,
				'edit_field_class' => 'no-border-bottom vc_column vc_col-sm-6 crum_vc dima-number-wrap'
			),
			array(
				'type'             => 'number',
				'heading'          => esc_html__( 'Spacer size', 'noor-assistant' ),
				'admin_label'      => true,
				'value'            => 150,
				'param_name'       => 'screen_ld_spacer_size',
				'edit_field_class' => 'no-border-bottom vc_column vc_col-sm-6 crum_vc '
			),

			/* Medium Devise*/
			array(
				'type'             => 'dima_heading_vc',
				'text'             => esc_html__( 'Medium Devise', 'noor-assistant' ),
				'param_name'       => 'sizing_md',
				'edit_field_class' => 'dima-heading-param-wrapper vc_column vc_col-sm-12',
			),
			array(
				'type'             => 'number',
				'heading'          => esc_html__( 'Screen resolution (px)', 'noor-assistant' ),
				'param_name'       => 'screen_md_resolution',
				'value'            => 1170,
				'edit_field_class' => 'no-border-bottom vc_column vc_col-sm-6 crum_vc dima-number-wrap'
			),
			array(
				'type'             => 'number',
				'heading'          => esc_html__( 'Spacer size', 'noor-assistant' ),
				'admin_label'      => true,
				'value'            => 120,
				'param_name'       => 'screen_md_spacer_size',
				'edit_field_class' => 'no-border-bottom vc_column vc_col-sm-6 crum_vc '
			),

			/*Small Devices Tablets*/
			array(
				'type'             => 'dima_heading_vc',
				'text'             => esc_html__( 'Small Devices ( Tablets )', 'noor-assistant' ),
				'param_name'       => 'sizing_sd',
				'edit_field_class' => 'dima-heading-param-wrapper vc_column vc_col-sm-12',
			),
			array(
				'type'             => 'number',
				'heading'          => esc_html__( 'Screen resolution (px)', 'noor-assistant' ),
				'param_name'       => 'screen_sd_resolution',
				'value'            => 969,
				'edit_field_class' => 'no-border-bottom vc_column vc_col-sm-6 crum_vc dima-number-wrap'
			),
			array(
				'type'             => 'number',
				'heading'          => esc_html__( 'Spacer size', 'noor-assistant' ),
				'admin_label'      => true,
				'value'            => 100,
				'param_name'       => 'screen_sd_spacer_size',
				'edit_field_class' => 'no-border-bottom vc_column vc_col-sm-6 crum_vc '
			),

			/*Extra Small Devices ( Mobile phones )*/
			array(
				'type'             => 'dima_heading_vc',
				'text'             => esc_html__( 'Small Devices ( Mobile phones )', 'noor-assistant' ),
				'param_name'       => 'sizing_xsd',
				'edit_field_class' => 'dima-heading-param-wrapper vc_column vc_col-sm-12',
			),
			array(
				'type'             => 'number',
				'heading'          => esc_html__( 'Screen resolution (px)', 'noor-assistant' ),
				'param_name'       => 'screen_xsd_resolution',
				'value'            => 480,
				'edit_field_class' => 'no-border-bottom vc_column vc_col-sm-6 crum_vc dima-number-wrap'
			),
			array(
				'type'             => 'number',
				'heading'          => esc_html__( 'Spacer size', 'noor-assistant' ),
				'admin_label'      => true,
				'value'            => 100,
				'param_name'       => 'screen_xsd_spacer_size',
				'edit_field_class' => 'no-border-bottom vc_column vc_col-sm-6 crum_vc '
			),

			array(
				'param_name'  => 'id',
				'group'       => esc_html__( 'General', 'noor-assistant' ),
				'heading'     => esc_html__( 'ID', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter a unique ID.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'class',
				'group'       => esc_html__( 'General', 'noor-assistant' ),
				'heading'     => esc_html__( 'Class', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter a unique class name.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			)
		)
	)
);