<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/*
* Add-on Name: testimonials
*/

class WPBakeryShortCode_Testimonials extends WPBakeryShortCode {
}

/**
 * testimonials
 */
vc_map(
	array(
		'base'        => 'testimonial',
		'name'        => esc_html__( 'Testimonial', 'noor-assistant' ),
		'weight'      => 810,
		'class'       => 'dima-vc-element dima-vc-element-testimonials',
		'icon'        => 'testimonials',
		'category'    => esc_html__( 'Typography', 'noor-assistant' ),
		'description' => esc_html__( 'Include a testimonials in your content', 'noor-assistant' ),
		'params'      => array(
			array(
				'param_name'  => 'content',
				'heading'     => esc_html__( 'Text', 'noor-assistant' ),
				'description' => esc_html__( 'Enter your text.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textarea_html',
				'holder'      => 'div',
				'value'       => ''
			),
			array(
				'param_name'  => 'float',
				'heading'     => esc_html__( 'Testimonial Style', 'noor-assistant' ),
				'description' => esc_html__( 'Choose testimonial style (Position of  photo,text.. )', 'noor-assistant' ),
				'type'        => 'dropdown',
				'holder'      => 'div',
				'value'       => array(
					esc_html__( '- Select Option -', 'noor-assistant' ) => '',
					esc_html__( 'Start', 'noor-assistant' )             => 'start',
					esc_html__( 'End', 'noor-assistant' )               => 'end',
					esc_html__( 'Top center', 'noor-assistant' )        => 'top-center',
					esc_html__( 'Top start', 'noor-assistant' )         => 'top-start',
					esc_html__( 'Top end', 'noor-assistant' )           => 'top-end',
					esc_html__( 'Bottom center', 'noor-assistant' )     => 'bottom-center',
					esc_html__( 'Bottom start', 'noor-assistant' )      => 'bottom-start',
					esc_html__( 'Bottom end', 'noor-assistant' )        => 'bottom-end',
				)
			),

			array(
				'param_name'  => 'text_alignment',
				'heading'     => esc_html__( 'Text Alignment', 'noor-assistant' ),
				'description' => esc_html__( 'Select the text alignment', 'noor-assistant' ),
				'type'        => 'dropdown',
				'holder'      => 'div',
				'value'       => array(
					esc_html__( 'Inherit', 'noor-assistant' ) => '',
					esc_html__( 'Start', 'noor-assistant' )   => 'start',
					esc_html__( 'center', 'noor-assistant' )  => 'center',
					esc_html__( 'End', 'noor-assistant' )     => 'end',
				)
			),
			array(
				'param_name'  => 'icon_float',
				'heading'     => esc_html__( 'Icon position', 'noor-assistant' ),
				'description' => esc_html__( 'Choose the position of quote icon', 'noor-assistant' ),
				'type'        => 'dropdown',
				'holder'      => 'div',
				'value'       => array(
					esc_html__( 'Default', 'noor-assistant' ) => '',
					esc_html__( 'Start', 'noor-assistant' )   => 'start',
					esc_html__( 'End', 'noor-assistant' )     => 'end',
				)
			),

			array(
				'param_name'  => 'author',
				'heading'     => esc_html__( 'Author Name', 'noor-assistant' ),
				'description' => esc_html__( 'Enter the author name', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'job',
				'heading'     => esc_html__( 'Author Position', 'noor-assistant' ),
				'description' => esc_html__( 'Enter the author position', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),

			array(
				'param_name'  => 'image',
				'heading'     => esc_html__( 'Author Photo', 'noor-assistant' ),
				'description' => esc_html__( 'Add the author photo', 'noor-assistant' ),
				'type'        => 'attach_image',
				'holder'      => 'div'
			),

			array(
				'param_name'  => 'image_circle',
				'group'       => 'style',
				'heading'     => esc_html__( 'Photo Circle', 'noor-assistant' ),
				'description' => esc_html__( 'Check to display the photo as a circle', 'noor-assistant' ),
				'type'        => 'checkbox',
				'holder'      => 'div',
				'value'       => array(
					'' => 'true'
				)
			),
			array(
				'param_name'  => 'border',
				'group'       => 'style',
				'heading'     => esc_html__( 'Add Border', 'noor-assistant' ),
				'description' => esc_html__( 'Check to add a border around the text', 'noor-assistant' ),
				'type'        => 'checkbox',
				'holder'      => 'div',
				'value'       => array(
					'' => 'true'
				)
			),
			array(
				'param_name'  => 'bg_color',
				'group'       => 'style',
				'heading'     => esc_html__( 'Background color', 'noor-assistant' ),
				'description' => esc_html__( 'Choose the background color', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'colorpicker',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'icon_color',
				'group'       => 'style',
				'heading'     => esc_html__( 'Quote icon color', 'noor-assistant' ),
				'description' => esc_html__( 'Choose the icon color', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'colorpicker',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'meta_color',
				'group'       => 'style',
				'heading'     => esc_html__( 'Meta Color', 'noor-assistant' ),
				'description' => esc_html__( 'Choose the meta color for author and author position', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'colorpicker',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'dima_vc_add_shadow',
				'group'       => 'style',
				'heading'     => esc_html__( 'Add Shadow', 'noor-assistant' ),
				'description' => esc_html__( 'Check to apply drop shadow effect', 'noor-assistant' ),
				'type'        => 'checkbox',
				'holder'      => 'div',
				'value'       => array(
					'' => 'true'
				)
			),
			array(
				'param_name'  => 'padding_top_bottom',
				'group'       => 'style',
				'heading'     => esc_html__( 'Padding Top & Bottom', 'noor-assistant' ),
				'description' => esc_html__( 'Enter the padding value between elements', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'padding_left_right',
				'group'       => 'style',
				'heading'     => esc_html__( 'Padding Left & Right', 'noor-assistant' ),
				'description' => esc_html__( 'Enter the padding value between elements', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),

			array(
				'param_name'  => 'icon',
				'heading'     => esc_html__( 'Hide quotre icon', 'noor-assistant' ),
				'description' => esc_html__( 'Check to hide Quote icon', 'noor-assistant' ),
				'type'        => 'checkbox',
				'holder'      => 'div',
				'value'       => array(
					'' => 'false'
				)
			),
			array(
				'param_name'  => 'url',
				'heading'     => esc_html__( 'URL Link', 'noor-assistant' ),
				'description' => esc_html__( 'Add external URL link to the author', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'target',
				'heading'     => esc_html__( 'Open link in a new tab', 'noor-assistant' ),
				'description' => esc_html__( 'Check to open your image link in a new tab.', 'noor-assistant' ),
				'type'        => 'checkbox',
				'holder'      => 'div',
				'value'       => array(
					'' => 'blank'
				)
			),
			array(
				'param_name'  => 'id',
				'heading'     => esc_html__( 'ID', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter a unique ID.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'class',
				'heading'     => esc_html__( 'Class', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter a unique class name.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'style',
				'group'       => 'style',
				'heading'     => esc_html__( 'Style', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter inline CSS.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			//**************Animation***************//
			array(
				'param_name'  => 'animation',
				'group'       => esc_html__( 'Animation', 'noor-assistant' ),
				'heading'     => esc_html__( 'Animation', 'noor-assistant' ),
				'description' => esc_html__( 'Select the type of animation you want to use.', 'noor-assistant' ),
				'type'        => 'dropdown',
				'holder'      => 'div',
				'value'       => $dima_vc->dima_animate_list_velocity,
			),

			array(
				'param_name'  => 'delay',
				'group'       => esc_html__( 'Animation', 'noor-assistant' ),
				'heading'     => esc_html__( 'Delay Animation', 'noor-assistant' ),
				'description' => esc_html__( 'Set when the animation start (milliseconds)', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div',
				'value'       => ''
			),

			array(
				'param_name'  => 'delay_duration',
				'group'       => esc_html__( 'Animation', 'noor-assistant' ),
				'heading'     => esc_html__( 'duration Animation', 'noor-assistant' ),
				'description' => esc_html__( 'Animation duration of each element  (milliseconds).', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div',
				'value'       => ''
			),

			array(
				'param_name'  => 'delay_offset',
				'group'       => esc_html__( 'Animation', 'noor-assistant' ),
				'heading'     => esc_html__( 'Delay Animation Offset', 'noor-assistant' ),
				'description' => esc_html__( 'Set the number ( px,%..) from the top of the viewport where the animation start.', 'noor-assistant' ) . "<a href='http://imakewebthings.com/waypoints/api/offset-option/' target='_blank' >" . esc_html__( 'Read More', 'noor-assistant' ) . "</a>",
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div',
				'value'       => ''
			),
			//**************!Animation***************//
			array(
				'param_name'  => 'tutorials',
				'type'        => 'dima_doc_link_param',
				'heading'     => '<span class="tippy dima-help dima-vc-toolip">' . dima_get_svg_icon( "ic_help" )
				                 . '<span class="dima-vc-tooltip-text">'
				                 . esc_html__( 'Video tutorial and theme documentation article', 'noor-assistant' ) . '</span></span>'
				                 . esc_html__( 'Tutorial', 'noor-assistant' ),
				'doc_link'    => '',
				'video_link'  => 'https://www.youtube.com/watch?v=5LHhHvvURM0',
				'doc_example' => 'https://noor.pixeldima.com/testimonials/',
			),
		)
	)
);