<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/*
* Add-on Name: Inline item icon
*/

class WPBakeryShortCode_Inline_Item_Icon extends WPBakeryShortCode {
}

class WPBakeryShortCode_Inline_Icons extends WPBakeryShortCodesContainer {
}

/**
 * Inline item icon.
 */
vc_map(
	array(
		'base'            => 'inline_item_icon',
		'name'            => esc_html__( 'Item Icon', 'noor-assistant' ),
		'weight'          => 960,
		'class'           => 'dima-vc-element dima-vc-element-item_icon',
		'icon'            => 'item_icon',
		'as_child'        => array( 'only' => 'meet_the_team', 'inline_icons' ),
		'category'        => esc_html__( 'Structure', 'noor-assistant' ),
		'description'     => esc_html__( 'Add Icon', 'noor-assistant' ),
		'content_element' => true,
		'params'          => array(
			array(
				'heading'     => esc_html__( 'Icons library', 'noor-assistant' ),
				'type'        => 'dropdown',
				'value'       => array(
					esc_html__( 'Font Awesome', 'noor-assistant' )                                     => 'fontawesome',
					esc_html__( 'Custom Font', 'noor-assistant' )                                      => 'customfont',
					esc_html__( 'SVG Icons (Stroke Gap Icons And Material Icons) ', 'noor-assistant' ) => 'svgicon',
				),
				'param_name'  => 'icon_type',
				'description' => esc_html__( 'Select icons library.', 'noor-assistant' ),
			),
			array(
				'param_name'  => 'icon_svg',
				'heading'     => esc_html__( 'Icon SVG', 'noor-assistant' ),
				'description' => esc_html__( 'Enter SVG Icon name', 'noor-assistant' ),
				'value'       => '',
				'dependency'  => array(
					'element' => 'icon_type',
					'value'   => 'svgicon',
				),
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'icon',
				'heading'     => esc_html__( 'Icon Class', 'noor-assistant' ),
				'description' => esc_html__( 'Enter Icon Class from Font Awesome or Simple Line Icons.', 'noor-assistant' ),
				'value'       => 'simple-icon-user',
				'dependency'  => array(
					'element' => 'icon_type',
					'value'   => 'customfont',
				),
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'type'        => 'iconpicker',
				'heading'     => esc_html__( 'Icon', 'noor-assistant' ),
				'param_name'  => 'icon_fontawesome',
				'value'       => 'simple-icon-user', // default value to backend editor admin_label
				'settings'    => array(
					'emptyIcon'    => true,
					// default true, display an "EMPTY" icon?
					'value'        => 'fontawesome',
					'iconsPerPage' => 4000,
					// default 100, how many icons per/page to display, we use (big number) to display all icons in single page
				),
				'dependency'  => array(
					'element' => 'icon_type',
					'value'   => 'fontawesome',
				),
				'description' => esc_html__( 'Select icon from library.', 'noor-assistant' ),
			),
			array(
				'param_name'  => 'title',
				'heading'     => esc_html__( 'Icon Title', 'noor-assistant' ),
				'description' => esc_html__( 'Add title to the icon (the title will be displayed as a tooltip)', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),

			array(
				'param_name'  => 'href',
				'heading'     => esc_html__( 'URL Link', 'noor-assistant' ),
				'description' => esc_html__( 'Add URL Link', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),

			array(
				'param_name'  => 'sicon_color',
				'group'       => 'style',
				'heading'     => esc_html__( 'social icon', 'noor-assistant' ),
				'description' => esc_html__( 'Choose social icon', 'noor-assistant' ),
				'type'        => 'dropdown',
				'holder'      => 'div',
				'value'       => $dima_vc->vc_sicon_color,
			),

			array(
				'param_name'  => 'icon_color',
				'group'       => 'style',
				'heading'     => esc_html__( 'Icon Color', 'noor-assistant' ),
				'description' => esc_html__( 'Choose icon color.', 'noor-assistant' ),
				'type'        => 'colorpicker',
			),
			array(
				'param_name'  => 'icon_bg',
				'group'       => 'style',
				'heading'     => esc_html__( 'Icon Background Color', 'noor-assistant' ),
				'description' => esc_html__( 'Choose icon background color.', 'noor-assistant' ),
				'type'        => 'colorpicker',
			),
			array(
				'param_name'  => 'id',
				'heading'     => esc_html__( 'ID', 'noor-assistant' ),
				'description' => $dima_vc->id_des,
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'class',
				'heading'     => esc_html__( 'Class', 'noor-assistant' ),
				'description' => $dima_vc->class_des,
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'style',
				'heading'     => esc_html__( 'Style', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter inline CSS.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'tutorials',
				'type'        => 'dima_doc_link_param',
				'heading'     => '<span class="tippy dima-help dima-vc-toolip">' . dima_get_svg_icon( "ic_help" )
				                 . '<span class="dima-vc-tooltip-text">'
				                 . esc_html__( 'Video tutorial and theme documentation article', 'noor-assistant' ) . '</span></span>'
				                 . esc_html__( 'Tutorial', 'noor-assistant' ),
				'doc_link'    => '',
				'video_link'  => 'https://www.youtube.com/watch?v=BQMAvgIUNuY',
				'doc_example' => 'https://noor.pixeldima.com/social-icons/',
			),
		)
	)
);

/**
 * Inline item icon.
 */
vc_map(
	array(
		'base'            => 'inline_icons',
		'name'            => esc_html__( 'Inline Icons', 'noor-assistant' ),
		'weight'          => 960,
		'class'           => 'dima-vc-element dima-vc-element-item_icon',
		'icon'            => 'inline_icon',
		'category'        => esc_html__( 'Structure', 'noor-assistant' ),
		'description'     => esc_html__( 'Add icons to your content', 'noor-assistant' ),
		'as_parent'       => array( 'only' => 'inline_item_icon' ),
		'content_element' => true,
		'js_view'         => 'VcColumnView',
		'params'          => array(
			array(
				'param_name'  => 'id',
				'heading'     => esc_html__( 'ID', 'noor-assistant' ),
				'description' => $dima_vc->id_des,
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'class',
				'heading'     => esc_html__( 'Class', 'noor-assistant' ),
				'description' => $dima_vc->class_des,
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),
			array(
				'param_name'  => 'style',
				'heading'     => esc_html__( 'Style', 'noor-assistant' ),
				'description' => esc_html__( '(Optional) Enter inline CSS.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			),

			array(
				'param_name'  => 'icon_style',
				'group'       => esc_html__( 'Style', 'noor-assistant' ),
				'heading'     => esc_html__( 'Icon Style', 'noor-assistant' ),
				'description' => esc_html__( 'Choose your icon style', 'noor-assistant' ),
				'type'        => 'dropdown',
				'holder'      => 'div',
				'value'       => array(
					esc_html__( '- Select Option -', 'noor-assistant' ) => '',
					esc_html__( 'Outline', 'noor-assistant' )           => 'outline-icon',
					esc_html__( 'Fill', 'noor-assistant' )              => 'fill-icon',
				)
			),

			array(
				'param_name'  => 'float',
				'group'       => esc_html__( 'Style', 'noor-assistant' ),
				'heading'     => esc_html__( 'Icon Alignment', 'noor-assistant' ),
				'description' => esc_html__( 'Select Icon alignment.', 'noor-assistant' ),
				'type'        => 'dropdown',
				'holder'      => 'div',
				'value'       => array(
					esc_html__( '- Select Option -', 'noor-assistant' ) => '',
					esc_html__( 'Start', 'noor-assistant' )             => 'text-start',
					esc_html__( 'Center', 'noor-assistant' )            => 'text-center',
					esc_html__( 'End', 'noor-assistant' )               => 'text-end',
				)
			),

			array(
				'param_name'  => 'size',
				'group'       => esc_html__( 'Style', 'noor-assistant' ),
				'heading'     => esc_html__( 'Icon Size', 'noor-assistant' ),
				'description' => esc_html__( 'Select the icon size', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'dropdown',
				'value'       => array(
					esc_html__( 'Select Option', 'noor-assistant' ) => '',
					esc_html__( 'Small', 'noor-assistant' )         => 'small',
					esc_html__( 'Medium', 'noor-assistant' )        => 'medium',
					esc_html__( 'Big', 'noor-assistant' )           => 'big',
				),
				'holder'      => 'div'
			),

			array(
				'param_name'  => 'circle',
				'group'       => esc_html__( 'Style', 'noor-assistant' ),
				'heading'     => esc_html__( 'Circle', 'noor-assistant' ),
				'description' => esc_html__( 'Check to use circle around the icon instead of square', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'toggle',
				'value'       => 'false',
				'holder'      => 'div'
			),
			//**************Animation***************//
			array(
				'param_name'  => 'animation',
				'group'       => esc_html__( 'Animation', 'noor-assistant' ),
				'heading'     => esc_html__( 'Animation', 'noor-assistant' ),
				'description' => esc_html__( 'Select the type of animation you want to use.', 'noor-assistant' ),
				'type'        => 'dropdown',
				'holder'      => 'div',
				'value'       => $dima_vc->dima_animate_list_velocity,
			),

			array(
				'param_name'  => 'delay',
				'group'       => esc_html__( 'Animation', 'noor-assistant' ),
				'heading'     => esc_html__( 'Delay Animation', 'noor-assistant' ),
				'description' => esc_html__( 'Set when the animation start (milliseconds)', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div',
				'value'       => ''
			),

			array(
				'param_name'  => 'delay_duration',
				'group'       => esc_html__( 'Animation', 'noor-assistant' ),
				'heading'     => esc_html__( 'duration Animation', 'noor-assistant' ),
				'description' => esc_html__( 'Animation duration of each element  (milliseconds).', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div',
				'value'       => ''
			),

			array(
				'param_name'  => 'delay_offset',
				'group'       => esc_html__( 'Animation', 'noor-assistant' ),
				'heading'     => esc_html__( 'Delay Animation Offset', 'noor-assistant' ),
				'description' => esc_html__( 'Set the number ( px,%..) from the top of the viewport where the animation start.', 'noor-assistant' ) . "<a href='http://imakewebthings.com/waypoints/api/offset-option/' target='_blank' >" . esc_html__( 'Read More', 'noor-assistant' ) . "</a>",
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div',
				'value'       => ''
			),
			//**************!Animation***************//
			array(
				'param_name'  => 'tutorials',
				'type'        => 'dima_doc_link_param',
				'heading'     => '<span class="tippy dima-help dima-vc-toolip">' . dima_get_svg_icon( "ic_help" )
				                 . '<span class="dima-vc-tooltip-text">'
				                 . esc_html__( 'Video tutorial and theme documentation article', 'noor-assistant' ) . '</span></span>'
				                 . esc_html__( 'Tutorial', 'noor-assistant' ),
				'doc_link'    => '',
				'video_link'  => 'https://www.youtube.com/watch?v=BQMAvgIUNuY',
				'doc_example' => 'https://noor.pixeldima.com/social-icons/',
			),
		)
	)
);