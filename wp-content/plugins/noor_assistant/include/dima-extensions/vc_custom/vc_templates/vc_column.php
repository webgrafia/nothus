<?php

if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}
$padding = $id = $class = $animation = $css = $section_content =
$min_height = $xld = $ld = $sd = $xsd = $border =
$di_bg_image = $style = $width = $visibility_xld =
$visibility_ld = $visibility_md = $visibility_sd = $visibility_xsd =
$translate_x_fixed = $translate_x = $translate_y = $translate_y_fixed = $column_layout =
$border_color = $back_repeat = $back_position = $back_attachment = $border_style = $dima_z_index = '';

extract( shortcode_atts( array(
	'id'             => '',
	'class'          => '',
	'style'          => '',
	'width'          => '',
	'xld'            => '',
	'ld'             => '',
	'sd'             => '',
	'xsd'            => '12',
	'visibility_xld' => '',
	'visibility_ld'  => '',
	'visibility_md'  => '',
	'visibility_sd'  => '',
	'visibility_xsd' => '',

	'animation'      => '',
	'delay'          => '',
	'delay_offset'   => '',
	'delay_duration' => '',

	'cover'       => false,
	'cover_color' => '',
	'bg_gradient' => 'false',
	'gr_end'      => '',
	'gr_opacity'  => '0.6',

	'di_bg_video'     => '',
	'di_bg_image'     => '',
	'dark'            => false,
	'min_height'      => '',
	'parallax'        => false,
	'parallax_start'  => '0%',
	'parallax_center' => '50%',
	'parallax_end'    => '100%',
	'bg_type'         => '',

	'css'               => '',
	'border_color'      => '',
	'border_style'      => '',
	'back_repeat'       => '',
	'back_attachment'   => 'scroll',
	'back_position'     => 'center center',
	'back_size'         => '',
	'dima_canvas_style' => '',
	'dima_canvas_color' => '',

	'translate_x_fixed' => '',
	'translate_x'       => '',
	'translate_y'       => '',
	'translate_y_fixed' => '',
	'dima_z_index'      => '',
	'column_layout'     => '',
	'sticky_sidebar'    => '',
), $atts ) );

$uniq_id = uniqid();

$row_cont_classes = array();
$id               = ( $id != '' ) ? 'id="' . esc_attr( $id ) . '"' : '';
$row_class[]      = ( $class != '' ) ? esc_attr( $class ) : '';
$row_class[]      = ( esc_attr( $xld ) ) ? 'ok-xld-' . esc_attr( $xld ) : '';
$row_class[]      = ( esc_attr( $ld ) ) ? 'ok-ld-' . esc_attr( $ld ) : '';
$row_class[]      = ( esc_attr( $sd ) ) ? 'ok-sd-' . esc_attr( $sd ) : '';
$row_class[]      = ( esc_attr( $xsd ) ) ? 'ok-xsd-' . esc_attr( $xsd ) : '';
$row_class[]      = 'page-section-' . $uniq_id . '';

if ( $visibility_xld != '' ) {
	$visibility_xld = explode( ',', $visibility_xld );
	$row_class[]    = ( esc_attr( $visibility_xld[0] ) != '' ) ? '' . esc_attr( $visibility_xld[0] ) . '-xld' : '';
}
if ( $visibility_ld != '' ) {
	$visibility_ld = explode( ',', $visibility_ld );
	$row_class[]   = ( esc_attr( $visibility_ld[0] ) != '' ) ? '' . esc_attr( $visibility_ld[0] ) . '-ld' : '';
}
if ( $visibility_md != '' ) {
	$visibility_md = explode( ',', $visibility_md );
	$row_class[]   = ( esc_attr( $visibility_md[0] ) != '' ) ? '' . esc_attr( $visibility_md[0] ) . '-md' : '';
}
if ( $visibility_sd != '' ) {
	$visibility_sd = explode( ',', $visibility_sd );
	$row_class[]   = ( esc_attr( $visibility_sd[0] ) != '' ) ? '' . esc_attr( $visibility_sd[0] ) . '-sd' : '';
}
if ( $visibility_xsd != '' ) {
	$visibility_xsd = explode( ',', $visibility_xsd );
	$row_class[]    = ( esc_attr( $visibility_xsd[0] ) != '' ) ? '' . esc_attr( $visibility_xsd[0] ) . '-xsd' : '';
}


if ( $border_color !== '' ) {
	$border = ' style="border-color:' . $border_color . ';border-style: ' . $border_style . ';"';
}
if ( $css !== '' ) {
	$row_cont_classes[] = trim( vc_shortcode_custom_css_class( $css ) );
}

$di_bg_image = ( $di_bg_image != '' ) ? '' . esc_attr( $di_bg_image ) . '' : '';
$bg_class    = ' background-image-hide';
$dark        = ( $dark == 'true' ) ? ' dark-bg' : '';

/*------------------------------*/
# Gradient Overlay Orientation
/*------------------------------*/
if ( $cover == 'true' && $bg_gradient == 'false' ) {

	$el       = '.page-section-' . $uniq_id . ' .dima-color-mask-' . $uniq_id . '';
	$cover    = '<div class="dima-section-cover background-image-holder dima-color-mask-' . $uniq_id . '"></div>';
	$gr_start = $cover_color;

	$horizontal = "background: " . $gr_start . ";";
	DIMA_Style::dima_addCSS( $el . '{'
	                         . $horizontal
	                         . '}', $uniq_id );

} elseif ( $bg_gradient != 'false' ) {

	$el       = '.page-section-' . $uniq_id . ' .dima-color-mask-' . $uniq_id . '';
	$vertical = $horizontal = $left_top = $left_bottom = $radial = '';
	$cover    = '<div class="dima-section-cover background-image-holder dima-color-mask-' . $uniq_id . '"></div>';

	$gr_start = dima_is_gradient_stop_transparent( $cover_color ) ? 'transparent' : $cover_color;
	$gr_end   = dima_is_gradient_stop_transparent( $gr_end ) ? 'transparent' : $gr_end;

	if ( $bg_gradient == 'vertical' ) {
		$vertical = "
            background: " . $gr_start . "; /* Old browsers */
            background: -moz-linear-gradient(top,  " . $gr_start . " 0%, " . $gr_end . " 100%); /* FF3.6+ */
            background: -webkit-gradient(linear, left top, left bottom, color-stop(0%," . $gr_start . "), color-stop(100%," . $gr_end . ")); /* Chrome,Safari4+ */
            background: -webkit-linear-gradient(top,  " . $gr_start . " 0%," . $gr_end . " 100%); /* Chrome10+,Safari5.1+ */
            background: -o-linear-gradient(top,  " . $gr_start . " 0%," . $gr_end . " 100%); /* Opera 11.10+ */
            background: -ms-linear-gradient(top,  " . $gr_start . " 0%," . $gr_end . " 100%); /* IE10+ */
            background: linear-gradient(to bottom,  " . $gr_start . " 0%," . $gr_end . " 100%); /* W3C */
        ";
	}

	if ( $bg_gradient == 'horizontal' ) {
		$horizontal = "
            background: " . $gr_start . "; /* Old browsers */
            background: -moz-linear-gradient(left,  " . $gr_start . " 0%, " . $gr_end . " 100%); /* FF3.6+ */
            background: -webkit-gradient(linear, left top, right top, color-stop(0%," . $gr_start . "), color-stop(100%," . $gr_end . ")); /* Chrome,Safari4+ */
            background: -webkit-linear-gradient(left,  " . $gr_start . " 0%," . $gr_end . " 100%); /* Chrome10+,Safari5.1+ */
            background: -o-linear-gradient(left,  " . $gr_start . " 0%," . $gr_end . " 100%); /* Opera 11.10+ */
            background: -ms-linear-gradient(left,  " . $gr_start . " 0%," . $gr_end . " 100%); /* IE10+ */
            background: linear-gradient(to right,  " . $gr_start . " 0%," . $gr_end . " 100%); /* W3C */
        ";
	}

	if ( $bg_gradient == 'left_top' ) {
		$left_top = "
            background: " . $gr_start . "; /* Old browsers */
            background: -moz-linear-gradient(-45deg,  " . $gr_start . " 0%, " . $gr_end . " 100%); /* FF3.6+ */
            background: -webkit-gradient(linear, left top, right bottom, color-stop(0%," . $gr_start . "), color-stop(100%," . $gr_end . ")); /* Chrome,Safari4+ */
            background: -webkit-linear-gradient(-45deg,  " . $gr_start . " 0%," . $gr_end . " 100%); /* Chrome10+,Safari5.1+ */
            background: -o-linear-gradient(-45deg,  " . $gr_start . " 0%," . $gr_end . " 100%); /* Opera 11.10+ */
            background: -ms-linear-gradient(-45deg,  " . $gr_start . " 0%," . $gr_end . " 100%); /* IE10+ */
            background: linear-gradient(135deg,  " . $gr_start . " 0%," . $gr_end . " 100%); /* W3C */
        ";
	}

	if ( $bg_gradient == 'left_bottom' ) {
		$left_bottom = "
            background: " . $gr_start . "; /* Old browsers */
            background: -moz-linear-gradient(45deg,  " . $gr_start . " 0%, " . $gr_end . " 100%); /* FF3.6+ */
            background: -webkit-gradient(linear, left bottom, right top, color-stop(0%," . $gr_start . "), color-stop(100%," . $gr_end . ")); /* Chrome,Safari4+ */
            background: -webkit-linear-gradient(45deg,  " . $gr_start . " 0%," . $gr_end . " 100%); /* Chrome10+,Safari5.1+ */
            background: -o-linear-gradient(45deg,  " . $gr_start . " 0%," . $gr_end . " 100%); /* Opera 11.10+ */
            background: -ms-linear-gradient(45deg,  " . $gr_start . " 0%," . $gr_end . " 100%); /* IE10+ */
            background: linear-gradient(45deg,  " . $gr_start . " 0%," . $gr_end . " 100%); /* W3C */
        ";
	}

	if ( $bg_gradient == 'radial' ) {
		$radial = "
            background: " . $gr_start . "; /* Old browsers */
            background: -moz-radial-gradient(center, ellipse cover,  " . $gr_start . " 0%, " . $gr_end . " 100%); /* FF3.6+ */
            background: -webkit-gradient(radial, center center, 0px, center center, 100%, color-stop(0%," . $gr_start . "), color-stop(100%," . $gr_end . ")); /* Chrome,Safari4+ */
            background: -webkit-radial-gradient(center, ellipse cover,  " . $gr_start . " 0%," . $gr_end . " 100%); /* Chrome10+,Safari5.1+ */
            background: -o-radial-gradient(center, ellipse cover,  " . $gr_start . " 0%," . $gr_end . " 100%); /* Opera 12+ */
            background: -ms-radial-gradient(center, ellipse cover,  " . $gr_start . " 0%," . $gr_end . " 100%); /* IE10+ */
            background: radial-gradient(ellipse at center,  " . $gr_start . " 0%," . $gr_end . " 100%); /* W3C */
        ";
	}

	$opacity = 'opacity:' . $gr_opacity . ';';

	DIMA_Style::dima_addCSS( $el . '{'
	                         . $vertical
	                         . $horizontal
	                         . $left_top
	                         . $left_bottom
	                         . $radial
	                         . $opacity
	                         . '}', $uniq_id );
}
$bg_con = $data_atts = '';
if ( $dima_canvas_style != '' ) {
	$uniqid    = uniqid( 'dima-canvas-' );
	$data_atts .= ' data-canvas-color="' . esc_attr( $dima_canvas_color ) . '"';
	$data_atts .= ' id="' . esc_attr( $uniqid ) . '"';
	$data_atts .= ' data-canvas-id="' . esc_attr( $uniqid ) . '"';
	$data_atts .= ' data-canvas-style="' . $dima_canvas_style . '"';

	if ( $dima_canvas_style == 'canvas_1' ) {
		wp_enqueue_script( 'jquery.particleground' );
	} else {
		wp_enqueue_script( 'dima-particles' );
	}
	$bg_con = '<div class="background-image-holder dima-row-bg-canvas" ' . $data_atts . '></div>';
}

if ( is_numeric( $di_bg_image ) ) {
	$di_bg_image_info = wp_get_attachment_image_src( $di_bg_image, 'full' );
	$di_bg_image      = $di_bg_image_info[0];
}

if ( $border_color !== '' ) {
	$border = ' style="border-color:' . $border_color . ';border-style: ' . $border_style . ';"';
}

$padding = ( $min_height != '' ) ? 'min-height: ' . $min_height . ';' : '';

if ( $padding != '' ) {
	$padding = 'style="' . $padding . '"';
}

$style = ( $style != '' ) ? 'style="' . $style . '"' : '';

$delay          = ( $delay != '' ) ? ' data-delay=' . $delay . '' : '';
$delay_offset   = ( $delay_offset != '' ) ? ' data-offset=' . $delay_offset . '' : '';
$animation      = ( $animation != '' ) ? ' data-animate=' . $animation . '' : '';
$delay_duration = ( $delay_duration != '' ) ? ' data-duration=' . $delay_duration . '' : '';

switch ( $width ) {
	case '1/1' :
		$width = 'ok-md-12';
		break;
	case '1/2' :
		$width = 'ok-md-6';
		break;
	case '1/3' :
		$width = 'ok-md-4';
		break;
	case '2/3' :
		$width = 'ok-md-8';
		break;
	case '1/4' :
		$width = 'ok-md-3';
		break;
	case '3/4' :
		$width = 'ok-md-9';
		break;
	case '1/6' :
		$width = 'ok-md-2';
		break;
	case '5/6' :
		$width = 'ok-md-10';
		break;
	case '5/12' :
		$width = 'ok-md-5';
		break;
	case '7/12' :
		$width = 'ok-md-7';
		break;
	default:
		$width = 'ok-md-12';
		break;
}

$row_class[] = $width;

if ( $this->settings['base'] == 'vc_column' ) {
	$row_class[] = 'column_parent';
} else {
	$row_class[] = 'column_child';
}

/** BEGIN - translate construction **/
if ( ( $translate_x != '0' && $translate_x != '' ) || ( $translate_y != '0' && $translate_y != '' ) ) {
	switch ( $translate_x ) {
		case 1:
			$row_class[] = 'translate_x_1';
			break;
		case 2:
			$row_class[] = 'translate_x_2';
			break;
		case 3:
			$row_class[] = 'translate_x_3';
			break;
		case - 1:
			$row_class[] = 'translate_x_neg_1';
			break;
		case - 2:
			$row_class[] = 'translate_x_neg_2';
			break;
		case - 3:
			$row_class[] = 'translate_x_neg_3';
			break;
	}

	switch ( $translate_y ) {
		case 1:
			$row_class[] = 'translate_y_1';
			break;
		case 2:
			$row_class[] = 'translate_y_2';
			break;
		case 3:
			$row_class[] = 'translate_y_3';
			break;
		case - 1:
			$row_class[] = 'translate_y_neg_1';
			break;
		case - 2:
			$row_class[] = 'translate_y_neg_2';
			break;
		case - 3:
			$row_class[] = 'translate_y_neg_3';
			break;
	}

	if ( $translate_x_fixed === 'true' ) {
		$row_class[] = 'translate_x_fixed';
	}
	if ( $translate_y_fixed === 'true' ) {
		$row_class[] = 'translate_y_fixed';
	}
}
if ( $dima_z_index !== '0' && $dima_z_index !== '' ) {
	$row_class[] = 'z_index_' . str_replace( '-', 'neg_', $dima_z_index );
}
/** END - translate construction **/

$row_class[] = preg_replace( '/\s+/', ' ', apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, implode( ' ', array_filter( $row_cont_classes ) ), $this->settings['base'], $atts ) );
$class       = esc_attr( trim( implode( ' ', $row_class ) ) );

switch ( $bg_type ) {
	case 'bg_image':
		if ( $parallax === 'parallax' ) {
			$back_size = '';
		} else {
			if ( $back_size === '' ) {
				$back_size = 'cover';
			}
		}

		$background      = array(
			'background-image'      => $di_bg_image,
			'background-color'      => $cover_color,
			'background-repeat'     => $back_repeat,
			'background-position'   => $back_position,
			'background-size'       => $back_size,
			'background-attachment' => $back_attachment,
		);
		$back_repeat     = ( isset( $background['background-repeat'] ) && $background['background-repeat'] !== '' ) ? 'background-repeat: ' . $background['background-repeat'] . ';' : '';
		$back_position   = ( isset( $background['background-position'] ) && $background['background-position'] !== '' ) ? 'background-position: ' . $background['background-position'] . ';' : '';
		$back_attachment = ( isset( $background['background-attachment'] ) && $background['background-attachment'] !== '' ) ? 'background-attachment: ' . $background['background-attachment'] . ';' : '';
		$back_size       = ( isset( $background['background-size'] ) && $background['background-size'] !== '' ) ? 'background-size: ' . $background['background-size'] . ';' : '';
		$back_url        = ( $di_bg_image !== '' ) ? 'background-image: url(' . $di_bg_image . ');' : '';

		if ( strpos( $back_repeat, "repeat" ) ) {
			$back_size = "";
		}
		$back_image_style = ( $back_url != '' || $back_repeat != '' || $back_position != '' || $back_attachment != '' || $back_size != '' ) ? ' style="' . $back_url . $back_repeat . $back_position . $back_attachment . $back_size . '"' : '';


		$parallax_start = $parallax_center = $parallax_end = "";

		if ( $parallax == 'parallax' ) {
			$parallax_start  = ( $parallax_start != '' ) ? 'data-parallax-start="' . esc_attr( $parallax_start ) . '"' : '';
			$parallax_center = ( $parallax_center != '' ) ? 'data-parallax-center="' . esc_attr( $parallax_center ) . '"' : '';
			$parallax_end    = ( $parallax_end != '' ) ? 'data-parallax-end="' . esc_attr( $parallax_end ) . '"' : '';
			$bg_class        .= " parallax-background";
		}

		$output = "<div {$id} class=\"{$class}\" {$style} {$animation} {$delay} {$delay_offset} {$delay_duration} {$border} >"
		          . $cover
		          . $bg_con
		          . '<div class="' . $dark . '" ' . $padding . '>'
		          . '<div class="' . $bg_class . '" ' . $parallax_start . $parallax_center . $parallax_end . $back_image_style . '>'
		          . '</div>'
		          . '<div class="dimacoltable page-section">'
		          . '<div class="dimacell">'
		          . do_shortcode( $content )
		          . '</div>'
		          . '</div>'
		          . '</div>'
		          . '</div>';
		break;
	case 'bg_video':
		if ( ! empty( $di_bg_video ) ) {
			$back_metadata   = dima_media_support::dima_get_metadata( $di_bg_video );
			$back_attributes = dima_media_support::dima_get_thumb_url( $di_bg_video );
			$provider        = dima_media_support::dima_detect_video_service( $di_bg_video );
			$video_orig_w    = $video_orig_h = $video_ratio = 0;

			if ( ! empty( $back_metadata ) ) {
				$video_orig_w = ( $back_metadata->width == 0 ) ? 800 : $back_metadata->width;
				$video_orig_h = ( $back_metadata->height == 0 ) ? 800 : $back_metadata->height;
				$video_ratio  = ( $video_orig_h === 0 ) ? 1.777 : $video_orig_w / $video_orig_h;
			}

			if ( $provider == 'youtube' || $provider == 'vimeo' ) {
				$video_orig_w = ( $back_metadata->width == 0 ) ? 800 : $back_metadata->width;
				$video_orig_h = ( $back_metadata->height == 0 ) ? 800 : $back_metadata->height;
				$video_ratio  = ( $video_orig_h === 0 ) ? 1.777 : $video_orig_w / $video_orig_h;

				$output = "<div {$id} class=\"background-element {$class}\" {$style} {$animation} {$delay} {$delay_offset} {$delay_duration}>"
				          . $cover
				          . $bg_con
				          . '<div class="page-section-content ' . $dark . '" ' . $padding . '>'
				          . '<div class="' . $bg_class . ' parallax-background" data-img-wrap="' . $di_bg_image . '">'
				          . '</div>'
				          . '<div class="dimacoltable">'
				          . '<div class="dimacell">'
				          . '<div class="' . $bg_class . ' dima-' . $provider . ' video dima-video-container" data-ratio="' . $video_ratio . '" data-provider="' . $provider . '" data-video="' . $di_bg_video . '" data-id="' . rand( 10000, 99999 ) . '" data-img-wrap="' . $di_bg_image . '">'
				          . '</div>'
				          . '<div class="page-section">'
				          . do_shortcode( $content )
				          . '</div>'
				          . '</div>'
				          . '</div>'
				          . '</div>'
				          . '</div>';
			} else {
				wp_enqueue_script( 'video-js' );
				wp_enqueue_script( 'bigvideo-js' );
				$output = "<div {$id} class=\"background-element {$class}\" {$style} {$animation} {$delay} {$delay_offset} {$delay_duration}>"
				          . $cover
				          . $bg_con
				          . '<div class="page-section-content ' . $dark . '" ' . $padding . '>'
				          . '<div class="' . $bg_class . ' parallax-background" data-img-wrap="' . $di_bg_image . '">'
				          . '</div>'
				          . '<div class="dimacoltable">'
				          . '<div class="dimacell">'
				          . '<div class="' . $bg_class . ' parallax-background video-wrap" data-video-wrap="' . $di_bg_video . '" data-img-wrap="' . $back_attributes . '">'
				          . '</div>'
				          . '<div class="page-section">'
				          . do_shortcode( $content )
				          . '</div>'
				          . '</div>'
				          . '</div>'
				          . '</div>'
				          . '</div>';

			}
		}
		break;
	default:
		$output = "<div {$id} class=\"{$class}\" {$style} {$animation} {$delay} {$delay_offset} {$delay_duration} {$border}>"
		          . $cover
		          . $bg_con
		          . '<div class="page-section ' . $dark . '" ' . $padding . '>'
		          . '<div class="dimacoltable page-section">'
		          . '<div class="dimacell">'
		          . do_shortcode( $content )
		          . '</div>'
		          . '</div>'
		          . '</div>'
		          . "</div>";
		break;
}

if ( $sticky_sidebar == 'true' ) {
	$sticky_sidebar = 'sidebar-is-sticky';
} else {
	$sticky_sidebar = '';
}

if ( $column_layout != '' ) {

	switch ( $column_layout ) {
		case 'start_sidebar':
			$output = "<aside class=\"$sticky_sidebar dima-sidebar hidden-tm hidden float-start\" role=\"complementary\">"
			          . do_shortcode( $content )
			          . '</aside>';
			break;
		case 'end_sidebar':
			$output = "<aside class=\"$sticky_sidebar dima-sidebar hidden-tm hidden float-end\" role=\"complementary\">"
			          . do_shortcode( $content )
			          . '</aside>';
			break;
		case 'main_end_content':
			$output = "<aside class=\"dima-container float-end\">"
			          . do_shortcode( $content )
			          . '</aside>';
			break;
		case 'main_start_content':
			$output = "<aside class=\"dima-container float-start\">"
			          . do_shortcode( $content )
			          . '</aside>';
			break;
	}

}

echo wpb_js_remove_wpautop( $output );