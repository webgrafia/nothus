<?php

if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}
$offset_md = $animation = $xld = $ld = $sd = $xsd =
$offset_xld = $offset_ld = $style = $width =
$translate_x_fixed = $translate_x = $translate_y = $translate_y_fixed = $dima_z_index = '';

extract( shortcode_atts( array(
	'id'             => '',
	'class'          => '',
	'style'          => '',
	'width'          => '',
	'xld'            => '',
	'ld'             => '',
	'sd'             => '6',
	'xsd'            => '12',
	'offset_xld'     => '',
	'offset_ld'      => '',
	'offset_md'      => '',
	'visibility_xld' => '',
	'visibility_ld'  => '',
	'visibility_md'  => '',
	'visibility_sd'  => '',
	'visibility_xsd' => '',

	'animation'      => '',
	'delay'          => '',
	'delay_offset'   => '',
	'delay_duration' => '',

	'translate_x_fixed' => '',
	'translate_x'       => '',
	'translate_y'       => '',
	'translate_y_fixed' => '',
	'dima_z_index'      => '',


), $atts ) );


$id          = ( $id != '' ) ? 'id="' . esc_attr( $id ) . '"' : '';
$row_class[] = ( $class != '' ) ? esc_attr( $class ) : '';
$row_class[] = ( esc_attr( $xld ) ) ? ' ok-xld-' . esc_attr( $xld ) : '';
$row_class[] = ( esc_attr( $ld ) ) ? ' ok-ld-' . esc_attr( $ld ) : '';
$row_class[] = ( esc_attr( $sd ) ) ? ' ok-sd-' . esc_attr( $sd ) : '';
$row_class[] = ( esc_attr( $xsd ) ) ? ' ok-xsd-' . esc_attr( $xsd ) : '';
$row_class[] = ( esc_attr( $offset_xld ) != '' ) ? ' ok-offset-xld-' . esc_attr( $offset_xld ) : '';
$row_class[] = ( esc_attr( $offset_ld ) != '' ) ? ' ok-offset-ld-' . esc_attr( $offset_ld ) : '';
$row_class[] = ( esc_attr( $offset_md ) != '' ) ? ' ok-offset-md-' . esc_attr( $offset_md ) : '';

if ( $visibility_xld != '' ) {
	$visibility_xld = explode( ',', $visibility_xld );
	$row_class[]    = ( esc_attr( $visibility_xld[0] ) != '' ) ? ' ' . esc_attr( $visibility_xld[0] ) . '-xld' : '';
}

if ( $visibility_ld != '' ) {
	$visibility_ld = explode( ',', $visibility_ld );
	$row_class[]   = ( esc_attr( $visibility_ld[0] ) != '' ) ? ' ' . esc_attr( $visibility_ld[0] ) . '-ld' : '';
}

if ( $visibility_md != '' ) {
	$visibility_md = explode( ',', $visibility_md );
	$row_class[]   = ( esc_attr( $visibility_md[0] ) != '' ) ? ' ' . esc_attr( $visibility_md[0] ) . '-md' : '';
}

if ( $visibility_sd != '' ) {
	$visibility_sd = explode( ',', $visibility_sd );
	$row_class[]   = ( esc_attr( $visibility_sd[0] ) != '' ) ? ' ' . esc_attr( $visibility_sd[0] ) . '-sd' : '';
}

if ( $visibility_xsd != '' ) {
	$visibility_xsd = explode( ',', $visibility_xsd );
	$row_class[]    = ( esc_attr( $visibility_xsd[0] ) != '' ) ? ' ' . esc_attr( $visibility_xsd[0] ) . '-xsd' : '';
}

$style = ( $style != '' ) ? $style : '';

$delay          = ( $delay != '' ) ? ' data-delay=' . $delay . '' : '';
$delay_offset   = ( $delay_offset != '' ) ? ' data-offset=' . $delay_offset . '' : '';
$animation      = ( $animation != '' ) ? ' data-animate=' . $animation . '' : '';
$delay_duration = ( $delay_duration != '' ) ? ' data-duration=' . $delay_duration . '' : '';

switch ( $width ) {
	case '1/1' :
		$width = 'ok-md-12';
		break;
	case '1/2' :
		$width = 'ok-md-6';
		break;
	case '1/3' :
		$width = 'ok-md-4';
		break;
	case '2/3' :
		$width = 'ok-md-8';
		break;
	case '1/4' :
		$width = 'ok-md-3';
		break;
	case '3/4' :
		$width = 'ok-md-9';
		break;
	case '1/6' :
		$width = 'ok-md-2';
		break;
	case '5/6' :
		$width = 'ok-md-10';
		break;
	case '5/12' :
		$width = 'ok-md-5';
		break;
	case '7/12' :
		$width = 'ok-md-7';
		break;
	default:
		$width = 'ok-md-12';
		break;

}

/** BEGIN - translate construction **/
if ( ( $translate_x != '0' && $translate_x != '' ) || ( $translate_y != '0' && $translate_y != '' ) ) {
	switch ( $translate_x ) {
		case 1:
			$row_class[] = 'translate_x_1';
			break;
		case 2:
			$row_class[] = 'translate_x_2';
			break;
		case 3:
			$row_class[] = 'translate_x_3';
			break;
		case - 1:
			$row_class[] = 'translate_x_neg_1';
			break;
		case - 2:
			$row_class[] = 'translate_x_neg_2';
			break;
		case - 3:
			$row_class[] = 'translate_x_neg_3';
			break;
	}

	switch ( $translate_y ) {
		case 1:
			$row_class[] = 'translate_y_1';
			break;
		case 2:
			$row_class[] = 'translate_y_2';
			break;
		case 3:
			$row_class[] = 'translate_y_3';
			break;
		case - 1:
			$row_class[] = 'translate_y_neg_1';
			break;
		case - 2:
			$row_class[] = 'translate_y_neg_2';
			break;
		case - 3:
			$row_class[] = 'translate_y_neg_3';
			break;
	}

	if ( $translate_x_fixed === 'true' ) {
		$row_class[] = 'translate_x_fixed';
	}
	if ( $translate_y_fixed === 'true' ) {
		$row_class[] = 'translate_y_fixed';
	}
}
if ( $dima_z_index !== '0' && $dima_z_index !== '' ) {
	$row_class[] = 'z_index_' . str_replace( '-', 'neg_', $dima_z_index );
}
/** END - translate construction **/

$row_class[] = $width;

$class = esc_attr( trim( implode( ' ', $row_class ) ) );
//$class       = dima_helper::dima_remove_white_space( esc_attr( trim( implode( ' ', $row_class ) ) ) );
$output = "<div {$id} class=\"{$class}\" style=\"{$style}\" {$animation}{$delay}{$delay_offset}{$delay_duration}>" . do_shortcode( $content ) . "</div>";

echo $output;