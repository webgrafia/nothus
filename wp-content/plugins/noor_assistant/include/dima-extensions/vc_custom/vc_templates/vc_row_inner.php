<?php
$section_content_classes = $id = $class = $bg_image = $style = $no_padding = $border = $bg_class = $css = $translate_x_fixed = $translate_x = $translate_y = $translate_y_fixed = $dima_z_index = '';;

extract( shortcode_atts( array(
	'id'                => '',
	'style'             => '',
	'class'             => '',
	'di_bg_image'       => '',
	'no_margin'         => false,
	'dark'              => false,
	'add_shadow'        => false,
	'di_bg_video'       => '',
	'section_content'   => '',
	'parallax'          => false,
	'parallax_start'    => '0%',
	'parallax_center'   => '50%',
	'parallax_end'      => '100%',
	'equal_height'      => false,
	'bg_type'           => '',
	'dima_canvas_style' => '',
	'dima_canvas_color' => '',

	'cover'       => false,
	'cover_color' => '',
	'bg_gradient' => 'false',
	'gr_end'      => '',
	'gr_opacity'  => '0.6',

	'css'             => '',
	'border_color'    => '',
	'border_style'    => '',
	'back_repeat'     => '',
	'back_attachment' => 'scroll',
	'back_position'   => 'center center',
	'back_size'       => '',

	'translate_x_fixed' => '',
	'translate_x'       => '',
	'translate_y'       => '',
	'translate_y_fixed' => '',
	'dima_z_index'      => '',
), $atts, 'section_row_inner' ) );
$row_cont_classes = array();
$uniq_id          = uniqid();

$demo        = dima_helper::dima_get_template();
$id          = ( $id != '' ) ? 'id="' . esc_attr( $id ) . '"' : '';
$row_class[] = ( $class != '' ) ? esc_attr( $class ) . ' ' : '';
$no_margin   = ( $no_margin == 'true' ) ? 'ok-no-margin' : '';
$di_bg_image = ( $di_bg_image != '' ) ? '' . esc_attr( $di_bg_image ) . '' : '';
$style       = ( $style != '' ) ? 'style="' . $style . '"' : '';
$bg_class    = ' background-image-hide';
$dark        = ( $dark == 'true' ) ? ' dark-bg' : '';

$row_class[] = ( $add_shadow == 'true' ) ? 'box-with-shadow' : '';
$row_class[] = ( $equal_height == 'true' ) ? ' dima-equal' : '';
$row_class[] = 'section';
$IMG_ID      = $di_bg_image;
$row_class[] = 'page-section-' . $uniq_id . ' ';

/*------------------------------*/
# Gradient Overlay Orientation
/*------------------------------*/
if ( $cover == 'true' && $bg_gradient == 'false' ) {

	$el       = '.page-section-' . $uniq_id . ' .dima-color-mask-' . $uniq_id . '';
	$cover    = '<div class="dima-section-cover background-image-holder dima-color-mask-' . $uniq_id . '"></div>';
	$gr_start = $cover_color;

	$horizontal = "background: " . $gr_start . ";";
	DIMA_Style::dima_addCSS( $el . '{'
	                         . $horizontal
	                         . '}', $uniq_id );

} elseif ( $bg_gradient != 'false' ) {
	$el       = '.page-section-' . $uniq_id . ' .dima-color-mask-' . $uniq_id . '';
	$vertical = $horizontal = $left_top = $left_bottom = $radial = '';
	$cover    = '<div class="dima-section-cover background-image-holder dima-color-mask-' . $uniq_id . '"></div>';

	$gr_start = dima_is_gradient_stop_transparent( $cover_color ) ? 'transparent' : $cover_color;
	$gr_end   = dima_is_gradient_stop_transparent( $gr_end ) ? 'transparent' : $gr_end;

	if ( $bg_gradient == 'vertical' ) {
		$vertical = "
            background: " . $gr_start . "; /* Old browsers */
            background: -moz-linear-gradient(top,  " . $gr_start . " 0%, " . $gr_end . " 100%); /* FF3.6+ */
            background: -webkit-gradient(linear, left top, left bottom, color-stop(0%," . $gr_start . "), color-stop(100%," . $gr_end . ")); /* Chrome,Safari4+ */
            background: -webkit-linear-gradient(top,  " . $gr_start . " 0%," . $gr_end . " 100%); /* Chrome10+,Safari5.1+ */
            background: -o-linear-gradient(top,  " . $gr_start . " 0%," . $gr_end . " 100%); /* Opera 11.10+ */
            background: -ms-linear-gradient(top,  " . $gr_start . " 0%," . $gr_end . " 100%); /* IE10+ */
            background: linear-gradient(to bottom,  " . $gr_start . " 0%," . $gr_end . " 100%); /* W3C */
        ";
	}

	if ( $bg_gradient == 'horizontal' ) {
		$horizontal = "
            background: " . $gr_start . "; /* Old browsers */
            background: -moz-linear-gradient(left,  " . $gr_start . " 0%, " . $gr_end . " 100%); /* FF3.6+ */
            background: -webkit-gradient(linear, left top, right top, color-stop(0%," . $gr_start . "), color-stop(100%," . $gr_end . ")); /* Chrome,Safari4+ */
            background: -webkit-linear-gradient(left,  " . $gr_start . " 0%," . $gr_end . " 100%); /* Chrome10+,Safari5.1+ */
            background: -o-linear-gradient(left,  " . $gr_start . " 0%," . $gr_end . " 100%); /* Opera 11.10+ */
            background: -ms-linear-gradient(left,  " . $gr_start . " 0%," . $gr_end . " 100%); /* IE10+ */
            background: linear-gradient(to right,  " . $gr_start . " 0%," . $gr_end . " 100%); /* W3C */
        ";
	}

	if ( $bg_gradient == 'left_top' ) {
		$left_top = "
            background: " . $gr_start . "; /* Old browsers */
            background: -moz-linear-gradient(-45deg,  " . $gr_start . " 0%, " . $gr_end . " 100%); /* FF3.6+ */
            background: -webkit-gradient(linear, left top, right bottom, color-stop(0%," . $gr_start . "), color-stop(100%," . $gr_end . ")); /* Chrome,Safari4+ */
            background: -webkit-linear-gradient(-45deg,  " . $gr_start . " 0%," . $gr_end . " 100%); /* Chrome10+,Safari5.1+ */
            background: -o-linear-gradient(-45deg,  " . $gr_start . " 0%," . $gr_end . " 100%); /* Opera 11.10+ */
            background: -ms-linear-gradient(-45deg,  " . $gr_start . " 0%," . $gr_end . " 100%); /* IE10+ */
            background: linear-gradient(135deg,  " . $gr_start . " 0%," . $gr_end . " 100%); /* W3C */
        ";
	}

	if ( $bg_gradient == 'left_bottom' ) {
		$left_bottom = "
            background: " . $gr_start . "; /* Old browsers */
            background: -moz-linear-gradient(45deg,  " . $gr_start . " 0%, " . $gr_end . " 100%); /* FF3.6+ */
            background: -webkit-gradient(linear, left bottom, right top, color-stop(0%," . $gr_start . "), color-stop(100%," . $gr_end . ")); /* Chrome,Safari4+ */
            background: -webkit-linear-gradient(45deg,  " . $gr_start . " 0%," . $gr_end . " 100%); /* Chrome10+,Safari5.1+ */
            background: -o-linear-gradient(45deg,  " . $gr_start . " 0%," . $gr_end . " 100%); /* Opera 11.10+ */
            background: -ms-linear-gradient(45deg,  " . $gr_start . " 0%," . $gr_end . " 100%); /* IE10+ */
            background: linear-gradient(45deg,  " . $gr_start . " 0%," . $gr_end . " 100%); /* W3C */
        ";
	}

	if ( $bg_gradient == 'radial' ) {
		$radial = "
            background: " . $gr_start . "; /* Old browsers */
            background: -moz-radial-gradient(center, ellipse cover,  " . $gr_start . " 0%, " . $gr_end . " 100%); /* FF3.6+ */
            background: -webkit-gradient(radial, center center, 0px, center center, 100%, color-stop(0%," . $gr_start . "), color-stop(100%," . $gr_end . ")); /* Chrome,Safari4+ */
            background: -webkit-radial-gradient(center, ellipse cover,  " . $gr_start . " 0%," . $gr_end . " 100%); /* Chrome10+,Safari5.1+ */
            background: -o-radial-gradient(center, ellipse cover,  " . $gr_start . " 0%," . $gr_end . " 100%); /* Opera 12+ */
            background: -ms-radial-gradient(center, ellipse cover,  " . $gr_start . " 0%," . $gr_end . " 100%); /* IE10+ */
            background: radial-gradient(ellipse at center,  " . $gr_start . " 0%," . $gr_end . " 100%); /* W3C */
        ";
	}

	$opacity = 'opacity:' . $gr_opacity . ';';

	DIMA_Style::dima_addCSS( $el . '{'
	                         . $vertical
	                         . $horizontal
	                         . $left_top
	                         . $left_bottom
	                         . $radial
	                         . $opacity
	                         . '}', $uniq_id );
}
//--------------------------


if ( $border_color !== '' ) {
	$border = ' style="border-color:' . $border_color . ';border-style: ' . $border_style . ';"';
}
if ( $css !== '' ) {
	$row_cont_classes[] = trim( vc_shortcode_custom_css_class( $css ) );
}
/** BEGIN - translate construction **/
if ( ( $translate_x != '0' && $translate_x != '' ) || ( $translate_y != '0' && $translate_y != '' ) ) {
	switch ( $translate_x ) {
		case 1:
			$row_class[] = 'translate_x_1';
			break;
		case 2:
			$row_class[] = 'translate_x_2';
			break;
		case 3:
			$row_class[] = 'translate_x_3';
			break;
		case - 1:
			$row_class[] = 'translate_x_neg_1';
			break;
		case - 2:
			$row_class[] = 'translate_x_neg_2';
			break;
		case - 3:
			$row_class[] = 'translate_x_neg_3';
			break;
	}

	switch ( $translate_y ) {
		case 1:
			$row_class[] = 'translate_y_1';
			break;
		case 2:
			$row_class[] = 'translate_y_2';
			break;
		case 3:
			$row_class[] = 'translate_y_3';
			break;
		case - 1:
			$row_class[] = 'translate_y_neg_1';
			break;
		case - 2:
			$row_class[] = 'translate_y_neg_2';
			break;
		case - 3:
			$row_class[] = 'translate_y_neg_3';
			break;
	}

	if ( $translate_x_fixed === 'true' ) {
		$row_class[] = 'translate_x_fixed';
	}
	if ( $translate_y_fixed === 'true' ) {
		$row_class[] = 'translate_y_fixed';
	}
}
//if ( $no_padding ) {
$section_content_classes = 'no-padding-section ';
//}

if ( $dima_z_index !== '0' && $dima_z_index !== '' ) {
	$row_class[] = 'z_index_' . str_replace( '-', 'neg_', $z_index );
}
/** END - translate construction **/
$row_class[] = preg_replace( '/\s+/', ' ', apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, implode( ' ', array_filter( $row_cont_classes ) ), $this->settings['base'], $atts ) );

$bg_con = $data_atts = '';
if ( $dima_canvas_style != '' ) {
	$uniqid    = uniqid( 'dima-canvas-' );
	$data_atts .= ' data-canvas-color="' . esc_attr( $dima_canvas_color ) . '"';
	$data_atts .= ' id="' . esc_attr( $uniqid ) . '"';
	$data_atts .= ' data-canvas-id="' . esc_attr( $uniqid ) . '"';
	$data_atts .= ' data-canvas-style="' . $dima_canvas_style . '"';

	if ( $dima_canvas_style == 'canvas_1' ) {
		wp_enqueue_script( 'jquery.particleground' );
	} else {
		wp_enqueue_script( 'dima-particles' );
	}
	$bg_con = '<div class="background-image-holder dima-row-bg-canvas" ' . $data_atts . '></div>';
}

$row_content = '<div class="ok-row ' . $no_margin . '">' . do_shortcode( $content ) . '</div>';

switch ( $section_content ) {
	case 'inside_container':
		$content_global = '<div class="container page-section">'
		                  . $row_content
		                  . '</div>';
		break;

	case 'outside_container':
		$content_global = '<div class="page-section full_width_section">'
		                  . $row_content
		                  . '</div>';
		break;
	default:
		$section_layout = dima_get_section_layout_meta();
		if ( $section_layout == "full-width" ) {
			$content_global = '<div class="container page-section">'
			                  . $row_content
			                  . '</div>';
		} else {
			$content_global = '<div class="page-section">'
			                  . $row_content
			                  . '</div>';
		}

}

if ( is_numeric( $di_bg_image ) ) {
	$bg_image_info = wp_get_attachment_image_src( $di_bg_image, 'full' );
	$di_bg_image   = $bg_image_info[0];
}

switch ( $bg_type ) {
	case 'bg_image':
		if ( ! empty( $di_bg_image ) ) {

			if ( $parallax === 'parallax' ) {
				$back_size = '';
			} else {
				if ( $back_size === '' ) {
					$back_size = 'cover';
				}
			}

			$background = array(
				'background-image'      => $di_bg_image,
				'background-color'      => $cover_color,
				'background-repeat'     => $back_repeat,
				'background-position'   => $back_position,
				'background-size'       => $back_size,
				'background-attachment' => $back_attachment,
			);

			$lazy_data = '';
			if ( DIMA_USE_LAZY ) {
				$lazy_data   = 'data-src="' . $di_bg_image . '"';
				$di_bg_image = wp_get_attachment_image_url( $IMG_ID, 'dima-lazy-image' );
				$bg_class    .= " js-lazy-image-css";
			}

			$back_repeat     = ( isset( $background['background-repeat'] ) && $background['background-repeat'] !== '' ) ? 'background-repeat: ' . $background['background-repeat'] . ';' : '';
			$back_position   = ( isset( $background['background-position'] ) && $background['background-position'] !== '' ) ? 'background-position: ' . $background['background-position'] . ';' : '';
			$back_attachment = ( isset( $background['background-attachment'] ) && $background['background-attachment'] !== '' ) ? 'background-attachment: ' . $background['background-attachment'] . ';' : '';
			$back_size       = ( isset( $background['background-size'] ) && $background['background-size'] !== '' ) ? 'background-size: ' . $background['background-size'] . ';' : '';
			$back_url        = ( $di_bg_image !== '' ) ? 'background-image: url(' . $di_bg_image . ');' : '';

			if ( strpos( $back_repeat, "repeat" ) ) {
				$back_size = "";
			}

			$back_image_style = ( $back_url != '' || $back_repeat != '' || $back_position != '' || $back_attachment != '' || $back_size != '' ) ? ' style="' . $back_url . $back_repeat . $back_position . $back_attachment . $back_size . '"' : '';

			$parallax_start = $parallax_center = $parallax_end = "";
			if ( $parallax == 'parallax' ) {
				$parallax_start  = ( $parallax_start != '' ) ? 'data-parallax-start="' . esc_attr( $parallax_start ) . '"' : '';
				$parallax_center = ( $parallax_center != '' ) ? 'data-parallax-center="' . esc_attr( $parallax_center ) . '"' : '';
				$parallax_end    = ( $parallax_end != '' ) ? 'data-parallax-end="' . esc_attr( $parallax_end ) . '"' : '';
				$bg_class        .= " parallax-background";
			}
			$output = '<div ' . $id . ' class="' . $section_content_classes . 'page-section-content' . $dark . ' overflow-hidden ' . esc_attr( trim( implode( ' ', $row_class ) ) ) . '" ' . $border . '>'
			          . $cover
			          . '<div class="' . $bg_class . '" ' . $parallax_start . $parallax_center . $parallax_end . $back_image_style . '>'
			          . '</div>'
			          . $bg_con
			          . $content_global
			          . '</div>';
		}
		break;
	case 'bg_video':
		if ( ! empty( $di_bg_video ) ) {

			$back_metadata   = dima_media_support::dima_get_metadata( $di_bg_video );
			$back_attributes = dima_media_support::dima_get_thumb_url( $di_bg_video );
			$provider        = dima_media_support::dima_detect_video_service( $di_bg_video );
			$video_orig_w    = $video_orig_h = $video_ratio = 0;

			if ( ! empty( $back_metadata ) ) {
				$video_orig_w = ( $back_metadata->width == 0 ) ? 800 : $back_metadata->width;
				$video_orig_h = ( $back_metadata->height == 0 ) ? 800 : $back_metadata->height;
				$video_ratio  = ( $video_orig_h === 0 ) ? 1.777 : $video_orig_w / $video_orig_h;
			}
			if ( $provider == 'youtube' || $provider == 'vimeo' ) {
				$output = '<div ' . $id . ' class="background-element ' . esc_attr( trim( implode( ' ', $row_class ) ) ) . 'section" >'
				          . $cover
				          . '<div class="' . $section_content_classes . 'page-section-content ' . $dark . ' overflow-hidden" ' . $border . '>'
				          . '<div class="' . $bg_class . ' video dima-video-container" data-ratio="' . $video_ratio . '" data-provider="' . $provider . '" data-video="' . $di_bg_video . '" data-id="' . rand( 10000, 99999 ) . '" data-img-wrap="' . $di_bg_image . '">'
				          . '</div>'
				          . $content_global
				          . '</div>'
				          . '</div>';
			} else {
				wp_enqueue_script( 'video-js' );
				wp_enqueue_script( 'bigvideo-js' );

				$output = '<div ' . $id . ' class="' . esc_attr( trim( implode( ' ', $row_class ) ) ) . 'background-element section"  ' . $border . ' ' . $style . '>'
				          . $cover
				          . '<div class="' . $section_content_classes . esc_attr( trim( implode( ' ', $row_class ) ) ) . ' page-section-content ' . $dark . ' overflow-hidden" ' . $border . '>'
				          . '<div class="' . $bg_class . ' parallax-background video-wrap" data-video-wrap="' . $di_bg_video . '" data-img-wrap="' . $back_attributes . '">'
				          . '</div>'
				          . $content_global
				          . '</div>'
				          . '</div>';

			}
		}
		break;
	default:
		$output = '<div ' . $id . ' class="' . esc_attr( trim( implode( ' ', $row_class ) ) ) . '" ' . $style . '>'
		          . $cover
		          . '<div class="' . $section_content_classes . 'page-section-content ' . $dark . '" ' . $border . ' >'
		          . $content_global
		          . $bg_con
		          . '</div>'
		          . '</div>';

		break;
}


echo $output;