<?php
if ( ! defined( 'DIMA_VERSION' ) ) {
	exit( 'No direct script access allowed' );
}
/**
 * Add Range Option to Visual Composer Params
 *
 * @author      @pixeldima
 * @copyright   PixelDima
 * @link        http://pixeldima.com
 * @since       Version 1.0
 * @package     noor
 */


if ( function_exists( 'dima_add_shortcode_param' ) ) {
	dima_add_shortcode_param( 'range', 'dima_range_settings_field' );
}


function dima_range_settings_field( $settings, $value ) {
	$param_name = isset( $settings['param_name'] ) ? $settings['param_name'] : '';
	$type       = isset( $settings['type'] ) ? $settings['type'] : '';
	$min        = isset( $settings['min'] ) ? $settings['min'] : '';
	$max        = isset( $settings['max'] ) ? $settings['max'] : '';
	$step       = isset( $settings['step'] ) ? $settings['step'] : '';
	$unit       = isset( $settings['unit'] ) ? $settings['unit'] : '';
	$uniqeID    = uniqid( $settings['param_name'] );
	$output     = '';
	$output     .= '<div class="dima-ui-input-slider">';
	$output     .= '<div class="dima-range-input" data-value="' . $value . '" data-min="' . $min . '" data-max="' . $max . '" data-step="' . $step . '" id="rangeInput-' . $uniqeID . '"></div>';
	$output     .= '<input name="' . $param_name . '"  class="range-input-selector wpb_vc_param_value ' . $param_name . ' ' . $type . '" type="text" value="' . $value . '"/><span class="unit">' . $unit . '</span></div>';

	$output .= '<script type="text/javascript">
    (function($) {
            dima_is_range_option("' . $uniqeID . '");
        })(jQuery);    
    </script>';


	return $output;
}