<?php
if ( ! defined( 'DIMA_VERSION' ) ) {
	exit( 'No direct script access allowed' );
}

/**
 * Add Range Option to Visual Composer Params
 *
 * @author      @pixeldima
 * @copyright   PixelDima
 * @link        http://pixeldima.com
 * @since       Version 1.0
 * @package     noor
 */


if ( function_exists( 'dima_add_shortcode_param' ) ) {
	dima_add_shortcode_param( 'visual_selector', 'dima_visual_selector_param_field' );
}


function dima_visual_selector_param_field( $settings, $value ) {
	$param_name = isset( $settings['param_name'] ) ? $settings['param_name'] : '';

	$class   = isset( $settings['class'] ) ? $settings['class'] : '';
	$type    = isset( $settings['type'] ) ? $settings['type'] : '';
	$options = isset( $settings['value'] ) ? $settings['value'] : '';
	$output  = '';
	$uniqeID = uniqid();

	$output .= '<div class="dima-visual-selector ' . $class . '" id="visual-selector' . $uniqeID . '">';
	foreach ( $options as $key => $option ) {
		$output .= '<a href="#" rel="' . $option . '"><img  src="' . DIMA_IMG_DIR . '/admin/' . $key . '" /></a>';
	}
	$output .= '<input name="' . $param_name . '" id="' . $param_name . '" class="wpb_vc_param_value ' . $param_name . ' ' . $type . '" type="hidden" value="' . $value . '"/>';
	$output .= '</div>';

	$output .= '<script type="text/javascript">
    (function($) {
            dima_visual_selector();
        })(jQuery);    
    </script>';

	return $output;
}