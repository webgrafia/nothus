<?php
if ( ! defined( 'DIMA_VERSION' ) ) {
	exit( 'No direct script access allowed' );
}

/**
 * Add Range Option to Visual Composer Params
 *
 * @author      @pixeldima
 * @copyright   PixelDima
 * @link        http://pixeldima.com
 * @since       Version 1.0
 * @package     noor
 */


if ( function_exists( 'dima_add_shortcode_param' ) ) {
	dima_add_shortcode_param( 'toggle', 'dima_toggle_param_field' );
}

function dima_toggle_param_field( $settings, $value ) {
	$param_name = isset( $settings['param_name'] ) ? $settings['param_name'] : '';
	$type       = isset( $settings['type'] ) ? $settings['type'] : '';
	$output     = '';
	$uniqeID    = uniqid( $settings['param_name'] );
	$output     .= '<span class="dima-toggle-button dima-switch-button dima-composer-toggle" id="toggle-switch-' . $uniqeID . '">';
	$output     .= '<span class="toggle-handle button-animation "></span>';
	$output     .= '<input type="hidden" class="wpb_vc_param_value ' . $param_name . ' ' . $type . '" value="' . $value . '" name="' . $param_name . '"/>';
	$output     .= '</span>';

	$output .= '<script type="text/javascript">

    	(function($) {
	       dima_toggle_option("' . $uniqeID . '");
        })(jQuery);

    </script>';

	return $output;
}
