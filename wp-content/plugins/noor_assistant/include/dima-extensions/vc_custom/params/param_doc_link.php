<?php
if (!defined('DIMA_VERSION')) exit('No direct script access allowed');

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! class_exists( 'dima_Doc_Link_Param' ) ) {
	class dima_Doc_Link_Param {
		function __construct() {
			if ( function_exists( 'vc_add_shortcode_param' ) ) {
				vc_add_shortcode_param( 'dima_doc_link_param', array( $this, 'dima_doc_link_param_callback' ) );
			}
		}

		function dima_doc_link_param_callback( $settings, $value ) {
			$param_name   = isset( $settings['param_name'] ) ? $settings['param_name'] : '';
			$class        = isset( $settings['class'] ) ? $settings['class'] : '';
			$doc_link     = isset( $settings['doc_link'] ) ? $settings['doc_link'] : '';
			$video_link   = isset( $settings['video_link'] ) ? $settings['video_link'] : '';
			$example_link = isset( $settings['doc_example'] ) ? $settings['doc_example'] : '';

			$output = '<div class="dima-tutorials-param-wrapper">';

			if ( $doc_link != '' ) {
				$output .= '<div class="dima-documentation-link"><a href="' . esc_html( $doc_link ) . '" target="_blank" rel="noopener">' . esc_html__( 'Theme documentation', 'noor-assistant' ) . '</a></div>';
			}

			if ( $video_link != '' ) {
				$output .= '<div class="dima-video-tutorial-link"><a href="' . esc_html( $video_link ) . '" target="_blank" rel="noopener">' . esc_html__( 'Video tutorial', 'noor-assistant' ) . '</a></div>';
			}

			if ( $example_link != '' ) {
				$output .= '<div class="dima-example-link"><a href="' . esc_html( $example_link ) . '" target="_blank" rel="noopener">' . esc_html__( 'Example', 'noor-assistant' ) . '</a></div>';
			}

			$output .= '</div>';

			return $output;
		}

	}

	$dima_Video_Link_Param = new dima_Doc_Link_Param();
}
