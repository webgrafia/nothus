<?php

/**
 * DIMA Framework
 * WARNING: This file is part of the DIMA Core Framework.
 * Do not edit the core files.
 *
 * @package Dima Framework
 * @subpackage Extensions
 * @version   1.0.0
 * @since     1.0.0
 * @author    PixelDima <info@pixeldima.com>
 */
class DIMA_VC {

	public $dima_animate_list;
	public $dima_animate_list_velocity;
	public $contact_forms = array();
	public $vc_column_width_list;
	public $vc_column_visibility;

	public $id_des;
	public $class_des;
	public $style_des;

	public function __construct() {
		$this->id_des    = esc_html__( '(Optional) Enter a unique ID.', 'noor-assistant' );
		$this->class_des = esc_html__( '(Optional) Enter a unique class name.', 'noor-assistant' );
		$this->style_des = esc_html__( '(Optional) Enter inline CSS.', 'noor-assistant' );
		$cf7             = get_posts( 'post_type="wpcf7_contact_form"&numberposts=-1' );

		if ( $cf7 ) {
			foreach ( $cf7 as $cform ) {
				$this->contact_forms[ $cform->post_title ] = $cform->ID;
			}
		} else {
			$this->contact_forms[ __( 'No contact forms found', 'js_composer' ) ] = 0;
		}

		$this->vc_column_width_list = array(
			esc_html__( 'Inherit from smaller', 'noor-assistant' ) => '',
			esc_html__( '1 column - 1/12', 'noor-assistant' )      => '1',
			esc_html__( '2 columns - 1/6', 'noor-assistant' )      => '2',
			esc_html__( '3 columns - 1/4', 'noor-assistant' )      => '3',
			esc_html__( '4 columns - 1/3', 'noor-assistant' )      => '4',
			esc_html__( '5 columns - 5/12', 'noor-assistant' )     => '5',
			esc_html__( '6 columns - 1/2', 'noor-assistant' )      => '6',
			esc_html__( '7 columns - 7/12', 'noor-assistant' )     => '7',
			esc_html__( '8 columns - 2/3', 'noor-assistant' )      => '8',
			esc_html__( '9 columns - 3/4', 'noor-assistant' )      => '9',
			esc_html__( '10 columns - 5/6', 'noor-assistant' )     => '10',
			esc_html__( '11 columns - 11/12', 'noor-assistant' )   => '11',
			esc_html__( '12 columns - 1/1', 'noor-assistant' )     => '12'
		);

		$this->vc_column_visibility = array(
			esc_html__( 'hidden', 'noor-assistant' )  => 'hidden',
			esc_html__( 'visible', 'noor-assistant' ) => 'visible',
		);

		$this->vc_sicon_color = array(
			esc_html__( '- Select Option -', 'noor-assistant' ) => '',
			esc_html__( 'twitter', 'noor-assistant' )           => 'twitter',
			esc_html__( 'facebook', 'noor-assistant' )          => 'facebook',
			esc_html__( 'googleplus', 'noor-assistant' )        => 'googleplus',
			esc_html__( 'pinterest', 'noor-assistant' )         => 'pinterest',
			esc_html__( 'linkedin', 'noor-assistant' )          => 'linkedin',
			esc_html__( 'youtube', 'noor-assistant' )           => 'youtube',
			esc_html__( 'vimeo', 'noor-assistant' )             => 'vimeo',
			esc_html__( 'tumblr', 'noor-assistant' )            => 'tumblr',
			esc_html__( 'instagram', 'noor-assistant' )         => 'instagram',
			esc_html__( 'flickr', 'noor-assistant' )            => 'flickr',
			esc_html__( 'dribbble', 'noor-assistant' )          => 'dribbble',
			esc_html__( 'quora', 'noor-assistant' )             => 'quora',
			esc_html__( 'foursquare', 'noor-assistant' )        => 'foursquare',
			esc_html__( 'forrst', 'noor-assistant' )            => 'forrst',
			esc_html__( 'vk', 'noor-assistant' )                => 'vk',
			esc_html__( 'wordpress', 'noor-assistant' )         => 'wordpress',
			esc_html__( 'stumbleupon', 'noor-assistant' )       => 'stumbleupon',
			esc_html__( 'yahoo', 'noor-assistant' )             => 'yahoo',
			esc_html__( 'blogger', 'noor-assistant' )           => 'blogger',
			esc_html__( 'soundcloud', 'noor-assistant' )        => 'soundcloud',
		);

		$this->dima_animate_list = array(
			esc_html__( '- Select Option -', 'noor-assistant' ) => '',
			'bounce'                                            => 'bounce',
			'flash'                                             => 'flash',
			'pulse'                                             => 'pulse',
			'rubberBand'                                        => 'rubberBand',
			'shake'                                             => 'shake',
			'headShake'                                         => 'headShake',
			'swing'                                             => 'swing',
			'tada'                                              => 'tada',
			'wobble'                                            => 'wobble',
			'jello'                                             => 'jello',
			'bounceIn'                                          => 'bounceIn',
			'bounceInDown'                                      => 'bounceInDown',
			'bounceInLeft'                                      => 'bounceInLeft',
			'bounceInRight'                                     => 'bounceInRight',
			'bounceInUp'                                        => 'bounceInUp',
			'bounceOut'                                         => 'bounceOut',
			'bounceOutDown'                                     => 'bounceOutDown',
			'bounceOutLeft'                                     => 'bounceOutLeft',
			'bounceOutRight'                                    => 'bounceOutRight',
			'bounceOutUp'                                       => 'bounceOutUp',
			'fadeIn'                                            => 'fadeIn',
			'fadeInDown'                                        => 'fadeInDown',
			'fadeInDownBig'                                     => 'fadeInDownBig',
			'fadeInLeft'                                        => 'fadeInLeft',
			'fadeInLeftBig'                                     => 'fadeInLeftBig',
			'fadeInRight'                                       => 'fadeInRight',
			'fadeInRightBig'                                    => 'fadeInRightBig',
			'fadeInUp'                                          => 'fadeInUp',
			'fadeInUpBig'                                       => 'fadeInUpBig',
			'fadeOut'                                           => 'fadeOut',
			'fadeOutDown'                                       => 'fadeOutDown',
			'fadeOutDownBig'                                    => 'fadeOutDownBig',
			'fadeOutLeft'                                       => 'fadeOutLeft',
			'fadeOutLeftBig'                                    => 'fadeOutLeftBig',
			'fadeOutRight'                                      => 'fadeOutRight',
			'fadeOutRightBig'                                   => 'fadeOutRightBig',
			'fadeOutUp'                                         => 'fadeOutUp',
			'fadeOutUpBig'                                      => 'fadeOutUpBig',
			'flipInX'                                           => 'flipInX',
			'flipInY'                                           => 'flipInY',
			'flipOutX'                                          => 'flipOutX',
			'flipOutY'                                          => 'flipOutY',
			'lightSpeedIn'                                      => 'lightSpeedIn',
			'lightSpeedOut'                                     => 'lightSpeedOut',
			'rotateIn'                                          => 'rotateIn',
			'rotateInDownLeft'                                  => 'rotateInDownLeft',
			'rotateInDownRight'                                 => 'rotateInDownRight',
			'rotateInUpLeft'                                    => 'rotateInUpLeft',
			'rotateInUpRight'                                   => 'rotateInUpRight',
			'rotateOut'                                         => 'rotateOut',
			'rotateOutDownLeft'                                 => 'rotateOutDownLeft',
			'rotateOutDownRight'                                => 'rotateOutDownRight',
			'rotateOutUpLeft'                                   => 'rotateOutUpLeft',
			'rotateOutUpRight'                                  => 'rotateOutUpRight',
			'hinge'                                             => 'hinge',
			'rollIn'                                            => 'rollIn',
			'rollOut'                                           => 'rollOut',
			'zoomIn'                                            => 'zoomIn',
			'zoomInDown'                                        => 'zoomInDown',
			'zoomInLeft'                                        => 'zoomInLeft',
			'zoomInRight'                                       => 'zoomInRight',
			'zoomInUp'                                          => 'zoomInUp',
			'zoomOut'                                           => 'zoomOut',
			'zoomOutDown'                                       => 'zoomOutDown',
			'zoomOutLeft'                                       => 'zoomOutLeft',
			'zoomOutRight'                                      => 'zoomOutRight',
			'zoomOutUp'                                         => 'zoomOutUp',
			'slideInDown'                                       => 'slideInDown',
			'slideInLeft'                                       => 'slideInLeft',
			'slideInRight'                                      => 'slideInRight',
			'slideInUp'                                         => 'slideInUp',
			'slideOutDown'                                      => 'slideOutDown',
			'slideOutLeft'                                      => 'slideOutLeft',
			'slideOutRight'                                     => 'slideOutRight',
			'slideOutUp'                                        => 'slideOutUp'
		);

		$this->dima_animate_list_velocity = array(
			esc_html__( 'No Animation', 'noor-assistant' )      => '',
			esc_html__( 'Fade In', 'noor-assistant' )           => 'transition.fadeIn',
			esc_html__( 'Flip Horizontally', 'noor-assistant' ) => 'transition.flipXIn',
			esc_html__( 'Flip Vertically', 'noor-assistant' )   => 'transition.flipYIn',

			esc_html__( 'Flip Bounce Horizontally', 'noor-assistant' ) => 'transition.flipBounceXIn',
			esc_html__( 'Flip Bounce Vertically', 'noor-assistant' )   => 'transition.flipBounceYIn',

			esc_html__( 'Swoop', 'noor-assistant' )  => 'transition.swoopIn',
			esc_html__( 'Whirl', 'noor-assistant' )  => 'transition.whirlIn',
			esc_html__( 'Shrink', 'noor-assistant' ) => 'transition.shrinkIn',
			esc_html__( 'Expand', 'noor-assistant' ) => 'transition.expandIn',

			esc_html__( 'Bounce', 'noor-assistant' )       => 'transition.bounceIn',
			esc_html__( 'Bounce Up', 'noor-assistant' )    => 'transition.bounceUpIn',
			esc_html__( 'Bounce Down', 'noor-assistant' )  => 'transition.bounceDownIn',
			esc_html__( 'Bounce Left', 'noor-assistant' )  => 'transition.bounceLeftIn',
			esc_html__( 'Bounce Right', 'noor-assistant' ) => 'transition.bounceRightIn',

			esc_html__( 'Slide Up', 'noor-assistant' )    => 'transition.slideUpIn',
			esc_html__( 'Slide Down', 'noor-assistant' )  => 'transition.slideDownIn',
			esc_html__( 'Slide Left', 'noor-assistant' )  => 'transition.slideLeftIn',
			esc_html__( 'Slide Right', 'noor-assistant' ) => 'transition.slideRightIn',

			esc_html__( 'Slide Up Big', 'noor-assistant' )    => 'transition.slideUpBigIn',
			esc_html__( 'Slide Down Big', 'noor-assistant' )  => 'transition.slideDownBigIn',
			esc_html__( 'Slide Right Big', 'noor-assistant' ) => 'transition.slideLeftBigIn',
			esc_html__( 'Slide Left Big', 'noor-assistant' )  => 'transition.slideRightBigIn',

			esc_html__( 'Perspective Up', 'noor-assistant' )    => 'transition.perspectiveUpIn',
			esc_html__( 'Perspective Down', 'noor-assistant' )  => 'transition.perspectiveDownIn',
			esc_html__( 'Perspective Right', 'noor-assistant' ) => 'transition.perspectiveLeftIn',
			esc_html__( 'Perspective Left', 'noor-assistant' )  => 'transition.perspectiveRightIn',
		);
		add_action( 'admin_init', array( $this, 'dima_vc_update_row' ) );
		add_action( 'admin_init', array( $this, 'dima_vc_update_row_inner' ) );
		add_action( 'admin_init', array( $this, 'dima_vc_update_column' ) );
		add_action( 'admin_init', array( $this, 'dima_vc_update_column_inner' ) );
		add_action( 'admin_init', array( $this, 'dima_vc_update_existing_shortcodes' ) );
		add_action( 'admin_init', array( $this, 'dima_vc_update_widget' ) );
	}

	public function dima_target_param_list() {
		return array(
			__( 'Same window', 'noor-assistant' ) => '_self',
			__( 'New window', 'noor-assistant' )  => '_blank',
		);
	}

	function dima_vc_update_existing_shortcodes() {

		vc_map_update( 'vc_raw_html', array(
			'name'        => esc_html__( 'Raw HTML', 'noor-assistant' ),
			'weight'      => 939,
			'icon'        => 'raw-html',
			'category'    => esc_html__( 'Content', 'noor-assistant' ),
			'description' => esc_html__( 'Output raw HTML code on your page', 'noor-assistant' )
		) );

		/**
		 * vc_raw_js
		 */
		vc_map_update( 'vc_raw_js', array(
			'name'        => esc_html__( 'Raw JavaScript', 'noor-assistant' ),
			'weight'      => 938,
			'icon'        => 'raw-js',
			'category'    => esc_html__( 'Content', 'noor-assistant' ),
			'description' => esc_html__( 'Output raw JavaScript code on your page', 'noor-assistant' )
		) );

		/**
		 * Revolution Slider
		 */
		if ( DIMA_REVOLUTION_SLIDER_IS_ACTIVE ) :

			vc_map_update( 'rev_slider_vc', array(
				'name'        => esc_html__( 'Revolution Slider', 'noor-assistant' ),
				'weight'      => 600,
				'icon'        => 'revslider',
				'category'    => esc_html__( 'Slideshow', 'noor-assistant' ),
				'description' => esc_html__( 'Place a Revolution Slider element into your content', 'noor-assistant' )
			) );

			vc_remove_param( 'rev_slider_vc', 'title' );
			vc_remove_param( 'rev_slider_vc', 'el_class' );

		endif;

		/**
		 * contact-form-7
		 */
		if ( DIMA_CONTACT_FORM_7_IS_ACTIVE ) :

			vc_map_update( 'contact-form-7', array(
				'name'        => esc_html__( 'Contact Form 7', 'noor-assistant' ),
				'weight'      => 520,
				'icon'        => 'contact-form-7',
				'category'    => esc_html__( 'Social', 'noor-assistant' ),
				'description' => esc_html__( 'Place one of your contact forms into your content', 'noor-assistant' )
			) );

		endif;

		/**
		 * gravityform
		 */
		if ( DIMA_GRAVITY_FORMS_IS_ACTIVE ) :

			$param_gf_forms_value = array( '- Select Option -' => '' );
			$forms                = RGFormsModel::get_forms( null, 'title' );
			foreach ( $forms as $form ) {
				$param_gf_forms_value[ $form->title ] = $form->id;
			}

			vc_map(
				array(
					'base'        => 'gravityform',
					'name'        => esc_html__( 'Gravity Form', 'noor-assistant' ),
					'weight'      => 600,
					'class'       => 'dima-vc-element dima-vc-element-gravity-form',
					'icon'        => 'gravity-form',
					'category'    => esc_html__( 'Social', 'noor-assistant' ),
					'description' => esc_html__( 'Place one of your Gravity Forms into your content', 'noor-assistant' ),
					'params'      => array(
						array(
							'param_name'  => 'id',
							'heading'     => 'Form',
							'description' => esc_html__( 'Select which form you would like to display.', 'noor-assistant' ),
							'type'        => 'dropdown',
							'holder'      => 'div',
							'value'       => $param_gf_forms_value
						),
						array(
							'param_name'  => 'title',
							'heading'     => esc_html__( 'Disable Title', 'noor-assistant' ),
							'description' => esc_html__( 'Select to disable the title of your form.', 'noor-assistant' ),
							'type'        => 'checkbox',
							'holder'      => 'div',
							'value'       => array(
								'' => 'false'
							)
						),
						array(
							'param_name'  => 'description',
							'heading'     => esc_html__( 'Disable Description', 'noor-assistant' ),
							'description' => esc_html__( 'Select to disable the description of your form.', 'noor-assistant' ),
							'type'        => 'checkbox',
							'holder'      => 'div',
							'value'       => array(
								'' => 'false'
							)
						),
						array(
							'param_name'  => 'ajax',
							'heading'     => esc_html__( 'Enable AJAX', 'noor-assistant' ),
							'description' => esc_html__( 'Select to enable the AJAX functionality of your form.', 'noor-assistant' ),
							'type'        => 'checkbox',
							'holder'      => 'div',
							'value'       => array(
								'' => 'true'
							)
						)
					)
				)
			);

		endif;
	}

	function dima_vc_gradient_overlay( $shotcode = 'vc_row' ) {
		/*------------------------------*/
		# Row Gradient Overlay
		/*------------------------------*/
		vc_add_param( $shotcode, array(
			'param_name'  => 'cover',
			'group'       => esc_html__( 'Style', 'noor-assistant' ),
			'heading'     => esc_html__( 'Cover', 'noor-assistant' ),
			'description' => esc_html__( 'Select to activate the cover above the background patterns and images.', 'noor-assistant' ),
			'type'        => 'toggle',
			'holder'      => 'div',
			'value'       => 'false'
		) );

		vc_add_param( $shotcode, array(
			"type"        => "dropdown",
			"heading"     => __( "Gradient Overlay Orientation", "noor-assistant" ),
			'group'       => esc_html__( 'Style', 'noor-assistant' ),
			"param_name"  => "bg_gradient",
			"width"       => 150,
			"value"       => array(
				__( '-- No Gradient --', "noor-assistant" ) => "false",
				__( 'Vertical ↓', "noor-assistant" )        => "vertical",
				__( 'Horizontal →', "noor-assistant" )      => "horizontal",
				__( 'Diagonal ↘', "noor-assistant" )        => "left_top",
				__( 'Diagonal ↗', "noor-assistant" )        => "left_bottom",
				__( 'Radial ○', "noor-assistant" )          => "radial",
			),
			"description" => __( "Choose the orientation of gradient overlay", "noor-assistant" ),
			'dependency'  => array(
				'element' => 'cover',
				'value'   => 'true',
			),
		) );
		vc_add_param( $shotcode, array(
			"type"             => "colorpicker",
			"heading"          => __( "Overlay Color Start", "noor-assistant" ),
			'group'            => esc_html__( 'Style', 'noor-assistant' ),
			"param_name"       => "cover_color",
			"value"            => "",
			'dependency'       => array(
				'element' => 'cover',
				'value'   => 'true',
			),
			"description"      => __( "Primary overlay color. Start color if used with gradient option selected.", "noor-assistant" ),
			'edit_field_class' => 'no-border-bottom vc_column vc_col-sm-6'
		) );
		vc_add_param( $shotcode, array(
			"type"             => "colorpicker",
			"heading"          => __( "Overlay Color End", "noor-assistant" ),
			'group'            => esc_html__( 'Style', 'noor-assistant' ),
			"param_name"       => "gr_end",
			"value"            => "",
			"description"      => __( "The ending color for gradient fill overlay. Use only with gradient option selected.", "noor-assistant" ),
			"dependency"       => array(
				'element' => "bg_gradient",
				'value'   => array(
					"vertical",
					"horizontal",
					"left_top",
					"left_bottom",
					"radial"
				)
			),
			'edit_field_class' => 'no-border-bottom vc_column vc_col-sm-6'
		) );
		vc_add_param( $shotcode, array(
			"type"        => "range",
			"heading"     => __( "Overlay Color Mask Opacity", "noor-assistant" ),
			'group'       => esc_html__( 'Style', 'noor-assistant' ),
			"param_name"  => "gr_opacity",
			"value"       => "0.6",
			"min"         => "0",
			"max"         => "1",
			"step"        => "0.1",
			"unit"        => 'alpha',
			"dependency"  => array(
				'element' => "bg_gradient",
				'value'   => array(
					"vertical",
					"horizontal",
					"left_top",
					"left_bottom",
					"radial"
				)
			),
			"description" => __( "The opacity of overlay layer which you set above. ", "noor-assistant" )
		) );
	}

	function dima_vc_shape_divider( $shotcode = 'vc_row' ) {
		/*------------------------------*/
		# Shape DividerTop
		/*------------------------------*/
		vc_add_param( $shotcode, array(
				"type"       => "toggle",
				"heading"    => __( "Has Top Shape Divider", "noor-assistant" ),
				"param_name" => "has_top_shape_divider",
				"value"      => "false",
				"group"      => __( 'Shape Divider', 'noor-assistant' ),
			)
		);

		vc_add_param( $shotcode, array(
			"heading"    => __( "Choose a Shape Pattern", 'noor-assistant' ),
			"param_name" => "top_shape_style",
			"class"      => 'shape-selector',
			"group"      => __( 'Shape Divider', 'noor-assistant' ),
			"value"      => array(
				'shape/big-wave-bottom.png'    => "big-wave-top",
				'shape/peaked-bottom.png'      => "peaked-top",
				'shape/small-waves-bottom.png' => "small-waves-top",
				'shape/diagonal-bottom.png'    => "diagonal-top",
				'shape/drop-bottom.png'        => "drop-top"
			),
			"type"       => "visual_selector"
		) );

		vc_add_param( $shotcode, array(
			"type"       => "dropdown",
			"heading"    => __( "Shape Size", "noor-assistant" ),
			"param_name" => "top_shape_size",
			"group"      => __( 'Shape Divider', 'noor-assistant' ),
			"value"      => array(
				"Big"   => "big",
				"Small" => "small"
			)
		) );
		vc_add_param( $shotcode, array(
			"type"       => "colorpicker",
			"heading"    => __( "Shape Color", "noor-assistant" ),
			"param_name" => "top_shape_color",
			"group"      => __( 'Shape Divider', 'noor-assistant' ),
			"value"      => '#fff',
		) );
		vc_add_param( $shotcode, array(
			"type"       => "colorpicker",
			"heading"    => __( "Background Color", "noor-assistant" ),
			"param_name" => "top_shape_bg_color",
			"group"      => __( 'Shape Divider', 'noor-assistant' ),
			"value"      => '',
		) );
		vc_add_param( $shotcode, array(
			"type"        => "textfield",
			"heading"     => __( "Extra class name", "noor-assistant" ),
			"param_name"  => "top_shape_el_class",
			"group"       => __( 'Shape Divider', 'noor-assistant' ),
			"value"       => "",
			"description" => __( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in Custom CSS Shortcode or Masterkey Custom CSS option.", "noor-assistant" )
		) );

		/*------------------------------*/
		# Shape DividerBottom
		/*------------------------------*/
		vc_add_param( $shotcode, array(
				"type"       => "toggle",
				"heading"    => __( "Has Bottom Shape Divider", "noor-assistant" ),
				"param_name" => "has_bottom_shape_divider",
				"value"      => "false",
				"group"      => __( 'Shape Divider', 'noor-assistant' ),
			)
		);

		vc_add_param( $shotcode, array(
			"heading"    => __( "Choose a Shape Pattern", 'noor-assistant' ),
			"param_name" => "bottom_shape_style",
			"class"      => 'Shape-selector',
			"group"      => __( 'Shape Divider', 'noor-assistant' ),
			"value"      => array(
				'shape/big-wave-top.png'    => "big-wave-bottom",
				'shape/peaked-top.png'      => "peaked-bottom",
				'shape/small-waves-top.png' => "small-waves-bottom",
				'shape/diagonal-top.png'    => "diagonal-bottom",
				'shape/drop-top.png'        => "drop-bottom"
			),
			"type"       => "visual_selector"
		) );

		vc_add_param( $shotcode, array(
			"type"       => "dropdown",
			"heading"    => __( "Shape Size", "noor-assistant" ),
			"param_name" => "bottom_shape_size",
			"group"      => __( 'Shape Divider', 'noor-assistant' ),
			"value"      => array(
				"Big"   => "big",
				"Small" => "small"
			)
		) );
		vc_add_param( $shotcode, array(
			"type"       => "colorpicker",
			"heading"    => __( "Shape Color", "noor-assistant" ),
			"param_name" => "bottom_shape_color",
			"group"      => __( 'Shape Divider', 'noor-assistant' ),
			"value"      => '#fff',
		) );
		vc_add_param( $shotcode, array(
			"type"       => "colorpicker",
			"heading"    => __( "Background Color", "noor-assistant" ),
			"param_name" => "bottom_shape_bg_color",
			"group"      => __( 'Shape Divider', 'noor-assistant' ),
			"value"      => '',
		) );
		vc_add_param( $shotcode, array(
			"type"        => "textfield",
			"heading"     => __( "Extra class name", "noor-assistant" ),
			"param_name"  => "bottom_shape_el_class",
			"group"       => __( 'Shape Divider', 'noor-assistant' ),
			"value"       => "",
			"description" => __( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in Custom CSS Shortcode or Masterkey Custom CSS option.", "noor-assistant" )
		) );

	}

	function dima_vc_translate( $shotcode = 'vc_row' ) {
		vc_add_param( $shotcode, array(
			"type"             => "range",
			'param_name'       => 'translate_y',
			'group'            => esc_html__( 'Translate', 'noor-assistant' ),
			'heading'          => esc_html__( 'Translate Y-Axis', 'noor-assistant' ),
			'description'      => esc_html__( 'Set how much the element has to Translate in the Y axis.', 'noor-assistant' ),
			"value"            => "0",
			"min"              => "-3",
			"max"              => "3",
			"step"             => "1",
			"unit"             => 'X',
			'edit_field_class' => 'no-border-bottom vc_column vc_col-sm-6'
		) );

		vc_add_param( $shotcode, array(
			'type'             => 'toggle',
			'param_name'       => 'translate_y_fixed',
			'heading'          => esc_html__( 'Translate Y-Axis Fixed', 'noor-assistant' ),
			'description'      => esc_html__( 'Turn Off Translate-Y Responsiveness', 'noor-assistant' ),
			'group'            => esc_html__( 'Translate', 'noor-assistant' ),
			'holder'           => 'div',
			'value'            => 'false',
			'edit_field_class' => 'no-border-bottom vc_column vc_col-sm-6 crum_vc '

		) );

		vc_add_param( $shotcode, array(
			'param_name'       => 'translate_x',
			'heading'          => esc_html__( 'Translate X-Axis', 'noor-assistant' ),
			'description'      => esc_html__( 'Set how much the element has to Translate in the X axis.', 'noor-assistant' ),
			'group'            => esc_html__( 'Translate', 'noor-assistant' ),
			'type'             => 'range',
			'save_always'      => true,
			"value"            => "0",
			"min"              => "-3",
			"max"              => "3",
			"step"             => "1",
			"unit"             => 'X',
			'edit_field_class' => 'no-border-bottom vc_column vc_col-sm-6'
		) );

		vc_add_param( $shotcode, array(
			'param_name'       => 'translate_x_fixed',
			'type'             => 'toggle',
			'group'            => esc_html__( 'Translate', 'noor-assistant' ),
			'heading'          => esc_html__( 'Translate X-Axis Fixed', 'noor-assistant' ),
			'description'      => esc_html__( 'Turn Off Translate-X Responsiveness', 'noor-assistant' ),
			'holder'           => 'false',
			'edit_field_class' => 'no-border-bottom vc_column vc_col-sm-6 crum_vc',
		) );

		vc_add_param( $shotcode, array(
			'param_name'       => 'dima_z_index',
			'type'             => 'range',
			'group'            => esc_html__( 'Translate', 'noor-assistant' ),
			'heading'          => esc_html__( 'Translate Z-Axis', 'noor-assistant' ),
			'description'      => esc_html__( 'Set how much the element has to Translate in the Z axis.', 'noor-assistant' ),
			'save_always'      => true,
			"value"            => "0",
			"min"              => "0",
			"max"              => "10",
			"step"             => "1",
			"unit"             => 'X',
			'holder'           => 'div',
			'edit_field_class' => 'no-border-bottom vc_column vc_col-sm-6'
		) );
	}

	public function dima_vc_update_row() {

		vc_map_update( 'vc_row', array(
			'name'        => esc_html__( 'Section Row', 'noor-assistant' ),
			'weight'      => 1000,
			'class'       => 'dima-vc-element dima-vc-element-section-row',
			'icon'        => 'section-row',
			'category'    => esc_html__( 'Structure', 'noor-assistant' ),
			'description' => esc_html__( 'Place and structure your shortcodes inside of a row', 'noor-assistant' ),
			'js_view'     => 'DimaRowView'
		) );
		if ( function_exists( 'vc_remove_param' ) ) {
			vc_remove_param( 'vc_row', 'columns_placement' );
			vc_remove_param( 'vc_row', 'parallax_speed_video' );
			vc_remove_param( 'vc_row', 'parallax_speed_bg' );
			vc_remove_param( 'vc_row', 'gap' );
			vc_remove_param( 'vc_row', 'equal_height' );
			vc_remove_param( 'vc_row', 'bg_color' );
			vc_remove_param( 'vc_row', 'font_color' );
			vc_remove_param( 'vc_row', 'padding' );
			vc_remove_param( 'vc_row', 'margin_bottom' );
			vc_remove_param( 'vc_row', 'bg_image' );
			vc_remove_param( 'vc_row', 'bg_image_repeat' );
			vc_remove_param( 'vc_row', 'el_class' );
			vc_remove_param( 'vc_row', 'full_width' );
			vc_remove_param( 'vc_row', 'full_height' );
			vc_remove_param( 'vc_row', 'content_placement' );
			vc_remove_param( 'vc_row', 'parallax' );
			vc_remove_param( 'vc_row', 'parallax_image' );
			vc_remove_param( 'vc_row', 'el_id' );
			vc_remove_param( 'vc_row', 'video_bg' );
			vc_remove_param( 'vc_row', 'video_bg_url' );
			vc_remove_param( 'vc_row', 'video_bg_parallax' );
			vc_remove_param( 'vc_row', 'css_animation' );
			vc_remove_param( 'vc_row', 'css' );
		}
		if ( function_exists( 'vc_remove_param' ) ) {
			vc_add_param( 'vc_row', array(
				'param_name'  => 'no_margin',
				'heading'     => esc_html__( 'Marginless Columns', 'noor-assistant' ),
				'description' => esc_html__( 'Check to remove the spacing between columns.', 'noor-assistant' ),
				'type'        => 'toggle',
				'holder'      => 'div',
				'value'       => 'false'
			) );

			vc_add_param( 'vc_row', array(
				'heading'     => esc_html__( 'Section Content', 'noor-assistant' ),
				'description' => esc_html__( 'Choose your Section Content [ Default, Inside Container, Outside Container ]', 'noor-assistant' ),
				"param_name"  => "section_content",
				"class"       => 'shape-selector',
				"value"       => array(
					'inherit.png'           => "",
					'inside-container.png'  => "inside_container",
					'outside-container.png' => "outside_container",
				),
				"type"        => "visual_selector"
			) );

			vc_add_param( 'vc_row', array(
				'param_name'  => 'no_padding',
				'heading'     => esc_html__( 'No Padding', 'noor-assistant' ),
				'description' => esc_html__( 'Check to remove the spacing Padding.', 'noor-assistant' ),
				'type'        => 'toggle',
				'holder'      => 'div',
				'value'       => 'false'
			) );

			vc_add_param( 'vc_row', array(
				'param_name'  => 'id',
				'heading'     => esc_html__( 'ID', 'noor-assistant' ),
				'description' => $this->id_des,
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			) );

			vc_add_param( 'vc_row', array(
				'param_name'  => 'class',
				'heading'     => esc_html__( 'Class', 'noor-assistant' ),
				'description' => $this->class_des,
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div'
			) );
			vc_add_param( 'vc_row', array(
				'param_name'  => 'dark',
				'group'       => esc_html__( 'Style', 'noor-assistant' ),
				'heading'     => esc_html__( 'Dark Background', 'noor-assistant' ),
				'description' => esc_html__( 'Check if you will work in dark background section (This will affect elements style)', 'noor-assistant' ),
				'type'        => 'toggle',
				'holder'      => 'div',
				'value'       => 'false'
			) );

			vc_add_param( 'vc_row', array(
				'param_name'  => 'equal_height',
				'group'       => esc_html__( 'Style', 'noor-assistant' ),
				'heading'     => esc_html__( 'Columns with equal height', 'noor-assistant' ),
				'description' => esc_html__( 'Check this to have columns that are all equally tall, matching the height of the tallest', 'noor-assistant' ),
				'type'        => 'toggle',
				'holder'      => 'div',
				'value'       => 'false'
			) );

			vc_add_param( 'vc_row', array(
				'param_name'  => 'bg_type',
				'group'       => esc_html__( 'Style', 'noor-assistant' ),
				'heading'     => esc_html__( 'Background Style', 'noor-assistant' ),
				'description' => esc_html__( 'Select background type.', 'noor-assistant' ),
				'type'        => 'dropdown',
				'save_always' => true,
				'value'       => array(
					esc_html__( '- Select Option -', 'noor-assistant' ) => '',
					esc_html__( 'Image', 'noor-assistant' )             => 'bg_image',
					esc_html__( 'Video', 'noor-assistant' )             => 'bg_video',
				),
				'holder'      => 'div',
			) );

			vc_add_param( 'vc_row', array(
				'param_name'  => 'di_bg_image',
				'group'       => esc_html__( 'Style', 'noor-assistant' ),
				'heading'     => esc_html__( 'Background Image', 'noor-assistant' ),
				'description' => esc_html__( 'Upload a background image to your content band (this will overwrite your Background Pattern).', 'noor-assistant' ),
				'type'        => 'attach_image',
				'dependency'  => array(
					'element' => 'bg_type',
					'value'   => 'bg_image',
				),
				'holder'      => 'div'
			) );

			vc_add_param( 'vc_row', array(
				'param_name'  => 'di_bg_video',
				'group'       => esc_html__( 'Style', 'noor-assistant' ),
				'heading'     => esc_html__( 'Background Video Path', 'noor-assistant' ),
				'description' => esc_html__( 'Include the path to your background video.', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'dependency'  => array(
					'element' => 'bg_type',
					'value'   => 'bg_video',
				),
				'holder'      => 'div'
			) );

			/* > Canvas*/
			vc_add_param( 'vc_row', array(
				'param_name'  => 'dima_canvas_style',
				'group'       => esc_html__( 'Style', 'noor-assistant' ),
				'heading'     => esc_html__( 'Canvas Style', 'noor-assistant' ),
				'description' => esc_html__( 'Choose the canvas style.', 'noor-assistant' ),
				'type'        => 'dropdown',
				'save_always' => true,
				'value'       => array(
					esc_html__( '- Select Option -', 'noor-assistant' ) => '',
					esc_html__( 'Network One', 'noor-assistant' )       => 'canvas_1',
					esc_html__( 'Network Two', 'noor-assistant' )       => 'default',
					esc_html__( 'Bubble', 'noor-assistant' )            => 'bubble',
					esc_html__( 'Nasa', 'noor-assistant' )              => 'nasa',
					esc_html__( 'Snow', 'noor-assistant' )              => 'snow',
				),
				'holder'      => 'div',
			) );

			vc_add_param( 'vc_row', array(
				'param_name'  => 'dima_canvas_color',
				'group'       => esc_html__( 'Style', 'noor-assistant' ),
				'heading'     => esc_html__( 'canvas Color', 'noor-assistant' ),
				'description' => esc_html__( 'Specify an canvas color.', 'noor-assistant' ),
				'type'        => 'colorpicker',
				'dependency'  => array(
					'element' => 'dima_canvas_style',
					'value'   => array( 'canvas_1' )
				),
				'value'       => 'rgba(255, 255, 255, 0.2)',
				'holder'      => 'div'
			) );

			/* ! Canvas*/

			vc_add_param( 'vc_row', array(
				'param_name'  => 'back_repeat',
				'group'       => esc_html__( 'Style', 'noor-assistant' ),
				'heading'     => esc_html__( 'background repeat', 'noor-assistant' ),
				'description' => wp_kses( __( 'Define the background repeat. <a href=\'http://www.w3schools.com/cssref/pr_background-repeat.asp\' target=\'_blank\'>Check this for reference</a>', 'noor-assistant' ), array(
					'a' => array(
						'href'   => array(),
						'target' => array()
					)
				) ),
				'type'        => 'dropdown',
				'holder'      => 'div',
				'value'       => array(
					esc_html__( 'background-repeat', 'noor-assistant' )   => '',
					esc_html__( 'No Repeat', 'noor-assistant' )           => 'no-repeat',
					esc_html__( 'Repeat All', 'noor-assistant' )          => 'repeat',
					esc_html__( 'Repeat Horizontally', 'noor-assistant' ) => 'repeat-x',
					esc_html__( 'Repeat Vertically', 'noor-assistant' )   => 'repeat-y',
					esc_html__( 'Inherit', 'noor-assistant' )             => 'inherit'
				),
				'dependency'  => array(
					'element' => 'bg_type',
					'value'   => array( 'bg_video', 'bg_image' ),
				),

			) );

			vc_add_param( 'vc_row', array(
				'param_name'  => 'back_attachment',
				'group'       => esc_html__( 'Style', 'noor-assistant' ),
				'heading'     => esc_html__( 'Background Attachment', 'noor-assistant' ),
				'description' => wp_kses( __( "Define the background attachment. <a href='http://www.w3schools.com/cssref/pr_background-attachment.asp' target='_blank'>Check this for reference</a>", 'noor-assistant' ),
					array(
						'a' => array(
							'href'   => array(),
							'target' => array()
						)
					) ),
				'type'        => 'dropdown',
				'holder'      => 'div',
				'std'         => 'scroll',
				'value'       => array(
					esc_html__( 'background-attachement', 'noor-assistant' ) => '',
					esc_html__( 'Fixed', 'noor-assistant' )                  => 'fixed',
					esc_html__( 'Scroll', 'noor-assistant' )                 => 'scroll',
					esc_html__( 'Inherit', 'noor-assistant' )                => 'inherit'
				),
				'dependency'  => array(
					'element' => 'bg_type',
					'value'   => array( 'bg_video', 'bg_image' ),
				),
			) );

			vc_add_param( 'vc_row', array(
				'param_name'  => 'back_position',
				'group'       => esc_html__( 'Style', 'noor-assistant' ),
				'heading'     => esc_html__( 'Background Position', 'noor-assistant' ),
				'description' => wp_kses( __( "Define the background position. <a href='http://www.w3schools.com/cssref/pr_background-position.asp' target='_blank'>Check this for reference</a>", 'noor-assistant' ),
					array(
						'a' => array(
							'href'   => array(),
							'target' => array()
						)
					) ),
				'type'        => 'dropdown',
				'holder'      => 'div',
				'value'       => array(
					esc_html__( 'background-position', 'noor-assistant' ) => '',
					esc_html__( 'Left Top', 'noor-assistant' )            => 'left top',
					esc_html__( 'Left Center', 'noor-assistant' )         => 'left center',
					esc_html__( 'Left Bottom', 'noor-assistant' )         => 'left bottom',
					esc_html__( 'Center Top', 'noor-assistant' )          => 'center top',
					esc_html__( 'Center Center', 'noor-assistant' )       => 'center center',
					esc_html__( 'Center Bottom', 'noor-assistant' )       => 'center bottom',
					esc_html__( 'Right Top', 'noor-assistant' )           => 'right top',
					esc_html__( 'Right Center', 'noor-assistant' )        => 'right center',
					esc_html__( 'Right Bottom', 'noor-assistant' )        => 'right bottom'
				),
				'dependency'  => array(
					'element' => 'bg_type',
					'value'   => array( 'bg_video', 'bg_image' ),
				),

			) );

			vc_add_param( 'vc_row', array(
				'param_name'  => 'back_size',
				'group'       => esc_html__( 'Style', 'noor-assistant' ),
				'heading'     => esc_html__( 'Background size', 'noor-assistant' ),
				'description' => wp_kses( __( "Define the background size (Default value is 'cover'). <a href='http://www.w3schools.com/cssref/css3_pr_background-size.asp' target='_blank'>Check this for reference</a>", 'noor-assistant' ),
					array(
						'a' => array(
							'href'   => array(),
							'target' => array()
						)
					) ),
				'type'        => 'textfield',
				'holder'      => 'div',
				'dependency'  => array(
					'element' => 'bg_type',
					'value'   => array( 'bg_video', 'bg_image' ),
				),
			) );

			vc_add_param( 'vc_row', array(
				'param_name'  => 'parallax',
				'group'       => esc_html__( 'Style', 'noor-assistant' ),
				'heading'     => esc_html__( 'Parallax', 'noor-assistant' ),
				'description' => esc_html__( 'Select to activate the parallax effect with background patterns and images.', 'noor-assistant' ),
				'type'        => 'dropdown',
				'holder'      => 'div',
				'dependency'  => array(
					'element' => 'bg_type',
					'value'   => array( 'bg_image', 'bg_video' )
				),
				'value'       => array(
					esc_html__( 'Off', 'noor-assistant' ) => '',
					esc_html__( 'On', 'noor-assistant' )  => 'parallax',
				)
			) );

			vc_add_param( 'vc_row', array(
				'param_name'  => 'parallax_start',
				'group'       => esc_html__( 'Style', 'noor-assistant' ),
				'heading'     => esc_html__( 'Parallax Start', 'noor-assistant' ),
				'description' => esc_html__( '% or px', 'noor-assistant' ),
				'type'        => 'textfield',
				'holder'      => 'div',
				'dependency'  => array(
					'element' => 'parallax',
					'value'   => array( 'parallax' )
				),
				'value'       => '0%'
			) );

			vc_add_param( 'vc_row', array(
				'param_name'  => 'parallax_center',
				'group'       => esc_html__( 'Style', 'noor-assistant' ),
				'heading'     => esc_html__( 'Parallax Center', 'noor-assistant' ),
				'description' => esc_html__( '% or px', 'noor-assistant' ),
				'type'        => 'textfield',
				'holder'      => 'div',
				'dependency'  => array(
					'element' => 'parallax',
					'value'   => array( 'parallax' )
				),
				'value'       => '50%'
			) );

			vc_add_param( 'vc_row', array(
				'param_name'  => 'parallax_end',
				'group'       => esc_html__( 'Style', 'noor-assistant' ),
				'heading'     => esc_html__( 'Parallax End', 'noor-assistant' ),
				'description' => esc_html__( '% or px', 'noor-assistant' ),
				'type'        => 'textfield',
				'holder'      => 'div',
				'dependency'  => array(
					'element' => 'parallax',
					'value'   => array( 'parallax' )
				),
				'value'       => '100%'
			) );

			self::dima_vc_gradient_overlay();
			self::dima_vc_shape_divider();

			/*------------------------------*/
			# Design Options
			/*------------------------------*/
			vc_add_param( 'vc_row', array(
				'type'       => 'css_editor',
				'group'      => __( 'Design Options', 'noor-assistant' ),
				'heading'    => __( 'CSS box', 'noor-assistant' ),
				'param_name' => 'css',
			) );

			vc_add_param( 'vc_row', array(
				'param_name'  => 'border_style',
				'heading'     => esc_html__( 'Border style.', 'noor-assistant' ),
				'description' => esc_html__( 'Specify a border style', 'noor-assistant' ),
				'type'        => 'dropdown',
				'holder'      => 'div',
				'group'       => __( 'Design Options', 'noor-assistant' ),
				'value'       => array(
					esc_html__( 'Hidden', 'noor-assistant' ) => 'hidden',
					esc_html__( 'Dashed', 'noor-assistant' ) => 'dashed',
					esc_html__( 'Solid', 'noor-assistant' )  => 'solid',
					esc_html__( 'Double', 'noor-assistant' ) => 'double',
					esc_html__( 'Groove', 'noor-assistant' ) => 'groove',
					esc_html__( 'Ridge', 'noor-assistant' )  => 'ridge',
					esc_html__( 'Inset', 'noor-assistant' )  => 'inset',
					esc_html__( 'Outset', 'noor-assistant' ) => 'outset',
				),
			) );

			vc_add_param( 'vc_row', array(
				'param_name'  => 'border_color',
				'heading'     => esc_html__( 'Border Color', 'noor-assistant' ),
				'description' => esc_html__( 'Specify a border color.', 'noor-assistant' ),
				'save_always' => true,
				'group'       => __( 'Design Options', 'noor-assistant' ),
				'type'        => 'colorpicker',
				'holder'      => 'div'
			) );

			//----------------
			self::dima_vc_translate( 'vc_row' );
			//----------------

			//**************Animation***************//
			vc_add_param( 'vc_row', array(
				'param_name'  => 'animation',
				'group'       => esc_html__( 'Animation', 'noor-assistant' ),
				'heading'     => esc_html__( 'Animation', 'noor-assistant' ),
				'description' => esc_html__( 'Select the type of animation you want to use.', 'noor-assistant' ),
				'type'        => 'dropdown',
				'holder'      => 'div',
				'value'       => $this->dima_animate_list_velocity,
			) );

			vc_add_param( 'animate_item', array(
					'param_name'  => 'delay',
					'group'       => esc_html__( 'Animation', 'noor-assistant' ),
					'heading'     => esc_html__( 'Animate item', 'noor-assistant' ),
					'description' => '',
					'save_always' => true,
					'type'        => 'textfield',
					'holder'      => 'div',
					'value'       => ''
				)
			);

			vc_add_param( 'vc_row', array(
					'param_name'  => 'animate_item',
					'group'       => esc_html__( 'Animation', 'noor-assistant' ),
					'heading'     => esc_html__( 'Delay Animation', 'noor-assistant' ),
					'description' => esc_html__( 'Set when the animation start (milliseconds)', 'noor-assistant' ),
					'save_always' => true,
					'type'        => 'textfield',
					'holder'      => 'div',
					'value'       => ''
				)
			);

			vc_add_param( 'vc_row', array(
				'param_name'  => 'delay_duration',
				'group'       => esc_html__( 'Animation', 'noor-assistant' ),
				'heading'     => esc_html__( 'duration Animation', 'noor-assistant' ),
				'description' => esc_html__( 'Animation duration of each element  (milliseconds).', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div',
				'value'       => ''
			) );

			vc_add_param( 'vc_row', array(
				'param_name'  => 'delay_offset',
				'group'       => esc_html__( 'Animation', 'noor-assistant' ),
				'heading'     => esc_html__( 'Delay Animation Offset', 'noor-assistant' ),
				'description' => esc_html__( 'Set the number ( px,%..) from the top of the viewport where the animation start.', 'noor-assistant' ) . "<a href='http://imakewebthings.com/waypoints/api/offset-option/' target='_blank' >" . esc_html__( 'Read More', 'noor-assistant' ) . "</a>",
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div',
				'value'       => ''
			) );
			//**************!Animation***************//

			vc_add_param( 'vc_row', array(
				'param_name'  => 'tutorials',
				'type'        => 'dima_doc_link_param',
				'heading'     => '<span class="tippy dima-help dima-vc-toolip">' . dima_get_svg_icon( "ic_help" )
				                 . '<span class="dima-vc-tooltip-text">'
				                 . esc_html__( 'Video tutorial and theme documentation article', 'noor-assistant' ) . '</span></span>'
				                 . esc_html__( 'Tutorial', 'noor-assistant' ),
				'doc_link'    => '',
				'video_link'  => 'https://www.youtube.com/watch?v=7secfm8eTQo',
				'doc_example' => 'https://noor.pixeldima.com/sections/',
			) );
		}
	}

	public function dima_vc_update_row_inner() {

		vc_map_update( 'vc_row_inner', array(
			'name'        => esc_html__( 'Section Row Inner', 'noor-assistant' ),
			'weight'      => 1000,
			'class'       => 'dima-vc-element dima-vc-element-section-row',
			'icon'        => 'section-row',
			'category'    => esc_html__( 'Structure', 'noor-assistant' ),
			'description' => esc_html__( 'Place and structure your shortcodes inside of a row', 'noor-assistant' ),
			'js_view'     => 'DimaRowView'
		) );

		vc_remove_param( 'vc_row_inner', 'gap' );
		vc_remove_param( 'vc_row_inner', 'equal_height' );
		vc_remove_param( 'vc_row_inner', 'bg_color' );
		vc_remove_param( 'vc_row_inner', 'font_color' );
		vc_remove_param( 'vc_row_inner', 'padding' );
		vc_remove_param( 'vc_row_inner', 'margin_bottom' );
		vc_remove_param( 'vc_row_inner', 'bg_image' );
		vc_remove_param( 'vc_row_inner', 'bg_image_repeat' );
		vc_remove_param( 'vc_row_inner', 'el_class' );
		vc_remove_param( 'vc_row_inner', 'full_width' );
		vc_remove_param( 'vc_row_inner', 'full_height' );
		vc_remove_param( 'vc_row_inner', 'content_placement' );
		vc_remove_param( 'vc_row_inner', 'video_bg' );
		vc_remove_param( 'vc_row_inner', 'video_bg_url' );
		vc_remove_param( 'vc_row_inner', 'video_bg_parallax' );
		vc_remove_param( 'vc_row_inner', 'parallax' );
		vc_remove_param( 'vc_row_inner', 'parallax_image' );
		vc_remove_param( 'vc_row_inner', 'el_id' );
		vc_remove_param( 'vc_row_inner', 'disable_element' );
		vc_remove_param( 'vc_row_inner', 'css' );

		vc_add_param( 'vc_row_inner', array(
			'param_name'  => 'no_margin',
			'heading'     => esc_html__( 'Marginless Columns', 'noor-assistant' ),
			'description' => esc_html__( 'Check to remove the spacing between columns.', 'noor-assistant' ),
			'type'        => 'checkbox',
			'holder'      => 'div',
			'value'       => array(
				'' => 'true'
			)
		) );

		vc_add_param( 'vc_row_inner', array(
			'param_name'  => 'section_content',
			'heading'     => esc_html__( 'Section Content', 'noor-assistant' ),
			'description' => esc_html__( 'Choose your section width', 'noor-assistant' ),
			'type'        => 'dropdown',
			'holder'      => 'div',
			'value'       => array(
				esc_html__( 'Default', 'noor-assistant' ) => '',
				'Inside Container'                        => 'inside_container',
				'Outside Container'                       => 'outside_container',
			)
		) );

		vc_add_param( 'vc_row_inner', array(
			'param_name'  => 'id',
			'heading'     => esc_html__( 'ID', 'noor-assistant' ),
			'description' => $this->id_des,
			'save_always' => true,
			'type'        => 'textfield',
			'holder'      => 'div'
		) );

		vc_add_param( 'vc_row_inner', array(
			'param_name'  => 'class',
			'heading'     => esc_html__( 'Class', 'noor-assistant' ),
			'description' => $this->class_des,
			'save_always' => true,
			'type'        => 'textfield',
			'holder'      => 'div'
		) );

		vc_add_param( 'vc_row_inner', array(
			'param_name'  => 'style',
			'heading'     => esc_html__( 'Style', 'noor-assistant' ),
			'description' => $this->style_des,
			'save_always' => true,
			'type'        => 'textarea',
			'holder'      => 'div'
		) );

		vc_add_param( 'vc_row_inner', array(
			'param_name'  => 'dark',
			'group'       => esc_html__( 'Style', 'noor-assistant' ),
			'heading'     => esc_html__( 'Dark Background', 'noor-assistant' ),
			'description' => esc_html__( 'Check if you will work in dark background section (This will affect elements style)', 'noor-assistant' ),
			'type'        => 'checkbox',
			'holder'      => 'div',
			'value'       => array(
				'' => 'true'
			)
		) );

		vc_add_param( 'vc_row_inner', array(
			'param_name'  => 'add_shadow',
			'group'       => esc_html__( 'Style', 'noor-assistant' ),
			'heading'     => esc_html__( 'Add Shadow', 'noor-assistant' ),
			'description' => '',
			'type'        => 'checkbox',
			'holder'      => 'div',
			'value'       => array(
				'' => 'true'
			)
		) );

		vc_add_param( 'vc_row_inner', array(
			'param_name'  => 'equal_height',
			'group'       => esc_html__( 'Style', 'noor-assistant' ),
			'heading'     => esc_html__( 'Columns with equal height', 'noor-assistant' ),
			'description' => esc_html__( 'Check this to have columns that are all equally tall, matching the height of the tallest', 'noor-assistant' ),
			'type'        => 'checkbox',
			'holder'      => 'div',
			'value'       => array(
				'' => 'true'
			)
		) );

		vc_add_param( 'vc_row_inner', array(
			'param_name'  => 'bg_type',
			'group'       => esc_html__( 'Style', 'noor-assistant' ),
			'heading'     => esc_html__( 'Background Type', 'noor-assistant' ),
			'description' => esc_html__( 'Select background type.', 'noor-assistant' ),
			'type'        => 'dropdown',
			'save_always' => true,
			'value'       => array(
				esc_html__( '- Select Option -', 'noor-assistant' ) => '',
				esc_html__( 'Image', 'noor-assistant' )             => 'bg_image',
				esc_html__( 'Video', 'noor-assistant' )             => 'bg_video',
			),
			'holder'      => 'div',
		) );

		vc_add_param( 'vc_row_inner', array(
			'param_name'  => 'di_bg_image',
			'group'       => esc_html__( 'Style', 'noor-assistant' ),
			'heading'     => esc_html__( 'Background Image', 'noor-assistant' ),
			'description' => esc_html__( 'Upload a background image to your content band (this will overwrite your Background Pattern).', 'noor-assistant' ),
			'type'        => 'attach_image',
			'dependency'  => array(
				'element' => 'bg_type',
				'value'   => 'bg_image',
			),
			'holder'      => 'div'
		) );

		vc_add_param( 'vc_row_inner', array(
			'param_name'  => 'di_bg_video',
			'group'       => esc_html__( 'Style', 'noor-assistant' ),
			'heading'     => esc_html__( 'Background Video Path', 'noor-assistant' ),
			'description' => esc_html__( 'Include the path to your background video.', 'noor-assistant' ),
			'save_always' => true,
			'type'        => 'textfield',
			'dependency'  => array(
				'element' => 'bg_type',
				'value'   => 'bg_video',
			),
			'holder'      => 'div'
		) );

		/* > Canvas*/
		vc_add_param( 'vc_row_inner', array(
			'param_name'  => 'dima_canvas_style',
			'group'       => esc_html__( 'Style', 'noor-assistant' ),
			'heading'     => esc_html__( 'Canvas Style', 'noor-assistant' ),
			'description' => esc_html__( 'Choose the canvas style.', 'noor-assistant' ),
			'type'        => 'dropdown',
			'save_always' => true,
			'value'       => array(
				esc_html__( '- Select Option -', 'noor-assistant' ) => '',
				esc_html__( 'Network One', 'noor-assistant' )       => 'canvas_1',
				esc_html__( 'Network Two', 'noor-assistant' )       => 'default',
				esc_html__( 'Bubble', 'noor-assistant' )            => 'bubble',
				esc_html__( 'Nasa', 'noor-assistant' )              => 'nasa',
				esc_html__( 'Snow', 'noor-assistant' )              => 'snow',
			),
			'holder'      => 'div',
		) );

		vc_add_param( 'vc_row_inner', array(
			'param_name'  => 'dima_canvas_color',
			'group'       => esc_html__( 'Style', 'noor-assistant' ),
			'heading'     => esc_html__( 'canvas Color', 'noor-assistant' ),
			'description' => esc_html__( 'Specify an canvas color.', 'noor-assistant' ),
			'type'        => 'colorpicker',
			'dependency'  => array(
				'element' => 'dima_canvas_style',
				'value'   => array( 'canvas_1' )
			),
			'value'       => 'rgba(255, 255, 255, 0.2)',
			'holder'      => 'div'
		) );
		/* ! Canvas*/

		self::dima_vc_gradient_overlay( 'vc_row_inner' );

		vc_add_param( 'vc_row_inner', array(
			'param_name'  => 'back_repeat',
			'group'       => esc_html__( 'Style', 'noor-assistant' ),
			'heading'     => esc_html__( 'background repeat', 'noor-assistant' ),
			'description' => wp_kses( __( 'Define the background repeat. <a href=\'http://www.w3schools.com/cssref/pr_background-repeat.asp\' target=\'_blank\'>Check this for reference</a>', 'noor-assistant' ), array(
				'a' => array(
					'href'   => array(),
					'target' => array()
				)
			) ),
			'type'        => 'dropdown',
			'holder'      => 'div',
			'value'       => array(
				esc_html__( 'background-repeat', 'noor-assistant' )   => '',
				esc_html__( 'No Repeat', 'noor-assistant' )           => 'no-repeat',
				esc_html__( 'Repeat All', 'noor-assistant' )          => 'repeat',
				esc_html__( 'Repeat Horizontally', 'noor-assistant' ) => 'repeat-x',
				esc_html__( 'Repeat Vertically', 'noor-assistant' )   => 'repeat-y',
				esc_html__( 'Inherit', 'noor-assistant' )             => 'inherit'
			),
			'dependency'  => array(
				'element' => 'bg_type',
				'value'   => array( 'bg_video', 'bg_image' ),
			),
		) );

		vc_add_param( 'vc_row_inner', array(
			'param_name'  => 'back_attachment',
			'group'       => esc_html__( 'Style', 'noor-assistant' ),
			'heading'     => esc_html__( 'Background Attachment', 'noor-assistant' ),
			'description' => wp_kses( __( "Define the background attachment. <a href='http://www.w3schools.com/cssref/pr_background-attachment.asp' target='_blank'>Check this for reference</a>", 'noor-assistant' ),
				array(
					'a' => array(
						'href'   => array(),
						'target' => array()
					)
				) ),
			'type'        => 'dropdown',
			'holder'      => 'div',
			'value'       => array(
				esc_html__( 'background-attachement', 'noor-assistant' ) => '',
				esc_html__( 'Fixed', 'noor-assistant' )                  => 'fixed',
				esc_html__( 'Scroll', 'noor-assistant' )                 => 'scroll',
				esc_html__( 'Inherit', 'noor-assistant' )                => 'inherit'
			),
			'dependency'  => array(
				'element' => 'bg_type',
				'value'   => array( 'bg_video', 'bg_image' ),
			),
		) );

		vc_add_param( 'vc_row_inner', array(
			'param_name'  => 'back_position',
			'group'       => esc_html__( 'Style', 'noor-assistant' ),
			'heading'     => esc_html__( 'Background Position', 'noor-assistant' ),
			'description' => wp_kses( __( "Define the background position. <a href='http://www.w3schools.com/cssref/pr_background-position.asp' target='_blank'>Check this for reference</a>", 'noor-assistant' ),
				array(
					'a' => array(
						'href'   => array(),
						'target' => array()
					)
				) ),
			'type'        => 'dropdown',
			'holder'      => 'div',
			'value'       => array(
				esc_html__( 'background-position', 'noor-assistant' ) => '',
				esc_html__( 'Left Top', 'noor-assistant' )            => 'left top',
				esc_html__( 'Left Center', 'noor-assistant' )         => 'left center',
				esc_html__( 'Left Bottom', 'noor-assistant' )         => 'left bottom',
				esc_html__( 'Center Top', 'noor-assistant' )          => 'center top',
				esc_html__( 'Center Center', 'noor-assistant' )       => 'center center',
				esc_html__( 'Center Bottom', 'noor-assistant' )       => 'center bottom',
				esc_html__( 'Right Top', 'noor-assistant' )           => 'right top',
				esc_html__( 'Right Center', 'noor-assistant' )        => 'right center',
				esc_html__( 'Right Bottom', 'noor-assistant' )        => 'right bottom'
			),
			'dependency'  => array(
				'element' => 'bg_type',
				'value'   => array( 'bg_video', 'bg_image' ),
			),
		) );

		vc_add_param( 'vc_row_inner', array(
			'param_name'  => 'back_size',
			'group'       => esc_html__( 'Style', 'noor-assistant' ),
			'heading'     => esc_html__( 'Background size', 'noor-assistant' ),
			'description' => wp_kses( __( "Define the background size (Default value is 'cover'). <a href='http://www.w3schools.com/cssref/css3_pr_background-size.asp' target='_blank'>Check this for reference</a>", 'noor-assistant' ),
				array(
					'a' => array(
						'href'   => array(),
						'target' => array()
					)
				) ),
			'type'        => 'textfield',
			'holder'      => 'div',
			'dependency'  => array(
				'element' => 'bg_type',
				'value'   => array( 'bg_video', 'bg_image' ),
			),
		) );

		vc_add_param( 'vc_row_inner', array(
			'param_name'  => 'parallax',
			'group'       => esc_html__( 'Style', 'noor-assistant' ),
			'heading'     => esc_html__( 'Parallax', 'noor-assistant' ),
			'description' => esc_html__( 'Select to activate the parallax effect with background patterns and images.', 'noor-assistant' ),
			'type'        => 'dropdown',
			'holder'      => 'div',
			'dependency'  => array(
				'element' => 'bg_type',
				'value'   => array( 'bg_image', 'bg_video' )
			),
			'value'       => array(
				esc_html__( 'Off', 'noor-assistant' ) => '',
				esc_html__( 'On', 'noor-assistant' )  => 'parallax',
			)
		) );

		vc_add_param( 'vc_row_inner', array(
			'param_name'  => 'parallax_start',
			'group'       => esc_html__( 'Style', 'noor-assistant' ),
			'heading'     => esc_html__( 'Parallax Start', 'noor-assistant' ),
			'description' => esc_html__( '% or px', 'noor-assistant' ),
			'type'        => 'textfield',
			'holder'      => 'div',
			'dependency'  => array(
				'element' => 'parallax',
				'value'   => array( 'parallax' )
			),
			'value'       => '0%'
		) );
		vc_add_param( 'vc_row_inner', array(
			'param_name'  => 'parallax_center',
			'group'       => esc_html__( 'Style', 'noor-assistant' ),
			'heading'     => esc_html__( 'Parallax Center', 'noor-assistant' ),
			'description' => esc_html__( '% or px', 'noor-assistant' ),
			'type'        => 'textfield',
			'holder'      => 'div',
			'dependency'  => array(
				'element' => 'parallax',
				'value'   => array( 'parallax' )
			),
			'value'       => '50%'
		) );

		vc_add_param( 'vc_row_inner', array(
			'param_name'  => 'parallax_end',
			'group'       => esc_html__( 'Style', 'noor-assistant' ),
			'heading'     => esc_html__( 'Parallax End', 'noor-assistant' ),
			'description' => esc_html__( '% or px', 'noor-assistant' ),
			'type'        => 'textfield',
			'holder'      => 'div',
			'dependency'  => array(
				'element' => 'parallax',
				'value'   => array( 'parallax' )
			),
			'value'       => '100%'
		) );

		//
		// Design Options
		//
		vc_add_param( 'vc_row_inner', array(
			'type'       => 'css_editor',
			'group'      => __( 'Design Options', 'noor-assistant' ),
			'heading'    => __( 'CSS box', 'noor-assistant' ),
			'param_name' => 'css',
		) );

		vc_add_param( 'vc_row_inner', array(
			'param_name'  => 'border_style',
			'heading'     => esc_html__( 'Border style.', 'noor-assistant' ),
			'description' => esc_html__( 'Specify a border style', 'noor-assistant' ),
			'type'        => 'dropdown',
			'holder'      => 'div',
			'group'       => __( 'Design Options', 'noor-assistant' ),
			'value'       => array(
				esc_html__( 'Hidden', 'noor-assistant' ) => 'hidden',
				esc_html__( 'Dashed', 'noor-assistant' ) => 'dashed',
				esc_html__( 'Solid', 'noor-assistant' )  => 'solid',
				esc_html__( 'Double', 'noor-assistant' ) => 'double',
				esc_html__( 'Groove', 'noor-assistant' ) => 'groove',
				esc_html__( 'Ridge', 'noor-assistant' )  => 'ridge',
				esc_html__( 'Inset', 'noor-assistant' )  => 'inset',
				esc_html__( 'Outset', 'noor-assistant' ) => 'outset',
			),
		) );

		vc_add_param( 'vc_row_inner', array(
			'param_name'  => 'border_color',
			'heading'     => esc_html__( 'Border Color', 'noor-assistant' ),
			'description' => esc_html__( 'Specify a border color.', 'noor-assistant' ),
			'save_always' => true,
			'group'       => __( 'Design Options', 'noor-assistant' ),
			'type'        => 'colorpicker',
			'holder'      => 'div'
		) );

		//
		// Translate
		//
		self::dima_vc_translate( 'vc_row_inner' );
	}

	public function dima_vc_update_column() {
		/**
		 * vc_column
		 */
		vc_map_update( 'vc_column', array(
			'name'        => esc_html__( 'Column', 'noor-assistant' ),
			'weight'      => 1000,
			'class'       => 'dima-vc-element dima-vc-element-section-row',
			'icon'        => 'section-row',
			'category'    => esc_html__( 'Structure', 'noor-assistant' ),
			'description' => esc_html__( 'Place and structure your shortcodes inside of a row', 'noor-assistant' ),
			'js_view'     => 'DimaColumnView'
		) );

		vc_remove_param( 'vc_column', 'parallax_speed_video' );
		vc_remove_param( 'vc_column', 'video_bg_parallax' );
		vc_remove_param( 'vc_column', 'video_bg_url' );
		vc_remove_param( 'vc_column', 'video_bg' );
		vc_remove_param( 'vc_column', 'css_animation' );
		vc_remove_param( 'vc_column', 'width' );
		vc_remove_param( 'vc_column', 'font_color' );
		vc_remove_param( 'vc_column', 'el_class' );
		vc_remove_param( 'vc_column', 'css' );
		vc_remove_param( 'vc_column', 'offset' );
		vc_remove_param( 'vc_column', 'el_id' );

		vc_add_param( 'vc_column', array(
			'param_name'  => 'column_layout',
			'heading'     => esc_html__( 'Column Layout', 'noor-assistant' ),
			'description' => esc_html__( 'This option allow you to add sidebar and specify the main in any page (Note: if this column is "Main on Start" the side column should be "Sidebar on End" and if this column is "Main on End" the side column should be "Sidebar on Start")', 'noor-assistant' ),
			'type'        => 'dropdown',
			'holder'      => 'div',
			'value'       => array(
				esc_html__( 'Inherit', 'noor-assistant' )          => '',
				esc_html__( 'Main on end', 'noor-assistant' )      => 'main_end_content',
				esc_html__( 'Main on start', 'noor-assistant' )    => 'main_start_content',
				esc_html__( 'Sidebar on End', 'noor-assistant' )   => 'end_sidebar',
				esc_html__( 'Sidebar On Start', 'noor-assistant' ) => 'start_sidebar',
			),
		) );

		vc_add_param( 'vc_column', array(
			'param_name'  => 'sticky_sidebar',
			'heading'     => esc_html__( 'Sidebar sticky', 'noor-assistant' ),
			'description' => esc_html__( 'This will make Sidebar sticky (Useful when a sidebar is too tall or too short compared to the rest of the content)', 'noor-assistant' ),
			'type'        => 'checkbox',
			'holder'      => 'div',
			'dependency'  => array(
				'element' => 'column_layout',
				'value'   => array(
					'end_sidebar',
					'start_sidebar'
				)
			),
			'value'       => array( '' => 'true' )
		) );

		vc_add_param( 'vc_column', array(
			'param_name'  => 'id',
			'heading'     => esc_html__( 'ID', 'noor-assistant' ),
			'description' => $this->id_des,
			'save_always' => true,
			'type'        => 'textfield',
			'holder'      => 'div'
		) );

		vc_add_param( 'vc_column', array(
			'param_name'  => 'class',
			'heading'     => esc_html__( 'Class', 'noor-assistant' ),
			'description' => $this->class_des,
			'save_always' => true,
			'type'        => 'textfield',
			'holder'      => 'div'
		) );

		vc_add_param( 'vc_column', array(
			'param_name'  => 'style',
			'heading'     => esc_html__( 'Style', 'noor-assistant' ),
			'description' => $this->style_des,
			'save_always' => true,
			'type'        => 'textfield',
			'holder'      => 'div'
		) );

		//------------------------ Style
		//========================================================

		vc_add_param( 'vc_column', array(
			'param_name'  => 'dark',
			'group'       => esc_html__( 'Style', 'noor-assistant' ),
			'heading'     => esc_html__( 'Dark Background', 'noor-assistant' ),
			'description' => esc_html__( 'Check if you will work in dark background section (This will affect elements style)', 'noor-assistant' ),
			'type'        => 'toggle',
			'holder'      => 'div',
			'value'       => 'false'
		) );

		vc_add_param( 'vc_column', array(
			'param_name'  => 'bg_type',
			'group'       => esc_html__( 'Style', 'noor-assistant' ),
			'heading'     => esc_html__( 'Background Type', 'noor-assistant' ),
			'description' => esc_html__( 'Select background type.', 'noor-assistant' ),
			'type'        => 'dropdown',
			'save_always' => true,
			'value'       => array(
				esc_html__( '- Select Option -', 'noor-assistant' ) => '',
				esc_html__( 'Background Image', 'noor-assistant' )  => 'bg_image',
				esc_html__( 'Background Video', 'noor-assistant' )  => 'bg_video',
			),
			'holder'      => 'div',
		) );

		vc_add_param( 'vc_column', array(
			'param_name'  => 'di_bg_image',
			'group'       => esc_html__( 'Style', 'noor-assistant' ),
			'heading'     => esc_html__( 'Background Image', 'noor-assistant' ),
			'description' => esc_html__( 'Upload a background image to your content band (this will overwrite your Background Pattern).', 'noor-assistant' ),
			'type'        => 'attach_image',
			'dependency'  => array(
				'element' => 'bg_type',
				'value'   => 'bg_image',
			),
			'holder'      => 'div'
		) );

		vc_add_param( 'vc_column', array(
			'param_name'  => 'di_bg_video',
			'group'       => esc_html__( 'Style', 'noor-assistant' ),
			'heading'     => esc_html__( 'Background Video Path', 'noor-assistant' ),
			'description' => esc_html__( 'Include the path to your background video.', 'noor-assistant' ),
			'save_always' => true,
			'type'        => 'textfield',
			'dependency'  => array(
				'element' => 'bg_type',
				'value'   => 'bg_video',
			),
			'holder'      => 'div'
		) );

		/* > Canvas*/
		vc_add_param( 'vc_column', array(
			'param_name'  => 'dima_canvas_style',
			'group'       => esc_html__( 'Style', 'noor-assistant' ),
			'heading'     => esc_html__( 'Canvas Style', 'noor-assistant' ),
			'description' => esc_html__( 'Choose the canvas style.', 'noor-assistant' ),
			'type'        => 'dropdown',
			'save_always' => true,
			'value'       => array(
				esc_html__( '- Select Option -', 'noor-assistant' ) => '',
				esc_html__( 'Network One', 'noor-assistant' )       => 'canvas_1',
				esc_html__( 'Network Two', 'noor-assistant' )       => 'default',
				esc_html__( 'Bubble', 'noor-assistant' )            => 'bubble',
				esc_html__( 'Nasa', 'noor-assistant' )              => 'nasa',
				esc_html__( 'Snow', 'noor-assistant' )              => 'snow',
			),
			'holder'      => 'div',
		) );

		vc_add_param( 'vc_column', array(
			'param_name'  => 'dima_canvas_color',
			'group'       => esc_html__( 'Style', 'noor-assistant' ),
			'heading'     => esc_html__( 'canvas Color', 'noor-assistant' ),
			'description' => esc_html__( 'Specify an canvas color.', 'noor-assistant' ),
			'type'        => 'colorpicker',
			'dependency'  => array(
				'element' => 'dima_canvas_style',
				'value'   => array( 'canvas_1' )
			),
			'value'       => 'rgba(255, 255, 255, 0.2)',
			'holder'      => 'div'
		) );

		/* ! Canvas*/

		vc_add_param( 'vc_column', array(
			'param_name'  => 'back_repeat',
			'group'       => esc_html__( 'Style', 'noor-assistant' ),
			'heading'     => esc_html__( 'background repeat', 'noor-assistant' ),
			'description' => wp_kses( __( 'Define the background repeat. <a href=\'http://www.w3schools.com/cssref/pr_background-repeat.asp\' target=\'_blank\'>Check this for reference</a>', 'noor-assistant' ), array(
				'a' => array(
					'href'   => array(),
					'target' => array()
				)
			) ),
			'type'        => 'dropdown',
			'holder'      => 'div',
			'value'       => array(
				esc_html__( 'background-repeat', 'noor-assistant' )   => '',
				esc_html__( 'No Repeat', 'noor-assistant' )           => 'no-repeat',
				esc_html__( 'Repeat All', 'noor-assistant' )          => 'repeat',
				esc_html__( 'Repeat Horizontally', 'noor-assistant' ) => 'repeat-x',
				esc_html__( 'Repeat Vertically', 'noor-assistant' )   => 'repeat-y',
				esc_html__( 'Inherit', 'noor-assistant' )             => 'inherit'
			)
		) );

		vc_add_param( 'vc_column', array(
			'param_name'  => 'back_attachment',
			'group'       => esc_html__( 'Style', 'noor-assistant' ),
			'heading'     => esc_html__( 'Background Attachment', 'noor-assistant' ),
			'description' => wp_kses( __( "Define the background attachment. <a href='http://www.w3schools.com/cssref/pr_background-attachment.asp' target='_blank'>Check this for reference</a>", 'noor-assistant' ),
				array(
					'a' => array(
						'href'   => array(),
						'target' => array()
					)
				) ),
			'type'        => 'dropdown',
			'holder'      => 'div',
			'dependency'  => array(
				'element' => 'bg_type',
				'value'   => array( 'bg_video', 'bg_image' ),
			),
			'value'       => array(
				esc_html__( 'background-attachement', 'noor-assistant' ) => '',
				esc_html__( 'Fixed', 'noor-assistant' )                  => 'fixed',
				esc_html__( 'Scroll', 'noor-assistant' )                 => 'scroll',
				esc_html__( 'Inherit', 'noor-assistant' )                => 'inherit'
			)
		) );

		vc_add_param( 'vc_column', array(
			'param_name'  => 'back_position',
			'group'       => esc_html__( 'Style', 'noor-assistant' ),
			'heading'     => esc_html__( 'Background Position', 'noor-assistant' ),
			'description' => wp_kses( __( "Define the background position. <a href='http://www.w3schools.com/cssref/pr_background-position.asp' target='_blank'>Check this for reference</a>", 'noor-assistant' ),
				array(
					'a' => array(
						'href'   => array(),
						'target' => array()
					)
				) ),
			'type'        => 'dropdown',
			'holder'      => 'div',
			'dependency'  => array(
				'element' => 'bg_type',
				'value'   => array( 'bg_video', 'bg_image' ),
			),
			'value'       => array(
				esc_html__( 'background-position', 'noor-assistant' ) => '',
				esc_html__( 'Left Top', 'noor-assistant' )            => 'left top',
				esc_html__( 'Left Center', 'noor-assistant' )         => 'left center',
				esc_html__( 'Left Bottom', 'noor-assistant' )         => 'left bottom',
				esc_html__( 'Center Top', 'noor-assistant' )          => 'center top',
				esc_html__( 'Center Center', 'noor-assistant' )       => 'center center',
				esc_html__( 'Center Bottom', 'noor-assistant' )       => 'center bottom',
				esc_html__( 'Right Top', 'noor-assistant' )           => 'right top',
				esc_html__( 'Right Center', 'noor-assistant' )        => 'right center',
				esc_html__( 'Right Bottom', 'noor-assistant' )        => 'right bottom'
			)
		) );

		vc_add_param( 'vc_column', array(
			'param_name'  => 'back_size',
			'group'       => esc_html__( 'Style', 'noor-assistant' ),
			'heading'     => esc_html__( 'Background size', 'noor-assistant' ),
			'description' => wp_kses( __( "Define the background size (Default value is 'cover'). <a href='http://www.w3schools.com/cssref/css3_pr_background-size.asp' target='_blank'>Check this for reference</a>", 'noor-assistant' ),
				array(
					'a' => array(
						'href'   => array(),
						'target' => array()
					)
				) ),
			'dependency'  => array(
				'element' => 'bg_type',
				'value'   => array( 'bg_video', 'bg_image' ),
			),
			'type'        => 'textfield',
			'holder'      => 'div',
		) );

		vc_add_param( 'vc_column', array(
			'param_name'  => 'parallax',
			'group'       => esc_html__( 'Style', 'noor-assistant' ),
			'heading'     => esc_html__( 'Parallax', 'noor-assistant' ),
			'description' => esc_html__( 'Select to activate the parallax effect with background patterns and images.', 'noor-assistant' ),
			'type'        => 'dropdown',
			'holder'      => 'div',
			'dependency'  => array(
				'element' => 'bg_type',
				'value'   => array( 'bg_image', 'bg_video' )
			),
			'value'       => array(
				esc_html__( 'Off', 'noor-assistant' ) => '',
				esc_html__( 'On', 'noor-assistant' )  => 'parallax',
			)
		) );

		vc_add_param( 'vc_column', array(
			'param_name'  => 'parallax_start',
			'group'       => esc_html__( 'Style', 'noor-assistant' ),
			'heading'     => esc_html__( 'Parallax Start', 'noor-assistant' ),
			'description' => esc_html__( '% or px', 'noor-assistant' ),
			'type'        => 'textfield',
			'holder'      => 'div',
			'dependency'  => array(
				'element' => 'parallax',
				'value'   => array( 'parallax' )
			),
			'value'       => '0%'
		) );

		vc_add_param( 'vc_column', array(
			'param_name'  => 'parallax_center',
			'group'       => esc_html__( 'Style', 'noor-assistant' ),
			'heading'     => esc_html__( 'Parallax Center', 'noor-assistant' ),
			'description' => esc_html__( '% or px', 'noor-assistant' ),
			'type'        => 'textfield',
			'holder'      => 'div',
			'dependency'  => array(
				'element' => 'parallax',
				'value'   => array( 'parallax' )
			),
			'value'       => '50%'
		) );

		vc_add_param( 'vc_column', array(
			'param_name'  => 'parallax_end',
			'group'       => esc_html__( 'Style', 'noor-assistant' ),
			'heading'     => esc_html__( 'Parallax End', 'noor-assistant' ),
			'description' => esc_html__( '% or px', 'noor-assistant' ),
			'type'        => 'textfield',
			'holder'      => 'div',
			'dependency'  => array(
				'element' => 'parallax',
				'value'   => array( 'parallax' )
			),
			'value'       => '100%'
		) );

		vc_add_param( 'vc_column', array(
			'param_name'  => 'min_height',
			'group'       => esc_html__( 'Style', 'noor-assistant' ),
			'heading'     => esc_html__( 'Min Height', 'noor-assistant' ),
			'description' => esc_html__( 'Insert the row minimum height in pixel.', 'noor-assistant' ),
			'save_always' => true,
			'type'        => 'textfield',
			'holder'      => 'div',
			'value'       => ''
		) );

		self::dima_vc_gradient_overlay( 'vc_column' );

		//------------------------ !Style

		//
		// Translate
		//
		self::dima_vc_translate( 'vc_column' );

		//1- Extra Large devices Desktops
		vc_add_param( 'vc_column', array(
			'param_name'  => 'xld',
			'heading'     => esc_html__( 'Extra Large devices', 'noor-assistant' ),
			'group'       => esc_html__( 'Responsive Width', 'noor-assistant' ),
			'description' => esc_html__( 'Select column width for Extra Large devices Desktops ( ≥1600px ).', 'noor-assistant' ),
			'type'        => 'dropdown',
			'holder'      => 'div',
			'value'       => $this->vc_column_width_list,
			'std'         => ''
		) );
		//2- Large devices Desktops    
		vc_add_param( 'vc_column', array(
			'param_name'  => 'ld',
			'heading'     => esc_html__( 'Large devices', 'noor-assistant' ),
			'group'       => esc_html__( 'Responsive Width', 'noor-assistant' ),
			'description' => esc_html__( 'Select column width for Large devices Desktops ( ≥1140px ).', 'noor-assistant' ),
			'type'        => 'dropdown',
			'holder'      => 'div',
			'value'       => $this->vc_column_width_list,
			'std'         => ''
		) );

		//4- Small devices    
		vc_add_param( 'vc_column', array(
			'param_name'  => 'sd',
			'heading'     => esc_html__( 'Small devices Tablets', 'noor-assistant' ),
			'group'       => esc_html__( 'Responsive Width', 'noor-assistant' ),
			'description' => esc_html__( 'Select column width for Small devices Tablets ( ≥768px ).', 'noor-assistant' ),
			'type'        => 'dropdown',
			'holder'      => 'div',
			'value'       => $this->vc_column_width_list,
			'std'         => ''
		) );
		//5- Extra small    
		vc_add_param( 'vc_column', array(
			'param_name'  => 'xsd',
			'heading'     => esc_html__( 'Extra small devices Phones', 'noor-assistant' ),
			'group'       => esc_html__( 'Responsive Width', 'noor-assistant' ),
			'description' => esc_html__( 'Select column width for Extra small devices Phones ( <768px ).', 'noor-assistant' ),
			'type'        => 'dropdown',
			'holder'      => 'div',
			'value'       => $this->vc_column_width_list,
			'std'         => '12'
		) );

		//hidden

		//1- Extra Large devices Desktops
		vc_add_param( 'vc_column', array(
			'param_name'  => 'visibility_xld',
			'heading'     => esc_html__( 'Extra Large devices', 'noor-assistant' ),
			'group'       => esc_html__( 'Responsive Visibility', 'noor-assistant' ),
			'description' => esc_html__( 'Choose column visibility for Extra Large devices Desktops ( ≥1600px ).', 'noor-assistant' ),
			'type'        => 'checkbox',
			'holder'      => 'div',
			'value'       => $this->vc_column_visibility,
			'std'         => ''
		) );


		//2- Large devices Desktops    
		vc_add_param( 'vc_column', array(
			'param_name'  => 'visibility_ld',
			'heading'     => esc_html__( 'Large devices', 'noor-assistant' ),
			'group'       => esc_html__( 'Responsive Visibility', 'noor-assistant' ),
			'description' => esc_html__( 'Choose column visibility for Large devices Desktops ( ≥1140px ).', 'noor-assistant' ),
			'type'        => 'checkbox',
			'holder'      => 'div',
			'value'       => $this->vc_column_visibility,
			'std'         => ''
		) );

		//3- Medium Devise    
		vc_add_param( 'vc_column', array(
			'param_name'  => 'visibility_md',
			'heading'     => esc_html__( 'Medium Devise', 'noor-assistant' ),
			'group'       => esc_html__( 'Responsive Visibility', 'noor-assistant' ),
			'description' => esc_html__( 'Choose column visibility for medium devise ( ≥989px ).', 'noor-assistant' ),
			'type'        => 'checkbox',
			'holder'      => 'div',
			'value'       => $this->vc_column_visibility,
			'std'         => ''
		) );

		//4- Small devices    
		vc_add_param( 'vc_column', array(
			'param_name'  => 'visibility_sd',
			'heading'     => esc_html__( 'Small devices', 'noor-assistant' ),
			'group'       => esc_html__( 'Responsive Visibility', 'noor-assistant' ),
			'description' => esc_html__( 'Choose column visibility for Small devices Tablets ( ≥768px ).', 'noor-assistant' ),
			'type'        => 'checkbox',
			'holder'      => 'div',
			'value'       => $this->vc_column_visibility,
			'std'         => ''
		) );

		//5- Extra small    
		vc_add_param( 'vc_column', array(
			'param_name'  => 'visibility_xsd',
			'heading'     => esc_html__( 'Extra small devices', 'noor-assistant' ),
			'group'       => esc_html__( 'Responsive Visibility', 'noor-assistant' ),
			'description' => esc_html__( 'Choose column visibility for Extra small devices Phones ( <768px ).', 'noor-assistant' ),
			'type'        => 'checkbox',
			'holder'      => 'div',
			'value'       => $this->vc_column_visibility,
			'std'         => ''
		) );

		//
		// Design Options
		//
		vc_add_param( 'vc_column', array(
			'type'       => 'css_editor',
			'group'      => __( 'Design Options', 'noor-assistant' ),
			'heading'    => __( 'CSS box', 'noor-assistant' ),
			'param_name' => 'css',
		) );

		vc_add_param( 'vc_column', array(
			'param_name'  => 'border_style',
			'heading'     => esc_html__( 'Border style.', 'noor-assistant' ),
			'description' => esc_html__( 'Specify a border style', 'noor-assistant' ),
			'type'        => 'dropdown',
			'holder'      => 'div',
			'group'       => __( 'Design Options', 'noor-assistant' ),
			'value'       => array(
				esc_html__( 'Hidden', 'noor-assistant' ) => 'hidden',
				esc_html__( 'Dashed', 'noor-assistant' ) => 'dashed',
				esc_html__( 'Solid', 'noor-assistant' )  => 'solid',
				esc_html__( 'Double', 'noor-assistant' ) => 'double',
				esc_html__( 'Groove', 'noor-assistant' ) => 'groove',
				esc_html__( 'Ridge', 'noor-assistant' )  => 'ridge',
				esc_html__( 'Inset', 'noor-assistant' )  => 'inset',
				esc_html__( 'Outset', 'noor-assistant' ) => 'outset',
			),
		) );

		vc_add_param( 'vc_column', array(
			'param_name'  => 'border_color',
			'heading'     => esc_html__( 'Border Color', 'noor-assistant' ),
			'description' => esc_html__( 'Specify a border color.', 'noor-assistant' ),
			'save_always' => true,
			'group'       => __( 'Design Options', 'noor-assistant' ),
			'type'        => 'colorpicker',
			'holder'      => 'div'
		) );
		//**************Animation***************//
		vc_add_param( 'vc_column', array(
			'param_name'  => 'animation',
			'group'       => esc_html__( 'Animation', 'noor-assistant' ),
			'heading'     => esc_html__( 'Animation', 'noor-assistant' ),
			'description' => esc_html__( 'Select the type of animation you want to use.', 'noor-assistant' ),
			'type'        => 'dropdown',
			'holder'      => 'div',
			'value'       => $this->dima_animate_list_velocity,
		) );

		vc_add_param( 'vc_column', array(
				'param_name'  => 'delay',
				'group'       => esc_html__( 'Animation', 'noor-assistant' ),
				'heading'     => esc_html__( 'Delay Animation', 'noor-assistant' ),
				'description' => esc_html__( 'Set when the animation start (milliseconds)', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div',
				'value'       => ''
			)
		);

		vc_add_param( 'vc_column', array(
			'param_name'  => 'delay_duration',
			'group'       => esc_html__( 'Animation', 'noor-assistant' ),
			'heading'     => esc_html__( 'duration Animation', 'noor-assistant' ),
			'description' => esc_html__( 'Animation duration of each element  (milliseconds).', 'noor-assistant' ),
			'save_always' => true,
			'type'        => 'textfield',
			'holder'      => 'div',
			'value'       => ''
		) );

		vc_add_param( 'vc_column', array(
			'param_name'  => 'delay_offset',
			'group'       => esc_html__( 'Animation', 'noor-assistant' ),
			'heading'     => esc_html__( 'Delay Animation Offset', 'noor-assistant' ),
			'description' => esc_html__( 'Set the number ( px,%..) from the top of the viewport where the animation start.', 'noor-assistant' ) . "<a href='http://imakewebthings.com/waypoints/api/offset-option/' target='_blank' >" . esc_html__( 'Read More', 'noor-assistant' ) . "</a>",
			'save_always' => true,
			'type'        => 'textfield',
			'holder'      => 'div',
			'value'       => ''
		) );
		//**************!Animation***************//


	}

	public function dima_vc_update_column_inner() {

		/**
		 * vc_column_inner
		 */
		vc_remove_param( 'vc_column_inner', 'width' );
		vc_remove_param( 'vc_column_inner', 'font_color' );
		vc_remove_param( 'vc_column_inner', 'el_class' );
		vc_remove_param( 'vc_column_inner', 'css' );
		vc_remove_param( 'vc_column_inner', 'offset' );
		vc_remove_param( 'vc_column_inner', 'el_id' );


		//**************Animation***************//
		vc_add_param( 'vc_column_inner', array(
			'param_name'  => 'animation',
			'group'       => esc_html__( 'Animation', 'noor-assistant' ),
			'heading'     => esc_html__( 'Animation', 'noor-assistant' ),
			'description' => esc_html__( 'Select the type of animation you want to use.', 'noor-assistant' ),
			'type'        => 'dropdown',
			'holder'      => 'div',
			'value'       => $this->dima_animate_list_velocity,
		) );

		vc_add_param( 'vc_column_inner', array(
				'param_name'  => 'delay',
				'group'       => esc_html__( 'Animation', 'noor-assistant' ),
				'heading'     => esc_html__( 'Delay Animation', 'noor-assistant' ),
				'description' => esc_html__( 'Set when the animation start (milliseconds)', 'noor-assistant' ),
				'save_always' => true,
				'type'        => 'textfield',
				'holder'      => 'div',
				'value'       => ''
			)
		);

		vc_add_param( 'vc_column_inner', array(
			'param_name'  => 'delay_duration',
			'group'       => esc_html__( 'Animation', 'noor-assistant' ),
			'heading'     => esc_html__( 'duration Animation', 'noor-assistant' ),
			'description' => esc_html__( 'Animation duration of each element  (milliseconds).', 'noor-assistant' ),
			'save_always' => true,
			'type'        => 'textfield',
			'holder'      => 'div',
			'value'       => ''
		) );

		vc_add_param( 'vc_column_inner', array(
			'param_name'  => 'delay_offset',
			'group'       => esc_html__( 'Animation', 'noor-assistant' ),
			'heading'     => esc_html__( 'Delay Animation Offset', 'noor-assistant' ),
			'description' => esc_html__( 'Set the number ( px,%..) from the top of the viewport where the animation start.', 'noor-assistant' ) . "<a href='http://imakewebthings.com/waypoints/api/offset-option/' target='_blank' >" . esc_html__( 'Read More', 'noor-assistant' ) . "</a>",
			'save_always' => true,
			'type'        => 'textfield',
			'holder'      => 'div',
			'value'       => ''
		) );
		//**************!Animation***************//
		//4-ID
		vc_add_param( 'vc_column_inner', array(
			'param_name'  => 'id',
			'heading'     => esc_html__( 'ID', 'noor-assistant' ),
			'description' => $this->id_des,
			'save_always' => true,
			'type'        => 'textfield',
			'holder'      => 'div'
		) );
		//5-Class
		vc_add_param( 'vc_column_inner', array(
			'param_name'  => 'class',
			'heading'     => esc_html__( 'Class', 'noor-assistant' ),
			'description' => $this->class_des,
			'save_always' => true,
			'type'        => 'textfield',
			'holder'      => 'div'
		) );
		//Style
		vc_add_param( 'vc_column_inner', array(
			'param_name'  => 'style',
			'heading'     => esc_html__( 'Style', 'noor-assistant' ),
			'description' => $this->style_des,
			'save_always' => true,
			'type'        => 'textfield',
			'holder'      => 'div'
		) );

		//Width

		//1- Extra Large devices Desktops
		vc_add_param( 'vc_column_inner', array(
			'param_name'  => 'xld',
			'heading'     => esc_html__( 'Extra Large devices', 'noor-assistant' ),
			'group'       => esc_html__( 'Responsive Width', 'noor-assistant' ),
			'description' => esc_html__( 'Select column width for Extra Large devices Desktops ( ≥1600px ).', 'noor-assistant' ),
			'type'        => 'dropdown',
			'holder'      => 'div',
			'value'       => $this->vc_column_width_list,
			'std'         => ''
		) );
		//2- Large devices Desktops    
		vc_add_param( 'vc_column_inner', array(
			'param_name'  => 'ld',
			'heading'     => esc_html__( 'Large devices', 'noor-assistant' ),
			'group'       => esc_html__( 'Responsive Width', 'noor-assistant' ),
			'description' => esc_html__( 'Select column width for Large devices Desktops ( ≥1140px ).', 'noor-assistant' ),
			'type'        => 'dropdown',
			'holder'      => 'div',
			'value'       => $this->vc_column_width_list,
			'std'         => ''
		) );

		//4- Small devices    
		vc_add_param( 'vc_column_inner', array(
			'param_name'  => 'sd',
			'heading'     => esc_html__( 'Small devices Tablets', 'noor-assistant' ),
			'group'       => esc_html__( 'Responsive Width', 'noor-assistant' ),
			'description' => esc_html__( 'Select column width for Small devices Tablets ( ≥768px ).', 'noor-assistant' ),
			'type'        => 'dropdown',
			'holder'      => 'div',
			'value'       => $this->vc_column_width_list,
			'std'         => '6'
		) );
		//5- Extra small    
		vc_add_param( 'vc_column_inner', array(
			'param_name'  => 'xsd',
			'heading'     => esc_html__( 'Extra small devices Phones', 'noor-assistant' ),
			'group'       => esc_html__( 'Responsive Width', 'noor-assistant' ),
			'description' => esc_html__( 'Select column width for Extra small devices Phones ( <768px ).', 'noor-assistant' ),
			'type'        => 'dropdown',
			'holder'      => 'div',
			'value'       => $this->vc_column_width_list,
			'std'         => '12'
		) );


		//Offset

		//1- Extra Large devices Desktops
		vc_add_param( 'vc_column_inner', array(
			'param_name'  => 'offset_xld',
			'heading'     => esc_html__( 'Extra Large devices', 'noor-assistant' ),
			'group'       => esc_html__( 'Responsive Offset', 'noor-assistant' ),
			'description' => esc_html__( 'Select column offset for Extra Large devices Desktops ( ≥1600px ).', 'noor-assistant' ),
			'type'        => 'dropdown',
			'holder'      => 'div',
			'value'       => $this->vc_column_width_list,
			'std'         => ''
		) );
		//2- Large devices Desktops    
		vc_add_param( 'vc_column_inner', array(
			'param_name'  => 'offset_ld',
			'heading'     => esc_html__( 'Large devices Desktops  ', 'noor-assistant' ),
			'group'       => esc_html__( 'Responsive Offset', 'noor-assistant' ),
			'description' => esc_html__( 'Select column offset for Large devices Desktops ( ≥1140px ).', 'noor-assistant' ),
			'type'        => 'dropdown',
			'holder'      => 'div',
			'value'       => $this->vc_column_width_list,
			'std'         => ''
		) );
		//3- Medium Devise    
		vc_add_param( 'vc_column_inner', array(
			'param_name'  => 'offset_md',
			'heading'     => esc_html__( 'Medium Devise', 'noor-assistant' ),
			'group'       => esc_html__( 'Responsive Offset', 'noor-assistant' ),
			'description' => esc_html__( 'Select column offset for medium devise ( ≥989px ).', 'noor-assistant' ),
			'type'        => 'dropdown',
			'holder'      => 'div',
			'value'       => $this->vc_column_width_list,
			'std'         => ''
		) );


		//hidden

		//1- Extra Large devices Desktops
		vc_add_param( 'vc_column_inner', array(
			'param_name'  => 'visibility_xld',
			'heading'     => esc_html__( 'Extra Large devices', 'noor-assistant' ),
			'group'       => esc_html__( 'Responsive Visibility', 'noor-assistant' ),
			'description' => esc_html__( 'Select column width for Extra Large devices Desktops ( ≥1600px ).', 'noor-assistant' ),
			'type'        => 'checkbox',
			'holder'      => 'div',
			'value'       => $this->vc_column_visibility,
			'std'         => ''
		) );


		//2- Large devices Desktops    
		vc_add_param( 'vc_column_inner', array(
			'param_name'  => 'visibility_ld',
			'heading'     => esc_html__( 'Large devices', 'noor-assistant' ),
			'group'       => esc_html__( 'Responsive Visibility', 'noor-assistant' ),
			'description' => esc_html__( 'Select column width for Large devices Desktops ( ≥1140px ).', 'noor-assistant' ),
			'type'        => 'checkbox',
			'holder'      => 'div',
			'value'       => $this->vc_column_visibility,
			'std'         => ''
		) );

		//3- Medium Devise    
		vc_add_param( 'vc_column_inner', array(
			'param_name'  => 'visibility_md',
			'heading'     => esc_html__( 'Medium Devise', 'noor-assistant' ),
			'group'       => esc_html__( 'Responsive Visibility', 'noor-assistant' ),
			'description' => esc_html__( 'Select column width for medium devise ( ≥989px ).', 'noor-assistant' ),
			'type'        => 'checkbox',
			'holder'      => 'div',
			'value'       => $this->vc_column_visibility,
			'std'         => ''
		) );

		//4- Small devices    
		vc_add_param( 'vc_column_inner', array(
			'param_name'  => 'visibility_sd',
			'heading'     => esc_html__( 'Small devices', 'noor-assistant' ),
			'group'       => esc_html__( 'Responsive Visibility', 'noor-assistant' ),
			'description' => esc_html__( 'Select column width for Small devices Tablets ( ≥768px ).', 'noor-assistant' ),
			'type'        => 'checkbox',
			'holder'      => 'div',
			'value'       => $this->vc_column_visibility,
			'std'         => ''
		) );

		//5- Extra small    
		vc_add_param( 'vc_column_inner', array(
			'param_name'  => 'visibility_xsd',
			'heading'     => esc_html__( 'Extra small devices', 'noor-assistant' ),
			'group'       => esc_html__( 'Responsive Visibility', 'noor-assistant' ),
			'description' => esc_html__( 'Select column width for Extra small devices Phones ( <768px ).', 'noor-assistant' ),
			'type'        => 'checkbox',
			'holder'      => 'div',
			'value'       => $this->vc_column_visibility,
			'std'         => ''
		) );

		//
		// Design Options
		//
		vc_add_param( 'vc_column_inner', array(
			'type'       => 'css_editor',
			'group'      => __( 'Design Options', 'noor-assistant' ),
			'heading'    => __( 'CSS box', 'noor-assistant' ),
			'param_name' => 'css',
		) );

		vc_add_param( 'vc_column_inner', array(
			'param_name'  => 'border_style',
			'heading'     => esc_html__( 'Border style.', 'noor-assistant' ),
			'description' => esc_html__( 'Specify a border style', 'noor-assistant' ),
			'type'        => 'dropdown',
			'holder'      => 'div',
			'group'       => __( 'Design Options', 'noor-assistant' ),
			'value'       => array(
				esc_html__( 'Hidden', 'noor-assistant' ) => 'hidden',
				esc_html__( 'Dashed', 'noor-assistant' ) => 'dashed',
				esc_html__( 'Solid', 'noor-assistant' )  => 'solid',
				esc_html__( 'Double', 'noor-assistant' ) => 'double',
				esc_html__( 'Groove', 'noor-assistant' ) => 'groove',
				esc_html__( 'Ridge', 'noor-assistant' )  => 'ridge',
				esc_html__( 'Inset', 'noor-assistant' )  => 'inset',
				esc_html__( 'Outset', 'noor-assistant' ) => 'outset',
			),
		) );

		vc_add_param( 'vc_column_inner', array(
			'param_name'  => 'border_color',
			'heading'     => esc_html__( 'Border Color', 'noor-assistant' ),
			'description' => esc_html__( 'Specify a border color.', 'noor-assistant' ),
			'save_always' => true,
			'group'       => __( 'Design Options', 'noor-assistant' ),
			'type'        => 'colorpicker',
			'holder'      => 'div'
		) );

		//
		// Translate
		//
		self::dima_vc_translate( 'vc_column_inner' );
	}

	public function dima_vc_update_widget() {

		vc_map_update( 'vc_widget_sidebar', array(
			'name'        => esc_html__( 'Widget Area', 'noor-assistant' ),
			'weight'      => 950,
			'class'       => 'dima-vc-element dima-vc-element-widget-sidebar',
			'icon'        => 'dima-widget',
			'category'    => esc_html__( 'Content', 'noor-assistant' ),
			'description' => esc_html__( 'WordPress widgetised sidebar', 'noor-assistant' ),
		) );

		vc_remove_param( 'vc_widget_sidebar', 'title' );
		vc_remove_param( 'vc_widget_sidebar', 'el_class' );

	}

}

GLOBAL $dima_vc;
$dima_vc = new DIMA_VC();

include_once DIMA_NOUR_ASSISTANT_TEMPLATE_PATH . '/include/dima-extensions/vc_custom/dima-vc-config.php';

include_once DIMA_NOUR_ASSISTANT_TEMPLATE_PATH . '/include/dima-extensions/vc_custom/params.php';

if ( ! function_exists( 'dima_vc_map_shortcodes' ) && dima_vc_config_on() ) {
	foreach ( glob( DIMA_NOUR_ASSISTANT_TEMPLATE_PATH . '/include/dima-extensions/vc_custom/map_shortcodes/*.php' ) as $shortcode ) {
		require_once( $shortcode );
	}
}
