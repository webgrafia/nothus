<?php
/**
 * DIMA Framework
 * WARNING: This file is part of the DIMA Core Framework.
 * Do not edit the core files.
 *
 * @package Dima Framework
 * @subpackage Extensions
 * @version   1.0.0
 * @since     1.0.0
 * @author    PixelDima <info@pixeldima.com>
 */


if ( ! function_exists( 'dima_vc_backend_styles' ) ) :
	function dima_vc_backend_styles() {
		wp_register_script( 'dima-vc-admin-js', DIMA_NOUR_ASSISTANT_URL . '/js/js_vc_backend_extend.js', array( 'jquery' ), DIMA_NOUR_ASSISTANT_VERSION, true );
		wp_enqueue_script( 'dima-vc-admin-js' );
	}
endif;

add_action( 'vc_backend_editor_render', 'dima_vc_backend_styles' );


if ( ! function_exists( 'dima_vc_back_and_frontend_styles' ) ) :
	function dima_vc_back_and_frontend_styles() {

		wp_register_script( 'dima-vc-all-admin-js', DIMA_NOUR_ASSISTANT_URL . '/js/js_dima_vc.js', array( 'jquery' ), DIMA_NOUR_ASSISTANT_VERSION, true );
		wp_enqueue_script( 'dima-vc-all-admin-js' );

		if ( DIMA_VISUAL_COMOPSER_IS_ACTIVE ) {
			if ( is_rtl() ) {
				wp_enqueue_style( 'dima-vc-admin-css', DIMA_NOUR_ASSISTANT_URL . '/css/vc_admin_rtl.css', false, DIMA_NOUR_ASSISTANT_VERSION, 'all' );
			} else {
				wp_enqueue_style( 'dima-vc-admin-css', DIMA_NOUR_ASSISTANT_URL . '/css/vc_admin.css', false, DIMA_NOUR_ASSISTANT_VERSION, 'all' );
			}
			wp_enqueue_style( 'dima-visual-composer', DIMA_NOUR_ASSISTANT_URL . '/css/visual-composer.css', null, DIMA_NOUR_ASSISTANT_VERSION, 'all' );
		}
	}
endif;
add_action( 'vc_frontend_editor_render', 'dima_vc_back_and_frontend_styles' );
add_action( 'vc_backend_editor_render', 'dima_vc_back_and_frontend_styles' );


if ( ! function_exists( 'dima_vc_frontend_styles' ) ):
	function dima_vc_frontend_styles() {
		if ( DIMA_VISUAL_COMOPSER_IS_ACTIVE ) {
			if ( is_rtl() ) {
				wp_enqueue_style( 'dima-vc-frontend-css', DIMA_NOUR_ASSISTANT_URL . '/css/vc_admin_frontend_rtl.css', false, DIMA_NOUR_ASSISTANT_VERSION, 'all' );
			} else {
				wp_enqueue_style( 'dima-vc-frontend-css', DIMA_NOUR_ASSISTANT_URL . '/css/vc_admin_frontend.css', false, DIMA_NOUR_ASSISTANT_VERSION, 'all' );
			}
		}
	}
endif;

add_action( 'vc_frontend_editor_render', 'dima_vc_frontend_styles' );
add_action( 'vc_inline_editor_page_view', 'dima_vc_frontend_styles' );

/**
 * override directory where Visual Composer should look for template
 */
if ( function_exists( 'vc_set_shortcodes_templates_dir' ) && dima_vc_config_on() ) {
	$dir = DIMA_NOUR_ASSISTANT_TEMPLATE_PATH . '/include/dima-extensions/vc_custom/vc_templates';
	vc_set_shortcodes_templates_dir( $dir );
}


function dima_vc_update() {
	if ( defined( 'WPB_VC_VERSION' ) && version_compare( WPB_VC_VERSION, '5.10', '<' ) ) {
		do_action( 'vc_before_init' );
	}
}

add_action( 'admin_init', 'dima_vc_update' );


function dima_vc_config_on() {
	return get_option( 'wpb_js_dima_config', true );
}

function dima_vc_integration_on() {
	return get_option( 'wpb_js_dima_vc_int', false );
}

function dima_vc_sanitize_checkbox( $value ) {
	return $value;
}


/**
 * Remove "Design options", "Custom CSS" tabs and prompt message to activate Visual Compos
 */

function dima_vc_setAsTheme() {
	if ( dima_vc_config_on() ) {
		vc_set_as_theme();
	} else {
		add_action( 'admin_notices', 'dima_vc_stop_update_notice', - 99 );
		vc_manager()->disableUpdater();
	}
}

add_action( 'vc_before_init', 'dima_vc_setAsTheme' );

function dima_vc_stop_update_notice() {
	remove_action( 'admin_notices', array( vc_license(), 'adminNoticeLicenseActivation' ) );
}

/**
 * [dima_vc_add_options_tab add Noor config tab]
 *
 * @param $tabs
 *
 * @return mixed
 */
function dima_vc_add_options_tab( $tabs ) {
	$tabs['noor-config'] = 'Noor';

	return $tabs;
}

add_filter( 'vc_settings_tabs', 'dima_vc_add_options_tab' );


/**
 * Tab: Noor Config Settings
 *
 * @param $vc_config
 */
function dima_vc_add_setting_fields( $vc_config ) {
	$tab = 'noor-config';
	$vc_config->addSection( $tab );
	$vc_config->addField( $tab, esc_html__( 'Noor Integration', 'noor-assistant' ), 'dima_config', 'dima_vc_sanitize_checkbox', 'dima_vc_dima_config' );
	$vc_config->addField( $tab, esc_html__( 'Visual Composer Integration', 'noor-assistant' ), 'dima_vc_int', 'dima_vc_sanitize_checkbox', 'dima_vc_dima_vc_int' );
}

add_action( 'vc_settings_tab-noor-config', 'dima_vc_add_setting_fields' );

/**
 * backward compatibility for vc_add_shortcode_param function
 *
 * @param string $param_name
 * @param string $param_function
 */
function dima_add_shortcode_param( $param_name, $param_function ) {
	if ( version_compare( WPB_VC_VERSION, '5.0', '>=' ) ) {
		vc_add_shortcode_param( $param_name, $param_function );
	} else {
		add_shortcode_param( $param_name, $param_function );
	}
}

/**
 * [dima_vc_options checkbox function]
 *
 * @param  [type] $setting_id  []
 * @param  [type] $default     []
 * @param  [type] $label       []
 * @param  [type] $description []
 */
function dima_vc_options( $setting_id, $default, $label, $description ) {
	$checked = ( $checked = get_option( 'wpb_js_' . $setting_id, $default ) ) ? $checked : false; ?>

    <label>
        <input type="checkbox"<?php echo( $checked ? ' checked="checked";' : '' ) ?> value="1"
               id="wpb_js_<?php echo $setting_id ?>" name="<?php echo 'wpb_js_' . esc_attr( $setting_id ) ?>">
		<?php echo esc_attr( $label ); ?>
    </label>
    <br/>
    <p class="dima-des description indicator-hint"><?php echo esc_html( $description ); ?></p>

	<?php
}

function dima_vc_dima_config() {
	return dima_vc_options( 'dima_config', true, esc_html__( 'Enable', 'noor-assistant' ), esc_html__( 'To be able to use Noor Shortcodes with Visual Composer you need to turn the Noor integration on.', 'noor-assistant' ) );
}

function dima_vc_dima_vc_int() {
	return dima_vc_options( 'dima_vc_int', false, esc_html__( 'Enable', 'noor-assistant' ), esc_html__( 'To be able to use All Visual Composer elemnts you need to turn the Visual Composer Integration on.', 'noor-assistant' ) );
}

/**
 *
 */
function dima_vc_disable_frontend_editor() {
	return dima_vc_options( 'dima_disable_frontend_editor', false, esc_html__( 'Remove', 'noor-assistant' ), esc_html__( 'Remove the Frontend editor.', 'noor-assistant' ) );
}

/**
 * Remove Default VC elements
 */
function dima_fix_tab_vc_bug() {
	vc_remove_element( "vc_tta_tabs" );
	vc_remove_element( "vc_tabs" );
	vc_remove_element( "vc_tta_tour" );
	vc_remove_element( "vc_tta_pageable" );
}

add_action( 'vc_before_init', 'dima_fix_tab_vc_bug' );

if ( ! function_exists( 'dima_vc_remove_default_shortcodes' && dima_vc_config_on() ) ) {
	function dima_vc_remove_default_shortcodes() {
		vc_remove_element( "vc_tta_tabs" );
		vc_remove_element( "vc_tta_accordion" );
		vc_remove_element( "vc_tta_tour" );
		vc_remove_element( "vc_tta_pageable" );
		vc_remove_element( "vc_text_separator" );
		vc_remove_element( "vc_images_carousel" );
		vc_remove_element( "vc_tour" );
		vc_remove_element( "vc_tabs" );
		vc_remove_element( "vc_teaser_grid" );
		vc_remove_element( "vc_posts_grid" );
		vc_remove_element( "vc_carousel" );
		vc_remove_element( "vc_posts_slider" );
		vc_remove_element( "vc_button2" );
		vc_remove_element( "vc_button" );
		vc_remove_element( "vc_accordion" );
		vc_remove_element( "vc_cta" );
		vc_remove_element( "vc_btn" );
		vc_remove_element( "vc_cta_button" );
		vc_remove_element( "vc_cta_button2" );
		vc_remove_element( "vc_basic_grid" );
		vc_remove_element( "vc_media_grid" );
		vc_remove_element( "vc_masonry_grid" );
		vc_remove_element( "vc_masonry_media_grid" );
		vc_remove_element( 'vc_gallery' );
		vc_remove_element( "vc_video" );
		vc_remove_element( "vc_column_text" );
		vc_remove_element( "vc_separator" );
		vc_remove_element( "vc_icon" );
		vc_remove_element( "vc_progress_bar" );
		vc_remove_element( "vc_empty_space" );
		vc_remove_element( "vc_message" );
		vc_remove_element( "vc_custom_heading" );
		vc_remove_element( "vc_flickr" );
		vc_remove_element( "vc_single_image" );
		vc_remove_element( "vc_gmaps" );
		vc_remove_element( "vc_facebook" );
		vc_remove_element( "vc_tweetmeme" );
		vc_remove_element( "vc_googleplus" );
		vc_remove_element( "vc_pinterest" );
		vc_remove_element( "vc_toggle" );
	}

	//Allow only PixelDima elements to be used on this site.

	if ( ! dima_vc_integration_on() ) {
		add_action( 'vc_before_init', 'dima_vc_remove_default_shortcodes' );
	}
}

/**
 * Remove Default Templates
 */
add_filter( 'vc_load_default_templates', '__return_empty_array', 1 );

if ( dima_vc_config_on() ) {

	/**
	 * [dima_vc_is_frontend check if frontend editing is active]
	 * @return bool
	 */
	function dima_vc_is_frontend() {
		return ( function_exists( 'vc_manager' ) && vc_manager()->mode() == 'page_editable' );
	}

	function dima_vc_custom_configure() {

		if ( ! dima_vc_is_frontend() ) {
			return;
		}
		add_action( 'wp_enqueue_scripts', 'dima_vc_libs', 999 );
		add_action( 'dima_head_css', 'dima_vc_custom_css', 999 );
	}

	add_action( 'init', 'dima_vc_custom_configure' );

	function dima_vc_libs() {
		wp_enqueue_script( 'mediaelement' );
	}

	function dima_vc_custom_css() {

		?>
        [class*="vc_col-"] [class*="ok-"] {
        width: 100%;
        }

        .vc_element:hover:before {
        content: '';
        margin: 0;
        padding: 0;
        position: absolute;
        z-index: 0;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        outline: #ccc dashed 1px;
        }

        .vc_vc_column:last-of-type {
        margin-right: 0;
        }

        @media (max-width: 767px) {
        .vc_vc_column[class*="vc_col"] {
        float: none;
        width: 100%;
        margin-right: 0;
        }
        }
		<?php
	}
}
?>