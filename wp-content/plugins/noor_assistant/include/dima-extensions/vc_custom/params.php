<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! class_exists( 'dima_Params_Constructor' ) ) {
	class dima_Params_Constructor {
		function __construct() {

		}

		function style_select( $value, $option, $title ) {
			$html  = '';
			$value = isset( $value ) && ! empty( $value ) ? $value : '';

			$html .= '			<option value="' . $option . '" ' . ( $option == $value ? 'selected' : '' ) . '>' . $title . '</option>';

			return $html;
		}

		function input_number( $value, $class, $placeholder ) {
			$html  = '';
			$name  = isset( $name ) && ! empty( $name ) ? $name : '';
			$value = isset( $value ) && $value != '' ? $value : '';
			$class = isset( $class ) && ! empty( $class ) ? $class : '';

			$html .= '	<input type="number" class="wpb_vc_param_value crum_number_field vc_container_form_field-' . esc_attr( $class ) . '" placeholder="' . esc_attr( $placeholder ) . '" value="' . $value . '" />';

			return $html;
		}

		function input_radio( $value, $option, $text, $list = true, $class = '' ) {
			$html    = '';
			$name    = isset( $name ) && ! empty( $name ) ? $name : '';
			$value   = isset( $value ) && $value != '' ? $value : '';
			$class   = isset( $class ) && ! empty( $class ) ? $class : '';
			$checked = $value == $option ? 'checked="checked"' : '';

			$html .= '	<label><input type="radio" ' . $checked . ' value="' . $option . '" />' . esc_html( $text ) . '</label>';

			if ( $list ) {
				$li_class = '';
				if ( $checked != '' ) {
					$li_class = 'active';
				}
				$html = '<li class="' . esc_attr( $li_class ) . '">' . $html . '</li>';
			}

			return $html;
		}

		function input_checkbox( $value, $label, $name, $checked, $class = '' ) {
			$html    = '';
			$value   = isset( $value ) && $value != '' ? $value : '';
			$label   = isset( $label ) && $label != '' ? $label : '';
			$checked = isset( $checked ) && $checked ? 'checked="checked"' : '';
			$class   = isset( $class ) && ! empty( $class ) ? $class : '';
			if ( $label != '' ) {
				$html .= '<label class="vc_checkbox-label">';
			}
			$html .= '<input type="checkbox" class="wpb_vc_param_value checkbox ' . esc_attr( $class ) . '" name="' . esc_attr( $name ) . '" value="' . $value . '" ' . $checked . ' />';
			if ( $label != '' ) {
				$html .= $label;
				$html .= '</label>';
			}

			return $html;
		}
	}


	foreach ( glob( DIMA_NOUR_ASSISTANT_TEMPLATE_PATH . '/include/dima-extensions/vc_custom/params/*.php' ) as $shortcode ) {
		require_once( $shortcode );
	}
}