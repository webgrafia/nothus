<?php
/**
 * Functions pertaining to content output.
 *
 * @package global
 * @subpackage 1-content
 * @version   1.0.0
 * @since     1.0.0
 * @author    PixelDima <info@pixeldima.com>
 *
 */

/**
 * @param       $element
 * @param array $params
 *
 * @return string
 */
function dima_creat_data_attributes( $element, $params = array() ) {
	$data = 'data-dima-element="' . $element . '"';

	if ( ! empty( $params ) ) {
		$params_json = htmlspecialchars( json_encode( $params ), ENT_QUOTES, 'UTF-8' );

		$data .= ' data-dima-params="' . $params_json . '"';
	}

	return $data;
}

function dima_short_remove_white_space( $text ) {
	$text = preg_replace( '/[\t\n\r\0\x0B]/', '', $text );
	$text = preg_replace( '/([\s])\1+/', ' ', $text );
	$text = trim( $text );

	return $text;
}

/**
 * @param $output
 *
 * @return array
 */
function dima_shortcodes_add_body_class( $output ) {

	$version = str_replace( '.', '_', DIMA_NOUR_ASSISTANT_VERSION );

	$output[] = 'dima-shortcodes-version' . $version;

	return $output;

}

add_filter( 'body_class', 'dima_shortcodes_add_body_class', 9999 );

// Allow shortcodes in widgets.
add_filter( 'widget_text', 'do_shortcode' );

function dima_get_admin_media_post() {
	$id        = vc_post_param( 'content' );
	$back_post = get_post( $id );
	if ( $back_post == '' ) {
		return;
	}
	$post_mime = $back_post->post_mime_type;

	$back_url = $back_icon = $img_name = $img_path = '';
	if ( strpos( $post_mime, 'image/' ) !== false ) {
		$background_url = wp_get_attachment_thumb_url( $id );
		$back_url       = ( $background_url != '' ) ? 'background-image: url(' . $background_url . ');' : '';
		$img_name       = get_the_title( $id );
		$img_path       = $background_url;
	} else if ( strpos( $post_mime, 'pattern/' ) !== false ) {
		$background_url = wp_get_attachment_thumb_url( $id );
		$back_url       = ( $background_url != '' ) ? 'background-image: url(' . $background_url . ');' : '';
		$img_name       = get_the_title( $id );
		$img_path       = $background_url;
	} else if ( strpos( $post_mime, 'video/' ) !== false ) {
		$back_icon = '<i class="fa fa-media-play" />';
	} else {
		switch ( $post_mime ) {
			case 'oembed/flickr':
			case 'oembed/instagram':
			case 'oembed/Imgur':
			case 'oembed/photobucket':
				$back_oembed = wp_oembed_get( $back_post->guid );
				preg_match_all( '/src="([^"]*)"/i', $back_oembed, $img_src );
				$back_url = ( isset( $img_src[1][0] ) ) ? 'background-image: url(' . str_replace( '"', '', $img_src[1][0] ) . ');' : '';
				break;

			case 'oembed/vimeo':
			case 'oembed/youtube':
				$back_icon = '<i class="fa fa-social-' . str_replace( 'oembed/', '', $post_mime ) . '" />';
				break;
		}
	}

	echo json_encode( array(
		'back_url'  => $back_url,
		'img_name'  => $img_name,
		'img_path'  => $img_path,
		'back_icon' => $back_icon,
		'back_mime' => $post_mime
	) );
	die();
}

add_action( 'wp_ajax_dima_get_media_post', 'dima_get_admin_media_post' );

if ( ! function_exists( 'dima_is_gradient_stop_transparent' ) ) {
	function dima_is_gradient_stop_transparent( $color ) {
		if ( strpos( $color, 'rgba' ) !== false ) {
			$var = $color;
			$var = str_replace( 'rgba(', '', $var );
			$var = str_replace( ')', '', $var );
			$var = explode( ',', $var );

			if ( floatval( $var[3] ) > 0.05 ) {
				return false;
			}

			return true;
		}

		return false;
	}
}

/**
 * Get template parts from shortcodes folder
 *
 * @param string  $slug
 * @param string  $name
 * @param boolean $return
 *
 * @return object
 *
 */
if ( ! function_exists( 'dima_get_shortcode_view' ) ) {
	function dima_get_shortcode_view( $shortcode_name, $name = '', $return = false, $view_params = array() ) {
		if ( $return ) {
			ob_start();
			dima_get_template_part( plugin_dir_path( __DIR__ ) . '/include/shortcodes/' . $shortcode_name . '/' . $name, $view_params );

			return ob_get_clean();
		} else {
			dima_get_template_part( plugin_dir_path( __DIR__ ) . '/include/shortcodes/' . $shortcode_name . '/' . $name, $view_params );
		}

	}
}

/**
 * Like get_template_part() put lets you pass args to the template file
 * Args are available in the tempalte as $view_params array
 *
 * @param string filepart
 * @param mixed wp_args style argument list
 *
 * @since 5.0.0
 * @since 5.9.1 Refactored the function to improve performance.
 */
if ( ! function_exists( 'dima_get_template_part' ) ) {
	function dima_get_template_part( $file, $view_params = array() ) {
		global $post;

		if ( file_exists( $file . '.php' ) ) {

			$file_path = ( $file . '.php' );

		} elseif ( file_exists( $file . '.php' ) ) {

			$file_path = realpath( $file . '.php' );

		}

		wp_parse_args( $view_params );
		ob_start();
		require( $file_path );
		$output = ob_get_clean();

		echo $output;


	}
}