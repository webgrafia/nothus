<?php
/**
 * Setup : Include all admin pages
 *
 * @package Dima_Framework
 * @subpackage admin
 * @version   1.0.0
 * @since     1.0.0
 * @author    PixelDima <info@pixeldima.com>
 * @copyright    (c) Copyright by PixelDima
 *
 */



// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

$theme_settings_path = DIMA_TEMPLATE_PATH . '/framework/functions/admin/pixeldima-setup';

/*-----------------------------*/
# Require Files
/*-----------------------------*/
require_once( $theme_settings_path . '/pixeldima-setup-settings.php' );
require_once( $theme_settings_path . '/framework-notices.php' );
require_once( $theme_settings_path . '/framework-home.php' );
require_once( $theme_settings_path . '/framework-customizer-backup.php' );
require_once( $theme_settings_path . '/framework-demo-content.php' );
require_once( $theme_settings_path . '/framework-plugins.php' );
require_once( $theme_settings_path . '/framework-system-status.php' );

/**
 * Setup Menu
 *
 * @return void
 */
function dima_sc_setup_add_menu() {
	add_menu_page( 'NOOR: Home', 'NOOR', 'manage_options', 'pixel-dima-dashboard', 'dima_setup_page_home', null, 3 );
	add_submenu_page( 'pixel-dima-dashboard', 'NOOR: Home', esc_html__( 'Welcome', 'noor-assistant' ), 'manage_options', 'pixel-dima-dashboard', 'dima_setup_page_home' );
	add_submenu_page( 'pixel-dima-dashboard', 'NOOR: Demos', esc_html__( 'Install Demos', 'noor-assistant' ), 'manage_options', 'pixeldima-demo', 'dima_setup_demo_content' );
	add_submenu_page( 'pixel-dima-dashboard', 'NOOR: Plugins', esc_html__( 'Plugins', 'noor-assistant' ), 'manage_options', 'pixeldima-setup-plugins', 'dima_setup_plugins' );
	add_submenu_page( 'pixel-dima-dashboard', 'NOOR: backup', esc_html__( 'Customizer Backup', 'noor-assistant' ), 'manage_options', 'pixeldima-customizer-backup', 'dima_page_customizer_backup' );
	add_submenu_page( 'pixel-dima-dashboard', 'NOOR: system', esc_html__( 'System Status', 'noor-assistant' ), 'manage_options', 'pixeldima-customizer-system', 'dima_setup_page_system_status' );
}

add_action( 'admin_menu', 'dima_sc_setup_add_menu' );

function dima_sc_get_link_plugins() {
	return admin_url( 'admin.php?page=pixeldima-setup-plugins' );
}
